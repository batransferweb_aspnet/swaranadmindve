﻿
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System.Configuration;

namespace SwaranAdmin_New.Base
{
   
    public class BaGoogleApi
    {

        /// <summary>
        /// Get distance by coordination
        /// </summary>
        /// <param name="coodination"></param>
        /// <returns></returns>
        public static GoogleApiVal GetDistanceOld(string fromAddress, string toAddress)
        {
            var apiValues = new GoogleApiVal();

            string status = string.Empty;
            JObject o = new JObject();
            string url = string.Empty;
            //Get mils value  ---->   &units=imperial
            //Km              ---->   &units=metric
            //AIzaSyBeJaXWhM_mH0oHd-7bYubDDI-K5svYCxI 

            url = ApiUrlHelper.GetUrl((int)ApiUrlEnum.GoogleApiServices) + fromAddress + "&destinations=" + toAddress + "&mode=driving&units=imperial&language=en-GB&key=AIzaSyBeJaXWhM_mH0oHd-7bYubDDI-K5svYCxI";

            //url =
            //    "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + fromAddress + "&destinations=" + toAddress + "&mode=driving&units=imperial&language=en-GB&key=AIzaSyBeJaXWhM_mH0oHd-7bYubDDI-K5svYCxI";

            //url =
            //    "https://maps.googleapis.com/maps/api/distancematrix/json?origins=53.3983070,-2.9821578&destinations=53.3955249,-2.9780799&mode=driving&units=imperial&language=en-GB";

            try
            {
                string requesturl = url;
                string content = FileGetContents(requesturl);
                o = JObject.Parse(content);

                status = o.SelectToken("status").ToString();
            }
            catch (Exception ex)
            {
                GetDistance(fromAddress, toAddress);
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, "GoogleApiVal GetDistance()", ex.InnerException != null ? ex.InnerException.Message : "");
            }
            if (status.Equals("OK"))
            {

                try
                {
                    decimal distanceMeters =
                        decimal.Parse(o.SelectToken("rows[0].elements[0].distance.value").ToString());
                    apiValues.Distance = Math.Round(ConvertMetersToMiles(distanceMeters), 2);
                    apiValues.Duration = int.Parse(o.SelectToken("rows[0].elements[0].duration.value").ToString()) / 60;

                }
                catch (Exception ex)
                {
                    ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, "GoogleApiVal GetDistance()", ex.InnerException != null ? ex.InnerException.Message : "");
                }
            }

            return apiValues;
        }

        public static decimal ConvertMetersToMiles(decimal meters)
        {
            return (meters / 1609.344M);
        }

        /// <summary>
        /// Get distance by post code
        /// </summary>
        /// <param name="coodination"></param>
        /// <returns></returns>


        private static string FileGetContents(string fileName)
        {
            string sContents = string.Empty;
            string me = string.Empty;
            try
            {
                if (fileName.ToLower().IndexOf("https:") > -1)
                {
                    WebClient wc = new WebClient();
                    byte[] response = wc.DownloadData(fileName);
                    sContents = System.Text.Encoding.ASCII.GetString(response);

                }
                else
                {
                    StreamReader sr = new StreamReader(fileName);
                    sContents = sr.ReadToEnd();
                    sr.Close();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, "FileGetContents", ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return sContents;
        }


        public static GoogleApiCurrentLocation GetGoogleApiCurrentLocation(string address, string key)
        {
            var googleApiCurrentLocation = new GoogleApiCurrentLocation();

            string status = string.Empty;
            var latLong = new string[2];
            JObject o = new JObject();
            string url = string.Empty;

            url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + address.Trim() + key + "";

            try
            {
                string requesturl = url;
                //https://maps.googleapis.com/maps/api/geocode/json?address=53.4687404,-2.9137853
                //string requesturl = @"https://maps.googleapis.com/maps/api/geocode/json?address=Spellbound%20Productions%20Ltd%20Cowdenbeath%20Path%20London%20N1%200LG&key=AIzaSyC7CYyUwwJzA5uuU6MK4frKZkOuWvZlg0I";
                string content = FileGetContents(requesturl);
                o = JObject.Parse(content);

                status = o.SelectToken("status").ToString();
            }
            catch (Exception ex)
            {
                googleApiCurrentLocation = GetGoogleApiCurrentLocation(address, key);

            }



            if (status.Equals("ZERO_RESULTS"))
            {

                latLong[0] = "0.00";
                latLong[1] = "0.00";

            }
            else if (status.Equals("OK"))
            {

                try
                {
                    //  latLong[0] = o.SelectToken("results[0].geometry.location.lat").ToString();
                    //latLong[1] = o.SelectToken("results[0].geometry.location.lng").ToString();
                    googleApiCurrentLocation.Address = o.SelectToken("results[0].formatted_address").ToString();
                    googleApiCurrentLocation.Town = o.SelectToken("results[0].address_components[4].long_name").ToString();
                    googleApiCurrentLocation.PostCode = o.SelectToken("results[0].address_components[6].long_name").ToString();

                }
                catch (Exception ex)
                {

                }
                //return latLong;

            }
            else
            {

                googleApiCurrentLocation = GetGoogleApiCurrentLocation(address, key);
            }

            return googleApiCurrentLocation;
        }

        public static List<GoogleApiDistanceVal> GetDistance(string fromAddress, string toAddress)
        {
            var list = new List<GoogleApiDistanceVal>();
            var shortlist = new List<GoogleApiDistanceVal>();
            string status = string.Empty;
            JObject o = new JObject();
            string url = string.Empty;

            string apikey = string.Empty;

            try
            {


                while (true)
                {
                    apikey = ConfigHelper.GetDistanceApiKey();
                    url =
                        "https://maps.googleapis.com/maps/api/directions/json?origin=" + fromAddress + "&destination=" + toAddress + "" +
                        "&units=imperial&alternatives=true&key=" + apikey;
                    string requesturl = url;
                    string content = FileGetContents(requesturl);
                    o = JObject.Parse(content);

                    status = o.SelectToken("status").ToString();
                    if (status.Equals("OK"))
                    {
                        using (var db = new BATransferEntities())
                        {
                            if (db.DistanceKeyTables.Any(x => x.KeyName.Equals(apikey)))
                            {
                                var obj = db.DistanceKeyTables.First(x => x.KeyName.Equals(apikey));
                                obj.NumberOfRequised = obj.NumberOfRequised + 1;
                                db.SaveChanges();
                            }

                        }

                        break;
                    }
                    else
                    {
                        if (!status.Equals("OVER_QUERY_LIMIT"))
                        {
                            ErrorHandling.LogFileWrite(status, apikey, status, url);
                        }
                    }
                }

                if (status.Equals("OK"))
                {


                    var val1 = new GoogleApiDistanceVal();
                    if (o.SelectToken("routes[0].legs[0].distance.value") != null)
                    {
                        decimal distanceMeters =
                            decimal.Parse(o.SelectToken("routes[0].legs[0].distance.value").ToString());
                        val1.Distance = System.Math.Round(ConvertMetersToMiles(distanceMeters), 2);
                        val1.Time =
                            System.Math.Round(
                                decimal.Parse(o.SelectToken("routes[0].legs[0].duration.value").ToString()) / 60, 2);
                        list.Add(val1);
                    }

                    var val_2 = new GoogleApiDistanceVal();
                    if (o.SelectToken("routes[1].legs[0].distance.value") != null)
                    {
                        decimal distanceMeters2 =
                            decimal.Parse(o.SelectToken("routes[1].legs[0].distance.value").ToString());
                        val_2.Distance = System.Math.Round(ConvertMetersToMiles(distanceMeters2), 2);
                        val_2.Time =
                            System.Math.Round(
                                decimal.Parse(o.SelectToken("routes[1].legs[0].duration.value").ToString()) / 60, 2);
                        list.Add(val_2);
                    }

                    var val_3 = new GoogleApiDistanceVal();
                    if (o.SelectToken("routes[2].legs[0].distance.value") != null)
                    {
                        decimal distanceMeters3 =
                            decimal.Parse(o.SelectToken("routes[2].legs[0].distance.value").ToString());
                        val_3.Distance = System.Math.Round(ConvertMetersToMiles(distanceMeters3), 2);
                        val_3.Time =
                            System.Math.Round(
                                decimal.Parse(o.SelectToken("routes[2].legs[0].duration.value").ToString()) / 60, 2);
                        list.Add(val_3);
                    }
                    return list.OrderBy(x => x.Distance).ToList();


                }
                else
                {
                    ErrorHandling.LogFileWrite(status, apikey, status, url);

                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, apikey, url);

            }
            return shortlist;
        }

        public static List<GoogleApiDistanceVal> GetDistance(string fromCoordinates, string toCoordinates, string fromAddress, string toAddress)
        {
            var list = new List<GoogleApiDistanceVal>();
            //    var shortlist = new List<GoogleApiDistanceVal>();
            string status = string.Empty;
            JObject o = new JObject();
            string requesturl = string.Empty;
            string apikey = string.Empty;

            try
            {
                string distanceUrl = ConfigurationManager.AppSettings["DistanceURL"];

                for (int i = 0; i < 4; i++)
                {
                    apikey = ConfigHelper.GetDistanceApiKey();
                    distanceUrl = distanceUrl.Replace("FROM-COO", fromCoordinates);
                    distanceUrl = distanceUrl.Replace("TO-COO", toCoordinates);
                    distanceUrl = distanceUrl.Replace("API-KEY", apikey);
                    // url = distanceUrl;
                    //url =
                    //    "https://maps.googleapis.com/maps/api/directions/json?origin=" + fromCoordinates + "&destination=" + toCoordinates + "" +
                    //    "&units=imperial&alternatives=true&key=" + apikey;

                    requesturl = distanceUrl;
                    string content = FileGetContents(requesturl);
                    o = JObject.Parse(content);

                    status = o.SelectToken("status").ToString();
                    if (status.Equals("OK"))
                    {
                        using (var db = new BATransferEntities())
                        {

                            var objKey = db.DistanceKeyTables.FirstOrDefault(x => x.KeyName.Equals(apikey));
                            if (objKey != null)
                            {
                                objKey.NumberOfRequised = objKey.NumberOfRequised + 1;
                                db.SaveChanges();
                            }


                        }

                        break;
                    }
                    else if (status.Equals("NOT_FOUND"))
                    {
                        string address = "From add -> " + fromAddress + "\n To add -> " + toAddress;
                        ErrorHandling.LogFileWriteErrorCoodinates(status, address, status, requesturl);
                        break;
                    }
                    else if (status.Equals("INVALID_REQUEST"))
                    {
                        string address = "From add -> " + fromAddress + "\n To add -> " + toAddress;
                        ErrorHandling.LogFileWriteErrorCoodinates(status, address, status, requesturl);
                        break;
                    }
                    else if (status.Equals("ZERO_RESULTS"))
                    {
                        string address = "From add -> " + fromAddress + "\n To add -> " + toAddress;
                        ErrorHandling.LogFileWriteErrorCoodinates(status, address, status, requesturl);
                        break;
                    }
                    else if (status.Equals("OVER_QUERY_LIMIT"))
                    {
                        string address = "From add -> " + fromAddress + "\n To add -> " + toAddress;
                        ErrorHandling.LogFileWriteErrorCoodinates(status, address, status, requesturl);
                    }
                }
                if (status.Equals("NOT_FOUND"))
                {
                    var val1 = new GoogleApiDistanceVal();
                    val1.Distance = 0;
                    val1.Time = 0;
                    list.Add(val1);
                    return list;
                }
                if (status.Equals("INVALID_REQUEST"))
                {
                    var val1 = new GoogleApiDistanceVal();
                    val1.Distance = 0;
                    val1.Time = 0;
                    list.Add(val1);
                    return list;
                }
                if (status.Equals("ZERO_RESULTS"))
                {
                    var val1 = new GoogleApiDistanceVal();
                    val1.Distance = 0;
                    val1.Time = 0;
                    list.Add(val1);
                    return list;
                }

                if (status.Equals("OK"))
                {


                    var val1 = new GoogleApiDistanceVal();
                    if (o.SelectToken("routes[0].legs[0].distance.value") != null)
                    {
                        decimal distanceMeters =
                            decimal.Parse(o.SelectToken("routes[0].legs[0].distance.value").ToString());
                        val1.Distance = System.Math.Round(ConvertMetersToMiles(distanceMeters), 2);
                        val1.Time =
                            System.Math.Round(
                                decimal.Parse(o.SelectToken("routes[0].legs[0].duration.value").ToString()) / 60, 2);
                        list.Add(val1);
                    }

                    var val_2 = new GoogleApiDistanceVal();
                    if (o.SelectToken("routes[1].legs[0].distance.value") != null)
                    {
                        decimal distanceMeters2 =
                            decimal.Parse(o.SelectToken("routes[1].legs[0].distance.value").ToString());
                        val_2.Distance = System.Math.Round(ConvertMetersToMiles(distanceMeters2), 2);
                        val_2.Time =
                            System.Math.Round(
                                decimal.Parse(o.SelectToken("routes[1].legs[0].duration.value").ToString()) / 60, 2);
                        list.Add(val_2);
                    }

                    var val_3 = new GoogleApiDistanceVal();
                    if (o.SelectToken("routes[2].legs[0].distance.value") != null)
                    {
                        decimal distanceMeters3 =
                            decimal.Parse(o.SelectToken("routes[2].legs[0].distance.value").ToString());
                        val_3.Distance = System.Math.Round(ConvertMetersToMiles(distanceMeters3), 2);
                        val_3.Time =
                            System.Math.Round(
                                decimal.Parse(o.SelectToken("routes[2].legs[0].duration.value").ToString()) / 60, 2);
                        list.Add(val_3);
                    }
                    return list.OrderBy(x => x.Distance).ToList();


                }
                else
                {
                    ErrorHandling.LogFileWrite(status, apikey, status, requesturl);

                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, apikey, requesturl);

            }
            return null;
        }

        public static List<GoogleApiDistanceVal> GetDistanceWithWayPoints(string fromAddress, string toAddress, List<string> wayPoits)
        {
            var list = new List<GoogleApiDistanceVal>();
            //    var shortlist = new List<GoogleApiDistanceVal>();
            string status = string.Empty;
            JObject o = new JObject();
            string url = string.Empty;

            string apikey = string.Empty;
            string viaList = string.Join("|", wayPoits);

            try
            {


                for (int i = 0; i < 4; i++)
                {
                    apikey = ConfigHelper.GetDistanceApiKey();
                    url =
                        "https://maps.googleapis.com/maps/api/directions/json?origin=" + fromAddress + "&destination=" + toAddress + "" +
                        "&waypoints=optimize:true" + viaList + "&units=imperial&alternatives=true&key=" + apikey;
                    string requesturl = url;
                    string content = FileGetContents(requesturl);
                    o = JObject.Parse(content);

                    status = o.SelectToken("status").ToString();
                    if (status.Equals("OK"))
                    {
                        using (var db = new BATransferEntities())
                        {

                            var objKey = db.DistanceKeyTables.FirstOrDefault(x => x.KeyName.Equals(apikey));
                            if (objKey != null)
                            {
                                objKey.NumberOfRequised = objKey.NumberOfRequised + 1;
                                db.SaveChanges();
                            }


                        }

                        break;
                    }
                    else if (status.Equals("NOT_FOUND"))
                    {
                        string address = "From add -> " + fromAddress + "\n To add -> " + toAddress;
                        ErrorHandling.LogFileWriteErrorCoodinates(status, address, status, requesturl);
                        break;
                    }
                    else if (status.Equals("INVALID_REQUEST"))
                    {
                        string address = "From add -> " + fromAddress + "\n To add -> " + toAddress;
                        ErrorHandling.LogFileWriteErrorCoodinates(status, address, status, requesturl);
                        break;
                    }
                    else if (status.Equals("ZERO_RESULTS"))
                    {
                        string address = "From add -> " + fromAddress + "\n To add -> " + toAddress;
                        ErrorHandling.LogFileWriteErrorCoodinates(status, address, status, requesturl);
                        break;
                    }
                    else if (!status.Equals("OVER_QUERY_LIMIT"))
                    {
                        string address = "From add -> " + fromAddress + "\n To add -> " + toAddress;
                        ErrorHandling.LogFileWriteErrorCoodinates(status, address, status, requesturl);
                    }
                }
                if (status.Equals("NOT_FOUND"))
                {
                    var val1 = new GoogleApiDistanceVal();
                    val1.Distance = 0;
                    val1.Time = 0;
                    list.Add(val1);
                    return list;
                }
                if (status.Equals("INVALID_REQUEST"))
                {
                    var val1 = new GoogleApiDistanceVal();
                    val1.Distance = 0;
                    val1.Time = 0;
                    list.Add(val1);
                    return list;
                }
                if (status.Equals("ZERO_RESULTS"))
                {
                    var val1 = new GoogleApiDistanceVal();
                    val1.Distance = 0;
                    val1.Time = 0;
                    list.Add(val1);
                    return list;
                }

                if (status.Equals("OK"))
                {


                    var val1 = new GoogleApiDistanceVal();
                    if (o.SelectToken("routes[0].legs[0].distance.value") != null)
                    {
                        decimal distanceMeters = 0;
                        decimal duration = 0;
                        if (wayPoits != null)
                        {
                            int lenth = wayPoits.Count() + 1;
                            for (int i = 0; i < lenth; i++)
                            {
                                distanceMeters = distanceMeters + decimal.Parse(o.SelectToken("routes[0].legs[" + i + "].distance.value").ToString());
                                duration = duration + decimal.Parse(o.SelectToken("routes[0].legs[" + i + "].duration.value").ToString());
                            }
                        }



                        val1.Distance = System.Math.Round(ConvertMetersToMiles(distanceMeters), 2);
                        val1.Time = System.Math.Round(duration / 60, 2);
                        list.Add(val1);
                    }

                    var val_2 = new GoogleApiDistanceVal();
                    if (o.SelectToken("routes[1].legs[0].distance.value") != null)
                    {
                        decimal distanceMeters2 =
                            decimal.Parse(o.SelectToken("routes[1].legs[0].distance.value").ToString());
                        val_2.Distance = System.Math.Round(ConvertMetersToMiles(distanceMeters2), 2);
                        val_2.Time =
                            System.Math.Round(
                                decimal.Parse(o.SelectToken("routes[1].legs[0].duration.value").ToString()) / 60, 2);
                        list.Add(val_2);
                    }

                    var val_3 = new GoogleApiDistanceVal();
                    if (o.SelectToken("routes[2].legs[0].distance.value") != null)
                    {
                        decimal distanceMeters3 =
                            decimal.Parse(o.SelectToken("routes[2].legs[0].distance.value").ToString());
                        val_3.Distance = System.Math.Round(ConvertMetersToMiles(distanceMeters3), 2);
                        val_3.Time =
                            System.Math.Round(
                                decimal.Parse(o.SelectToken("routes[2].legs[0].duration.value").ToString()) / 60, 2);
                        list.Add(val_3);
                    }
                    return list.OrderBy(x => x.Distance).ToList();


                }
                else
                {
                    ErrorHandling.LogFileWrite(status, apikey, status, url);

                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, apikey, url);

            }
            return null;
        }
    }

    public class GoogleApiDistanceVal
    {
        public decimal Distance { get; set; }
        public decimal Time { get; set; }
        //public decimal MemiumDistance { get; set; }
        //public decimal MemiumTime { get; set; }
        //public decimal LingDistance { get; set; }
        //public decimal LongTime { get; set; }
    }
}