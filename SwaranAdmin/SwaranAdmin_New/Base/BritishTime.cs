﻿using System;

namespace SwaranAdmin_New.Base
{
    public class BritishTime
    {
        public static DateTime GetDateTime()
        {
            var britishZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
            var today = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, britishZone);
            return today;
        }
    }
}