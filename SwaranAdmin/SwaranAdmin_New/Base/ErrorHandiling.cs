﻿using System;
using System.IO;
using System.Web;

namespace SwaranAdmin_New.Base
{
    public class ErrorHandling
    {


        /// <summary>
        /// This method is for writting the Log file with message parameter
        /// </summary>
        /// <param name="message"></param>
        public static void LogFileWrite(string message, string stackTrace, string methodname, string innerException)
        {
            FileStream fileStream = null;
            StreamWriter streamWriter = null;
            try
            {
                //  string logFilePath = Environment.CurrentDirectory + "\\ExceptionLog\\";
                string logFilePath = HttpContext.Current.Server.MapPath(string.Format("~/A_ExceptionLog/"));
                if (!Directory.Exists(logFilePath))
                {
                    Directory.CreateDirectory(logFilePath);
                }

                logFilePath = logFilePath + "LogOn" + DateTime.Today.ToString(" yyyy MM dd - ") + DateTime.Now.DayOfWeek +
                              "." + "txt";

                if (logFilePath.Equals("")) return;

                #region Create the Log file directory if it does not exists

                DirectoryInfo logDirInfo = null;
                FileInfo logFileInfo = new FileInfo(logFilePath);
                logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
                if (!logDirInfo.Exists) logDirInfo.Create();

                #endregion Create the Log file directory if it does not exists

                if (!logFileInfo.Exists)
                {
                    fileStream = logFileInfo.Create();
                }
                else
                {
                    fileStream = new FileStream(logFilePath, FileMode.Append);
                }
                streamWriter = new StreamWriter(fileStream);
                streamWriter.WriteLine("--------" + methodname + "-------------------" + methodname + "-----" + DateTime.Now + "---------------------------" + methodname + "------------------" + methodname + "-------------\n");

                streamWriter.WriteLine(message);

                streamWriter.WriteLine("----------------------------------------------INNER EXCEPTION--------------------------------------------------------------------------------------------------------------------------");
                streamWriter.WriteLine(innerException);
                streamWriter.WriteLine("----------------------------------------------STACK-TRACE------------------------------------------------------------------------------------------------------------------------------");
                streamWriter.WriteLine(stackTrace);
                streamWriter.WriteLine("\n\n");

            }
            catch (Exception)
            {

            }

            finally
            {
                if (streamWriter != null) streamWriter.Close();
                if (fileStream != null) fileStream.Close();
            }

        }

        public static void LogFileWriteErrorCoodinates(string message, string stackTrace, string methodname, string innerException)
        {
            FileStream fileStream = null;
            StreamWriter streamWriter = null;
            try
            {
                //  string logFilePath = Environment.CurrentDirectory + "\\ExceptionLog\\";
                string logFilePath = HttpContext.Current.Server.MapPath(string.Format("~/A_ExceptionLog/A_ErrorCodinates/"));
                if (!Directory.Exists(logFilePath))
                {
                    Directory.CreateDirectory(logFilePath);
                }

                logFilePath = logFilePath + "LogOn" + DateTime.Today.ToString(" yyyy MM dd - ") + DateTime.Now.DayOfWeek +
                              "." + "txt";

                if (logFilePath.Equals("")) return;

                #region Create the Log file directory if it does not exists

                DirectoryInfo logDirInfo = null;
                FileInfo logFileInfo = new FileInfo(logFilePath);
                logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
                if (!logDirInfo.Exists) logDirInfo.Create();

                #endregion Create the Log file directory if it does not exists

                if (!logFileInfo.Exists)
                {
                    fileStream = logFileInfo.Create();
                }
                else
                {
                    fileStream = new FileStream(logFilePath, FileMode.Append);
                }
                streamWriter = new StreamWriter(fileStream);
                streamWriter.WriteLine("--------" + methodname + "-------------------" + methodname + "-----" + DateTime.Now + "---------------------------" + methodname + "------------------" + methodname + "-------------\n");

                streamWriter.WriteLine(message);

                streamWriter.WriteLine("----------------------------------------------INNER EXCEPTION--------------------------------------------------------------------------------------------------------------------------");
                streamWriter.WriteLine(innerException);
                streamWriter.WriteLine("----------------------------------------------STACK-TRACE------------------------------------------------------------------------------------------------------------------------------");
                streamWriter.WriteLine(stackTrace);
                streamWriter.WriteLine("\n\n");

            }
            catch (Exception)
            {

            }

            finally
            {
                if (streamWriter != null) streamWriter.Close();
                if (fileStream != null) fileStream.Close();
            }

        }

        public static void LogFileWriteWithOblect(string messageXml, string message, string stackTrace, string methodname, string innerException)
        {
            FileStream fileStream = null;
            StreamWriter streamWriter = null;
            try
            {
                //  string logFilePath = Environment.CurrentDirectory + "\\ExceptionLog\\";
                string logFilePath = HttpContext.Current.Server.MapPath(string.Format("~/A_ExceptionLog/"));
                if (!Directory.Exists(logFilePath))
                {
                    Directory.CreateDirectory(logFilePath);
                }

                logFilePath = logFilePath + "LogOn" + DateTime.Today.ToString(" yyyy MM dd - ") + DateTime.Now.DayOfWeek +
                              "." + "txt";

                if (logFilePath.Equals("")) return;

                #region Create the Log file directory if it does not exists

                DirectoryInfo logDirInfo = null;
                FileInfo logFileInfo = new FileInfo(logFilePath);
                logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
                if (!logDirInfo.Exists) logDirInfo.Create();

                #endregion Create the Log file directory if it does not exists

                if (!logFileInfo.Exists)
                {
                    fileStream = logFileInfo.Create();
                }
                else
                {
                    fileStream = new FileStream(logFilePath, FileMode.Append);
                }
                streamWriter = new StreamWriter(fileStream);
                streamWriter.WriteLine("--------" + methodname + "-------------------" + methodname + "-----" + DateTime.Now + "---------------------------" + methodname + "------------------" + methodname + "-------------\n");
                streamWriter.WriteLine("----------------XML------------------XML------------XML---- ---------------XML---------------XML-------------------------XML----------------------");
                streamWriter.WriteLine(messageXml);
                streamWriter.WriteLine("----------------------------------------------ERROR  --------------------------------------------------------------------------------------------------------------------------");
                streamWriter.WriteLine(message);
                streamWriter.WriteLine("----------------------------------------------INNER EXCEPTION--------------------------------------------------------------------------------------------------------------------------");
                streamWriter.WriteLine(innerException);
                streamWriter.WriteLine("----------------------------------------------STACK-TRACE------------------------------------------------------------------------------------------------------------------------------");
                streamWriter.WriteLine(stackTrace);
                streamWriter.WriteLine("\n\n");

            }
            catch (Exception)
            {

            }

            finally
            {
                if (streamWriter != null) streamWriter.Close();
                if (fileStream != null) fileStream.Close();
            }

        }

        public static void LogFileWriteWorldPay(string message, string stackTrace, string methodname, string innerException)
        {
            FileStream fileStream = null;
            StreamWriter streamWriter = null;
            try
            {
                //  string logFilePath = Environment.CurrentDirectory + "\\ExceptionLog\\";
                string logFilePath = HttpContext.Current.Server.MapPath(string.Format("~/A_ExceptionLog/WorldPay/"));
                if (!Directory.Exists(logFilePath))
                {
                    Directory.CreateDirectory(logFilePath);
                }

                logFilePath = logFilePath + "LogOn" + DateTime.Today.ToString(" yyyy MM dd - ") + DateTime.Now.DayOfWeek +
                              "." + "txt";

                if (logFilePath.Equals("")) return;

                #region Create the Log file directory if it does not exists

                DirectoryInfo logDirInfo = null;
                FileInfo logFileInfo = new FileInfo(logFilePath);
                logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
                if (!logDirInfo.Exists) logDirInfo.Create();

                #endregion Create the Log file directory if it does not exists

                if (!logFileInfo.Exists)
                {
                    fileStream = logFileInfo.Create();
                }
                else
                {
                    fileStream = new FileStream(logFilePath, FileMode.Append);
                }
                streamWriter = new StreamWriter(fileStream);
                streamWriter.WriteLine("----------------" + methodname + "------------------------------ <-------NEXT CARD PAYMENT --->" + DateTime.Now + "-------------------------------------------------------------------");
                streamWriter.WriteLine(message);

                streamWriter.WriteLine("--------------------------------------------------------CARD PAYMENT------------------------------------------------------------------------------------------------------------------");
                streamWriter.WriteLine(innerException);
                streamWriter.WriteLine("---------------------------------------------------------WORLD PAY RESPONSE-----------------------------------------------------------------------------------------------------------");
                streamWriter.WriteLine(stackTrace);
                streamWriter.WriteLine("\n\n");

            }
            catch (Exception)
            {

            }

            finally
            {
                if (streamWriter != null) streamWriter.Close();
                if (fileStream != null) fileStream.Close();
            }

        }



        

    }
}