﻿using SwaranAdmin_New.DataManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwaranAdmin_New.Base
{
    public static class UserHelper
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static bool IsStaffFirstLogin(string userName)
        {
            bool isFirstLogin = false;

            using (var db = new BATransferEntities())
            {
                //return db.ApiUrls.First(x => x.ApiUrlId == id).ApiUrlValue;
                var relevantAspNetUser = db.AspNetUsers.FirstOrDefault(anu => anu.UserName == userName);

                if(relevantAspNetUser != null)
                {
                    if (relevantAspNetUser.StaffProfiles != null)
                    {
                        var relevantStaff = relevantAspNetUser.StaffProfiles.FirstOrDefault();

                        if(relevantStaff != null)
                        {
                            //isFirstLogin = relevantStaff.IsOldUser
                            if(!relevantStaff.IsOldUser)
                            {
                                isFirstLogin = true;
                            }

                        }
                    };
                }
            }

            return isFirstLogin;
        }

        public static bool IsStaffAccountEnabled(string userName)
        {
            bool isStaffEnabled = false;

            using (var db = new BATransferEntities())
            {
                //return db.ApiUrls.First(x => x.ApiUrlId == id).ApiUrlValue;
                var relevantAspNetUser = db.AspNetUsers.FirstOrDefault(anu => anu.UserName == userName);

                if (relevantAspNetUser != null)
                {
                    if (relevantAspNetUser.StaffProfiles != null)
                    {
                        var relevantStaff = relevantAspNetUser.StaffProfiles.FirstOrDefault();

                        if (relevantStaff != null)
                        {
                            //isFirstLogin = relevantStaff.IsOldUser
                            if (relevantStaff.IsEnabled)
                            {
                                isStaffEnabled = true;
                            }

                        }
                    };
                }
            }

            return isStaffEnabled;
        }
    }
}