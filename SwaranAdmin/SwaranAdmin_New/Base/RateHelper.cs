﻿using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models.Pricing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwaranAdmin_New.Base
{
    public static class RateHelper
    {
        public static string GetRateName(long rateId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var relevantCustomerRate = db.Rate_Customer.FirstOrDefault(rc => rc.RateId == rateId);

                    if (relevantCustomerRate != null)
                    {
                        return relevantCustomerRate.RateName;
                    }
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return string.Empty;
        }

        public static string GetVehicleTypeNameByRateCustomerVehicleTypeId(long rateCustomerVehicleTypeId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var relevantCustomerRate = db.Rate_Customer_VehicleType.FirstOrDefault(rc => rc.RateCustomerVehicleTypeId == rateCustomerVehicleTypeId);

                    if (relevantCustomerRate != null)
                    {
                        if (relevantCustomerRate.VehicleType != null)
                        {
                            return relevantCustomerRate.VehicleType.VehicleTypeName;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return string.Empty;
        }

        public static CustomerRateDom GetRateByRateByRateCustomerDistanceDayId(long rateCustomerDistanceDayId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var relevantCustomerRateDistanceDay = db.RateCustomer_Distance_Day.FirstOrDefault(rcv => rcv.DayId == rateCustomerDistanceDayId && !rcv.IsDeleted);

                    if (relevantCustomerRateDistanceDay != null)
                    {
                        if (relevantCustomerRateDistanceDay.Rate_Customer_VehicleType != null)
                        {
                            var rateCustomerVehicleType = relevantCustomerRateDistanceDay.Rate_Customer_VehicleType;

                            if (relevantCustomerRateDistanceDay.Rate_Customer_VehicleType.Rate_Customer != null)
                            {
                                var rateCustomer = relevantCustomerRateDistanceDay.Rate_Customer_VehicleType.Rate_Customer;

                                string dayName = string.Empty;
                                
                                switch (relevantCustomerRateDistanceDay.Day)
                                {
                                    case 0:
                                        dayName = "Default";
                                        break;
                                    case 1:
                                        dayName = "Monday";
                                        break;
                                    case 2:
                                        dayName = "Tuesday";
                                        break;
                                    case 3:
                                        dayName = "Wednesday";
                                        break;
                                    case 4:
                                        dayName = "Thursday";
                                        break;
                                    case 5:
                                        dayName = "Friday";
                                        break;
                                    case 6:
                                        dayName = "Saturday";
                                        break;
                                    case 7:
                                        dayName = "Sunday";
                                        break;
                                }

                                return new CustomerRateDom()
                                {
                                    RateId = rateCustomer.RateId,
                                    RateName = rateCustomer.RateName,
                                    CustomerRateVehicleTypeId = rateCustomerVehicleType.RateCustomerVehicleTypeId,
                                    VehicleTypeId = rateCustomerVehicleType.VehicletypeId,
                                    VehicleTypeName = rateCustomerVehicleType.VehicleType != null ? rateCustomerVehicleType.VehicleType.VehicleTypeName : string.Empty,
                                    DayName = dayName

                                };
                            }
                        }
                    }
                }                
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return null;
        }

    }
}