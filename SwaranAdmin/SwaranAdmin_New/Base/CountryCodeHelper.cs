﻿ 
using System.Linq;
using SwaranAdmin_New.DataManager;

namespace SwaranAdmin_New.Base
{
    public class CountryCodeHelper
    {
        public static string GetCountryCode(string countryCode)
        {
            using (var db = new BATransferEntities())
            {

                if (!string.IsNullOrEmpty(countryCode) && db.Countries.Any(x => x.Iso.Equals(countryCode)))
                {

                    return db.Countries.First(x => x.Iso.Equals(countryCode)).Phonecode.ToString();
                }

            }
            return "";
        }
        public static long GetCountryId(string countryCode)
        {
            long coiuntryId = 0;
            using (var db = new BATransferEntities())
            {

                if (!string.IsNullOrEmpty(countryCode) && db.Countries.Any(x => x.Iso.Equals(countryCode)))
                {

                    coiuntryId = db.Countries.First(x => x.Iso.Equals(countryCode)).ID;
                }

            }
            return coiuntryId;
        }
    }
}