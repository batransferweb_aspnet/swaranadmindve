﻿
using Newtonsoft.Json;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SwaranAdmin_New.Base
{
    public class EmailHelper
    {
        #region SEND EMAIL  

        public static async Task<bool> SendEmailWithoutAttachtment(RootEmail email, string apiUrl)
        {
            try
            {



                HttpClient client = new HttpClient()
                {
                    BaseAddress = new Uri(apiUrl)
                };


                var emailJson = JSonHelper.ConvertObjectToJSon(email);
                HttpContent contentPost = new StringContent(emailJson, Encoding.UTF8, "application/json");
                var response = await client.PostAsync(apiUrl, contentPost);
                response.EnsureSuccessStatusCode();
                var result = await response.Content.ReadAsStringAsync();
                var main = JSonHelper.ConvertJSonToObject<MainEmailOut>(result);
                return main.Status;
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }
        public static async Task<bool> SendEmail(RootEmail emailDomain, string apiUrl, EmailAttachmentsViewMOdel email)
        {

            try
            {

                var postData = new
                {
                    FromEmailAddress = emailDomain.FromEmailAddress,
                    ToEmailAddresses = emailDomain.ToEmailAddresses,
                    Cc = new string[] { },
                    Bcc = new string[] { },
                    Subject = emailDomain.Subject,
                    Body = emailDomain.Body,
                    Smtp = emailDomain.Smtp,
                    Port = emailDomain.Port,
                    Username = emailDomain.Username,
                    Password = emailDomain.Password,
                    IsEnableSsl = emailDomain.IsEnableSsl,
                    DisplayName = emailDomain.DisplayName
                };

                HttpClient client = new HttpClient();

                var content = new MultipartFormDataContent();


                content.Headers.Add("Settings", JsonConvert.SerializeObject(postData));
                if (email.Attachments != null)
                {
                    byte[] bytes = email.Attachments.ToArray();
                    var fileContent = new ByteArrayContent(bytes);

                    fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = email.FileNames

                    };

                    content.Add(fileContent);
                }
                var response = await client.PostAsync(apiUrl, content).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }


            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }

        public static async Task<bool> SendEmail(RootEmail emailDomain, string apiUrl)
        {

            try
            {

                var postData = new
                {
                    FromEmailAddress = emailDomain.FromEmailAddress,
                    ToEmailAddresses = emailDomain.ToEmailAddresses,
                    Cc = new string[] { },
                    Bcc = new string[] { },
                    Subject = emailDomain.Subject,
                    Body = emailDomain.Body,
                    Smtp = emailDomain.Smtp,
                    Port = emailDomain.Port,
                    Username = emailDomain.Username,
                    Password = emailDomain.Password,
                    IsEnableSsl = emailDomain.IsEnableSsl,
                    DisplayName = emailDomain.DisplayName
                };

                HttpClient client = new HttpClient();

                var content = new MultipartFormDataContent();


                content.Headers.Add("Settings", JsonConvert.SerializeObject(postData));


                var response = await client.PostAsync(apiUrl, content).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    return true;
                }


            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }
        #endregion

        #region GET EMAIL CREDENTIALS
        public static EmailCredentialsViewModel GetBookingNotificationEmailCredentials(int emailDomainId)
        {
            var email = new EmailCredentialsViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {


                    if (
                        db.EmailDomains.Any(
                            x =>
                                x.EmailDomainId == emailDomainId && x.IsDeleted == false &&
                                x.IsEnabled == true))
                    {
                        var objectEmail =
                            db.EmailDomains.First(
                                x =>
                                    x.EmailDomainId == emailDomainId &&
                                    x.IsDeleted == false && x.IsEnabled == true);
                        email.FromEmail = objectEmail.FromEmail;
                        email.Password = objectEmail.Password;
                        email.Port = objectEmail.Port;
                        email.Smtp = objectEmail.Smtp;
                        email.IsEnabledSSL = objectEmail.IsEnabledSSL;
                        email.Username = objectEmail.Username;
                        email.DisplayName = objectEmail.DisplayName;

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return email;
        }

        public static EmailCredentialsViewModel GetPasswordResetEmailCredentials()
        {
            var email = new EmailCredentialsViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {

                    if (
                        db.EmailDomains.Any(
                            x =>
                                x.EmailDomainId == (int)EmailDomainEnum.PasswordResetEmail && x.IsDeleted == false &&
                                x.IsEnabled == true))
                    {
                        var objectEmail =
                            db.EmailDomains.First(
                                x =>
                                    x.EmailDomainId == (int)EmailDomainEnum.PasswordResetEmail && x.IsDeleted == false &&
                                    x.IsEnabled == true);
                        email.FromEmail = objectEmail.FromEmail;
                        email.Password = objectEmail.Password;
                        email.Port = objectEmail.Port;
                        email.Smtp = objectEmail.Smtp;
                        email.IsEnabledSSL = objectEmail.IsEnabledSSL;
                        email.Username = objectEmail.Username;
                        email.DisplayName = objectEmail.DisplayName;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return email;
        }

        public static EmailCredentialsViewModel GetCustomerEmaillCredentials()
        {
            var email = new EmailCredentialsViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {

                    if (
                        db.EmailDomains.Any(
                            x =>
                                x.EmailDomainId == (int)EmailDomainEnum.CustomerEmail && x.IsDeleted == false &&
                                x.IsEnabled == true))
                    {
                        var objectEmail =
                            db.EmailDomains.First(
                                x =>
                                    x.EmailDomainId == (int)EmailDomainEnum.CustomerEmail && x.IsDeleted == false &&
                                    x.IsEnabled == true);
                        email.FromEmail = objectEmail.FromEmail;
                        email.Password = objectEmail.Password;
                        email.Port = objectEmail.Port;
                        email.Smtp = objectEmail.Smtp;
                        email.IsEnabledSSL = objectEmail.IsEnabledSSL;
                        email.Username = objectEmail.Username;
                        email.DisplayName = objectEmail.DisplayName;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return email;
        }

        #endregion

        #region GET EMAIL API URL
        public static string GetEmailUrl(int apiUrlId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    if (db.ApiUrls.Any(x => x.ApiUrlId == apiUrlId))
                    {

                        return db.ApiUrls.First(x => x.ApiUrlId == apiUrlId).ApiUrlValue;
                    }
                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return "";
        }
        #endregion

    }



    public class MainEmailOut
    {
        public bool Status { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public bool Data { get; set; }
    }


    public class RootEmail
    {
        public string FromEmailAddress { get; set; }
        public List<string> ToEmailAddresses { get; set; }
        public string[] Cc { get; set; }
        public string[] Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Smtp { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public object Attachments { get; set; }
        public bool IsEnableSsl { get; set; }
        public string DisplayName { get; set; }

    }

    public class EmailAttachmentsViewMOdel
    {
        public string FileNames { get; set; }
        public byte[] Attachments { get; set; }
    }
}