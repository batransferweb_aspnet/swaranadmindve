﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SwaranAdmin_New.Base
{
    internal class SessionAuthorizeAttribute : AuthorizeAttribute
    {

        //public override void OnAuthorization(AuthorizationContext filterContext)
        //{
        //    base.OnAuthorization(filterContext);

        //    if (filterContext.Result is HttpUnauthorizedResult)
        //    {
        //        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
        //        {
        //            { "controller", filterContext.RouteData.Values[ "controller" ] },
        //            { "action", "Login" },
        //            { "ReturnUrl", filterContext.HttpContext.Request.RawUrl }
        //        });
        //    }
        //}

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException("httpContext");
            }
            IPrincipal user = httpContext.User;
            if (!user.Identity.IsAuthenticated)
            {
                return false;
            }
            else
            {
                return true;
            }
            ////return httpContext.Session["InsuredKey"] != null;

            //var authroized = base.AuthorizeCore(httpContext);
            //if (!authroized)
            //{
            //    // the user is not authenticated or the forms authentication
            //    // cookie has expired
            //    return false;
            //}

            //// Now check the session:
            //var myvar = httpContext.Session["User"];
            //if (myvar == null)
            //{
            //    // the session has expired
            //    return false;
            //}

            //return true;

        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                                   new RouteValueDictionary
                                   {
                                       { "action", "Login" },
                                       { "controller", "Account" }
                                   });
        }
    }
}