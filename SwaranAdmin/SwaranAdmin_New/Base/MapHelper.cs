﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

 namespace SwaranAdmin_New.Base
{
    public static class MapHelper
    {
        public static string SendMap(string serialisedData, string url)
        {


            var request = (HttpWebRequest)WebRequest.Create(url);

            var data = Encoding.ASCII.GetBytes(serialisedData);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();
            JObject json = JObject.Parse(responseString);

            return json["Data"].ToString();
            ;

        }
    }



    public class RootSaveMap
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public int StatusCode { get; set; }
        public string Data { get; set; }
    }


    public class RootMap
    {
        public string BookingNumber { get; set; }
        public Pickuppoint PickupPoint { get; set; }
        public Dropoffpoint DropoffPoint { get; set; }
        public List<Viapoint> ViaPoints { get; set; }
        public string Route { get; set; }
        public string Size { get; set; }
    }

    public class Pickuppoint
    {
        public string Lat { get; set; }
        public string Long { get; set; }
    }

    public class Dropoffpoint
    {
        public string Lat { get; set; }
        public string Long { get; set; }
    }

    public class Viapoint
    {
        public string Lat { get; set; }
        public string Long { get; set; }
    }

}