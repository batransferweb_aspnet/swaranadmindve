﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using SwaranAdmin_New.Models.BookingMo;

namespace SwaranAdmin_New.Base
{
    public class FixedPriceHelper
    {
        public static FixPriceIdViewModel GetFixpriceId(TelPriceViewModel address)
        {
            var fixedPrice = new FixPriceIdViewModel();
            try
            {
                if (address.FromFixedPriceAriaPointId > 0 && address.ToFixedPriceAriaPointId > 0)
                {
                    fixedPrice.FromFixedPriceAriaPointId = address.FromFixedPriceAriaPointId;
                    fixedPrice.ToFixedPriceAriaPointId = address.ToFixedPriceAriaPointId;
                    return fixedPrice;
                }

                using (var db = new BATransferEntities())
                {

                    if (address.FromFixedPriceAriaPointId == 0 && (address.FromAriapointTypeId == (int)AreapointTypeEnum.Place || address.FromAriapointTypeId == (int)AreapointTypeEnum.Station))
                    {
                        string postcode = address.FromPostCode != null && address.FromPostCode.Split(' ')[0].Length > 0
                            ? address.FromPostCode.Split(' ')[0].Trim()
                            : string.Empty;
                        int numericValue;
                        if (!int.TryParse(postcode.Substring(postcode.Length - 1), out numericValue))
                        {
                            postcode = postcode.Remove(postcode.Length - 1, 1).Trim();
                        }

                        var objPOI = db.PointOfInterest_Place.FirstOrDefault(x => x.OuterPostcode.Equals(postcode) && x.IsDefault);

                        if (objPOI != null)
                        {
                            fixedPrice.FromFixedPriceAriaPointId = objPOI.AriaPointId;
                        }
                    }
                    else
                    {
                        fixedPrice.FromFixedPriceAriaPointId = address.FromFixedPriceAriaPointId;
                    }

                    if (address.ToFixedPriceAriaPointId == 0 && (address.ToAriapointTypeId == (int)AreapointTypeEnum.Place || address.ToAriapointTypeId == (int)AreapointTypeEnum.Station))
                    {
                        string postcode = address.ToPostCode != null && address.ToPostCode.Split(' ')[0].Length > 0
                            ? address.ToPostCode.Split(' ')[0].Trim()
                            : string.Empty;
                        int numericValue;
                        if (!int.TryParse(postcode.Substring(postcode.Length - 1), out numericValue))
                        {
                            postcode = postcode.Remove(postcode.Length - 1, 1).Trim();
                        }

                        var objPOI = db.PointOfInterest_Place.FirstOrDefault(x => x.OuterPostcode.Equals(postcode) && x.IsDefault);

                        if (objPOI != null)
                        {
                            fixedPrice.ToFixedPriceAriaPointId = objPOI.AriaPointId;
                        }
                    }
                    else
                    {
                        fixedPrice.ToFixedPriceAriaPointId = address.ToFixedPriceAriaPointId;
                    }


                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return fixedPrice;
        }

        public static FixPriceIdViewModel GetReturnFixpriceId(ReturnJourney address)
        {
            var fixedPrice = new FixPriceIdViewModel();
            try
            {
                if (address.FromFixedPriceAriaPointId > 0 && address.ToFixedPriceAriaPointId > 0)
                {
                    fixedPrice.FromFixedPriceAriaPointId = address.FromFixedPriceAriaPointId;
                    fixedPrice.ToFixedPriceAriaPointId = address.ToFixedPriceAriaPointId;
                    return fixedPrice;
                }

                using (var db = new BATransferEntities())
                {

                    if (address.FromFixedPriceAriaPointId == 0 && (address.FromAriapointTypeId == (int)AreapointTypeEnum.Place || address.FromAriapointTypeId == (int)AreapointTypeEnum.Station))
                    {
                        string postcode = address.FromPostCode != null && address.FromPostCode.Split(' ')[0].Length > 0
                            ? address.FromPostCode.Split(' ')[0].Trim()
                            : string.Empty;
                        int numericValue;
                        if (!int.TryParse(postcode.Substring(postcode.Length - 1), out numericValue))
                        {
                            postcode = postcode.Remove(postcode.Length - 1, 1).Trim();
                        }

                        var objPOI = db.PointOfInterest_Place.FirstOrDefault(x => x.OuterPostcode.Equals(postcode) && x.IsDefault);

                        if (objPOI != null)
                        {
                            fixedPrice.FromFixedPriceAriaPointId = objPOI.AriaPointId;
                        }
                    }
                    else
                    {
                        fixedPrice.FromFixedPriceAriaPointId = address.FromFixedPriceAriaPointId;
                    }

                    if (address.ToFixedPriceAriaPointId == 0 && (address.ToAriapointTypeId == (int)AreapointTypeEnum.Place || address.ToAriapointTypeId == (int)AreapointTypeEnum.Station))
                    {
                        string postcode = address.ToPostCode != null && address.ToPostCode.Split(' ')[0].Length > 0
                            ? address.ToPostCode.Split(' ')[0].Trim()
                            : string.Empty;
                        int numericValue;
                        if (!int.TryParse(postcode.Substring(postcode.Length - 1), out numericValue))
                        {
                            postcode = postcode.Remove(postcode.Length - 1, 1).Trim();
                        }

                        var objPOI = db.PointOfInterest_Place.FirstOrDefault(x => x.OuterPostcode.Equals(postcode) && x.IsDefault);

                        if (objPOI != null)
                        {
                            fixedPrice.ToFixedPriceAriaPointId = objPOI.AriaPointId;
                        }
                    }
                    else
                    {
                        fixedPrice.ToFixedPriceAriaPointId = address.ToFixedPriceAriaPointId;
                    }


                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return fixedPrice;
        }


        public static long GetFromViapointFixedPriceId(int fromAriapointTypeId, string fromPostCode)
        {

            try
            {
                using (var db = new BATransferEntities())
                {

                    if ((fromAriapointTypeId == (int)AreapointTypeEnum.Place || fromAriapointTypeId == (int)AreapointTypeEnum.Station))
                    {
                        string postcode = fromPostCode != null && fromPostCode.Split(' ')[0].Length > 0
                            ? fromPostCode.Split(' ')[0].Trim()
                            : string.Empty;
                        int numericValue;
                        if (!int.TryParse(postcode.Substring(postcode.Length - 1), out numericValue))
                        {
                            postcode = postcode.Remove(postcode.Length - 1, 1).Trim();
                        }

                        var objPOI = db.PointOfInterest_Place.FirstOrDefault(x => x.OuterPostcode.Equals(postcode) && x.IsDefault);

                        if (objPOI != null)
                        {
                            return objPOI.AriaPointId;
                        }
                    }



                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return 0;
        }

        public static long GetToViapointFixedPriceId(int toAriapointTypeId, string toPostCode)
        {

            try
            {


                using (var db = new BATransferEntities())
                {

                    if ((toAriapointTypeId == (int)AreapointTypeEnum.Place || toAriapointTypeId == (int)AreapointTypeEnum.Station))
                    {
                        string postcode = toPostCode != null && toPostCode.Split(' ')[0].Length > 0
                            ? toPostCode.Split(' ')[0].Trim()
                            : string.Empty;
                        int numericValue;
                        if (!int.TryParse(postcode.Substring(postcode.Length - 1), out numericValue))
                        {
                            postcode = postcode.Remove(postcode.Length - 1, 1).Trim();
                        }

                        var objPOI = db.PointOfInterest_Place.FirstOrDefault(x => x.OuterPostcode.Equals(postcode) && x.IsDefault);

                        if (objPOI != null)
                        {
                            return objPOI.AriaPointId;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return 0;
        }

        public static bool IsAway(long FromFixedPriceAriaPointId, long ToFixedPriceAriaPointId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<FixedPriceIsAwayViewModel> objPoi = db.PointOfInterest_Place.Where(c => c.AriaPointId == FromFixedPriceAriaPointId && c.AriaPointId == ToFixedPriceAriaPointId).Select(x => new FixedPriceIsAwayViewModel()
                    {
                        IsAway = x.IsAway
                    });
                    if (objPoi != null)
                    {
                        foreach (var item in objPoi.ToList())
                        {
                            if (item.IsAway)
                            {
                                return true;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;

        }
    }

    public class FixPriceIdViewModel
    {
        public long FromFixedPriceAriaPointId { get; set; }
        public long ToFixedPriceAriaPointId { get; set; }
        
    }

    public class FixedPriceIsAwayViewModel
    {
        public bool IsAway { get; set; }
    }
}