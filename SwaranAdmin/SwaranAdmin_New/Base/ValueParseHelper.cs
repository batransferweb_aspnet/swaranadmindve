﻿using System;
using System.Globalization;
using System.Web;

namespace SwaranAdmin_New.Base
{
    public static class ValueParseHelper
    {



        public static DateTime TryParseDateTime(string dateString)
        {

            DateTime parsedDate = DateTime.ParseExact(dateString, "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

            return parsedDate;

        }

        public static DateTime TryParseDate(string dateString)
        {


            DateTime parsedDate = DateTime.ParseExact(dateString, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            return parsedDate;

        }
        public static DateTime? TryParseNullDate(string dateString)
        {


            DateTime parsedDate = DateTime.ParseExact(dateString, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            return parsedDate;

        }
        public static string ToStringDate(DateTime dateString)
        {

            return dateString.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

        }
        public static string ToStringDateTime(DateTime dateString)
        {

            return dateString.ToString("dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);

        }

        public static string ToShortStringDate(DateTime dateString)
        {

            return dateString.ToString("dd/MM/yy", CultureInfo.InvariantCulture);


        }


        public static string GetIPAddress()
        {
            string szRemoteAddr = HttpContext.Current.Request.UserHostAddress;
            string szXForwardedFor = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            // string visitorIPAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            string szIP = "";

            if (szXForwardedFor == null)
            {
                szIP = szRemoteAddr;
            }
            else
            {
                szIP = szXForwardedFor;
                if (szIP.IndexOf(",") > 0)
                {
                    string[] ipRange = szIP.Split(',');
                    int le = 0;
                    return ipRange[le];
                }
            }
            return szIP;
        }

    }

  
}