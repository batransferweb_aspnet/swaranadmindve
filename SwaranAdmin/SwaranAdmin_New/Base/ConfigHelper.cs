﻿

using SwaranAdmin_New.DataManager;
using System;
using System.Configuration;
using System.Linq;

namespace SwaranAdmin_New.Base
{
    public class ConfigHelper
    {
        #region Api Key Configuration

        public static string GetDistanceApiKey()
        {
            Random rnd = new Random();
            try
            {
                using (var db = new BATransferEntities())
                {
                    var keyList = db.DistanceKeyTables.Select(x => x).ToList();
                    int r = rnd.Next(keyList.Count);
                    return keyList[r].KeyName;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return "";
        }

        #endregion

        #region Booking Configuration 
        public static int GetBookingRequestConfiguration(int requestConfigurationId)
        {
            int value = 0;

            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.BookingRequestConfigurations.Any(x => x.IsEnabled && !x.IsDeleted && x.BookingRequestConfigurationId == requestConfigurationId))
                    {
                        var objConfig =
                          db.BookingRequestConfigurations.First(
                              x => x.IsEnabled && !x.IsDeleted && x.BookingRequestConfigurationId == requestConfigurationId);
                        if (int.TryParse(objConfig.Value, out value))
                        {
                            return value;
                        }


                    }


                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return 0;
        }

        public static string GetBookingCancelation(int configId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.BookingCancelConfigurations.Any(x => x.IsActive && !x.IsDelete && x.BookingCancelId == configId))
                    {
                        return
                            db.BookingCancelConfigurations.First(
                                x => x.IsActive && !x.IsDelete && x.BookingCancelId == configId).TypeOfCancelation;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return "";
        }


        public static BookingNotificationConfigurationsViewModel GetBookingNotificationConfiguration(int configId)
        {
            var config = new BookingNotificationConfigurationsViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.BookingNotificationConfigurations.Any(x => x.IsEnabled && !x.IsDeleted && x.BookingNotificationConfigurationId == configId))
                    {

                        var objConfig = db.BookingNotificationConfigurations.First(x => x.IsEnabled && !x.IsDeleted && x.BookingNotificationConfigurationId == configId);
                        config.IsEmailEnabled = objConfig.IsEmailEnabled;
                        config.IsSmsEnabled = objConfig.IsSmsEnabled;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return config;
        }

        public static bool IsGustBookingEnabled(int siteId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.GustBookingConfigurations.Any(x => x.IsActvie && x.GustBooingId == siteId))
                    {

                        return db.GustBookingConfigurations.First(x => x.IsActvie && x.GustBooingId == siteId).IsWebEnable;

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }



        #endregion

        #region Get Email Subject

        public static string GetEmailSubject(int bookingStatus)
        {
            string subject = string.Empty;
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.BookingStatusConfigurations.Any(x => x.StatusId == bookingStatus && x.IsEnabled == true && x.IsDeleted == false))
                    {
                        subject =
                            db.BookingStatusConfigurations.First(
                                x => x.StatusId == bookingStatus && x.IsEnabled == true && x.IsDeleted == false)
                                .EmailSubject;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return subject;
        }


        public static string GetBookingSummeryUrl(int siteId)
        {
            string summaryUrl = string.Empty;
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.Organizations.Any(x => x.OrganizationId == siteId && x.IsEnable == true && x.IsDeleted == false))
                    {
                        summaryUrl =
                            db.Organizations.First(
                                x => x.OrganizationId == siteId && x.IsEnable == true && x.IsDeleted == false)
                                .SummaryUrl;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return summaryUrl;
        }
        #endregion

        #region Get Logo

        public static string GetWebLogo(int siteId)
        {
            string logoUrl = string.Empty;
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.Organizations.Any(x => x.OrganizationId == siteId && x.IsEnable == true && x.IsDeleted == false))
                    {
                        logoUrl =
                            db.Organizations.First(
                                x => x.OrganizationId == siteId && x.IsEnable == true && x.IsDeleted == false)
                                .LogoUrl;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return logoUrl;
        }

        public static string GetWebsiteUrl(int siteId) 
        {
            string logoUrl = string.Empty;
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.Organizations.Any(x => x.OrganizationId == siteId && x.IsEnable == true && x.IsDeleted == false))
                    {
                        logoUrl =
                            db.Organizations.First(
                                x => x.OrganizationId == siteId && x.IsEnable == true && x.IsDeleted == false)
                                .Website;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return logoUrl;
        }

        public static string GetWebLogoSource(int sourceId)
        {
            string logoUrl = string.Empty;
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.BookingSourceEnums.Any(x => x.BookingSourceEnumId == sourceId))
                    {
                        logoUrl =
                            db.BookingSourceEnums.First(x => x.BookingSourceEnumId == sourceId).Organization.LogoUrl;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return logoUrl;
        }

        #endregion
        #region Get Google reCAPTCHA

        public static string GetGooglereCAPTCHA(int siteId)
        {

            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.Organizations.Any(x => x.OrganizationId == siteId && x.IsEnable == true && x.IsDeleted == false))
                    {
                        return
                            db.Organizations.First(
                                x => x.OrganizationId == siteId && x.IsEnable == true && x.IsDeleted == false)
                                .ReCAPTCHA;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return "";
        }

        #endregion

        #region Tokent Validity Period

        public static int GetTokentValidityPeriod(int configId)
        {
            int value = 0;
            try
            {

                using (var db = new BATransferEntities())
                {
                    if (db.Customer_Configuration.Any(x => x.customerConfigurationId == configId))
                    {
                        var objonfig =
                              db.Customer_Configuration.First(x => x.customerConfigurationId == configId);
                        if (int.TryParse(objonfig.factor, out value))
                        {
                            return value;
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return value;
        }
        #endregion

        #region Get organization  Location 
        public static string GetOrganizationLocation(int organizationId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.Organizations.Any(x => x.OrganizationId == organizationId && x.IsEnable && !x.IsDeleted))
                    {
                        return
                            db.Organizations.First(x => x.OrganizationId == organizationId && x.IsEnable && !x.IsDeleted)
                                .Location;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return "";
        }
        #endregion 
        #region Payment Status Sublect
        public static string GetPaymentStatusSubject(int paymentStatus)
        {
            string subject = string.Empty;
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.PaymentStatusConfigurations.Any(x => x.PaymentStatusConfigurationId == paymentStatus && x.IsEnabled == true && x.IsDeleted == false))
                    {
                        subject =
                            db.PaymentStatusConfigurations.First(
                                x => x.PaymentStatusConfigurationId == paymentStatus && x.IsEnabled == true && x.IsDeleted == false)
                                .EmailSubject;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return subject;
        }
        #endregion

        #region Send email Configuration
        public static bool IsSendEmail(int bookingStatus)
        {

            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.BookingNotificationConfigurations.Any(x => x.BookingNotificationConfigurationId == bookingStatus && x.IsEnabled && !x.IsDeleted))
                    {

                        return db.BookingNotificationConfigurations.First(
                            x => x.BookingNotificationConfigurationId == bookingStatus && x.IsEnabled && !x.IsDeleted)
                            .IsEmailEnabled;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return false;
        }
        public static bool IsSendSms(int bookingStatus)
        {

            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.BookingNotificationConfigurations.Any(x => x.BookingNotificationConfigurationId == bookingStatus && x.IsEnabled && !x.IsDeleted))
                    {

                        return db.BookingNotificationConfigurations.First(
                            x => x.BookingNotificationConfigurationId == bookingStatus && x.IsEnabled && !x.IsDeleted)
                            .IsSmsEnabled;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetPriceFormat()
        {
            string priceFormat = string.Empty;

            priceFormat = ConfigurationManager.AppSettings["PriceFormat"];

            if (string.IsNullOrEmpty(priceFormat))
            {
                priceFormat = "{0:n2}";
            }

            return priceFormat;
        }

        #endregion

        public static bool IsBlockMember(string userid) 
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objBlockMember = db.sp_swaran_blockmember(userid).FirstOrDefault();
                    if (objBlockMember != null)
                    {
                        return objBlockMember.Value == 1 ? true : false;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return false;
        }

    }

    public class BookingNotificationConfigurationsViewModel
    {
        public bool IsEmailEnabled { get; set; }

        public bool IsSmsEnabled { get; set; }
    }

    public class BookingRequestConfigurationViewModel
    {
        public int BookingToJourneyMinutesInterval { get; set; }

        public int BookingToJourneyMaxDaysInterval { get; set; }

        public int BookingToJourneyMaxDistanceInterval { get; set; }
    }
}