﻿using SwaranAdmin_New.DataManager;
using System.Linq;

namespace SwaranAdmin_New.Base
{
    public static class OrganizationHelper
    {

        /// <summary>
        ///
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public static int GetRelevantOrganizationFromSite(int sourceId)
        {
            int organizationId = 0;

            using (var db = new BATransferEntities())
            {
                var relevantSource = db.BookingSourceEnums.FirstOrDefault(bse => bse.BookingSourceEnumId == sourceId);

                if (relevantSource != null)
                {
                    organizationId = relevantSource.OrganizationId;
                }
            }

            return organizationId;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public static string OrganizationNameFromSite(int sourceId)
        {
            string organizationName = string.Empty;

            using (var db = new BATransferEntities())
            {
                var relevantSource = db.BookingSourceEnums.FirstOrDefault(bse => bse.BookingSourceEnumId == sourceId);

                if (relevantSource != null && relevantSource.Organization != null)
                {
                    organizationName = relevantSource.Organization.OrganizationName;
                }
            }

            return organizationName;
        }

        public static bool IsSendToGhostFromSite(int siteId)
        {


            using (var db = new BATransferEntities())
            {
                if (db.Organizations.Any(bse => bse.OrganizationId == siteId))
                {
                    var objOrg = db.Organizations.First(bse => bse.OrganizationId == siteId);
                    return objOrg.IsSendToGhost;

                }
            }

            return false;
        }

        public static bool IsSendToGhostFromSource(int sourceId)
        {


            using (var db = new BATransferEntities())
            {

                var relevantSource = db.BookingSourceEnums.FirstOrDefault(bse => bse.BookingSourceEnumId == sourceId);

                if (relevantSource != null && relevantSource.Organization != null)
                {
                    return relevantSource.Organization.IsSendToGhost;
                }
            }

            return false;
        }
        public static decimal CardProcessingFeefromSite(int siteId)
        {


            using (var db = new BATransferEntities())
            {
                if (db.Organizations.Any(bse => bse.OrganizationId == siteId))
                {
                    var objOrg = db.Organizations.First(bse => bse.OrganizationId == siteId);
                    return objOrg.CardProcessingFee;

                }
            }

            return 0;
        }

        public static string GetOrganizationLocation(int sourceId)
        {


            using (var db = new BATransferEntities())
            {

                var relevantSource = db.BookingSourceEnums.FirstOrDefault(bse => bse.BookingSourceEnumId == sourceId);

                if (relevantSource != null && relevantSource.Organization != null)
                {
                    return relevantSource.Organization.Location;
                }
            }

            return string.Empty;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public static string GetOrganizationLogoFromSite(int sourceId)
        {
            string logoUrl = string.Empty;

            using (var db = new BATransferEntities())
            {
                var relevantSource = db.BookingSourceEnums.FirstOrDefault(bse => bse.BookingSourceEnumId == sourceId);

                if (relevantSource != null && relevantSource.Organization != null)
                {
                    logoUrl = relevantSource.Organization.LogoUrl;
                }
            }

            return logoUrl;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public static OrganizationDom GetOrganizationFromSite(int sourceId)
        {
            OrganizationDom response = null;

            using (var db = new BATransferEntities())
            {
                var relevantSource = db.BookingSourceEnums.FirstOrDefault(bse => bse.BookingSourceEnumId == sourceId);

                if (relevantSource != null && relevantSource.Organization != null)
                {
                    response = new OrganizationDom()
                    {
                        OrganizationName = relevantSource.Organization.OrganizationName,
                        Description = relevantSource.Organization.Description,
                        LogoUrl = relevantSource.Organization.LogoUrl,
                        Website = relevantSource.Organization.Website,
                        WebsiteText = relevantSource.Organization.WebsiteText,
                        DisplayName = relevantSource.Organization.DisplayText,
                        //Email = relevantSource.Organization.OrganizationContacts.Any(oc => oc.ContactTypeEnumId == (int)BaTransfer_CustomerService_DataTransfer.Common.Enums.ContactTypeEnum.Email)
                        //        ? relevantSource.Organization.OrganizationContacts.LastOrDefault(oc => oc.ContactTypeEnumId == (int)BaTransfer_CustomerService_DataTransfer.Common.Enums.ContactTypeEnum.Email).Value : string.Empty,
                        //EmailDisplayText = relevantSource.Organization.OrganizationContacts.Any(oc => oc.ContactTypeEnumId == (int)BaTransfer_CustomerService_DataTransfer.Common.Enums.ContactTypeEnum.Email)
                        //        ? relevantSource.Organization.OrganizationContacts.LastOrDefault(oc => oc.ContactTypeEnumId == (int)BaTransfer_CustomerService_DataTransfer.Common.Enums.ContactTypeEnum.Email).DisplayText : string.Empty,
                        Location = relevantSource.Organization.Location,
                        MemberDiscount = relevantSource.Organization.MemberDiscount,
                        JourneyDiscount = relevantSource.Organization.JourneyDiscount

                    };
                }

            }

            return response;
        }

    }

    public class OrganizationDom
    {
        public string OrganizationName { get; set; }
        public string Description { get; set; }
        public string LogoUrl { get; set; }
        public string Website { get; set; }
        public string WebsiteText { get; set; }
        public string Email { get; set; }
        public string EmailDisplayText { get; set; }
        public string DisplayName { get; set; }
        public string Location { get; set; }
        public decimal MemberDiscount { get; set; }
        public decimal JourneyDiscount { get; set; }
    }
}