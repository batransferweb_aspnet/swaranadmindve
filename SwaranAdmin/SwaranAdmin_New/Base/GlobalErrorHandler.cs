﻿using System;
using System.Web.Mvc;

 namespace SwaranAdmin_New.Base
{
    class MyErrorHandler : HandleErrorAttribute
    {
        public override void OnException(ExceptionContext filterContext)
        {
            Log(filterContext.Exception);

            base.OnException(filterContext);
        }

        private void Log(Exception ex)
        {
            ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");

        }
    }
}