﻿using System.IO;
using System.Net;
using System.Text;

 namespace SwaranAdmin_New.Base
{
    public static class SmsHelper
    {
        public static string SendSms(string serialisedData, string url, out HttpWebResponse webResponse)
        {


            var request = (HttpWebRequest)WebRequest.Create(url);

            var data = Encoding.ASCII.GetBytes(serialisedData);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            webResponse = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();

            return responseString;

        }
    }
}