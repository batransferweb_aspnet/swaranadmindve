﻿using System;
using System.Linq;

 namespace SwaranAdmin_New.Base
{
    public static class GenerateStringHelper
    {
        /// <summary>
        /// Generate a random string.
        /// </summary>
        /// <param name="length"></param>
        /// <param name="isNumeric"></param>
        /// <param name="isCapital"></param>
        /// <param name="isSimple"></param>
        /// <returns>
        ///The generated string.
        /// </returns>
        public static string GenerateRandomString(int length, bool isNumeric = false, bool isCapital = false, bool isSimple = false)
        {
            string chars = string.Empty;

            if (isNumeric)
            {
                chars += "0123456789";
            }

            if (isCapital)
            {
                chars += "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            }

            if (isSimple)
            {
                chars += "abcdefghijklmnopqrstuvwxyz";
            }

            if (!isNumeric && !isCapital && !isSimple)
            {
                chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            }

            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}