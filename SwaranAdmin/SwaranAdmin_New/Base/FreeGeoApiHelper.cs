﻿using System;
using System.Net.Http;

namespace SwaranAdmin_New.Base
{
    public class FreeGeoApiHelper
    {
        public static Rootobject GetCountryCode(string ip)
        {
            //  ip = "124.43.20.216";
            try
            {
                string apiUrl = "http://freegeoip.net/json/" + ip;
                HttpClient HttpClient = new HttpClient();

                var response = HttpClient.GetAsync(apiUrl).Result;

                if (response != null && response.ReasonPhrase != "Unauthorized")
                {

                    var result = response.Content.ReadAsStringAsync().Result;
                    var rootobject = JSonHelper.ConvertJSonToObject<Rootobject>(result);

                    return rootobject;


                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                   System.Reflection.MethodBase.GetCurrentMethod().Name,
                   ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }
    }

    public class Rootobject
    {
        public string ip { get; set; }
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string region_code { get; set; }
        public string region_name { get; set; }
        public string city { get; set; }
        public string zip_code { get; set; }
        public string time_zone { get; set; }
        public float latitude { get; set; }
        public float longitude { get; set; }
        public int metro_code { get; set; }
    }
}