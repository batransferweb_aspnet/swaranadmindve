﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using SwaranAdmin_New.Models.PriceCalculation;

namespace SwaranAdmin_New.Base
{
    public class AddressHelper
    {

        #region Singleton
        private static AddressHelper _instance;
        private static readonly object LockThis = new object();
        protected AddressHelper()
        {

        }
        public static AddressHelper Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new AddressHelper());
                }
            }
        }
        #endregion


        public List<AddressDom> LoadAddresses(string name)
        {
            var list = new List<AddressDom>();

            string airportportCode = string.Empty;
            string postcode = string.Empty;


            string commonText = string.Empty;
            int buildingNumber = 0;


            Regex regexFullpostcode =
                    new Regex(
                        @"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})");
            Regex regexNoSpace =
                    new Regex(
                        @"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})",
                        RegexOptions.IgnorePatternWhitespace);

            try
            {

                if (name.Split(' ').Length > 1)
                {
                    //var listStreet = streetText.ToUpper().Split(',').ToList();
                    //var listBuildingNames = buidingText.ToUpper().Split(',').ToList();

                    for (int j = 0; j < name.Split(' ').Length; j++)
                    {

                        Match match = null;
                        if (j > 0 && j < 2)
                            match = regexFullpostcode.Match(name);
                        //  match = regexFullpostcode.Match(name.Split(' ')[j - 1] + " " + name.Split(' ')[j]);


                        if (match != null && match.Success)
                        {
                            postcode = name.Split(' ')[j - 1] + " " + name.Split(' ')[j];

                        }
                        else
                        {
                            postcode = "";
                            if (j > 0)
                            {
                                commonText = name;// name.Split(' ')[j - 1] + " " + name.Split(' ')[j];
                            }
                        }

                        Match matchNoSpace = regexNoSpace.Match(name.Split(' ')[j]);
                        if (matchNoSpace.Success)
                        {
                            string postcodeNospace = name.Split(' ')[j];

                            postcode = postcodeNospace.Substring(0, postcodeNospace.Length - 3) + " " + postcodeNospace.Substring(postcodeNospace.Length - 3);
                            name = name.Replace(name.Split(' ')[j], postcode);
                        }



                    }


                }
                else
                {
                    Match match = regexFullpostcode.Match(name);
                    Match matchNoSpace = regexNoSpace.Match(name);
                    if (match.Success)
                    {
                        postcode = name;
                    }
                    if (matchNoSpace.Success)
                    {
                        postcode = name.Substring(0, name.Length - 3) + " " + name.Substring(name.Length - 3);


                    }
                    //else if (name.Length == 3)
                    //{
                    //    if (!Regex.IsMatch(name, @"\d+"))
                    //    {
                    //        airportportCode = name;
                    //    }
                    //    else
                    //    {
                    //        postcode = "";
                    //        commonText = name;
                    //    }
                    //}
                    else
                    {
                        postcode = "";
                        commonText = name;

                    }


                }



                string areaType = "";
                list = GetAddressElasticSearch(postcode, airportportCode, areaType, commonText);

            }
            catch (Exception ex)
            {


            }

            return list;
        }

        public List<AddressDom> GetAddressElasticSearch(string postcode, string airpostcode, string areaType, string commonText)
        {
            try
            {


                HttpClient client = new HttpClient();
                //string apiUrl = "https://essearch.batransfer.info/api/Address/GetAddress?postCode=" + postcode + "&airportCode=" + airpostcode + "&commonString=" + commonText + "&areaType=" + areaType + "";

                string apiUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.ElasticAddressApi) + "GetAddress?postCode=" + postcode + "&airportCode=" + airpostcode + "&commonString=" + commonText + "&areaType=" + areaType + "";
                var response = client.GetAsync(apiUrl).Result;

                if (response != null && response.ReasonPhrase != "Unauthorized")
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var rootobject = JsonConvert.DeserializeObject<MainAddress>(result);
                    return rootobject.Addresses;
                }

            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public List<AddressDom> GetFavouriteAddress(string userid)
        {

            var newList = new List<AddressDom>();
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<FavouriteAddressViewModel> objAddressList =
                        db.sp_user_favourite_address(userid).ToList().Select(x => new FavouriteAddressViewModel()
                        {
                            FavouriteAddressId = x.FavouriteAddressId,
                            AddressId = x.AddressId,
                            AreaPointId = (int)x.FixedPriceAriaPointId,
                            AreaTypeId = x.AreaTypeId,
                            //  AreaTypeName = x.AreaTypeName,
                            FullAddressLabel = x.Label,
                            FullAddress = x.FullAddress,
                            OrganisationName = x.OrganisationName,
                            DepartmentName = x.DepartmentName,
                            BuildingName = x.BuildingName,
                            SubBuildingName = x.SubBuildingName,
                            BuildingNumber = x.BuildingNumber,
                            CountryCode = x.CountryCode,
                            Street = x.Street,
                            PostTown = x.PostTown,
                            Postcode = x.Postcode,
                            Longitude = x.Long ?? 0,
                            Latitude = x.Lat ?? 0,
                            Note = x.Note,
                            AirportCode = x.AirportCode,

                        });

                    string addressid = string.Join(",", objAddressList.Select(s => s.AddressId).ToArray());

                    //string apiUrl = "https://essearch.batransfer.info/api/Address/GetAddressByIds?addressIds=" + addressid;

                    string apiUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.ElasticAddressApi) + "GetAddressByIds?addressIds=" +
                                  addressid;

                    HttpClient httpClient = new HttpClient();

                    var response = httpClient.GetAsync(apiUrl).Result;

                    if (response != null && response.ReasonPhrase != "Unauthorized")
                    {

                        var result = response.Content.ReadAsStringAsync().Result;
                        var rootobject = JsonConvert.DeserializeObject<MainAddress>(result);
                        if (rootobject != null && rootobject.Addresses != null)
                        {


                            foreach (var address in rootobject.Addresses.ToList())
                            {
                                var favoritAdd = objAddressList.FirstOrDefault(x => x.AddressId == address.AddressId);
                                if (favoritAdd != null)
                                {
                                    var aa = new AddressDom();
                                    aa.FavouriteAddressId = favoritAdd.FavouriteAddressId;
                                    aa.AddressId = favoritAdd.AddressId;
                                    aa.AirportCode = favoritAdd.AirportCode;
                                    aa.AreaPointId = favoritAdd.AreaPointId;
                                    aa.AreaTypeId = favoritAdd.AreaTypeId;
                                    aa.AreaTypeName = favoritAdd.AreaTypeName;
                                    aa.FullAddressLabel = favoritAdd.FullAddressLabel;
                                    aa.FullAddress = favoritAdd.FullAddress;
                                    aa.OrganisationName = favoritAdd.OrganisationName;
                                    aa.DepartmentName = favoritAdd.DepartmentName;
                                    aa.BuildingName = favoritAdd.BuildingName;
                                    aa.SubBuildingName = favoritAdd.SubBuildingName;
                                    aa.BuildingNumber = favoritAdd.BuildingNumber;
                                    aa.CountryCode = favoritAdd.CountryCode;
                                    aa.Street = favoritAdd.Street;
                                    aa.PostTown = favoritAdd.PostTown;
                                    aa.Postcode = favoritAdd.Postcode;
                                    aa.Longitude = favoritAdd.Longitude;
                                    aa.Latitude = favoritAdd.Latitude;
                                    aa.Note = favoritAdd.Note;
                                    aa.IsNonElastic = false;
                                    newList.Add(aa);
                                }
                            }
                        }

                    }
                    return newList;

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;


        }

        public List<AddressDom> LoadAllStreet(string name) 
        {
            var list = new List<AddressDom>();

            string airportportCode = string.Empty;
            string postcode = string.Empty;


            string commonText = string.Empty;
            int buildingNumber = 0;


            Regex regexFullpostcode =
                    new Regex(
                        @"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})");
            Regex regexNoSpace =
                    new Regex(
                        @"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})",
                        RegexOptions.IgnorePatternWhitespace);

            try
            {

                if (name.Split(' ').Length > 1)
                {
                    //var listStreet = streetText.ToUpper().Split(',').ToList();
                    //var listBuildingNames = buidingText.ToUpper().Split(',').ToList();

                    for (int j = 0; j < name.Split(' ').Length; j++)
                    {

                        Match match = null;
                        if (j > 0 && j < 2)
                            match = regexFullpostcode.Match(name);
                        //  match = regexFullpostcode.Match(name.Split(' ')[j - 1] + " " + name.Split(' ')[j]);


                        if (match != null && match.Success)
                        {
                            postcode = name.Split(' ')[j - 1] + " " + name.Split(' ')[j];

                        }
                        else
                        {
                            postcode = "";
                            if (j > 0)
                            {
                                commonText = name;// name.Split(' ')[j - 1] + " " + name.Split(' ')[j];
                            }
                        }

                        Match matchNoSpace = regexNoSpace.Match(name.Split(' ')[j]);
                        if (matchNoSpace.Success)
                        {
                            string postcodeNospace = name.Split(' ')[j];

                            postcode = postcodeNospace.Substring(0, postcodeNospace.Length - 3) + " " + postcodeNospace.Substring(postcodeNospace.Length - 3);
                            name = name.Replace(name.Split(' ')[j], postcode);
                        }



                    }


                }
                else
                {
                    Match match = regexFullpostcode.Match(name);
                    Match matchNoSpace = regexNoSpace.Match(name);
                    if (match.Success)
                    {
                        postcode = name;
                    }
                    if (matchNoSpace.Success)
                    {
                        postcode = name.Substring(0, name.Length - 3) + " " + name.Substring(name.Length - 3);


                    } 
                    else
                    {
                        postcode = "";
                        commonText = name;

                    }


                }



                string areaType = "";
                list = GetStreetsElasticSearch(postcode, airportportCode, areaType, commonText);

            }
            catch (Exception ex)
            {


            }

            return list;
        }

        public List<AddressDom> GetStreetsElasticSearch(string postcode, string airpostcode, string areaType, string commonText) 
        {
            try
            {

                var ist = new List<AddressDom>();
                HttpClient client = new HttpClient();
                //string apiUrl = "https://essearch.batransfer.info/api/Address/GetAddress?postCode=" + postcode + "&airportCode=" + airpostcode + "&commonString=" + commonText + "&areaType=" + areaType + "";

                string apiUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.ElasticAddressApi) + "GetAddress?postCode=" + postcode + "&airportCode=" + airpostcode + "&commonString=" + commonText + "&areaType=" + areaType + "";
                var response = client.GetAsync(apiUrl).Result;

                if (response != null && response.ReasonPhrase != "Unauthorized")
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var rootobject = JsonConvert.DeserializeObject<MainAddress>(result);
                    var streets = rootobject.Addresses.GroupBy(x => x.Street);

                    streets.ToList().ForEach(obj =>
                    { 
                        foreach (var item in obj)
                        {  
                            ist.Add(item);
                        }
                        
                    });

                    return ist;
                }

            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public static SpecialOccasionFixedPriceDom GetFixedPriceDetail(long fixedPriceId)
        {

            SpecialOccasionFixedPriceDom fixedPrice = null;

            try
            {
                using (var db = new BATransferEntities())
                {
                    var relevantFixedPrice = db.FixedPrice_AriaPoints_Distance.FirstOrDefault(fad => fad.PriceId == fixedPriceId);

                    if(relevantFixedPrice != null)
                    {
                        fixedPrice = new SpecialOccasionFixedPriceDom()
                        {
                            FixedPriceId = fixedPriceId,
                            FromAreaPointId = relevantFixedPrice.FromAriapointId,
                            ToAreaPointId = relevantFixedPrice.ToAriapointId
                        };
                    }
                }
            }
            catch(Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return fixedPrice;
        }
    }
}