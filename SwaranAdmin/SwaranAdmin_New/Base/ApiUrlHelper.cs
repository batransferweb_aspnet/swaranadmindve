﻿ 
using System.Linq;
using SwaranAdmin_New.DataManager;

namespace SwaranAdmin_New.Base
{
    public static class ApiUrlHelper
    {

        public static string GetUrl(int id)
        {
            using (var db = new BATransferEntities())
            {
                return db.ApiUrls.First(x => x.ApiUrlId == id).ApiUrlValue;
            }

        }
    }
}