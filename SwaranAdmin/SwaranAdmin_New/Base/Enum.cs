﻿namespace SwaranAdmin_New.Base
{

    public enum EmissionTypeEnum
    {
        NoEmmission=1,
        UltraNoEmmission = 2,
    }
    public enum ViapointConfigEnum
    {
        PriceCalculationId = 1
    }
    public enum DriverJobStatusEnum
    {
        Waiting_for_action=1,
        Waiting_for_interview=2,
        Approved=3,
        Waiting_for_reply=4,
        Could_not_contact=5,
        Cancelled=6
    }
    public enum LoginStatusEnum
    {
        LoggedIn = 1,
        LoggedOut = 2,
        LoggedOff = 3
    }

    public enum BookingStatusEnum
    {
        Requested = 1,
        Confirmed = 2,
        WaitingForVerification = 3,
        Hold = 4,
        Cancelled = 5,
        Dispatched = 6,
        Arrived = 7,
        POB = 8,
        SoonToClear = 9,
        Panic = 10,
        Finished = 11,
        Reject = 12,
        Pending = 13
    }

    public enum TransactionTypeValEnum
    {
        CreditByAdmin = 1,
        CreditBySystem = 2,
        DebitByAdmin = 3,
        DebitBySystem = 4,
        DebitByCustomer = 5
    }

    public enum TransactionTypeValueEnum 
    {
        CreditByAdmin = 1,
        CreditBySystem = 2,
        DebitByAdmin = 3,
        DebitBySystem = 4
    }
    public enum SeriousnessEnum
    {
        Major = 1,
        Minor = 2,

    }
    public enum PreferredMethodofContactEnum
    {
        Home_Phone = 1,
        Mobile_Phone = 2,
        Email = 3,
    }
    public enum BookingRefundTypeEnum
    {
        FullBookingRefund = 1,
        OneWayBookingRefund = 2,
        ReturnBookingRefund = 3
    }

    public enum YearsDriversLicenceHeldEnum
    {
        LessThan1Year = 1,
        Years_1to2 = 2,
        Years_2to3 = 3,
        Years_3to4 = 4,
        Years_4to5 = 5,
        MoreThan5Year = 6,
    }
    public enum PreferToWorkEnum
    {
        Full_Time = 1,
        Part_Time = 2,
        Weekends_Only = 3,
        WeekendsAndEvenings = 4,

    }

    public enum WillingToStartEnum
    {
        Immediately = 1,
        in_1_week = 2,
        in_2_weeks = 3,
        in_3_weeks = 4,
        in_4_weeks = 5,
        in_more_than_5_weeks = 6,

    }


    public enum FreeJourneyCreateEnum
    {
        System = 1,
        Staff = 2
    }
    public enum CustomerFreeJourneyTypeEnum
    {
        FreeJourney = 1
    }
    public enum AddressCriteriaEnum
    {
        Street = 1,
        BuildingName = 2
    }
    public enum BookingCancelEnum
    {
        Over_Booking = 1,
        Due_to_heavy_traffic = 2,
        Allocated_Vehicle_accident = 3,
        Cancelled_by_customer = 4,
        Duplicate_Booking_by_Error_or_Mistake = 5,
        Card_Payment_not_Completed = 6,
        Test_Booking_by_staff = 7,
        Anonymous_Booking = 8,
        Others = 9,
    }

    public enum VehicleTypeEnum
    {
        Saloon_Car = 1,
        Estate = 2,
        Executive_Car = 3,
        MPV_5 = 4,
        MPV_6 = 5,
        MPV_7 = 6,
        MPV_8 = 7,
    }

    public enum CustomCardChargeTypeEnum
    {
        Amount = 1,
        Percentage = 2
    }
    public enum JourneyPaymentType
    {
        CardPrice = 1,
        CashPrice = 2,
    }
    public enum ReturnJourneyEnum
    {
        ReverseReturnJourney = 1,
        NewReturnJourney = 2,
        NoReturnJourney = 3,

    }
    public enum TokentValidityPeriod_Enum
    {
        IsMultipleLoginEnabled = 1,
        LoginValidityHours = 2,
        PasswordResetTokenValidityHours = 3,
        EmailVerificationTokenValidityMinutes = 4,
        PhoneVerificationTokenValidityMinutes = 5
    }
    public enum BookingCancelConfigurationEnum 
    {
        Over_Booking = 1,
        Due_to_heavy_traffic = 2,
        Allocated_Vehicle_accident = 3,
        Cancelled_by_customer = 4,
        Duplicate_Booking_by_Error_or_Mistake = 5,
        Card_Payment_not_Completed = 6,
        Test_Booking_by_staff = 7,
        Anonymous_Booking = 8,
        Others = 9

    }
    public enum BookingRequestConfiguration_Enum
    {
        BookingToJourneyMinutesInterval = 1,
        BookingToJourneyMaxDaysInterval = 2,
        BookingToJourneyMaxDistanceInterval = 3

    }
    public enum BookingCharge_Enum
    {
        Card_Charge = 1,
        VAT = 2,
        Tax = 3,
        Bank_Transfer_Charge = 4,
    }
    public enum BookingSource_Enum
    {
        Unknown = 0,
        BA_Web = 1,
        BA_Tel = 2, 
        BA_Android = 3,
        BA_iOS = 4,
        BA_Windows = 5,
        ACC_Web = 6,
        AO_Web = 7,
        ET_Web = 8,
        WT_Web = 9,
        ACC_iOS = 10,
        BAT_Web = 11,
        BAL_Web = 12,
        BAC_Web = 13,
    }

    public enum PaymentRedirectURLEum
    {
        WEB = 1,
        LOCAL = 2,
    }
    public enum BookingSiteEnum
    {
        BA = 1,//Ba transfer
        ACC = 2,//121Transfer
        AO = 3,//Airport only
        ET = 4,
        WT = 5, //Wembley_Minicabs 
        WPF = 6,
        BAT = 7,//ba-transfer.com
        BAL = 8,//ba-transfer.london
        BAC = 9//batransfer.co.uk
    }

    public enum PointsTypeEnum
    {
        PerJourney = 1,
    }
    public enum FreeJourneyPointsTypeEnum
    {
        FreeJourney = 1,
        VehicleUpgrade=2
    }
    public enum DefaultRateEnum
    {
        GUST_RATE = 1,
        CARD_RATE = 2,
        CASH_RATE = 3,
        BLUE_RATE = 4,
        BROWN_RATE = 5,
        SILVER_RATE = 6,
        GOLD_RATE = 7,
        PLATINUM_RATE = 8,
        DIAMOND_RATE = 9,
        OUTER_LONDON_RATE = 1010,
    }



    public enum LoyaltyType_Enum
    {
        New = 0,
        Blue = 1,
        Brown = 2,
        Silver = 3,
        Gold = 4,
        Platinum = 5,
        Diamond = 6
    }


    public enum StatementTypeEnum
    {
        Daily = 1,
        Weekly = 2,
        Monthly = 3,
        Quarterly = 4,
        Yearly = 5
    }
    public enum BookingPersonTypeEnum
    {
        Member = 1,
        Gust = 2
    }
    public enum BabySeatTypeEnum
    {
        ChildSeat = 1,
        BoosterSeat = 2,
        InfantSeat = 3
    }

    public enum RateConfigEnum
    {
        MaximumDistance = 1,

    }

    public enum EmailDomainEnum
    {
        UserVerificationEmail = 1,
        BookingNotificationEmailBA = 2,
        PasswordResetEmail = 3,
        DriverStatementEmail = 4,
        ReplyEmail = 5,
        ProfileUpdateEmail = 6,
        CustomerEmail = 7,
        RefundEmail = 8,
        BookingNotificationEmailET = 9,
        BookingNotificationEmailAO = 10,
        BookingNotificationEmailWT = 11,
        ProfileUpdateEmailBA = 12,
        ProfileUpdateEmailET = 13,
        ProfileUpdateEmailAO = 14,
        ProfileUpdateEmailWT = 15,
        BookingNotificationEmailBAT = 16,
        BookingNotificationEmailBAL = 17,
        BookingNotificationEmailBAC = 18

    }





    public enum ApiUrlEnum
    {
        EmailApi = 1,
        SmsApi = 2,
        PaymentApi = 3,
        ProfileApi = 4,
        CustomerFileApi = 5,
        DriverFileApi = 6,
        StaffFileApi = 7,
        PasswordResetBaseUrl = 8,
        UserEmailVerificationBaseUrl = 9,
        AccountLoginPage = 10,
        HomePage = 11,
        AuthorizePaymentApi = 12,
        AddressService = 13,
        GoogleApiServices = 14,
        DriverStatementApi = 15,
        BookingInfoApi = 16,
        MapApi = 17,
        UserActionLogApi = 18,
        BookingNotifierApi = 19,
        EmailWithoutAttachmentApi = 20,
        BookingPushNotificationApi = 21,
        ElasticAddressApi = 22,
        FullRefundUrl = 23,
        PartialRefundUrl = 24,
        SmsBookingApi = 25,
        SmsDispatchApi = 26,
        StaffUserEmailVerificationBaseUrl = 27,
        SwaranRedirectUrl = 28

    }

    public enum BookingConfirmationConfigurationEnum
    {
        IsMemberBookingConfirmationUponPayment = 1,
        IsCorporateBookingConfirmationUponPayment = 2,
        IsMemberBookingConfirmationUponEditBooking = 3,
        IsCorporateBookingConfirmationUponEditBooking = 4,
        IsMemberBookingConfirmationUponRequestBooking = 5,
        IsCorporateBookingConfirmationUponRequestBooking = 6
    }

    public enum JourneyTypeEnum
    {
        OneWay = 1,
        TwoWay = 2
    }


    public enum AreapointTypeEnum
    {
        Place = 1,
        City = 2,
        Cruiseport = 3,
        Station = 4,
        POI = 6,
        Airport = 10,

    }
    public enum PreviousPaymentTypeEnum
    {
        Cash = 1,
        Card = 2,
        Bank_Transfer = 3,
        Free_Journey = 4,
        Points = 5,
        Promo_Code = 6,
        Voucher = 7,
        Use_My_Credits = 8,
        Pay_Booking_Fee = 9
    }
    public enum PaymentTypeEnum
    {
        Cash = 1,
        Card = 2,
        Bank_Transfer = 3,
        Free_Journey = 4,
        Points = 5,
        Promo_Code = 6,
        Voucher = 7, 
        Use_My_Credits = 8,
        Pay_Booking_Fee=9,
        Account = 10
    }

    public enum CardTypeEnum
    {
        VISA_CREDIT = 19,
        MASTER_CREDIT = 20,
        AMEX_CREDIT = 21,
        VISA_DEBIT = 22,
        MASTER_DEBIT = 23,
        AMEX_DEBIT = 24
    }
    public enum PaymentSourceEnum
    {
        WorldPay = 1,
        PayPal = 2
    }

    public enum PaymentStatusEnum
    {
        Faild=0,
        Pending = 1,
        Cancelled = 2,
        PartiallyPaid = 3,
        Paid = 4,
        Rejected = 5,
        Authorized = 6,
        FullyRefund = 7,
        PartialRefund = 8,
        BookingFeePending=9
    }

    public enum CardPaymentTypeEnum
    {
        PayOnline = 1,
        PayOverThePhone = 2,
    }

    public enum CustomerTypeEnum
    {
        Member = 1,
        Corporate = 2,
    }





    public enum BookingRequestConfigurationEnum
    {
        BookingToJourneyMinutesInterval = 1,
    }

    public enum BookingNotificationConfigurationEnum
    {
        BookingRequested = 1,
        BookingConfirmed = 2,
        BookingWaitingForVerification = 3,
        BookingHold = 4,
        BookingCancelled = 5,
        BookingDispatched = 6,
        BookingVehicleArrived = 7,
        BookingPOB = 8,
        SoonToClear = 9,
        Panic = 10,
        BookingFinished = 11,
        BookingRejected = 12,
        BookingPending = 13,
        PaymentModeChanged = 100
    }

    public enum PushNotificationTypeEnum
    {

        BookingRequested = 1,
        BookingConfirmed = 2,
        BookingWaitingForVerification = 3,
        BookingHold = 4,
        BookingCancelled = 5,
        BookingDispatched = 6,
        BookingVehicleArrived = 7,
        BookingPOB = 8,
        SoonToClear = 9,
        Panic = 10,
        BookingFinished = 11,
        BookingRejected = 12,
        BookingPending = 13,
        Advertisement = 14,        
        PaymentModeChanged = 100
    }

    public enum StatusCodeEnum
    {
        OK = 200,
        BadRequest = 400,
        Unauthorized = 401,
        Forbidden = 403,
        NotFound = 404,
        Conflict = 409,
        InternalServerError = 500,
        TokenExpired = 600,
        UserCreationFailed = 601,
        Failed = 602,
        UserNotAuthorized = 603,
        DefaultRecordNotFound = 604,
        IpRestriction = 605,
        UserSuspended = 606,
        AlreadyProcessed = 607,
        EmptySet = 608
    }




    public enum CustomerConfigurationEnum
    {
        isMultipleLoginEnabled = 1,
        loginValidityHours = 2,
        passwordResetTokenValidityMinutes = 3,
        emailVerificationTokenValidityMinutes = 4,
        phoneVerificationTokenValidityMinutes = 5
    }

    public enum VehicleCombinationConfigurationEnum
    {
        CombinationCount = 1,
        MaxPassangersCount = 2

    }


    public enum DriverAddDeductionEnum
    {
        Addition = 1,
        Deduction = 2
    }

    public enum EnumPriceCalculationType
    {
        DistancePrice = 1,
        SpeedPrice = 2,
        FixedPrice = 3,
    }


    public enum EnumVATType
    {
        InclusiveVAT = 1,
        PlusVAT = 2,
        ExemptVAT = 3,

    }

    public enum EnumRateFomulla
    {
        CustomerRateFomulla,
        DriverRateFomulla
    }

    public enum AccountType
    {
        Cash,
    }

    public enum RateAccountTypes
    {
        AccountRate,
        DriverRate,
        AccountWaitingRate,
        DriverWaitingRate,
    }
    public enum EnumRateTypesEnums
    {
        Distance,
        Speed,
    }

    public enum JobStatus
    {
        Reject,
        Accept,
        Timeout,
    }

    //public enum JourneyTypesEnum
    //{
    //    PlaceToPlace,
    //    PlaceToPlot,
    //    PlotToPlace,
    //    PlotToPlot,
    //    PlaceToAllPlot,
    //    AllPlotToPlace
    //}

    public enum AccountTypes
    {
        AccountName,
        AllAccount
    }

    public enum EnumVehicleTypes
    {
        VehicleName,
        AllVehicleTypes
    }


    public enum DriverRates
    {
        DriverRateName,
        AllDriverRates
    }

    public enum EnumDriverStatus
    {
        UnAvailable = 0,
        Available = 1,
        Break = 2,
        Job = 3,

    }

    public enum DriverJobStatus
    {
        OnRoute = 1,//- Light Blue  
        Arrived = 2,// - Pink
        POB = 3,// (passenger on Board) /Busy - Red 
        SoonToClear = 4,//- Yellow  
        Finish = 5,//green
    }

    public enum EnumCustomerType
    {
        Member = 1,
        Corporate = 2,
    }

    public enum EnumExtraCharge
    {
        BookingFee = 1,
        ToNearest = 2,
        AdditionalVehicleFee = 3,
        MinimumFee = 4,
        ExtraDrop = 5,
        BaseFee = 6,
        CancellationFee = 7,
        ServiceCharge = 8
    }

    public enum EnumCustomerRateDistance
    {
        DEFAULTDAY = 1,
        ALLPLOT = 2,
    }

    public enum EnumCustomerRateDay
    {
        DEFAULT = 0,
        MONDAY = 1,
        TUESDAY = 2,
        WENSDAY = 3,
        THURSDAY = 4,
        FRIDAY = 5,
        SATURDAY = 6,
        SUNDAY = 7

    }

}

