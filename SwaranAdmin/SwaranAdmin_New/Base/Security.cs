﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

 namespace SwaranAdmin_New.Base
{
    public static class Security
    {
        private const string EntropyValue = "BATransfer12";

        public static bool VerifyHashedPassword(string hashedPassword, string password)
        {
            byte[] buffer4;
            if (hashedPassword == null)
            {
                return false;
            }
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            byte[] src = Convert.FromBase64String(hashedPassword);
            if ((src.Length != 0x31) || (src[0] != 0))
            {
                return false;
            }
            byte[] dst = new byte[0x10];
            Buffer.BlockCopy(src, 1, dst, 0, 0x10);
            byte[] buffer3 = new byte[0x20];
            Buffer.BlockCopy(src, 0x11, buffer3, 0, 0x20);
            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, dst, 0x3e8))
            {
                buffer4 = bytes.GetBytes(0x20);
            }
            return ByteArraysEqual(buffer3, buffer4);
        }

        public static bool ByteArraysEqual(byte[] a1, byte[] b1)
        {
            int i;
            if (a1.Length == b1.Length)
            {
                i = 0;
                while (i < a1.Length && (a1[i] == b1[i])) //Earlier it was a1[i]!=b1[i]
                {
                    i++;
                }
                if (i == a1.Length)
                {
                    return true;
                }
            }

            return false;
        }

        public static string HashPassword(string password)
        {
            byte[] salt;
            byte[] buffer2;
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, 0x10, 0x3e8))
            {
                salt = bytes.Salt;
                buffer2 = bytes.GetBytes(0x20);
            }
            byte[] dst = new byte[0x31];
            Buffer.BlockCopy(salt, 0, dst, 1, 0x10);
            Buffer.BlockCopy(buffer2, 0, dst, 0x11, 0x20);
            return Convert.ToBase64String(dst);
        }

        public static string EncryptPassword(string password)
        {

            byte[] bIn = Encoding.Unicode.GetBytes(password);
            byte[] bSalt = Convert.FromBase64String(EntropyValue);
            byte[] bRet = null;

            HashAlgorithm hm = new HMACSHA256();

            KeyedHashAlgorithm kha = (KeyedHashAlgorithm)hm;
            if (kha.Key.Length == bSalt.Length)
            {
                kha.Key = bSalt;
            }
            else if (kha.Key.Length < bSalt.Length)
            {
                byte[] bKey = new byte[kha.Key.Length];
                Buffer.BlockCopy(bSalt, 0, bKey, 0, bKey.Length);
                kha.Key = bKey;
            }
            else
            {
                byte[] bKey = new byte[kha.Key.Length];
                for (int iter = 0; iter < bKey.Length;)
                {
                    int len = Math.Min(bSalt.Length, bKey.Length - iter);
                    Buffer.BlockCopy(bSalt, 0, bKey, iter, len);
                    iter += len;
                }
                kha.Key = bKey;
            }

            bRet = kha.ComputeHash(bIn);

            return Convert.ToBase64String(bRet);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="token"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string EncryptToken(string password, string value)
        {

            byte[] salt;
            byte[] buffer2;
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, 0x10, 0x3e8))
            {
                salt = bytes.Salt;
                buffer2 = bytes.GetBytes(0x20);
            }
            byte[] dst = new byte[0x31];
            Buffer.BlockCopy(salt, 0, dst, 1, 0x10);
            Buffer.BlockCopy(buffer2, 0, dst, 0x11, 0x20);
            return Convert.ToBase64String(dst);

            //string encryptValue = value.Replace('/', 'B').Replace(' ', 'A').Replace(':', 'T').Substring(4, 12);
            //byte[] bIn = Encoding.Unicode.GetBytes(token);
            //byte[] bSalt = Convert.FromBase64String(encryptValue);
            //byte[] bRet = null;

            //HashAlgorithm hm = new HMACSHA256();

            //KeyedHashAlgorithm kha = (KeyedHashAlgorithm)hm;
            //if (kha.Key.Length == bSalt.Length)
            //{
            //    kha.Key = bSalt;
            //}
            //else if (kha.Key.Length < bSalt.Length)
            //{
            //    byte[] bKey = new byte[kha.Key.Length];
            //    Buffer.BlockCopy(bSalt, 0, bKey, 0, bKey.Length);
            //    kha.Key = bKey;
            //}
            //else
            //{
            //    byte[] bKey = new byte[kha.Key.Length];
            //    for (int iter = 0; iter < bKey.Length;)
            //    {
            //        int len = Math.Min(bSalt.Length, bKey.Length - iter);
            //        Buffer.BlockCopy(bSalt, 0, bKey, iter, len);
            //        iter += len;
            //    }
            //    kha.Key = bKey;
            //}

            //bRet = kha.ComputeHash(bIn);

            //return Convert.ToBase64String(bRet);
        }
    }
}