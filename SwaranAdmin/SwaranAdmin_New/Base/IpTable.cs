﻿ 
using System;
using SwaranAdmin_New.DataManager;

namespace SwaranAdmin_New.Base
{
    public class IpTable
    {
        public static string SaveIp()
        {
            try
            {


                using (var db = new BATransferEntities())
                {
                    var ipTable = new IPTable();
                    ipTable.UserHostName = System.Web.HttpContext.Current.Request.UserHostAddress;
                    ipTable.UserAgent = System.Web.HttpContext.Current.Request.UserAgent;
                    ipTable.Url = System.Web.HttpContext.Current.Request.Url.ToString();
                    ipTable.QueryString = System.Web.HttpContext.Current.Request.QueryString.ToString();
                    ipTable.Path = System.Web.HttpContext.Current.Request.Path;
                    ipTable.IsSecureConnection = System.Web.HttpContext.Current.Request.IsSecureConnection;
                    ipTable.IsAuthenticated = System.Web.HttpContext.Current.Request.IsAuthenticated;
                    ipTable.ContentType = System.Web.HttpContext.Current.Request.ContentType;
                    ipTable.Browser = System.Web.HttpContext.Current.Request.Browser.Browser;
                    ipTable.ApplicationPath = System.Web.HttpContext.Current.Request.ApplicationPath;
                    ipTable.CreatedDate = DateTime.UtcNow;
                    db.IPTables.Add(ipTable);
                    db.SaveChanges();
                    return ipTable.UserHostName;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return "";
        }

    }
}