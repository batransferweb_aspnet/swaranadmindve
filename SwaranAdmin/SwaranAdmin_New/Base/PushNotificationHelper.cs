﻿using SwaranAdmin_New.DataManager;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;

namespace SwaranAdmin_New.Base
{
    public static class PushNotificationHelper
    {

        /// <summary>
        ///
        /// </summary>
        /// <param name="deviceOsType"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool PostPushNotification(string deviceOsType, string message)
        {
            bool response = false;

            switch (deviceOsType)
            {
                case "iOS":
                    PostToiOS(message);
                    response = true;
                    break;
                case "Android":
                    PostToAndroid(message);
                    response = true;
                    break;
            }

            return response;
        }

        private static void PostToiOS(string message)
        {
            string devicetocken = "7a7c4d3a0342b213718ccece131a51906f748a87eb8ccae6b35f757814bb96d7";//  iphone device token
            int port = 2195;
            String hostname = "gateway.sandbox.push.apple.com";
            //String hostname = "gateway.push.apple.com";

            string certificatePath = HttpContext.Current.Server.MapPath("final.p12");

            string certificatePassword = "123";

            X509Certificate2 clientCertificate = new X509Certificate2(certificatePath, certificatePassword, X509KeyStorageFlags.MachineKeySet);
            X509Certificate2Collection certificatesCollection = new X509Certificate2Collection(clientCertificate);

            TcpClient client = new TcpClient(hostname, port);
            SslStream sslStream = new SslStream(
                            client.GetStream(),
                            false,
                            new RemoteCertificateValidationCallback(ValidateServerCertificate),
                            null
            );

            try
            {
                sslStream.AuthenticateAsClient(hostname, certificatesCollection, SslProtocols.Default, false);
            }
            catch (AuthenticationException ex)
            {
                Console.WriteLine("Authentication failed");
                client.Close();
                HttpContext.Current.Request.SaveAs(HttpContext.Current.Server.MapPath("Authenticationfailed.txt"), true);
                return;
            }


            //// Encode a test message into a byte array.
            MemoryStream memoryStream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memoryStream);

            writer.Write((byte)0);  //The command
            writer.Write((byte)0);  //The first byte of the deviceId length (big-endian first byte)
            writer.Write((byte)32); //The deviceId length (big-endian second byte)

            byte[] b0 = HexString2Bytes(devicetocken);
            WriteMultiLineByteArray(b0);

            writer.Write(b0);
            String payload;
            string strmsgbody = "";
            int totunreadmsg = 20;
            strmsgbody = message;

            Debug.WriteLine("during testing via device!");
            HttpContext.Current.Request.SaveAs(HttpContext.Current.Server.MapPath("APNSduringdevice.txt"), true);

            payload = "{\"aps\":{\"alert\":\"" + strmsgbody + "\",\"badge\":" + totunreadmsg.ToString() + ",\"sound\":\"mailsent.wav\"},\"acme1\":\"bar\",\"acme2\":42}";

            writer.Write((byte)0); //First byte of payload length; (big-endian first byte)
            writer.Write((byte)payload.Length);     //payload length (big-endian second byte)

            byte[] b1 = System.Text.Encoding.UTF8.GetBytes(payload);
            writer.Write(b1);
            writer.Flush();

            byte[] array = memoryStream.ToArray();
            Debug.WriteLine("This is being sent...\n\n");
            Debug.WriteLine(array);
            try
            {
                sslStream.Write(array);
                sslStream.Flush();
            }
            catch
            {
                Debug.WriteLine("Write failed buddy!!");
                HttpContext.Current.Request.SaveAs(HttpContext.Current.Server.MapPath("Writefailed.txt"), true);
            }

            client.Close();
            Debug.WriteLine("Client closed.");
            HttpContext.Current.Request.SaveAs(HttpContext.Current.Server.MapPath("APNSSuccess.txt"), true);
        }

        private static void PostToAndroid(string message)
        {
            string regId = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
            //API Key created in Google project
            var applicationID = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

            //Project ID created in Google project.
            var SENDER_ID = "xxxxxxxxxxxx";
            var varMessage = message;
            WebRequest tRequest;
            tRequest = WebRequest.Create("https://android.googleapis.com/gcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

            tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

            string postDataToServer = "collapse_key=score_update&time_to_live=108&delay_while_idle=1&data.message="
                               + varMessage + "&data.time=" + System.DateTime.Now.ToString() + "®istration_id=" + regId + "";


            Console.WriteLine(postDataToServer);
            Byte[] byteArray = Encoding.UTF8.GetBytes(postDataToServer);
            tRequest.ContentLength = byteArray.Length;

            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse tResponse = tRequest.GetResponse();

            dataStream = tResponse.GetResponseStream();

            StreamReader tReader = new StreamReader(dataStream);

            String sResponseFromServer = tReader.ReadToEnd();

            tReader.Close();
            dataStream.Close();
            tResponse.Close();
        }

        private static byte[] HexString2Bytes(string hexString)
        {
            //check for null
            if (hexString == null) return null;
            //get length
            int len = hexString.Length;
            if (len % 2 == 1) return null;
            int len_half = len / 2;
            //create a byte array
            byte[] bs = new byte[len_half];
            try
            {
                //convert the hexstring to bytes
                for (int i = 0; i != len_half; i++)
                {
                    bs[i] = (byte)Int32.Parse(hexString.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Exception : " + ex.Message);
            }
            //return the byte array
            return bs;
        }
        // The following method is invoked by the RemoteCertificateValidationDelegate.
        public static bool ValidateServerCertificate(
              object sender,
              X509Certificate certificate,
              X509Chain chain,
              SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            Console.WriteLine("Certificate error: {0}", sslPolicyErrors);

            // Do not allow this client to communicate with unauthenticated servers.
            return false;
        }
        public static void WriteMultiLineByteArray(byte[] bytes)
        {
            const int rowSize = 20;
            int iter;

            Console.WriteLine("initial byte array");
            Console.WriteLine("------------------");

            for (iter = 0; iter < bytes.Length - rowSize; iter += rowSize)
            {
                Console.Write(
                    BitConverter.ToString(bytes, iter, rowSize));
                Console.WriteLine("-");
            }

            Console.WriteLine(BitConverter.ToString(bytes, iter));
            Console.WriteLine();
        }

        public static PushNotificationFormatDom GetPushNotificationMessageByPushNotificationTypeId(int pushNotificationTypeId)
        {
            PushNotificationFormatDom pushNotificationMessage = null;

            using (var db = new BATransferEntities())
            {
                var relevantPushNotificationConfig = db.PushNotificationTypeEnums.FirstOrDefault(pne => pne.PushNotificationTypeEnumId == pushNotificationTypeId);

                if (relevantPushNotificationConfig != null)
                {
                    pushNotificationMessage = new PushNotificationFormatDom()
                    {
                        Title = relevantPushNotificationConfig.Title,
                        Message = relevantPushNotificationConfig.Message,
                        IsAppendBookingNumberForMessage = relevantPushNotificationConfig.IsAppendBookingNumberForMessage,
                        IsAppendBookingNumberForTitle = relevantPushNotificationConfig.IsAppendBookingNumberForTitle
                    };
                }
            }

            return pushNotificationMessage;
        }

        public static string PushMessage(string serialisedData, string apiUrl, out HttpWebResponse webResponse)
        {

            string url = apiUrl;

            var request = (HttpWebRequest)WebRequest.Create(url);

            var data = Encoding.ASCII.GetBytes(serialisedData);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            webResponse = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();

            return responseString;
            
        }

        public class PushNotificationFormatDom
        {
            public string Title { get; set; }
            public string Message { get; set; }
            public bool IsAppendBookingNumberForMessage { get; set; }
            public bool IsAppendBookingNumberForTitle { get; set; }
        }
    }
}