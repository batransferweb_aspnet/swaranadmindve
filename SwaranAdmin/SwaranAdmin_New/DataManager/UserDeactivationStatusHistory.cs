//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class UserDeactivationStatusHistory
    {
        public long UserDeactivationStatusHistoryId { get; set; }
        public string UserId { get; set; }
        public string RequestToken { get; set; }
        public bool IsActivation { get; set; }
        public int DeactivationType { get; set; }
        public string Reason { get; set; }
        public int UserRole { get; set; }
        public bool IsSuccess { get; set; }
        public System.DateTime sysModifiedDate { get; set; }
        public System.DateTime sysCreatedDate { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
    }
}
