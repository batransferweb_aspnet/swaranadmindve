//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class Organization
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Organization()
        {
            this.AppVersionConfigurations = new HashSet<AppVersionConfiguration>();
            this.BookingSourceEnums = new HashSet<BookingSourceEnum>();
            this.LoyaltyMemberRateConfigurations = new HashSet<LoyaltyMemberRateConfiguration>();
            this.CustomerSites = new HashSet<CustomerSite>();
            this.NotificationOrganizations = new HashSet<NotificationOrganization>();
            this.OrganizationCardPriceConfigurations = new HashSet<OrganizationCardPriceConfiguration>();
            this.OrganizationContacts = new HashSet<OrganizationContact>();
            this.OrganizationPaymentBookingCharges = new HashSet<OrganizationPaymentBookingCharge>();
            this.OrganizationReviewSites = new HashSet<OrganizationReviewSite>();
        }
    
        public int OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public string Description { get; set; }
        public string LogoUrl { get; set; }
        public string Website { get; set; }
        public string WebsiteText { get; set; }
        public string DisplayText { get; set; }
        public string Location { get; set; }
        public decimal MemberDiscount { get; set; }
        public bool IsEnable { get; set; }
        public bool IsDeleted { get; set; }
        public string FacebookToken { get; set; }
        public string TwitterToken { get; set; }
        public string ReCAPTCHA { get; set; }
        public decimal JourneyDiscount { get; set; }
        public decimal CardProcessingFee { get; set; }
        public string SummaryUrl { get; set; }
        public decimal StudentDiscount { get; set; }
        public bool IsCustomerCreditEnabled { get; set; }
        public bool IsSendToGhost { get; set; }
        public string OfZPhoneNo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppVersionConfiguration> AppVersionConfigurations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BookingSourceEnum> BookingSourceEnums { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LoyaltyMemberRateConfiguration> LoyaltyMemberRateConfigurations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerSite> CustomerSites { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NotificationOrganization> NotificationOrganizations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrganizationCardPriceConfiguration> OrganizationCardPriceConfigurations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrganizationContact> OrganizationContacts { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrganizationPaymentBookingCharge> OrganizationPaymentBookingCharges { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrganizationReviewSite> OrganizationReviewSites { get; set; }
    }
}
