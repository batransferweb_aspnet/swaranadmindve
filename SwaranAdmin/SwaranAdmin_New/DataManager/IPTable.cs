//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class IPTable
    {
        public long IPID { get; set; }
        public string HostAddress { get; set; }
        public string UserAgent { get; set; }
        public string UserHostName { get; set; }
        public string Url { get; set; }
        public string QueryString { get; set; }
        public string Path { get; set; }
        public Nullable<bool> IsSecureConnection { get; set; }
        public Nullable<bool> IsAuthenticated { get; set; }
        public string ContentType { get; set; }
        public string Browser { get; set; }
        public string ApplicationPath { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }
}
