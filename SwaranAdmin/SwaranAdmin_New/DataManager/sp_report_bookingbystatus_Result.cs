//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    
    public partial class sp_report_bookingbystatus_Result
    {
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> Finished { get; set; }
        public Nullable<int> Cancelled { get; set; }
        public Nullable<int> Confirmed { get; set; }
    }
}
