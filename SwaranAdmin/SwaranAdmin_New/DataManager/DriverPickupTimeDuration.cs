//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class DriverPickupTimeDuration
    {
        public long DriverPickupTimeDurationId { get; set; }
        public int PassportTypeEnumId { get; set; }
        public int JourneyClassEnumId { get; set; }
        public bool IsSinglePassenger { get; set; }
        public bool IsWithLuggage { get; set; }
        public int TimeDuration { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime sysModifiedDate { get; set; }
        public System.DateTime sysCreatedDate { get; set; }
    
        public virtual JourneyClassEnum JourneyClassEnum { get; set; }
        public virtual PassportTypeEnum PassportTypeEnum { get; set; }
    }
}
