//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class BookingVehicletypeDriver
    {
        public long BookingVehicletypeDriverId { get; set; }
        public long BookingVehicleTypeId { get; set; }
        public Nullable<long> RequestedDriverId { get; set; }
        public Nullable<long> AssignDriverId { get; set; }
        public int Status { get; set; }
    
        public virtual DriverProfile DriverProfile { get; set; }
        public virtual DriverProfile DriverProfile1 { get; set; }
        public virtual BookingVehicleType BookingVehicleType { get; set; }
    }
}
