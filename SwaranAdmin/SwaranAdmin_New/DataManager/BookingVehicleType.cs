//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class BookingVehicleType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BookingVehicleType()
        {
            this.BookingVehicletypeDrivers = new HashSet<BookingVehicletypeDriver>();
            this.Journeys = new HashSet<Journey>();
        }
    
        public long BookingVehicleTypeId { get; set; }
        public long BookingId { get; set; }
        public long VehicleTypeId { get; set; }
        public decimal DistancePrice { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public bool IsDeleted { get; set; }
        public Nullable<System.DateTime> DeletedDate { get; set; }
        public int CustomerRating { get; set; }
        public string CustomerComment { get; set; }
        public int DriverRating { get; set; }
        public string DriverComment { get; set; }
    
        public virtual Booking Booking { get; set; }
        public virtual VehicleType VehicleType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BookingVehicletypeDriver> BookingVehicletypeDrivers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Journey> Journeys { get; set; }
    }
}
