//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class Rate_SpecialOccasion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Rate_SpecialOccasion()
        {
            this.SpecialOccasionAreaPoints = new HashSet<SpecialOccasionAreaPoint>();
        }
    
        public long OccasionsId { get; set; }
        public long CustomerRateId { get; set; }
        public string RateFormula { get; set; }
        public string Title { get; set; }
        public System.DateTime FromDate { get; set; }
        public System.DateTime ToDate { get; set; }
        public string Comments { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public decimal Surcharge { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public string DropOffComments { get; set; }
        public decimal DropOffCharge { get; set; }
        public bool IsAllPlot { get; set; }
        public bool IsBothAreapointApplied { get; set; }
    
        public virtual Rate_Customer Rate_Customer { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SpecialOccasionAreaPoint> SpecialOccasionAreaPoints { get; set; }
    }
}
