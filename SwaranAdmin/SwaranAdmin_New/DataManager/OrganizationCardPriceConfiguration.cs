//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class OrganizationCardPriceConfiguration
    {
        public int OrganizationCardPriceConfigurationId { get; set; }
        public int OrganizationId { get; set; }
        public int ChargeType { get; set; }
        public decimal Value { get; set; }
        public string Comments { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
    
        public virtual Organization Organization { get; set; }
    }
}
