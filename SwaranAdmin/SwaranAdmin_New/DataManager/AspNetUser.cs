//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class AspNetUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AspNetUser()
        {
            this.Booking_TEMP = new HashSet<Booking_TEMP>();
            this.UserDeactivationStatusHistories = new HashSet<UserDeactivationStatusHistory>();
            this.ContactPersons = new HashSet<ContactPerson>();
            this.AspNetUserClaims = new HashSet<AspNetUserClaim>();
            this.AspNetUserLogins = new HashSet<AspNetUserLogin>();
            this.BlockMemberBookings = new HashSet<BlockMemberBooking>();
            this.Bookings = new HashSet<Booking>();
            this.BookingOperations = new HashSet<BookingOperation>();
            this.CompanyFreeJourneys = new HashSet<CompanyFreeJourney>();
            this.CreditRegistries = new HashSet<CreditRegistry>();
            this.Customers = new HashSet<Customer>();
            this.CustomerCards = new HashSet<CustomerCard>();
            this.CustomerFeedbacks = new HashSet<CustomerFeedback>();
            this.CustomerFeedbackDriverEmails = new HashSet<CustomerFeedbackDriverEmail>();
            this.CustomerFeedbackResponseEmails = new HashSet<CustomerFeedbackResponseEmail>();
            this.CustomerFreeJourneys = new HashSet<CustomerFreeJourney>();
            this.CustomerSites = new HashSet<CustomerSite>();
            this.CustomerSpecialPreferences = new HashSet<CustomerSpecialPreference>();
            this.CustomerSpecialPreferences1 = new HashSet<CustomerSpecialPreference>();
            this.DriverFeedbacks = new HashSet<DriverFeedback>();
            this.DriverProfiles = new HashSet<DriverProfile>();
            this.FavouriteAddresses = new HashSet<FavouriteAddress>();
            this.IpRestrictions = new HashSet<IpRestriction>();
            this.MemberLogins = new HashSet<MemberLogin>();
            this.MemberLogins1 = new HashSet<MemberLogin>();
            this.MemberLoginHistories = new HashSet<MemberLoginHistory>();
            this.LoginHistories = new HashSet<LoginHistory>();
            this.MemberLoginHistories1 = new HashSet<MemberLoginHistory>();
            this.LostProperties = new HashSet<LostProperty>();
            this.PaymentRefunds = new HashSet<PaymentRefund>();
            this.StaffFeedbackResponseEmails = new HashSet<StaffFeedbackResponseEmail>();
            this.StaffLogins = new HashSet<StaffLogin>();
            this.StaffLoginHistories = new HashSet<StaffLoginHistory>();
            this.StaffProfiles = new HashSet<StaffProfile>();
            this.SystemComplaints = new HashSet<SystemComplaint>();
            this.TempUserAddresses = new HashSet<TempUserAddress>();
            this.AspNetRoles = new HashSet<AspNetRole>();
        }
    
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Gender { get; set; }
        public string LandPhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string PrimaryPhoneNumber { get; set; }
        public bool IsSendSms { get; set; }
        public bool IsCompanyUser { get; set; }
        public Nullable<long> CompanyId { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime sysModifiedDate { get; set; }
        public System.DateTime sysCreatedDate { get; set; }
        public bool isUserSuspended { get; set; }
        public string reason { get; set; }
        public bool isUserValidated { get; set; }
        public string resetPasswordToken { get; set; }
        public System.DateTime resetPasswordTokenCreatedDate { get; set; }
        public int resetPasswordTokenValidTime { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public Nullable<System.DateTime> LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }
        public string countryCode { get; set; }
        public bool isFacebookLogin { get; set; }
        public string facebookToken { get; set; }
        public bool isAuthorized { get; set; }
        public string verificationCode { get; set; }
        public bool IsEmailVerified { get; set; }
        public bool IsPhoneVerified { get; set; }
        public string EmailVerificationToken { get; set; }
        public string PhoneVerificationToken { get; set; }
        public System.DateTime EmailVerificationTokenCreatedDate { get; set; }
        public System.DateTime PhoneVerificationTokenCreatedDate { get; set; }
        public bool IsIpRestricted { get; set; }
        public bool IsIpDeniedRestriction { get; set; }
        public long MemberId { get; set; }
        public Nullable<int> TitleId { get; set; }
        public string LandPhoneCountryCode { get; set; }
        public string MobileNumberCountryCode { get; set; }
        public bool IsMobileNumberPrimary { get; set; }
        public bool IsDriver { get; set; }
        public bool IsStaff { get; set; }
        public bool IsCustomer { get; set; }
        public string NationalIdentity { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string PostalCode { get; set; }
        public string Street { get; set; }
        public string Town { get; set; }
        public Nullable<long> CountryId { get; set; }
        public string MobilePasswordResetCode { get; set; }
        public bool IsPasswordResetRequired { get; set; }
        public bool IsCompanyAdmin { get; set; }
        public bool IsTempPassword { get; set; }
        public bool IsStaffAccountVerified { get; set; }
        public bool IsDriverAccountVerified { get; set; }
        public string TempMobileNumber { get; set; }
        public string TempMobileNumberCountryCode { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Booking_TEMP> Booking_TEMP { get; set; }
        public virtual Title Title { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserDeactivationStatusHistory> UserDeactivationStatusHistories { get; set; }
        public virtual CompanyProfile CompanyProfile { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ContactPerson> ContactPersons { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AspNetUserClaim> AspNetUserClaims { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AspNetUserLogin> AspNetUserLogins { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BlockMemberBooking> BlockMemberBookings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Booking> Bookings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BookingOperation> BookingOperations { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CompanyFreeJourney> CompanyFreeJourneys { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CreditRegistry> CreditRegistries { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Customer> Customers { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerCard> CustomerCards { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerFeedback> CustomerFeedbacks { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerFeedbackDriverEmail> CustomerFeedbackDriverEmails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerFeedbackResponseEmail> CustomerFeedbackResponseEmails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerFreeJourney> CustomerFreeJourneys { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerSite> CustomerSites { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerSpecialPreference> CustomerSpecialPreferences { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerSpecialPreference> CustomerSpecialPreferences1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DriverFeedback> DriverFeedbacks { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DriverProfile> DriverProfiles { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FavouriteAddress> FavouriteAddresses { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<IpRestriction> IpRestrictions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MemberLogin> MemberLogins { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MemberLogin> MemberLogins1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MemberLoginHistory> MemberLoginHistories { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LoginHistory> LoginHistories { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MemberLoginHistory> MemberLoginHistories1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LostProperty> LostProperties { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PaymentRefund> PaymentRefunds { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StaffFeedbackResponseEmail> StaffFeedbackResponseEmails { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StaffLogin> StaffLogins { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StaffLoginHistory> StaffLoginHistories { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StaffProfile> StaffProfiles { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SystemComplaint> SystemComplaints { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TempUserAddress> TempUserAddresses { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AspNetRole> AspNetRoles { get; set; }
    }
}
