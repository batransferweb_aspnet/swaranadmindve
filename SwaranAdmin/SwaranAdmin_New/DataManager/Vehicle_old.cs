//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class Vehicle_old
    {
        public long AllocateDriverid { get; set; }
        public long VehicleTypeId { get; set; }
        public string RegistrationNumber { get; set; }
        public string VehicleUsage { get; set; }
        public string Class { get; set; }
        public string ImageURL { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string ChildSeatType { get; set; }
        public string ChildSeatCount { get; set; }
        public bool IsDriverOwnedVehicle { get; set; }
        public bool IsCompanyVehicle { get; set; }
        public bool IsCurrentUser { get; set; }
        public string OwnersName { get; set; }
        public string OwnerAddHouseNumber { get; set; }
        public string OwnerAddStreetName { get; set; }
        public string OwnerAddTown { get; set; }
        public string OwnerAddFullPostcode { get; set; }
        public string OwnerAddCountry { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsLoged { get; set; }
        public bool IsDisabled { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string BodyStyle { get; set; }
        public bool IsTextiOnly { get; set; }
        public bool IsCourierOnly { get; set; }
        public long VehicleID { get; set; }
    }
}
