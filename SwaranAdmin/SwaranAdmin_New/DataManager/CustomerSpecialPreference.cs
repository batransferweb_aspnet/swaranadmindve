//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomerSpecialPreference
    {
        public long CustomerSpecialPreferenceId { get; set; }
        public Nullable<long> CustomerFeedbackId { get; set; }
        public Nullable<long> AddressId { get; set; }
        public string Note { get; set; }
        public string CustomerUserId { get; set; }
        public string StaffUserId { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public System.DateTime sysModifiedDate { get; set; }
        public System.DateTime sysCreatedDate { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual AspNetUser AspNetUser1 { get; set; }
        public virtual CustomerFeedback CustomerFeedback { get; set; }
    }
}
