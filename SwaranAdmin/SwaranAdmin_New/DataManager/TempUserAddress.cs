//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class TempUserAddress
    {
        public long TempUserAddressId { get; set; }
        public Nullable<long> FavouriteAddressId { get; set; }
        public string UserId { get; set; }
        public int AreaTypeId { get; set; }
        public string Label { get; set; }
        public string FullAddress { get; set; }
        public string OrganisationName { get; set; }
        public string DepartmentName { get; set; }
        public string BuildingName { get; set; }
        public string SubBuildingName { get; set; }
        public Nullable<int> BuildingNumber { get; set; }
        public string Street { get; set; }
        public string PostTown { get; set; }
        public string Postcode { get; set; }
        public string CountryCode { get; set; }
        public decimal Lat { get; set; }
        public decimal Long { get; set; }
        public bool isEnabled { get; set; }
        public bool isDeleted { get; set; }
        public System.DateTime sysModifiedDate { get; set; }
        public System.DateTime sysCreatedDate { get; set; }
        public int NumberOfJourneys { get; set; }
        public string Note { get; set; }
        public bool IsVerified { get; set; }
    
        public virtual AspNetUser AspNetUser { get; set; }
        public virtual FavouriteAddress FavouriteAddress { get; set; }
    }
}
