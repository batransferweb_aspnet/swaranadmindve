//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    
    public partial class sp_report_bookingbysite_datecreated_Result
    {
        public Nullable<System.DateTime> Date { get; set; }
        public Nullable<int> BA_Web { get; set; }
        public Nullable<int> AO_Web { get; set; }
        public Nullable<int> ET_Web { get; set; }
        public Nullable<int> WT_Web { get; set; }
        public Nullable<int> BAT_Web { get; set; }
        public Nullable<int> BAL_Web { get; set; }
        public Nullable<int> BAC_Web { get; set; }
        public Nullable<int> BA_iOS { get; set; }
        public Nullable<int> BA_Android { get; set; }
    }
}
