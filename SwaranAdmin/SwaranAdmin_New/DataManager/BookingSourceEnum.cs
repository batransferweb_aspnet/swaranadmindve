//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class BookingSourceEnum
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BookingSourceEnum()
        {
            this.BookingSourcePaymentDiscounts = new HashSet<BookingSourcePaymentDiscount>();
            this.CustomerFeedbacks = new HashSet<CustomerFeedback>();
            this.CustomerSites = new HashSet<CustomerSite>();
            this.DriverFeedbacks = new HashSet<DriverFeedback>();
            this.LostProperties = new HashSet<LostProperty>();
        }
    
        public int BookingSourceEnumId { get; set; }
        public string BookingSourceEnumName { get; set; }
        public string Description { get; set; }
        public string SuccessUrl { get; set; }
        public string FailedUrl { get; set; }
        public int OrganizationId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BookingSourcePaymentDiscount> BookingSourcePaymentDiscounts { get; set; }
        public virtual Organization Organization { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerFeedback> CustomerFeedbacks { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerSite> CustomerSites { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DriverFeedback> DriverFeedbacks { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LostProperty> LostProperties { get; set; }
    }
}
