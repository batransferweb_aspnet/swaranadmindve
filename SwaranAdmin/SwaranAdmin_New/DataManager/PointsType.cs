//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class PointsType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PointsType()
        {
            this.BookingPoints_TEMP = new HashSet<BookingPoints_TEMP>();
            this.BookingPoints = new HashSet<BookingPoint>();
        }
    
        public int PointsTypeId { get; set; }
        public string PointsTypeName { get; set; }
        public int Points { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsEnabled { get; set; }
        public Nullable<System.DateTime> CancelledDate { get; set; }
        public System.DateTime sysModifiedDate { get; set; }
        public System.DateTime sysCreatedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BookingPoints_TEMP> BookingPoints_TEMP { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BookingPoint> BookingPoints { get; set; }
    }
}
