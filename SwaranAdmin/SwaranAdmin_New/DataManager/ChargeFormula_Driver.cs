//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class ChargeFormula_Driver
    {
        public long FormulaID { get; set; }
        public string FormulaName { get; set; }
        public string Formula { get; set; }
        public string FormulaColumnIds { get; set; }
        public string FormulaFunction { get; set; }
        public string FormulaType { get; set; }
        public string RateTablesName { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
