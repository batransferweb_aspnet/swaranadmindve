//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class DefaultCustomerProfile
    {
        public string postalCode { get; set; }
        public string street { get; set; }
        public string fax { get; set; }
        public bool isEnabled { get; set; }
        public bool isDeleted { get; set; }
        public long totalJourneyTime { get; set; }
        public decimal totalJourneyDistance { get; set; }
        public int totalPoints { get; set; }
        public long totalNumberOfJourneys { get; set; }
        public int availablePoints { get; set; }
        public long availableCashBalance { get; set; }
        public decimal availableDistance { get; set; }
        public long availableTime { get; set; }
        public string promoCode { get; set; }
        public bool isFreeJourneyAvailable { get; set; }
        public int loyaltyMemberType { get; set; }
        public bool isSubscriptionEnabled { get; set; }
        public bool isMarketingEmailEnabled { get; set; }
        public string town { get; set; }
        public long countryID { get; set; }
        public long tempId { get; set; }
    }
}
