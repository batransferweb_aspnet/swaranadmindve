//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class DeviceTypeEnum
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DeviceTypeEnum()
        {
            this.NotificationDeviceTypes = new HashSet<NotificationDeviceType>();
        }
    
        public int DeviceTypeEnumId { get; set; }
        public string DeviceTypeEnumName { get; set; }
        public string Description { get; set; }
        public string DisplayText { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NotificationDeviceType> NotificationDeviceTypes { get; set; }
    }
}
