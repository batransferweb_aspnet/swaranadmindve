//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    
    public partial class DriverBookingSearch_Result
    {
        public long BookingId { get; set; }
        public long JourneyId { get; set; }
        public System.DateTime BookingDateTime { get; set; }
        public string VehicleTypeName { get; set; }
        public string RegistrationNumber { get; set; }
        public string MapUrl { get; set; }
        public Nullable<decimal> Total { get; set; }
    }
}
