//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class Setting
    {
        public long AppSettingId { get; set; }
        public long DriverId { get; set; }
        public bool IsToAddessVisible { get; set; }
        public bool IsFromAddressVisible { get; set; }
        public bool IsAcceptButtonVisible { get; set; }
        public bool IsRejectButtonVisible { get; set; }
        public string CommandText { get; set; }
        public string Title { get; set; }
        public bool IsTimePopup { get; set; }
        public Nullable<int> PopupTimeOut { get; set; }
        public bool IsBookingIdVisible { get; set; }
        public bool IsAccountTypeVisible { get; set; }
        public bool IsNumberOfPassengersVisible { get; set; }
        public bool IsPassengerNameVisible { get; set; }
        public bool IsPassengerTelVisible { get; set; }
        public bool IsPassengerEmailVisible { get; set; }
        public bool IsBookedPersonNameVisible { get; set; }
        public bool IsBookedPersonTelVisible { get; set; }
        public Nullable<decimal> ArrivedDistance { get; set; }
        public Nullable<decimal> SoonToClearDistance { get; set; }
        public int SpeedCalculationTime { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        public virtual DriverProfile DriverProfile { get; set; }
    }
}
