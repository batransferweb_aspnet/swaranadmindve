//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class Rate_Driver
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Rate_Driver()
        {
            this.DriverProfiles = new HashSet<DriverProfile>();
            this.PointOfInterest_WaitingCharge_DropOff_Driver = new HashSet<PointOfInterest_WaitingCharge_DropOff_Driver>();
            this.PointOfInterest_WaitingCharge_Pickup_Driver = new HashSet<PointOfInterest_WaitingCharge_Pickup_Driver>();
            this.Rate_Driver_VehicleType = new HashSet<Rate_Driver_VehicleType>();
        }
    
        public long DriverRateId { get; set; }
        public string DriverRateName { get; set; }
        public decimal OverallCom { get; set; }
        public decimal PickupWaitingCom { get; set; }
        public decimal PickupPakingCom { get; set; }
        public decimal DropoffWaitingCom { get; set; }
        public decimal DropoffParkingCom { get; set; }
        public bool IsDefault { get; set; }
        public bool IsDelete { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DriverProfile> DriverProfiles { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PointOfInterest_WaitingCharge_DropOff_Driver> PointOfInterest_WaitingCharge_DropOff_Driver { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PointOfInterest_WaitingCharge_Pickup_Driver> PointOfInterest_WaitingCharge_Pickup_Driver { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rate_Driver_VehicleType> Rate_Driver_VehicleType { get; set; }
    }
}
