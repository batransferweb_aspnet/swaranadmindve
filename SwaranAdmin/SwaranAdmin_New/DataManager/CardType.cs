//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class CardType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CardType()
        {
            this.Booking_TEMP = new HashSet<Booking_TEMP>();
            this.Bookings = new HashSet<Booking>();
            this.BookingPaymentCards = new HashSet<BookingPaymentCard>();
            this.BookingPaymentCard_TEMP = new HashSet<BookingPaymentCard_TEMP>();
        }
    
        public long CardId { get; set; }
        public string Name { get; set; }
        public int ChargeType { get; set; }
        public decimal Value { get; set; }
        public string Comments { get; set; }
        public string ImageUrl { get; set; }
        public bool IsDelete { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Booking_TEMP> Booking_TEMP { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Booking> Bookings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BookingPaymentCard> BookingPaymentCards { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BookingPaymentCard_TEMP> BookingPaymentCard_TEMP { get; set; }
    }
}
