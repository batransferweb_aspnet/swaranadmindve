//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    
    public partial class SearchDriver_Result
    {
        public long DriverID { get; set; }
        public string CallSign { get; set; }
        public string FirstName { get; set; }
        public string DriverFullName { get; set; }
        public string HomeAddHousrNumber { get; set; }
        public string HomeAddStreetName { get; set; }
        public string HomeAddTown { get; set; }
        public string HomeAddFullPostCode { get; set; }
        public string HomeAddCountry { get; set; }
        public string TaxiBadgeNumber { get; set; }
        public Nullable<System.DateTime> DateOfIssue { get; set; }
        public Nullable<System.DateTime> DateOfExpiry { get; set; }
    }
}
