//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SwaranAdmin_New.DataManager
{
    using System;
    using System.Collections.Generic;
    
    public partial class EmissionZone
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EmissionZone()
        {
            this.EmissionZoneDays = new HashSet<EmissionZoneDay>();
        }
    
        public long ZoneId { get; set; }
        public long AddressId { get; set; }
        public string Title { get; set; }
        public string StreetName { get; set; }
        public string Postcode { get; set; }
        public bool IsEnabeld { get; set; }
        public int EmissionType { get; set; }
        public string Description { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public System.DateTime CreatedDate { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EmissionZoneDay> EmissionZoneDays { get; set; }
    }
}
