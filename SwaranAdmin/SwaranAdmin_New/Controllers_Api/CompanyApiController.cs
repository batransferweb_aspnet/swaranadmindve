﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Routing;
using SwaranAdmin_New.DataAccess.Company;

namespace SwaranAdmin_New.Controllers_Api
{
    [RoutePrefix("api/company")]
    public class CompanyApiController : ApiController
    {
        private CompanyProfileDal company = CompanyProfileDal.Instance;

        [HttpGet]
        [Route("companyprofiles")]
        public IHttpActionResult GetCompanyProfice()
        {
            var prof = company.GetCompanyProfiles();
            return Ok(prof);
        }

        [HttpGet]
        [Route("email")]
        public IHttpActionResult GetEmail(string userid)
        {
            var prof = company.GetEmail(userid);
            return Ok(prof);
        }

        [HttpGet]
        [Route("accountusers")]
        public IHttpActionResult GetAccountUsers()
        {
            var prof = company.GetAccountUsers();
            return Ok(prof);
        }


        [HttpGet]
        [Route("removeacccustomer")]
        public IHttpActionResult RemoveAccountUsers(long memberid)
        {
            var prof = company.RemoveAccountUsers(memberid);
            return Ok(prof);
        }

        [HttpGet]
        [Route("companylist")]
        public IHttpActionResult GetCompanyList()
        {
            var prof = company.GetCompanyList();
            return Ok(prof);
        }
        [HttpGet]
        [Route("removeaccount")]
        public IHttpActionResult Removeaccount(long companyId) 
        {
            var prof = company.Removeaccount(companyId);
            return Ok(prof);
        }

        

    }
}
