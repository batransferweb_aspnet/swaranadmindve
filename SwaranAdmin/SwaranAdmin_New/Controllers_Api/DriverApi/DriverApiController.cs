﻿using SwaranAdmin_New.DataAccess.DriverDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SwaranAdmin_New.Controllers_Api.DriverApi
{
    [RoutePrefix("api/driverapplications")]
    public class DriverApiController : ApiController
    {
        DriverApplicationDal rexDriverApp = DriverApplicationDal.Instance;
        [HttpGet]
        [Route("driverjobs")]
        public IHttpActionResult GetDriverJobjs(int status)
        {
           var objDriverApp=  rexDriverApp.GetDriverJobjs(status);
            return Ok(objDriverApp);
        }


        [HttpGet]
        [Route("changedriverjob")]
        public IHttpActionResult DeleteDriverJob(int id) 
        {
            var status = rexDriverApp.DeleteDriverJob(id);
            return Ok(status);
        }

        [HttpGet]
        [Route("changedriverjobstatus")]
        public IHttpActionResult  ChaneDriverJobStatus(long appid,int statusid) 
        {
            var status = rexDriverApp.ChaneDriverJobStatus(appid, statusid);
            return Ok(status);
        }
    }
}
