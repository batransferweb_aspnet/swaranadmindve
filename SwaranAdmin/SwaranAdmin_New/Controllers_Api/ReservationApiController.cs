﻿using Microsoft.AspNet.Identity;
using SwaranAdmin_New.DataAccess;
using SwaranAdmin_New.Models;
using System.Web.Http;
using System.Web.Routing;
using SwaranAdmin_New.Autocab;

namespace SwaranAdmin_New.Controllers_Api
{
    [RoutePrefix("api/reservation")]
    public class ReservationApiController : ApiController
    {
        private ReservationDal reservation = ReservationDal.Instance;
        private GhostDal _ghost = GhostDal.Instance;
       [HttpGet]
        [Route("bookinghistory")]
        public IHttpActionResult GetResrvation(string userid)
        {
            var resrvation = reservation.GetResrvation(userid);
            return Ok(resrvation);
        }

        [HttpGet]
        [Route("resrvationstatus")]
        public IHttpActionResult GetResrvationByStatus(int bookingStatus)
        {
            //public List<ReservationHistoryViewModel> GetResrvationByStatus(int paymentStatus,
            //  DateTime bookingDate, int bookingStatus, int companyId, int loyaltytype, int orizinationid)
            var resrvationstatus = reservation.GetResrvationByStatus(bookingStatus);
            return Ok(resrvationstatus);
        }
        [HttpGet]
        [Route("resrvationstatusbyuser")]
        public IHttpActionResult GetUserResrvationByStatus(string userid, int bookingStatus)
        {
            
            var resrvationstatus = reservation.GetUserResrvationByStatus(userid,  bookingStatus);
            return Ok(resrvationstatus);
        }
       


        [HttpPost]
        [Route("monthlybooking")]
        public IHttpActionResult GetMonthlyBooking(ReservationDateRange range)
        {
            var monthlyBooking = reservation.GetMonthlyBooking(range);
            return Ok(monthlyBooking);
        }

        [HttpPost]
        [Route("cancelbooking")]
        public IHttpActionResult CancelBooking(CancelBookingViewModel cancelBooking)
        {

            string userId = User.Identity.GetUserId();
            var data = reservation.CancelBooking(cancelBooking, userId);

            return Json(data);
        }

        [HttpPost]
        [Route("confirmbooking")]
        public IHttpActionResult ConfirmBooking(ConfirmBookingViewModel confirmBooking)
        {
            MainCancelBooking data=new MainCancelBooking();
            string userId = User.Identity.GetUserId();
            if (!confirmBooking.IsCancelBooking)
            {
                  data = reservation.ConfirmBooking(confirmBooking, userId);
                return Json(data);
            }
            if (confirmBooking.IsCancelBooking)
            {
                  data = reservation.ConfirmCancelBooking(confirmBooking, userId);
                var token = System.Web.HttpContext.Current.Server.UrlDecode(confirmBooking.Token);
             var authorizationResponse=   _ghost.SendToGhost(token, confirmBooking.IsRetunBooking);

            }
            return Json(data);
        }


        [HttpGet]
        [Route("changepaymentmethod")]
        public IHttpActionResult GetCurrentPaymentMethods(string token, int paymentTypeId) 
        {
            var data = reservation.GetCurrentPaymentMethods(token, paymentTypeId);

            return Json(data);
        }


        [HttpGet]
        [Route("bookingfee")]
        public IHttpActionResult GetBookingFee(int paymenttypeId, int siteid, decimal journeyAmount,string totken)
        {
            var bookingfee = reservation.GetBookingFee(paymenttypeId, siteid, journeyAmount, totken);
            return Ok(bookingfee);
        }

        [HttpGet]
        [Route("customercredits")]
        public IHttpActionResult GetCustomerCredits(string userId)
        { 
            var totCredits = reservation.GetCustomerCredits(userId);
            return Ok(totCredits);
        }

        [HttpGet]
        [Route("curentbooking")]
        public IHttpActionResult GetCurentbooking(string bookingnumber) 
        {
            var bookings = reservation.GetCurentbooking(bookingnumber); 
            return Ok(bookings);
        }

        [HttpGet]
        [Route("freejourneycode")]
        public IHttpActionResult GetFreejourneycode(string userId,string code)
        {
            var isvalid = reservation.GetFreejourneycode(userId, code);
            return Ok(isvalid);
        }

        [HttpGet]
        [Route("allbookings")]
        public IHttpActionResult GetAllBoonings(string bookingNumber)
        {
            var currentBookings = reservation.GetAllBookings( bookingNumber);
            return Ok(currentBookings);
        }

        [HttpGet]
        [Route("vehicleupdates")]
        public IHttpActionResult LoadVehicleUpdate(long currentVehicleId)
        {
            var vehicleUpgrates = reservation.LoadVehicleUpdate(currentVehicleId);
            return Ok(vehicleUpgrates);
        }


        [HttpPost]
        [Route("savevehicleupdates")]
        public IHttpActionResult SaveVehicleUpdate(SaveUpdateVehicle updateVehicle)
        {
            var data = reservation.SaveVehicleUpdate(updateVehicle);
            return Json(data);
        }

        [HttpPost]
        [Route("vehicleupdatesonline")]
        public IHttpActionResult SaveVehicleUpdatePayment(SaveUpdateVehicle updateVehicle)
        {
            var data = reservation.VehicleUpgratePayOnline(updateVehicle);
            return Json(data);
        }


        [HttpGet]
        [Route("vehicleupgradepoints")]
        public IHttpActionResult GetVehicleUpgradePoints(long currentVehicleId, long bookingVehicleUpgradeId)
        {
            var points = reservation.GetVehicleUpgradePoints(currentVehicleId, bookingVehicleUpgradeId);
            return Ok(points);
        }


        [HttpGet]
        [Route("pendingbookings")]
        public IHttpActionResult GetPendingBookings()
        {
            var count = reservation.GetPendingBookingsCount();
            return Ok(count);
        }
    }
}
