﻿using Microsoft.AspNet.Identity;
using SwaranAdmin_New.DataAccess;
using SwaranAdmin_New.Models;
using SwaranAdmin_New.Models.Member;
using System.Threading.Tasks;
using System.Web.Http;
using SwaranAdmin_New.Autocab;
using SwaranAdmin_New.Base;

namespace SwaranAdmin_New.Controllers_Api
{
    [RoutePrefix("api/member")]
    public class MemberApiController : ApiController
    {

        private MemberDal _member = MemberDal.Instance;
        private GhostDal ghost = GhostDal.Instance;
        [HttpGet]
        [Route("loyaltycustomer")]
        public IHttpActionResult GetLoyaltyCustomerSearch(string loyalty, string numberOfRecord, string currentpage)
        {
            var loyaltyCustomer = _member.GetLoyaltyCustomerSearch(loyalty, numberOfRecord, currentpage);
            return Ok(loyaltyCustomer);
        }

        [HttpGet]
        [Route("customer")]
        public IHttpActionResult CustomerSearch(string search)
        {

            var loyaltyCustomer = _member.CustomerSearch(search);
            return Ok(loyaltyCustomer);
        }

        [HttpGet]
        [Route("isblockmember")]
        public IHttpActionResult IsBlockMember(string userid) 
        {

            var isblockmember = ConfigHelper.IsBlockMember(userid);
            return Ok(isblockmember);
        }

        [HttpGet]
        [Route("currentcustomer")]
        public IHttpActionResult SearchCurrentCustomer(string userid)  
        {

            var loyaltyCustomer = _member.SearchCurrentCustomer(userid);
            return Ok(loyaltyCustomer);
        }
        
        [HttpGet]
        [Route("customerfreejourneys")]
        public IHttpActionResult CustomerAllFreejourney(string customerUserId)
        {

            var customerFreeJourneys = _member.CustomerAllFreejourney(customerUserId);
            return Ok(customerFreeJourneys);
        }
        [HttpGet]
        [Route("customercredits")]
        public IHttpActionResult Customercredits(string customerUserId)
        {

            var customerDiscount = _member.GetCustomercredits(customerUserId);
            return Ok(customerDiscount);
        }
        [HttpGet]
        [Route("customerstatment")]
        public IHttpActionResult GetCustomerStatement(string customerUserId)
        {

            var customerDiscount = _member.GetCustomerStatement(customerUserId);
            return Ok(customerDiscount);
        }


        
        [HttpPost]
        [Route("deletefreejourney")]
        public IHttpActionResult DeleteFreeJourney(FreeJourneyCancelViewModel model)
        {
            var userid = User.Identity.GetUserId();
            var delete = _member.DeleteFreeJourney(model, userid);
            return Ok(delete);
        }

        [HttpGet]
        [Route("getrefoundgrid")]
        public IHttpActionResult GetRefoundGrid(string bookingnumber)
        {
            var adminUserId = User.Identity.GetUserId();
            var delete = _member.GetRefundGrid(bookingnumber, adminUserId);
            return Ok(delete);
        }

        [HttpGet]
        [Route("getrepaymentgrid")]
        public IHttpActionResult GetRePaymentGrid(string bookingNumber)
        {
            var adminUserId = User.Identity.GetUserId();
            var delete = _member.GetRePaymentGrid(bookingNumber, adminUserId);
            return Ok(delete);
        }

        [HttpPost]
        [Route("paymentfullyrefund")]
        public async Task<IHttpActionResult> WorldpayFullyRefund(FullRefundViewModel repayment)
        {
            var adminUserId = User.Identity.GetUserId();
            var status = await _member.WorldpayFullyRefund(repayment, adminUserId);
            return Ok(status);
        }


        [HttpPost]
        [Route("paymentpartialyrefund")]
        public async Task<IHttpActionResult> WorldpayPartialyRefund(PartialyRefundViewModel repayment)
        {
            var adminUserId = User.Identity.GetUserId();
            var status = await _member.WorldpayPartialyRefund(repayment, adminUserId);
            return Ok(status);
        }

        [HttpGet]
        [Route("tellfriend")]
        public IHttpActionResult GetInfoTellFriends()
        {
            var tellfriend = _member.GetInfoTellFriends();
            return Ok(tellfriend);
        }

        [HttpGet]
        [Route("sendemail")]
        public async Task<IHttpActionResult> SendEmailTellFriends(int id)
        {
            var message = await _member.SendEmailTellFriends(id);
            return Ok(message);
        }

        [HttpGet]
        [Route("specialPreference")]
        public IHttpActionResult GetSpecialPreference(string userid)
        {

            var specialPreference = _member.GetSpecialPreference(userid);
            return Ok(specialPreference);
        }

        [HttpGet]
        [Route("customerreview")]
        public IHttpActionResult GetCustomerReview()
        {
            var review = _member.GetCustomerReview();
            return Ok(review);
        }

        [HttpGet]
        [Route("allowcustomerreview")] 
        public IHttpActionResult AllowCustomerReview(long reviewId) 
        {
            var review = _member.AllowCustomerReview(reviewId);
            return Ok(review);
        }
        [HttpGet]
        [Route("regectcustomerreview")]
        public IHttpActionResult RejectCustomerReview(long reviewId) 
        {
            var review = _member.RejectCustomerReview(reviewId);
            return Ok(review); 
        }

        [HttpGet]
        [Route("sendtoghost")]
        public IHttpActionResult SendToGhost(string token,bool isReturnBooking) 
        {
            var authorizationResponse =  ghost.SendToGhost(token, isReturnBooking);
            return Ok(authorizationResponse);
        }

        [HttpGet]
        [Route("caboperator")]
        public IHttpActionResult UpdateCabOperator(string userid, bool isCabOperator)
        {
            var updatedMessage = _member.UpdateCabOperator(userid, isCabOperator);
            return Ok(updatedMessage);
        }
    }
}
