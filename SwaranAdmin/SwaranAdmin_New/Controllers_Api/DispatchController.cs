﻿using SwaranAdmin_New.DataAccess;
using SwaranAdmin_New.Models;
using System.Web.Http;

namespace SwaranAdmin_New.Controllers_Api
{
    [RoutePrefix("api/dispatch")]
    public class DispatchController : ApiController
    {

        private DispatchDal _dispatch = DispatchDal.Instance;

        [HttpPost]
        [Route("allrecentbookings")]
        public IHttpActionResult GetBookingHistory(DispathchSearchViewModel dispathch)
        {

            var booking = _dispatch.GetRecentBookings(dispathch);
            return Ok(booking);
        }
    }
}
