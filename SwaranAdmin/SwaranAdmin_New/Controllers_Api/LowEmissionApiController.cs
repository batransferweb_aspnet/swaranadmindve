﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SwaranAdmin_New.DataAccess;
using SwaranAdmin_New.DataAccess.PriceCalculationDal;
using SwaranAdmin_New.Models;

namespace SwaranAdmin_New.Controllers_Api
{
    [RoutePrefix("api/lowemission")]
    public class LowEmissionApiController : ApiController
    {
        private SpecialOccasionDal rexSpeOcc = SpecialOccasionDal.Instance;
        private LowEmissionZoneDal lowEmission = LowEmissionZoneDal.Instance;

        [HttpGet]
        [Route("daylist")]
        public IHttpActionResult GetDayList()
        {
            //var dayList = new string[] { "All Day", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
            var dayList = new List<EmissionDayViewmodel>()
            {  new EmissionDayViewmodel() { DayId = 0,Name ="All Day" },
                new EmissionDayViewmodel() { DayId = 1,Name ="Sunday" },
                 new EmissionDayViewmodel() { DayId = 2,Name ="Monday" },
                  new EmissionDayViewmodel() { DayId = 3,Name ="Tuesday" },
                   new EmissionDayViewmodel() { DayId = 4,Name ="Wednesday" },
                    new EmissionDayViewmodel() { DayId = 5,Name ="Thursday" },
                     new EmissionDayViewmodel() { DayId = 6,Name ="Friday" },
                      new EmissionDayViewmodel() { DayId = 7,Name ="Saturday" },
                      
            };

            return Ok(dayList);
        }

        [HttpGet]
        [Route("allcustomerrates")]
        public IHttpActionResult GetAllCurrentRate()
        {
            var objRates = rexSpeOcc.GetAllCurrentRate(); 
            return Ok(objRates);
        }

        [HttpPost]
        [Route("emissonzone")]
        public IHttpActionResult SaveEmissionZones(LowEmissionZoneViewModel model) 
        {
            lowEmission.SaveEmissionZones(model);
            return Ok();
        }

        [HttpGet]
        [Route("allemissonzone")]
        public IHttpActionResult GetAllEmissionZones() 
        {
         var objEmission=  lowEmission.GetAllEmissionZones();
            return Ok(objEmission);
        }
    }
}
