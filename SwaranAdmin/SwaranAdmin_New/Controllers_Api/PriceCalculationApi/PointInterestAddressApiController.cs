﻿using SwaranAdmin_New.DataAccess.PriceCalculationDal;
using System.Web.Http;
using System.Linq;
using System.Web.Http.Results;
using SwaranAdmin_New.Models.PriceCalculation;
using Microsoft.AspNet.Identity;
using SwaranAdmin_New.Base;

namespace SwaranAdmin_New.Controllers_Api.PriceCalculationApi
{
    [RoutePrefix("api/pointofinterest")]
    public class PointInterestAddressApiController : ApiController
    {
        private PointInterestAddressDal rexPoI = PointInterestAddressDal.Instance;
        private SpecialOccasionDal rexSpeOcc = SpecialOccasionDal.Instance;
        [HttpGet]
        [Route("alladdress")]
        public IHttpActionResult GetAllPoiAddress()
        {
            var objAddress = rexPoI.GetAllPOIAddress();

            return Ok(objAddress);
        }

        [HttpGet]
        [Route("specialoccasion")]
        public IHttpActionResult GetAllSpecialOccasion()
        {
            var objAddress = rexSpeOcc.GetAllSpecialOccasion();

            return Ok(objAddress);
        }

        [HttpGet]
        [Route("allcustomerrates")]
        public IHttpActionResult GetAllCurrentRate()
        {
            var objRates = rexSpeOcc.GetAllCurrentRate();

            return Ok(objRates);
        }

        [HttpGet]
        [Route("poiaddress")]
        public IHttpActionResult GetAllPoIAddress(string address) 
        {
            var objAddress = rexSpeOcc.GetAllPoIAddress(address);  

            return Ok(objAddress);
        }

        [HttpGet]
        [Route("allpoiaddress")]
        public IHttpActionResult GetAllPoIAddress(long specialOccasionId)
        {
            var objAddress = rexSpeOcc.GetAllPoIAddress(specialOccasionId);

            return Ok(objAddress);
        }

        [HttpGet]
        [Route("relevantfixedprice")]
        public IHttpActionResult GetRelevantFixedPrices(string fromSearch, string toSearch, bool isSpecialOccasion)
        {
            var objAddress = rexSpeOcc.GetRelevantFixedPrices(fromSearch, toSearch, isSpecialOccasion);

            return Ok(objAddress);
        }

        [HttpGet]
        [Route("relevantfixedpricespecialoccasion")]
        public IHttpActionResult GetRelevantFixedPriceSpecialOccasion(long specialOccasionId)
        {
            var objAddress = rexSpeOcc.GetRelevantFixedPricesSpecialOccasion(specialOccasionId);

            return Ok(objAddress);
        }

        [HttpGet]
        [Route("relevantpointofinterest")]
        public IHttpActionResult GetRelevantPointOfInterests(string fromSearchString, string toSearchString)
        {
            var objAddress = rexSpeOcc.GetRelevantPointOfInterests(fromSearchString, toSearchString);

            return Ok(objAddress);
        }

        [HttpPost]
        [Route("savefixedprice")]
        public IHttpActionResult SaveFixedPrice(SaveFixedPriceDom saveFixedPriceRequest)
        {
            var userid = User.Identity.GetUserId();

            var objAddress = rexSpeOcc.SaveFixedPrice(saveFixedPriceRequest, userid);

            return Ok(objAddress);
        }

        [HttpPost]
        [Route("calculatefixedpricetimeanddistance")]
        public GoogleApiDistanceVal CalculateFixedPriceTimeAndDistance(FixedPriceDistanceTimeCalculationDom fixedPriceCalculationRequest)
        {
            GoogleApiDistanceVal response = new GoogleApiDistanceVal();

            var distanceDetail = BaGoogleApi.GetDistance(fixedPriceCalculationRequest.FromCoordinates, fixedPriceCalculationRequest.ToCoordinates, fixedPriceCalculationRequest.FromFullAddress, fixedPriceCalculationRequest.ToFullAddress);

            response = distanceDetail.OrderBy(ds => ds.Time).FirstOrDefault();

            //var userid = User.Identity.GetUserId();

            //var objAddress = rexSpeOcc.SaveFixedPrice(saveFixedPriceRequest, userid);

            //return Ok(objAddress);

            return response;
        }

        [HttpGet]
        [Route("dailysurgepoiaddress")]
        public IHttpActionResult GetDailySurgePoIAddress(long dailySurgeId)
        {
            var objAddress = rexSpeOcc.GetDailySurgePoIAddress(dailySurgeId);

            return Ok(objAddress);
        }

        
    }
}
