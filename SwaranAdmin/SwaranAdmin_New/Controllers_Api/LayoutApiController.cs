﻿using SwaranAdmin_New.DataAccess;
using System.Web.Http;
using System.Web.Routing;

namespace SwaranAdmin_New.Controllers_Api
{
    [RoutePrefix("api/layout")]
    public class LayoutApiController : ApiController
    {
        private LayoutDal _layout = LayoutDal.Instance;
        [HttpGet]
        [Route("common")]
        public IHttpActionResult GetLayoutInfo()
        {
            var userName = User.Identity.Name;

            var booking = _layout.GetLayoutInfo(userName);
            return Ok(booking);
        }
    }
}
