﻿using SwaranAdmin_New.DataAccess.ReportDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SwaranAdmin_New.Controllers_Api.ReportApi
{
    [RoutePrefix("api/report/booking")]
    public class BookingReportApiController : ApiController
    {
        BookingReportDal _report = BookingReportDal.Instance;
        [HttpGet]
        [Route("bookingstatus")]
        public IHttpActionResult GetBookingByStatus(string startdate,string enddate) 
        {
            var bookings = _report.GetBookingByStatus(startdate, enddate);
            return Ok(bookings);
        }

        [HttpGet]
        [Route("totalcreatedbookings")]
        public IHttpActionResult GetTotBookings(string startdate, string enddate) 
        {
            var bookings = _report.GetTotBookings(startdate, enddate);
            return Ok(bookings);
        }


        [HttpGet]
        [Route("bookingsbysite")]
        public IHttpActionResult GetBookingBySite(string startdate, string enddate)
        {
            var bookings = _report.GetBookingBySite(startdate, enddate);
            return Ok(bookings);
        }

        [HttpGet]
        [Route("bookingsbysitecreateddate")]
        public IHttpActionResult GetBookingBySiteCreatedDate(string startdate, string enddate) 
        {
            var bookings = _report.GetBookingBySiteCreatedDate(startdate, enddate);
            return Ok(bookings);
        }
        [HttpGet]
        [Route("bookingsbypaymenttype")]
        public IHttpActionResult GetBookingByPaymenttype(string startdate, string enddate) 
        {
            var bookings = _report.GetBookingByPaymenttype(startdate, enddate); 
            return Ok(bookings);
        }

        [HttpGet]
        [Route("bookingsbyplot")]
        public IHttpActionResult GetBookingByPlot(string startdate, string enddate) 
        {
            var bookings = _report.GetBookingByPlot(startdate, enddate);
            return Ok(bookings);
        }

        [HttpGet]
        [Route("customerbookingseach")]
        public IHttpActionResult GetCustomerBookingSearchese(string startdate, string enddate) 
        {
            var bookings = _report.GetCustomerBookingSearchese(startdate, enddate); 
            return Ok(bookings);
        }

    }
}
