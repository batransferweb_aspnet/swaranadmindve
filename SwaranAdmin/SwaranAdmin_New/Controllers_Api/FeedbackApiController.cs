﻿using SwaranAdmin_New.DataAccess;
using System.Web.Http;

namespace SwaranAdmin_New.Controllers_Api
{
    [RoutePrefix("api/feedback")]
    public class FeedbackApiController : ApiController
    {
        CustomerFeedbackDal _feedBack = CustomerFeedbackDal.Instance;
        [HttpGet]
        [Route("currentfeedback")]
        public IHttpActionResult GetBookingHistory(long feddbackid)
        {

            var feedBack = _feedBack.GetCurrentFeedbcak(feddbackid);
            return Ok(feedBack);
        }


        [HttpGet]
        [Route("driveremail")]
        public IHttpActionResult GetDriverEmail(string userid)
        {

            var email = _feedBack.GetDriverEmail(userid);
            return Ok(email);
        }

        [HttpGet]
        [Route("systemcomplaintpagecount")]
        public IHttpActionResult GetSystemComplaintPageCount(int numberOfRecord, int currentpage)
        {
            var pagecount = _feedBack.GetSystemComplaintPageCount(numberOfRecord, currentpage);
            return Ok(pagecount);
        }


        [HttpGet]
        [Route("customerfeedbackpagecount")]
        public IHttpActionResult GetFeedBackPageCount(int numberOfRecord, int currentpage)
        {
            var pagecount = _feedBack.GetFeedBackPageCount(numberOfRecord, currentpage);
            return Ok(pagecount);
        }

        [HttpGet]
        [Route("disablefeedback")]
        public IHttpActionResult GetHideFeedback(int feedbackid)
        {
            var pagecount = _feedBack.GetHideFeedback(feedbackid);
            return Ok(pagecount);
        }

        [HttpGet]
        [Route("disablesystemcomplaint")]
        public IHttpActionResult GetSystemComplaint(int complaintId)
        {
            var pagecount = _feedBack.GetSystemComplaint(complaintId);
            return Ok(pagecount);
        }        
    }
} 
