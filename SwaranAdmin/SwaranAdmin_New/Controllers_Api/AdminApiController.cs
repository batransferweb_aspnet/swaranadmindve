﻿using SwaranAdmin_New.DataAccess;
using SwaranAdmin_New.Models;
using System.Web.Http;
using SwaranAdmin_New.Models.Staff;

namespace SwaranAdmin_New.Controllers_Api
{
    [RoutePrefix("api/admin")]
    public class AdminApiController : ApiController
    {
        private AdminDal _member = AdminDal.Instance;

        [HttpGet]
        [Route("refundhistory")]
        public IHttpActionResult GetRefoundHistory()
        {
            var loyaltyCustomer = _member.GetRefoundHistory();
            return Ok(loyaltyCustomer);
        }

        [HttpGet]
        [Route("freejourneyconfig")]
        public IHttpActionResult GetFreejourneyConfig()
        {
            var freejourney = _member.GetFreejourneyConfig();
            return Ok(freejourney);
        }

        [HttpPost]
        [Route("savedistancebased")]
        public IHttpActionResult SaveDistanceBase(SaveDistanceBaseViewModel model)
        {
            var freejourney = _member.SaveDistanceBase(model);
            return Ok(freejourney);
        }

        [HttpPost]
        [Route("savepricebased")]
        public IHttpActionResult SavePriceBase(SavePriceBaseViewModel model)
        {
            var freejourney = _member.SavePriceBase(model);
            return Ok(freejourney);
        }

        [HttpPost]
        [Route("savevehiclebased")]
        public IHttpActionResult SaveVehicleBased(SaveVehiclebasedViewModel model)
        {
            var freejourney = _member.SaveVehicleBased(model);
            return Ok(freejourney);
        }

        [HttpPost]
        [Route("savejourneytypebased")]
        public IHttpActionResult SaveJourneyTypeBased(SaveJourneyTypeBasedViewModel model)
        {
            var freejourney = _member.SaveJourneyTypeBased(model);
            return Ok(freejourney);
        }

        [HttpGet]
        [Route("loadPendingStaff")]
        public IHttpActionResult GetPendingStaff()
        {
            var pendingStaff = _member.GetPendingStaff();
            return Ok(pendingStaff);
        }

        [HttpGet]
        [Route("approveStaff")]
        public IHttpActionResult ApproveStaff(long staffId)
        {
            var pendingStaff = _member.ApproveStaff(staffId);
            return Ok(pendingStaff);
        }

        [HttpGet]
        [Route("loadAllStaff")]
        public IHttpActionResult GetAllStaff()
        {
            var allStaff = _member.GetAllStaff();
            return Ok(allStaff);
        }

        [HttpGet]
        [Route("modifyStaff")]
        public IHttpActionResult ModifyStaff(long staffId, bool isEnabled)
        {
            var pendingStaff = _member.ModifyStaff(staffId, isEnabled);
            return Ok(pendingStaff);
        }
        [HttpGet]
        [Route("allbookings")]
        public IHttpActionResult Getallbookings(int minimumCount, int numberOfRecord, int currentpage)
        { 
            var totBookings = _member.Getallbookings(minimumCount, numberOfRecord, currentpage); 
            return Ok(totBookings);
        }
        [HttpGet]
        [Route("savecards")]
        public IHttpActionResult GetCustomerCards() 
        {
            var customerCards = _member.GetCustomerCards(); 
            return Ok(customerCards);
        }


        [HttpGet]
        [Route("pushNotificationConfiguration")]
        public IHttpActionResult GetPushNotificationConfiguration()
        {
            var configs = _member.GetPushNotificationConfiguration();

            return Ok(configs);
        }
		
		
        [HttpPost]
        [Route("deletesavecards")]
        public IHttpActionResult DeleteCustomerCards(CustomerDeleteSavedCardViewModel model)  
        {  
            var objDeleteCd = _member.DeleteCustomerCards(model);
            return Ok(objDeleteCd);
        }

        [HttpGet]
        [Route("organizations")]
        public IHttpActionResult GetOrganizations()
        {
            var org = _member.GetOrganizations();
            return Ok(org);
        }

        [HttpGet]
        [Route("bannermassages")]
        public IHttpActionResult GetBannerMessage(int siteId)
        {
            var objBanner = _member.GetBannerMessage(siteId);
            return Ok(objBanner); 
        }
    }
}
