﻿using SwaranAdmin_New.DataAccess;
using System.Web.Http;

namespace SwaranAdmin_New.Controllers_Api
{
    [RoutePrefix("api/autocab")]
    public class AutocabApiController : ApiController
    {
        private AutoCabDal _rexAutoCab = AutoCabDal.Instance;

        [HttpGet]
        [Route("vehicletypes")]
        public IHttpActionResult GetVehicletypes(long vehicletypeid)
        {
            var vahicaletypes = _rexAutoCab.GetVehicletypes(vehicletypeid);
            return Ok(vahicaletypes);
        }

    }
}
