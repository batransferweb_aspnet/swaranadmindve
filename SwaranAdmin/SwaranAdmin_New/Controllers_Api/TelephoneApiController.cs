﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Routing;
using System.Web.UI.WebControls;
using System.Web.WebPages;
using Microsoft.AspNet.Identity;
using SwaranAdmin_New.Autocab.Base;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataAccess.TeleBooking;
using SwaranAdmin_New.Models;
using SwaranAdmin_New.Models.BookingMo;
using SwaranAdmin_New.Models.TeleBooking;

namespace SwaranAdmin_New.Controllers_Api
{
    [RoutePrefix("api/telebooking")]
    public class TelephoneApiController : ApiController
    {
        private TelebookingDal _repositiry = TelebookingDal.Instance;

        [HttpGet]
        [Route("vehicletypes")]
        public IHttpActionResult GetVehicletypes()   
        {
            var objVehicletypes = _repositiry.GetVehicletypes(); 
            return Ok(objVehicletypes); 
        }

        [HttpGet]
        [Route("pessangercount")]
        public IHttpActionResult GetMaxNumOfPessangerCount()
        {
            var objVehicletypes = _repositiry.GetMaxNumOfPessangerCount();
            return Ok(objVehicletypes);
        }


        [HttpPost]
        [Route("calculateprice")]
        public IHttpActionResult CalculatePrice(TelPriceViewModel booking)
        {

            long rateId = (int)DefaultRateEnum.CASH_RATE;
            var viaModel = new ViaPriceCalViewModel();
                var priceCal = new PriceCalOutViewModel();

             

                var fixedPrice = FixedPriceHelper.GetFixpriceId(booking);
            booking.FromFixedPriceAriaPointId = fixedPrice.FromFixedPriceAriaPointId;
            booking.ToFixedPriceAriaPointId = fixedPrice.ToFixedPriceAriaPointId;


            bool isAnyVia =   booking.ViaPointAddress != null &&  booking.ViaPointAddress.Any(x => !x.Postcode.IsEmpty());
                if (isAnyVia)
                {
                    var viaConfig = _repositiry.GetViaConfiguration();
                    viaModel.ViaMileRate = viaConfig.MileRate;
                    viaModel.ViaTimeRate = viaConfig.ViaTimeRate;
                    viaModel.ViaMinimumRate = viaConfig.MinimumRate;
                    viaModel.IsFastViaRoute = viaConfig.IsFastRoute;

                    if (viaModel.MainDistanceVia == 0)
                    {
                        var distanceListMain =
                                        BaGoogleApi.GetDistance(
                                            booking.FromLat + "," + booking.FromLong,
                                            booking.ToLat + "," + booking.ToLong, booking.BookingFrom, booking.BookingTo);
                        if (distanceListMain != null && distanceListMain.Count > 0)
                        {

                            GoogleApiDistanceVal objDis = null;
                            objDis = viaModel.IsFastViaRoute
                                        ? distanceListMain.OrderBy(x => x.Time).First()
                                        : distanceListMain.OrderByDescending(x => x.Time).First();
                            viaModel.MainDistanceVia = objDis.Distance;
                            viaModel.MainTimeVia = objDis.Time;

                        }
                        if (viaModel.MainDistanceVia == 0)
                        {
                            //return View(model);
                        }

                        if (booking.ViaPointAddress != null)
                        {
                            var listViapoints = booking.ViaPointAddress.Where(x => !x.Postcode.IsEmpty()).ToArray();
                            var viaList = new List<string>();
                            int y = 0;
                            string waypoits = string.Empty;
                            foreach (var item in listViapoints)
                            {
                                y++;
                                if (y == 1)
                                {
                                    waypoits = "|" + item.Lat + "," + item.Long;
                                }
                                else
                                {
                                    waypoits = item.Lat + "," + item.Long;
                                }
                                viaList.Add(waypoits);
                            }
                            var distanceListVia = BaGoogleApi.GetDistanceWithWayPoints(booking.FromLat + "," + booking.FromLong, booking.ToLat + "," + booking.ToLong, viaList);
                            //booking.TotalViaDistance = distanceListVia.Min(x => x.Distance);
                            //model.TotalOneWayDistance = distanceListVia.Min(x => x.Distance); 
                            //model.TotalDistance = distanceListVia.Min(x => x.Distance);
                            //var dis = distanceListVia.Min(x => x.Time);
                            //model.TotalTime = (int)dis + _repositiry.GetExtraDrivingTime((int)dis);
                            //TimeSpan startTimeSpan = TimeSpan.FromMinutes((int)dis);
                            //model.StartTimeLabel = startTimeSpan.ToString(@"hh\:mm");
                            //TimeSpan timeSpanVia = TimeSpan.FromMinutes(model.TotalTime);
                            //model.TotalTimeLabel = timeSpanVia.ToString(@"hh\:mm");


                            if (distanceListVia != null && distanceListVia.Count > 0)
                            {
                                GoogleApiDistanceVal objDis = null;
                                objDis = viaModel.IsFastViaRoute
                                            ? distanceListVia.OrderBy(x => x.Time).First()
                                            : distanceListVia.OrderByDescending(x => x.Time).First();
                                //model.TotalOneWayDistance = objDis.Distance;
                                //model.TotalDistance = objDis.Distance;
                                //model.TotalViaDistance = objDis.Distance;
                                //model.TotalViaTime = objDis.Time;
                                //model.TotalTime = (int)objDis.Time + _repositiry.GetExtraDrivingTime((int)objDis.Time);
                                //TimeSpan timeSpan = TimeSpan.FromMinutes(model.TotalTime);
                                //model.TotalTimeLabel = timeSpan.ToString(@"hh\:mm");
                                //TimeSpan startTimeSpan = TimeSpan.FromMinutes((int)objDis.Time);
                                //model.StartTimeLabel = startTimeSpan.ToString(@"hh\:mm");
                            }

                        }

                    }
                }
                if (!isAnyVia)
                {
                    var availableFixedPrice = _repositiry.IsAvailableFixedPrice(booking.FromFixedPriceAriaPointId, booking.ToFixedPriceAriaPointId);
                    booking.TotalOneWayDistance = availableFixedPrice.DrivingDistance;
                    booking.TotalDistance = availableFixedPrice.DrivingDistance;
                    //booking.TotalTime = availableFixedPrice.Drivingtime + _repositiry.GetExtraDrivingTime(availableFixedPrice.Drivingtime);
                    //TimeSpan timeSpan = TimeSpan.FromMinutes(booking.TotalTime);
                    //booking.TotalTimeLabel = timeSpan.ToString(@"hh\:mm");
                    //TimeSpan startTime = TimeSpan.FromMinutes((int)availableFixedPrice.Drivingtime);
                    //booking.StartTimeLabel = startTime.ToString(@"hh\:mm");

                    if (!availableFixedPrice.IsFixedPriceAvailable)
                    {

                        //without viapoints Distance 
                        var distanceList = BaGoogleApi.GetDistance(booking.FromLat + "," + booking.FromLong, booking.ToLat + "," + booking.ToLong, booking.BookingFrom, booking.BookingTo);
                        // booking.RateId = (int)DefaultRateEnum.OUTER_LONDON_RATE;
                        int value = _repositiry.IsLondonBooking(booking.FromFixedPriceAriaPointId, booking.ToFixedPriceAriaPointId);
                        rateId = value == 1 ? (int)DefaultRateEnum.CASH_RATE : (int)DefaultRateEnum.OUTER_LONDON_RATE;

                        //string organizationLocation = OrganizationHelper.GetOrganizationLocation((int)booking.BaseSiteId);

                        //if (!string.IsNullOrEmpty(organizationLocation))
                        //{
                        //    int minOfficeDistance = int.Parse(ConfigurationManager.AppSettings["MinOfficeDistance"]);
                        //    decimal fromOfzDistace = 0; decimal toofzDistance = 0;
                        //    var distanceListFrom = BaGoogleApi.GetDistance(organizationLocation, booking.ToLat + "," + booking.ToLong, "Office location", booking.BookingTo);
                        //    if (distanceListFrom != null && distanceListFrom.Count > 0)
                        //    {
                        //        fromOfzDistace = distanceListFrom.Min(x => x.Distance);

                        //    }
                        //    var distanceListTo = BaGoogleApi.GetDistance(organizationLocation, booking.FromLat + "," + booking.FromLong, booking.BookingFrom, "Office location");
                        //    if (distanceListTo != null && distanceListTo.Count > 0)
                        //    {
                        //        toofzDistance = distanceListTo.Min(x => x.Distance);

                        //    }
                        //    if (fromOfzDistace > minOfficeDistance && toofzDistance > minOfficeDistance)
                        //    {
                        //        booking.RateId = (int)DefaultRateEnum.OUTER_LONDON_RATE;
                        //    }


                        //}

                        //if (distanceList != null && distanceList.Count > 0)
                        //{

                        //    var minimum = distanceList.Min(x => x.Distance);
                        //    booking.TotalOneWayDistance = distanceList.Min(x => x.Distance);
                        //    booking.TotalDistance = distanceList.Min(x => x.Distance);
                        //    var dis = distanceList.Min(x => x.Time);
                        //    booking.TotalTime = (int)dis + _repositiry.GetExtraDrivingTime((int)dis);
                        //    TimeSpan timeSpanVal = TimeSpan.FromMinutes(booking.TotalTime);
                        //    booking.TotalTimeLabel = timeSpanVal.ToString(@"hh\:mm");
                        //    TimeSpan startTimeSpan = TimeSpan.FromMinutes((int)dis);
                        //    booking.StartTimeLabel = startTimeSpan.ToString(@"hh\:mm");
                        //}

                        if (distanceList != null && distanceList.Count > 0)
                        {
                            GoogleApiDistanceVal objDis = null;
                            objDis = viaModel.IsFastViaRoute
                                        ? distanceList.OrderBy(x => x.Time).First()
                                        : distanceList.OrderByDescending(x => x.Time).First();
                            //model.TotalOneWayDistance = objDis.Distance;
                            //model.TotalDistance = objDis.Distance;
                            //model.TotalTime = (int)objDis.Time + _repositiry.GetExtraDrivingTime((int)objDis.Time);
                            //TimeSpan timeSpanVal = TimeSpan.FromMinutes(model.TotalTime);
                            //model.TotalTimeLabel = timeSpanVal.ToString(@"hh\:mm");
                            //TimeSpan startTimeSpan = TimeSpan.FromMinutes((int)objDis.Time);
                            //model.StartTimeLabel = startTimeSpan.ToString(@"hh\:mm");
                        }

                    }
                } 
            bool isWebPriceEnabled = bool.Parse(ConfigurationManager.AppSettings["IsWebPriceEnabled"]);
                if (isWebPriceEnabled)
                {
                priceCal = _repositiry.GetOneWayPrice(booking, rateId, viaModel);

            }

            return Ok(priceCal);
        }

            //public IHttpActionResult CalculatePrice(TelPriceViewModel booking)
            //{
            //    long rateId = 0;
            //    var viaModel = new ViaPriceCalViewModel();
            //    var priceCal = new PriceCalOutViewModel();

            //    bool isIndex = bool.Parse(ConfigurationManager.AppSettings["IsIndex"]);
            //    ////if (isIndex)
            //    ////{
            //    ////    string ss = XmlHelper.GetXMLFromObject(model);
            //    ////    XmlObjectWriter.WriteXml("Index", ss);
            //    ////}

            //    //ViewBag.discountList = _repositiry.GetDiscountList();
            //    //ViewBag.passangerList = _repositiry.GetNumofPassangersList(model);
            //    //ViewBag.timeList = _repositiry.GetCurrentTimeList(0);
            //    if (ModelState.ContainsKey("ToBuildingNumber"))
            //        ModelState["ToBuildingNumber"].Errors.Clear();
            //    if (ModelState.ContainsKey("FromBuildingNumber"))
            //        ModelState["FromBuildingNumber"].Errors.Clear();


            //    for (int i = 0; i < 5; i++)
            //    {

            //        if (ModelState.ContainsKey("ViaPointAddress[" + i + "].AriaPoitId"))
            //            ModelState["ViaPointAddress[" + i + "].AriaPoitId"].Errors.Clear();
            //        if (ModelState.ContainsKey("ViaPointAddress[" + i + "].AriaTypeId"))
            //            ModelState["ViaPointAddress[" + i + "].AriaTypeId"].Errors.Clear();
            //        if (ModelState.ContainsKey("ViaPointAddress[" + i + "].BuildingNumber"))
            //            ModelState["ViaPointAddress[" + i + "].BuildingNumber"].Errors.Clear();
            //        if (ModelState.ContainsKey("ViaPointAddress[" + i + "].Postcode"))
            //            ModelState["ViaPointAddress[" + i + "].Postcode"].Errors.Clear();
            //        if (ModelState.ContainsKey("ViaPointAddress[" + i + "].Lat"))
            //            ModelState["ViaPointAddress[" + i + "].Lat"].Errors.Clear();
            //        if (ModelState.ContainsKey("ViaPointAddress[" + i + "].Long"))
            //            ModelState["ViaPointAddress[" + i + "].Long"].Errors.Clear();
            //    }


            //    //alwas via point set to null
            //    //  booking.ViaPointAddress = null;

            //    var fixedPrice = FixedPriceHelper.GetFixpriceId(booking);
            //    booking.FromFixedPriceAriaPointId = fixedPrice.FromFixedPriceAriaPointId;
            //    booking.ToFixedPriceAriaPointId = fixedPrice.ToFixedPriceAriaPointId;





            //    if (booking.ViaPointAddress != null)
            //    {
            //        var listViapoints = booking.ViaPointAddress.Where(x => !x.Postcode.IsEmpty()).ToArray();
            //        var viaList = new List<string>();
            //        int y = 0;
            //        string waypoits = string.Empty;
            //        foreach (var item in listViapoints)
            //        {
            //            y++;
            //            if (y == 1)
            //            {
            //                waypoits = "|" + item.Lat + "," + item.Long;
            //            }
            //            else
            //            {
            //                waypoits = item.Lat + "," + item.Long;
            //            }
            //            viaList.Add(waypoits);
            //        }
            //        var distanceListVia = BaGoogleApi.GetDistanceWithWayPoints(booking.FromLat + "," + booking.FromLong,
            //            booking.ToLat + "," + booking.ToLong, viaList);
            //        //booking.TotalViaDistance = distanceListVia.Min(x => x.Distance);
            //        //booking.TotalOneWayDistance = distanceListVia.Min(x => x.Distance); 
            //        //booking.TotalDistance = distanceListVia.Min(x => x.Distance);
            //        //var dis = distanceListVia.Min(x => x.Time);
            //        //booking.TotalTime = (int)dis + _repositiry.GetExtraDrivingTime((int)dis);
            //        //TimeSpan startTimeSpan = TimeSpan.FromMinutes((int)dis);
            //        //booking.StartTimeLabel = startTimeSpan.ToString(@"hh\:mm");
            //        //TimeSpan timeSpanVia = TimeSpan.FromMinutes(booking.TotalTime);
            //        //booking.TotalTimeLabel = timeSpanVia.ToString(@"hh\:mm");

            //        if (distanceListVia != null && distanceListVia.Count > 0)
            //        {
            //            GoogleApiDistanceVal objDis = null;
            //            objDis = viaModel.IsFastViaRoute
            //                ? distanceListVia.OrderBy(x => x.Time).First()
            //                : distanceListVia.OrderByDescending(x => x.Time).First();
            //            booking.TotalOneWayDistance = objDis.Distance;
            //            booking.TotalDistance = objDis.Distance;
            //            viaModel.TotalViaDistance = objDis.Distance;
            //            viaModel.TotalViaTime = objDis.Time;
            //            //    viaModel.TotalTime = (int)objDis.Time + _repositiry.GetExtraDrivingTime((int)objDis.Time);
            //            //    TimeSpan timeSpan = TimeSpan.FromMinutes(booking.TotalTime);
            //            //    booking.TotalTimeLabel = timeSpan.ToString(@"hh\:mm");
            //            //    TimeSpan startTimeSpan = TimeSpan.FromMinutes((int)objDis.Time);
            //            //    booking.StartTimeLabel = startTimeSpan.ToString(@"hh\:mm");
            //            //} 
            //        }

            //        bool isAvaiVias = false;
            //        if (booking.ViaPointAddress != null)
            //        {
            //            isAvaiVias = booking.ViaPointAddress.Any(x => !x.Postcode.IsEmpty());
            //        }
            //        var availableFixedPrice = _repositiry.IsAvailableFixedPrice(booking.FromFixedPriceAriaPointId,
            //            booking.ToFixedPriceAriaPointId);
            //        booking.TotalOneWayDistance = availableFixedPrice.DrivingDistance;
            //        booking.TotalDistance = availableFixedPrice.DrivingDistance;
            //        //  booking.TotalTime = availableFixedPrice.Drivingtime + _repositiry.GetExtraDrivingTime(availableFixedPrice.Drivingtime);
            //        // TimeSpan span = TimeSpan.FromMinutes(booking.TotalTime);
            //        //  booking.TotalTimeLabel = String.Format("{0:%h} hours {0:%m} minutes", span); //span.ToString(@"hh\hours  mm\minutes");
            //        // TimeSpan startTime = TimeSpan.FromMinutes(availableFixedPrice.Drivingtime);
            //        //  booking.StartTimeLabel = String.Format("{0:%h} hours {0:%m} minutes", startTime);// startTime.ToString(@"hh\:mm");
            //        if (!availableFixedPrice.IsFixedPriceAvailable)
            //        {

            //            //without viapoints Distance 
            //            var distanceList = BaGoogleApi.GetDistance(booking.FromLat + "," + booking.FromLong,
            //                booking.ToLat + "," + booking.ToLong, booking.BookingFrom, booking.BookingTo);

            //            int value = _repositiry.IsLondonBooking(booking.FromFixedPriceAriaPointId,
            //                booking.ToFixedPriceAriaPointId);
            //            rateId = value == 1 ? (int) DefaultRateEnum.CASH_RATE : (int) DefaultRateEnum.OUTER_LONDON_RATE;


            //            var viaConfig = _repositiry.GetViaConfiguration();

            //            if (distanceList != null && distanceList.Count > 0)
            //            {
            //                GoogleApiDistanceVal objDis = null;
            //                objDis = viaConfig.IsFastRoute
            //                    ? distanceList.OrderBy(x => x.Time).First()
            //                    : distanceList.OrderByDescending(x => x.Time).First();
            //                booking.TotalOneWayDistance = objDis.Distance;
            //                booking.TotalDistance = objDis.Distance;
            //                // booking.TotalTime = (int)objDis.Time + _repositiry.GetExtraDrivingTime((int)objDis.Time);
            //                // TimeSpan timeSpan = TimeSpan.FromMinutes(booking.TotalTime);
            //                // booking.TotalTimeLabel = timeSpan.ToString(@"hh\:mm");
            //                TimeSpan startTimeSpan = TimeSpan.FromMinutes((int) objDis.Time);
            //                //  booking.StartTimeLabel = startTimeSpan.ToString(@"hh\:mm");
            //            }

            //        }

            //        if ((booking.FromFixedPriceAriaPointId > 0 && booking.ToFixedPriceAriaPointId > 0) ||
            //            booking.TotalDistance > 0)
            //        {

            //            bool isWebPriceEnabled = bool.Parse(ConfigurationManager.AppSettings["IsWebPriceEnabled"]);
            //            if (isWebPriceEnabled)
            //            {
            //                priceCal = _repositiry.GetOneWayPrice(booking, rateId);

            //            }

            //        }

            //        return Ok(priceCal);
            //    }
            //}

        }
}
