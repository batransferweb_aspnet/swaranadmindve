﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using SwaranAdmin_New.Base;
using System.Web.Http;

namespace SwaranAdmin_New.Controllers_Api
{
    [RoutePrefix("api/address")]
    public class AddressApiController : ApiController
    {
        private AddressHelper _Local = AddressHelper.Instance;
        [HttpGet]
        [Route("alladdress")]
        public IHttpActionResult GetAddress(string name)
        {
            string text = !string.IsNullOrEmpty(name) ? name.ToUpper() : name;
            var list = _Local.LoadAddresses(text);
            return Json(list);

        } 
        [HttpGet]
        [Route("favouriteaddress")]
        public IHttpActionResult GetFavouriteAddress(string userid)
        {
          var list = _Local.GetFavouriteAddress(userid);
             return Json(list);

           
        }

        [HttpGet]
        [Route("loadstreets")]
        public IHttpActionResult GetStreets(string name) 
        {
            var list = _Local.LoadAllStreet(name);
            return Json(list);


        }
    }
}
