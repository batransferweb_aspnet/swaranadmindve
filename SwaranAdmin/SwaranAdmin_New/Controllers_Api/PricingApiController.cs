﻿using SwaranAdmin_New.DataAccess;
using SwaranAdmin_New.Models.Pricing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SwaranAdmin_New.Controllers_Api
{
    [RoutePrefix("api/pricing")]
    public class PricingApiController : ApiController
    {

        private PricingDal _price = PricingDal.Instance;

        [HttpGet]
        [Route("customerRate")]
        public IHttpActionResult GetCustomerRates()
        {
            var customerRate = _price.GetCustomerRates();
            return Ok(customerRate);
        }

        [HttpGet]
        public IHttpActionResult GetRateVehicleTypeDistanceDetail(long rateCustomerDistanceDayId)
        {
            var customerRateDistanceDetail = _price.GetRateVehicleTypeDistanceDetail(rateCustomerDistanceDayId);
            return Ok(customerRateDistanceDetail);
        }

        [HttpPost]
        [Route("saveDistance")]
        public IHttpActionResult SaveDistanceDetail(RateCustomerDistanceForUpdate distanceModel)
        {
            var customerRateDistanceDetail = _price.SaveDistanceDetail(distanceModel);
            return Ok(customerRateDistanceDetail);
        }

        [HttpPost]
        [Route("deleteDistance")]
        public IHttpActionResult DeleteDistanceDetail(RateCustomerDistanceForDelete distanceModel)
        {
            var customerRateDistanceDetail = _price.DeleteDistanceDetail(distanceModel);
            return Ok(customerRateDistanceDetail);
        }

        [HttpPost]
        [Route("createDistance")]
        public IHttpActionResult CreateDistanceDetail(RateCustomerDistanceForCreate distanceModel)
        {
            var customerRateDistanceDetail = _price.CreateDistanceDetail(distanceModel);
            return Ok(customerRateDistanceDetail);
        }

        [HttpPost]
        [Route("createRateVehicle")]
        public IHttpActionResult CreateRateVehicle(RateCustomerVehicleForCreate rateVehicle)
        {
            var customerRateVehicle = _price.CreateRateVehicle(rateVehicle);
            return Ok(customerRateVehicle);
        }

        [HttpPost]
        [Route("createRateVehicleDay")]
        public IHttpActionResult CreateRateVehicleDay(RateCustomerVehicleDayForCreate rateVehicleDay)
        {
            var customerRateVehicle = _price.createRateVehicleDay(rateVehicleDay);
            return Ok(customerRateVehicle);
        }


        [HttpPost]
        [Route("saveDistanceNew")]
        public IHttpActionResult SaveDistanceDetailNew(RateCustomerDistanceForUpdate distanceModel)
        {
            var customerRateDistanceDetail = _price.SaveDistanceDetailNew(distanceModel);
            return Ok(customerRateDistanceDetail);
        }

        [HttpPost]
        [Route("deleteDistanceNew")]
        public IHttpActionResult DeleteDistanceDetailNew(RateCustomerDistanceForDelete distanceModel)
        {
            var customerRateDistanceDetail = _price.DeleteDistanceDetailNew(distanceModel);
            return Ok(customerRateDistanceDetail);
        }

        [HttpPost]
        [Route("createDistanceNew")]
        public IHttpActionResult CreateDistanceDetailNew(RateCustomerDistanceForCreate distanceModel)
        {
            var customerRateDistanceDetail = _price.CreateDistanceDetailNew(distanceModel);
            return Ok(customerRateDistanceDetail);
        }

        [HttpPost]
        [Route("createRateVehicleDayNew")]
        public IHttpActionResult CreateRateVehicleDayNew(RateCustomerVehicleDayForCreate rateVehicleDay)
        {
            var customerRateVehicle = _price.createRateVehicleDayNew(rateVehicleDay);
            return Ok(customerRateVehicle);
        }


    }
}
