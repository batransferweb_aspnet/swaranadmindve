﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SwaranAdmin_New.Models
{
    public class StaffAdditionalDetailsModel
    {
        public string Id { get; set; }
        public string PostalCode { get; set; }
        public string Street { get; set; }        
        public string Fax { get; set; }
        public string Town { get; set; }
        public int? CountryId { get; set; }
        public bool IsSuperAdmin { get; set; }
        public bool IsPaySlipActive { get; set; }
        public string PaymentMethod { get; set; }
        public string Workbase { get; set; }
        public int? PayslipFrequencyId { get; set; }
        //[DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        [Range(0.0, Double.MaxValue, ErrorMessage = "Invalid PayRateAccountant")]
        public decimal? PayRateAccountant { get; set; }
        //[DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        [Range(0.0, Double.MaxValue, ErrorMessage = "Invalid PayRateDirect")]
        public decimal? PayRateDirect { get; set; }
        public string PayOption { get; set; }
        public int? MaxHoursAccountant { get; set; }
        [Range(0.0, Double.MaxValue, ErrorMessage = "Invalid Pension")]
        public decimal? Pension { get; set; }
        public int? CompanyWorkingFor { get; set; }
    }
}