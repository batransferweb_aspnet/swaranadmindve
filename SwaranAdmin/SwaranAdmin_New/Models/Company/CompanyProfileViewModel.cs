﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SwaranAdmin_New.Models.Company
{
    public class CompanyProfileViewModel
    {
        public long CompanyId { get; set; } 
        public string CompanyName { get; set; }
        public string CompanyRegNumber { get; set; }
        public string RateFormula { get; set; }
        public string RateName { get; set; } 
        public string ContractReference { get; set; } 
        public string PaymentType { get; set; }
        public string CompanyEmail { get; set; }

    }

    public class CompanyUserViewModel 
    {
        public string UserName { get; set; } 
        public string UserId { get; set; }

        [Display(Name = "Company Email")]
        [Required]
        [EmailAddress]
        public string CompanyEmail { get; set; }

        [Display(Name = "Company Name")]
        public long ComapanyId { get; set; }
        [Display(Name = "Company Name")]
        [Required]
        public string ComapanyName { get; set; }
  
        
        public string RateName { get; set; } 
        [Required]
        public string RateFormula { get; set; }


    }

    public class ContractReferanceViewModel 
    {
         
        [Required]
        public string ContractReference { get; set; }
        [Required]
        public string PaymentType { get; set; }
        [Display(Name = "Company Name")]
        public long ComapanyId { get; set; }
        [Display(Name = "Company Name")]
        [Required]
        public string ComapanyName { get; set; }
        [Display(Name = "Company Email")]
        [Required]
        [EmailAddress]
        public string CompanyEmail { get; set; } 


    }

    public class CompanyViewModel
    {
        public long CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string CompanyRegNumber { get; set; }
        public string NatureOfBusiness { get; set; }
        public string Website { get; set; }
        public int CompanyEstablishedYear { get; set; }
        public string PostalCode { get; set; }
        public string Street { get; set; }
        public string Town { get; set; }
        public long CountryID { get; set; }
      //  public decimal MonthlySpend { get; set; }
        public string AuthorisedBy { get; set; }
        public string Fax { get; set; }
      public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
       // public long TotalJourneyTime { get; set; }
      //  public decimal TotalJourneyDistance { get; set; }
     //   public int TotalPoints { get; set; }
      //  public long TotalNumberOfJourneys { get; set; }
     //   public int AvailablePoints { get; set; }
     //   public decimal AvailableCashBalance { get; set; }
      //  public decimal AvailableDistance { get; set; }
     //   public long AvailableTime { get; set; }
       // public string PromoCode { get; set; }
     //   public bool IsFreeJourneyAvailable { get; set; }
       // public int NumberOfFreeJourneys { get; set; }
     //   public int LoyaltyMemberType { get; set; }
       // public int MaximumUserCount { get; set; }
       // public bool IsApproved { get; set; }
      //  public bool IsSubscriptionEnabled { get; set; }
       public bool IsMarketingEmailEnabled { get; set; } 
        public string Email { get; set; }
       // public  int LiveAccountId { get; set; }
        public string PaymentType { get; set; }
        public string ContractReference { get; set; }
      //  public string RateFormula { get; set; }
      //  public string RateName { get; set; }
     //   public string DisplayName { get; set; }
      //  public string AccountCode { get; set; }
       public string AddressLine1 { get; set; }
      public string AddressLine2 { get; set; }
        public string County { get; set; }
        public string TelephoneNumber { get; set; }
        public string CommentForBookingUsers { get; set; }
     //   public int InvoiceFrequencyId { get; set; }
      //  public bool IsSuspended { get; set; }
      //  public int VatTypeId { get; set; }
    //    public int AutoPriority { get; set; }
      //  public string CommentForAccountUsers { get; set; }
    //    public decimal Discount { get; set; }
    //    public int NumberOfEmailInvoice { get; set; }
   //     public bool IsExportInvoice { get; set; }
  //      public string CommentShownOnInvoice { get; set; }
   //     public int NumberOfPrintInvoice { get; set; }
   //     public decimal ServiceCharge { get; set; }
     //   public decimal TaxRate { get; set; }
   //     public int NumberOfUploadedInvoice { get; set; }
    }

}