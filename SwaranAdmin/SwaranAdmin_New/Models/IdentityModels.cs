﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;

namespace SwaranAdmin_New.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [StringLength(40, ErrorMessage = "FirstName cannot be longer than 40 characters.")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(40, ErrorMessage = "LastName cannot be longer than 20 characters.")]
        public string LastName { get; set; }
        public int Gender { get; set; }
        public string LandPhoneCountryCode { get; set; }
        [StringLength(20, ErrorMessage = "LandPhoneNumber cannot be longer than 20 characters.")]
        public string LandPhoneNumber { get; set; }
        [StringLength(20, ErrorMessage = "MobileNumberCountryCode cannot be longer than 20 characters.")]
        public string MobileNumberCountryCode { get; set; }
        [StringLength(20, ErrorMessage = "MobileNumber cannot be longer than 20 characters.")]
        public string MobileNumber { get; set; }
        [Required]
        [Index("PhoneIndex", IsUnique = true)]
        [StringLength(20, ErrorMessage = "PrimaryPhoneNumber cannot be longer than 20 characters.")]
        public string PrimaryPhoneNumber { get; set; }
        public bool IsMobileNumberPrimary { get; set; }
        public bool IsSendSms { get; set; }
        public bool IsCompanyUser { get; set; }
        public long? CompanyId { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime sysModifiedDate { get; set; }
        public DateTime sysCreatedDate { get; set; }
        public bool isUserSuspended { get; set; }
        [StringLength(200, ErrorMessage = "reason cannot be longer than 200 characters.")]
        public string reason { get; set; }
        public bool isUserValidated { get; set; }
        [StringLength(250, ErrorMessage = "resetPasswordToken cannot be longer than 250 characters.")]
        public string resetPasswordToken { get; set; }
        public DateTime resetPasswordTokenCreatedDate { get; set; }
        public int resetPasswordTokenValidTime { get; set; }
        [StringLength(10, ErrorMessage = "countryCode cannot be longer than 10 characters.")]
        public string countryCode { get; set; }
        public bool isFacebookLogin { get; set; }
        //[Required]
        [Index("FacebookIndex", IsUnique = true)]
        [StringLength(300, ErrorMessage = "facebookToken cannot be longer than 300 characters.")]
        public string facebookToken { get; set; }
        public bool isAuthorized { get; set; }
        [StringLength(5, ErrorMessage = "verificationCode cannot be longer than 5 characters.")]
        public string verificationCode { get; set; }
        public bool IsEmailVerified { get; set; }
        public bool IsPhoneVerified { get; set; }
        [StringLength(250, ErrorMessage = "resetPasswordToken cannot be longer than 250 characters.")]
        public string EmailVerificationToken { get; set; }
        [StringLength(5, ErrorMessage = "PhoneVerificationToken cannot be longer than 5 characters.")]
        public string PhoneVerificationToken { get; set; }
        public DateTime EmailVerificationTokenCreatedDate { get; set; }
        public DateTime PhoneVerificationTokenCreatedDate { get; set; }
        public bool IsIpRestricted { get; set; }
        public bool IsIpDeniedRestriction { get; set; }
        //[Index("MemberIndex", IsUnique = true)]
        //  [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)]
        public long MemberId { get; set; }
        public int TitleId { get; set; }
        public bool IsDriver { get; set; }
        public bool IsStaff { get; set; }
        public bool IsCustomer { get; set; }
        [StringLength(25, ErrorMessage = "NationalIdentity cannot be longer than 25 characters.")]
        public string NationalIdentity { get; set; }
        public DateTime? DateOfBirth { get; set; }
        [StringLength(10, ErrorMessage = "PostalCode cannot be longer than 10 characters.")]
        public string PostalCode { get; set; }
        [StringLength(500, ErrorMessage = "Street cannot be longer than 500 characters.")]
        public string Street { get; set; }
        [StringLength(50, ErrorMessage = "Town cannot be longer than 50 characters.")]
        public string Town { get; set; }
        public long? CountryId { get; set; }
        public bool IsPasswordResetRequired { get; set; }
        public bool IsCompanyAdmin { get; set; }
        public bool IsTempPassword { get; set; }
        public bool IsStaffAccountVerified { get; set; }
        public bool IsDriverAccountVerified { get; set; }
        public string MobilePasswordResetCode { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}
