﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SwaranAdmin_New.Models
{
    public class FeedbacksViewModel
    {
        public long FeedBackId { get; set; }
        public long? BookingId { get; set; }
        public string CustomerEmail { get; set; }
        public string BookingNumber { get; set; }
        public string MercuryNumber { get; set; }
        public string PhoneNumber { get; set; }

        public string CustomerName { get; set; }
        public string OrganizationName { get; set; }
        public string Improvements { get; set; }
        public string Comments { get; set; }
        public int DriverRating { get; set; }
        public int ReservationRating { get; set; }
        public decimal ReservationRatingPercentage { get; set; }
        public bool IsVehicleClean { get; set; }
        public bool WillYouRecommendUs { get; set; }
        public bool IsDriverOfferedBusinessCard { get; set; }
        public string FeedbackDate { get; set; }
        public bool? C_Did_the_driver_arrive_on_time_ { get; set; }
        public bool? Was_the_driver_wearing_suit_ { get; set; }
        public bool? Did_the_driver_help_you_with_the_luggage_ { get; set; }
        public bool? Was_the_driver_polite_and_friendly_ { get; set; }
        public bool? Did_the_driver_maintain_appropriate_speed_ { get; set; }

        public bool IsAddedToDriverComplaint { get; set; }
        public bool IsAddedToDriverAppreciation { get; set; }
        public bool IsAddedToSystemComplaint { get; set; }
        public bool IsMailedToCustomer { get; set; }
        public bool IsMailedToStaff { get; set; }
        public bool IsAddedToSpecialPreference { get; set; }
        public bool IsAddedToSuggestion { get; set; }
        public bool IsAddedToTestimonial { get; set; }

        public int NumberOfPages { get; set; }

        public string Loyalty { get; set; }
        public bool IsSentToCustomerService { get; set; }

    }

    public class SpecialPreferencesViewModel
    {
        public long Feedbackid { get; set; }
        public long FavouriteAddressId { get; set; }
        public long AddressId { get; set; }

        public string Note { get; set; }
        public string UserId { get; set; }
        public string StaffId { get; set; }
    }

    public class CustomerEmailViewModel
    {
        public string StaffId { get; set; }
        [DisplayName("Operator Name")]
        public string StaffName { get; set; }
        public long Feedbackid { get; set; }
        [DisplayName("Customer Name")]
        public string CustomerName { get; set; }

        [Required(ErrorMessage = "Please enter valid email address")]
        [EmailAddress]
        [DisplayName("Email")]
        public string ToAddress { get; set; }
        [Required(ErrorMessage = "Email Subject is required")]
        public string Subject { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = "Email body is required")]
        public string Message { get; set; }

        public string TemplateName { get; set; }
    }
    public class StaffEmailViewModel
    {
        public long Feedbackid { get; set; }
        public string StaffId { get; set; }
        [DisplayName("Operator Name")]
        public string OperatorName { get; set; }
        [DisplayName("Staff Name")]
        public string StaffName { get; set; }

        [Required(ErrorMessage = "Please enter valid email address")]
        [EmailAddress]
        [DisplayName("Email")]
        public string ToAddress { get; set; }
        [Required(ErrorMessage = "Email Subject is required")]
        public string Subject { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = "Email body is required")]
        public string Message { get; set; }

        public string TemplateName { get; set; }
    }
    public class CustomerEmailTemplateViewModel
    {
        public int CustomerEmailTemplateId { get; set; }
        public string TemplateName { get; set; }
        public string Template { get; set; }
    }


    public class CustomerReviewViewModel
    {
        public long FeedbackId { get; set; }
        public long ReviewId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Location { get; set; }
        public int Rating { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public int SiteId { get; set; }
        public bool IsActive { get; set; }
        public bool IsEnable { get; set; }
        public bool IsDeleted { get; set; }
    }
    public class SystemComplaintViewModel
    {
        [DisplayName("Operator Name")]
        public string OperatorName { get; set; }
        public string CreatedBy { get; set; }
        public long FeedBackId { get; set; }
        public int LiveComplaintId { get; set; }
        public string MercuryNumber { get; set; }
        public string Seriousness { get; set; }
        public string Complaint1 { get; set; }
        public string UserId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerPhone { get; set; }

        public string IncidentDate { get; set; }

        public string CustomerInfo { get; set; }
        public int PageCount { get; set; }
        public long ComplaintId { get; set; }
    }

    public class DriverComplaintViewModel
    {
        [Display(Name = "Drivers Name")]
        public string DriverUseId { get; set; }

        public string DriverName { get; set; }
        [Display(Name = "Drivers E-Mail")]
        public string DriverEmail { get; set; }
        public long FeedBackId { get; set; }
        public bool ComplaintId { get; set; }
        public int LiveComplaintId { get; set; }
        public int Type { get; set; }
        public string MercuryNumber { get; set; }
        [Display(Name = "Complaint Seriousness")]
        public string Seriousness { get; set; }
        [Display(Name = "Complaint")]
        [AllowHtml]
        [Required(ErrorMessage = "Email body is required")]
        public string MessageContant { get; set; }
        public string UserId { get; set; }
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }
        [Display(Name = "Customer E-Mail")]
        public string CustomerEmail { get; set; }
        [Display(Name = "Contact no")]
        [Required]
        public string CustomerPhone { get; set; }
        public string IncidentDate { get; set; }
        public string CreatedBy { get; set; }
        [Display(Name = "Vehicle no")]
        public string VehicleNo { get; set; }
        [Display(Name = "Event Title")]
        public string EventTitle { get; set; }
        public int EventTitleId { get; set; }

    }

    public class DriverAppreciationViewModel
    {
        [Display(Name = "Event Title")]
        public string AppreciationTitle { get; set; }
        public int AppreciationTitleId { get; set; }
        [Display(Name = "Drivers Name")]
        public string DriverName { get; set; }
        public string DriverUseId { get; set; }

        public string DriverEmail { get; set; }
        public long FeedBackId { get; set; }
        public string CreatedBy { get; set; }
        public long DriverAppreciationId { get; set; }
        public long CustomerFeedbackId { get; set; }
        [AllowHtml]
        [Required(ErrorMessage = "Email body is required")]
        public string MessageContant { get; set; }
        [Display(Name = "Customer Name")]
        public string CustomerName { get; set; }
        [Display(Name = "Customer E-Mail")]
        public string CustomerEmail { get; set; }
        [Display(Name = "Contact no")]
        [Required]
        public string CustomerPhone { get; set; }
        public string DriverUserId { get; set; }
    }


}