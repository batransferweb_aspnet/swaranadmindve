﻿using SwaranAdmin_New.Models.PriceCalculation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SwaranAdmin_New.Models.Pricing
{
    public class CustomerRateViewModel
    {
        public long RateId { get; set; }
        public string RateName { get; set; }
        public string RateType { get; set; }
        //public long VatRateTd { get; set; }
        //public bool IsDelete { get; set; }
        //public bool IsDistanceShow { get; set; }
        //public bool IsSpeedShow { get; set; }
        //public DateTime? CreatedDate { get; set; }
        //public DateTime? ModifiedDate { get; set; }
        //public bool IsDefault { get; set; }
        public bool IsActive { get; set; }

        //public RateVehicleList RateVehicleList { get; set; }
    }

    public class RateVehicleList
    {
        public List<RateVehicleType> RateVehicleTypes { get; set; }
    }

    public class CustomerRateDetailModel
    {
        public long RateId { get; set; }
        public string RateName { get; set; }
        public string RateType { get; set; }
        public long VatRateId { get; set; }
        //public bool IsDelete { get; set; }
        public bool IsDistanceShow { get; set; }
        public bool IsSpeedShow { get; set; }
        //public DateTime? CreatedDate { get; set; }
        //public DateTime? ModifiedDate { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }

    }

    public class RateVehicleType
    {
        public long RateCustomerVehicleTypeId { get; set; }
        public long RateId { get; set; }
        //public VehicleTypeDom VehicleTypeDetail { get; set; }
        public long VehicleTypeId { get; set; }
        public string VehicleTypeName { get; set; }
    }

    public class RateCustomerDistanceDayDetail
    {
        public long RateCustomerVehicleTypeId { get; set; }
        public List<RateCustomerDistanceDayDom> RateCustomerDistanceDayRecords { get; set; }
    }

    public class RateCustomerDistanceDayDom
    {
        public long RateCustomerDistanceDayId { get; set; }
        public string DayName { get; set; }
    }
    
    public class RateCustomerDistanceDom
    {
        public long RateCustomerDistanceDayId { get; set; }
        public long DistanceId { get; set; }
        public decimal DistanceFrom { get; set; }
        public decimal DistanceTo { get; set; }
        public decimal Amount { get; set; }
        public string Unit { get; set; }
        public bool IsDefault { get; set; }
    }

    public class VehicleTypeDom
    {
        public long VehicleTypeId { get; set; }
        public string VehicleTypeName { get; set; }
    }

    public class NewRateVehicleRequest
    {
        public long RateId { get; set; }
        [DisplayName("Vehicle Type")]
        public long VehicleTypeId { get; set; }
        //public List<VehicleTypeDom> AvailableVehicleTypes { get; set; }
        public int Day { get; set; }
    }

    public class RateCustomerDistanceForUpdate
    {
        public long CurrentRateCustomerDistanceDayId { get; set; }
        public long CurrentDistanceId { get; set; }
        public decimal CurrentDistanceFrom { get; set; }
        public decimal CurrentDistanceTo { get; set; }
        public decimal CurrentAmount { get; set; }
        public string CurrentUnit { get; set; }
        public bool CurrentIsDefault { get; set; }
        public long NextDistanceId { get; set; }
        public long PreviousDistanceId { get; set; }
    }

    public class RateCustomerDistanceForDelete
    {
        public long CurrentRateCustomerDistanceDayId { get; set; }
        public long CurrentDistanceId { get; set; }
        public decimal CurrentDistanceFrom { get; set; }
        public decimal CurrentDistanceTo { get; set; }
        public bool CurrentIsDefault { get; set; }
    }

    public class RateCustomerDistanceForCreate
    {
        public long CurrentRateCustomerDistanceDayId { get; set; }
        public long CurrentDistanceId { get; set; }
        public decimal CurrentDistanceFrom { get; set; }
        public decimal CurrentDistanceTo { get; set; }
        public decimal CurrentAmount { get; set; }
        public string CurrentUnit { get; set; }
        public bool CurrentIsDefault { get; set; }        
    }

    public class RateCustomerVehicleForCreate
    {
        public long RateId { get; set; }
        public long VehicleTypeId { get; set; }
        //public int Day { get; set;}
    }

    public class RateCustomerVehicleDayForCreate
    {
        public long RateId { get; set; }
        public long VehicleTypeId { get; set; }
        //public int Day { get; set;}
        public long RateCustomerVehicleTypeId { get; set; }
        public int Day { get; set; }
        public List<RateCustomerDistanceDom> DistanceDetail { get; set; }
    }

    public class RateCustomerVehicleDayForCreateResponse
    {
        public long RateId { get; set; }
        //public int Day { get; set;}
        public long RateCustomerVehicleTypeId { get; set; }
    }


    public class NewRateVehicleDayRequest
    {
        public long RateId { get; set; }
        public long RateCustomerVehicleTypeId { get; set; }
        public long VehicleTypeId { get; set; }
        public string VehicleTypeName { get; set; }
        public int Day { get; set; }
        public string DayName { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        //public long RateCustomerVehicleTypeId { get; set; }
        //public List<VehicleTypeDom> AvailableVehicleTypes { get; set; }
        public List<RateCustomerDistanceDom> DistanceDetail { get; set; }
    }

    public class CustomerRateDom
    {
        public long RateId { get; set; }
        public string RateName { get; set; }
        public long CustomerRateVehicleTypeId { get; set; }
        public long VehicleTypeId { get; set; }
        public string VehicleTypeName { get; set; }
        public string DayName { get; set; }
    }

    public class RateDailySurge
    {
        //public long DailySurgeId { get; set; }
        public long RateId { get; set; }
        //public VehicleTypeDom VehicleTypeDetail { get; set; }
        public int DayId { get; set; }
        public string DayName { get; set; }
    }

    public class RateDailySurgeDayDom
    {
        public long DailySurgeId { get; set; }
        public long CustomerRateId { get; set; }
        public string CustomerRateName { get; set; }
        public int DayId { get; set; }
        public string DayName { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public decimal SurgePercentage { get; set; }
        public decimal SurgeAmount { get; set; }
        public bool IsPercentageEnabled { get; set; }
        public bool IsActive { get; set; }
        public bool IsAllPlot { get; set; }
        public long[] AreaPointIds { get; set; }
    }

    public class AllDailySurgeViewModel
    {
        public List<POIAddressViewModel> AllPoiAddress { get; set; }
        public List<POIAddressViewModel> SpecialOccAddress { get; set; }
    }
}