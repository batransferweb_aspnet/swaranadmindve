﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwaranAdmin_New.Models.PriceCalculation
{
    public class DailySurgeViewModel
    {
        public long DailySurgeId { get; set; }
        public long CustomerRateId { get; set; }        
    }
}