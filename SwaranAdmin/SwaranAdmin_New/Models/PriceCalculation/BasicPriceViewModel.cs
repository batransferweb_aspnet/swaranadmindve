﻿using System.Collections.Generic;
using System.ComponentModel;

namespace SwaranAdmin_New.Models
{

    public class AddressViewModel
    {
        public int AddressId { get; set; }
        public int AreaPointId { get; set; }
        public int AreaTypeId { get; set; }
        public string Airportcode { get; set; }
        public string AreaTypeName { get; set; }
        public string FullAddress { get; set; }
        public string OrganisationName { get; set; }
        public string DepartmentName { get; set; }
        public string BuildingName { get; set; }
        public string SubBuildingName { get; set; }
        public int? BuildingNumber { get; set; }
        public string Street { get; set; }
        public string PostTown { get; set; }
        public string Postcode { get; set; }
        public string CountryCode { get; set; }
        public decimal Lat { get; set; }
        public decimal Longi { get; set; }
    }

    public class FixedPriceViewModel
    {
        public long FixedPriceId { get; set; }
        public int FromAriapointId { get; set; }
        public string FromLocation { get; set; }
        public int ToAriapointId { get; set; }
        public string ToLocation { get; set; }
        public decimal BasicPrice { get; set; }
        public decimal FromPickupCharge { get; set; }
        public decimal FromDropoffCharge { get; set; }
        public decimal ToPickupCharge { get; set; }
        public decimal ToDropOffCharge { get; set; }
        public bool IsAwayFrom { get; set; }
        public bool IsAwayTo { get; set; } 
    }

    public class PointofInterestViewModel
    {

        public long AriapointId { get; set; }
        public string Location { get; set; }
        public string AreaType { get; set; }

        public decimal PickupCharge { get; set; }
        public decimal DropoffCharge { get; set; }
        [DisplayName("Is Outer London")]
        public bool IsAway { get; set; }

    }

    public class BasicPriceViewModel
    {
        public long FixedPriceId { get; set; }
        public string FromLocation { get; set; }
        public string ToLocation { get; set; }
        public decimal BasicPrice { get; set; }
    }

    public class MultiplePriceUpdateViewModel
    {
        public List<BasicPriceViewModel> FixedPrices { get; set; }
        public List<BasicPriceViewModel> NonAddedFixedPrices { get; set; }
        public decimal BasicPrice { get; set; }
        public long[] AriaPointId { get; set; }
    }



}