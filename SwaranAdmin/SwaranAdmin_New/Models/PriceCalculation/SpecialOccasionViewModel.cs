﻿using SwaranAdmin_New.CustomValidation;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SwaranAdmin_New.Models.PriceCalculation
{
    public class SpecialOccasionViewModel
    {
        public long OccasionsId { get; set; }
        public long CustomerRateId { get; set; }
        [Required]
        public string RateFormula { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string FromDate { get; set; }
        [Required]
        public string ToDate { get; set; }
        public string Comments { get; set; }
        public string RateName { get; set; }
        [DisplayName("Surcharge *")]
        [Required]
        public decimal Surcharge { get; set; }
        //[SpecialOccationValidation(OccasionsIdProperty = "OccasionsId", CustomerRateIdProperty = "CustomerRateId")]
        public string OccasionDataRange { get; set; }
        [DisplayName("Is Enabled")]
        public bool IsEnabled { get; set; }
        [DisplayName("Is Deleted")]
        public bool IsDeleted { get; set; }

        //public bool IsAllPlot { get; set; }
        public int IsAllPlot { get; set; }

        //public int[] AriaPointId { get; set; }
        //public SpecialOccasionAreaPointDom[] AreaPointId { get; set; }
        //public SpecialOccasionAreaPointDom[] FixedPriceId { get; set; }             
        public long[] AreaPointId { get; set; }
        public long[] FixedPriceId { get; set; }
        //public List<SelectListItem> AriaPointIds { get; set; }

        [DisplayName("Dropoff Charge *")]
        [Required]
        public decimal DropOffCharge { get; set; }

        public string DropOffComments { get; set; } 
        public bool IsBothAreapointApplied { get; set; }
    }

    public class SpOccCustomerRateViewModel
    {
        public long RateId { get; set; }
        public string RateName { get; set; }

    } 
    public class AllSpecialOccationViewModel
    {
        public List<POIAddressViewModel> AllPoiAddress { get; set; }
        public List<POIAddressViewModel> SpecialOccAddress { get; set; }  
    }
    public   class POIAddressViewModel 
    {
        public long AriaPointId { get; set; }
        public string AreaGroup { get; set; }
        public string FullPostcodeWithoutSpace { get; set; }
        public string FullPostcode { get; set; }
        public string OuterPostcode { get; set; }
        public string InnerPostcode { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string SearchName { get; set; }
        public string AreaType { get; set; }
        public long AreaTypeId { get; set; }
        public decimal Lat { get; set; }
        public decimal Long { get; set; }
        public decimal LiveLat { get; set; }
        public decimal LiveLong { get; set; }
        public string Town { get; set; }
        public bool IsDisplay { get; set; }
        public bool IsUpdated { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public decimal PickupCharge { get; set; }
        public decimal DropoffCharge { get; set; }
        public int AddressId { get; set; }
        public bool IsDefault { get; set; }
    }

    public class SaveFixedPriceDom
    {
        public long FromAreaPointId { get; set; }
        public long ToAreaPointId { get; set; }
        public decimal BasicPrice { get; set; }
        public decimal Distance { get; set; }
        public decimal Duration { get; set; }
    }

    public class FixedPriceDistanceTimeCalculationDom
    {
        //public long FromAreaPointId { get; set; }
        public string FromCoordinates { get; set; }
        //public string FromLong { get; set; }
        public string FromFullAddress { get; set; }
        //public long ToAreaPointId { get; set; }
        public string ToCoordinates { get; set; }
        //public string ToLong { get; set; }
        public string ToFullAddress { get; set; }
        //public decimal BasicPrice { get; set; }
        //public decimal Distance { get; set; }
        //public decimal Duration { get; set; }
    }

    public class SpecialOccasionAreaPointDom
    {
        public long? FromAreaPointId { get; set; }
        public long? ToAreaPointId { get; set; }
        public long? FixedPriceId { get; set; }
    }

    public class SpecialOccasionFixedPriceDom
    {
        public long? FromAreaPointId { get; set; }
        public long? ToAreaPointId { get; set; }
        public long? FixedPriceId { get; set; }
    }

}