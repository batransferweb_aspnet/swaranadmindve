﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwaranAdmin_New.Models
{
    public class EmailCredentialsViewModel
    {
        public string DisplayName { get; set; }
        public string FromEmail { get; set; }
        public bool IsEnabledSSL { get; set; }
        public string Password { get; set; }

        public int Port { get; set; }
        public string Smtp { get; set; }
        public string Username { get; set; }
    }
    public class GoogleApiVal
    {
        public decimal Distance { get; set; }
        public int Duration { get; set; }
    }
    public class GoogleApiCurrentLocation
    {
        public string Town { get; set; }
        public string PostCode { get; set; }
        public string Address { get; set; }

    }

}