﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.util;
using System.Web;

namespace SwaranAdmin_New.Models.TeleBooking
{
    public class TelVehicleTypeViewModel 
    {
        public long VehicleId { get; set; } 
        public string Name { get; set; } 
    }

    public class PriceCalOutViewModel
    {
        public decimal Amount { get; set; }
        public decimal PickupCharge { get; set; }
        public decimal Dropoffcharge { get; set; }
        public decimal OnewaySurcharge { get; set; }
        public decimal AccountFee { get; set; }
        public string SurChDec { get; set; }

        public string DropoffsurChDec { get; set; }
        public string NoEmissionDes { get; set; }
         
        public List<BookingVehicleDom> listBookingVehicle { get; set; }
    }

    public class ViaConfigurationViewModel
    {
        public decimal MinimumRate { get; set; }
        public decimal MileRate { get; set; }
        public decimal ViaTimeRate { get; set; }
        public bool IsFastRoute { get; set; }
    }
    public class TeleBookingViewModel
    {
    }
}