﻿using System.ComponentModel;

namespace SwaranAdmin_New.Models
{
    public class RefundHistoryViewModel
    {
        public long BookingId { get; set; }
        public string BookingNumber { get; set; }
        public string BookedPersonName { get; set; }
        public string BookingFrom { get; set; }
        public string BookingTo { get; set; }
        public string BookingStatus { get; set; }
        public string RefundBy { get; set; }
        public string Reason { get; set; }
        public string RefundDate { get; set; }
    }
    public class AdminViewModel
    {
    }

    public class ViapintConfigViewModel
    {
        [DisplayName("Minimum Rate")]
        public decimal MinimumRate { get; set; }
        [DisplayName("Mile Rate")]
        public decimal MileRate { get; set; }
        [DisplayName("Time Rate")]
        public decimal TimeRate { get; set; } 
        [DisplayName("Is Fast Route")]
        public bool IsFastViaRoute { get; set; }
    }

    public class PushNotificationConfigModel
    {
        public int PushNotificationTypeEnumId { get; set; }
        public string PushNotificationTypeEnumName { get; set; }
        public string Message { get; set; }
        public string Title { get; set; }
        public bool IsAppendBookingNumberForMessage { get; set; }
        public bool IsAppendBookingNumberForTitle { get; set; }

    }
}