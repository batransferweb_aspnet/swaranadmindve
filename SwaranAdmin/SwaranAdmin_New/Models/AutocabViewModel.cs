﻿using System.ComponentModel;

namespace SwaranAdmin_New.Models
{
    public class AutoCabVehicletypeConfigViewModel
    {
        [DisplayName("Vehicletype")]
        public int VehicleTypeId { get; set; }
        [DisplayName("Autocab Vehicletype")]
        public string VehicleTypeName { get; set; }
        [DisplayName("Autocab Vehicle Category")]
        public string VehicleCatogory { get; set; }
    }

    public class AutocabVehicleTypeViewModel
    {
        public long VehicleTypeId { get; set; }

        public string Name { get; set; }
    }
    public class AutocabViewModel
    {
    }
}