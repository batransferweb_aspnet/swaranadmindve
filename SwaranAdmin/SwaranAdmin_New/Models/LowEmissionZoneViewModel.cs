﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SwaranAdmin_New.CustomValidation;

namespace SwaranAdmin_New.Models
{
    public class LowEmissionZoneViewModel
    {
        public string FullAddress { get; set; } 
        public long ZoneId { get; set; } 
       
        [Required]
        public string Postcode { get; set; } 
        public int EmissionType { get; set; } 
        public string Title { get; set; } 
        public string Comments { get; set; } 
        public string RateName { get; set; }  
        public string TimeRange { get; set; }
        public string EmissionRateFormula { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public List<Streets> StreetList { get; set; }
        public List<ZoneViewmodel> Zones { get; set; }
        public List<EmissionDayViewmodel> Days { get; set; }
        public List<TimeViewModel> TimeRangeList { get; set; }  

    }

    public class TimeViewModel 
    {
        public string TimeRangeid { get; set; }
        public string TimeRange { get; set; } 
    }

    public class Streets
    {
        public long AddressId { get; set; }  
        public string Name { get; set; } 
    }

    public class LowEmissionZoneDayViewModel 
    {

        public string RateName { get; set; } 
        public List<ZoneViewmodel> Zones { get; set; }
        public List<EmissionDayViewmodel> Days { get; set; } 

        public string StartTime { get; set; }
        public int EndTime { get; set; }  
        public string EmissionRateFormula { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }

    }

    public class EmissionDayViewmodel 
    {
        public int DayId { get; set; } 
        public string Name { get; set; }
    }

    public class ZoneViewmodel 
    {
        public long ZoneId { get; set; } 
        public string Name { get; set; }
    }

    public class EmissionZoneViewModel
    { 
      public long  ZoneId  { get; set; }
      public long AddressId  { get; set; }
      public string  Title  { get; set; }
      public string  StreetName  { get; set; }
      public string  Postcode  { get; set; } 
      public string  EmissionType  { get; set; }
      public string  Description  { get; set; }
    }
}