﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwaranAdmin_New.Models
{
    public class AddressPointViewModel
    {
        public long AriaPointId { get; set; }
        public string FullPostcode { get; set; }

        public string Name { get; set; }

        public string AreaType { get; set; }

        public decimal Lat { get; set; }

        public decimal Long { get; set; }
    }







    public class MainAddress
    {
        public List<AddressDom> Addresses { get; set; }
    }

    public class AddressDom
    {
        public long FavouriteAddressId { get; set; }
        public long AddressId { get; set; }
        public string AirportCode { get; set; }
        public int AreaPointId { get; set; }
        public long AreaTypeId { get; set; }
        public string AreaTypeName { get; set; }
        public string BuildingName { get; set; }
        public string BuildingNumber { get; set; }
        public string CountryCode { get; set; }
        public string DepartmentName { get; set; }
        public string FullAddressLabel { get; set; }
        public string FullAddress { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Note { get; set; }
        public string OrganisationName { get; set; }
        public string PostTown { get; set; }
        public string Postcode { get; set; }
        public string Street { get; set; }
        public string SubBuildingName { get; set; }
        public bool IsNonElastic { get; set; }
    }

    public class FavouriteAddressViewModel
    {

        public long FavouriteAddressId { get; set; }
        public long AddressId { get; set; }
        public string PickupNote { get; set; }
        public string Label { get; set; }

        public string FullAddress { get; set; }

        public string AirportCode { get; set; }
        public int AreaPointId { get; set; }
        public long AreaTypeId { get; set; }
        public string AreaTypeName { get; set; }
        public string BuildingName { get; set; }
        public string BuildingNumber { get; set; }
        public string CountryCode { get; set; }
        public string DepartmentName { get; set; }
        public string FullAddressLabel { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Note { get; set; }
        public string OrganisationName { get; set; }
        public string PostTown { get; set; }
        public string Postcode { get; set; }
        public string Street { get; set; }
        public string SubBuildingName { get; set; }
        public bool IsNonElastic { get; set; }

    }
}