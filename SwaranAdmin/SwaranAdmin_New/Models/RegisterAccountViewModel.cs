﻿using System.ComponentModel.DataAnnotations;

namespace SwaranAdmin_New.Models
{
    public class TitleViewModel
    {
        public int TitleId { get; set; }

        public string TitleName { get; set; }

    }

    public class CountryInfoViewModel
    {
        public long CountryId { get; set; }

        public string CountryName { get; set; }

        public string CountryCode { get; set; }

        public string PhoneCode { get; set; }
        public string PhoneDisplayCode { get; set; }
    }

    public class PassengersViewMode
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }

    public class NumberOfLuggagesViewMode
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }

    public class DriverEllapseViewMode
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
    public class NumberOfHandLuggagesViewMode
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
    public class EmailTokenViewModel
    {
        public string UserId { get; set; }
        public string Email { get; set; }
        [Required(ErrorMessage = "Token is required")]
        public string CurrentEmailToken { get; set; }

    }

    public class CreateEmailTokenViewModel
    {
        // public string UserId { get; set; }
        public string Email { get; set; }

    }

    public class EmailVerificationViewModel
    {
        public string UserId { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }

    }

    public class AccountViewModel
    {
    }

    public class NumberOfChildSeatViewMode
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
    public class NumberOfBoosterSeatViewMode
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
    public class NumberOfInfantSeatViewMode
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }

    public class ForgotPasswordResetViewMode
    {
        public string UserId { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class SavedCardViewModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
