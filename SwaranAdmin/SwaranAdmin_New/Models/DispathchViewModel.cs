﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SwaranAdmin_New.Models
{
    public class DispathchViewModel
    {
        public string BookingToken { get; set; }

        public int TimeLeftId { get; set; }

        public long? BookingId { get; set; }
        public string Name { get; set; }
        public string BookingNumber { get; set; }

        public string BookingStatus { get; set; }
        public string BookingTimeSpan { get; set; }
        public string BookingDateTime { get; set; }
        public string TimeLeft { get; set; }
        public string PaymentType { get; set; }
        public string BookingSource { get; set; }
        public int? Journeys { get; set; }
        public string Loyalty { get; set; }
        public string Country { get; set; }
    }

    public class DispathchSearchViewModel
    {
        public int paymentStatus { get; set; }
        public int bookingStatus { get; set; }
        public int siteId { get; set; }
        public string date { get; set; }
        //public string month { get; set; }
        //public string year { get; set; }
        public string timeZoneInfo { get; set; }
    }
    public class CurrentBookingViewModel
    {
        [Display(Name = "Booking Number")]
        public string BookingNumber { get; set; }
        [Display(Name = "Booked Person Name")]
        public string BookedPersonName { get; set; }
        [Display(Name = "Member Id")]
        public long MemberId { get; set; }

        [Display(Name = "Loyalty")]
        public string Loyalty { get; set; }

        [Display(Name = "Booked Person Mobile")]
        public string BookedPersonMobileNo { get; set; }


        [Display(Name = "Alternative Mobile")]
        public string AlternativeMobile { get; set; }
        [Display(Name = "Booked Person Email")]
        public string BookedPersonEmail { get; set; }
        [Display(Name = "Booking Date")]
        public string BookingDate { get; set; }

        [Display(Name = "Booking Time")]
        public string BookingTime { get; set; }

        [Display(Name = "Date/Time Reserved")]
        public string ReservedDateTime { get; set; }
        [Display(Name = "Pickup")]
        public string BookingFrom { get; set; }
        [Display(Name = "Drop Off")]
        public string BookingTo { get; set; }
        [Display(Name = "Booking Status")]
        public string BookingStatus { get; set; }
        [Display(Name = "Vehicle Type")]
        public string VehicleType { get; set; }
        [Display(Name = "Passengers")]
        public int NumOfPassengers { get; set; }

        [Display(Name = "ChildSeats")]
        public int ChildSeats { get; set; }
        [Display(Name = "BoosterSeat")]
        public int BoosterSeat { get; set; }
        [Display(Name = "InfantSeats")]
        public int InfantSeats { get; set; }
        [Display(Name = "Card PaymentType")]
        public string CardPaymentType { get; set; }

        public int CardPaymentTypeId { get; set; }
        [Display(Name = "Card Name")]
        public string CardType { get; set; }

        public int CardTypeId { get; set; }

        [Display(Name = "Journey Type")]
        public string JourneyType { get; set; }

        [Display(Name = "Hand Luggage")]
        public int NumberOfHandLuggages { get; set; }
        [Display(Name = "Luggage")]
        public int NumberOfLuggages { get; set; }

        [Display(Name = "Total Amount")]
        public decimal TotalAmount { get; set; }

        [Display(Name = "Discount")]
        public decimal Discount { get; set; }
        [Display(Name = "Passenger Email")]
        public string PassengerEmail { get; set; }
        [Display(Name = "Passenger Name")]
        public string PassengerName { get; set; }
        [Display(Name = "Mobile")]
        public string LoyaltyPassengerMobileNo { get; set; }
        [Display(Name = "Payment Method")]
        public string PaymentMethod { get; set; }

        public int PaymentMethodId { get; set; }
        [Display(Name = "Payment Status")]
        public string PaymentStatus { get; set; }
        [Display(Name = "Customer Comment")]
        public string CustomerComment { get; set; }
        [Display(Name = "Customer Comment")]

        public string ReceiptNumber { get; set; }
        public string ModifiedDate { get; set; }
        [Display(Name = "Driver Ellapse Time")]
        public int DriverEllapseTime { get; set; }

        public string FlightNo { get; set; }
        [Display(Name = "Driver Display Name")]
        public string DriverDisplayName { get; set; }

        public bool IsReturnBooking { get; set; }
        public long? ReferenceBookingId { get; set; }
        [Display(Name = "Country")]
        public string Country { get; set; }
        [Display(Name = "Ip Address")]
        public string IpAddress { get; set; }
        public string CreatedDate { get; set; }
        public int? FromAriaPointTypeId { get; set; }
        public int? ToAriaPointTypeId { get; set; }
        [Display(Name = "Customer Comment")]
        public string BookingDateTimeReturn { get; set; }
        public string BookingFromReturn { get; set; }
        public string BookingToReturn { get; set; }
        public int DriverEllapseTimeReturn { get; set; }
        public string FlightNoReturn { get; set; }
        public string DriverDisplayNameReturn { get; set; }
        public decimal TotalDistancePriceReturn { get; set; }
        public int? FromAriaPointTypeIdReturn { get; set; }
        public int? ToAriaPointTypeIdReturn { get; set; }
        public decimal OtherCharges { get; set; }
        public decimal TotalPrice { get; set; }
        public string PrimaryPhoneNumber { get; set; }
        public string PassengerPhoneNumber { get; set; }
        public string BookingToken { get; set; }

        public bool IsConfirmBooking { get; set; }

        public bool IsCancelBooking { get; set; }

    }

    public class PaymentViewModel
    {
        public List<PaymentTypeDom> AllOtherPaymentTypes { get; set; }
       // public PaymentTypeCardDom PaymentTypeCard { get; set; }
    }

   
    public class PaymentTypeDom
    {
        public int PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }

    }
    public class PaymentTypeCardDom
    {
        public int PaymentTypeId { get; set; }
        public string PaymentTypeName { get; set; }
        public List<CardTypeDom> CardTypes { get; set; }
        public List<CardPaymentTypeDom> CardPaymentTypes { get; set; }
    }

    public class CardTypeDom
    {
        public long CardId { get; set; }
        public string Name { get; set; }
        public int ChargeType { get; set; }
        public decimal Value { get; set; }
        public string ImageUrl { get; set; }
    }
    public class CardPaymentTypeDom
    {
        public int CardPaymentTypeId { get; set; }
        public string CardPaymentTypeName { get; set; }

    }

    public class BookingFeeViewModel
    {
        public decimal BookingFee { get; set; }
        public decimal AdditionalCharge { get; set; }
    }
    public class UserCreditViewModel
    {
        public decimal AvailableCashBalance { get; set; }
        public decimal CardProcesingFee { get; set; }
    }

    public class MailCustomeViewModel
    {
        [Required(ErrorMessage = "Please enter valid email address")]
        [EmailAddress]
        public string ToAddress { get; set; }
        [Required(ErrorMessage = "Email Subject is required")]
        public string Subject { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = "Email body is required")]
        public string Message { get; set; }
    }
    public class MailAdminViewModel
    {
        [Required(ErrorMessage = "Please enter valid email address")]
        [EmailAddress]
        public string ToAddress { get; set; }
        [Required(ErrorMessage = "Email Subject is required")]
        public string Subject { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = "Email body is required")]
        public string Message { get; set; }
    }
}