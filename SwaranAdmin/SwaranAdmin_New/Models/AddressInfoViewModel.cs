﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SwaranAdmin_New.CustomValidation;

namespace SwaranAdmin_New.Models
{
    //public class AddressInfoViewModel  
    //{
    //    public long VehicleTypeId { get; set; } 
    //    public long? AddressSearchId { get; set; }
    //    public long FromFavouriteAddressId { get; set; }
    //    public long ToFavouriteAddressId { get; set; }
    //    public bool IsNonElasticFromAdd { get; set; }
    //    public bool IsNonElasticToAdd { get; set; }
    //    public string BidReference { get; set; }
    //    public string AutocabVehicleType { get; set; }
    //    public string AutocabVehicleCategory { get; set; }
    //    public long EmissionZoneId { get; set; }
    //    public string UserId { get; set; }
    //    [BookingDateCustomValidation]
    //    [Required(ErrorMessage = "Date is Required")]
    //    [Display(Name = "PickUp Date")]
    //    [DataType(DataType.Date)]
    //    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    //    // [BookingDateCustomValidation(ErrorMessage = "Invalid Date")]
    //    public string BookingDate { get; set; }

    //    [BookingDateTimeCustomValidation(BookingDateProperty = "BookingDate")]
    //    [Display(Name = "Pick Up Time")]
    //    [Required(ErrorMessage = "Time is Required")]
    //    public string BookingTime { get; set; }
    //    // [Required(ErrorMessage = "Invalid Pickup Point")]

    //    public string ClientDatetTime { get; set; }
    //    [Required(ErrorMessage = "Invalid Pickup Point")]
    //    public string BookingFrom { get; set; }

    //    [Required(ErrorMessage = "Invalid Dropoff Point")]
    //    public string BookingTo { get; set; }

    //    //  [Required(ErrorMessage = "Invalid Pickup Point")]
    //    [BookingFromAddressValidation(ToPostcodeProperty = "ToPostCode", FromPostcodeProperty = "FromPostCode")]
    //    [RegularExpression(@"^((?![<>]).)*$", ErrorMessage = "Invalid Pickup Point")]
    //    public string BookingFromLabel { get; set; }
    //    [HiddenInput(DisplayValue = false)]
    //    public string FromPostCode { get; set; }

    //    [BookingToAddressValidation(ToPostcodeProperty = "ToPostCode", FromPostcodeProperty = "FromPostCode")]
    //    [RegularExpression(@"^((?![<>]).)*$", ErrorMessage = "Invalid Dropoff Point")]
    //    public string BookingToLabel { get; set; }
    //    [HiddenInput(DisplayValue = false)]

    //    public string ToPostCode { get; set; }

    //    [Required(ErrorMessage = "Required")]
    //    [Range(1, 50, ErrorMessage = "Please select the passengers")]
    //    public int NumOfPassengers { get; set; }

    //    [Required(ErrorMessage = "Required")]
    //    public int StudentDiscount { get; set; }
    //    public decimal StudentDiscountAmount { get; set; }
    //    public string PickupNote { get; set; }
    //    public long FromAddressId { get; set; }
    //    public int FromAriapointTypeId { get; set; }
    //    //[BookingFromAddressValidation(ToPostcodeProperty = "ToPostCode")]
    //    //[HiddenInput(DisplayValue = false)]
    //    //[RegularExpression(@"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})", ErrorMessage = "Invalid Pickup Point")]
    //    // public string FromPostCode { get; set; }
    //    public string FromAirportCode { get; set; }
    //    public long FromFixedPriceAriaPointId { get; set; }
    //    public decimal FromLat { get; set; }
    //    public decimal FromLong { get; set; }
    //    public string FromOrganisationName { get; set; }
    //    public string FromDepartmentName { get; set; }
    //    public string FromBuildingName { get; set; }
    //    public string FromSubBuildingName { get; set; }
    //    public string FromBuildingNumber { get; set; }
    //    public string FromStreet { get; set; }
    //    public string FromPostTown { get; set; }
    //    public long ToAddressId { get; set; }
    //    public int ToAriapointTypeId { get; set; }
    //    //[HiddenInput(DisplayValue = false)]
    //    //[RegularExpression(@"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})", ErrorMessage = "Invalid Dropoff Postcode")]
    //    //[BookingAddressValidation(FromPostcodeProperty = "FromPostCode")]
    //    //public string ToPostCode { get; set; }
    //    public string ToAirportCode { get; set; }
    //    public long ToFixedPriceAriaPointId { get; set; }
    //    public decimal ToLat { get; set; }
    //    public decimal ToLong { get; set; }
    //    public string ToOrganisationName { get; set; }
    //    public string ToDepartmentName { get; set; }
    //    public string ToBuildingName { get; set; }
    //    public string ToSubBuildingName { get; set; }
    //    public string ToBuildingNumber { get; set; }
    //    public string ToStreet { get; set; }
    //    public string ToPostTown { get; set; }


    //    public decimal TotalDistance { get; set; }
    //    public decimal TotalDistancePrice { get; set; }
    //    public decimal TotalDistanceVia { get; set; }
    //    public decimal MainDistanceVia { get; set; }
    //    public decimal TotalViaDistance { get; set; }
    //    public decimal TotalOneWayDistance { get; set; }
    //    public decimal TotalTwoWayDistance { get; set; }
    //    public int TotalTime { get; set; }

    //    public string TotalTimeLabel { get; set; }
    //    public string StartTimeLabel { get; set; }
    //    public long RateId { get; set; }

    //    public string IpAddress { get; set; }

    //    public List<BookingVehicletypeCombinations> VehicletypeCombinations { get; set; }


    //    public List<ViaPointAddress> ViaPointAddress { get; set; }

    //    public string IsReturnJourney { get; set; }

    //    public bool IsRoboticFalse { get; set; }

    //    public decimal ViaMinimumRate { get; set; }
    //    public decimal ViaMileRate { get; set; }
    //    public bool IsFastViaRoute { get; set; }

    //    public decimal TotalViaTime { get; set; }
    //    public decimal MainTimeVia { get; set; }
    //    public decimal ViaTimeRate { get; set; } 
        

    //}

    public class ViaPointAddress
    {
        public long ViapointId { get; set; }
        public long AriaPoitId { get; set; }

        // [RegularExpression(@"^((?![<>]).)*$", ErrorMessage = "Invalid Dropoff Point")]
        public string FullAddress { get; set; }
        public int AriaTypeId { get; set; }
        public string OrganisationName { get; set; }
        public string DepartmentName { get; set; }
        public string BuildingName { get; set; }
        public string SubBuildingName { get; set; }
        public long? BuildingNumber { get; set; }
        public string Street { get; set; }
        public string PostTown { get; set; }
        [HiddenInput(DisplayValue = false)]
        //[Required(ErrorMessage = "Invalid Viapoint")]
        //[RegularExpression(@"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})", ErrorMessage = "Invalid Postcode")]
        public string Postcode { get; set; }
        public string CountryCode { get; set; }
        public decimal Lat { get; set; }
        public decimal Long { get; set; }
        public decimal VispointDistance { get; set; }
        public decimal VispointDistanceLast { get; set; }
    }

    //public class BookingVehicletypeCombinations
    //{
    //    public string BidReference { get; set; }
    //    public long CombinationId { get; set; }
    //    public string Name { get; set; }

    //    public string ImageUrl { get; set; }
    //    public int NumberOfVehicles { get; set; }
    //    public int NumberOfPassengers { get; set; }
    //    public int NumberOfLuggages { get; set; }
    //    public int NumberOfHandLuggages { get; set; }
    //    public int DisplayTotalVolume { get; set; }
    //    public int MaxNumberOfFoldeableSeats { get; set; }
    //    public bool HasChildSeates { get; set; }
    //    public bool IsNormal { get; set; }
    //    public bool IsVIPType { get; set; }
    //    public bool IsWheelchair { get; set; }
    //    public bool IsLuxurious { get; set; }
    //    public int PaymentType { get; set; }
    //    public decimal CashPrice { get; set; }
    //    public decimal StudentDiscount { get; set; }
    //    public decimal Distance { get; set; }
    //    public decimal PickupCharge { get; set; }
    //    public decimal DropoffCharge { get; set; }
    //    public decimal AccountFee { get; set; }

    //    public decimal CardPrice { get; set; }
    //    public decimal OnewaySurcharge { get; set; }
    //    public string OnewaySurchargeDescription { get; set; }
    //    public string DropoffSurchargeDescription { get; set; }
    //    public string NoEmissionDes { get; set; }

    //    public string ImageSm { get; set; }
    //    public string Description { get; set; }
    //    public int NumberOfChildSeat { get; set; }
    //    public int NumberOfBoosterSeat { get; set; }
    //    public int NumberOfInfantSeat { get; set; }

    //    public List<BookingVehicleDom> BookingVehicles { get; set; }
    //}

    public class ReturnJourney  
    {
        public string BidReference { get; set; }
        public string VehicleTypeName { get; set; }
        #region Retun Journey

        public string ReturnJourneyType { get; set; }
        [BookingFromAddressValidation(ToPostcodeProperty = "ToPostCode", FromPostcodeProperty = "FromPostCode")]
        [RegularExpression(@"^((?![<>]).)*$", ErrorMessage = "Invalid Pickup Point")]
        public string PickupPoint { get; set; }
        [HiddenInput(DisplayValue = false)]
        [RegularExpression(@"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})", ErrorMessage = "Invalid Pickup Point")]
        public string FromPostCode { get; set; }
        [BookingToAddressValidation(ToPostcodeProperty = "ToPostCode", FromPostcodeProperty = "FromPostCode")]
        [RegularExpression(@"^((?![<>]).)*$", ErrorMessage = "Invalid Dropoff Point")]
        public string DropoffPoint { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ToPostCode { get; set; }

        public long FromAddressId { get; set; }
        public long ToAddressId { get; set; }
        public int FromAriapointTypeId { get; set; }

        public long FromFixedPriceAriaPointId { get; set; }
        public decimal FromLat { get; set; }
        public decimal FromLong { get; set; }
        public string FromOrganisationName { get; set; }
        public string FromDepartmentName { get; set; }
        public string FromBuildingName { get; set; }
        public string FromSubBuildingName { get; set; }
        public string FromBuildingNumber { get; set; }
        public string FromStreet { get; set; }
        public string FromPostTown { get; set; }

        public int ToAriapointTypeId { get; set; }

        public long ToFixedPriceAriaPointId { get; set; }
        public decimal ToLat { get; set; }
        public decimal ToLong { get; set; }
        public string ToOrganisationName { get; set; }
        public string ToDepartmentName { get; set; }
        public string ToBuildingName { get; set; }
        public string ToSubBuildingName { get; set; }
        public string ToBuildingNumber { get; set; }
        public string ToStreet { get; set; }
        public string ToPostTown { get; set; }

        public string OneWayBookingDateTime { get; set; }

        [Required(ErrorMessage = "Return Journey Booking Date required")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]

        public string BookingDate { get; set; }
        [Required(ErrorMessage = "Return Journey Booking Time required")]
        [ReturnDateTimeValidation(BookingDateProperty = "BookingDate", OnewayDateTimeProperty = "OneWayBookingDateTime", ErrorMessage = "Return date should be later than pick up date")]
        public string BookingTime { get; set; }


        [Required(ErrorMessage = "Please select meeting time with driver!")]
        public int FlightDriverEllapseTime { get; set; }


        [Required(ErrorMessage = "Please select meeting time with driver!")]
        public int CruiseShipDriverEllapseTime { get; set; }



        public string DisplayNameFlight { get; set; }
        [Required(ErrorMessage = "Return Journey Flight No is required")]
        public string FlightNo { get; set; }
        [Required(ErrorMessage = "Return Journey Cruise ship or Ferry name is required")]
        public string CruiseShipName { get; set; }
        [Required(ErrorMessage = "Return Journey Display Name is required")]
        public string DisplayNameCruiseShip { get; set; }
        [Required(ErrorMessage = "Return Journey Landing Date is required")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string LandingDateFlight { get; set; }
        [ReturnFlightLandingDateTimeValidation(LandingDateFlightProperty = "LandingDateFlight", OnewayDateTimeProperty = "OneWayBookingDateTime", ErrorMessage = "Return date should be later than pick up date")]
        [Required(ErrorMessage = "Return Landing Time is required")]
        public string LandingTimeFlight { get; set; }



        [Required(ErrorMessage = "Return Journey Landing Date is required")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string LandingDateCruiseShip { get; set; }
        [ReturnCruiseShipLandingDateTimeValidation(LandingDateCruiseShipProperty = "LandingDateCruiseShip", OnewayDateTimeProperty = "OneWayBookingDateTime", ErrorMessage = "Return date should be later than pick up date")]
        [Required(ErrorMessage = "Return Landing Time is required")]
        public string LandingTimeCruiseShip { get; set; }


        #endregion
        public List<ViaPointAddress> ViaPointAddress { get; set; }

        // public bool IsReverseReturnJourney { get; set; }
        public List<BookingVehicleDom> BookingVehicles { get; set; }
        public string AutocabVehicleTypeName { get; set; }
        public string AutocabVehicleCategory { get; set; }

        public decimal ReturnDistance { get; set; }
        public int ReturnTime { get; set; }
        //   public string ReturnTimeLabel { get; set; } 
        public long RateId { get; set; }
        public string UserId { get; set; }
        public string ReturnDropoffDateTime { get; set; }
        public decimal OtherCharges { get; set; }
        //     [ReturnwayAmountValidation(ErrorMessage = "Invalid Return Price")]
        [Required(ErrorMessage = "Invalid Return Price")]
        public decimal ReturnWayPrice { get; set; }
        [OnewayAmountValidation(ErrorMessage = "Invalid Price")]
        public decimal OneWayPrice { get; set; }

        public double DateDiffCount { get; set; }

        //   public string UserId { get; set; }

        public decimal StudentDiscountAmount { get; set; }
        public decimal AccountFee { get; set; }
        public decimal PickupCharge { get; set; }
        public decimal DropoffCharge { get; set; }
        public string IsReturnJourney { get; set; }
        public decimal ReturnSurcharge { get; set; }
        public string ReturnwaySurchargeDescription { get; set; }
        public string ReturnwayDropoffSurchargeDescription { get; set; }

        public decimal MainDistanceVia { get; set; }

        public decimal TotalDistanceVia { get; set; }
        public decimal TotalViaDistance { get; set; }
        public int TotalReturnTime { get; set; }
        public bool IsVehicleAvailabel { get; set; }

        public decimal ViaMinimumRate { get; set; }
        public decimal ViaMileRate { get; set; }
        public bool IsFastViaRoute { get; set; }
        public string NoEmissionDes { get; set; }
        public long EmissionZoneId { get; set; }
    }


    public class ReservationModel  
    {
        public bool IsCABOperator { get; set; }
        public string OfficePhoneNo { get; set; }
        public long LoginId { get; set; }
        public bool IsVehicleUpgradeEnabeld { get; set; }
        public bool IsUpgradeByPoints { get; set; }
        public int UpgradeAmount { get; set; }
        public int UpgradePoints { get; set; }

        [Required(ErrorMessage = " Upgrade VehicleType is required")]
        public long UpgradeVehicleTypeId { get; set; }
        public long CurrentVehicletypeId { get; set; }
        public int AviPoints { get; set; }

        public int MinimumPoints { get; set; }
        public bool IsFreeJourneyAccessAmt { get; set; }

        public decimal FreeJourneyAccessAmt { get; set; }
        public string Loyalty { get; set; }
        public long BookingId { get; set; }

        public string BookingToken { get; set; }

        public string UserId { get; set; }

        public int FromAriapointTypeId { get; set; }

        public int ToAriapointTypeId { get; set; }
        public bool IsMemberBooking { get; set; }

        public string VehicleTypeName { get; set; }

        [Display(Name = "Passengers*")]
        public int NumOfPassengers { get; set; }

        public string BookingDateTime { get; set; }

        public string BookingToDateTime { get; set; }
        [Display(Name = "Landmark If Any")]
        public string PickupNote { get; set; }


        [Display(Name = "Country Code of Alternative No")]
        public string PassengerAlternativeMobileCountryCode { get; set; }
        [Display(Name = "Alternative Phone No")]
        [Phone(ErrorMessage = "Invalid Alternative Phone No ")]
        public string PassengerAlternativeMobileNo { get; set; }

        public int? PassengerTitle { get; set; }
        [Display(Name = "First Name*")]
        [Required(ErrorMessage = "First Name is required")]
        public string PassengerFirstName { get; set; }
        [Display(Name = "Last Name*")]
        [Required(ErrorMessage = "Last Name is required")]
        public string PassengerLastName { get; set; }
        [Display(Name = "Country Code Mobile No*")]
        public string PassengerMobileNumberCountryCode { get; set; }
        [Display(Name = "Mobile Phone No*")]
        [Phone(ErrorMessage = "Invalid Mobile Phone No ")]
        public string PassengerMobileNo { get; set; }
        [Display(Name = "Email*")]
        [EmailAddress(ErrorMessage = "The Email field is not a valid e-mail address.")]
        public string PassengerEmail { get; set; }
        public int? BookedPersonTitleId { get; set; }
        [Display(Name = "First Name*")]
        [Required(ErrorMessage = "First Name is required")]
        public string BookedPersonFirstName { get; set; }
        [Display(Name = "Last Name*")]
        [Required(ErrorMessage = "Last Name is required")]
        public string BookedPersonLastName { get; set; }
        [Display(Name = "Country Code of Mobile No*")]
        public string BookedPersonMobileNumberCountryCode { get; set; }
        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Mobile Phone required")]
        [Display(Name = "Mobile Phone No*")]
        [Phone(ErrorMessage = "Invalid Mobile Phone No ")]
        public string BookedPersonMobileNo { get; set; }
        [Display(Name = "Country Code of Alternative No")]
        public string BookedPersonAlternativeMobileCountryCode { get; set; }
        [Display(Name = "Alternative Phone No")]
        [DataType(DataType.PhoneNumber)]
        [Phone(ErrorMessage = "Invalid Alternative Phone No ")]
        public string BookedPersonAlternativeMobileNo { get; set; }
        [Display(Name = "Email*")]
        [EmailAddress(ErrorMessage = "The Email field is not a valid e-mail address.")]
        [Required(ErrorMessage = "Email is required")]
        public string BookedPersonEmail { get; set; }

        [Display(Name = "Is there anything else we need to know?")]
        [StringLength(500, ErrorMessage = "Can not be exit 500 characters")]
        public string CustomerComment { get; set; }

        public int BoosterSeat { get; set; }

        public int InfantSeats { get; set; }

        public int ChildSeats { get; set; }
        [Display(Name = "No of Check in Luggage")]
        [Required(ErrorMessage = "Number Of Luggages is required")]
        public int NumberOfLuggages { get; set; }
        [Display(Name = "No of Hand in Luggage")]
        [Required(ErrorMessage = "Number Of Hand Luggages is required")]
        public int NumberOfHandLuggages { get; set; }

        public int CustomerType { get; set; }

        [Required(ErrorMessage = "Journey Type is required")]
        public int JourneyTypeId { get; set; }
        public string JourneyType { get; set; }
        public string BookingPersonType { get; set; }
        public bool HasChildSeates { get; set; }


        //[Required(ErrorMessage = "Card Type is required")]
        //[Range(1, 10000, ErrorMessage = "Card Type is required")]
        //[Display(Name = "Card Type")]
        //public long? CardTypeId { get; set; }

        [Required(ErrorMessage = "Payment Type is required")]
        [Range(1, 100000, ErrorMessage = "Payment Type is required")]
        [Display(Name = "Payment Type")]
        public int? CardPaymentTypeId { get; set; }

        [Required(ErrorMessage = "Payment Method is required")]
        [Range(1, 100000, ErrorMessage = "Payment Method is required")]

        [Display(Name = "  Payment Method*")]
        public int PaymentMethodId { get; set; }

        public int PaymentStatusId { get; set; }

        public int PaymentTypeId { get; set; }
        [Range(1, 100000, ErrorMessage = "required")]
        [Display(Name = "New Card/Saved Card")]
        public int SaveCardId { get; set; }

        public bool IsSavedCardPayment { get; set; }

        [Display(Name = "  CVV Number*")]
        [Required(ErrorMessage = "CVV Numberis required")]
        public string CvcNumber { get; set; }
        public string CardHodelName { get; set; }
        [Display(Name = " Code*")]
        [Required(ErrorMessage = "Free journey code is required")]
        [FreeJourneyCodeValidation(UserIdProperty = "UserId", ErrorMessage = "Invalid free journey code")]
        public string FreeJourneyCode { get; set; }

        [Display(Name = "Use My Available Credits")]
        [Required(ErrorMessage = "Credits is required")]
        [UseMyCreditsValidation(UserIdProperty = "UserId", ErrorMessage = "Invalid credit amount")]
        public decimal MyCredits { get; set; }


        public string IpAddress { get; set; }//17

        public int JourneyPaymentType { get; set; }

        public long? CompanyId { get; set; }

        public decimal AccountFee { get; set; }
        public decimal AccountFeeReturnWay { get; set; }
        public string MapUrl { get; set; }





        [Required(ErrorMessage = "Please select meeting time with driver!")]
        [Display(Name = "When should the driver pick you up *")]
        public int DriverEllapseTime { get; set; }

        [Display(Name = "Display Name ")]
        public string DisplayNameFlight { get; set; }
        [Required(ErrorMessage = "Flight No is required")]
        [Display(Name = "Flight Number *")]
        public string FlightNo { get; set; }
        [Required(ErrorMessage = "Cruise ship or Ferry name is required")]
        [Display(Name = "Cruise Ship/Ferry Name*")]
        public string CruiseShipName { get; set; }
        [Required(ErrorMessage = "Display Name is required")]
        [Display(Name = "Display Name *")]
        public string DisplayNameCruiseShip { get; set; }
        [Required(ErrorMessage = "Landing Date is required")]

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string LandingDate { get; set; }
        [Required(ErrorMessage = "Landing Time is required")]

        [LandingDateTimeValidation(LandingDateProperty = "LandingDate", ErrorMessage = "Invalid Landing Time")]
        public string LandingTime { get; set; }
        public DateTime? LandingDateTime { get; set; }
        public bool IsAuthenticated { get; set; }
        public string Viapoints { get; set; }

        public bool Babyseat { get; set; }
        public string CountryName { get; set; }
        public bool IsSendPassengerEmail { get; set; }
        public bool IsSendPassengerSms { get; set; }
        public bool IsCompanyUser { get; set; }
        public string ContractReference { get; set; }
        public string GhostPaymentType { get; set; }
        public int? TitleId { get; set; }


        public string BookingNumber { get; set; }
        //public string ReservationDateTime { get; set; }
        public DateTime BookingDeuTime { get; set; }
        public bool IsSpeedCalculation { get; set; }
        public DateTime BookingDeuTimeReturn { get; set; }
        // public decimal TotalDistance { get; set; }
        // public int TotalTime { get; set; }
        public long RateId { get; set; }
        // public string ReturnViapoints { get; set; }

        public string ReturnPickupPoint { get; set; }
        public string ReturnPickupPointBookingDateTime { get; set; }

        public string ReturnDropoffPoint { get; set; }

        public string ReturnDropoffDateTime { get; set; }
        public bool IsReturnViapoints { get; set; }
        public int BookingSource { get; set; }
        public int BookingSiteId { get; set; }
        //[TermsAndConditions(ErrorMessage = "Please Accepet terms and conditions")]
        [Range(typeof(bool), "true", "true", ErrorMessage = "Please Accepet terms and conditions")]
        public bool IsTermConditionAcceptd { get; set; }

        public int TotalPassengers { get; set; }
        public int TotalHandLuggage { get; set; }
        public int TotalLuggage { get; set; }

        public decimal ReturnWayPrice { get; set; }
        public decimal ReturnWayAccountPrice { get; set; }
        public decimal OneWayPrice { get; set; }
        public decimal OneWayAccountPrice { get; set; }
        public decimal BookingFee { get; set; }
        public decimal OneWayCredit { get; set; }
        public decimal ReturnWayCredit { get; set; }
        public decimal Surcharge { get; set; }
        public decimal FinalPrice { get; set; }
        public decimal JourneyAmount { get; set; }
        public decimal AdditionalCharge { get; set; }
        public string WorldpayCustomerKey { get; set; }
        public string WorldpayToken { get; set; }
        [Display(Name = "Type of Journey")]
        public int JourneyClass { get; set; }

        //[Display(Name = "How did you hear about us?*")]
        //[Range(1, 100000, ErrorMessage = "Field required")]
        public int BookingResearchId { get; set; }

        public bool IsValidCvcNumber { get; set; }
        #region From Address Module
        public long FromFavouriteAddressId { get; set; }
        public long FromAddressId { get; set; }
        public string BookingFrom { get; set; }
        [FromAreapoitPostcodeValidation]
        public string FromPostCode { get; set; }
        public string PriviousFromPostCode { get; set; }
        //  [Required(ErrorMessage = "* Required")]
        public string BookingFromLabel { get; set; }
        public string CustomBookingFromLabel { get; set; }

        public string FromAirportCode { get; set; }
        public long FromFixedPriceAriaPointId { get; set; }
        public decimal FromLat { get; set; }
        public decimal FromLong { get; set; }
        public string FromOrganisationName { get; set; }
        public string FromDepartmentName { get; set; }
        public string FromBuildingName { get; set; }
        public string FromSubBuildingName { get; set; }
        public string FromBuildingNumber { get; set; }
        public string FromStreet { get; set; }
        public string FromPostTown { get; set; }
        #endregion

        #region To Address Module
        public long ToFavouriteAddressId { get; set; }
        public string CustomBookingToLabel { get; set; }
        public long ToAddressId { get; set; }
        public string BookingTo { get; set; }
        //    [Required(ErrorMessage = "* Required")]
        public string BookingToLabel { get; set; }
        [ToAreapointPostcodeValidation]
        public string ToPostCode { get; set; }
        public string PriviousToPostCode { get; set; }
        public string ToAirportCode { get; set; }
        public long ToFixedPriceAriaPointId { get; set; }
        public decimal ToLat { get; set; }
        public decimal ToLong { get; set; }
        public string ToOrganisationName { get; set; }
        public string ToDepartmentName { get; set; }
        public string ToBuildingName { get; set; }
        public string ToSubBuildingName { get; set; }
        public string ToBuildingNumber { get; set; }
        public string ToStreet { get; set; }
        public string ToPostTown { get; set; }
        #endregion


    }

    public class BookingVehicleDom
    {
        public long BookingVehicleTypeId { get; set; }
        public long VehicleTypeId { get; set; }
        public string VehicletypeName { get; set; }
        public decimal DistancePrice { get; set; }
        public string AutocabVehicleTyle { get; set; }
        public string AutocabVehicleCategory { get; set; }

    }

    public class SaveBookingViewModel
    {
        public string BookingId { get; set; }
        public DateTime BookingdateTime { get; set; }
        public string BookingFrom { get; set; }

        public string BookingTo { get; set; }

        public string RedirectPaymentUrl { get; set; }

        public string BookingSource { get; set; }
        public string BookingNumber { get; set; }

        public string BookingToken { get; set; }

        public string Description { get; set; }
        public string WorldpayToken { get; set; }

        public decimal FinalPrice { get; set; }
        public decimal OneWayPrice { get; set; }
        public decimal FreeJourneyAccessAmt { get; set; }

    }

    public class FixedPriceAndDrivingDistanceViewModel
    {
        public bool IsFixedPriceAvailable { get; set; }

        public int Drivingtime { get; set; }
        public decimal DrivingDistance { get; set; }
    }

}