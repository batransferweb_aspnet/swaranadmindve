﻿namespace SwaranAdmin_New.Models
{
    public class WorldpayViewModel
    {
        public string BookingNumber { get; set; }


    }
    public class RepaymentViewModel
    {
        public string BookingNumber { get; set; }
        public string BookingToken { get; set; }
        public string BookingDateTime { get; set; }
        public string Status { get; set; }
        public string BookingFrom { get; set; }
        public string BookingTo { get; set; }
        public string BookedPersonName { get; set; }
        public string BookedPersonTel { get; set; }
        public int PaymentMethodId { get; set; }
        public string PaymentMethod { get; set; }
        public bool IsReturnBooking { get; set; }
        public decimal OneWayPrice { get; set; }
        public decimal OneWayCredit { get; set; }
        public decimal ReturnPrice { get; set; }
        public decimal ReturnCredit { get; set; }
        public string JourneyType { get; set; }

        public string AdminUserId { get; set; }
        public decimal FinalPrice { get; set; }
        public string PaymentStatus { get; set; }
    }
    public class RefoundGridViewModel
    {

        public string BookingNumber { get; set; }
        public string BookingDateTime { get; set; }
        public string BookingToken { get; set; }
        public int Status { get; set; }
        public string BookingFrom { get; set; }
        public string BookingTo { get; set; }
        public string BookedPersonName { get; set; }
        public string BookedPersonTel { get; set; }
        public bool IsReturnBooking { get; set; }
        public decimal FinalPrice { get; set; }
        public string OrderCode { get; set; }
        public decimal? Amount { get; set; }
        public string AdminUserId { get; set; }
    }

    public class WorldpayParrtialRefoundViewModel
    {
        public int amount { get; set; }
        public string orderCode { get; set; }
        public bool isReturnBooking { get; set; }
        public string userId { get; set; }
    }


    public class FullRefundViewModel
    {

        public string bookingNumber { get; set; }
        public string orderCode { get; set; }
        public int bookingRefundType { get; set; }
        public string userId { get; set; }
        public string refundRemarks { get; set; }
        public bool isCreditRefund { get; set; }
        public int creditsToRefund { get; set; }
        public bool isRefundByAdmin { get; set; }

        public bool isIncludeAdditionalCharge { get; set; }
    }




    public class WorldpayFullyRefundViewModel
    {
        public string orderCode { get; set; }
        public int bookingRefundType { get; set; }
        public string userId { get; set; }
        public string refundRemarks { get; set; }
        public bool isCreditRefund { get; set; }
        public int creditsToRefund { get; set; }
        public bool isRefundByAdmin { get; set; }
        public bool isIncludeAdditionalCharge { get; set; }


    }





    public class WorldpayPartialRefundViewModel
    {
        public decimal amount { get; set; }
        public string orderCode { get; set; }
        public bool isReturnBooking { get; set; }
        public string userId { get; set; }
        public string refundRemarks { get; set; }
        public bool isCreditRefund { get; set; }
        public int creditsToRefund { get; set; }
        public bool isRefundByAdmin { get; set; }
    }







    public class PartialyRefundViewModel
    {
        public string bookingNumber { get; set; }
        public decimal amount { get; set; }
        public bool isReturnBooking { get; set; }
        public string refundRemarks { get; set; }

    }

    public class BookingSummaryDom
    {
       
        public string BookingNumber { get; set; }
        public string BookingDateTime { get; set; }
        public string FromFullAddress { get; set; }
        public string Viapoints { get; set; }
        public string ToFullAddress { get; set; }
        public decimal TotalPrice { get; set; }
        public string PaymentType { get; set; }
        public string VehicleTypesName { get; set; }
        public string VehicleTypesNameUrl { get; set; }
        public int NumberOfPassanger { get; set; }
        public int NumberOfLuggages { get; set; }
        public int NumberHandOfLuggages { get; set; }
        public int PaymentStatus { get; set; }
        public bool IsReturnJourney { get; set; }
        public string ReturnBookingDateTime { get; set; }
        public string ReturnFromFullAddress { get; set; }
        public string ReturnViapoints { get; set; }
        public string ReturnToFullAddress { get; set; }


        public decimal OtherCharges { get; set; }
        public decimal OneWayPrice { get; set; }
        public decimal ReturnWayPrice { get; set; }
        public decimal OneWayCredit { get; set; }
        public decimal ReturnWayCredit { get; set; }
        public decimal TotalDistancePrice { get; set; }
        public int BookingSource { get; set; }

        public int BookingStatusReturnJourney { get; set; }
        public int BookingStatus { get; set; }

        public string OneWayGhostId { get; set; }

        public string  ReturnWayGhostId { get; set; }

        public string RedirectPaymentUrl { get; set; }

        public decimal FinalPrice { get; set; }

        public decimal BookingFee { get; set; }

        public bool IsBookingFeePaid { get; set; } 
        public decimal OnewayOtherDiscounts { get; set; }
        public decimal ReturnWayOtherDiscounts { get; set; }

    }

    public class WorldPayOnlineBookingViewmodel
    {
        public string BookingNumber { get; set; }
    }
}