﻿using SwaranAdmin_New.CustomValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SwaranAdmin_New.Models.Member
{
    public class MemberSearchViewModel
    {
        public string UserId { get; set; }
        public long MemberId { get; set; }
        public string FullName { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; } 

        public int NumOfFreeJourneyIssued { get; set; }
        public int NumOfFreeJourneyUsed { get; set; }

        public string DatOfJoin { get; set; }

        public string LastLoggedIn { get; set; }

        public int AvailablePoints { get; set; }
        public string LastPointUpdatedDate { get; set; }

        public string Loyalty { get; set; }

        public int Points { get; set; }
        public int NoOfWaitingBooking { get; set; } 
        public int NoOfCancelBooking { get; set; }
        public int NoOfCompletedBooking { get; set; }
        public int NoOfConfirmedBooking { get; set; }

        public bool IsCABOperator { get; set; } 

    }

    public class LoyaltyMemberSearchViewModel
    {
        public int NmuOfPage { get; set; }
        public long MemberId { get; set; }

        public string FullName { get; set; }
        public string Type { get; set; }

        public string Email { get; set; }
        public string Mobile { get; set; }

        public int PointsLeft { get; set; }

        public int FJGiven { get; set; }

        public int FJUsed { get; set; }

        public string LastLoggedIn { get; set; }
    }

    public class CustomerFreeJourneyViewModel
    {
        public long CustomerFreeJourneyId { get; set; }
        [Display(Name = "User Name *")]
        [Required]
        public string UserName { get; set; }
        [Required]
        [Display(Name = "Member Id *")]
        public string MemberId { get; set; }
        public string UserId { get; set; }
        public long CustomerFreeJourneyTypeId { get; set; }
        public string Code { get; set; }
        public bool IsUsed { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string ExpiryDate { get; set; }

        [Required]
        [Display(Name = "Description *")]
        public string Description { get; set; }
        public string OperatorId { get; set; }

    }

    public class CustomerDiscountViewModel
    {
        public string Customersearch { get; set; }
        public string UserName { get; set; }
        public int MemberId { get; set; }
        public long CustomerTransactionId { get; set; }
        public string CustomerUserId { get; set; }
        [Required]
        [Display(Name = "Description *")]
        public string Description { get; set; }
        public int TransactionTypeId { get; set; }
        public string TransactionReference { get; set; }
        public decimal InitialCredit { get; set; }
        [Required(ErrorMessage = "Credit Amount Required")]
        [ConfigRange(1.1, ErrorMessage = "The value entered must be {1} to {2}!")]
        [Display(Name = "Amount *")]
        public decimal TransactionCredit { get; set; }
        public decimal TransactionDebit { get; set; }
        public decimal FinalCredit { get; set; }
        public string StaffUserId { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsEnabled { get; set; }

        public decimal AvailabelCredits { get; set; } 

    }

    public class MemberSpecialPreferenceViewModel
    {
        public string MemberId { get; set; }
        public string UserName { get; set; }
        public string UserId { get; set; }
        public string StaffId { get; set; }
        [Required(ErrorMessage = "Special Preference Required")]
        [Display(Name = "Special Preference")]
        public string Note { get; set; }
        public bool IsEnabled { get; set; }
    }

    public class MainCustomerCreditsViewModel
    {
        public decimal AvailabelCredits { get; set; }  
        public List<ReadAllCustomerCreditsViewModel> ListCustomerCredits { get; set; }  
    }

    public class ReadAllCustomerCreditsViewModel
    {
        public string TransactionDate { get; set; }
        public string TransactionType { get; set; }
        public string Description { get; set; }
        public decimal TransactionCredit { get; set; }
        public decimal TransactionDebit { get; set; }
        public decimal FinalCredit { get; set; }
        public string StaffName { get; set; }
    }
    public class ReadAllFreeJourneyViewMOdel
    {
        public long CustomerFreeJourneyId { get; set; }
        public string Code { get; set; }
        public bool IsUsed { get; set; }
        public string UsedDate { get; set; }
        public string ExpiryDate { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedDate { get; set; }
        public string FromAddress { get; set; }
        public string ToAddress { get; set; }
        public string CreatedBy { get; set; }
        public string DeletedBy { get; set; }
        public string DeletedReason { get; set; }
        public string CustomerUserId { get; set; }


    }

    public class FreeJourneyCancelViewModel
    {
        public string Reson { get; set; }
        public long FreeJourneyId { get; set; }
    }

    public class SendQuickEmailViewModel
    {
        [Required(ErrorMessage = "Please enter valid email address")]
        [EmailAddress]
        public string ToAddress { get; set; }
        [Required(ErrorMessage = "Email Subject is required")]
        public string Subject { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = "Email body is required")]
        public string Message { get; set; }
    }

    public class TellAFriendEmailViewModel
    {
        public long TellAFriendEmailId { get; set; }
        public string YourName { get; set; }
        public string YourEmail { get; set; }
        public string FriendEmail1 { get; set; }
        public string FriendEmail2 { get; set; }
        public string FriendEmail3 { get; set; }
        public string MessageBody { get; set; }
        public bool IsApproved { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
    }

    public   class AdminCustomerReviewViewModel 
    {
        public long ReviewId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Location { get; set; }
        public int Rating { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsEnable { get; set; }
        public bool IsDeleted { get; set; }
        public string SiteId { get; set; }
    }


    public class BlockMemberBookingViewModel 
    {
        public string UserName { get; set; }
        public string MemberId { get; set; } 
        public long BlockId { get; set; }
        public string UserId { get; set; } 
        public string Telephone { get; set; }
        public string CookieId { get; set; }
        public string EmailId { get; set; }
        public string IpAddress { get; set; }
        [Required]
        public string ResonForBlock { get; set; }
        public string Customersearch { get; set; } 

    }

    public class MemberPendingActivationViewModel
    {
        public string Id { get; set; }
        public string  FirstName { get; set; } 
        public string LastName { get; set; } 
        public string PhoneNumber { get; set; } 
        public string Email { get; set; }
        public string UserName { get; set; }
        public string CreatedDate { get; set; }
        public long MemberId { get; set; }
        public bool IsEmailVerified { get; set; }
        public bool IsAuthorized { get; set; } 
       
    }


    public class BanarTextViewModel 
    {
        public int SiteId { get; set; } 
        [Required]
        public string Message { get; set; } 
        public string ForColor { get; set; } 
        public string BackColor { get; set; } 
        public bool IsEnabeld { get; set; } 
        public bool IsDeleted { get; set; } 
  

    }

    public class OrganizationViewModel
    {
        public int SiteId { get; set; }
        public string SiteName { get; set; }
    }

}