﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwaranAdmin_New.Models.DriverMo
{
    public class DriverApplicationViewModel
    {
        public long DriverApplicationId { get; set; } 
        public string AppliedDate { get; set; }

        public string Name { get; set; }

        public string JobType { get; set; }

        public string Status { get; set; }
        public int StatusId { get; set; } 

    }


    public class CurrentDriverApplicationViewModel 
    {
        public long DriverApplicationId { get; set; }
        public string FullName { get; set; } 
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullAddress { get; set; }
        public string Postcode { get; set; }
        public string PostTown { get; set; }
        public string Street { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string PreferredMethodofContact { get; set; }
        public string DateOfBirth { get; set; }
        public string YearsDriversLicenceHeld { get; set; }
        public string PreferToWork { get; set; }
        public string WillingToStart { get; set; }
        public string ExperienceDetail { get; set; }
        public string Status { get; set; }
        public string County { get; set; }
        public string PrivateHireLicenceType { get; set; }
        public string DrivingLicenseType { get; set; }
        public string PointsOnLicense { get; set; }
        public string RegisteredOrganization { get; set; }
        public string Gender { get; set; }

    }
}