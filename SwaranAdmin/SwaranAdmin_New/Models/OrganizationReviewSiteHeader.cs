﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SwaranAdmin_New.Models
{
    public class OrganizationReviewSiteHeader
    {
        public long OrganizationReviewSiteId { get; set; }
        public int ReviewSiteId { get; set; }
        public string  ReviewSiteName { get; set; }
        public int OrganizationId { get; set; }
        public string OrganizationName { get; set; }
        public bool IsPromotionalMessageEnabled { get; set; }
    }

    public class OrganizationReviewSiteDetail
    {
        public long OrganizationReviewSiteId { get; set; }
        public int ReviewSiteId { get; set; }
        [DisplayName("Review Site")]
        public string ReviewSiteName { get; set; }
        public int OrganizationId { get; set; }
        [DisplayName("Organization")]
        public string OrganizationName { get; set; }
        [DisplayName("Is BCC")]
        public bool IsBccEnabled { get; set; }
        [DisplayName("BCC Emails")]
        public string BccEmail { get; set; }
        [DisplayName("Is Promotion")]
        public bool IsPromotionalMessageEnabled { get; set; }
        [DisplayName("Default Message")]
        [AllowHtml]
        public string DefaultMessage { get; set; }
        [DisplayName("Promotional Message")]
        [AllowHtml]
        public string PromotionalMessage { get; set; }
        [DisplayName("Api Key")]
        public string ApiKey { get; set; }
        [DisplayName("Sending Order")]
        public int SendingOrder { get; set; }
        [DisplayName("Begin Day")]
        public int BeginDay { get; set; }
        [DisplayName("End Day")]
        public int EndDay { get; set; }
        [DisplayName("Maximum Review Count")]
        public int MaximumReviewCount { get; set; }
        [DisplayName("Current Review Count")]
        public int CurrentReviewCount { get; set; }
        [DisplayName("Is Enabled")]
        public bool IsEnabled { get; set; }
    }
}