﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SwaranAdmin_New.CustomValidation;

namespace SwaranAdmin_New.Models.BookingMo
{
    public class TelBookingViewModel
    {
        
    
        public long VehicleTypeId { get; set; }
        public long? AddressSearchId { get; set; }
        public long FromFavouriteAddressId { get; set; }
        public long ToFavouriteAddressId { get; set; }
        public bool IsNonElasticFromAdd { get; set; }
        public bool IsNonElasticToAdd { get; set; }
        public string BidReference { get; set; }
        public string AutocabVehicleType { get; set; }
        public string AutocabVehicleCategory { get; set; }
        public long EmissionZoneId { get; set; }
        public string UserId { get; set; }
        [BookingDateCustomValidation]
        [Required(ErrorMessage = "Date is Required")]
        [Display(Name = "PickUp Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        // [BookingDateCustomValidation(ErrorMessage = "Invalid Date")]
        public string BookingDate { get; set; }

        [BookingDateTimeCustomValidation(BookingDateProperty = "BookingDate")]
        [Display(Name = "Pick Up Time")]
        [Required(ErrorMessage = "Time is Required")]
        public string BookingTime { get; set; }
        // [Required(ErrorMessage = "Invalid Pickup Point")]

        public string ClientDatetTime { get; set; }
        [Required(ErrorMessage = "Invalid Pickup Point")]
        public string BookingFrom { get; set; }

        [Required(ErrorMessage = "Invalid Dropoff Point")]
        public string BookingTo { get; set; }

        //  [Required(ErrorMessage = "Invalid Pickup Point")]
        [BookingFromAddressValidation(ToPostcodeProperty = "ToPostCode", FromPostcodeProperty = "FromPostCode")]
        [RegularExpression(@"^((?![<>]).)*$", ErrorMessage = "Invalid Pickup Point")]
        public string BookingFromLabel { get; set; }
        
        [BookingToAddressValidation(ToPostcodeProperty = "ToPostCode", FromPostcodeProperty = "FromPostCode")]
        [RegularExpression(@"^((?![<>]).)*$", ErrorMessage = "Invalid Dropoff Point")]
        public string BookingToLabel { get; set; }
        [HiddenInput(DisplayValue = false)]
 

        [Required(ErrorMessage = "Required")]
        [Range(1, 50, ErrorMessage = "Please select the passengers")]
        public int NumOfPassengers { get; set; }

        [Required(ErrorMessage = "Required")]
        public int StudentDiscount { get; set; }
        public decimal StudentDiscountAmount { get; set; }
        public string PickupNote { get; set; }
        public long FromAddressId { get; set; }
        public int FromAriapointTypeId { get; set; }
        //[BookingFromAddressValidation(ToPostcodeProperty = "ToPostCode")]
        //[HiddenInput(DisplayValue = false)]
        //[RegularExpression(@"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})", ErrorMessage = "Invalid Pickup Point")]
        // public string FromPostCode { get; set; }

        [FromAreapoitPostcodeValidation]
        public string FromPostCode { get; set; }
        public string PriviousFromPostCode { get; set; }
        //  [Required(ErrorMessage = "* Required")]



        [ToAreapointPostcodeValidation]
        public string ToPostCode { get; set; }

        public string FromAirportCode { get; set; }
        public long FromFixedPriceAriaPointId { get; set; }
        public decimal FromLat { get; set; }
        public decimal FromLong { get; set; }
        public string FromOrganisationName { get; set; }
        public string FromDepartmentName { get; set; }
        public string FromBuildingName { get; set; }
        public string FromSubBuildingName { get; set; }
        public string FromBuildingNumber { get; set; }
        public string FromStreet { get; set; }
        public string FromPostTown { get; set; }
        public long ToAddressId { get; set; }
        public int ToAriapointTypeId { get; set; }
        //[HiddenInput(DisplayValue = false)]
        //[RegularExpression(@"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})", ErrorMessage = "Invalid Dropoff Postcode")]
        //[BookingAddressValidation(FromPostcodeProperty = "FromPostCode")]
        //public string ToPostCode { get; set; }
        public string ToAirportCode { get; set; }
        public long ToFixedPriceAriaPointId { get; set; }
        public decimal ToLat { get; set; }
        public decimal ToLong { get; set; }
        public string ToOrganisationName { get; set; }
        public string ToDepartmentName { get; set; }
        public string ToBuildingName { get; set; }
        public string ToSubBuildingName { get; set; }
        public string ToBuildingNumber { get; set; }
        public string ToStreet { get; set; }
        public string ToPostTown { get; set; }


        public decimal TotalDistance { get; set; }
        public decimal TotalDistancePrice { get; set; }
        public decimal TotalDistanceVia { get; set; }
        public decimal MainDistanceVia { get; set; }
        public decimal TotalViaDistance { get; set; }
        public decimal TotalOneWayDistance { get; set; }
        public decimal TotalTwoWayDistance { get; set; }
        public int TotalTime { get; set; }

        public string TotalTimeLabel { get; set; }
        public string StartTimeLabel { get; set; }
        public long RateId { get; set; }
          


        public List<ViaPointAddress> ViaPointAddress { get; set; }

        public string IsReturnJourney { get; set; }

        public bool IsRoboticFalse { get; set; }

        public decimal ViaMinimumRate { get; set; }
        public decimal ViaMileRate { get; set; }
        public bool IsFastViaRoute { get; set; }

        public decimal TotalViaTime { get; set; }
        public decimal MainTimeVia { get; set; }
        public decimal ViaTimeRate { get; set; }

         
        public string Account { get; set; } 
    
       
        public string DropoffPointNote { get; set; }
       
        public string FullName { get; set; }
        public string DriverNote { get; set; }
        public string Telephone { get; set; }

        public int VehicletypeId { get; set; } //saloon,estate,v executeive,MPV 5 ,MVP 6   

        public int Passengers { get; set; }

        public int HandLuggage { get; set; }

        public string LoyaltyTrier { get; set; }

        public string ReqDrivers { get; set; }

       

        public string YourReference { get; set; }
        public string OurReference { get; set; }
        public string Priority { get; set; } 
        public string ReturnTime { get; set; } 
        public string RepeatDays { get; set; } 
        public string FlightInfo { get; set; }
        public string CustomerEmail { get; set; } 
        


         
        public bool IsCABOperator { get; set; }
        public string OfficePhoneNo { get; set; }
        public long LoginId { get; set; }
        public bool IsVehicleUpgradeEnabeld { get; set; }
        public bool IsUpgradeByPoints { get; set; }
        public int UpgradeAmount { get; set; }
        public int UpgradePoints { get; set; }

        [Required(ErrorMessage = " Upgrade VehicleType is required")]
        public long UpgradeVehicleTypeId { get; set; }
        public long CurrentVehicletypeId { get; set; }
        public int AviPoints { get; set; }

        public int MinimumPoints { get; set; }
        public bool IsFreeJourneyAccessAmt { get; set; }

        public decimal FreeJourneyAccessAmt { get; set; }
        public string Loyalty { get; set; }
        public long BookingId { get; set; }

        public string BookingToken { get; set; }

    
        public bool IsMemberBooking { get; set; }

        public string VehicleTypeName { get; set; }
         

        public string BookingDateTime { get; set; }

        public string BookingToDateTime { get; set; } 


        [Display(Name = "Country Code of Alternative No")]
        public string PassengerAlternativeMobileCountryCode { get; set; }
        [Display(Name = "Alternative Phone No")]
        [Phone(ErrorMessage = "Invalid Alternative Phone No ")]
        public string PassengerAlternativeMobileNo { get; set; }

        public int? PassengerTitle { get; set; }
        [Display(Name = "First Name*")]
        [Required(ErrorMessage = "First Name is required")]
        public string PassengerFirstName { get; set; }
        [Display(Name = "Last Name*")]
        [Required(ErrorMessage = "Last Name is required")]
        public string PassengerLastName { get; set; }
        [Display(Name = "Country Code Mobile No*")]
        public string PassengerMobileNumberCountryCode { get; set; }
        [Display(Name = "Mobile Phone No*")]
        [Phone(ErrorMessage = "Invalid Mobile Phone No ")]
        public string PassengerMobileNo { get; set; }
        [Display(Name = "Email*")]
        [EmailAddress(ErrorMessage = "The Email field is not a valid e-mail address.")]
        public string PassengerEmail { get; set; }
        public int? BookedPersonTitleId { get; set; }
        [Display(Name = "First Name*")]
        [Required(ErrorMessage = "First Name is required")]
        public string BookedPersonFirstName { get; set; }
        [Display(Name = "Last Name*")]
        [Required(ErrorMessage = "Last Name is required")]
        public string BookedPersonLastName { get; set; }
        [Display(Name = "Country Code of Mobile No*")]
        public string BookedPersonMobileNumberCountryCode { get; set; }
        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Mobile Phone required")]
        [Display(Name = "Mobile Phone No*")]
        [Phone(ErrorMessage = "Invalid Mobile Phone No ")]
        public string BookedPersonMobileNo { get; set; }
        [Display(Name = "Country Code of Alternative No")]
        public string BookedPersonAlternativeMobileCountryCode { get; set; }
        [Display(Name = "Alternative Phone No")]
        [DataType(DataType.PhoneNumber)]
        [Phone(ErrorMessage = "Invalid Alternative Phone No ")]
        public string BookedPersonAlternativeMobileNo { get; set; }
        [Display(Name = "Email*")]
        [EmailAddress(ErrorMessage = "The Email field is not a valid e-mail address.")]
        [Required(ErrorMessage = "Email is required")]
        public string BookedPersonEmail { get; set; }

        [Display(Name = "Is there anything else we need to know?")]
        [StringLength(500, ErrorMessage = "Can not be exit 500 characters")]
        public string CustomerComment { get; set; }

        public int BoosterSeat { get; set; }

        public int InfantSeats { get; set; }

        public int ChildSeats { get; set; }
        [Display(Name = "No of Check in Luggage")]
        [Required(ErrorMessage = "Number Of Luggages is required")]
        public int NumberOfLuggages { get; set; }
        [Display(Name = "No of Hand in Luggage")]
        [Required(ErrorMessage = "Number Of Hand Luggages is required")]
        public int NumberOfHandLuggages { get; set; }

        public int CustomerType { get; set; }

        [Required(ErrorMessage = "Journey Type is required")]
        public int JourneyTypeId { get; set; }
        public string JourneyType { get; set; }
        public string BookingPersonType { get; set; }
        public bool HasChildSeates { get; set; }


        //[Required(ErrorMessage = "Card Type is required")]
        //[Range(1, 10000, ErrorMessage = "Card Type is required")]
        //[Display(Name = "Card Type")]
        //public long? CardTypeId { get; set; }

        [Required(ErrorMessage = "Payment Type is required")]
        [Range(1, 100000, ErrorMessage = "Payment Type is required")]
        [Display(Name = "Payment Type")]
        public int? CardPaymentTypeId { get; set; }

        [Required(ErrorMessage = "Payment Method is required")]
        [Range(1, 100000, ErrorMessage = "Payment Method is required")]

        [Display(Name = "  Payment Method*")]
        public int PaymentMethodId { get; set; }

        public int PaymentStatusId { get; set; }

        public int PaymentTypeId { get; set; }
        [Range(1, 100000, ErrorMessage = "required")]
        [Display(Name = "New Card/Saved Card")]
        public int SaveCardId { get; set; }

        public bool IsSavedCardPayment { get; set; }

        [Display(Name = "  CVV Number*")]
        [Required(ErrorMessage = "CVV Numberis required")]
        public string CvcNumber { get; set; }
        public string CardHodelName { get; set; }
        [Display(Name = " Code*")]
        [Required(ErrorMessage = "Free journey code is required")]
        [FreeJourneyCodeValidation(UserIdProperty = "UserId", ErrorMessage = "Invalid free journey code")]
        public string FreeJourneyCode { get; set; }

        [Display(Name = "Use My Available Credits")]
        [Required(ErrorMessage = "Credits is required")]
        [UseMyCreditsValidation(UserIdProperty = "UserId", ErrorMessage = "Invalid credit amount")]
        public decimal MyCredits { get; set; }


        public string IpAddress { get; set; }//17

        public int JourneyPaymentType { get; set; }

        public long? CompanyId { get; set; }

        public decimal AccountFee { get; set; }
        public decimal AccountFeeReturnWay { get; set; }
        public string MapUrl { get; set; }





        [Required(ErrorMessage = "Please select meeting time with driver!")]
        [Display(Name = "When should the driver pick you up *")]
        public int DriverEllapseTime { get; set; }

        [Display(Name = "Display Name ")]
        public string DisplayNameFlight { get; set; }
        [Required(ErrorMessage = "Flight No is required")]
        [Display(Name = "Flight Number *")]
        public string FlightNo { get; set; }
        [Required(ErrorMessage = "Cruise ship or Ferry name is required")]
        [Display(Name = "Cruise Ship/Ferry Name*")]
        public string CruiseShipName { get; set; }
        [Required(ErrorMessage = "Display Name is required")]
        [Display(Name = "Display Name *")]
        public string DisplayNameCruiseShip { get; set; }
        [Required(ErrorMessage = "Landing Date is required")]

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string LandingDate { get; set; }
        [Required(ErrorMessage = "Landing Time is required")]

        [LandingDateTimeValidation(LandingDateProperty = "LandingDate", ErrorMessage = "Invalid Landing Time")]
        public string LandingTime { get; set; }
        public DateTime? LandingDateTime { get; set; }
        public bool IsAuthenticated { get; set; }
        public string Viapoints { get; set; }

        public bool Babyseat { get; set; }
        public string CountryName { get; set; }
        public bool IsSendPassengerEmail { get; set; }
        public bool IsSendPassengerSms { get; set; }
        public bool IsCompanyUser { get; set; }
        public string ContractReference { get; set; }
        public string GhostPaymentType { get; set; }
        public int? TitleId { get; set; }


        public string BookingNumber { get; set; }
        //public string ReservationDateTime { get; set; }
        public DateTime BookingDeuTime { get; set; }
        public bool IsSpeedCalculation { get; set; }
        public DateTime BookingDeuTimeReturn { get; set; } 

        public string ReturnPickupPoint { get; set; }
        public string ReturnPickupPointBookingDateTime { get; set; }

        public string ReturnDropoffPoint { get; set; }

        public string ReturnDropoffDateTime { get; set; }
        public bool IsReturnViapoints { get; set; }
        public int BookingSource { get; set; }
        public int BookingSiteId { get; set; }
        //[TermsAndConditions(ErrorMessage = "Please Accepet terms and conditions")]
        [Range(typeof(bool), "true", "true", ErrorMessage = "Please Accepet terms and conditions")]
        public bool IsTermConditionAcceptd { get; set; }

        public int TotalPassengers { get; set; }
        public int TotalHandLuggage { get; set; }
        public int TotalLuggage { get; set; }
        public decimal PickupCharge { get; set; }
        public decimal DropoffCharge { get; set; }
        public decimal DistancePrice { get; set; }
        public decimal Distance { get; set; }
        public decimal ReturnWayPrice { get; set; }
        public decimal ReturnWayAccountPrice { get; set; }
        public decimal OneWayPrice { get; set; }
        public decimal OneWayAccountPrice { get; set; }
        public decimal BookingFee { get; set; }
        public decimal OneWayCredit { get; set; }
        public decimal ReturnWayCredit { get; set; }
        public decimal Surcharge { get; set; }
        public decimal FinalPrice { get; set; }
        public decimal JourneyAmount { get; set; }
        public decimal AdditionalCharge { get; set; }
        public string WorldpayCustomerKey { get; set; }
        public string WorldpayToken { get; set; }
        [Display(Name = "Type of Journey")]
        public int JourneyClass { get; set; }

        //[Display(Name = "How did you hear about us?*")]
        //[Range(1, 100000, ErrorMessage = "Field required")]
        public int BookingResearchId { get; set; }

        public bool IsValidCvcNumber { get; set; }
       
       
        
        
      
        public int PassportType { get; set; }


    

}

    public class TelPriceViewModel
    {
        public string BookingFrom { get; set; }
        public string BookingTo { get; set; }
        public long FromFixedPriceAriaPointId { get; set; }
        public long ToFixedPriceAriaPointId { get; set; }

        public int FromAriapointTypeId { get; set; }
        public int ToAriapointTypeId { get; set; }

        public string FromPostCode { get; set; } 
        public string ToPostCode { get; set; }
        public decimal FromLat { get; set; }
        public decimal FromLong { get; set; }
        public decimal ToLat { get; set; }
        public decimal ToLong { get; set; }
       
        public long VehicleTypeId { get; set; }
       
        public string UserId { get; set; }
        public long EmissionZoneId { get; set; }

        public string BookingDate { get; set; } 
        public string BookingTime { get; set; }
        public decimal TotalOneWayDistance { get; set; }
        public decimal TotalDistance { get; set; }
      
        public List<ViaPointAddress> ViaPointAddress { get; set; }
    }

    public class ViaPriceCalViewModel
    {
        public decimal ViaMinimumRate { get; set; }
        public decimal ViaMileRate { get; set; }
        public decimal TotalViaDistance { get; set; }
        public decimal TotalViaTime { get; set; }
        public decimal MainTimeVia { get; set; }
        public decimal ViaTimeRate { get; set; }
        public decimal MainDistanceVia { get; set; }
         
        public bool IsFastViaRoute { get; set; }
    }
}