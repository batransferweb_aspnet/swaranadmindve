﻿namespace SwaranAdmin_New.Models
{
    public class LayoutViewModel
    {
        public string UserName { get; set; }
        public int Count { get; set; }
    }

    public class WaitingPartialViewModel
    {
        public string From { get; set; }
        public string To { get; set; }

        public string BookingDateTime { get; set; }
    }
}