﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace SwaranAdmin_New.Models
{
    public class FreeJourneyAdminViewModel
    {
        public CustomerFreeJourneyTypeViewModel CustomerFreeJourneyType { get; set; }
        public List<FreeJourneyAreaTypeViewModel> AllAreaTypes { get; set; }
        public List<FreeJourneyAreaTypeViewModel> FreeJourneyAreaTypes { get; set; }

        public List<FreeJourneyVehicleTypesViewModel> AllVehicleTypes { get; set; }
        public List<FreeJourneyVehicleTypesViewModel> FreeJourneyVehicleTypes { get; set; }
    }

    public class FreeJourneyVehicleTypesViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
    public class FreeJourneyAreaTypeViewModel
    {
        public int JourneyTypeId { get; set; }
        public string AreaType { get; set; }


    }

    public class CustomerFreeJourneyTypeViewModel
    {

        public long CustomerFreeJourneyTypeId { get; set; }
        public string Name { get; set; }
        public decimal MaxDistance { get; set; }
        public decimal MaxPrice { get; set; }
        public string FromDateTime { get; set; }
        public string ToDateTime { get; set; }
        public string FromPostCode { get; set; }
        public string ToPostCode { get; set; }
        public bool IsTwoWay { get; set; }
        public bool IsVehicleBased { get; set; }
        public bool IsJourneyTypeBased { get; set; }
        public bool IsDistanceBased { get; set; }
        public bool IsPriceBased { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }


    }

    public class SaveDistanceBaseViewModel
    {
        public bool IsDistanceBasd { get; set; }
        public decimal? MaxDistance { get; set; }
    }
    public class SavePriceBaseViewModel
    {
        public bool IsPriceBasd { get; set; }
        public decimal? MaxPrice { get; set; }
    }

    public class SaveVehiclebasedViewModel
    {
        public List<SelectListItem> Items { get; set; }
        public bool IsVehicleBased { get; set; }
    }

    public class SaveJourneyTypeBasedViewModel
    {
        public List<SelectListItem> Items { get; set; }
        public bool IsJourneyTypeBased { get; set; }
    }

    public class CustomerDeleteSavedCardViewModel
    {
        public long Customercard  { get; set; } 
        public string SavecardRemarks { get; set; } 
    }
}