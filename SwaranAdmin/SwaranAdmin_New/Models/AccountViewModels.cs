﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SwaranAdmin_New.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
        public bool IsEmailVarified { get; set; }
        [Required]
        [Display(Name = "User Name")]
        [EmailAddress]
        public string Email { get; set; }

    }

    public class RegisterViewModel
    {

        //public string SecurityStamp { get; set; }
        [Required]
        //[RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Please Enter Correct First Name")]
        //[StringLength(40, ErrorMessage = "First Name cannot be longer than 40 characters.")]
        public string UserName { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Please Enter Correct First Name")]
        [StringLength(40, ErrorMessage = "First Name cannot be longer than 40 characters.")]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "Please Enter Correct Last Name")]
        [StringLength(40, ErrorMessage = "Last Name cannot be longer than 20 characters.")]
        public string LastName { get; set; }
        [Display(Name = "Postcode")]
        public string PostalCode { get; set; }
        public string Street { get; set; }
        public string Town { get; set; }

        [Required]
        [EmailAddress]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$", ErrorMessage = "Please Enter Correct Email Address")]
        public string Email { get; set; }


        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [StringLength(10, ErrorMessage = "country Code cannot be longer than 10 characters.")]
        public string CountryCode { get; set; }
        //public int Gender { get; set; } 
        public string LandPhoneCountryCode { get; set; }
        [StringLength(20, ErrorMessage = "Land Phone Number cannot be longer than 20 characters.")]
        public string LandPhoneNumber { get; set; }
        public string MobileNumberCountryCode { get; set; }

        //  [RegularExpression(@"^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$", ErrorMessage = "Please Enter Correct Mobile Number")]
        [Required]
        [StringLength(20, ErrorMessage = "Mobile Number cannot be longer than 20 characters.")]
        public string MobileNumber { get; set; }

        [StringLength(20, ErrorMessage = "Primary Phone Number cannot be longer than 20 characters.")]
        public string PrimaryPhoneNumber { get; set; }
        public bool IsMobileNumberPrimary { get; set; }

        public bool IsFacebookLogin { get; set; }
        [StringLength(300, ErrorMessage = "facebook Token cannot be longer than 300 characters.")]
        public string FacebookToken { get; set; }
        public long CountryId { get; set; }
        public string ReturnUrl { get; set; }
        public bool Proceed { get; set; }
    }

    public class ResetPasswordViewModel
    {
        //[Required]
        //[EmailAddress]
        //[Display(Name = "Email")]
        //public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        //public string Code { get; set; }

        public string UserId { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        //[Display(Name = "Email")]
        public string Username { get; set; }
    }
}
