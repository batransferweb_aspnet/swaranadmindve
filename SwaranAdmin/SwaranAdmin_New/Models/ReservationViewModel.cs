﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SwaranAdmin_New.Models
{
    public class ReservationViewModel
    {
        public string Journeydate { get; set; }
        public string BookingNumber { get; set; }
        public string BookingFrom { get; set; }
        public string BookingTo { get; set; }
        public string Status { get; set; }
        public int StatusId { get; set; }
        public string BookingToken { get; set; }
    }

    public class ReservationHistoryViewModel
    {
        public long BookingId { get; set; }
        public string BookingToken { get; set; }
        public string Status { get; set; }
        public string BookingSource { get; set; }
        public int BookingSource1 { get; set; }
        public string BookingNumber { get; set; }
        public string CreatedDate { get; set; }
        public string BookingDateTime { get; set; }
        public string JourneyType { get; set; }
        public string BookingFrom { get; set; }
        public string BookingTo { get; set; }
        public string PaymentType { get; set; }
        public bool IsSourceBooking { get; set; }
        public string BookedPersonName { get; set; }
        public string LoyaltyType { get; set; }
        public string Timeleft { get; set; }
        public string Country { get; set; }
        public string VehicleTypes { get; set; }

        public string FromPostcode { get; set; }
        public string ToPostcode { get; set; }
        public bool IsReturnBooking { get; set; }
        public string AutocabNumber { get; set; }
        public bool IsSentToGhost { get; set; }

        public int RemainingHours { get; set; }

        public int PaymentStatus { get; set; }
        public int PaymentTypeId { get; set; }
        public bool IsVehicleUpgradeEnabeld { get; set; }
        public bool IsCABOperator { get; set; } 
    }

    public class ReservationMonthlyChartViewModel
    {
        public DateTime? BookingDate { get; set; }
        public int BookingCount { get; set; }
    }
    public class ReservationDateRange
    {
        public string startdate { get; set; }
        public string enddate { get; set; }
    }
    public class CurrentBookingDisplayViewModel
    {
        [AllowHtml]
        public string MainBooking { get; set; }

        public string BookingToken { get; set; }
        public bool IsCancelBooking { get; set; } 
        public bool IsReturnBooking { get; set; }
        public bool IsSourceBooking { get; set; } 
        public string BookedPersonEmail { get; set; }

        public int BookingStatus { get; set; }

        public int CardPaymentType { get; set; }

      //  public int PaymentMethod { get; set; }
        public int PaymentStatus { get; set; }
        [Display(Name = "Payment Method")]
        public int PaymentMethodId { get; set; }

        public decimal MyCredits { get; set; }

        public int JourneyTypeId { get; set; }

        public string FreeJourneyCode { get; set; }

        public string AutocabNumber { get; set; }

        public string UserId { get; set; }

        public decimal OneWayPrice { get; set; }
        public decimal ReturnWayPrice { get; set; }

        public decimal BookingFee { get; set; }

        public string AutocabAuthorizationReference { get; set; }

        public decimal OneWayCredit { get; set; }
        public decimal ReturnWayCredit { get; set; }

        public bool IsUseMyCreditCardPayment { get; set; }

        public int SiteId { get; set; }   
        public string WorldPayReference { get; set; }
        public decimal OtherDiscounts { get; set; }

    }

    public class CancelBookingViewModel
    {
        public string Token { get; set; }
        public bool IsRetunBooking { get; set; }
        public string CancelDate { get; set; }

        public decimal CancelAmt { get; set; }

        public string Reson { get; set; }

        public int ResonId { get; set; } 

    }


    public class ConfirmBookingViewModel
    {
        public bool IsCancelBooking { get; set; } 
        public string Token { get; set; }
        public bool IsRetunBooking { get; set; }
        public string CancelDate { get; set; }

        public decimal CancelAmt { get; set; }

    }


    public class MainCancelBooking
    {
        public bool Status { get; set; }
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }

    public class SaveUpdateVehicle
    {
        public int Points { get; set; }
        public int VehicleUpgradeId { get; set; }
        public int CurrentVehicletypeId { get; set; }
        public long BookingId { get; set; }
        public bool IsUpgrateByPoints { get; set; }
    }

    public class UpgrateVehicleListViewModel
    {
        public long VehicleTypeId { get; set; }
        public int Points { get; set; }
        public string Text { get; set; }
    }

    public class CurrentBookingsViewModel
    {
        public long BookingId { get; set; }
        public string BookingFrom { get; set; }
        public string BookingTo { get; set; }
        public string BookingDateTime { get; set; }
        public long VehicleTypeId { get; set; }
        public string VehicleTypeName { get; set; }

    }

    public class PendingBookingViewModel
    {
        public long BookingId { get; set; }
        public System.DateTime BookingDateTime { get; set; }
        public string BookingNumber { get; set; }
        public string BookedPersonName { get; set; }
        public string BookingFrom { get; set; }
        public string BookingTo { get; set; }
        public int StatusId { get; set; }
        public bool IsReturnBooking { get; set; }
        public string Status { get; set; }
        public string BookingToken { get; set; }
        public int BookingPoints { get; set; }
        public  int TotalPoints { get; set; }
        public bool IsCardPaymentFailed { get; set; }
        public bool IsBookingFeePaymentFailed { get; set; }
    }
}