﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwaranAdmin_New.Models.ReportMo
{
    public class ReportBookingModel
    {
    }
    public class ReportBookingStatusViewModel
    {
        public DateTime Date { get; set; }
        public int Finished { get; set; }
        public int Cancelled { get; set; }
        public int Confirmed { get; set; }
    }

    public class ReportTotalCreatedBookingsViewModel 
    {
        public DateTime CreatedDate { get; set; } 
        public int TotLogings { get; set; }
        public int FinalCount { get; set; } 
        public int CreatedBookings { get; set; } 
    }

    public class ReportSiteViewModel
    {
        public List<ReportBookingSiteViewModel> ListSite { get; set; }
        public List<BookingSitePieViewModel> AllSite { get; set; }
    }


    public class BookingSitePieViewModel
    {
        public string label { get; set; }
        public int value { get; set; }
        public string labelcolor { get; set; }
    }

    public class ReportBookingSiteViewModel
    {

        public DateTime Date { get; set; }
        public int BA { get; set; }
        public int AO { get; set; }
        public int ET { get; set; }
        public int WT { get; set; }
        public int BAT { get; set; }
        public int BAL { get; set; }
        public int BAC { get; set; }
        public int APP { get; set; }
        public int AND { get; set; } 
    }



    public class ReportBookingPaymenttypeViewModel
    {
        public DateTime Date { get; set; }
        public int Cash { get; set; }
        public int Card { get; set; }
        public int FreeJourney { get; set; }
        public int UseCredits { get; set; }
    }

    public class ReportPlotPieViewModel
    {
        public List<ReportPlotPieMore50ViewModel> PlotMore50 { get; set; }
        public List<ReportPlotPieLess50ViewModel> PlotLess50 { get; set; }
        public List<ReportPlotPieLess20ViewModel> PlotLess20 { get; set; }


        public List<ReportPlotPieDropoffViewModel> PlotMore50Dropoff { get; set; }
        public List<ReportPlotPieDropoffViewModel> PlotLess50Dropoff { get; set; }
        public List<ReportPlotPieDropoffViewModel> PlotLess20Dropoff { get; set; }
    }

    public class ReportPlotPieDropoffViewModel  
    {
        public string label { get; set; }
        public int value { get; set; }
        public string labelcolor { get; set; }
    }


    public class ReportPlotPieLess20ViewModel 
    {
        public string label { get; set; }
        public int value { get; set; }
        public string labelcolor { get; set; }
    }
    public class ReportPlotPieLess50ViewModel 
    {
        public string label { get; set; }
        public int value { get; set; }
        public string labelcolor { get; set; }
    }

    public class ReportPlotPieMore50ViewModel 
    {
        public string label { get; set; }
        public int value { get; set; }
        public string labelcolor { get; set; }
    }

    public class ReportCustomerBookingSearchViewModel 
    {
        public string FromPlot { get; set; }
        public string ToPlot { get; set; } 
        public int BookingCount { get; set; } 
       
    }


    public class ReportBookingPlotViewModel 
    {
        public int AL1 { get; set; }
        public int AL2 { get; set; }
        public int AL5 { get; set; }
        public int AL6 { get; set; }
        public int AL9 { get; set; }
        public int B1 { get; set; }
        public int B26 { get; set; }
        public int BA1 { get; set; }
        public int BH1 { get; set; }
        public int BH19 { get; set; }
        public int BN1 { get; set; }
        public int BN11 { get; set; }
        public int BN23 { get; set; }
        public int BR1 { get; set; }
        public int BR2 { get; set; }
        public int BR3 { get; set; }
        public int BR4 { get; set; }
        public int BR5 { get; set; }
        public int BR6 { get; set; }
        public int BR7 { get; set; }
        public int BR8 { get; set; }
        public int BS1 { get; set; }
        public int BS48 { get; set; }
        public int CB1 { get; set; }
        public int CB10 { get; set; }
        public int CB2 { get; set; }
        public int CF62 { get; set; }
        public int CM1 { get; set; }
        public int CM24 { get; set; }
        public int CO1 { get; set; }
        public int CO12 { get; set; }
        public int CR0 { get; set; }
        public int CR2 { get; set; }
        public int CR3 { get; set; }
        public int CR4 { get; set; }
        public int CR5 { get; set; }
        public int CR6 { get; set; }
        public int CR7 { get; set; }
        public int CR8 { get; set; }
        public int CR9 { get; set; }
        public int CT1 { get; set; }
        public int CT11 { get; set; }
        public int CT15 { get; set; }
        public int CT16 { get; set; }
        public int CT20 { get; set; }
        public int CT9 { get; set; }
        public int CV1 { get; set; }
        public int CV32 { get; set; }
        public int CV37 { get; set; }
        public int DA1 { get; set; }
        public int DA10 { get; set; }
        public int DA11 { get; set; }
        public int DA12 { get; set; }
        public int DA13 { get; set; }
        public int DA14 { get; set; }
        public int DA15 { get; set; }
        public int DA16 { get; set; }
        public int DA17 { get; set; }
        public int DA18 { get; set; }
        public int DA2 { get; set; }
        public int DA3 { get; set; }
        public int DA4 { get; set; }
        public int DA5 { get; set; }
        public int DA6 { get; set; }
        public int DA7 { get; set; }
        public int DA8 { get; set; }
        public int DA9 { get; set; }
        public int E1 { get; set; }
        public int E10 { get; set; }
        public int E11 { get; set; }
        public int E12 { get; set; }
        public int E13 { get; set; }
        public int E14 { get; set; }
        public int E15 { get; set; }
        public int E16 { get; set; }
        public int E17 { get; set; }
        public int E18 { get; set; }
        public int E2 { get; set; }
        public int E20 { get; set; }
        public int E3 { get; set; }
        public int E4 { get; set; }
        public int E5 { get; set; }
        public int E6 { get; set; }
        public int E7 { get; set; }
        public int E8 { get; set; }
        public int E9 { get; set; }
        public int EC1 { get; set; }
        public int EC2 { get; set; }
        public int EC3 { get; set; }
        public int EC4 { get; set; }
        public int EH1 { get; set; }
        public int EN1 { get; set; }
        public int EN2 { get; set; }
        public int EN3 { get; set; }
        public int EN4 { get; set; }
        public int EN5 { get; set; }
        public int EN6 { get; set; }
        public int EN7 { get; set; }
        public int EN8 { get; set; }
        public int EN9 { get; set; }
        public int G1 { get; set; }
        public int GL50 { get; set; }
        public int GU1 { get; set; }
        public int GU2 { get; set; }
        public int GU30 { get; set; }
        public int GU6 { get; set; }
        public int GU9 { get; set; }
        public int HA0 { get; set; }
        public int HA1 { get; set; }
        public int HA2 { get; set; }
        public int HA3 { get; set; }
        public int HA4 { get; set; }
        public int HA5 { get; set; }
        public int HA6 { get; set; }
        public int HA7 { get; set; }
        public int HA8 { get; set; }
        public int HA9 { get; set; }
        public int IG1 { get; set; }
        public int IG10 { get; set; }
        public int IG11 { get; set; }
        public int IG2 { get; set; }
        public int IG3 { get; set; }
        public int IG4 { get; set; }
        public int IG5 { get; set; }
        public int IG6 { get; set; }
        public int IG7 { get; set; }
        public int IG8 { get; set; }
        public int IG9 { get; set; }
        public int KT1 { get; set; }
        public int KT10 { get; set; }
        public int KT11 { get; set; }
        public int KT12 { get; set; }
        public int KT13 { get; set; }
        public int KT14 { get; set; }
        public int KT15 { get; set; }
        public int KT16 { get; set; }
        public int KT17 { get; set; }
        public int KT18 { get; set; }
        public int KT19 { get; set; }
        public int KT2 { get; set; }
        public int KT20 { get; set; }
        public int KT21 { get; set; }
        public int KT22 { get; set; }
        public int KT23 { get; set; }
        public int KT24 { get; set; }
        public int KT3 { get; set; }
        public int KT4 { get; set; }
        public int KT5 { get; set; }
        public int KT6 { get; set; }
        public int KT7 { get; set; }
        public int KT8 { get; set; }
        public int KT9 { get; set; }
        public int L24 { get; set; }
        public int LE1 { get; set; }
        public int LE11 { get; set; }
        public int LE13 { get; set; }
        public int LU2 { get; set; }
        public int M1 { get; set; }
        public int M90 { get; set; }
        public int MK10 { get; set; }
        public int MK9 { get; set; }
        public int N1 { get; set; }
        public int N10 { get; set; }
        public int N11 { get; set; }
        public int N12 { get; set; }
        public int N13 { get; set; }
        public int N14 { get; set; }
        public int N15 { get; set; }
        public int N16 { get; set; }
        public int N17 { get; set; }
        public int N18 { get; set; }
        public int N19 { get; set; }
        public int N2 { get; set; }
        public int N20 { get; set; }
        public int N21 { get; set; }
        public int N22 { get; set; }
        public int N3 { get; set; }
        public int N4 { get; set; }
        public int N5 { get; set; }
        public int N6 { get; set; }
        public int N7 { get; set; }
        public int N8 { get; set; }
        public int N9 { get; set; }
        public int NE1 { get; set; }
        public int NG1 { get; set; }
        public int NR1 { get; set; }
        public int NR6 { get; set; }
        public int NW1 { get; set; }
        public int NW10 { get; set; }
        public int NW11 { get; set; }
        public int NW2 { get; set; }
        public int NW3 { get; set; }
        public int NW4 { get; set; }
        public int NW5 { get; set; }
        public int NW6 { get; set; }
        public int NW7 { get; set; }
        public int NW8 { get; set; }
        public int NW9 { get; set; }
        public int OX1 { get; set; }
        public int OX18 { get; set; }
        public int OX2 { get; set; }
        public int PE1 { get; set; }
        public int PO1 { get; set; }
        public int PO5 { get; set; }
        public int RG1 { get; set; }
        public int RH1 { get; set; }
        public int RH10 { get; set; }
        public int RH11 { get; set; }
        public int RH12 { get; set; }
        public int RH13 { get; set; }
        public int RH14 { get; set; }
        public int RH15 { get; set; }
        public int RH16 { get; set; }
        public int RH17 { get; set; }
        public int RH18 { get; set; }
        public int RH19 { get; set; }
        public int RH2 { get; set; }
        public int RH20 { get; set; }
        public int RH3 { get; set; }
        public int RH4 { get; set; }
        public int RH5 { get; set; }
        public int RH6 { get; set; }
        public int RH7 { get; set; }
        public int RH8 { get; set; }
        public int RH9 { get; set; }
        public int RM1 { get; set; }
        public int RM10 { get; set; }
        public int RM11 { get; set; }
        public int RM12 { get; set; }
        public int RM13 { get; set; }
        public int RM14 { get; set; }
        public int RM15 { get; set; }
        public int RM16 { get; set; }
        public int RM17 { get; set; }
        public int RM18 { get; set; }
        public int RM19 { get; set; }
        public int RM2 { get; set; }
        public int RM20 { get; set; }
        public int RM3 { get; set; }
        public int RM4 { get; set; }
        public int RM5 { get; set; }
        public int RM6 { get; set; }
        public int RM7 { get; set; }
        public int RM8 { get; set; }
        public int RM9 { get; set; }
        public int SE1 { get; set; }
        public int SE10 { get; set; }
        public int SE11 { get; set; }
        public int SE12 { get; set; }
        public int SE13 { get; set; }
        public int SE14 { get; set; }
        public int SE15 { get; set; }
        public int SE16 { get; set; }
        public int SE17 { get; set; }
        public int SE18 { get; set; }
        public int SE19 { get; set; }
        public int SE2 { get; set; }
        public int SE20 { get; set; }
        public int SE21 { get; set; }
        public int SE22 { get; set; }
        public int SE23 { get; set; }
        public int SE24 { get; set; }
        public int SE25 { get; set; }
        public int SE26 { get; set; }
        public int SE27 { get; set; }
        public int SE28 { get; set; }
        public int SE3 { get; set; }
        public int SE4 { get; set; }
        public int SE5 { get; set; }
        public int SE6 { get; set; }
        public int SE7 { get; set; }
        public int SE8 { get; set; }
        public int SE9 { get; set; }
        public int SL0 { get; set; }
        public int SL1 { get; set; }
        public int SL2 { get; set; }
        public int SL3 { get; set; }
        public int SL4 { get; set; }
        public int SL5 { get; set; }
        public int SL6 { get; set; }
        public int SL7 { get; set; }
        public int SL8 { get; set; }
        public int SL9 { get; set; }
        public int SM1 { get; set; }
        public int SM2 { get; set; }
        public int SM3 { get; set; }
        public int SM4 { get; set; }
        public int SM5 { get; set; }
        public int SM6 { get; set; }
        public int SM7 { get; set; }
        public int SO14 { get; set; }
        public int SO50 { get; set; }
        public int SP1 { get; set; }
        public int SP4 { get; set; }
        public int SS0 { get; set; }
        public int SS2 { get; set; }
        public int SW1 { get; set; }
        public int SW10 { get; set; }
        public int SW11 { get; set; }
        public int SW12 { get; set; }
        public int SW13 { get; set; }
        public int SW14 { get; set; }
        public int SW15 { get; set; }
        public int SW16 { get; set; }
        public int SW17 { get; set; }
        public int SW18 { get; set; }
        public int SW19 { get; set; }
        public int SW1E { get; set; }
        public int SW2 { get; set; }
        public int SW20 { get; set; }
        public int SW3 { get; set; }
        public int SW4 { get; set; }
        public int SW5 { get; set; }
        public int SW6 { get; set; }
        public int SW7 { get; set; }
        public int SW8 { get; set; }
        public int SW9 { get; set; }
        public int TN1 { get; set; }
        public int TN13 { get; set; }
        public int TN17 { get; set; }
        public int TN34 { get; set; }
        public int TN38 { get; set; }
        public int TW1 { get; set; }
        public int TW10 { get; set; }
        public int TW11 { get; set; }
        public int TW12 { get; set; }
        public int TW13 { get; set; }
        public int TW14 { get; set; }
        public int TW15 { get; set; }
        public int TW16 { get; set; }
        public int TW17 { get; set; }
        public int TW18 { get; set; }
        public int TW19 { get; set; }
        public int TW2 { get; set; }
        public int TW20 { get; set; }
        public int TW3 { get; set; }
        public int TW4 { get; set; }
        public int TW5 { get; set; }
        public int TW6 { get; set; }
        public int TW7 { get; set; }
        public int TW8 { get; set; }
        public int TW9 { get; set; }
        public int UB1 { get; set; }
        public int UB10 { get; set; }
        public int UB2 { get; set; }
        public int UB3 { get; set; }
        public int UB4 { get; set; }
        public int UB5 { get; set; }
        public int UB6 { get; set; }
        public int UB7 { get; set; }
        public int UB8 { get; set; }
        public int UB9 { get; set; }
        public int W1 { get; set; }
        public int W10 { get; set; }
        public int W11 { get; set; }
        public int W12 { get; set; }
        public int W13 { get; set; }
        public int W14 { get; set; }
        public int W2 { get; set; }
        public int W3 { get; set; }
        public int W4 { get; set; }
        public int W5 { get; set; }
        public int W6 { get; set; }
        public int W7 { get; set; }
        public int W8 { get; set; }
        public int W9 { get; set; }
        public int WC1 { get; set; }
        public int WC2 { get; set; }
        public int WD17 { get; set; }
        public int WD18 { get; set; }
        public int WD19 { get; set; }
        public int WD23 { get; set; }
        public int WD24 { get; set; }
        public int WD25 { get; set; }
        public int WD3 { get; set; }
        public int WD4 { get; set; }
        public int WD5 { get; set; }
        public int WD6 { get; set; }
        public int WD7 { get; set; }
    }
}
