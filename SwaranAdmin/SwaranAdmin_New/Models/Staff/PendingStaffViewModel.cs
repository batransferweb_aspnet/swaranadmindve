﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwaranAdmin_New.Models.Staff
{

    public class PendingStaffViewModel
    {
        public long MemberId { get; set; }
        public long StaffProfileId { get; set; }
        public string StaffName { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
    }

    public class StaffViewModel
    {
        public long MemberId { get; set; }
        public long StaffProfileId { get; set; }
        public string StaffName { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public bool IsEnabled { get; set; }
    }

    public class AdminAllBookings
    {
        public int BookingCount { get; set; }
        public string   BookedPersonEmail { get; set; }
        public string BookedPersonTel { get; set; }
        public string   BookedPersonName   { get; set; } 
        public string UserId { get; set; }
        public int NumberOfPages { get; set; }

    }


    public class CustomerSavedCardViewModel 
    {
        public long CustomerCardId { get; set; }
      //  public string Id { get; set; }
        public string Email { get; set; }
        public long MemberId { get; set; }
        public string FullName { get; set; }
        public string CardClass { get; set; }
        public string CardIssuer { get; set; }
        //public string CardProductTypeDescContactless { get; set; }
        //public string CardProductTypeDescNonContactless { get; set; }
        public string CardSchemeName { get; set; }
        public string CardSchemeType { get; set; }
        public string CardType { get; set; }
        public string CountryCode { get; set; }
        public string Expiry { get; set; }
        public string MaskedCardNumber { get; set; }
      
        public string Name { get; set; }
        public  bool IsPrepaid { get; set; }
        public string Type { get; set; }
        public string Token { get; set; }
        public bool IsPrimary { get; set; }
    }

    public class RemoveSaveCardViewModel
    {
        public string SavecardRemarks { get; set; }  
    }
}