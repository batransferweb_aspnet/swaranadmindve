﻿using System.Web.Optimization;
namespace SwaranAdmin_New
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                   "~/Scripts/jquery-3.1.1.min.js",
                 // "~/Scripts/jquery-3.2.1.min.js",
                    "~/Scripts/jquery-ui-1.12.1.min.js",
                       //  "~/admin-lte/bower_components/jquery/dist/jquery.min.js",
                       //  "~/admin-lte/bower_components/jquery-ui/jquery-ui.min.js",
                       "~/admin-lte/bower_components/bootstrap/dist/js/bootstrap.min.js",
                     // "~/Scripts/bootstrap.js",
                       "~/admin-lte/bower_components/moment/min/moment.min.js",
                       "~/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js",
                         "~/admin-lte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js",
                     "~/admin-lte/bower_components/fastclick/lib/fastclick.js",
                        "~/Scripts/jquery-confirm.js",
                       //  "~/admin-lte/js/app.min.js",
                       "~/Scripts/tether.js",
                        "~/Scripts/jquery.twbsPagination-1.3.1.js",
                         "~/Scripts/bootstrap3-typeahead.js",
                            //"~/Scripts/jquery-ui-1.12.1.min.js",
                            "~/admin-lte/plugins/iCheck/icheck.min.js",
                           "~/admin-lte/js/adminlte.min.js",
                          "~/Scripts/App/dropdownaddress.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/Morris").Include(
                  "~/admin-lte/bower_components/raphael/raphael.min.js",
                         "~/admin-lte/bower_components/morris.js/morris.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/Sparkline ").Include(
                   "~/admin-lte/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jvectormap").Include(
                            "~/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                            "~/admin-lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"));

            bundles.Add(new ScriptBundle("~/bundles/Knob_Chart ").Include(
                    "~/admin-lte/bower_components/jquery-knob/dist/jquery.knob.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*" ));

            bundles.Add(new ScriptBundle("~/bundles/dataTables").Include(
                "~/admin-lte/bower_components/datatables.net/js/jquery.dataTables.min.js",
           "~/admin-lte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js",
            "~/admin-lte/bower_components/datatables.net/js/dataTables.buttons.min.js"
            //"~/admin-lte/bower_components/datatables.net/js/dataTables.select.min.js",
            //"~/admin-lte/bower_components/datatables.net/js/dataTables.editor.min.js"
             ));

            //bundles.Add(new ScriptBundle("~/bundles/gridMvc").Include(
            //    "~/Scripts/gridmvc.min.js"
            // ));


            bundles.Add(new ScriptBundle("~/bundles/daterangepicker").Include(
               "~/admin-lte/bower_components/select2/dist/js/select2.full.min.js",
               "~/admin-lte/bower_components/bootstrap-daterangepicker/daterangepicker.js",
                "~/admin-lte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
                 "~/admin-lte/plugins/timepicker/bootstrap-timepicker.min.js", 
                    "~/admin-lte/plugins/input-mask/jquery.inputmask.js",
                    "~/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js",
                    "~/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js",
                     "~/admin-lte/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"
                ));

             








            bundles.Add(new StyleBundle("~/bundles/mycss").Include( 
              "~/admin-lte/bower_components/bootstrap/dist/css/bootstrap.min.css", 
                "~/admin-lte/bower_components/font-awesome/css/font-awesome.min.css",
                   "~/admin-lte/bower_components/Ionicons/css/ionicons.min.css",
                     "~/admin-lte/css/AdminLTE.min.css",
                     "~/admin-lte/css/skins/_all-skins.min.css",
                     "~/admin-lte/css/alt/AdminLTE-bootstrap-social.min.css",
                    "~/admin-lte/css/alt/AdminLTE-fullcalendar.min.css",
                         "~/admin-lte/css/alt/AdminLTE-select2.min.css",
                         "~/Content/jquery-confirm.css",
                         "~/admin-lte/bower_components/jvectormap/jquery-jvectormap.css",
                         "~/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css",
                         "~/admin-lte/plugins/iCheck/flatblue.css",
                           // "~/admin-lte/bower_components/jquery-ui/themes/base/all.css"
                          "~/admin-lte/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css",
                           "~/Content/themes/base/jquery-ui.min.css"
                      //  "~/Content/DataTables/css/dataTables.bootstrap.min.css",
                      //  "~/Content/bootstrap.min.css"
                      ));
            // <link rel="stylesheet" href="../../bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
            bundles.Add(new StyleBundle("~/bundles/datatable/css").Include(
                   "~/admin-lte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css",
                    "~/admin-lte/bower_components/datatables.net/css/buttons.dataTables.min.css",
                     "~/admin-lte/bower_components/datatables.net/css/select.dataTables.min.css",
                      "~/admin-lte/bower_components/datatables.net/css/editor.dataTables.min.css"
                  ));
            bundles.Add(new StyleBundle("~/bundles/morrischart").Include(
                     "~/admin-lte/bower_components/morris.js/morris.css"
                  ));
            bundles.Add(new StyleBundle("~/bundles/datepicker").Include(
                "~/admin-lte/bower_components/bootstrap-daterangepicker/daterangepicker.css",
                 "~/admin-lte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                 "~/admin-lte/plugins/timepicker/bootstrap-timepicker.min.css"
               ));

            bundles.Add(new LessBundle("~/bundle/less").Include("~/Content/App/my-listbox.less"));
        }
    }
}
