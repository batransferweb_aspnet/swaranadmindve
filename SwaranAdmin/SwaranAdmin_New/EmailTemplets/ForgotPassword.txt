﻿

<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	   <meta name="x-apple-disable-message-reformatting">
      <title>Common Mail</title>
   </head>
   <body style="font: 100% Arial, Helvetica, sans-serif; background: #666666; margin: 0; padding: 0; text-align: center; color: #000000;">
      <div style="width: 46em; background: #FFFFFF; margin: 0 auto; border: 1px solid #000000; text-align: left;">
         <header>
            <div style="background: #fff; padding: 0 10px 0 20px;">
               <a href=""><img src="https://ba-dock.org/abcd/I/temp_images/logo_batransfer.png" alt="logo" style="width:35%; padding-top:20px;"/></a>
               <div style="float:right;">
                  <h4 style="float:right; color:#032272;">Tel: +44 208 900 2299</h4>
                  <br/>
                  <span style=" color: #bf132b; float: right; font-size: 13px; margin-top: -20px;">Hotline</span>
               </div>
            </div>
            <!-- end #header -->
         </header>
         <div style="position: relative; background: #032272; margin-top: 15px; padding-bottom: 2px; padding-top: 2px;">
            <h1 style="margin: 0; padding: 10px 0; color:#FFFFFF; text-align:center; text-transform:uppercase; font-weight:normal; font-size:20px;"> 
			<img src="https://ba-dock.org/abcd/I/temp_images/arrow.png" alt="" style="position:absolute; top:35px; left:329px"/>  Forgot Password  </h1>
         </div>


         <div style="padding: 15px 30px; background: #FFFFFF; text-decoration:none;">
		 <p>  PLEASE DO NOT REPLY THIS MESSAGE   </p>
            <p>Hi , SENDERNAME</p>
             <p>You recently requested to reset your password for your BaTransfer account</p>
             <p>Use the password below<br/> (Use this password for your login and reset your password.)</p>
             <h4>Your temporary password is :- NEWPASSWORD </h4>
			 <p>This password reset is only valid for next 30 minutes</p>
			
			 
			  <p>Thanks,<br/>
			  Batransfer Team</p>
            <div style="border-top:1px solid  #DFDFDF; margin-top:70px;">
               <h3 style="text-align:center; color:#444444;">Our Contact Details</h3>
               <p style="color:#666666; text-align:center; margin-top: -10px;">Always keep our phone numbers and you can contact the office in case if you found any difficulty to locate the driver.</p>
               <div>
                  <p style="color:#666666; text-align:center; margin-top: -10px; font-size:15px;">Free UK land lines phone: <a href="#" style="color: #2693ff; text-decoration: none;">0800 043 0477</a> / UK Mobile: <a href="#" style="color: #2693ff; text-decoration: none;">+44 20 7118 0110</a></p>
                  <p style="color:#666666; text-align:center; margin-top: -10px; font-size:15px;">Abroad: <a href="#" style="color: #2693ff; text-decoration: none;">+44 20 7118 0110</a> / Alternative: <a href="#" style="color: #2693ff; text-decoration: none;">+44 20 8902 0123</a></p>
                  <p style="color:#666666; text-align:center; margin-top: -10px; font-size:15px;"><a href="mailto:contactus@batransfer.com" style="color: #2693ff; text-decoration: none;">contactus@batransfer.com</a></p>
               </div>
            </div>
         </div>



         <!-- end #mainContent -->
         <footer>
            <div style="padding: 20px 30px; background:#323441;">
               <p style="margin: 0; padding: 4px 0; color:#eeeeee; text-align:center;">Thank you for using us and hope to see you again in your future airport transport requirements. <b>Have a nice journey!</b></p>
               <p style="margin: 0; padding: 4px 0; color:#eeeeee; text-align:center; color:#bbbbbb;">Kind Regards</p>
               <p style="margin: 0; padding: 4px 0; color:#eeeeee; text-align:center;"><a href="www.englandtransfers.com" style="color:#2693ff; font-weight:bold; text-align:center; text-decoration:none; margin-top: -10px;">www.englandtransfers.com</a></p>
               <p style="margin: 0; padding: 4px 0; color:#eeeeee; text-align:center;"><a href="" style="color:#2693ff; font-weight:bold; text-align:center; text-decoration:none; margin-top: -10px;"><img src="https://ba-dock.org/abcd/I/temp_images/fbicon.png" alt="footer" style="margin-left:6px; margin-right:6px;"/></a> <a href=""><img src="https://ba-dock.org/abcd/I/temp_images/btn_twitter.png" alt="footer" style="margin-left:6px; margin-right:6px;"/></a></p>
            </div>
            <!-- end #footer -->
         </footer>
         <div>
            <p style="text-align:center;"> <img src="https://ba-dock.org/abcd/I/temp_images/footer_img1.jpg" alt="footer"/></p>
         </div>
      </div>
      <!-- end #container -->
   </body>
</html>

