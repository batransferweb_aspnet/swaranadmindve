﻿ <div style="clear:both;"></div>
<div id="mainContent" style="padding: 15px 30px; background: #FFFFFF;">
<!--/end personal details-->
<div class="res_details" style="width:100%;">
   <div class="titile-area" style="background:#0e3a7d; float:left; width: 50%; ">
      <div class="subtitle" style="background:#c3122a; float:left; margin-right: 10px;">
         <img src="https://ba-dock.org/abcd/I/temp_images/res-icon.png" alt="BAO" style="float:left; width:30px; margin: 8px;"/>
      </div>
      <h2 style="color:#FFFFFF; line-height: 10px; float: left; font-weight: 500; font-size: 20px; text-transform:uppercase;">Reservation Details</h2>
   </div>
   <div style="clear:both;"></div>
   <div class="form-area" style="background:#ebebeb; width:100%; float: left; font-size:17px; padding-left: 20px;padding-bottom: 20px; ">
      <div class="form2-left" style="float:left; width:45%;">
         <div class="form-left" style="float:left; width:45%; color:#666666; font-weight:600; line-height: 5px;">
            <p style="">Reservation ID<span style="float:right;">:</span></p>
            <p style="">Total Amount<span style="float:right;">:</span></p>
         </div>
         <div class="form-right" style="color:#666666; margin-left:5px; float: left; line-height: 8px;">
            <p>BOOKINGID </p>
            <p>&#163; BOOKINGAMOUNT</p>
         </div>
      </div>
      <div class="form2-left" style="float:left; width:55%;">
         <div class="form-left" style="float:left; width:45%; color:#666666; font-weight:600; line-height: 5px;">
            <p style="">Reserved Date<span style="float:right;">:</span></p>
            <p style="">Payment Method<span style="float:right;">:</span></p>
         </div>
         <div class="form-right" style="color:#666666; margin-left:5px; float: left; line-height: 8px;">
            <p>RESERVATIONDATETIME</p>
            <p>BOOKINGPAYMENTMETHOD</p>
         </div>
      </div>
      <div style="float:left; color:#666666;font-size:15px; line-height: 5px;">
         PAYMENTOVERTHEPHONE
      </div>
   </div>
</div>