﻿
$(document).on('invalid-form.validate', 'form', function () {

    $("#LodingAni").fadeOut("fast");
    var button = $(this).find('input[type="submit"]');
    setTimeout(function () {

        button.removeAttr('disabled');

    }, 1);
});
$(document).on('submit', 'form', function () {
    //  var button = $(this).find('input[type="submit"]');
    setTimeout(function () {
        $("form :input[type='submit']").each(function(){
            var button = $(this); // This is the jquery object of the input, do what you will
            button.attr('disabled', 'disabled');
        });


    }, 0);
});

$(document).on('invalid-form.validate', 'form', function () {

    var button = $(this).find('#quoteSubmitBtn');
    setTimeout(function () {

        button.removeAttr('disabled');

    }, 1);
});
$(document).on('submit', 'form', function () {
    var button = $(this).find('#quoteSubmitBtn');
    setTimeout(function () {
        button.attr('disabled', 'disabled');
    }, 0);
});


$(document).ready(function() {

    $(".btnsubmit").click(function() {
        $("#LodingAni").fadeIn("fast");

    });

    $(".jqueryui-marker-datepicker").datepicker(
     {
         minDate: 0,
         dateFormat: "dd/mm/yy",
         onSelect: function (dateText) {
             $.ajax({
                 url: '/api/home/timelist?date=' + this.value,
                 type: 'GET',
                 dataType: 'JSON',
                 success: function (data) {
                     $("#BookingTime").html("");
                     $.each(data, function (index, item) {
                         $("#BookingTime").append(new Option(item.text, item.value));
                     });
                 },
                 error: function (data) {

                     $("#LodingAni").fadeOut("fast");
                 },
                 failure: function (data) {

                     $("#LodingAni").fadeOut("fast");
                 }
             });

         }
     }).css("display", "inline-block")
     .next("button").button(
     {
         icons: { primary: "ui-icon-calendar" },
         lable: "Select a date",
         text: false
     });

    $.validator.setDefaults({
        ignore: [],
        // any other default options and/or rules
    });

});


$(function() {

    $("#viapoint_1").hide();
    $("#viapoint_2").hide();
    $("#viapoint_3").hide();
    $("#viapoint_4").hide();
    $("#viapoint_5").hide();
    var isValidViapoint1 = @Html.Raw(Json.Encode(ViewData["Viapoint1"]));
    if(isValidViapoint1)
    {
        $("#viapoint_1").show();
    }
    var isValidViapoint2= @Html.Raw(Json.Encode(ViewData["Viapoint2"]));
    if(isValidViapoint2)
    {
        $("#viapoint_2").show();
        $(".viapoint1-btn-display").hide();
    }
    var isValidViapoint3 = @Html.Raw(Json.Encode(ViewData["Viapoint3"]));
    if(isValidViapoint3)
    {
        $("#viapoint_3").show();
        $(".viapoint2-btn-display").hide();
    }
    var isValidViapoint4 = @Html.Raw(Json.Encode(ViewData["Viapoint4"]));
    if(isValidViapoint4)
    {
        $("#viapoint_4").show();
        $(".viapoint3-btn-display").hide();
    }
    var isValidViapoint5 = @Html.Raw(Json.Encode(ViewData["Viapoint5"]));
    if(isValidViapoint5)
    {
        $("#viapoint_5").show();
        $(".viapoint4-btn-display").hide();
    }

});


$(".add-viapoint").click(function () {

    $("#viapoint_1").show();
});
$(".add-viapoint2").click(function () {
    $(".viapoint1-btn-display").hide();
    $("#viapoint_2").show();
});
$(".add-viapoint3").click(function () {
    $(".viapoint2-btn-display").hide();
    $("#viapoint_3").show();
});
$(".add-viapoint4").click(function () {
    $(".viapoint3-btn-display").hide();
    $("#viapoint_4").show();
});
$(".add-viapoint5").click(function () {
    $(".viapoint4-btn-display").hide();
    $("#viapoint_5").show();
});
$(".remove-viapoint1").click(function () {
    $("#viapoint_1").hide();

    $("input[name='ViaPointAddress[0].FullAddress']").val("");
    $("input[name='ViaPointAddress[0].AriaPoitId']").val("");
    $("input[name='ViaPointAddress[0].AriaTypeId']").val("");
    $("input[name='ViaPointAddress[0].AddressId']").val("");
    $("input[name='ViaPointAddress[0].OrganisationName']").val("");
    $("input[name='ViaPointAddress[0].DepartmentName']").val("");
    $("input[name='ViaPointAddress[0].BuildingName']").val("");
    $("input[name='ViaPointAddress[0].SubBuildingName']").val("");
    $("input[name='ViaPointAddress[0].BuildingNumber']").val("");
    $("input[name='ViaPointAddress[0].Street']").val("");
    $("input[name='ViaPointAddress[0].PostTown']").val("");
    $("input[name='ViaPointAddress[0].Postcode']").val("");
    $("input[name='ViaPointAddress[0].CountryCode']").val("");
    $("input[name='ViaPointAddress[0].Lat']").val("");
    $("input[name='ViaPointAddress[0].Long']").val("");

    $("#LodingAni").fadeIn("fast");


    $("#quoteSubmitBtn").click();
});
$(".remove-viapoint2").click(function () {
    $(".viapoint1-btn-display").show();
    $("#viapoint_2").hide();
    $("input[name='ViaPointAddress[1].FullAddress']").val("");
    $("input[name='ViaPointAddress[1].AriaPoitId']").val("");
    $("input[name='ViaPointAddress[1].AriaTypeId']").val("");
    $("input[name='ViaPointAddress[1].AddressId']").val("");
    $("input[name='ViaPointAddress[1].OrganisationName']").val("");
    $("input[name='ViaPointAddress[1].DepartmentName']").val("");
    $("input[name='ViaPointAddress[1].BuildingName']").val("");
    $("input[name='ViaPointAddress[1].SubBuildingName']").val("");
    $("input[name='ViaPointAddress[1].BuildingNumber']").val("");
    $("input[name='ViaPointAddress[1].Street']").val("");
    $("input[name='ViaPointAddress[1].PostTown']").val("");
    $("input[name='ViaPointAddress[1].Postcode']").val("");
    $("input[name='ViaPointAddress[1].CountryCode']").val("");
    $("input[name='ViaPointAddress[1].Lat']").val("");
    $("input[name='ViaPointAddress[1].Long']").val("");

    $("#LodingAni").fadeIn("fast");


    $("#quoteSubmitBtn").click();
});
$(".remove-viapoint3").click(function () {
    $(".viapoint2-btn-display").show();
    $("#viapoint_3").hide();
    $("input[name='ViaPointAddress[2].FullAddress']").val("");
    $("input[name='ViaPointAddress[2].AriaPoitId']").val("");
    $("input[name='ViaPointAddress[2].AriaTypeId']").val("");
    $("input[name='ViaPointAddress[2].AddressId']").val("");
    $("input[name='ViaPointAddress[2].OrganisationName']").val("");
    $("input[name='ViaPointAddress[2].DepartmentName']").val("");
    $("input[name='ViaPointAddress[2].BuildingName']").val("");
    $("input[name='ViaPointAddress[2].SubBuildingName']").val("");
    $("input[name='ViaPointAddress[2].BuildingNumber']").val("");
    $("input[name='ViaPointAddress[2].Street']").val("");
    $("input[name='ViaPointAddress[2].PostTown']").val("");
    $("input[name='ViaPointAddress[2].Postcode']").val("");
    $("input[name='ViaPointAddress[2].CountryCode']").val("");
    $("input[name='ViaPointAddress[2].Lat']").val("");
    $("input[name='ViaPointAddress[2].Long']").val("");

    $("#LodingAni").fadeIn("fast");


    $("#quoteSubmitBtn").click();
});
$(".remove-viapoint4").click(function () {
    $(".viapoint3-btn-display").show();
    $("#viapoint_4").hide();
    $("input[name='ViaPointAddress[3].FullAddress']").val("");
    $("input[name='ViaPointAddress[3].AriaPoitId']").val("");
    $("input[name='ViaPointAddress[3].AriaTypeId']").val("");
    $("input[name='ViaPointAddress[3].AddressId']").val("");
    $("input[name='ViaPointAddress[3].OrganisationName']").val("");
    $("input[name='ViaPointAddress[3].DepartmentName']").val("");
    $("input[name='ViaPointAddress[3].BuildingName']").val("");
    $("input[name='ViaPointAddress[3].SubBuildingName']").val("");
    $("input[name='ViaPointAddress[3].BuildingNumber']").val("");
    $("input[name='ViaPointAddress[3].Street']").val("");
    $("input[name='ViaPointAddress[3].PostTown']").val("");
    $("input[name='ViaPointAddress[3].Postcode']").val("");
    $("input[name='ViaPointAddress[3].CountryCode']").val("");
    $("input[name='ViaPointAddress[3].Lat']").val("");
    $("input[name='ViaPointAddress[3].Long']").val("");

    $("#LodingAni").fadeIn("fast");


    $("#quoteSubmitBtn").click();
});
$(".remove-viapoint5").click(function () {
    $(".viapoint4-btn-display").show();
    $("#viapoint_5").hide();
    $("input[name='ViaPointAddress[4].FullAddress']").val("");
    $("input[name='ViaPointAddress[4].AriaPoitId']").val("");
    $("input[name='ViaPointAddress[4].AriaTypeId']").val("");
    $("input[name='ViaPointAddress[4].AddressId']").val("");
    $("input[name='ViaPointAddress[4].OrganisationName']").val("");
    $("input[name='ViaPointAddress[4].DepartmentName']").val("");
    $("input[name='ViaPointAddress[4].BuildingName']").val("");
    $("input[name='ViaPointAddress[4].SubBuildingName']").val("");
    $("input[name='ViaPointAddress[4].BuildingNumber']").val("");
    $("input[name='ViaPointAddress[4].Street']").val("");
    $("input[name='ViaPointAddress[4].PostTown']").val("");
    $("input[name='ViaPointAddress[4].Postcode']").val("");
    $("input[name='ViaPointAddress[4].CountryCode']").val("");
    $("input[name='ViaPointAddress[4].Lat']").val("");
    $("input[name='ViaPointAddress[4].Long']").val("");

    $("#LodingAni").fadeIn("fast");

    $("#quoteSubmitBtn").click();

});