﻿

$(document).ready(function () {


    function validateDate(dtValue) {
        var regex = /[0-3][0-9]\/[0-1][0-9]\/[0-9]{4} [0-1][0-9]:[0-5][0-9]/;
        return dtValue.match(regex);
    }

    var pickuppointRequest = null;
    var dropOffPointRequest = null;
    var viapoint1Request = null;
    var viapoint2Request = null;
    var viapoint3Request = null;
    var viapoint4Request = null;
    var viapoint5Request = null;
    //^\S+.*?\S+$

    $('input.BookingFrom').typeahead({
        hint: true,
        //delay:0,
        showHintOnFocus: "all",
        name: 'BookingFromLabel',
        minLength: 0,
        autoSelect: true,
        highlight: true,
        items: 500,

        source: function (query, process) {

            var that = this;
            var regex = "^[a-zA-Z0-9\\s/g_.-]*$";

            if (!query.match(regex)) {
                $("input[name='FromPostCode']").val(" ");
            }
            if (query.match(regex) && query.length > 1) {

                pickuppointRequest = $.ajax({
                    url: '/api/address/alladdress',
                    type: 'GET',
                    dataType: 'JSON',
                    data: 'name=' + query,
                    beforeSend: function () {
                        if (pickuppointRequest != null) {
                            pickuppointRequest.abort();
                        }
                        that.$element.addClass('loadinggif');
                    },
                    complete: function () {
                        that.$element.removeClass('loadinggif');
                    },

                    success: function (data) {
                        //debugger;
                        map = {};
                        var cnames = new Array();
                        for (var i = 0; i < data.length; i++) {

                            var id = data[i].AreaPointId + "|" + data[i].AreaTypeId + "|" + data[i].FullAddress + "|" + data[i].AddressId + "|"
                                + data[i].OrganisationName + "|" + data[i].DepartmentName + "|" + data[i].BuildingName + "|" + data[i].SubBuildingName + "|"
                                + data[i].BuildingNumber + "|" + data[i].Street + "|" + data[i].PostTown + "|" + data[i].Postcode + "|"
                                + data[i].CountryCode + "|" + data[i].Latitude + "|" + data[i].Longitude + "|"
                                + data[i].AirportCode + "|" + "|" + data[i].IsNonElastic + "|" + data[i].FullAddress + "|0";
                            name = data[i].FullAddress;
                            map[name] = { id: id, name: name };
                            cnames.push(name);
                        }
                        process(cnames);

                    }
                });

            } else {

                $.ajax({
                    url: '/api/address/favouriteaddress',
                    type: 'GET',
                    dataType: 'JSON',
                    data: 'userid=' + $("input[name='UserId']").val(),
                    beforeSend: function () {

                        that.$element.addClass('loadinggif');
                    },
                    complete: function () {
                        that.$element.removeClass('loadinggif');
                    },

                    success: function (data) {
                        //debugger;
                        map = {};
                        var cnames = new Array();
                        for (var i = 0; i < data.length; i++) {

                            var id = data[i].AreaPointId + "|" + data[i].AreaTypeId + "|" + data[i].FullAddress + "|"
                                + data[i].AddressId + "|" + data[i].OrganisationName + "|" + data[i].DepartmentName + "|"
                                + data[i].BuildingName + "|" + data[i].SubBuildingName + "|" + data[i].BuildingNumber + "|"
                                + data[i].Street + "|" + data[i].PostTown + "|" + data[i].Postcode + "|" + data[i].CountryCode + "|"
                                + data[i].Latitude + "|" + data[i].Longitude + "|" + data[i].AirportCode + "|" + data[i].Note + "|"
                                + data[i].IsNonElastic + "|" + data[i].FullAddressLabel + "|" + data[i].FavouriteAddressId;
                            name = data[i].FullAddressLabel;
                            map[name] = { id: id, name: name };
                            cnames.push(name);
                        }
                        process(cnames);
                    }
                });
            }
        },
        sorter: function (items) {
            return items;// items.sort();
        },
        matcher: function (item) {

            return true;


        },
        updater: function (item) {

            debugger;
            $("input[name='FromFixedPriceAriaPointId']").val(map[item].id.split('|')[0]);
            var ariapointTypeId = (map[item].id.split('|')[1]) === "0" ? "1" : map[item].id.split('|')[1];
            
            $("input[name='FromAriapointTypeId']").val(ariapointTypeId);
            $("input[name='BookingFrom']").val(map[item].id.split('|')[2]);
            $("input[name='BookingFromLabel']").val(map[item].id.split('|')[18]); 
            $("input[name='FromAddressId']").val(map[item].id.split('|')[3]); 
            var organisationName = (map[item].id.split('|')[4]) === 'null' ? "" : map[item].id.split('|')[4];
            $("input[name='FromOrganisationName']").val(organisationName);
            var department = (map[item].id.split('|')[5]) === 'null' ? "" : map[item].id.split('|')[5];
            $("input[name='FromDepartmentName']").val(department);
            var buildingName = (map[item].id.split('|')[6]) === 'null' ? "" : map[item].id.split('|')[6];
            $("input[name='FromBuildingName']").val(buildingName);
            var subBuildingName = (map[item].id.split('|')[7]) === 'null' ? "" : map[item].id.split('|')[7];
            $("input[name='FromSubBuildingName']").val(subBuildingName);
            var buildingNumber = (map[item].id.split('|')[8]) === '0' || (map[item].id.split('|')[8]) === 'null' ? "" : map[item].id.split('|')[8];
            $("input[name='FromBuildingNumber']").val(buildingNumber);
            var street = (map[item].id.split('|')[9]) === 'null' ? "" : map[item].id.split('|')[9];
            $("input[name='FromStreet']").val(street);
            var postTown = (map[item].id.split('|')[10]) === 'null' ? "" : map[item].id.split('|')[10];
            $("input[name='FromPostTown']").val(postTown);
            $("input[name='FromPostCode']").val(map[item].id.split('|')[11]);
            $("input[name='CountryCode']").val(map[item].id.split('|')[12]); 
            $("input[name='FromLat']").val(map[item].id.split('|')[13]);
            $("input[name='FromLong']").val(map[item].id.split('|')[14]);
            $("input[name='FromAirportCode']").val(map[item].id.split('|')[15]);
            $("input[name='PickupNote']").val((map[item].id.split('|')[16]) === 'null' ? "" : map[item].id.split('|')[16]);
            $("input[name='IsNonElasticFromAdd']").val(map[item].id.split('|')[17]);
            $("input[name='FromFavouriteAddressId']").val(map[item].id.split('|')[19]);
          

            return item;
        }

    });
    
    $('input.BookingTo').typeahead({
        hint: true,
        // delay: 0,
        showHintOnFocus: "all",
        name: 'BookingFrom',
        minLength: 0,
        autoSelect: true,
        highlight: true,
        items: 500,
        source: function (query, process) {

            var that = this;
            var regex = "^[a-zA-Z0-9\\s/g_.-]*$";
            if (!query.match(regex)) {
                $("input[name='ToPostCode']").val(" ");
            }
            if (query.match(regex) && query.length > 1) {

                dropOffPointRequest = $.ajax({
                    url: '/api/address/alladdress',
                    type: 'GET',
                    dataType: 'JSON',
                    data: 'name=' + query,
                    beforeSend: function () {
                        if (dropOffPointRequest != null) {
                            dropOffPointRequest.abort();
                        }
                        that.$element.addClass('loadinggif');
                    },

                    complete: function () {
                        that.$element.removeClass('loadinggif');
                    },
                    success: function (data) {

                        map = {};
                        var cnames = new Array();
                        for (var i = 0; i < data.length; i++) {
                            var id = data[i].AreaPointId + "|" + data[i].AreaTypeId + "|" + data[i].FullAddress + "|"
                                + data[i].AddressId + "|" + data[i].OrganisationName + "|" + data[i].DepartmentName + "|"
                                + data[i].BuildingName + "|" + data[i].SubBuildingName + "|" + data[i].BuildingNumber + "|"
                                + data[i].Street + "|" + data[i].PostTown + "|" + data[i].Postcode + "|" + data[i].CountryCode + "|"
                                + data[i].Latitude + "|" + data[i].Longitude
                                + "|" + data[i].AirportCode + "|" + "|" + data[i].IsNonElastic + "|" + data[i].FullAddress + "|0";
                            name = data[i].FullAddress;
                            map[name] = { id: id, name: name };
                            cnames.push(name);

                            //console.log(data);
                            //var i = new Array();
                        }
                        process(cnames);
                    }
                });
            } else {

                $.ajax({
                    url: '/api/address/favouriteaddress',
                    type: 'GET',
                    dataType: 'JSON',
                    data: 'userid=' + $("input[name='UserId']").val(),
                    beforeSend: function () {

                        that.$element.addClass('loadinggif');
                    },
                    complete: function () {
                        that.$element.removeClass('loadinggif');
                    },

                    success: function (data) {

                        map = {};
                        var cnames = new Array();
                        for (var i = 0; i < data.length; i++) {
                            // //debugger;
                            var id = data[i].AreaPointId + "|" + data[i].AreaTypeId + "|" + data[i].FullAddress + "|" + data[i].AddressId + "|"
                                + data[i].OrganisationName + "|" + data[i].DepartmentName + "|" + data[i].BuildingName + "|"
                                + data[i].SubBuildingName + "|" + data[i].BuildingNumber + "|" + data[i].Street + "|"
                                + data[i].PostTown + "|" + data[i].Postcode + "|" + data[i].CountryCode + "|"
                                + data[i].Latitude + "|" + data[i].Longitude + "|" + data[i].AirportCode + "|"
                                + "|" + data[i].IsNonElastic + "|" + data[i].FullAddressLabel + "|" + data[i].FavouriteAddressId;
                            name = data[i].FullAddressLabel;
                            map[name] = { id: id, name: name };
                            cnames.push(name);
                        }
                        process(cnames);
                    }
                });
            }
        },
        sorter: function (items) {
            return items;//.sort();
        },
        matcher: function (item) {
            //if (item.toLowerCase().indexOf(this.query.trim().toLowerCase()) != -1) {
            return true;
            // }
        },
        updater: function (item) {


            $("input[name='ToFixedPriceAriaPointId']").val(map[item].id.split('|')[0]);
            var ariapointTypeId = (map[item].id.split('|')[1]) === "0" ? "1" : map[item].id.split('|')[1];
            if (ariapointTypeId == 6) {

                $("#toAreaPointPostcode-div").show();
                $("#ToAreaPointPostcode").val("");
            } else
            { 
                $("#toAreaPointPostcode-div").hide();
            }
            $("input[name='ToAriapointTypeId']").val(ariapointTypeId);
            $("input[name='BookingTo']").val(map[item].id.split('|')[2]);
            $("input[name='BookingToLabel']").val(map[item].id.split('|')[18]);
            //var ss = map[item].id.split('|')[18];
            //alert(ss);
            $("input[name='ToAddressId']").val(map[item].id.split('|')[3]);
            var organisationName = (map[item].id.split('|')[4]) === 'null' ? "" : map[item].id.split('|')[4];
            $("input[name='ToOrganisationName']").val(organisationName);
            var department = (map[item].id.split('|')[5]) === 'null' ? "" : map[item].id.split('|')[5];
            $("input[name='ToDepartmentName']").val(department);
            var buildingName = (map[item].id.split('|')[6]) === 'null' ? "" : map[item].id.split('|')[6];
            $("input[name='ToBuildingName']").val(buildingName);
            var subBuildingName = (map[item].id.split('|')[7]) === 'null' ? "" : map[item].id.split('|')[7];
            $("input[name='ToSubBuildingName']").val(subBuildingName);
            var buildingNumber = (map[item].id.split('|')[8]) === '0' || (map[item].id.split('|')[8]) === 'null' ? "" : map[item].id.split('|')[8];
            $("input[name='ToBuildingNumber']").val(buildingNumber);
            var street = (map[item].id.split('|')[9]) === 'null' ? "" : map[item].id.split('|')[9];
            $("input[name='ToStreet']").val(street);
            var postTown = (map[item].id.split('|')[10]) === 'null' ? "" : map[item].id.split('|')[10];
            $("input[name='ToPostTown']").val(postTown);
            $("input[name='ToPostCode']").val(map[item].id.split('|')[11]);
            $("input[name='CountryCode']").val(map[item].id.split('|')[12]);
            $("input[name='ToLat']").val(map[item].id.split('|')[13]);
            $("input[name='ToLong']").val(map[item].id.split('|')[14]);
            $("input[name='ToAirportCode']").val(map[item].id.split('|')[15]);
            $("input[name='IsNonElasticToAdd']").val(map[item].id.split('|')[17]);
            $("input[name='ToFavouriteAddressId']").val(map[item].id.split('|')[19]);
             

            return item;
        }

    });
  

 
});

