﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using SwaranAdmin_New.Models;

[assembly: OwinStartupAttribute(typeof(SwaranAdmin_New.Startup))]
namespace SwaranAdmin_New
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();
        }
        private void createRolesandUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            // In Startup iam creating first Admin Role and creating a default Admin User     
            if (!roleManager.RoleExists("Admin"))
            {

                // first we create Admin rool    
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);

                //Here we create a Admin super user who will maintain the website                   



                //var user = UserManager.FindByEmail("uwusri@gmail.com");
                ////Add default User to Role Admin    
                //if (user != null)
                //{
                //    var result1 = UserManager.AddToRole(user.Id, "Admin");

                //}
            }
            if (!roleManager.RoleExists("Controller"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Controller";
                roleManager.Create(role);

            }

            if (!roleManager.RoleExists("Manager"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Manager";
                roleManager.Create(role);

            }
            // creating Creating Manager role     
            if (!roleManager.RoleExists("Accounts Manage"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Accounts Manage";
                roleManager.Create(role);

            }

            // creating Creating Employee role     
            if (!roleManager.RoleExists("Driver Records Manager"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Driver Records Manager";
                roleManager.Create(role);

            }
            if (!roleManager.RoleExists("Driver Training Manager"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Driver Training Manager";
                roleManager.Create(role);

            }
            if (!roleManager.RoleExists("Customer Relation"))
            {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Customer Relation";
                roleManager.Create(role);

            }
        }
    }
}
