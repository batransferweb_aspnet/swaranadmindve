﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataAccess;
using SwaranAdmin_New.Models.Pricing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SwaranAdmin_New.Controllers
{
    //[SessionAuthorize]
    [Authorize(Users = "krishnananthan.v@gmail.com,agi,uwusri@gmail.com,siv@batransfer.com")]
    public class PricingController : Controller
    {

        private PricingDal _price = PricingDal.Instance;

        #region Views

        // GET: Pricing
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Rate()
        {
            var customerRates = _price.GetCustomerRates();
            ViewBag.Message = TempData["SuccessMessage"] != null ? TempData["SuccessMessage"] : "";
            return View(customerRates);
        }

        [HttpGet]
        public ActionResult EditRate(long rateId)
        {

            GetVatRates();
            var customerRate = _price.GetCustomerRateDetail(rateId); 
            return View(customerRate);
        }

        [HttpPost]
        public ActionResult EditRate(CustomerRateDetailModel model)
        {
            bool isUpdateSuccess = _price.UpdateCustomerRate(model);

            if(isUpdateSuccess)
            {
                TempData["SuccessMessage"] = "The rate has been updated successfully.";
                //return RedirectToAction("EditRate", new { rateId = model.RateId });
                return RedirectToAction("Rate");
            }
            else
            {
                return View(model);
            }            
        }

        private void GetVatRates()
        {
            var vatRates = _price.GetVatRates();
            ViewData["VatRates"] = vatRates;
        }

        private void GetCustomerRates()
        {
            var customerRate = _price.GetCustomerRate();
            ViewData["CustomerRates"] = customerRate;
        }

        [HttpGet]
        public ActionResult AddRateVehicleType(long? rateId)
        {

            var availableVehicleTypes = _price.GetAvailableVehicleTypes(rateId);
            NewRateVehicleRequest newNewRateVehicleRequest = new NewRateVehicleRequest();
            newNewRateVehicleRequest.VehicleTypeId = 0;
            newNewRateVehicleRequest.RateId = (long)rateId;
            ViewBag.AvailableVehicleTypes = availableVehicleTypes;
            ViewBag.RateId = rateId;
            ViewBag.NavigateUrl = "/Rate";
            ViewBag.RateId = rateId;
            ViewBag.NavigateDescription = RateHelper.GetRateName((long)rateId);

            return View(newNewRateVehicleRequest);
        }

        [HttpGet]
        public ActionResult AddRateVehicleTypeDay(long rateId, long rateCustomerVehicleTypeId)
        {

           
            var days = _price.GetAvailableDays(rateCustomerVehicleTypeId);

            //var availableVehicleTypes = _price.GetAvailableVehicleTypes(rateCustomerVehicleTypeId);
            NewRateVehicleDayRequest newNewRateVehicleDayRequest = new NewRateVehicleDayRequest();
            newNewRateVehicleDayRequest.Day = 0;
            newNewRateVehicleDayRequest.RateCustomerVehicleTypeId = rateCustomerVehicleTypeId;
            newNewRateVehicleDayRequest.DistanceDetail = new List<RateCustomerDistanceDom>();
            newNewRateVehicleDayRequest.DistanceDetail.Add(new RateCustomerDistanceDom()
            {
                RateCustomerDistanceDayId = 0,
                DistanceId = 0,
                DistanceFrom = 0,
                DistanceTo = 0,
                Amount = 0,
                Unit = "mile",
                IsDefault = false
            });

            //newNewRateVehicleDayRequest.RateCustomerVehicleTypeId = rateCustomerVehicleTypeId;
            ViewBag.Days = days;
            ViewBag.RateId = rateId;
            ViewBag.NavigateUrl = "/Rate";
            ViewBag.NavigateDescription = RateHelper.GetRateName(rateId);
            ViewBag.NavigateUrl1 = "/LoadRateVehicleType/" + rateId;//Request.Url.AbsoluteUri;
            ViewBag.NavigateDescription1 = RateHelper.GetVehicleTypeNameByRateCustomerVehicleTypeId(rateCustomerVehicleTypeId);

            return View(newNewRateVehicleDayRequest);
        }


        [HttpGet]
        public ActionResult LoadRateVehicleType(long rateId)
        {
            var customerRateVehicleTypes = _price.GetVehicleTypeForCustomerRate(rateId);

            ViewBag.NavigateUrl = "/Rate";
            ViewBag.RateId = rateId;
            ViewBag.NavigateDescription = RateHelper.GetRateName(rateId);
            //return PartialView("_RateVehicleView", customerRateVehicleTypes);
            //return PartialView(customerRateVehicleTypes);

            return View(customerRateVehicleTypes);
        }

        [HttpGet]
        public ActionResult LoadRateVehicleTypeDayDetail(long rateId, long rateCustomerVehicleTypeId)
        {
            var customerRateDistanceDetail = _price.GetRateVehicleTypeDayDetail(rateCustomerVehicleTypeId);

            ViewBag.NavigateUrl = "/Rate";
            ViewBag.NavigateDescription = RateHelper.GetRateName(rateId);
            ViewBag.NavigateUrl1 = "/LoadRateVehicleType/" + rateId;//Request.Url.AbsoluteUri;
            ViewBag.NavigateDescription1 = RateHelper.GetVehicleTypeNameByRateCustomerVehicleTypeId(rateCustomerVehicleTypeId);
            ViewBag.RateCustomerVehicleTypeId = rateCustomerVehicleTypeId;
            ViewBag.RateId = rateId;

            return View(customerRateDistanceDetail);
        }

        [HttpGet]
        public ActionResult LoadRateVehicleTypeDistanceDetail(long rateCustomerDistanceDayId)
        {
            var customerRateDistanceDetail = _price.GetRateVehicleTypeDistanceDetail(rateCustomerDistanceDayId);

            var relevantCustomerRate = RateHelper.GetRateByRateByRateCustomerDistanceDayId(rateCustomerDistanceDayId);

            if (relevantCustomerRate != null)
            {
                ViewBag.NavigateUrl = "/Rate";
                ViewBag.NavigateDescription = RateHelper.GetRateName(relevantCustomerRate.RateId);
                ViewBag.NavigateUrl1 = "/LoadRateVehicleType/" + relevantCustomerRate.RateId;//Request.Url.AbsoluteUri;
                ViewBag.NavigateDescription1 = relevantCustomerRate.VehicleTypeName;
                ViewBag.NavigateUrl2 = "/Pricing/LoadRateVehicleTypeDayDetail?rateId=" + relevantCustomerRate.RateId + "&rateCustomerVehicleTypeId=" + relevantCustomerRate.CustomerRateVehicleTypeId;//Request.Url.AbsoluteUri;
                ViewBag.NavigateDescription2 = relevantCustomerRate.DayName;
            }

            return View(customerRateDistanceDetail);
        }

        [HttpGet]
        public ActionResult LoadRateDailySurge(long rateId)
        {
            var customerRateDailySurges = _price.GetDailySurgeForCustomerRate(rateId);

            ViewBag.NavigateUrl = "/Rate";
            ViewBag.RateId = rateId;
            ViewBag.NavigateDescription = RateHelper.GetRateName(rateId);
            //return PartialView("_RateVehicleView", customerRateVehicleTypes);
            //return PartialView(customerRateVehicleTypes);

            return View(customerRateDailySurges);
        }

        [HttpGet]
        public ActionResult LoadRateDailySurgeDayDetail(long rateId, int dayId)
        {
            var customerRateDistanceDetail = _price.GetRateDailySurgeDayDetail(rateId, dayId);

            ViewBag.NavigateUrl = "/Rate";
            ViewBag.NavigateDescription = RateHelper.GetRateName(rateId);
            ViewBag.NavigateUrl1 = "/Pricing/LoadRateDailySurge/?rateId=" + rateId;//Request.Url.AbsoluteUri;
            ViewBag.NavigateDescription1 = Enum.GetName(typeof(EnumCustomerRateDay), dayId);
            ViewBag.DayId = dayId;
            ViewBag.RateId = rateId;
            ViewBag.Message = TempData["SuccessMessage"] != null ? TempData["SuccessMessage"] : "";
            return View(customerRateDistanceDetail);
        }

        [HttpGet]
        public ActionResult EditRateDailySurgeDetail(long dailySurgeId, int dayId)
        {
            var customerRateDistanceDetail = _price.GetRateDailySurgeDetail(dailySurgeId);

            //var relevantCustomerRate = RateHelper.GetRateName(rateCustomerDistanceDayId);

            if (customerRateDistanceDetail != null)
            {

                //ViewBag.NavigateUrl = "/Rate";
                //ViewBag.NavigateDescription = RateHelper.GetRateName(customerRateDistanceDetail.CustomerRateId);
                //ViewBag.NavigateUrl1 = "/LoadRateVehicleType/" + customerRateId;//Request.Url.AbsoluteUri;
                //ViewBag.NavigateDescription1 = relevantCustomerRate.VehicleTypeName;
                //ViewBag.NavigateUrl2 = "/Pricing/LoadRateVehicleTypeDayDetail?rateId=" + relevantCustomerRate.RateId + "&rateCustomerVehicleTypeId=" + relevantCustomerRate.CustomerRateVehicleTypeId;//Request.Url.AbsoluteUri;
                //ViewBag.NavigateDescription2 = relevantCustomerRate.DayName;
                ViewBag.NavigateUrl = "/Rate";
                ViewBag.NavigateDescription = RateHelper.GetRateName(customerRateDistanceDetail.CustomerRateId);
                ViewBag.NavigateUrl1 = "/LoadRateDailySurge/" + customerRateDistanceDetail.CustomerRateId;//Request.Url.AbsoluteUri;
                ViewBag.NavigateDescription1 = Enum.GetName(typeof(EnumCustomerRateDay), dayId);
                ViewBag.DayId = dayId;
                ViewBag.RateId = customerRateDistanceDetail.CustomerRateId;
            }

            return View(customerRateDistanceDetail);
        }

        [HttpPost]
        public ActionResult EditRateDailySurgeDetail(RateDailySurgeDayDom model)
        {

            model.IsAllPlot = model.AreaPointIds == null ? true : model.IsAllPlot;

            bool isUpdateSuccess = _price.UpdateDailySurge(model);

            if (isUpdateSuccess)
            {
                TempData["SuccessMessage"] = "The daily surge has been updated successfully.";
                return RedirectToAction("LoadRateDailySurgeDayDetail", new { rateId = model.CustomerRateId, dayId = model.DayId });
            }
            else
            {
                return View(model);
            }
        }

        [HttpGet]
        //public ActionResult AddRateDailySurge(long? rateId, int dayId)
        public ActionResult AddRateDailySurge(long? rateId)
        {

            //var availableVehicleTypes = _price.GetAvailableVehicleTypes(rateId);
            var days = _price.GetAvailableDaysForDailySurge(rateId);
            RateDailySurgeDayDom newRateDailySurgeDayDom = new RateDailySurgeDayDom();
            newRateDailySurgeDayDom.DayId = 0;
            newRateDailySurgeDayDom.CustomerRateId = (long)rateId;
            newRateDailySurgeDayDom.IsAllPlot = false;
            ViewBag.Days = days;
            ViewBag.RateId = rateId;
            ViewBag.TimeList = _price.GetTimeList();
            //ViewBag.NavigateUrl = "/Rate";
            //ViewBag.NavigateDescription = RateHelper.GetRateName((long)rateId);
            ViewBag.NavigateUrl = "/Rate";
            ViewBag.NavigateDescription = RateHelper.GetRateName((long)rateId);
            ViewBag.NavigateUrl1 = "/Pricing/LoadRateDailySurge/?rateId=" + rateId;//Request.Url.AbsoluteUri;
            return View(newRateDailySurgeDayDom);
        }

        [HttpPost]
        public ActionResult AddRateDailySurge(RateDailySurgeDayDom model)
        {
            model.IsAllPlot = model.AreaPointIds == null ? true : model.IsAllPlot;

            bool isCreateSuccess = _price.CreateDailySurge(model);

            if (isCreateSuccess)
            {
                TempData["SuccessMessage"] = "The daily surge has been added successfully.";
                return RedirectToAction("LoadRateDailySurgeDayDetail", new { rateId = model.CustomerRateId, dayId = model.DayId });
            }
            else
            {
                var days = _price.GetAvailableDaysForDailySurge(model.CustomerRateId);
                ViewBag.Days = days;
                ViewBag.TimeList = _price.GetTimeList();
                return View(model);
            }
        }


        [HttpGet]
        public ActionResult LoadRateVehicleTypeDistanceDetailNew(long rateCustomerDistanceDayId)
        {
            var customerRateDistanceDetail = _price.GetRateVehicleTypeDistanceDetailNew(rateCustomerDistanceDayId);

            var relevantCustomerRate = RateHelper.GetRateByRateByRateCustomerDistanceDayId(rateCustomerDistanceDayId);

            if (relevantCustomerRate != null)
            {
                ViewBag.NavigateUrl = "/Rate";
                ViewBag.NavigateDescription = RateHelper.GetRateName(relevantCustomerRate.RateId);
                ViewBag.NavigateUrl1 = "/LoadRateVehicleType/" + relevantCustomerRate.RateId;//Request.Url.AbsoluteUri;
                ViewBag.NavigateDescription1 = relevantCustomerRate.VehicleTypeName;
                ViewBag.NavigateUrl2 = "/Pricing/LoadRateVehicleTypeDayDetail?rateId=" + relevantCustomerRate.RateId + "&rateCustomerVehicleTypeId=" + relevantCustomerRate.CustomerRateVehicleTypeId;//Request.Url.AbsoluteUri;
                ViewBag.NavigateDescription2 = relevantCustomerRate.DayName;
            }

            return View(customerRateDistanceDetail);
        }


        //[HttpGet]
        //public ActionResult LoadRateVehicleTypeDayDetailNew(long rateId, long rateCustomerVehicleTypeId)
        //{
        //    var customerRateDistanceDetail = _price.GetRateVehicleTypeDayDetail(rateCustomerVehicleTypeId);

        //    ViewBag.NavigateUrl = "/Rate";
        //    ViewBag.NavigateDescription = RateHelper.GetRateName(rateId);
        //    ViewBag.NavigateUrl1 = "/LoadRateVehicleType/" + rateId;//Request.Url.AbsoluteUri;
        //    ViewBag.NavigateDescription1 = RateHelper.GetVehicleTypeNameByRateCustomerVehicleTypeId(rateCustomerVehicleTypeId);
        //    ViewBag.RateCustomerVehicleTypeId = rateCustomerVehicleTypeId;
        //    ViewBag.RateId = rateId;

        //    return View(customerRateDistanceDetail);
        //}


        #endregion

        #region Old Commented

        //#region Partial Views

        //[HttpGet]
        //public ActionResult LoadRateVehicleTypeDayDetailPartial(long rateId, long rateCustomerVehicleTypeId)
        //{
        //    var customerRateDistanceDetail = _price.GetRateVehicleTypeDayDetail(rateCustomerVehicleTypeId);
        //    ViewBag.VehicleName = RateHelper.GetVehicleTypeNameByRateCustomerVehicleTypeId(rateCustomerVehicleTypeId);
        //    return PartialView("_LoadRateVehicleTypeDayDetailView", customerRateDistanceDetail);
        //}

        //[HttpGet]
        //public ActionResult LoadRateVehicleTypePartial(long rateId)
        //{
        //    var customerRateVehicleTypes = _price.GetVehicleTypeForCustomerRate(rateId);
        //    var availableVehicleTypes = _price.GetAvailableVehicleTypes(rateId);
        //    ViewBag.RateId = rateId;
        //    ViewBag.RateName = RateHelper.GetRateName(rateId);
        //    ViewBag.AvailableVehicleTypes = availableVehicleTypes;

        //    List<SelectListItem> days = new List<SelectListItem>();
        //    days.Add(new SelectListItem { Value = 0.ToString(), Text = "Default" });
        //    days.Add(new SelectListItem { Value = 1.ToString(), Text = "Monday" });
        //    days.Add(new SelectListItem { Value = 2.ToString(), Text = "Tuesday" });
        //    days.Add(new SelectListItem { Value = 3.ToString(), Text = "Wednesday" });
        //    days.Add(new SelectListItem { Value = 4.ToString(), Text = "Thursday" });
        //    days.Add(new SelectListItem { Value = 5.ToString(), Text = "Friday" });
        //    days.Add(new SelectListItem { Value = 6.ToString(), Text = "Saturday" });
        //    days.Add(new SelectListItem { Value = 7.ToString(), Text = "Sunday" });
        //    ViewBag.Days = days;

        //    return PartialView("_RateVehicleView", customerRateVehicleTypes);
        //}

        //[HttpGet]
        //public ActionResult LoadRateVehicleTypeDistanceDetailPartial(long rateCustomerDistanceDayId)
        //{
        //    var customerRateDistanceDetail = _price.GetRateVehicleTypeDistanceDetail(rateCustomerDistanceDayId);
        //    ViewBag.RateCustomerDistanceDayId = rateCustomerDistanceDayId;

        //    return PartialView("_LoadRateVehicleTypeDistanceDetailView", customerRateDistanceDetail);
        //}

        //[HttpGet]
        //public ActionResult AddRateVehicleTypePartial(long rateId)
        //{

        //    var availableVehicleTypes = _price.GetAvailableVehicleTypes(rateId);
        //    NewRateVehicleRequest newNewRateVehicleRequest = new NewRateVehicleRequest();
        //    newNewRateVehicleRequest.VehicleTypeId = 0;
        //    newNewRateVehicleRequest.RateId = rateId;
        //    newNewRateVehicleRequest.Day = 0;
        //    ViewBag.AvailableVehicleTypes = availableVehicleTypes;
        //    ViewBag.RateName = _price.GetRateNameByRateId(rateId);

        //    List<SelectListItem> days = new List<SelectListItem>();
        //    days.Add(new SelectListItem { Value = 0.ToString(), Text = "Default" });
        //    days.Add(new SelectListItem { Value = 1.ToString(), Text = "Monday" });
        //    days.Add(new SelectListItem { Value = 2.ToString(), Text = "Tuesday" });
        //    days.Add(new SelectListItem { Value = 3.ToString(), Text = "Wednesday" });
        //    days.Add(new SelectListItem { Value = 4.ToString(), Text = "Thursday" });
        //    days.Add(new SelectListItem { Value = 5.ToString(), Text = "Friday" });
        //    days.Add(new SelectListItem { Value = 6.ToString(), Text = "Saturday" });
        //    days.Add(new SelectListItem { Value = 7.ToString(), Text = "Sunday" });
        //    ViewBag.Days = days;

        //    return PartialView("_AddRateVehicleTypeView", newNewRateVehicleRequest);
        //}

        //[HttpPost]
        //public ActionResult AddRateVehicleTypeDayPartial(NewRateVehicleDayRequest request)
        //{
        //    List<NewRateVehicleDayRequest> newRequest = new List<NewRateVehicleDayRequest>();
        //    newRequest.Add(request);

        //    return PartialView("_AddRateVehicleTypeDayView", newRequest);
        //}

        //#endregion

        #endregion

    }
}