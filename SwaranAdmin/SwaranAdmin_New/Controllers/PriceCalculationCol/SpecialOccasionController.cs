﻿using SwaranAdmin_New.DataAccess.PriceCalculationDal;
using SwaranAdmin_New.Models.PriceCalculation;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SwaranAdmin_New.Controllers.PriceCalculationCol
{
    [Authorize]
    public class SpecialOccasionController : Controller
    {
        private SpecialOccasionDal rexSpecialOccasion = SpecialOccasionDal.Instance;
        // GET: SpecialOccasion
        public ActionResult SpecialOccasionView()
        {

            return View();
        }

        public ActionResult AddSpecialOccasionView()
        {
            var model = new SpecialOccasionViewModel();
            model.IsAllPlot = 1;
            return View(model);
        }

        [HttpPost]
        public ActionResult AddSpecialOccasionView(SpecialOccasionViewModel model)
        {
            //model.IsAllPlot = model.AriaPointId == null ? true : model.IsAllPlot;
            //if (model.IsAllPlot)
            //{
            //    if (ModelState.ContainsKey("DropOffCharge"))
            //        ModelState["DropOffCharge"].Errors.Clear();

            //    if(model.Surcharge ==0)
            //    {
            //        ModelState.AddModelError("Surcharge", "Invalid Surcharge");
            //    }
            //}

            //model.IsAllPlot = model.AriaPointId == null ? 1 : model.IsAllPlot;
            model.IsAllPlot = (model.AreaPointId == null && model.IsAllPlot == 1) ? 1 : model.IsAllPlot;
            model.IsBothAreapointApplied = (model.FixedPriceId != null && model.IsAllPlot == 3);

            if (model.IsAllPlot == 1)
            {
                if (ModelState.ContainsKey("DropOffCharge"))
                    ModelState["DropOffCharge"].Errors.Clear();

                if (model.Surcharge == 0)
                {
                    ModelState.AddModelError("Surcharge", "Invalid Surcharge");
                }
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            rexSpecialOccasion.SaveSpecialOccasion(model);
            return View("SpecialOccasionView");
        }

        public ActionResult EditSpecialOccasionView(long id)
        {
            var model = rexSpecialOccasion.GetCurrentSpecialOccasion(id);
            model.OccasionDataRange = model.FromDate + "-" + model.ToDate;
             
            return View(model);
        }

        [HttpPost]
       public ActionResult EditSpecialOccasionView(SpecialOccasionViewModel model) 
       {
            //model.IsAllPlot = model.AriaPointId == null ? true : model.IsAllPlot;
            //if (model.IsAllPlot)
            //{
            //    if (ModelState.ContainsKey("DropOffCharge"))
            //        ModelState["DropOffCharge"].Errors.Clear();

            //    if (model.Surcharge == 0)
            //    {
            //        ModelState.AddModelError("Surcharge", "Invalid Surcharge");
            //    }
            //}

            model.IsAllPlot = (model.AreaPointId == null && model.IsAllPlot == 1) ? 1 : model.IsAllPlot;
            //model.IsBothAreapointApplied = (model.IsAllPlot == 2);
            model.IsBothAreapointApplied = (model.FixedPriceId != null && model.IsAllPlot == 3);

            if (model.IsAllPlot == 1)
            {
                if (ModelState.ContainsKey("DropOffCharge"))
                    ModelState["DropOffCharge"].Errors.Clear();

                if (model.Surcharge == 0)
                {
                    ModelState.AddModelError("Surcharge", "Invalid Surcharge");
                }
            }            

            if (!ModelState.IsValid)
            {
                return View(model);
            }
              rexSpecialOccasion.SaveSpecialOccasion(model);
            return View("SpecialOccasionView");
        }
    }
}