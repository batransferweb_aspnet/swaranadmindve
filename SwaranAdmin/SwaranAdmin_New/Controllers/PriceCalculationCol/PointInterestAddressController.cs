﻿using SwaranAdmin_New.DataAccess.PriceCalculationDal;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System.Linq;
using System.Web.Mvc;

namespace SwaranAdmin_New.Controllers.PriceCalculationCol
{
    [Authorize(Users = "krishnananthan.v@gmail.com,uwusri@gmail.com,agi")]
    public class PointInterestAddressController : Controller
    {

        private PointInterestAddressDal rexPointOfInterest = PointInterestAddressDal.Instance;

        // GET: PointInterestAddress
        public ActionResult PointInterestAllAddressView()
        {
            return View();
        }

        public ActionResult PointInterestView() 
        {
            return View();
        }

        // GET: PointInterestAddress/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PointInterestAddress/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PointInterestAddress/Create
        [HttpPost]
        public ActionResult Create(AddressViewModel model)
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                }
                ModelState.Clear();
                return View();
            }
            catch
            {
                return View();
            }
        }

        // GET: PointInterestAddress/Edit/5
        public ActionResult EditPoI(int id)
        {
            var pointofInterestViewModel = new PointofInterestViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.PointOfInterest_Place.Any(x => x.AriaPointId == id))
                    {
                        var objPoI = db.PointOfInterest_Place.First(x => x.AriaPointId == id);
                        pointofInterestViewModel.AreaType = objPoI.AreaType;
                        pointofInterestViewModel.AriapointId = objPoI.AriaPointId;
                        pointofInterestViewModel.Location = objPoI.FullPostcode + " " + objPoI.Name;
                        pointofInterestViewModel.PickupCharge = objPoI.PickupCharge;
                        pointofInterestViewModel.DropoffCharge = objPoI.DropoffCharge;
                        pointofInterestViewModel.IsAway = objPoI.IsAway; 

                    }
                }

                return View(pointofInterestViewModel);
            }
            catch
            {
                return View(pointofInterestViewModel);
            }
        }

        // POST: PointInterestAddress/Edit/5
        [HttpPost]
        public ActionResult EditPoI(PointofInterestViewModel model)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.PointOfInterest_Place.Any(x => x.AriaPointId == model.AriapointId))
                    {
                        var objPoI = db.PointOfInterest_Place.First(x => x.AriaPointId == model.AriapointId);
                        objPoI.PickupCharge = model.PickupCharge;
                        objPoI.DropoffCharge = model.DropoffCharge;
                        objPoI.IsAway = model.IsAway;
                        db.SaveChanges();

                    }
                }

                return RedirectToAction("PointInterestAllAddressView");
            }
            catch
            {
                return View();
            }
        }


        public ActionResult EditBasicpriceView(int id)
        {
            var basicPrice = new BasicPriceViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.sp_swaran_currentbasicprice(id).Any())
                    {
                        var objBasicPrice = db.sp_swaran_currentbasicprice(id).First();
                        basicPrice.FixedPriceId = objBasicPrice.FixedPriceId;
                        basicPrice.FromLocation = objBasicPrice.FromLocation;
                        basicPrice.ToLocation = objBasicPrice.ToLocation;
                        basicPrice.BasicPrice = objBasicPrice.BasicPrice;

                    }
                }
                return View(basicPrice);
            }
            catch
            {
                return View();
            }
        }


        [HttpPost]
        public ActionResult EditBasicpriceView(BasicPriceViewModel model)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.FixedPrice_AriaPoints_Distance.Any(x => x.PriceId == model.FixedPriceId))
                    {
                        var objPoI = db.FixedPrice_AriaPoints_Distance.First(x => x.PriceId == model.FixedPriceId);
                        objPoI.BasicPrice = model.BasicPrice;
                        db.SaveChanges();

                    }
                }

                return RedirectToAction("PointInterestAllAddressView");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult BulkPriceUpdate()
        {
            var model = new MultiplePriceUpdateViewModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult BulkPriceUpdate(MultiplePriceUpdateViewModel model)
        {
             
            if (model.AriaPointId != null)
            {
                if (model.BasicPrice <= 0 )
                {
                    ModelState.AddModelError("Basic Price", "Invalid Basic Price");
                }
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            rexPointOfInterest.SaveBulkUpdatePrice(model);
            return View("BulkPriceUpdate");
            //return View(model);
        }

        public ActionResult AddFixedPrice()
        {
            //var model = new MultiplePriceUpdateViewModel();
            //return View(model);
            return View();
        }

    }
}
