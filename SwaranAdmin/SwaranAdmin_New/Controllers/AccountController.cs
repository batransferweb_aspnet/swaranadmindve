﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataAccess;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SwaranAdmin_New.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        ApplicationDbContext context;
        private RegisterDal _registor = RegisterDal.Instance;
        public AccountController()
        {
            context = new ApplicationDbContext();
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            string IPAddress = IPHelper.GetIPAddress(Request.ServerVariables["HTTP_VIA"],
                Request.ServerVariables["HTTP_X_FORWARDED_FOR"],
                Request.ServerVariables["REMOTE_ADDR"]);
            ErrorHandling.LogFileWrite("NEW IP -> "+ IPAddress, "NEW IP ->"+ IPAddress, "NEW Ip-> "+ IPAddress, "NEW Ip-> "+ IPAddress);

            bool isIpSet = bool.Parse(ConfigurationManager.AppSettings["IsIpSet"]);
            if (isIpSet)
            {
               // string ip = System.Web.HttpContext.Current.Request.UserHostAddress ?? "";
                string ipport = ValueParseHelper.GetIPAddress();
                string ip = ipport.Split(':').Length > 0 ? ipport.Split(':')[0] : ipport;

                ErrorHandling.LogFileWrite(ipport, ipport, ip, ip);
                string MyIpVal = ConfigurationManager.AppSettings["MyIpVal"].ToString();
                var list = MyIpVal.Split('|').ToList();

                if (list != null && list.Contains(ip))
                {
                    ViewBag.ReturnUrl = returnUrl;
                    return View();
                }
                return Redirect("https://www.batransfer.com/");
            }
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {

            if (ModelState.ContainsKey("Email"))
                ModelState["Email"].Errors.Clear();

            if (!ModelState.IsValid)
            {
                model.IsEmailVarified = true;
                return View(model);
            }

            if (UserHelper.IsStaffAccountEnabled(model.UserName))
            {

                string message = string.Empty;
                bool isvaliduser = false;

                if (!IsVerifiedEmailFromLogin(model.UserName, out message, out isvaliduser))
                {
                    ModelState.AddModelError(string.Empty, message);

                    model.IsEmailVarified = !isvaliduser;
                    ViewBag.emailVerification = message;
                    return View(model);
                }
                else
                {
                    model.IsEmailVarified = true;
                }

                var currentuser = await SignInManager.UserManager.FindByNameAsync(model.UserName);
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, change to shouldLockout: true
                var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, shouldLockout: false);

                switch (result)
                {

                    case SignInStatus.Success:
                        if (currentuser.IsTempPassword)
                        {
                            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                            string encodemailtoken = System.Web.HttpContext.Current.Server.UrlEncode(currentuser.EmailVerificationToken);
                            return RedirectToLocal("/Account/ResetPassword?code=" + encodemailtoken);
                        }

                        if (string.IsNullOrEmpty(returnUrl))
                        {
                            var relevantUser = await UserManager.FindByNameAsync(model.UserName);

                            using (var db = new BATransferEntities())
                            {
                                StaffLogin newStaffLogin = new StaffLogin()
                                {
                                    UserId = relevantUser.Id,
                                    LoginToken = Security.EncryptToken(relevantUser.Id + DateTime.UtcNow.ToString(), DateTime.UtcNow.ToString()),
                                    Status = (int)LoginStatusEnum.LoggedIn,
                                    DeviceId = string.Empty,
                                    DeviceType = string.Empty,
                                    DeviceOsType = string.Empty,
                                    DeviceOsVersion = string.Empty,
                                    NotificationId = string.Empty,
                                    DeviceIpAddress = string.Empty,
                                    DevicePublicIpAddress = string.Empty,
                                    IsEnabled = true,
                                    IsDeleted = false,
                                    loggedInDateTime = DateTime.Now,
                                    loggedOutDateTime = null,
                                    sysCreatedDate = DateTime.UtcNow,
                                    sysModifiedDate = DateTime.UtcNow
                                };

                                db.StaffLogins.Add(newStaffLogin);
                                db.SaveChanges();

                                StaffLoginHistory newStaffLoginHistory = new StaffLoginHistory()
                                {
                                    StaffLoginId = newStaffLogin.StaffLoginId,
                                    UserId = newStaffLogin.UserId,
                                    LoginToken = newStaffLogin.LoginToken,
                                    Status = newStaffLogin.Status,
                                    DeviceId = newStaffLogin.DeviceId,
                                    DeviceType = newStaffLogin.DeviceType,
                                    DeviceOsType = newStaffLogin.DeviceOsType,
                                    DeviceOsVersion = newStaffLogin.DeviceOsVersion,
                                    NotificationId = newStaffLogin.NotificationId,
                                    DeviceIpAddress = newStaffLogin.DeviceIpAddress,
                                    DevicePublicIpAddress = newStaffLogin.DevicePublicIpAddress,
                                    IsEnabled = newStaffLogin.IsEnabled,
                                    IsDeleted = newStaffLogin.IsDeleted,
                                    loggedInDateTime = newStaffLogin.loggedInDateTime,
                                    loggedOutDateTime = newStaffLogin.loggedOutDateTime,
                                    sysCreatedDate = newStaffLogin.sysCreatedDate,
                                    sysModifiedDate = newStaffLogin.sysModifiedDate
                                };

                                db.StaffLoginHistories.Add(newStaffLoginHistory);
                                db.SaveChanges();

                            }

                            bool isFirstLogin = UserHelper.IsStaffFirstLogin(model.UserName);

                            if (isFirstLogin)
                            {
                                //var relevantUser = UserManager.FindByName(model.UserName);
                                return RedirectToAction("AdditionalDetails", "Home", new { uid = relevantUser.Id });
                                //return RedirectToAction("TelBookingView", "TelBooking");
                                //return RedirectToAction("Index", "Home");
                                //return RedirectToAction("AdditionalDetails", "Home", new { Id = System.Web.HttpContext.Current.Server.UrlEncode(model.UserName) });
                            }
                            else
                            {
                                //return RedirectToAction("TelBookingView", "TelBooking");
                                return RedirectToAction("Index", "Home");
                            }
                        }
                        else
                        {
                            return Redirect(returnUrl);
                        }
                    //return RedirectToAction("Index", "Home");
                    //return RedirectToAction("TelBookingView", "TelBooking");
                    //if (string.IsNullOrEmpty(returnUrl))
                    //    return RedirectToAction("Index", "Home"); 
                    //return Redirect(returnUrl);

                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    case SignInStatus.RequiresVerification:
                        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = model.RememberMe });
                    case SignInStatus.Failure:
                    default:
                        ModelState.AddModelError("", "Invalid login attempt.");
                        return View(model);
                }
            }
            else
            {
                ModelState.AddModelError("", "Your staff account has not been enabled. Please contact the administrator.");
                return View(model);
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {

            if (HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            var registor = (RegisterViewModel)Session["Register"] ?? new RegisterViewModel();

            var listTile = _registor.LoadTitles();
            SelectList list = new SelectList(listTile, "TitleId", "TitleName");
            ViewBag.titleList = list;


            var countryPhoneCodeList = _registor.LoadCountrys();
            SelectList listphoneCode = new SelectList(countryPhoneCodeList, "CountryCode", "PhoneDisplayCode");
            ViewBag.listphoneCode = listphoneCode;

            var countryCodeList = _registor.LoadCountrys();
            SelectList listCountryCode = new SelectList(countryCodeList, "CountryCode", "CountryName");
            ViewBag.listCountryCode = listCountryCode;

            if (!string.IsNullOrEmpty(System.Web.HttpContext.Current.Request.UserHostAddress))
            {
                var ip = System.Web.HttpContext.Current.Request.UserHostAddress;

                var countryObj = FreeGeoApiHelper.GetCountryCode(ip);
                registor.CountryCode = countryObj.country_code;
                registor.LandPhoneCountryCode = countryObj.country_code;
                registor.MobileNumberCountryCode = countryObj.country_code;
            }

            registor.IsMobileNumberPrimary = true;



            return View(registor);

        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            try
            {

                //model.PrimaryPhoneNumber = model.MobileNumber;
                var countryPhoneCodeList = _registor.LoadCountrys();
                SelectList listphoneCode = new SelectList(countryPhoneCodeList, "CountryCode", "PhoneDisplayCode");
                ViewBag.listphoneCode = listphoneCode;

                var countryCodeList = _registor.LoadCountrys();
                SelectList listCountryCode = new SelectList(countryCodeList, "CountryCode", "CountryName");
                ViewBag.listCountryCode = listCountryCode;

                //var response = Request["g-recaptcha-response"];
                //string secret = ConfigHelper.GetGooglereCAPTCHA((int)BookingSiteEnum.ET);
                //var client = new WebClient();
                //var reply =
                //    client.DownloadString(
                //        string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secret, response));

                //var captchaResponse = JsonConvert.DeserializeObject<CaptchaResponse>(reply);
                ////when response is false check for the error message
                //if (!captchaResponse.Success)
                //{
                //    if (captchaResponse.ErrorCodes.Count <= 0) return View();

                //    var error = captchaResponse.ErrorCodes[0].ToLower();
                //    switch (error)
                //    {
                //        case ("missing-input-secret"):
                //            ViewBag.Message = "The secret parameter is missing.";
                //            break;
                //        case ("invalid-input-secret"):
                //            ViewBag.Message = "The secret parameter is invalid or malformed.";
                //            break;

                //        case ("missing-input-response"):
                //            ViewBag.Message = "The response parameter is missing.";
                //            break;
                //        case ("invalid-input-response"):
                //            ViewBag.Message = "The response parameter is invalid or malformed.";
                //            break;

                //        default:
                //            ViewBag.Message = "Error occured. Please try again";
                //            break;
                //    }
                //    return View(model);
                //}

                var userForUserName = UserManager.FindByName(model.UserName);

                var userForEmail = UserManager.FindByEmail(model.Email);

                if (userForUserName == null && userForEmail == null)
                {

                    model.LandPhoneCountryCode = CountryCodeHelper.GetCountryCode(model.LandPhoneCountryCode);
                    model.MobileNumberCountryCode = CountryCodeHelper.GetCountryCode(model.MobileNumberCountryCode);
                    model.CountryId = CountryCodeHelper.GetCountryId(model.CountryCode);

                    string primaryPhoneNumber = model.MobileNumber;


                    long memberId = _registor.GenerateMemberId(model.FirstName);

                    //if (_registor.IsMobileNumberExsist(primaryPhoneNumber))
                    //{
                    //    ModelState.AddModelError("MobileNumber", "Mobile Number Already Exists");
                    //    return View(model);
                    //}

                    if (!ModelState.IsValid)
                    {
                        return View(model);
                    }
                    string email_Toket = Security.EncryptToken(model.Email + model.PrimaryPhoneNumber,
                                   DateTime.UtcNow.ToString());

                    var user = new ApplicationUser()
                    {

                        UserName = model.UserName,
                        Email = model.Email,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Gender = 0,
                        LandPhoneCountryCode = "",
                        LandPhoneNumber = "",
                        MobileNumberCountryCode = "",
                        MobileNumber = model.MobileNumber,
                        PrimaryPhoneNumber = primaryPhoneNumber,
                        IsMobileNumberPrimary = true,
                        IsSendSms = true,
                        IsCompanyUser = false,
                        CompanyId = null,
                        IsEnabled = true,
                        IsDeleted = false,
                        sysModifiedDate = DateTime.UtcNow,
                        sysCreatedDate = DateTime.UtcNow,
                        isUserSuspended = false,
                        reason = string.Empty,
                        isUserValidated = true,
                        resetPasswordToken = string.Empty,
                        resetPasswordTokenCreatedDate = DateTime.UtcNow,
                        resetPasswordTokenValidTime = 0,
                        countryCode = "",
                        isFacebookLogin = false,
                        //facebookToken = "",
                        facebookToken = memberId.ToString(),
                        isAuthorized = false,
                        IsEmailVerified = false,
                        IsPhoneVerified = false,
                        EmailVerificationToken = email_Toket,
                        PhoneVerificationToken = GenerateStringHelper.GenerateRandomString(4, true, false, false),
                        EmailVerificationTokenCreatedDate = DateTime.UtcNow,
                        PhoneVerificationTokenCreatedDate = DateTime.UtcNow,
                        TitleId = 1,
                        IsCustomer = false,
                        IsDriver = false,
                        IsStaff = true,
                        CountryId = null,
                        NationalIdentity = string.Empty,
                        PostalCode = "",
                        Street = "",
                        Town = "",
                        DateOfBirth = null,
                        IsPasswordResetRequired = true,
                        IsCompanyAdmin = false,
                        MemberId = memberId

                    };

                    var result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        var isSuccess = await _registor.CreateUserProfile(user.Id, model);

                        string userEmailVerificationBaseUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.StaffUserEmailVerificationBaseUrl);

                        string url = userEmailVerificationBaseUrl + System.Web.HttpContext.Current.Server.UrlEncode(email_Toket);

                        string logFilePath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/RegistorToken.txt"));
                        string text = System.IO.File.ReadAllText(logFilePath);
                        text = text.Replace("TOKENSRI", email_Toket);
                        text = text.Replace("MAINURL", url);

                        EmailCredentialsViewModel emailCredentials = EmailHelper.GetPasswordResetEmailCredentials();

                        var listEmails = new List<string>() { model.Email };
                        var email = new RootEmail();
                        email.Attachments = null;
                        email.Body = text;
                        email.FromEmailAddress = emailCredentials.FromEmail;
                        email.Username = emailCredentials.Username;
                        email.Password = emailCredentials.Password;
                        email.Port = emailCredentials.Port;
                        email.Smtp = emailCredentials.Smtp;
                        email.Subject = "Email confirmation";
                        email.ToEmailAddresses = listEmails;
                        email.IsEnableSsl = emailCredentials.IsEnabledSSL;
                        email.DisplayName = emailCredentials.DisplayName;
                        //   string jsonEmail = JSonHelper.ConvertObjectToJSon(email);

                        string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                        await EmailHelper.SendEmailWithoutAttachtment(email, apiUrl);

                        AuthenticationManager.SignOut();

                        var emailToken = new EmailTokenViewModel();
                        emailToken.Email = model.Email;
                        emailToken.UserId = user.Id;
                        emailToken.CurrentEmailToken = "";

                        Session["emailToken"] = emailToken;
                        return RedirectToAction("EmailToken", "Account");

                    }

                    AddErrors(result);
                }
                else if (userForEmail != null || userForUserName != null)
                {

                    if (userForEmail != null && userForEmail.IsStaff)
                    {
                        ModelState.AddModelError("", "Already a staff account related to this email. Please login.");
                        model.Proceed = false;
                    }
                    else if (userForEmail != null && !userForEmail.IsStaff)
                    {
                        if (!model.Proceed)
                        {
                            ModelState.AddModelError("", "Already an account related to this email. Do you want to use this email for staff account also? ");
                            model.Proceed = true;
                        }
                        else
                        {

                            userForEmail.IsStaff = true;

                            var updateResult = await UserManager.UpdateAsync(userForEmail);

                            if (updateResult.Succeeded)
                            {
                                #region Create staff profile

                                string email_Toket = Security.EncryptToken(model.Email + model.PrimaryPhoneNumber,
                                   DateTime.UtcNow.ToString());

                                var isSuccess = await _registor.CreateUserProfile(userForEmail.Id, model);

                                string userEmailVerificationBaseUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.UserEmailVerificationBaseUrl);

                                string url = userEmailVerificationBaseUrl + System.Web.HttpContext.Current.Server.UrlEncode(email_Toket);

                                string logFilePath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/RegistorToken.txt"));
                                string text = System.IO.File.ReadAllText(logFilePath);
                                text = text.Replace("TOKENSRI", email_Toket);
                                text = text.Replace("MAINURL", url);

                                EmailCredentialsViewModel emailCredentials = EmailHelper.GetPasswordResetEmailCredentials();

                                var listEmails = new List<string>() { model.Email };
                                var email = new RootEmail();
                                email.Attachments = null;
                                email.Body = text;
                                email.FromEmailAddress = emailCredentials.FromEmail;
                                email.Username = emailCredentials.Username;
                                email.Password = emailCredentials.Password;
                                email.Port = emailCredentials.Port;
                                email.Smtp = emailCredentials.Smtp;
                                email.Subject = "Email confirmation";
                                email.ToEmailAddresses = listEmails;
                                email.IsEnableSsl = emailCredentials.IsEnabledSSL;
                                email.DisplayName = emailCredentials.DisplayName;
                                //   string jsonEmail = JSonHelper.ConvertObjectToJSon(email);

                                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                                await EmailHelper.SendEmailWithoutAttachtment(email, apiUrl);

                                AuthenticationManager.SignOut();

                                var emailToken = new EmailTokenViewModel();
                                emailToken.Email = model.Email;
                                emailToken.UserId = userForEmail.Id;
                                emailToken.CurrentEmailToken = "";

                                Session["emailToken"] = emailToken;
                                return RedirectToAction("EmailToken", "Account");

                                #endregion
                            }

                            AddErrors(updateResult);
                        }
                    }

                    if (userForUserName != null && userForUserName.IsStaff)
                    {
                        ModelState.AddModelError("", "Already a staff account related to this username. Please login.");
                        model.Proceed = false;
                    }
                    else if (userForUserName != null && !userForUserName.IsStaff)
                    {
                        if (!model.Proceed)
                        {
                            ModelState.AddModelError("", "Already an account related to this username. Do you want to use this username for staff account also? ");
                            model.Proceed = true;
                        }
                        else
                        {
                            userForUserName.IsStaff = true;

                            var updateResult = await UserManager.UpdateAsync(userForUserName);

                            if (updateResult.Succeeded)
                            {
                                #region Create staff profile

                                string email_Toket = Security.EncryptToken(model.Email + model.PrimaryPhoneNumber,
                                   DateTime.UtcNow.ToString());

                                var isSuccess = await _registor.CreateUserProfile(userForUserName.Id, model);

                                string userEmailVerificationBaseUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.UserEmailVerificationBaseUrl);

                                string url = userEmailVerificationBaseUrl + System.Web.HttpContext.Current.Server.UrlEncode(email_Toket);

                                string logFilePath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/RegistorToken.txt"));
                                string text = System.IO.File.ReadAllText(logFilePath);
                                text = text.Replace("TOKENSRI", email_Toket);
                                text = text.Replace("MAINURL", url);

                                EmailCredentialsViewModel emailCredentials = EmailHelper.GetPasswordResetEmailCredentials();

                                var listEmails = new List<string>() { model.Email };
                                var email = new RootEmail();
                                email.Attachments = null;
                                email.Body = text;
                                email.FromEmailAddress = emailCredentials.FromEmail;
                                email.Username = emailCredentials.Username;
                                email.Password = emailCredentials.Password;
                                email.Port = emailCredentials.Port;
                                email.Smtp = emailCredentials.Smtp;
                                email.Subject = "Email confirmation";
                                email.ToEmailAddresses = listEmails;
                                email.IsEnableSsl = emailCredentials.IsEnabledSSL;
                                email.DisplayName = emailCredentials.DisplayName;
                                //   string jsonEmail = JSonHelper.ConvertObjectToJSon(email);

                                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                                await EmailHelper.SendEmailWithoutAttachtment(email, apiUrl);

                                AuthenticationManager.SignOut();

                                var emailToken = new EmailTokenViewModel();
                                emailToken.Email = model.Email;
                                emailToken.UserId = userForUserName.Id;
                                emailToken.CurrentEmailToken = "";

                                Session["emailToken"] = emailToken;
                                return RedirectToAction("EmailToken", "Account");

                                #endregion
                            }

                            AddErrors(updateResult);
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
                ModelState.AddModelError("", "Registration is not successful. Please, try again or contact the administrator.");
                return View(model);
            }

            return View(model);
        }


        //[ErrorFilter]
        //[CompressContent]
        //[MinifyHtml]
        //[OutputCache(CacheProfile = "CacheCompressedContent5Minutes")]
        [AllowAnonymous]
        public ActionResult EmailToken()
        {

            var tokent = Session["emailToken"] != null ? (EmailTokenViewModel)Session["emailToken"] : new EmailTokenViewModel();
            TempData["success"] = "Email successfully sent";
            return View(tokent);
        }

        [HttpPost]
        //[ErrorFilter]
        //[CompressContent]
        //[MinifyHtml]
        //[OutputCache(CacheProfile = "CacheCompressedContent5Minutes")]
        [AllowAnonymous]
        public async Task<ActionResult> EmailToken(EmailTokenViewModel token)
        {
            string message = string.Empty;

            if (!string.IsNullOrEmpty(token.UserId))
            {

                if (_registor.IsVerified(token, out message))
                {
                    TempData["success"] = message;
                    return View("Login");
                }
                else
                {
                    TempData["success"] = message;
                    return View();
                }
            }
            else
            {
                TempData["success"] = "session expired try again";
                return View();
            }
        }

        [AllowAnonymous]
        public ActionResult CreateEmailToken()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateEmailToken(CreateEmailTokenViewModel login)
        {
            var tokent = await _registor.CreateEmailToket(login.Email);
            if (tokent.IsSuccess)
            {
                TempData["success"] = tokent.Message;
                return RedirectToAction("EmailToken", "Account");
            }
            else
            {
                TempData["success"] = tokent.Message; ;
                return View();
            }
        }
        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                var user = await UserManager.FindByNameAsync(model.Username);

                string message = string.Empty;
                bool isvaliduser = false;

                if (!IsUserVerified(model.Username, out message, out isvaliduser))
                {

                    TempData["Message"] = message;
                    return View(model);
                }

                if (user != null)
                {
                    string tempPassword = GenerateRandomString(6);

                    var passwordtoken = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

                    var result = await UserManager.ResetPasswordAsync(user.Id, passwordtoken, tempPassword);

                    if (result.Succeeded)
                    {
                        string token = _registor.GenerateEmailToken(user.Id);

                        user.EmailVerificationToken = token;
                        user.EmailVerificationTokenCreatedDate = DateTime.UtcNow;
                        user.IsTempPassword = true;
                        UserManager.Update(user);

                        var isSendEmail = await _registor.SendForgotPassword(model.Username, token, user.FirstName, tempPassword);

                        if (isSendEmail)
                        {
                            TempData["Success"] = "Email has been sent.";
                            var login = new LoginViewModel();
                            login.UserName = user.UserName;
                            login.IsEmailVarified = user.IsEmailVerified;
                            return View("Login", login);
                        }
                        else
                        {
                            TempData["Message"] = "Something went wrong try again";
                        }
                    }
                    else
                    {
                        TempData["Message"] = "Please try again";
                    }
                }
                else
                {
                    TempData["Message"] = "Email is not registered";
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var entityValidationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in entityValidationErrors.ValidationErrors)
                    {
                        ErrorHandling.LogFileWrite(ex.Message, "Property: " + validationError.PropertyName, System.Reflection.MethodBase.GetCurrentMethod().Name, " Error: " + validationError.ErrorMessage);
                    }
                }
                TempData["Message"] = "Something went wrong ";
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            var status = _registor.ChckIsValidEmailToken(code);

            var forgotPassword = new ResetPasswordViewModel();
            if (status.IsSuccess)
            {
                forgotPassword.UserId = status.UserId;
                return View(forgotPassword);
            }
            else
            {
                TempData["Message"] = status.Message;
            }


            return View(forgotPassword);
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            try
            {

                if (ModelState.IsValid)
                {

                    var user = await UserManager.FindByIdAsync(model.UserId);


                    var token = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

                    var result = await UserManager.ResetPasswordAsync(user.Id, token, model.Password);
                    if (result.Succeeded)
                    {
                        user.IsTempPassword = false;
                        await UserManager.UpdateAsync(user);

                        TempData["success"] = "Password updated successfully";

                        return RedirectToAction("Login", "Account");

                    }
                    else
                    {
                        ViewBag.SucceededMessage = "Please try again";
                    }
                }
            }
            catch (Exception ex)
            {


            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

            //return RedirectToAction("Index", "Home");
            string returnUrl = Request.UrlReferrer != null ? Request.UrlReferrer.PathAndQuery : "/TelBooking/TelBookingView";
            return RedirectToAction("Login", "Account", new { returnUrl = returnUrl });
            //string returnUrl = Request.UrlReferrer != null ? Request.UrlReferrer.PathAndQuery : "/TelBooking/TelBookingView";
            //return Redirect(returnUrl);
        }

        [HttpGet]
        //[ValidateAntiForgeryToken]
        public ActionResult LockScreen(string returnUrl)
        {

            //Set the model data
            LoginViewModel model = new LoginViewModel();
            model.UserName = User.Identity.GetUserName();

            //Get returnUrl
            //if (string.IsNullOrEmpty(returnUrl) && Request.UrlReferrer != null)
            //    returnUrl = Request.UrlReferrer.PathAndQuery;

            ViewBag.ReturnURL = returnUrl;
            //Get the user profile image

            ////Sign out the user 
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            //Session.Abandon();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Abandon();
            return RedirectToAction("Login", "Account");
        }
        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }


        [AllowAnonymous]
        public ActionResult CustomerForgotPasswordView()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CustomerForgotPasswordView(ForgotPasswordViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                var user = UserManager.FindByEmail(model.Username);
                if (user == null)
                {
                    //TempData["loginerror"] = "Email is not registered";
                    //TempData["IsEmailVarified"] = true;
                    //return RedirectToAction("Login", "Account");
                    TempData["Message"] = "Email is not being registered.";
                    return View(model);
                }
                if (!user.IsEmailVerified && !user.IsPhoneVerified)
                {
                    //TempData["loginerror"] = "Email or mobile number not verified, Please click below link and verify your email or mobile";
                    //TempData["EmailVarifiedEmail"] = model.Email;
                    //TempData["IsEmailVarified"] = false;
                    //return RedirectToAction("Login", "Account");
                    TempData["Message"] = "Email or mobile number is not being verified.";
                    return View(model);
                }

                string tempPassword = GenerateRandomString(6);
                var passwordtoken = await UserManager.GeneratePasswordResetTokenAsync(user.Id);

                var result = await UserManager.ResetPasswordAsync(user.Id, passwordtoken, tempPassword);
                if (result.Succeeded)
                {
                    string emailToket = Security.EncryptToken(user.Email + user.PrimaryPhoneNumber,
                        DateTime.UtcNow.ToString());
                    user.EmailVerificationToken = emailToket;
                    user.EmailVerificationTokenCreatedDate = DateTime.UtcNow;
                    user.IsTempPassword = true;
                    UserManager.Update(user);
                    //if (user.MobileNumberCountryCode != null && user.MobileNumberCountryCode.Equals("44"))
                    //{
                    //    SendFogotPasswordSms(user.MobileNumber, tempPassword);
                    //}
                    //else
                    //{
                    //    SendFogotPasswordSms(user.MobileNumberCountryCode + user.MobileNumber, tempPassword);
                    //}
                    var isSendEmail = await _registor.SendCustomerForgotPassword(model.Username, emailToket, user.FirstName, tempPassword);
                    if (isSendEmail)
                    {

                        TempData["success"] = "Email has been sent.";


                    }
                    else
                    {
                        TempData["Message"] = "Something went wrong try again";
                    }

                }
                else
                {
                    TempData["Message"] = "Please try again";
                }

            }
            catch (DbEntityValidationException ex)
            {
                foreach (var entityValidationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in entityValidationErrors.ValidationErrors)
                    {
                        ErrorHandling.LogFileWrite(ex.Message, "Property: " + validationError.PropertyName, System.Reflection.MethodBase.GetCurrentMethod().Name, " Error: " + validationError.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public string GenerateRandomString(int length)
        {
            string chars = string.Empty;

            chars += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

        #region Private Method

        //private bool IsVerifiedEmail(string email, out string message, out bool isvaliduser)
        private bool IsUserVerified(string username, out string message, out bool isvaliduser)
        {
            message = string.Empty;
            isvaliduser = false;
            try
            {

                isvaliduser = true;

                var objVerification = UserManager.FindByName(username);
                if (objVerification != null)
                {

                    if (objVerification.isAuthorized && !objVerification.IsEmailVerified)
                    {

                        message = "Email not verified";
                        return false;
                    }
                    if (objVerification.isAuthorized && objVerification.IsEmailVerified)
                    {
                        return true;
                    }
                    else
                    {
                        message = "Email not verified";
                        return false;
                    }
                }
                else
                {
                    message = "Email is not registered";
                    return false;
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return false;
        }

        private bool IsVerifiedEmailFromLogin(string username, out string message, out bool isvaliduser)
        {
            message = string.Empty;
            isvaliduser = false;
            try
            {

                isvaliduser = true;

                var objVerification = UserManager.FindByName(username);
                if (objVerification != null)
                {

                    if (objVerification.IsStaffAccountVerified && !objVerification.IsEmailVerified)
                    {

                        message = "Email not verified";
                        return false;
                    }
                    if (objVerification.IsStaffAccountVerified && objVerification.IsEmailVerified)
                    {
                        if (objVerification.IsStaff)
                        {
                            //if (DateTime.UtcNow < objVerification.EmailVerificationTokenCreatedDate.AddHours(ConfigHelper.GetTokentValidityPeriod((int)TokentValidityPeriod_Enum.PasswordResetTokenValidityHours)))
                            //{
                            //    return true;
                            //}
                            //else
                            //{
                            //    return false;
                            //}

                            return true;
                        }
                        else
                        {
                            message = "Invalid Staff Login";
                            return false;
                        }
                    }
                    else
                    {
                        message = "Email not verified";
                        return false;
                    }
                }
                else
                {
                    message = "Invalid UserName";
                    return false;
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return false;
        }

        #endregion
    }

    public class CaptchaResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("error-codes")]
        public List<string> ErrorCodes { get; set; }
    }

}
