﻿using Microsoft.AspNet.Identity;
using SwaranAdmin_New.DataAccess;
using SwaranAdmin_New.Models;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SwaranAdmin_New.Controllers
{
    [Authorize]
    public class CustomerFeedbackController : Controller
    {
        CustomerFeedbackDal _feedBack = CustomerFeedbackDal.Instance;
        // GET: CustomerFeedback
        [HttpGet]
        public ActionResult FeedbackMainView()
        {
            return View();
        }

        public ActionResult _FeedbackView(int numberOfRecord, int currentpage)
        {
            var feedbacks = _feedBack.LoadFeedbacks(numberOfRecord, currentpage);
            return View(feedbacks);
        }
        [HttpGet]
        public ActionResult SpecialPreferencesView(long feedbackid)
        {//SpecialPreferencesViewModel
            var pickupNote = _feedBack.GetPickupNote(feedbackid);
            pickupNote.Feedbackid = feedbackid;
            return View(pickupNote);
        }
        [HttpPost]
        public ActionResult SpecialPreferencesView(SpecialPreferencesViewModel model)
        {
            model.StaffId = User.Identity.GetUserId();
            var pickupNote = _feedBack.UpdateSpecialPreferences(model);
            if (pickupNote)
            {

                return View("FeedbackMainView");
            }
            return View();
        }
        [HttpGet]
        public ActionResult CustomerMailView(string emailEddress, string userName, long feedbackid)
        {
            var customerEmail = new CustomerEmailViewModel();
            customerEmail.ToAddress = emailEddress;
            customerEmail.CustomerName = userName;
            customerEmail.Feedbackid = feedbackid;
            var emailTeplets = _feedBack.GetCustomerEmailTeplets();
            ViewBag.emailTeplets = emailTeplets;
            return View(customerEmail);
        }

        [HttpPost]
        public async Task<ActionResult> CustomerMailView(CustomerEmailViewModel model)
        {
            var emailTeplets = _feedBack.GetCustomerEmailTeplets();
            ViewBag.emailTeplets = emailTeplets;

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            model.StaffId = User.Identity.GetUserId();
            var message = await _feedBack.SendCustomerMail(model);
            if (message.IsSuccess)
            {
                ModelState.Clear();
                TempData["message"] = message.Message;
                return View("FeedbackMainView");
            }

            return View(model);
        }

        public ActionResult DisplayCustomerMailView(long feedbackid)
        {
            var customerEmail = _feedBack.GetCustomerMail(feedbackid);

            return View(customerEmail);
        }
        [HttpGet]
        public ActionResult StaffMailView(long feedbackid)
        {
            var customerEmail = new StaffEmailViewModel();
            customerEmail.Feedbackid = feedbackid;
            var allstaff = _feedBack.GetAllStaffEmail();
            ViewBag.allstaff = allstaff;
            return View(customerEmail);
        }

        [HttpPost]
        public async Task<ActionResult> StaffMailView(StaffEmailViewModel model)
        {
            var allstaff = _feedBack.GetAllStaffEmail();
            ViewBag.allstaff = allstaff;

            if (!ModelState.IsValid)
            {
                return View(model);
            }
            model.StaffId = User.Identity.GetUserId();
            var message = await _feedBack.SendStaffMail(model);
            if (message.IsSuccess)
            {
                ModelState.Clear();
                TempData["message"] = message.Message;
                return View("FeedbackMainView");
            }

            return View(model);
        }

        public ActionResult DisplayStaffMailView(long feedbackid)
        {
            var staffEmail = _feedBack.GetStaffMail(feedbackid);

            return View(staffEmail);
        }
        [HttpGet]
        public ActionResult CustomerReviewView(long feedbackid)
        {

            var customerEmail = new CustomerReviewViewModel();
            customerEmail.FeedbackId = feedbackid;

            return View(customerEmail);
        }

        [HttpPost]
        public ActionResult CustomerReviewView(CustomerReviewViewModel model)
        {

            bool isSucces = _feedBack.SaveCustomerReview(model);
            if (isSucces)
            {
                return View("FeedbackMainView");
            }

            return View(model);
        }
        [HttpGet]
        public ActionResult SystemComplaintView(long feedbackid)
        {

            var customerEmail = new SystemComplaintViewModel();
            customerEmail.FeedBackId = feedbackid;

            return View(customerEmail);
        }

        [HttpPost]
        public ActionResult SystemComplaintView(SystemComplaintViewModel model)
        {
            model.CreatedBy = User.Identity.GetUserId();
            bool isSucces = _feedBack.SaveSystemComplaint(model);
            if (isSucces)
            {
                return View("FeedbackMainView");
            }

            return View(model);
        }
        public ActionResult DisplaySystemComplaintView(long feedbackid)
        {
            var systemComplint = _feedBack.GetSystemComplaintView(feedbackid);

            return View(systemComplint);
        }


        public ActionResult DisplayDriverComplaintView(long feedbackid)
        {
            var driverComplaint = _feedBack.GetDriverComplaint(feedbackid);

            return View(driverComplaint);
        }


        [HttpGet]
        public ActionResult DriverComplaintView(long feedbackid)
        {
            var driverTitles = _feedBack.GetDriverTitleTeplets();
            ViewBag.driverTitles = driverTitles;
            var driverNames = _feedBack.GetDriverNames();
            ViewBag.driverNames = driverNames;


            var customerEmail = new DriverComplaintViewModel();
            customerEmail.FeedBackId = feedbackid;
            return View(customerEmail);
        }

        [HttpPost]
        public async Task<ActionResult> DriverComplaintView(DriverComplaintViewModel model)
        {
            var driverNames = _feedBack.GetDriverNames();
            ViewBag.driverNames = driverNames;
            var driverTitles = _feedBack.GetDriverTitleTeplets();
            ViewBag.driverTitles = driverTitles;
            model.CreatedBy = User.Identity.GetUserId();
            var feedback = await _feedBack.SaveDriverComplaint(model);
            if (feedback.IsSuccess)
            {
                return View("FeedbackMainView");
            }

            return View(model);
        }
        [HttpGet]
        public ActionResult DriverAppreciationView(long feedbackid)
        {
            var driverNames = _feedBack.GetDriverNames();
            ViewBag.driverNames = driverNames;
            var driverTitles = _feedBack.GetDriverAppreciationTitleTeplets();
            ViewBag.driverTitles = driverTitles;
            var customerEmail = new DriverAppreciationViewModel();
            customerEmail.FeedBackId = feedbackid;
            return View(customerEmail);
        }

        [HttpPost]
        public async Task<ActionResult> DriverAppreciationView(DriverAppreciationViewModel model)
        {
            var driverNames = _feedBack.GetDriverNames();
            ViewBag.driverNames = driverNames;
            var driverTitles = _feedBack.GetDriverAppreciationTitleTeplets();
            ViewBag.driverTitles = driverTitles;
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            model.CreatedBy = User.Identity.GetUserId();
            var feedback = await _feedBack.SaveDriverAppreciation(model);
            if (feedback.IsSuccess)
            {
                return View("FeedbackMainView");
            }
            return View(model);
        }
        public ActionResult DisplayDriverAppreciationView(long feedbackid)
        {
            var driverComplaint = _feedBack.GetDriverComplaint(feedbackid);

            return View(driverComplaint);
        }

        [HttpGet]
        public ActionResult ReviewSiteConfig()
        {
            var allOrganizationReviewSite = _feedBack.GetAllOrganizationReviewSites();
            ViewBag.Message = TempData["SuccessMessage"] != null ? TempData["SuccessMessage"] : "";

            return View(allOrganizationReviewSite);
        }

        [HttpGet]
        public ActionResult EditReviewSite(long organizationReviewSiteId)
        {
            var relevantOrganizationReviewSite = _feedBack.GetRelevantOrganizationReviewSite(organizationReviewSiteId);

            return View(relevantOrganizationReviewSite);
        }

        //[HttpPost]
        //public ActionResult EditReviewSite(long organizationReviewSiteId)
        //{
        //    var relevantOrganizationReviewSite = _feedBack.GetRelevantOrganizationReviewSite(organizationReviewSiteId);

        //    return View(relevantOrganizationReviewSite);
        //}

        [HttpPost]
        public ActionResult EditReviewSite(OrganizationReviewSiteDetail model)
        {
            bool isUpdateSuccess = _feedBack.UpdateRelevantOrganizationReviewSite(model);

            if (isUpdateSuccess)
            {
                TempData["SuccessMessage"] = "The review site has been updated successfully.";
                //return RedirectToAction("EditRate", new { rateId = model.RateId });
                return RedirectToAction("ReviewSiteConfig");
            }
            else
            {
                return View(model);
            }
        }
    }
}