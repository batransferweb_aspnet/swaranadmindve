﻿using Microsoft.AspNet.Identity;
using SwaranAdmin_New.DataAccess;
using SwaranAdmin_New.Models;
using System.Web.Mvc;

namespace SwaranAdmin_New.Controllers
{
    [Authorize]
    public class WorldPayPaymentController : Controller
    {
        private WorldpayDal worldpay = WorldpayDal.Instance;
        public ActionResult CardPaymentView()
        {
            var worldPay = new WorldPayOnlineBookingViewmodel();
            return View(worldPay);
        }
        [Authorize(Users = "krishnananthan.v@gmail.com,uwusri@gmail.com,Brunila,agi,siv@batransfer.com")]
        public ActionResult RepaymentView()
        {

            return View();
        }
        public ActionResult PayWorldpayOnline(string tokent)
        {
            var userid = User.Identity.GetUserId();
            //  string tokent = "AJ9n1V+55Q/SMUExg77LMffAu8zHc0uUOxVgjaXzKHZ5RjvgvZMaj2vw0I8BzvcJ2g==";
            var url = worldpay.PayOnlineWorldpay(tokent, userid);
            return Redirect(url);
        }
        public ActionResult PayOnlinEditBookinge(string tokent)
        {
            var userid = User.Identity.GetUserId();
            var token = System.Web.HttpContext.Current.Server.UrlDecode(tokent);
            var url = worldpay.PayOnlineWorldpay(token, userid);
            return Redirect(url);
        }
        public ActionResult ManualCardPayment(string msg = "" , bool isError = false)
        {

            ViewBag.Error = isError;

            if (!isError && !string.IsNullOrEmpty(msg))
            {
                ViewBag.Message = "Payment Succeeded. WorldPay Reference : " + msg;
            }
            else if (isError && !string.IsNullOrEmpty(msg))
            {
                ViewBag.Message = "Payment Failed. Plese retry.";
            }            

            return View();
        }

        public ActionResult PayWorldpayOnlineManual(string bookingNumber, decimal amount)
        {
            var userid = User.Identity.GetUserId();
            //  string tokent = "AJ9n1V+55Q/SMUExg77LMffAu8zHc0uUOxVgjaXzKHZ5RjvgvZMaj2vw0I8BzvcJ2g==";
            var url = worldpay.PayOnlineWorldpayManual(bookingNumber, amount, userid);
            return Redirect(url);
        }
    }
}