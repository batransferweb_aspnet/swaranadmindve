﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace SwaranAdmin_New.Controllers
{
    [Authorize]
    public class UploadElController : Controller
    {
        // GET: UploadEl
        public ActionResult Upload()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Upload(FormCollection formCollection)
        {
            if (Request != null)
            {
                HttpPostedFileBase file = Request.Files["UploadedFile"];
                if ((file != null) && (file.ContentLength > 0) && !string.IsNullOrEmpty(file.FileName))
                {
                    string fileName = file.FileName;
                    string fileContentType = file.ContentType;
                    byte[] fileBytes = new byte[file.ContentLength];
                    var data = file.InputStream.Read(fileBytes, 0, Convert.ToInt32(file.ContentLength));
                    var usersList = new List<Users>();
                    //using (var package = new ExcelPackage(file.InputStream))
                    //{
                    //    var currentSheet = package.Workbook.Worksheets;
                    //    var workSheet = currentSheet.First();
                    //    var noOfCol = workSheet.Dimension.End.Column;
                    //    var noOfRow = workSheet.Dimension.End.Row;

                    //    for (int rowIterator = 2; rowIterator <= noOfRow; rowIterator++)
                    //    {
                    //        var user = new Users();
                    //        user.FirstName = workSheet.Cells[rowIterator, 1].Value.ToString();
                    //        user.LastName = workSheet.Cells[rowIterator, 2].Value.ToString();
                    //        usersList.Add(user);
                    //    }
                    //}
                }
            }
            return View("Index");
        }
    }

    public class Users { public string FirstName { get; set; } public string LastName { get; set; } }
}