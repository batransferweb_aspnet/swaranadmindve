﻿using SwaranAdmin_New.DataAccess.DriverDal;
using SwaranAdmin_New.Models.DriverMo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SwaranAdmin_New.Controllers.DriverCOL
{
    [Authorize]
    public class DriverApplicationController : Controller
    {
        DriverApplicationDal rexDriverApp = DriverApplicationDal.Instance;
        // GET: DriverApplication
        public ActionResult DriverJobView() 
        {
            return View();
        }

        public ActionResult CurrentDriverJobView(long driverappid) 
        { 
            var app= rexDriverApp.GetCurrentDriverJob(driverappid);
            return View(app);
        }

        public ActionResult CurrentDriverJobPrintView(long driverappid) 
        {
            var app = rexDriverApp.GetCurrentDriverJob(driverappid);
            return View(app);
        }


        public ActionResult  DriverJobsPrintView(int status)
        {
            var allapplication = rexDriverApp.GetDriverJobjs(status); 
            return View(allapplication);
        }


    }
}