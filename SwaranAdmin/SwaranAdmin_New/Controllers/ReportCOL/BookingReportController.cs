﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SwaranAdmin_New.Controllers.ReportCOL
{
    [Authorize(Users = "krishnananthan.v@gmail.com,uwusri@gmail.com,agi,siv@batransfer.com")]
    public class BookingReportController : Controller
    {
        // GET: BookingReport
        public ActionResult MonthlyBookingView() 
        {
            return View();
        }
        public ActionResult MonthlyBookingSiteView() 
        {
            return View();
        }

        public ActionResult MonthlyBookingPaymenttypeView() 
        {
            return View();
        }

        public ActionResult MonthlyBookingPlotView() 
        {
            return View();
        }

        public ActionResult BookingSearchView() 
        {
            return View();
        }

        public ActionResult TotalCreatedBookingView() 
        {
            return View();
        }
        public ActionResult TotalCreatedBookingBySiteView() 
        {
            return View();
        }
    }
}