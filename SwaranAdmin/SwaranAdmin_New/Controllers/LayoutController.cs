﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataAccess;
using System.Web.Mvc;

namespace SwaranAdmin_New.Controllers
{
    [Authorize]
    public class LayoutController : Controller
    {
        private ReservationDal _reservation = ReservationDal.Instance;
        public ActionResult _WaitingBookingView()
        {
            var waitingBookings = _reservation.GetResrvationByStatus((int)BookingStatusEnum.Requested);
            return PartialView(waitingBookings);
        }
    }
}