﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SwaranAdmin_New.DataAccess.Company;
using SwaranAdmin_New.Models.Company;

namespace SwaranAdmin_New.Controllers
{
    public class CompanyController : Controller
    {
        private CompanyProfileDal company = CompanyProfileDal.Instance;
        // GET: Company
        [HttpGet]
        public ActionResult AddCompanyUserView()
        {
            ViewBag.isSavedMesage = false;
            ViewBag.IsSaved = false;
            ViewBag.Message = "";
            return View();
        }

        [HttpPost]
        public ActionResult AddCompanyUserView(CompanyUserViewModel model)
        {
           
            if (!ModelState.IsValid)
            {
                ViewBag.isSavedMesage = false;
                ViewBag.IsSaved = false;
                ViewBag.Message = "";
                return View(model);
            }
           model.RateFormula = "PRICE" + model.RateFormula;
          var saveed=    company.SaveCompanyUser(model);
            ViewBag.isSavedMesage = true;
            ViewBag.IsSaved = saveed.IsSaved;
            ViewBag.Message = saveed.Message;
            return View();
        }

        [HttpGet]
        public ActionResult  ListCompanyUserView() 
        {
      
            return View();
        }


        [HttpGet]
        public ActionResult AddContractReferanceView() 
        {
            ViewBag.isSavedMesage = false;
            ViewBag.IsSaved = false;
            ViewBag.Message = "";
            return View();
        }


        [HttpPost]
        public ActionResult AddContractReferanceView(ContractReferanceViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.isSavedMesage = false;
                ViewBag.IsSaved = false;
                ViewBag.Message = "";
                return View(model);
            } 
           var saveed = company.SaveContractReference(model);
            ViewBag.isSavedMesage = true;
            ViewBag.IsSaved = saveed.IsSaved;
            ViewBag.Message = saveed.Message;
            return View();
        }

        [HttpGet]
        public ActionResult CompanyProfileView() 
        { 
            return View();
        }

        [HttpGet]
        public ActionResult EditeCompanyProfileView(long companyid)
        {
            var objCompany = company.GetCompany(companyid);
            ViewBag.isSavedMesage = false;
            ViewBag.IsSaved = false;
            ViewBag.Message = "";
            return View(objCompany); 
        }

        [HttpPost]
        public ActionResult EditeCompanyProfileView(CompanyViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.isSavedMesage = false;
                ViewBag.IsSaved = false;
                ViewBag.Message = "";
                return View(model);
            }
            var saveed = company.UpdateCompanyProfile(model);
            ViewBag.isSavedMesage = true;
            ViewBag.IsSaved = saveed.IsSaved;
            ViewBag.Message = saveed.Message;
            return View();
        }
    }
}