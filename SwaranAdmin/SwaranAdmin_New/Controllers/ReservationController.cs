﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataAccess;
using SwaranAdmin_New.Models;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SwaranAdmin_New.Controllers
{
    [Authorize]
    public class ReservationController : Controller
    {
        private ReservationDal reservation = ReservationDal.Instance;
        [HttpGet]
        public ActionResult SearchReservationView()
        {
            return View();
        }
        [HttpGet]
        public ActionResult BookingStatusView()
        {
            return View();
        }
        [HttpGet]
        public ActionResult ReservationByAreaView()
        {
            return View();
        }
        [HttpGet]
        public ActionResult MonthlyBookingView()
        {
            return View();
        }
        [HttpGet]
        public ActionResult HourlyBookingView()
        {
            return View();
        }



        [HttpGet]
        public ActionResult EditBooking(string bookingToken, bool isReturnBooking)
        {

            var currentBooking = reservation.GetEditBooking(bookingToken, isReturnBooking);
            currentBooking.IsReturnBooking = isReturnBooking;
            return View(currentBooking);
        }

        [HttpPost] 
        [ValidateAntiForgeryToken]
        public ActionResult EditBooking(CurrentBookingDisplayViewModel model)
        {
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GostView(CurrentBookingDisplayViewModel model)
        {

            if (model.PaymentMethodId == 100)
            {
                if (string.IsNullOrEmpty(model.WorldPayReference) || string.IsNullOrEmpty(model.WorldPayReference) || (!string.IsNullOrEmpty(model.WorldPayReference) && model.WorldPayReference.Length == 36))
                {
                    return RedirectToAction("EditBooking", "Reservation", new { bookingToken = model.BookingToken, isReturnBooking = model.IsReturnBooking });
                }
            }

            BookingSummaryDom summery = reservation.ChangePayment(model);

            if (model.PaymentMethodId == (int)PaymentTypeEnum.Card)
            {
                return Redirect(summery.RedirectPaymentUrl);
               
            }
            if (model.PaymentMethodId == (int)PaymentTypeEnum.Pay_Booking_Fee)
            {
                return Redirect(summery.RedirectPaymentUrl);

            }
            return View(summery);
        }

        public ActionResult MailCustomerView(string email)
        {
            var model = new MailCustomeViewModel();
            model.ToAddress = email;
            return View(model);
        }


        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> MailCustomerView(MailCustomeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var attachmet = new EmailAttachmentsViewMOdel();
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {

                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/App_Data/"), fileName);
                    file.SaveAs(path);

                    byte[] bytes = System.IO.File.ReadAllBytes(path);
                    attachmet.FileNames = file.FileName;
                    attachmet.Attachments = bytes;
                    string[] sizes = { "B", "KB", "MB", "GB", "TB" };
                    double len = new FileInfo(path).Length;
                    int order = 0;

                    var fileSize = (len / 1024) / 1024;

                    if (fileSize >= 32)
                    {
                        TempData["filesize"] = "invalid File Size";
                        return View(model);
                    }
                    //while (len >= 1024 && order < sizes.Length - 1)
                    //{
                    //    order++;
                    //    len = len / 1024;
                    //}
                    //string result = String.Format("{0:0.##} {1}", len, sizes[order]);
                }
            }

            var message = await reservation.SendQuickEmail(model, attachmet);
            if (message.IsSuccess)
            {
                ModelState.Clear();
                TempData["message"] = message.Message;
            }
            return View();
        }
        public ActionResult MailAdminView()
        {
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> MailAdminView(MailAdminViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var attachmet = new EmailAttachmentsViewMOdel();
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {

                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/App_Data/"), fileName);
                    file.SaveAs(path);

                    byte[] bytes = System.IO.File.ReadAllBytes(path);
                    attachmet.FileNames = file.FileName;
                    attachmet.Attachments = bytes;
                    string[] sizes = { "B", "KB", "MB", "GB", "TB" };
                    double len = new FileInfo(path).Length;
                    int order = 0;

                    var fileSize = (len / 1024) / 1024;

                    if (fileSize >= 32)
                    {
                        TempData["filesize"] = "invalid File Size";
                        return View(model);
                    }
                    //while (len >= 1024 && order < sizes.Length - 1)
                    //{
                    //    order++;
                    //    len = len / 1024;
                    //}
                    //string result = String.Format("{0:0.##} {1}", len, sizes[order]);
                }
            }

            var message = await reservation.SendQuickAdminEmail(model, attachmet);
            if (message.IsSuccess)
            {
                ModelState.Clear();
                TempData["message"] = message.Message;
            }
            return View();
        }

        [HttpGet]
        public ActionResult VehicleUpgradeView()
        {
            
            return View();

        }

        [AllowAnonymous]
        public ActionResult PendingBookingView()
        {
          var pendingBookings = reservation.GetPendingBookings();
            return View(pendingBookings);
        }



        [AllowAnonymous]
        public ActionResult CompletedView(long id)
        {
             reservation.UpdateCompletedBookings(id);
            var pendingBookings = reservation.GetPendingBookings();
            return RedirectToAction("PendingBookingView", pendingBookings);
        }


        [AllowAnonymous]
        public ActionResult CancelView(long id)
        {
              reservation.UpdateCancelBookings(id);
            var pendingBookings = reservation.GetPendingBookings();
            return RedirectToAction("PendingBookingView", pendingBookings);
        }
    }
}