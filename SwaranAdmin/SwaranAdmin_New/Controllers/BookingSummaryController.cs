﻿using SwaranAdmin_New.Autocab;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SwaranAdmin_New.Autocab.Base;

namespace SwaranAdmin_New.Controllers
{
    [Authorize]
    public class BookingSummaryController : Controller
    {
        // GET: BookingSummary
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Details(string tkn, string err, string msg)//, int prepyttype)
        {
            string loyaltyType = string.Empty;
            try
            {


                Booking relevantBookingReturn = null;
                using (var db = new BATransferEntities())
                {
                     
                    if (db.Bookings.Any(bt => bt.BookingToken.Equals(tkn)))
                    { 
                        var relevantBooking = db.Bookings.First(bt => bt.BookingToken.Equals(tkn));

                        var worldpayOrdercode = relevantBooking.BookingPayments.First(x => x.PaymentTypeId == (int)PaymentTypeEnum.Card).BookingPaymentCards.First().OrderCode;
                        SendEmail(relevantBooking, db);

                        String noImage =
                        "http://ba-dock.org/abcd/I//2016927114658679e9003c3a-6a80-4887-9bf3-1d30f94fae65d60OXPN36utYWQsCY0Q2.png";
                        String noProfile =
                            "http://ba-dock.org/abcd/I//201692711461617f5c36775-c287-4c9b-b598-b51cad94050f32ylyQ4CqTuObPsdiJiQ.png";

                        //var vehicleCombination = (BookingVehicletypeCombinations)Session["vehicleCombination"];
                        //var returnJourney = (ReturnJourney)Session["ReturnJourney"];

                        //var model = (ReservationModel)Session["Reservetion"];
                        var summary = new BookingSummaryDom();
                        summary.BookingStatus = relevantBooking.Status;
                        summary.BookingNumber = relevantBooking.BookingNumber;
                        summary.BookingSource = relevantBooking.BookingSource;
                        summary.BookingDateTime = ValueParseHelper.ToStringDateTime(relevantBooking.BookingDateTime);
                        summary.FromFullAddress = relevantBooking.BookingFrom;
                        summary.Viapoints = relevantBooking.JourneyViapoints != null
                                            ? string.Join(", ", relevantBooking.JourneyViapoints.Select(x => x.ViaFullAddress)) : "";
                        summary.ToFullAddress = relevantBooking.BookingTo;

                        summary.VehicleTypesName = relevantBooking.BookingVehicleTypes != null
                                                ? string.Join(", ", relevantBooking.BookingVehicleTypes.Select(x => x.VehicleType.VehicleTypeName))
                                                : "";
                        summary.VehicleTypesNameUrl = relevantBooking.MapUrl;
                        summary.PaymentType = Enum.GetName(typeof(PaymentTypeEnum), relevantBooking.PaymentMethod);


                        summary.NumberOfPassanger = relevantBooking.NumberOfPassengers;
                        summary.NumberOfLuggages = relevantBooking.Luggage;
                        summary.NumberHandOfLuggages = relevantBooking.HandLuggage;
                        summary.PaymentStatus = relevantBooking.PaymentStatus;

                        summary.OneWayCredit = relevantBooking.ConsumerDebit;
                        summary.OneWayPrice = relevantBooking.TotalDistancePrice;
                        summary.OtherCharges = relevantBooking.OtherCharges;
                        summary.TotalDistancePrice = relevantBooking.TotalDistancePrice + relevantBooking.OtherCharges;
                        if (relevantBooking.IsSourceBooking)
                        {
                            relevantBookingReturn = db.Bookings.First(bt => bt.BookingId == relevantBooking.ReferenceBookingId && bt.IsReturnBooking);
                            summary.BookingStatusReturnJourney = relevantBookingReturn.Status;
                            summary.IsReturnJourney = true;
                            summary.ReturnFromFullAddress = relevantBookingReturn.BookingFrom;
                            summary.ReturnViapoints = relevantBookingReturn.JourneyViapoints != null
                                            ? string.Join(", ", relevantBookingReturn.JourneyViapoints.Select(x => x.ViaFullAddress)) : ""; ;
                            summary.ReturnToFullAddress = relevantBookingReturn.BookingTo;

                            summary.ReturnWayCredit = relevantBookingReturn.ConsumerDebit;
                            summary.ReturnWayPrice = relevantBookingReturn.TotalDistancePrice;
                            summary.TotalDistancePrice = relevantBookingReturn.TotalDistancePrice + relevantBooking.TotalDistancePrice + (relevantBooking.ConsumerDebit + relevantBookingReturn.ConsumerDebit);
                            summary.ReturnBookingDateTime = ValueParseHelper.ToStringDateTime(relevantBookingReturn.BookingDateTime);

                        }


                        bool isErrorOccurred = false;

                        switch (err.Trim())
                        {
                            case "1":
                                isErrorOccurred = true;
                                break;
                            default:
                                isErrorOccurred = false;
                                break;
                        }

                        ViewBag.Error = isErrorOccurred;

                        string bookingSource = Enum.GetName(typeof(BookingSource_Enum), summary.BookingSource);

                        string description = "Booking/" + bookingSource + "/" + summary.BookingNumber;
                        string encodeDescription = System.Web.HttpContext.Current.Server.UrlEncode(description);
                        string encodeToken = System.Web.HttpContext.Current.Server.UrlEncode(tkn);
                        string callBackUrl = Server.UrlEncode(Request.Url.Host + ":44367" + "/BookingSummary/Details?tkn=" + encodeToken);
                        //string callBackUrl = Server.UrlEncode(Request.Url.Host + "/BookingSummary/Details?tkn=" + tkn);


                        if (!isErrorOccurred)
                        {
                            //var reservetion = (ReservationModel)Session["Reservetion"];
                            //var address = (AddressInfoViewModel)Session["booking"];

                            //var returnJourney = (ReturnJourney)Session["ReturnJourney"];

                            #region Autocab 
                            bool isSetPrice = bool.Parse(ConfigurationManager.AppSettings["IsSetPrice"]);
                            AutoCabAgentViewModel config = AutoCabHelper.GetAutoCabAgentConfig();
                            if (db.Bookings.Any(x => x.BookingId == relevantBooking.BookingId && string.IsNullOrEmpty(x.Autocab_BookingReference) && x.Status != (int)BookingStatusEnum.Cancelled))
                            {

                                if (!string.IsNullOrEmpty(relevantBooking.UserId))
                                {
                                    var customer = db.Customers.FirstOrDefault(x => x.userId.Equals(relevantBooking.UserId));

                                    int loyaltyTypeId = customer != null ? customer.loyaltyMemberType : 0;

                                    loyaltyType = Enum.GetName((typeof(LoyaltyType_Enum)), loyaltyTypeId);
                                }

                                var bookingAvailability = isSetPrice
                                          ? AutoCapVehicletypeRequest.GetBookingAvailabilityCardMainSetPrice(
                                              relevantBooking,
                                              worldpayOrdercode)
                                          : AutoCapVehicletypeRequest.GetBookingAvailabilityCardMain(relevantBooking,
                                              worldpayOrdercode);

                                var authorizationResponse = AutoCapVehicletypeRequest.BookingAuthorizationRequestMainCard(relevantBooking,
                                    bookingAvailability.AvailabilityReference, 0, loyaltyType, config);

                                if (authorizationResponse.AuthorizationReference != null &&
                                    db.Booking_TEMP.Any(x => x.BookingId == relevantBooking.BookingId) &&
                                    authorizationResponse.AuthorizationReference != null)
                                {
                                    var objBookingTemp =
                                        db.Booking_TEMP.First(x => x.BookingId == relevantBooking.BookingId);
                                    objBookingTemp.Status = (int)BookingStatusEnum.Requested;
                                    objBookingTemp.Autocab_AuthorizationReference =
                                        authorizationResponse.AuthorizationReference;
                                    objBookingTemp.Autocab_BookingReference = authorizationResponse.BookingReference;
                                    objBookingTemp.VendorID = bookingAvailability.Vendor.Id;
                                    objBookingTemp.AutocabNumber =
                                        authorizationResponse.AuthorizationReference.Split('-').Length > 0
                                            ? authorizationResponse.AuthorizationReference.Split('-')[2]
                                            : "";
                                    db.SaveChanges();
                                    summary.OneWayGhostId = objBookingTemp.AutocabNumber;

                                    var objBooking = db.Bookings.First(x => x.BookingId == relevantBooking.BookingId);
                                    objBooking.Status = (int)BookingStatusEnum.Requested;
                                    objBooking.Autocab_AuthorizationReference =
                                        authorizationResponse.AuthorizationReference;
                                    objBooking.Autocab_BookingReference = authorizationResponse.BookingReference;
                                    objBooking.VendorID = bookingAvailability.Vendor.Id;
                                    objBooking.AutocabNumber = authorizationResponse.AuthorizationReference.Split('-')
                                                                   .Length > 0
                                        ? authorizationResponse.AuthorizationReference.Split('-')[2]
                                        : "";
                                    db.SaveChanges();
                                }
                            }


                            if (relevantBooking.JourneyType == (int)JourneyTypeEnum.TwoWay && relevantBookingReturn.Status != (int)BookingStatusEnum.Cancelled)
                            {
                                if (db.Bookings.Any(x => x.BookingId == relevantBookingReturn.BookingId && string.IsNullOrEmpty(x.Autocab_BookingReference)))
                                {
                                  
                                  
                                    var bookingAvailabilityReturn = isSetPrice
                                              ? AutoCapVehicletypeRequest.GetBookingAvailabilityReturnMainCardSetPrice
                                                  (
                                                      relevantBookingReturn, config)
                                              : AutoCapVehicletypeRequest.GetBookingAvailabilityReturnMainCard(
                                                  relevantBookingReturn);


                                    var authorizationResponseReturn =
                                        AutoCapVehicletypeRequest.BookingAuthorizationRequestMainCard(relevantBookingReturn,
                                            bookingAvailabilityReturn.AvailabilityReference, 0, loyaltyType, config);

                                    if (authorizationResponseReturn.AuthorizationReference != null && db.Booking_TEMP.Any(x => x.BookingId == relevantBookingReturn.BookingId))
                                    {
                                        var objBookingTemp = db.Booking_TEMP.First(x => x.BookingId == relevantBookingReturn.BookingId);
                                        objBookingTemp.Status = (int)BookingStatusEnum.Requested;
                                        objBookingTemp.Autocab_AuthorizationReference =
                                            authorizationResponseReturn.AuthorizationReference;
                                        objBookingTemp.Autocab_BookingReference = authorizationResponseReturn.BookingReference;
                                        objBookingTemp.VendorID = bookingAvailabilityReturn.Vendor.Id;
                                        objBookingTemp.AutocabNumber = authorizationResponseReturn.AuthorizationReference.Split('-').Length > 0
                                                            ? authorizationResponseReturn.AuthorizationReference.Split('-')[2]
                                                            : "";
                                        summary.ReturnWayGhostId = objBookingTemp.AutocabNumber;
                                        db.SaveChanges();

                                        var objBooking = db.Bookings.First(x => x.BookingId == relevantBookingReturn.BookingId);
                                        objBooking.Status = (int)BookingStatusEnum.Requested;
                                        objBooking.Autocab_AuthorizationReference =
                                            authorizationResponseReturn.AuthorizationReference;
                                        objBooking.Autocab_BookingReference = authorizationResponseReturn.BookingReference;
                                        objBooking.VendorID = bookingAvailabilityReturn.Vendor.Id;
                                        objBooking.AutocabNumber = authorizationResponseReturn.AuthorizationReference.Split('-').Length > 0
                                                            ? authorizationResponseReturn.AuthorizationReference.Split('-')[2]
                                                            : "";
                                        db.SaveChanges();

                                    }


                                }

                            }



                            #endregion
                        }
                        return View(summary);
                    }
                    else
                    {

                        return RedirectToAction("Index", "Home");
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult CashBookingFeeView(string tkn, string err, string msg)
        {
            string loyaltyType = string.Empty;
            try
            {


                Booking relevantBookingReturn = null;
                using (var db = new BATransferEntities())
                {

                    if (db.Bookings.Any(bt => bt.BookingToken.Equals(tkn)))
                    {
                        var relevantBooking = db.Bookings.First(bt => bt.BookingToken.Equals(tkn));

                        

                        String noImage =
                        "http://ba-dock.org/abcd/I//2016927114658679e9003c3a-6a80-4887-9bf3-1d30f94fae65d60OXPN36utYWQsCY0Q2.png";
                        String noProfile =
                            "http://ba-dock.org/abcd/I//201692711461617f5c36775-c287-4c9b-b598-b51cad94050f32ylyQ4CqTuObPsdiJiQ.png";

                        //var vehicleCombination = (BookingVehicletypeCombinations)Session["vehicleCombination"];
                        //var returnJourney = (ReturnJourney)Session["ReturnJourney"];

                        //var model = (ReservationModel)Session["Reservetion"];
                        var summary = new BookingSummaryDom();
                        summary.BookingStatus = relevantBooking.Status;
                        summary.BookingNumber = relevantBooking.BookingNumber;
                        summary.BookingSource = relevantBooking.BookingSource;
                        summary.BookingDateTime = ValueParseHelper.ToStringDateTime(relevantBooking.BookingDateTime);
                        summary.FromFullAddress = relevantBooking.BookingFrom;
                        summary.Viapoints = relevantBooking.JourneyViapoints != null
                                            ? string.Join(", ", relevantBooking.JourneyViapoints.Select(x => x.ViaFullAddress)) : "";
                        summary.ToFullAddress = relevantBooking.BookingTo;

                        summary.VehicleTypesName = relevantBooking.BookingVehicleTypes != null
                                                ? string.Join(", ", relevantBooking.BookingVehicleTypes.Select(x => x.VehicleType.VehicleTypeName))
                                                : "";
                        summary.VehicleTypesNameUrl = relevantBooking.MapUrl;
                        summary.PaymentType = Enum.GetName(typeof(PaymentTypeEnum), relevantBooking.PaymentMethod);


                        summary.NumberOfPassanger = relevantBooking.NumberOfPassengers;
                        summary.NumberOfLuggages = relevantBooking.Luggage;
                        summary.NumberHandOfLuggages = relevantBooking.HandLuggage;
                        summary.PaymentStatus = relevantBooking.PaymentStatus;

                        summary.OneWayCredit = relevantBooking.ConsumerDebit;
                        summary.OneWayPrice = relevantBooking.TotalDistancePrice;
                        summary.OtherCharges = relevantBooking.OtherCharges;
                        summary.TotalDistancePrice = relevantBooking.TotalDistancePrice + relevantBooking.OtherCharges;
                        if (relevantBooking.IsSourceBooking)
                        {
                            relevantBookingReturn = db.Bookings.First(bt => bt.BookingId == relevantBooking.ReferenceBookingId && bt.IsReturnBooking);
                            summary.BookingStatusReturnJourney = relevantBookingReturn.Status;
                            summary.IsReturnJourney = true;
                            summary.ReturnFromFullAddress = relevantBookingReturn.BookingFrom;
                            summary.ReturnViapoints = relevantBookingReturn.JourneyViapoints != null
                                            ? string.Join(", ", relevantBookingReturn.JourneyViapoints.Select(x => x.ViaFullAddress)) : ""; ;
                            summary.ReturnToFullAddress = relevantBookingReturn.BookingTo;

                            summary.ReturnWayCredit = relevantBookingReturn.ConsumerDebit;
                            summary.ReturnWayPrice = relevantBookingReturn.TotalDistancePrice;
                            summary.TotalDistancePrice = relevantBookingReturn.TotalDistancePrice + relevantBooking.TotalDistancePrice + (relevantBooking.ConsumerDebit + relevantBookingReturn.ConsumerDebit);
                            summary.ReturnBookingDateTime = ValueParseHelper.ToStringDateTime(relevantBookingReturn.BookingDateTime);

                        }


                        bool isErrorOccurred = false;

                        switch (err.Trim())
                        {
                            case "1":
                                isErrorOccurred = true;
                                break;
                            default:
                                isErrorOccurred = false;
                                break;
                        }

                        ViewBag.Error = isErrorOccurred;

                        string bookingSource = Enum.GetName(typeof(BookingSource_Enum), summary.BookingSource);

                        string description = "Booking/" + bookingSource + "/" + summary.BookingNumber;
                        string encodeDescription = System.Web.HttpContext.Current.Server.UrlEncode(description);
                        string encodeToken = System.Web.HttpContext.Current.Server.UrlEncode(tkn);
                        string callBackUrl = Server.UrlEncode(Request.Url.Host + ":44367" + "/BookingSummary/Details?tkn=" + encodeToken);
                        //string callBackUrl = Server.UrlEncode(Request.Url.Host + "/BookingSummary/Details?tkn=" + tkn);

                        if (!isErrorOccurred)
                        {
                            SendEmailBookingFee(relevantBooking, db);
                        }
                        
                        return View(summary);
                    }
                   
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return RedirectToAction("Index", "Home");

        }


        [HttpGet] 
        public ActionResult VehicleUpgradeView(string tkn, int isRetrunBooking, string err, string msg)//, int prepyttype) 
        {
            try
            {
                decimal totalAmount = 0;
                decimal totalOnewayPrice = 0;
                Booking relevantBookingReturn = null;
                using (var db = new BATransferEntities())
                {

                    string worldpayOrdercode = string.Empty;

                    var relevantBooking = db.Bookings.First(bt => bt.BookingToken.Equals(tkn) && bt.IsReturnBooking == (isRetrunBooking == 1));
                    if (relevantBooking != null)
                    {
                        String noImage =
                        "https://ba-dock.org/abcd/I//2016927114658679e9003c3a-6a80-4887-9bf3-1d30f94fae65d60OXPN36utYWQsCY0Q2.png";
                        String noProfile =
                            "https://ba-dock.org/abcd/I//201692711461617f5c36775-c287-4c9b-b598-b51cad94050f32ylyQ4CqTuObPsdiJiQ.png";

                        //var vehicleCombination = (BookingVehicletypeCombinations)Session["vehicleCombination"];
                        //var returnJourney = (ReturnJourney)Session["ReturnJourney"];

                        //var model = (ReservationModel)Session["Reservetion"];
                        var summary = new BookingSummaryDom();
                        summary.BookingNumber = relevantBooking.BookingNumber;
                        summary.BookingStatus = relevantBooking.Status;
                        summary.BookingSource = relevantBooking.BookingSource;
                        summary.BookingDateTime = ValueParseHelper.ToStringDateTime(relevantBooking.BookingDateTime);
                        summary.FromFullAddress = relevantBooking.BookingFrom;
                        summary.Viapoints = relevantBooking.JourneyViapoints != null
                                            ? string.Join(", ", relevantBooking.JourneyViapoints.Select(x => x.ViaFullAddress)) : "";
                        summary.ToFullAddress = relevantBooking.BookingTo;

                        summary.VehicleTypesName = relevantBooking.BookingVehicleTypes != null
                                                ? string.Join(", ", relevantBooking.BookingVehicleTypes.Select(x => x.VehicleType.VehicleTypeName))
                                                : "";
                        summary.VehicleTypesNameUrl = relevantBooking.MapUrl;
                        summary.PaymentType = Enum.GetName(typeof(PaymentTypeEnum), relevantBooking.PaymentMethod);


                        summary.NumberOfPassanger = relevantBooking.NumberOfPassengers;
                        summary.NumberOfLuggages = relevantBooking.Luggage;
                        summary.NumberHandOfLuggages = relevantBooking.HandLuggage;
                        summary.PaymentStatus = relevantBooking.PaymentStatus;
                        summary.BookingFee = relevantBooking.OtherCharges;
                        if (relevantBooking.Status != (int)BookingStatusEnum.Cancelled)
                        {
                            summary.OneWayCredit = relevantBooking.ConsumerDebit;
                            summary.OneWayPrice = relevantBooking.TotalDistancePrice;
                            summary.FinalPrice = relevantBooking.FinalPrice;// relevantBooking.TotalDistancePrice + relevantBooking.OtherCharges;
                            totalOnewayPrice = relevantBooking.FinalPrice;
                            totalAmount = relevantBooking.FinalPrice;
                        }



                        bool isErrorOccurred = false;

                        switch (err.Trim())
                        {
                            case "1":
                                isErrorOccurred = true;
                                break;
                            default:
                                isErrorOccurred = false;
                                break;
                        }

                        ViewBag.Error = isErrorOccurred;

                        string bookingSource = Enum.GetName(typeof(BookingSource_Enum), summary.BookingSource);

                        string description = "VehicleUpgrade/" + bookingSource + "/" + summary.BookingNumber;
                        string encodeDescription = System.Web.HttpContext.Current.Server.UrlEncode(description);
                        string encodeToken = System.Web.HttpContext.Current.Server.UrlEncode(tkn);
                        string path = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                        string callBackUrl = Server.UrlEncode(path + "/BookingSummary/VehicleUpgradeView?tkn=" + encodeToken + "&isRetrunBooking=" + isRetrunBooking);

                        ViewBag.Message = "Your payment has been failed.";
                        ViewBag.Token = encodeToken;
                        ViewBag.CallBackUrl = callBackUrl;


                        string bookingFeeUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.PaymentApi) +
                                          "Payment/CaptureBookingFee" + "?amnt=" + relevantBooking.VehicleUpgradeAmt + "&desc=" +
                                          encodeDescription + "&bnu=" +
                                          System.Web.HttpContext.Current.Server.UrlEncode(summary.BookingNumber) +
                                          "&cbu=" + callBackUrl;
                        ViewBag.PaymentUrl = bookingFeeUrl;
                        if (!isErrorOccurred)
                        {
                            SendEmail(relevantBooking, db);
                            bool isSendToGhost = OrganizationHelper.IsSendToGhostFromSource(relevantBooking.BookingSource);

                            if (isSendToGhost)
                            {
                                var config = AutoCabHelper.GetAutoCabAgentConfig();
                                if (config != null)
                                {
                                    #region Autocab 

                                    //if (relevantBooking.PaymentMethod == (int)PaymentTypeEnum.Card)

                                    //{
                                    //    string loyaltyType = string.Empty;
                                    //    if (!string.IsNullOrEmpty(relevantBooking.UserId))
                                    //    {
                                    //        var customer =
                                    //            db.Customers.FirstOrDefault(x => x.userId.Equals(relevantBooking.UserId));

                                    //        int loyaltyTypeId = customer != null ? customer.loyaltyMemberType : 0;

                                    //        loyaltyType = Enum.GetName((typeof(LoyaltyType_Enum)), loyaltyTypeId) + " Customer";
                                    //    }
                                    //    bool isSetPrice = bool.Parse(ConfigurationManager.AppSettings["IsSetPrice"]);

                                    //    if (string.IsNullOrEmpty(relevantBooking.Autocab_AuthorizationReference) &&
                                    //        string.IsNullOrEmpty(relevantBooking.Autocab_BookingReference) &&
                                    //        relevantBooking.Status != (int)BookingStatusEnum.Cancelled)
                                    //    {




                                    //        var bookingAvailability = isSetPrice
                                    //            ? AutoCapVehicletypeRequest.GetBookingAvailabilityCardMainSetPrice(
                                    //                relevantBooking,
                                    //                worldpayOrdercode, config)
                                    //            : AutoCapVehicletypeRequest.GetBookingAvailabilityCardMain(relevantBooking,
                                    //                worldpayOrdercode, config);
                                    //        // bookingAvailability.AvailablityReference
                                    //        var authorizationResponse =
                                    //            AutoCapVehicletypeRequest.BookingAuthorizationRequestMainCard(
                                    //                relevantBooking,
                                    //                bookingAvailability.AvailabilityReference, 0, loyaltyType, config);

                                    //        if (db.Booking_TEMP != null &&
                                    //            db.Booking_TEMP.Any(x => x.BookingId == relevantBooking.BookingId) &&
                                    //            authorizationResponse.AuthorizationReference != null)
                                    //        {
                                    //            var objBooking =
                                    //                db.Booking_TEMP.First(x => x.BookingId == relevantBooking.BookingId);
                                    //            objBooking.Status = (int)BookingStatusEnum.Requested;
                                    //            objBooking.Autocab_AuthorizationReference =
                                    //                authorizationResponse.AuthorizationReference;
                                    //            objBooking.Autocab_BookingReference = authorizationResponse.BookingReference;
                                    //            objBooking.VendorID = bookingAvailability.Vendor.Id;
                                    //            objBooking.AutocabNumber = authorizationResponse.AuthorizationReference !=
                                    //                                       null &&
                                    //                                       authorizationResponse.AuthorizationReference
                                    //                                           .Split('-')
                                    //                                           .Length > 0
                                    //                ? authorizationResponse.AuthorizationReference.Split('-')[2]
                                    //                : "";
                                    //            db.SaveChanges();

                                    //        }

                                    //        if (db.Bookings != null &&
                                    //            db.Bookings.Any(x => x.BookingId == relevantBooking.BookingId) &&
                                    //            authorizationResponse.AuthorizationReference != null)
                                    //        {
                                    //            var objBooking =
                                    //                db.Bookings.First(x => x.BookingId == relevantBooking.BookingId);
                                    //            objBooking.Status = (int)BookingStatusEnum.Requested;
                                    //            objBooking.Autocab_AuthorizationReference =
                                    //                authorizationResponse.AuthorizationReference;
                                    //            objBooking.Autocab_BookingReference = authorizationResponse.BookingReference;
                                    //            objBooking.VendorID = bookingAvailability.Vendor.Id;
                                    //            objBooking.AutocabNumber = authorizationResponse.AuthorizationReference !=
                                    //                                       null &&
                                    //                                       authorizationResponse.AuthorizationReference
                                    //                                           .Split('-')
                                    //                                           .Length > 0
                                    //                ? authorizationResponse.AuthorizationReference.Split('-')[2]
                                    //                : "";
                                    //            db.SaveChanges();
                                    //        }
                                    //    }

                                    //    if (relevantBooking.JourneyType == (int)JourneyTypeEnum.TwoWay)
                                    //    {
                                    //        if (
                                    //            string.IsNullOrEmpty(relevantBookingReturn.Autocab_AuthorizationReference) &&
                                    //            string.IsNullOrEmpty(relevantBookingReturn.Autocab_BookingReference) &&
                                    //            relevantBookingReturn.Status != (int)BookingStatusEnum.Cancelled)
                                    //        {

                                    //            var bookingAvailabilityReturn = isSetPrice
                                    //                ? AutoCapVehicletypeRequest.GetBookingAvailabilityReturnMainCardSetPrice
                                    //                    (
                                    //                        relevantBookingReturn, config)
                                    //                : AutoCapVehicletypeRequest.GetBookingAvailabilityReturnMainCard(
                                    //                    relevantBookingReturn, config);

                                    //            var authorizationResponseReturn =
                                    //                AutoCapVehicletypeRequest.BookingAuthorizationRequestMainCard(
                                    //                    relevantBookingReturn,
                                    //                    bookingAvailabilityReturn.AvailabilityReference, 0, loyaltyType, config);

                                    //            if (db.Booking_TEMP != null &&
                                    //                db.Booking_TEMP.Any(x => x.BookingId == relevantBookingReturn.BookingId) &&
                                    //                authorizationResponseReturn.AuthorizationReference != null)
                                    //            {
                                    //                var objBooking =
                                    //                    db.Booking_TEMP.First(
                                    //                        x => x.BookingId == relevantBookingReturn.BookingId);
                                    //                objBooking.Status = (int)BookingStatusEnum.Requested;
                                    //                objBooking.Autocab_AuthorizationReference =
                                    //                    authorizationResponseReturn.AuthorizationReference;
                                    //                objBooking.Autocab_BookingReference =
                                    //                    authorizationResponseReturn.BookingReference;
                                    //                objBooking.VendorID = bookingAvailabilityReturn.Vendor.Id;
                                    //                objBooking.AutocabNumber =
                                    //                    authorizationResponseReturn.AuthorizationReference != null &&
                                    //                    authorizationResponseReturn.AuthorizationReference.Split('-').Length >
                                    //                    0
                                    //                        ? authorizationResponseReturn.AuthorizationReference.Split('-')[
                                    //                            2]
                                    //                        : "";
                                    //                db.SaveChanges();

                                    //            }

                                    //            if (db.Bookings != null &&
                                    //                db.Bookings.Any(x => x.BookingId == relevantBookingReturn.BookingId) &&
                                    //                authorizationResponseReturn.AuthorizationReference != null)
                                    //            {
                                    //                var objBooking =
                                    //                    db.Bookings.First(
                                    //                        x => x.BookingId == relevantBookingReturn.BookingId);
                                    //                objBooking.Autocab_AuthorizationReference =
                                    //                    authorizationResponseReturn.AuthorizationReference;
                                    //                objBooking.Status = (int)BookingStatusEnum.Requested;
                                    //                objBooking.Autocab_BookingReference =
                                    //                    authorizationResponseReturn.BookingReference;
                                    //                objBooking.VendorID = bookingAvailabilityReturn.Vendor.Id;
                                    //                objBooking.AutocabNumber =
                                    //                    authorizationResponseReturn.AuthorizationReference != null &&
                                    //                    authorizationResponseReturn.AuthorizationReference.Split('-').Length >
                                    //                    0
                                    //                        ? authorizationResponseReturn.AuthorizationReference.Split('-')[
                                    //                            2]
                                    //                        : "";
                                    //                db.SaveChanges();
                                    //            }
                                    //        }
                                    //    }
                                    //}


                                    #endregion
                                }
                            }
                        }
                        else
                        {
                            //RepaymentSendEmail(relevantBooking, db, paymentUrl);

                        }


                        return View(summary);
                    }
                    else
                    {

                        return RedirectToAction("Index", "Home");
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return RedirectToAction("Index", "Home");
        }


        public void SendEmail(Booking booking, BATransferEntities db)
        {
            decimal finalAmt = 0;

            try
            {

                EmailCredentialsViewModel emailCredentials = new EmailCredentialsViewModel();

                int bookingSiteId = OrganizationHelper.GetRelevantOrganizationFromSite(booking.BookingSource);
                string logoUrl = string.Empty;
                switch (bookingSiteId)
                {
                    case (int)BookingSiteEnum.BA:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBA);
                        }
                        break;
                    case (int)BookingSiteEnum.AO:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.AO);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailAO);
                        }
                        break;
                    case (int)BookingSiteEnum.ET:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.ET);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailET);
                        }
                        break;
                    case (int)BookingSiteEnum.WT:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.WT);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailWT);
                        }
                        break;
                    case (int)BookingSiteEnum.BAT:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BAT);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAT);
                        }
                        break;
                    case (int)BookingSiteEnum.BAL:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BAL);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAL);
                        }
                        break;
                    case (int)BookingSiteEnum.BAC:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BAC);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBA);
                        }
                        break;
                    default:
                        break;
                }

                StringBuilder builder = new StringBuilder();
                string FileCardPayment = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/CardPayment.txt"));
                string cardPaymenttext = System.IO.File.ReadAllText(FileCardPayment);

                finalAmt = booking.BookingPayments.First(x => x.BookingId == booking.BookingId).PaymentAmount;
                if (booking.IsSourceBooking)
                {
                    var returnBooking =
                         db.Bookings.First(bt => bt.BookingId == booking.ReferenceBookingId && bt.IsReturnBooking);
                    finalAmt = finalAmt + returnBooking.BookingPayments.First(x => x.BookingId == booking.ReferenceBookingId).PaymentAmount;
                }

                cardPaymenttext = cardPaymenttext.Replace("LOGO-URL", logoUrl);
                cardPaymenttext = cardPaymenttext.Replace("BOOKINGNUMBER", booking.BookingNumber);
                cardPaymenttext = cardPaymenttext.Replace("PAYMENTSTATUS", Enum.GetName(typeof(PaymentStatusEnum), booking.PaymentStatus));
                cardPaymenttext = cardPaymenttext.Replace("AMOUNT", finalAmt.ToString());
                //cardPaymenttext = cardPaymenttext.Replace("NUMBEROFLUGGAHES", booking.NumberOfLuggages.ToString());
                //cardPaymenttext = cardPaymenttext.Replace("NUMBEROFHANDLUGGAHES", booking.NumberOfHandLuggages.ToString());
                //cardPaymenttext = cardPaymenttext.Replace("BOOKINGAMOUNT", (booking.OtherCharges + booking.JourneyAmount).ToString());

                builder.Append(cardPaymenttext);

                var listEmails = new List<string>();
                if (booking.IsSendPassengerEmail)
                {
                    listEmails.Add(booking.PassengerEmail);
                    listEmails.Add(booking.BookedPersonEmail);
                }
                else
                {

                    listEmails.Add(booking.BookedPersonEmail);
                }



                string _emailSubject = ConfigHelper.GetPaymentStatusSubject(booking.PaymentStatus);

                if (!string.IsNullOrEmpty(_emailSubject))
                {
                    //Srikanth, Your Reservation Status at batransfer.com - Ref: BA479068
                    _emailSubject = _emailSubject.Replace("{0}", booking.BookedPersonName);
                    _emailSubject = _emailSubject.Replace("{1}", booking.BookingNumber + " is " + Enum.GetName(typeof(PaymentStatusEnum), booking.PaymentStatus));
                }


                var email = new RootEmail();
                email.Attachments = null;
                email.Body = builder.ToString();
                email.FromEmailAddress = emailCredentials.FromEmail;
                email.Username = emailCredentials.Username;
                email.Password = emailCredentials.Password;
                email.Port = emailCredentials.Port;
                email.Smtp = emailCredentials.Smtp;

                email.Subject = _emailSubject;
                email.ToEmailAddresses = listEmails;
                email.DisplayName = emailCredentials.DisplayName;
                email.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                EmailHelper.SendEmailWithoutAttachtment(email, apiUrl);
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }


        public void SendEmailBookingFee(Booking booking, BATransferEntities db) 
        {
            decimal bookingFee = 0;

            try
            {

                EmailCredentialsViewModel emailCredentials = new EmailCredentialsViewModel();

                int bookingSiteId = OrganizationHelper.GetRelevantOrganizationFromSite(booking.BookingSource);
                string logoUrl = string.Empty;
                switch (bookingSiteId)
                {
                    case (int)BookingSiteEnum.BA:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBA);
                        }
                        break;
                    case (int)BookingSiteEnum.AO:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.AO);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailAO);
                        }
                        break;
                    case (int)BookingSiteEnum.ET:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.ET);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailET);
                        }
                        break;
                    case (int)BookingSiteEnum.WT:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.WT);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailWT);
                        }
                        break;
                    case (int)BookingSiteEnum.BAT:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BAT);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAT);
                        }
                        break;
                    case (int)BookingSiteEnum.BAL:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BAL);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAL);
                        }
                        break;
                    case (int)BookingSiteEnum.BAC:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BAC);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBA);
                        }
                        break;
                    default:
                        break;
                }

                StringBuilder builder = new StringBuilder();
                string FileCardPayment = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/CardPayment.txt"));
                string cardPaymenttext = System.IO.File.ReadAllText(FileCardPayment);

                bookingFee = booking.OtherCharges;

                cardPaymenttext = cardPaymenttext.Replace("LOGO-URL", logoUrl);
                cardPaymenttext = cardPaymenttext.Replace("BOOKINGNUMBER", booking.BookingNumber);
                cardPaymenttext = cardPaymenttext.Replace("PAYMENTSTATUS", Enum.GetName(typeof(PaymentStatusEnum), booking.PaymentStatus));
                cardPaymenttext = cardPaymenttext.Replace("AMOUNT", bookingFee.ToString());
                //cardPaymenttext = cardPaymenttext.Replace("NUMBEROFLUGGAHES", booking.NumberOfLuggages.ToString());
                //cardPaymenttext = cardPaymenttext.Replace("NUMBEROFHANDLUGGAHES", booking.NumberOfHandLuggages.ToString());
                //cardPaymenttext = cardPaymenttext.Replace("BOOKINGAMOUNT", (booking.OtherCharges + booking.JourneyAmount).ToString());

                builder.Append(cardPaymenttext);

                var listEmails = new List<string>();
                if (booking.IsSendPassengerEmail)
                {
                    listEmails.Add(booking.PassengerEmail);
                    listEmails.Add(booking.BookedPersonEmail);
                }
                else
                {

                    listEmails.Add(booking.BookedPersonEmail);
                }



                string _emailSubject = ConfigHelper.GetPaymentStatusSubject(booking.PaymentStatus);

                if (!string.IsNullOrEmpty(_emailSubject))
                {
                    //Srikanth, Your Reservation Status at batransfer.com - Ref: BA479068
                    _emailSubject = _emailSubject.Replace("{0}", booking.BookedPersonName);
                    _emailSubject = _emailSubject.Replace("{1}", booking.BookingNumber + " is " + Enum.GetName(typeof(PaymentStatusEnum), booking.PaymentStatus));
                }


                var email = new RootEmail();
                email.Attachments = null;
                email.Body = builder.ToString();
                email.FromEmailAddress = emailCredentials.FromEmail;
                email.Username = emailCredentials.Username;
                email.Password = emailCredentials.Password;
                email.Port = emailCredentials.Port;
                email.Smtp = emailCredentials.Smtp;

                email.Subject = _emailSubject;
                email.ToEmailAddresses = listEmails;
                email.DisplayName = emailCredentials.DisplayName;
                email.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                EmailHelper.SendEmailWithoutAttachtment(email, apiUrl);
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }

    }
}