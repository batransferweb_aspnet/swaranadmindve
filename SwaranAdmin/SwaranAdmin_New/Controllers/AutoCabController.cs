﻿using SwaranAdmin_New.DataAccess;
using SwaranAdmin_New.Models;
using System.Web.Mvc;

namespace SwaranAdmin_New.Controllers
{
    [Authorize]
    public class AutoCabController : Controller
    {
        private AutoCabDal _rexAutoCab = AutoCabDal.Instance;
        [HttpGet]
        public ActionResult AutoCabVehicletypeConfigView()
        {
            ViewBag.Message = "";
            ViewBag.Vehicletype = _rexAutoCab.GetAllVehicleType();
            return View();
        }

        [HttpPost]
        public ActionResult AutoCabVehicletypeConfigView(AutoCabVehicletypeConfigViewModel model)
        {
            ViewBag.Vehicletype = _rexAutoCab.GetAllVehicleType();
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            ViewBag.Message = _rexAutoCab.UpdateAutocabVehicletypes(model);
            return View();
        }
    }
}