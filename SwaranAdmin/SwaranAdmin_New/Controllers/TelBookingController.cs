﻿using SwaranAdmin_New.DataManager;
using System.Web.Mvc;
using SwaranAdmin_New.DataAccess.TeleBooking;

namespace SwaranAdmin_New.Controllers
{
    [Authorize]
    public class TelBookingController : Controller
    {
        private TelebookingDal _repositiry = TelebookingDal.Instance;
        // GET: TelBooking
        public ActionResult TelBookingView()
        {
            var listLuggage = _repositiry.GetNumberOfLuggageList(10);
            ViewBag.listLuggage = listLuggage;

            var luggageHandList = _repositiry.GetNumberOfHandLuggageList(10);
            ViewBag.listHandLuggage = luggageHandList;
            return View();
        }
    }
}