﻿using SwaranAdmin_New.Autocab;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System.Web.Mvc;
using System.Linq;
using System;

namespace SwaranAdmin_New.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        GhostDal ghost = GhostDal.Instance;
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult CustomerBookingsView(int bookingstatus,string userid,string memberId) 
        {
            ViewData["UserId"] = userid;// "0b381c57-9dff-46a6-9e62-747a944fa7fc";
            ViewData["BookingStatus"] = bookingstatus;
            ViewData["MemberId"] = memberId;
            return View();
        }

        [HttpGet]
        public ActionResult AdditionalDetails(string uid)
        {
            StaffAdditionalDetailsModel model = new StaffAdditionalDetailsModel();
            model.Id = uid;
            return View(model);
        }

        [HttpPost]
        public ActionResult AdditionalDetails(StaffAdditionalDetailsModel model)
        {

            if (ModelState.IsValid)
            {

                try
                {

                    using (var db = new BATransferEntities())
                    {

                        var relevantStaffProfile = db.StaffProfiles.FirstOrDefault(sp => sp.UserId == model.Id);

                        if(relevantStaffProfile != null)
                        {
                            relevantStaffProfile.sysModifiedDate = DateTime.UtcNow;
                            relevantStaffProfile.postalCode = !string.IsNullOrEmpty(model.PostalCode) ? model.PostalCode : string.Empty;
                            relevantStaffProfile.street = !string.IsNullOrEmpty(model.Street) ? model.Street : string.Empty;
                            relevantStaffProfile.fax = !string.IsNullOrEmpty(model.Fax) ? model.Fax : string.Empty;
                            relevantStaffProfile.town = !string.IsNullOrEmpty(model.Town) ? model.Town : string.Empty;
                            relevantStaffProfile.countryID = model.CountryId;
                            relevantStaffProfile.PaymentMethod = model.PaymentMethod;
                            relevantStaffProfile.Workbase = model.Workbase;
                            relevantStaffProfile.PayslipFrequencyId = model.PayslipFrequencyId;
                            relevantStaffProfile.PayRateAccountant = model.PayRateAccountant;
                            relevantStaffProfile.PayRateDirect = model.PayRateDirect;
                            relevantStaffProfile.PayOption = model.PayOption;
                            relevantStaffProfile.MaxHoursAccountant = model.MaxHoursAccountant;
                            relevantStaffProfile.Pension = model.Pension;
                            relevantStaffProfile.CompanyWorkingFor = model.CompanyWorkingFor;
                            relevantStaffProfile.IsOldUser = true;

                            db.SaveChanges();
                        }
                        else
                        {
                            StaffProfile newStaffProfile = new StaffProfile()
                            {
                                UserId = model.Id,
                                IsEnabled = true,
                                IsDeleted = false,
                                sysCreatedDate = DateTime.UtcNow,
                                sysModifiedDate = DateTime.UtcNow,
                                postalCode = !string.IsNullOrEmpty(model.PostalCode) ? model.PostalCode : string.Empty,
                                street = !string.IsNullOrEmpty(model.Street) ? model.Street : string.Empty,
                                fax = !string.IsNullOrEmpty(model.Fax) ? model.Fax : string.Empty,
                                town = !string.IsNullOrEmpty(model.Town) ? model.Town : string.Empty,
                                countryID = model.CountryId,
                                PaymentMethod = model.PaymentMethod,
                                Workbase = model.Workbase,
                                PayslipFrequencyId = model.PayslipFrequencyId,
                                PayRateAccountant = model.PayRateAccountant,
                                PayRateDirect = model.PayRateDirect,
                                PayOption = model.PayOption,
                                MaxHoursAccountant = model.MaxHoursAccountant,
                                Pension = model.Pension,
                                CompanyWorkingFor = model.CompanyWorkingFor,
                                ImageUrl = string.Empty,
                                IsOldUser = true
                            };

                            db.StaffProfiles.Add(newStaffProfile);
                            db.SaveChanges();
                        }

                        return RedirectToAction("Index", "Home");
                        //return RedirectToAction("TelBookingView", "TelBooking");
                    }
                }
                catch (Exception exception)
                {
                    ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
                    ModelState.AddModelError("", "User update failed try again.");
                    return View(model);
                }

            }
            else
            {
                return View(model);
            }

        }
    }
}
