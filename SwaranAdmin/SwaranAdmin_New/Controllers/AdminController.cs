﻿using System.Web.Mvc;
using SwaranAdmin_New.DataAccess;
using SwaranAdmin_New.Models;
using SwaranAdmin_New.Models.Member;

namespace SwaranAdmin_New.Controllers
{
    [Authorize(Users = "krishnananthan.v@gmail.com,uwusri@gmail.com,agi,siv@batransfer.com,puvanarajan@gmail.com")]
    public class AdminController : Controller
    {
        private AdminDal repoAdmin = AdminDal.Instance;
        [HttpGet]
        public ActionResult RefundHistoryView()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ApproveStaff()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ManageStaff()
        {
            return View();
        }

        [HttpGet]
        public ActionResult TotalBookingView() 
        {
            return View();
        }

        [HttpGet]
        public ActionResult JobHistoryView() 
        {
            return View();
        }

        [HttpGet]
        public ActionResult  ViapointConfigView()
        {
         var config=   repoAdmin.GetViapointConfig();
            return View(config);
        }

        [HttpPost]
        public ActionResult ViapointConfigView(ViapintConfigViewModel config)
        {
            if (!ModelState.IsValid)
            {
                return View(config);
            }
            string msg= repoAdmin.SaveViapointConfig(config);
            ViewBag.Message = msg;
            return View(config);
        }

        [HttpGet]
        public ActionResult PushNotificationConfiguration()
        {
            //var config = repoAdmin.GetPushNotificationConfiguration();
            //return View(config);

            if(TempData["Message"] != null)
            {
                ViewBag.Message = TempData["Message"];
            }

            return View();
        }

        [HttpGet]
        public ActionResult EditPushNotificationConfiguration(int pushNotificationTypeId)
        {
            var config = repoAdmin.GetPushNotificationConfigurationById(pushNotificationTypeId);
            //return View(config);
            return View(config);
        }

        [HttpPost]
        public ActionResult EditPushNotificationConfiguration(PushNotificationConfigModel config)
        {
            if (!ModelState.IsValid)
            {
                return View(config);
            }

            bool isSuccess = false;
            string msg = repoAdmin.SavePushNotificationConfiguration(config, out isSuccess);            

            if(isSuccess)
            {
                TempData["Message"] = msg;
                return RedirectToAction("PushNotificationConfiguration");
            }
            else
            {
                ViewBag.Message = msg;
                return View(config);
            }
           
        }
		
		[HttpGet]
        public ActionResult CustomerSaveCardView() 
        {
         
            return View();
        }

        [HttpGet]
        public ActionResult BannerTxtView() 
        {
            ViewBag.isSavedMesage = false;
            ViewBag.IsSaved = false;
            ViewBag.Message = "";
            return View();
        }

        [HttpPost]
        public ActionResult BannerTxtView(BanarTextViewModel model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.isSavedMesage = false;
                ViewBag.IsSaved = false;
                ViewBag.Message = "";
                return View(model);
            }
           var saveed= repoAdmin.UpdateBannerText(model) ;
            ViewBag.isSavedMesage = true;
            ViewBag.IsSaved = saveed.IsSaved;
            ViewBag.Message = saveed.Message;
            return View(model);
        }
		    }
}
