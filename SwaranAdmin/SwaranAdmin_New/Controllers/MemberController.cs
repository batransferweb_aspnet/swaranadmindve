﻿using Microsoft.AspNet.Identity;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataAccess;
using SwaranAdmin_New.Models.Member;
using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SwaranAdmin_New.Controllers
{
    [Authorize]
    public class MemberController : Controller
    {
        private MemberDal member = MemberDal.Instance;
        public ActionResult MemberSearch()
        {


            return View();
        }

        public ActionResult LoyaltyMemberSearch()
        {
            return View();
        }
        [Authorize(Users = "krishnananthan.v@gmail.com,uwusri@gmail.com,agi,Arane,thanan,sushma")]
        [HttpGet]
        public ActionResult AddFreeJourney()
        {
            ViewBag.isSavedMesage = false;
            ViewBag.UserID = "";
            return View();
        }
        [HttpPost]
        public ActionResult AddFreeJourney(CustomerFreeJourneyViewModel model)
        {
            ViewBag.isSavedMesage = false;
            ViewBag.UserID = model.UserId;
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.OperatorId = User.Identity.GetUserId();
            model.Code = member.GenerateRandomString(9);

            ViewBag.isSaved = member.CreateFreeJourney(model);
            if (ViewBag.isSaved)
            {
                ModelState.Clear();
            }
            return View();
        }
        [HttpGet]
        public ActionResult SendQuickEmail()
        {

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> SendQuickEmail(SendQuickEmailViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var attachmet = new EmailAttachmentsViewMOdel();
            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {

                    var fileName = Path.GetFileName(file.FileName);
                    var path = Path.Combine(Server.MapPath("~/App_Data/"), fileName);
                    file.SaveAs(path);

                    byte[] bytes = System.IO.File.ReadAllBytes(path);
                    attachmet.FileNames = file.FileName;
                    attachmet.Attachments = bytes;
                    string[] sizes = { "B", "KB", "MB", "GB", "TB" };
                    double len = new FileInfo(path).Length;
                    int order = 0;

                    var fileSize = (len / 1024) / 1024;

                    if (fileSize >= 32)
                    {
                        TempData["filesize"] = "invalid File Size";
                        return View(model);
                    }
                    //while (len >= 1024 && order < sizes.Length - 1)
                    //{
                    //    order++;
                    //    len = len / 1024;
                    //}
                    //string result = String.Format("{0:0.##} {1}", len, sizes[order]);
                }
            }

            var message = await member.SendQuickEmail(model, attachmet);
            if (message.IsSuccess)
            {
                ModelState.Clear();
                TempData["message"] = message.Message;
            }
            return View();
        }

        [HttpGet]
        public ActionResult AddMemberDiscount()
        {
            ViewBag.isSavedMesage = false;
            ViewBag.UserID = "";
            return View();
        }
        [HttpPost]
        public ActionResult AddMemberDiscount(string submit, CustomerDiscountViewModel model)
        {
            ViewBag.isSavedMesage = false;
            ViewBag.UserID = model.CustomerUserId;
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            model.StaffUserId = User.Identity.GetUserId();
            if (submit.Equals("Add"))
            {
                ViewBag.isSaved = member.CreateCustomerDiscount(model);
            }
            if (submit.Equals("Deduct"))
            {
                ViewBag.isSaved = member.DeductCustomerDiscount(model);
            }

            if (ViewBag.isSaved)
            {
                model = new CustomerDiscountViewModel();
                ModelState.Clear();
            }
            return View(model);
        }


        [HttpGet]
        public ActionResult MemberStatmentView() 
        {

            return View();
        }


        [HttpGet]
        public ActionResult TellFriendView()
        {

            return View();
        }

        [HttpGet]
        public ActionResult AddMemberSpecialPreferencesView()
        {

            return View();
        }

        [HttpPost]
        public ActionResult AddMemberSpecialPreferencesView(MemberSpecialPreferenceViewModel model)
        {

            if (!ModelState.IsValid)
            {
                ViewBag.isSaved = false;
                return View(model);
            }
            model.StaffId = User.Identity.GetUserId();
            ViewBag.isSaved = member.AddMemberSpecialPreferences(model);
            if (ViewBag.isSaved)
            {
                ModelState.Clear();
            }
            return View();
        }

        [HttpGet]
        public ActionResult TestimonialView() 
        {

            return View();
        }


      
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Receipt(string bookingToken)
        {

            string bookingNumber = string.Empty;
            var memoryStream = member.GetReceipt(bookingToken, out bookingNumber);
            string handle = Guid.NewGuid().ToString();
            if (memoryStream != null)
                TempData[handle] = memoryStream.ToArray();

            return new JsonResult()
            {
                Data = new { FileGuid = handle, FileName = bookingNumber + ".pdf" }
            };

        }


        [HttpGet]
        public ActionResult Download(string fileGuid, string fileName)
        {
            if (TempData[fileGuid] != null)
            {
                byte[] data = TempData[fileGuid] as byte[];
                return File(data, "application/pdf", fileName);
            }
            else
            {
                // Problem - Log the error, generate a blank file,
                //           redirect to another controller action - whatever fits with your application
                return new EmptyResult();
            }
        }

        public ActionResult OldBookings(long memberId)
        {
             

            ViewBag.OldBookingUrl = ConfigurationManager.AppSettings["OldBookingUrl"] + "memberId=" + memberId;

            return View();
        }

        [HttpGet]
        public ActionResult BlockMemberView()
        {
            ViewBag.isSavedMesage = false;
            return View();
        }
        [HttpPost]
        public ActionResult BlockMemberView(BlockMemberBookingViewModel model,string submit)
        {
            ViewBag.isSavedMesage = false;
            ViewBag.isSaved = true;
            ViewBag.UserID = model.UserId;
            if (submit.EndsWith("Unblock This Member"))
            {
                ModelState.Clear();
                member.UnblockMember(model);
                return View();
            }
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            ViewBag.isSavedMesage = true;
            ViewBag.isSaved = member.SaveBlockmember(model);
            if (ViewBag.isSaved)
            {
                ModelState.Clear();
            }
            return View();
        }

        [HttpGet]
        public ActionResult PendingActivationView()
        {
           var listMember= member.GetPendingMember();
            return View(listMember);
        }

        [HttpGet]
        public ActionResult ActiveView(string Id) 
        {
            member.Activemember(Id);
            var listMember = member.GetPendingMember();
            return RedirectToAction("PendingActivationView", listMember);
        }
        [HttpGet]
        public ActionResult CancelView(string Id) 
        {
            member.DeleteActivation(Id);
            var listMember = member.GetPendingMember();
            return RedirectToAction("PendingActivationView", listMember);
        }
       
    }
}