﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SwaranAdmin_New.Models;

namespace SwaranAdmin_New.Controllers
{
    public class LowEmissionZoneController : Controller
    {
        // GET: LowEmissionZone
        public ActionResult EmissionZoneView()
        {
            var lowEmmition = new LowEmissionZoneViewModel();
            return View(lowEmmition);
        }
        [HttpGet]
        public ActionResult AllEmissionZoneView() 
        { 
            return View();
        }
    }
}