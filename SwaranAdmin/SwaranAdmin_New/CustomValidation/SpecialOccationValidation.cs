﻿
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SwaranAdmin_New.CustomValidation
{
     

    public sealed class SpecialOccationValidationAttribute : ValidationAttribute
    {
        public string OccasionsIdProperty { get; set; } 
         
        public string CustomerRateIdProperty { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
              
                string occasionsId = HttpContext.Current.Request[OccasionsIdProperty];  
                string rateid =HttpContext.Current.Request[CustomerRateIdProperty];
                string daterange = value.ToString();
                string fdate = daterange.Split('-')[0].Trim();
                string tdate = daterange.Split('-')[1].Trim();
                var fromDatetime = ValueParseHelper.TryParseDateTime(fdate);
                var toDatetime = ValueParseHelper.TryParseDateTime(tdate);
                using (var db = new BATransferEntities())
                {
                    var objSpecialOcc = db.sp_swaran_Check_specialoccations(fromDatetime, long.Parse(rateid), long.Parse(occasionsId)).FirstOrDefault();
                    if(objSpecialOcc != null)
                    {
                        return new ValidationResult(objSpecialOcc.Title+ " already exists");
                    }

                    var objSpecialOccTo = db.sp_swaran_Check_specialoccations(toDatetime, long.Parse(rateid), long.Parse(occasionsId)).FirstOrDefault();
                    if (objSpecialOccTo != null)
                    {
                        return new ValidationResult(objSpecialOccTo.Title + " already exists");
                    }
                }


                return ValidationResult.Success;


            }
            catch (Exception ex)
            {

                return new ValidationResult("Invalid date range");
            }

        }

    }
}