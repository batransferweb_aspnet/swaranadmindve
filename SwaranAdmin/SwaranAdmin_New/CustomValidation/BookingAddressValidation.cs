﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.Models;

namespace SwaranAdmin_New.CustomValidation
{

    public class BookingAddressValidation
    {
    }

    public class TermsAndConditionsAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return value is bool && (bool) value;
        }
    }

    public class CheckBoxAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return value is bool && (bool) value;
        }
    }

    public sealed class BookingDateTimeCustomValidationAttribute : ValidationAttribute
    {
        public string BookingDateProperty { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                int miniMinitues = ConfigHelper.GetBookingRequestConfiguration(
                    (int) BookingRequestConfiguration_Enum.BookingToJourneyMinutesInterval);
                // Get Value of the DateStart property
                string bookingDate = HttpContext.Current.Request[BookingDateProperty];
                DateTime bookingDatetime = DateTime.Parse(bookingDate + " " + value);

                if (bookingDatetime.Date == DateTime.Now.Date)
                {
                    if (BritishTime.GetDateTime().AddMinutes(miniMinitues) < bookingDatetime)
                        return ValidationResult.Success;
                    else
                        return
                            new ValidationResult("If booking within " + miniMinitues +
                                                 "mins then please call the office ");

                }
                else
                {
                    return ValidationResult.Success;
                }
            }
            catch (Exception)
            {

                return new ValidationResult("Invalid Time");
            }

        }
    }

    public sealed class LandingDateTimeValidationAttribute : ValidationAttribute
    {

        public string LandingDateProperty { get; set; }

        public override bool IsValid(object value)
        {
            try
            {
                DateTime landingDateTime = DateTime.Now;
                string landingDate = HttpContext.Current.Request[LandingDateProperty];
                if (value != null && DateTime.TryParse(landingDate + " " + value, out landingDateTime))
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
            catch (Exception ex)
            {

                return false;

            }

        }
    }

    public sealed class BookingDateCustomValidationAttribute : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                DateTime bookingDatetime = DateTime.Parse(value.ToString());

                // Meeting start time must be before the end time
                if (bookingDatetime.Date < DateTime.Now.Date)
                    return new ValidationResult("Invalid Date");
                else
                    return ValidationResult.Success;

            }
            catch (Exception)
            {

                return new ValidationResult("Invalid Date");
            }

        }
    }

    public sealed class BookingFromAddressValidationAttribute : ValidationAttribute
    {
        public string ToPostcodeProperty { get; set; }

        public string FromPostcodeProperty { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                string postCodeRegex =
                    "(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})";
                //string addressRegex = "^[a-zA-Z0-9\\s/g_.-]*$";
                string addressRegex = "^[a-zA'&-Z0-9\\s/g\\((.*?)\\)_.-]*$";

                string toPostcode = HttpContext.Current.Request[ToPostcodeProperty];
                string fromPostcode = HttpContext.Current.Request[FromPostcodeProperty];
                string address = value.ToString();

                // Meeting start time must be before the end time

                var matchAddress = Regex.Match(address, addressRegex);

                if (!matchAddress.Success)
                {
                    return new ValidationResult("Invalid Pickup Point");
                }

                var matchPostcode = Regex.Match(fromPostcode, postCodeRegex);

                if (!matchPostcode.Success)
                {
                    return new ValidationResult("Invalid Pickup Point");
                }


                if (toPostcode.Equals(fromPostcode))
                {
                    return new ValidationResult("Cannot enter same pickup and dropoff points");
                }

                return ValidationResult.Success;


            }
            catch (Exception ex)
            {

                return new ValidationResult("Invalid Pickup Point");
            }

        }

    }

    public sealed class FromAreapoitPostcodeValidationAttribute : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                string postCodeRegex =
                    "(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})";

                Regex regexFullpostcode =
                    new Regex(
                        @"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})");
                Regex regexNoSpace =
                    new Regex(
                        @"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})",
                        RegexOptions.IgnorePatternWhitespace);



                if (value != null)
                {
                    string poscode = value.ToString().ToUpper();
                    Match match = regexFullpostcode.Match(poscode);
                    Match matchNoSpace = regexNoSpace.Match(poscode);
                    if (match.Success || matchNoSpace.Success)
                    {
                        return ValidationResult.Success;
                    }
                    else
                    {
                        return new ValidationResult("Invalid postcode");
                    }
                }
                else
                {
                    return new ValidationResult("Invalid postcode");
                }


                return ValidationResult.Success;


            }
            catch (Exception ex)
            {

                return new ValidationResult("Invalid postcode");
            }

        }

    }

    public sealed class ToAreapointPostcodeValidationAttribute : ValidationAttribute
    {

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                string postCodeRegex =
                    "(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})";

                Regex regexFullpostcode =
                    new Regex(
                        @"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})");
                Regex regexNoSpace =
                    new Regex(
                        @"(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})",
                        RegexOptions.IgnorePatternWhitespace);



                if (value != null)
                {
                    string poscode = value.ToString().ToUpper();
                    Match match = regexFullpostcode.Match(poscode);
                    Match matchNoSpace = regexNoSpace.Match(poscode);
                    if (match.Success || matchNoSpace.Success)
                    {
                        return ValidationResult.Success;

                        // string postcode = address.FromPostCode != null
                        //    ? address.FromPostCode.Split(' ')[0]
                        //    :string.Empty;
                        //int numericValue;
                        //if (!int.TryParse(postcode.Substring(postcode.Length - 1), out numericValue))
                        //{
                        //    postcode = postcode.Remove(postcode.Length - 1, 1);
                        //}


                    }
                    else
                    {
                        return new ValidationResult("Invalid postcode");
                    }
                }
                else
                {
                    return new ValidationResult("Invalid postcode");
                }



            }
            catch (Exception ex)
            {

                return new ValidationResult("Invalid postcode");
            }

        }

    }

    public sealed class BookingToAddressValidationAttribute : ValidationAttribute
    {
        public string ToPostcodeProperty { get; set; }

        public string FromPostcodeProperty { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            try
            {
                string postCodeRegex =
                    "(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})";
                //  string addressRegex = "^[a-zA-Z0-9\\s/g_.-]*$";
                // string addressRegex = "^[a-zA-Z0-9\\s/g\\((.*?)\\)]*$";
                string addressRegex = "^[a-zA'&-Z0-9\\s/g\\((.*?)\\)_.-]*$";
                string toPostcode = HttpContext.Current.Request[ToPostcodeProperty];
                string fromPostcode = HttpContext.Current.Request[FromPostcodeProperty];
                string address = value.ToString();



                var matchAddress = Regex.Match(address, addressRegex);

                if (!matchAddress.Success)
                {
                    return new ValidationResult("Invalid Dropoff Point");
                }

                var matchPostcode = Regex.Match(toPostcode, postCodeRegex);

                if (!matchPostcode.Success)
                {
                    return new ValidationResult("Invalid Dropoff Point");
                }


                if (toPostcode.Equals(fromPostcode))
                {
                    return new ValidationResult("Cannot enter same pickup and dropoff points");
                }

                return ValidationResult.Success;


            }
            catch (Exception)
            {

                return new ValidationResult("Invalid Dropoff Point");
            }

        }

    }

    //public sealed class BookingViaAddressValidationAttribute : ValidationAttribute 
    //{
    //    public string Postcode { get; set; }

    //    public string FromPostcodeProperty { get; set; }

    //    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    //    {
    //        try
    //        {
    //            string postCodeRegex = "(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|(([A-Z-[QVX]][0-9][A-HJKPSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY])))) [0-9][A-Z-[CIKMOV]]{2})";
    //            //  string addressRegex = "^[a-zA-Z0-9\\s/g_.-]*$";
    //            // string addressRegex = "^[a-zA-Z0-9\\s/g\\((.*?)\\)]*$";
    //            string addressRegex = "^[a-zA-Z0-9\\s/g\\((.*?)\\)_.-]*$";
    //            string toPostcode = HttpContext.Current.Request[Postcode];
    //            string currentPostcode = HttpContext.Current.Request[FromPostcodeProperty]; 
    //            string address = value.ToString();



    //            var matchAddress = Regex.Match(address, addressRegex);

    //            if (!matchAddress.Success)
    //            {
    //                return new ValidationResult("Invalid via Point");
    //            }

    //            var matchPostcode = Regex.Match(toPostcode, postCodeRegex);

    //            if (!matchPostcode.Success)
    //            {
    //                return new ValidationResult("Invalid via Point");
    //            }


    //            if (toPostcode.Equals(currentPostcode))
    //            {
    //                return new ValidationResult("Cannot enter same address");
    //            }

    //            return ValidationResult.Success;


    //        }
    //        catch (Exception)
    //        {

    //            return new ValidationResult("Invalid via Point");
    //        }

    //    }

    //}

    public class BookingViaAddressValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var list = value as List<ViaPointAddress>;
            if (list == null)
            {

                return true;
            }
            return false;
        }
    }

    public sealed class ReturnFlightLandingDateTimeValidationAttribute : ValidationAttribute
    {
        public string OnewayDateTimeProperty { get; set; }
        public string LandingDateFlightProperty { get; set; }

        public override bool IsValid(object value)
        {
            try
            {

                string onewayDateTime = HttpContext.Current.Request[OnewayDateTimeProperty];
                string bookingDate = HttpContext.Current.Request[LandingDateFlightProperty];
                DateTime oneWayDateTime = DateTime.Parse(onewayDateTime);

                DateTime returnDate = DateTime.Parse(bookingDate + " " + value);
                ;

                // Meeting start time must be before the end time
                return oneWayDateTime < returnDate;
            }
            catch (Exception ex)
            {

                return false;
            }

        }
    }

    public sealed class ReturnCruiseShipLandingDateTimeValidationAttribute : ValidationAttribute
    {
        public string OnewayDateTimeProperty { get; set; }
        public string LandingDateCruiseShipProperty { get; set; }

        public override bool IsValid(object value)
        {
            try
            {

                string onewayDateTime = HttpContext.Current.Request[OnewayDateTimeProperty];
                string bookingDate = HttpContext.Current.Request[LandingDateCruiseShipProperty];
                DateTime oneWayDateTime = DateTime.Parse(onewayDateTime);

                DateTime returnDate = DateTime.Parse(bookingDate + " " + value);
                ;

                // Meeting start time must be before the end time
                return oneWayDateTime < returnDate;
            }
            catch (Exception ex)
            {

                return false;
            }

        }
    }

    public sealed class ReturnDateTimeValidationAttribute : ValidationAttribute
    {
        public string OnewayDateTimeProperty { get; set; }
        public string BookingDateProperty { get; set; }

        public override bool IsValid(object value)
        {
            try
            {

                string onewayDateTime = HttpContext.Current.Request[OnewayDateTimeProperty];
                string bookingDate = HttpContext.Current.Request[BookingDateProperty];
                DateTime oneWayDateTime = DateTime.Parse(onewayDateTime);

                DateTime returnDate = DateTime.Parse(bookingDate + " " + value);
                ;

                // Meeting start time must be before the end time
                return oneWayDateTime < returnDate;
            }
            catch (Exception)
            {

                return false;
            }

        }
    }

    //public sealed class ReturnBookingDateCustomValidationAttribute : ValidationAttribute 
    //{

    //    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    //    {
    //        try
    //        {
    //            DateTime bookingDatetime = DateTime.Parse(value.ToString());

    //            // Meeting start time must be before the end time
    //            if (bookingDatetime.Date < DateTime.Now.Date)
    //                return new ValidationResult("Invalid Date");
    //            else
    //                return ValidationResult.Success;

    //        }
    //        catch (Exception)
    //        {

    //            return new ValidationResult("Invalid Date");
    //        }

    //    }
    //}
    public sealed class OnewayAmountValidationAttribute : ValidationAttribute
    {

        public override bool IsValid(object value)
        {
            try
            {

                return (decimal) value > 0;
            }
            catch (Exception)
            {

                return false;
            }

        }
    }

    public sealed class ReturnwayAmountValidationAttribute : ValidationAttribute
    {

        public override bool IsValid(object value)
        {
            try
            {

                return (decimal) value > 0;
            }
            catch (Exception)
            {

                return false;
            }

        }
    }

}