﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SwaranAdmin_New.DataManager;

namespace SwaranAdmin_New.CustomValidation
{
   
        public sealed class FreeJourneyCodeValidationAttribute : ValidationAttribute
        {
            public string UserIdProperty { get; set; }

            public override bool IsValid(object value)
            {
                try
                {
                    var code = value?.ToString() ?? "";

                    string userId = HttpContext.Current.Request[UserIdProperty];

                    using (var db = new BATransferEntities())
                    {
                        var isvalid = db.CustomerFreeJourneys.Any(x => x.UserId.Equals(userId) && x.Code.Equals(code) && !x.IsUsed && x.IsEnabled);
                        return isvalid;
                    }

                    // Meeting start time must be before the end time

                }
                catch (Exception)
                {

                    return false;
                }

            }
        }


    public sealed class UseMyCreditsValidationAttribute : ValidationAttribute
    {
        public string UserIdProperty { get; set; }

        public override bool IsValid(object value)
        {
            try
            {

                decimal crtAmt = decimal.Parse(value.ToString());
                string userId = HttpContext.Current.Request[UserIdProperty];
                if (crtAmt > 0)
                {
                    using (var db = new BATransferEntities())
                    {
                        var isvalid = db.Customers.Any(x => x.userId.Equals(userId) && x.TotalCredits >= crtAmt);
                        return isvalid;
                    }
                }

                return false;

            }
            catch (Exception)
            {

                return false;
            }

        }
    }


}