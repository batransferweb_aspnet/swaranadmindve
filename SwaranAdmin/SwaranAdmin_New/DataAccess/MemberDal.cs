﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using SwaranAdmin_New.Models.Member;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Xml.Linq;

namespace SwaranAdmin_New.DataAccess
{
    public class MemberDal
    {
        #region Singalton
        private static MemberDal _instance;
        private static readonly object LockThis = new object();
        protected MemberDal()
        {
        }
        public static MemberDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new MemberDal());
                }
            }
        }
        #endregion

        public List<LoyaltyMemberSearchViewModel> GetLoyaltyCustomerSearch(string loyalty, string numberOfRecord, string currentpage)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    int loya = int.Parse(loyalty);
                    int recourds = int.Parse(numberOfRecord);
                    int currPage = int.Parse(currentpage);
                    var result =
                        db.LoyaltyCustomerSearch(loya, recourds, currPage).ToList();
                    IEnumerable<LoyaltyMemberSearchViewModel> objRecentBooking =
                        result.ToList().Select(x => new LoyaltyMemberSearchViewModel()
                        {
                            NmuOfPage = x.NumberOfPages ?? 0,
                            Email = x.Email,
                            FJGiven = x.FreeJourneysIssued ?? 0,
                            FJUsed = x.FreeJourneysUsed ?? 0,
                            FullName = x.FullName,
                            LastLoggedIn = x.LoggedIn.ToString(),
                            MemberId = x.MemberId,
                            Mobile = x.Mobile,
                            PointsLeft = x.AvailablePoints ?? 0,
                            Type = x.Type,

                        });

                    return objRecentBooking.ToList();

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");


            }
            return null;
        }



        public string GenerateRandomString(int length)
        {
            string chars = string.Empty;

            chars += "%$#0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }



        public bool CreateFreeJourney(CustomerFreeJourneyViewModel model)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var freeJourney = new CustomerFreeJourney();

                    freeJourney.BookingId = null;
                    freeJourney.Code = model.Code;
                    freeJourney.CustomerFreeJourneyTypeId = 1;
                    freeJourney.ExpiryDate = null;// ValueParseHelper.TryParseNullDate(model.ExpiryDate);
                    freeJourney.UserId = model.UserId;
                    freeJourney.sysCreatedDate = DateTime.UtcNow;
                    freeJourney.sysModifiedDate = DateTime.UtcNow;
                    freeJourney.IsEnabled = true;
                    freeJourney.IsFreeJourneyApplied = true;
                    freeJourney.LiveFreeJourneyId = 0;
                    freeJourney.LiveReservationId = 0;
                    freeJourney.UsedDate = null;
                    freeJourney.Description = model.Description;
                    freeJourney.CreatedStaffId = model.OperatorId;
                    freeJourney.CreatedBy = (int)FreeJourneyCreateEnum.Staff;
                    freeJourney.IsDeleted = false;
                    freeJourney.DeletedDate = null;
                    freeJourney.DeletedStaffId = null;
                    freeJourney.DeletedReason = "";
                    db.CustomerFreeJourneys.Add(freeJourney);
                    db.SaveChanges();
                    return true;

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");

                return false;
            }


        }



        public bool CreateCustomerDiscount(CustomerDiscountViewModel model)
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    if (db.Customers.Any(x => x.userId.Equals(model.CustomerUserId)))
                    {
                        var customer = db.Customers.First(x => x.userId.Equals(model.CustomerUserId));
                        var initialCredit = customer.TotalCredits;
                        customer.CreditByAdmin = customer.CreditByAdmin + model.TransactionCredit;
                        customer.TotalCredits = customer.TotalCredits + model.TransactionCredit;

                        var discount = new CustomerTransaction();
                        discount.TransactionTypeId = (int)TransactionTypeValueEnum.CreditByAdmin;
                        discount.TransactionReference = "";
                        discount.InitialCredit = initialCredit; // ValueParseHelper.TryParseNullDate(model.ExpiryDate);
                        discount.TransactionCredit = model.TransactionCredit;
                        discount.TransactionDebit = 0;
                        discount.FinalCredit = initialCredit + model.TransactionCredit;
                        discount.CustomerUserId = model.CustomerUserId;
                        discount.IsEnabled = true;
                        discount.IsDeleted = false;
                        discount.Description = model.Description;
                        discount.StaffUserId = model.StaffUserId;
                        discount.sysCreatedDate = DateTime.UtcNow;
                        discount.sysModifiedDate = DateTime.UtcNow;
                        db.CustomerTransactions.Add(discount);
                        db.SaveChanges();
                        return true;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }

        public bool AddMemberSpecialPreferences(MemberSpecialPreferenceViewModel model)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.CustomerSpecialPreferences.Any(x => x.CustomerUserId.Equals(model.UserId)))
                    {
                        var objSpecialPreferences = db.CustomerSpecialPreferences.First(x => x.CustomerUserId.Equals(model.UserId));
                        objSpecialPreferences.Note = model.Note;
                        objSpecialPreferences.StaffUserId = model.StaffId;
                        objSpecialPreferences.IsEnabled = model.IsEnabled;
                        db.SaveChanges();
                    }
                    else
                    {
                        var specialPreferences = new CustomerSpecialPreference();
                        specialPreferences.Note = model.Note;
                        specialPreferences.AddressId = null;
                        specialPreferences.CustomerUserId = model.UserId;
                        specialPreferences.StaffUserId = model.StaffId;
                        specialPreferences.IsEnabled = true;
                        specialPreferences.IsDeleted = false;
                        specialPreferences.sysCreatedDate = DateTime.UtcNow;
                        specialPreferences.sysModifiedDate = DateTime.UtcNow;
                        db.CustomerSpecialPreferences.Add(specialPreferences);
                        db.SaveChanges();
                    }
                    return true;

                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }

        public bool DeductCustomerDiscount(CustomerDiscountViewModel model)
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    if (db.Customers.Any(x => x.userId.Equals(model.CustomerUserId)))
                    {
                        var customer = db.Customers.First(x => x.userId.Equals(model.CustomerUserId));
                        var initialCredit = customer.TotalCredits;
                        customer.CreditByAdmin = customer.CreditByAdmin > 0 ? customer.CreditByAdmin - model.TransactionCredit : model.TransactionCredit;
                        customer.TotalCredits = customer.TotalCredits > 0 ? customer.TotalCredits - model.TransactionCredit : model.TransactionCredit;
                        var discount = new CustomerTransaction();
                        discount.TransactionTypeId = (int)TransactionTypeValueEnum.DebitByAdmin;
                        discount.TransactionReference = "";
                        discount.InitialCredit = initialCredit; // ValueParseHelper.TryParseNullDate(model.ExpiryDate);
                        discount.TransactionCredit = 0;
                        discount.TransactionDebit = model.TransactionCredit;
                        discount.FinalCredit = initialCredit > 0 ? initialCredit - model.TransactionCredit : model.TransactionCredit;
                        discount.CustomerUserId = model.CustomerUserId;
                        discount.IsEnabled = true;
                        discount.IsDeleted = false;
                        discount.Description = model.Description;
                        discount.StaffUserId = model.StaffUserId;
                        discount.sysCreatedDate = DateTime.UtcNow;
                        discount.sysModifiedDate = DateTime.UtcNow;
                        db.CustomerTransactions.Add(discount);
                        db.SaveChanges();
                        return true;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }
        public List<MemberSearchViewModel> CustomerSearch(string search)
        {
            try
            {
                using (var db = new BATransferEntities())
                {


                    var result =
                        db.CustomerSearchNew(search).ToList();
                    IEnumerable<MemberSearchViewModel> objRecentBooking =
                        result.ToList().Select(x => new MemberSearchViewModel()
                        {
                            UserId = x.UserId,
                            MemberId = x.MemberId,
                            FullName = x.FullName,
                            Mobile = x.Mobile,
                            Email = x.Email, 
                            NumOfFreeJourneyIssued = x.FreeJourneysIssued ?? 0,
                            NumOfFreeJourneyUsed = x.FreeJourneysUsed ?? 0,
                            DatOfJoin = x.DateJoined.ToString(),
                            LastLoggedIn = x.LoggedIn.ToString(),
                            AvailablePoints = x.AvailablePoints ?? 0,
                            LastPointUpdatedDate = x.LastPointUpdatedDate?.ToString() ?? "",
                            Points = x.AvailablePoints ?? 0,
                            Loyalty=x.Loyalty,
                         

                        });

                    return objRecentBooking.ToList();

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");


            }
            return null;
        }

        public  MemberSearchViewModel  SearchCurrentCustomer(string userid) 
        {
            try
            {
                using (var db = new BATransferEntities())
                {


                    var result =
                        db.CustomerSearchByUserId(userid).ToList(); 
                    IEnumerable<MemberSearchViewModel> objRecentBooking =
                        result.ToList().Select(x => new MemberSearchViewModel()
                        {
                            UserId = x.UserId,
                            MemberId = x.MemberId,
                            FullName = x.FullName,
                            Mobile = x.Mobile,
                            Email = x.Email, 
                            NumOfFreeJourneyIssued = x.FreeJourneysIssued ?? 0,
                            NumOfFreeJourneyUsed = x.FreeJourneysUsed ?? 0,
                            DatOfJoin = x.DateJoined.ToString(),
                            LastLoggedIn = x.LoggedIn.ToString(),
                            AvailablePoints = x.AvailablePoints ?? 0,
                            LastPointUpdatedDate = x.LastPointUpdatedDate?.ToString() ?? "",
                            Points = x.AvailablePoints ?? 0,
                            Loyalty = x.Loyalty,
                            NoOfWaitingBooking = x.NoOfWaitingBooking ?? 0,
                            NoOfCancelBooking = x.NoOfCancelledBooking??0,
                            NoOfCompletedBooking = x.NoOfCompletedBooking??0,
                            NoOfConfirmedBooking = x.NoOfConfirmedBooking??0,
                            IsCABOperator=x.IsCABOperator??false,

                        });

                    return objRecentBooking.FirstOrDefault();

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");


            }
            return null;
        }
        public SpecialPreferenceDisplayView GetSpecialPreference(string userid)
        {
            var specil = new SpecialPreferenceDisplayView();
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.CustomerSpecialPreferences.Any(x => x.CustomerUserId.Equals(userid)))
                    {
                        var preferance = db.CustomerSpecialPreferences.First(x => x.CustomerUserId.Equals(userid));
                        specil.Note = preferance.Note;
                        specil.IsEnabled = preferance.IsEnabled; 
                    } 

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");


            }
            return specil;
        }

        public List<ReadAllFreeJourneyViewMOdel> CustomerAllFreejourney(string customerUserId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {


                    var result =
                        db.sp_read_all_freejourneys(customerUserId).ToList();
                    IEnumerable<ReadAllFreeJourneyViewMOdel> objRecentBooking =
                        result.ToList().Select(x => new ReadAllFreeJourneyViewMOdel()
                        {
                            CustomerFreeJourneyId = x.CustomerFreeJourneyId,
                            Code = x.Code,
                            CreatedBy = x.CreatedBy,
                            CreatedDate = x.sysCreatedDate.ToString(),
                            UsedDate = x.UsedDate?.ToString() ?? "",
                            ExpiryDate = x.ExpiryDate?.ToString() ?? "",
                            FromAddress = x.FromAddress,
                            ToAddress = x.ToAddress,
                            DeletedBy = x.DeletedBy,
                            DeletedReason = x.DeletedReason,
                            IsUsed = x.IsUsed,
                            IsEnabled = x.IsEnabled,
                            IsDeleted = x.IsDeleted,
                            CustomerUserId = customerUserId
                        });

                    return objRecentBooking.ToList();

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");


            }
            return null;
        }

        public List<ReadAllCustomerCreditsViewModel> GetCustomerStatement(string customerUserId) 
        {
            try
            {
                using (var db = new BATransferEntities())
                {


                    var result =
                        db.sp_swaran_customer_transaction_history(customerUserId).ToList();
                    IEnumerable<ReadAllCustomerCreditsViewModel> objRecentBooking =
                        result.ToList().Select(x => new ReadAllCustomerCreditsViewModel()
                        {
                            TransactionDate = ValueParseHelper.ToShortStringDate(x.TransactionDate),
                            TransactionType = x.TransactionType,
                            Description = x.Description,
                            TransactionCredit = x.TransactionCredit,
                            TransactionDebit = x.TransactionDebit,
                            FinalCredit = x.FinalCredit,
                            StaffName = x.StaffName
                        });

                    return objRecentBooking.ToList();

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");


            }
            return null;
        }

        public MainCustomerCreditsViewModel  GetCustomercredits(string customerUserId)
        {
            var main = new MainCustomerCreditsViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    var customer = db.Customers.FirstOrDefault(c => c.userId.EndsWith(customerUserId));
                    if (customer != null)
                    {
                        main.AvailabelCredits= customer.TotalCredits;
                    }

                    var result =
                        db.sp_swaran_customer_transaction_history_admin(customerUserId).ToList();
                    IEnumerable<ReadAllCustomerCreditsViewModel> objRecentBooking =
                        result.ToList().Select(x => new ReadAllCustomerCreditsViewModel()
                        {
                            TransactionDate =ValueParseHelper.ToShortStringDate(x.TransactionDate),
                            TransactionType = x.TransactionType,
                            Description = x.Description,
                            TransactionCredit = x.TransactionCredit,
                            TransactionDebit = x.TransactionDebit,
                            FinalCredit = x.FinalCredit,
                            StaffName = x.StaffName

                        });

                    main .ListCustomerCredits= objRecentBooking.ToList();

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");


            }
            return main;
        }

        public string DeleteFreeJourney(FreeJourneyCancelViewModel model, string deletedStaffId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.CustomerFreeJourneys.Any(x => x.CustomerFreeJourneyId == model.FreeJourneyId))
                    {
                        var freeJourney =
                            db.CustomerFreeJourneys.First(x => x.CustomerFreeJourneyId == model.FreeJourneyId);
                        if (!freeJourney.IsUsed && freeJourney.IsEnabled)
                        {
                            freeJourney.IsDeleted = true;
                            freeJourney.IsEnabled = false;
                            freeJourney.DeletedStaffId = deletedStaffId;
                            freeJourney.DeletedReason = model.Reson;
                            freeJourney.sysModifiedDate = DateTime.UtcNow;
                            db.SaveChanges();
                            return "Successfuly Deleted";
                        }
                        else
                        {
                            return "freejourney already used";
                        }
                    }
                    else
                    {
                        return "Invalid Id";
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");
                return "Something wend worng";


            }

        }

        public List<RefoundGridViewModel> GetRefundGrid(string bookingNumber, string adminUserId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<RefoundGridViewModel> refoundGrid = db.sp_swaran_refund_grid(bookingNumber).ToList().Select(x => new RefoundGridViewModel()
                    {
                        BookedPersonName = x.BookedPersonName,
                        BookedPersonTel = x.BookedPersonTel,
                        BookingDateTime = x.BookingDateTime.ToString(),
                        BookingFrom = x.BookingFrom,
                        BookingTo = x.BookingTo,
                        BookingNumber = x.BookingNumber,
                        BookingToken = x.BookingToken,
                        IsReturnBooking = x.IsReturnBooking,
                        Status = x.Status,
                        Amount = x.Amount,
                        FinalPrice = x.FinalPrice,
                        OrderCode = x.OrderCode,
                        AdminUserId = adminUserId
                    });

                    return refoundGrid.ToList();
                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return null;
        }
        public List<RepaymentViewModel> GetRePaymentGrid(string bookingNumber, string adminUserId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<RepaymentViewModel> refoundGrid = db.sp_swaran_check_payment_status(bookingNumber).ToList().Select(x => new RepaymentViewModel()
                    {
                        BookingNumber = x.BookingNumber,
                        BookingToken = HttpContext.Current.Server.UrlEncode(x.BookingToken),
                        BookingDateTime = ValueParseHelper.ToStringDateTime(x.BookingDateTime),
                        Status = x.Status,
                        BookingFrom = x.BookingFrom,
                        BookingTo = x.BookingTo,
                        BookedPersonName = x.BookedPersonName,
                        BookedPersonTel = x.BookedPersonTel,
                        PaymentMethodId = x.PaymentMethodId,
                        PaymentMethod = x.PaymentMethod,
                        IsReturnBooking = x.IsReturnBooking,
                        OneWayPrice = x.OneWayPrice,
                        OneWayCredit = x.OneWayCredit,
                        ReturnPrice = x.ReturnPrice,
                        ReturnCredit = x.ReturnCredit,
                        JourneyType = x.JourneyType,
                        AdminUserId = adminUserId,
                        FinalPrice = x.TotalPrice,
                        PaymentStatus = x.PaymentStatus
                    });

                    return refoundGrid.ToList();
                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return null;
        }

        public async Task<EmailVerificationViewModel> SendQuickEmail(SendQuickEmailViewModel model, EmailAttachmentsViewMOdel emailAttachement)
        {
            var userVerify = new EmailVerificationViewModel();
            try
            {
                EmailCredentialsViewModel emailCredentials = EmailHelper.GetCustomerEmaillCredentials();

                var listEmails = new List<string>() { model.ToAddress };
                var emailRoot = new RootEmail();
                emailRoot.Attachments = null;
                emailRoot.Body = model.Message;
                emailRoot.FromEmailAddress = emailCredentials.FromEmail;
                emailRoot.Username = emailCredentials.Username;
                emailRoot.Password = emailCredentials.Password;
                emailRoot.Port = emailCredentials.Port;
                emailRoot.Smtp = emailCredentials.Smtp;
                emailRoot.Subject = model.Subject;
                emailRoot.ToEmailAddresses = listEmails;
                emailRoot.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailApi);
                userVerify.IsSuccess = await EmailHelper.SendEmail(emailRoot, apiUrl, emailAttachement);
                userVerify.Message = "Email successfully sent";

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return userVerify;
        }

        public async Task<SaveWorlpayMain> WorldpayFullyRefund(FullRefundViewModel repayment, string userId)
        {
            var saveWorlpayMain = new SaveWorlpayMain();
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.Bookings.Any(x => x.BookingNumber.Equals(repayment.bookingNumber)))
                    {
                        var objBookingFixed = db.Bookings.First(x => x.BookingNumber.Equals(repayment.bookingNumber));

                        if (objBookingFixed.PaymentMethod == (int)PaymentTypeEnum.Card &&
                            (objBookingFixed.PaymentStatus == (int)PaymentStatusEnum.Paid ||
                            objBookingFixed.PaymentStatus == (int)PaymentStatusEnum.PartialRefund))
                        {
                            var refundViewModel = new WorldpayFullyRefundViewModel();

                            refundViewModel.userId = userId;
                            refundViewModel.bookingRefundType = repayment.bookingRefundType;
                            refundViewModel.refundRemarks = repayment.refundRemarks;
                            refundViewModel.orderCode =
                                objBookingFixed.BookingPayments.First(
                                    x => x.PaymentTypeId == (int)PaymentTypeEnum.Card)
                                    .BookingPaymentCards.First()
                                    .OrderCode;
                            refundViewModel.isRefundByAdmin = true;
                            refundViewModel.isCreditRefund = false;
                            refundViewModel.creditsToRefund = 0;
                            refundViewModel.isIncludeAdditionalCharge = repayment.isIncludeAdditionalCharge;
                            HttpClient client = new HttpClient();
                            //  string apiUrl = "http://paymentgateway.batransfer.at/api/payment/refundpayment";

                            string apiUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.FullRefundUrl);
                            var resentBookingJson = JSonHelper.ConvertObjectToJSon(refundViewModel);
                            HttpContent contentPost = new StringContent(resentBookingJson, Encoding.UTF8, "application/json");

                            var response = await client.PostAsync(apiUrl, contentPost);
                            response.EnsureSuccessStatusCode();
                            var result = await response.Content.ReadAsStringAsync();
                            var saveObject = JSonHelper.ConvertJSonToObject<SaveWorlpayMain>(result);

                            return saveObject;


                        }
                    }
                    else
                    {
                        saveWorlpayMain.status = false;
                        saveWorlpayMain.message = "Invalid Booking Number";
                        return saveWorlpayMain;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
                saveWorlpayMain.status = false;
                saveWorlpayMain.message = ex.Message;
                return saveWorlpayMain;
            }
            return null;
        }

        public async Task<SaveWorlpayMain> WorldpayPartialyRefund(PartialyRefundViewModel repayment, string UserId)
        {
            var saveWorlpayMain = new SaveWorlpayMain();
            try
            {
                using (var db = new BATransferEntities())
                {

                    if (db.Bookings.Any(x => x.BookingNumber.Equals(repayment.bookingNumber)))
                    {
                        var objBookingFixed = db.Bookings.First(x => x.BookingNumber.Equals(repayment.bookingNumber));

                        if (objBookingFixed.PaymentMethod == (int)PaymentTypeEnum.Card
                          && (objBookingFixed.PaymentStatus == (int)PaymentStatusEnum.Paid
                          || objBookingFixed.PaymentStatus == (int)PaymentStatusEnum.PartialRefund))
                        {
                            var refundViewModel = new WorldpayPartialRefundViewModel();
                            refundViewModel.amount = repayment.amount;
                            refundViewModel.userId = UserId;
                            refundViewModel.isReturnBooking = repayment.isReturnBooking;
                            refundViewModel.refundRemarks = repayment.refundRemarks;
                            refundViewModel.orderCode =
                                objBookingFixed.BookingPayments.First(
                                    x => x.PaymentTypeId == (int)PaymentTypeEnum.Card)
                                    .BookingPaymentCards.First()
                                    .OrderCode;
                            refundViewModel.isRefundByAdmin = true;
                            refundViewModel.isCreditRefund = false;
                            refundViewModel.creditsToRefund = 0;

                            HttpClient client = new HttpClient();
                            //  string apiUrl = "http://paymentgateway.batransfer.at/api/payment/partialrefundpayment";
                            string apiUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.PartialRefundUrl);
                            var resentBookingJson = JSonHelper.ConvertObjectToJSon(refundViewModel);
                            HttpContent contentPost = new StringContent(resentBookingJson, Encoding.UTF8, "application/json");

                            var response = await client.PostAsync(apiUrl, contentPost);
                            response.EnsureSuccessStatusCode();
                            var result = await response.Content.ReadAsStringAsync();
                            var saveObject = JSonHelper.ConvertJSonToObject<SaveWorlpayMain>(result);
                            return saveObject;
                        }
                    }
                    else
                    {
                        saveWorlpayMain.status = false;
                        saveWorlpayMain.message = "Invalid Booking Number";
                        return saveWorlpayMain;

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
                saveWorlpayMain.status = false;
                saveWorlpayMain.message = ex.Message;
                return saveWorlpayMain;
            }
            return saveWorlpayMain;
        }

        #region Tell a friend
        public List<TellAFriendEmailViewModel> GetInfoTellFriends()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<TellAFriendEmailViewModel> objAFriend =
                        db.TellAFriendEmails.Where(x => !x.IsApproved).Select(x => new TellAFriendEmailViewModel()
                        {
                            TellAFriendEmailId = x.TellAFriendEmailId,
                            MessageBody = x.MessageBody,
                            YourEmail = x.YourEmail,
                            YourName = x.YourName,
                            FriendEmail1 = x.FriendEmail1,
                            FriendEmail2 = x.FriendEmail2,
                            FriendEmail3 = x.FriendEmail3
                        });

                    return objAFriend.ToList();
                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public async Task<bool> SendEmailTellFriends(int id)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.TellAFriendEmails.Any(x => x.TellAFriendEmailId == id))
                    {
                        var objTellFriend = db.TellAFriendEmails.First(x => x.TellAFriendEmailId == id);


                        var message = await SendEmailtoMe(objTellFriend);
                        if (message)
                        {
                            objTellFriend.IsApproved = true;
                            db.SaveChanges();

                        }
                        return message;
                    }
                }


            }
            catch (Exception ex)
            {


                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }






        public void SendTellFriendEmail(TellAFriendEmail infor)
        {
            try
            {
                var listemail = new List<string>();

                if (!string.IsNullOrEmpty(infor.FriendEmail1))
                {
                    listemail.Add(infor.FriendEmail1);
                }
                if (!string.IsNullOrEmpty(infor.FriendEmail2))
                {
                    listemail.Add(infor.FriendEmail2);
                }
                if (!string.IsNullOrEmpty(infor.FriendEmail3))
                {
                    listemail.Add(infor.FriendEmail3);
                }
                string logFilePathTellFriends = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/TellFriends.txt"));
                string tellFriendstext = System.IO.File.ReadAllText(logFilePathTellFriends);

                tellFriendstext = tellFriendstext.Replace("SENDERNAME", infor.YourName);
                tellFriendstext = tellFriendstext.Replace("SENDERMESSAGE", infor.MessageBody);




                EmailCredentialsViewModel emailCredentials = EmailHelper.GetCustomerEmaillCredentials();
                var email = new RootEmail();
                email.Attachments = null;
                email.Body = tellFriendstext;
                email.FromEmailAddress = emailCredentials.FromEmail;
                email.Username = emailCredentials.Username;
                email.Password = emailCredentials.Password;
                email.Port = emailCredentials.Port;
                email.Smtp = emailCredentials.Smtp;

                email.Subject = infor.YourName + " sent you an invitation to batransfer.com";
                email.ToEmailAddresses = listemail;
                email.DisplayName = emailCredentials.DisplayName;
                email.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailApi);
                EmailHelper.SendEmail(email, apiUrl);



            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }


        }




        public async Task<bool> SendEmailtoMe(TellAFriendEmail infor)
        {
            try
            {




                var listemail = new List<string>();
                listemail.Add(infor.YourEmail);

                string logFilePathTellFriends = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/TellFriendsTome.txt"));
                string tellFriendstext = System.IO.File.ReadAllText(logFilePathTellFriends);

                tellFriendstext = tellFriendstext.Replace("SENDERNAME", infor.YourName);

                EmailCredentialsViewModel emailCredentials = EmailHelper.GetCustomerEmaillCredentials();
                var email = new RootEmail();
                email.Attachments = null;
                email.Body = tellFriendstext;
                email.FromEmailAddress = emailCredentials.FromEmail;
                email.Username = emailCredentials.Username;
                email.Password = emailCredentials.Password;
                email.Port = emailCredentials.Port;
                email.Smtp = emailCredentials.Smtp;

                email.Subject = " Thank you for the recommendation";
                email.ToEmailAddresses = listemail;
                email.DisplayName = emailCredentials.DisplayName;
                email.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailApi);
                SendTellFriendEmail(infor);
                return await EmailHelper.SendEmail(email, apiUrl);



            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }

            return false;
        }

        #endregion


        #region Testimonial
        public List<AdminCustomerReviewViewModel> GetCustomerReview() 
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<AdminCustomerReviewViewModel> objAFriend =
                        db.sp_swaran_RetrieveReviewsForAdmin().ToList().Select(x => new AdminCustomerReviewViewModel()
                        {
                            ReviewId = x.ReviewId,
                            Name = x.Name,
                            Email = x.Email,
                            Location = x.Location,
                            Rating = x.Rating,
                            Title = x.Title,
                            Message = x.Message, 
                            CreatedDate = x.CreatedDate,
                            SiteId = x.Site

                        });

                    return objAFriend.ToList();
                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public bool AllowCustomerReview(long reviewId) 
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objReview = db.CustomerReviews.FirstOrDefault(x => x.ReviewId == reviewId);
                    if (objReview != null)
                    {
                        objReview.IsActive = true;
                        objReview.IsEnable = true;
                        db.SaveChanges();
                        return true;
                    }
                    return false;

                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }
        public bool RejectCustomerReview(long reviewId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objReview = db.CustomerReviews.FirstOrDefault(x => x.ReviewId == reviewId);
                    if (objReview != null)
                    {
                        objReview.IsDeleted = true;
                        
                        db.SaveChanges();
                        return true;
                    }
                    return false;

                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }
        #endregion


        public MemoryStream GetReceipt(string bookingToken, out string bookingNumber)
        {

            bookingNumber = string.Empty;
            MemoryStream memoryStream = new MemoryStream();
            var currentBooking = new CurrentBookingDisplayViewModel();
            Booking objBookingReturn = null;
            try
            {
                // bookingToken = "ACV7kZ+VIvLq8C9k7R9b6qbrOEr8Gd+ORwe6mlImtNpnaQOikgtapFhlwROWqJafnA==";
                using (var db = new BATransferEntities())
                {


                    if (db.Bookings.Any(x => x.BookingToken.Equals(bookingToken)))
                    {
                        var objBooking = db.Bookings.First(x => x.BookingToken.Equals(bookingToken));

                        bookingNumber = objBooking.BookingNumber;

                        if (objBooking.IsSourceBooking)
                        {
                            objBookingReturn =
                                db.Bookings.First(
                                    bt => bt.BookingId == objBooking.ReferenceBookingId && bt.IsReturnBooking);
                        }
                        currentBooking.MainBooking = ReceiptString(objBooking, objBookingReturn);
                    }
                }


                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                    {
                        StringReader sr = new StringReader(currentBooking.MainBooking);

                        Document pdfDoc = new Document(PageSize.A4);

                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);



                        PdfWriter.GetInstance(pdfDoc, memoryStream).CloseStream = false;
                        pdfDoc.Open();

                        htmlparser.Parse(sr);

                        //XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);

                        pdfDoc.Close();
                        //byte[] bytes = memoryStream.ToArray();
                        //memoryStream.Close();

                        byte[] byteInfo = memoryStream.ToArray();
                        memoryStream.Write(byteInfo, 0, byteInfo.Length);
                        memoryStream.Position = 0;



                    }
                }


            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            //var file = File(currentBooking.MainBooking, "application/pdf", "fileName.pdf");
            //return file;

            return memoryStream;
        }


        private string ReceiptString(Booking booking, Booking returnJourney)
        {
            try
            {
                StringBuilder builder = new StringBuilder();
                string logFilePathHeader = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Receipt/ReceiptTemplate.html"));

                //LOGO-URL
                string textHeader = System.IO.File.ReadAllText(logFilePathHeader);
                string logoUrl = ConfigHelper.GetWebLogoSource(booking.BookingSource);
                textHeader = textHeader.Replace("LOGO-URL", logoUrl);
                textHeader = textHeader.Replace("#BOOKINGNUMBER#", booking.BookingNumber);
                textHeader = textHeader.Replace("#PASSENGERNAME#", booking.BookedPersonName);
                textHeader = textHeader.Replace("#PASSENGEREMAIL#", booking.BookedPersonEmail);
                textHeader = textHeader.Replace("#PASSENGERMOBILE#", booking.BookedPersonTel);
                textHeader = textHeader.Replace("#ONEWAYPRICE#", booking.FinalPrice.ToString());
                textHeader = textHeader.Replace("#PICKUPDATE#", booking.BookingDateTime.ToString());
                textHeader = textHeader.Replace("#PICKUPPOINT#", booking.BookingFrom.ToString());
                textHeader = textHeader.Replace("#DROPOFFPOINT#", booking.BookingTo.ToString());
                builder.Append(textHeader);

                if (returnJourney != null)
                {
                    string returnBookingPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Receipt/ReceiptReturnDetails.html"));

                    string returnBookingString = System.IO.File.ReadAllText(returnBookingPath);
                    returnBookingString = returnBookingString.Replace("#RETURNPRICE#", returnJourney.FinalPrice.ToString());
                    returnBookingString = returnBookingString.Replace("#RETURNPICKUPDATE#", returnJourney.BookingDateTime.ToString());
                    returnBookingString = returnBookingString.Replace("#RETURNPICKUPPOINT#", returnJourney.BookingFrom.ToString());
                    returnBookingString = returnBookingString.Replace("#RETURNDROPOFFPOINT#", returnJourney.BookingTo.ToString());
                    builder.Append(returnBookingString);
                }

                string receiptFooterPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Receipt/ReceiptFooter.html"));

                string receiptFooterString = System.IO.File.ReadAllText(receiptFooterPath);
                builder.Append(receiptFooterString);

                return builder.ToString();

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }


        public string UpdateCabOperator(string userid,bool isCabOperator)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var customer = db.Customers.FirstOrDefault(x=>x.userId.Equals(userid));
                    if (customer != null)
                    {
                        customer.IsCABOperator = isCabOperator;
                        db.SaveChanges();
                        return "Successfully Updated";
                    }
                }
            }
            catch (Exception ex)
            {


                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return "Could Not Update as CAB Operator";
        }

        public bool SaveBlockmember(BlockMemberBookingViewModel member)
        {
            try
            {
                using (var db=new BATransferEntities())
                {
                    var objBlockmember = db.BlockMemberBookings.FirstOrDefault(x => x.UserId.Equals(member.UserId));
                    if (objBlockmember != null)
                    {
                        objBlockmember.EmailId = member.EmailId?.Trim() ?? "";
                        objBlockmember.TelephoneNumber = member.Telephone?.Trim() ?? "";
                        objBlockmember.CookieId = member.CookieId?.Trim() ?? "";
                        objBlockmember.IpAddress = member.IpAddress?.Trim() ?? "";
                        objBlockmember.ResonForBlock = member.ResonForBlock?.Trim() ?? "";
                        objBlockmember.ModifiedDate = DateTime.UtcNow;
                        db.SaveChanges();
                    }
                    else
                    {
                        var book = new BlockMemberBooking();
                        book.UserId = member.UserId;
                        book.EmailId = member.EmailId?.Trim() ?? "";
                        book.TelephoneNumber = member.Telephone?.Trim() ?? "";
                        book.CookieId = member.CookieId?.Trim() ?? "";
                        book.IpAddress = member.IpAddress?.Trim() ?? "";
                        book.ResonForBlock = member.ResonForBlock?.Trim() ?? "";
                        book.ModifiedDate = DateTime.UtcNow;
                        book.CreateDate = DateTime.UtcNow;
                        db.BlockMemberBookings.Add(book);
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {


                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }


        public bool UnblockMember(BlockMemberBookingViewModel member) 
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objBlockmember = db.BlockMemberBookings.FirstOrDefault(x => x.UserId.Equals(member.UserId));
                    if (objBlockmember != null)
                    {
                        db.BlockMemberBookings.Remove(objBlockmember);
                        db.SaveChanges();
                    }
                    return true;

                }
            }
            catch (Exception ex)
            {


                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }

        #region  Pending Member Activation
        public List<MemberPendingActivationViewModel> GetPendingMember()
        {
            try
            {
                var britishTime = BritishTime.GetDateTime().AddHours(-12);
                using (var db = new BATransferEntities())
                {

                    IEnumerable<MemberPendingActivationViewModel> members =
                        db.sp_pendingActiveMember(britishTime, britishTime)
                            .ToList()
                            .Select(x => new MemberPendingActivationViewModel()
                            {
                                CreatedDate = x.CreatedDate?.ToString("dd/MM/yyyy") ?? "",
                                Email = x.Email,
                                FirstName = x.FirstName,
                                Id = x.Id,
                                IsAuthorized = x.isAuthorized,
                                IsEmailVerified = x.IsEmailVerified,
                                LastName = x.LastName,
                                MemberId = x.MemberId,
                                PhoneNumber = x.PrimaryPhoneNumber,
                                UserName = x.UserName

                            });

                    return members.ToList();
                }
            }
            catch (Exception ex)
            {


                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public void DeleteActivation(string id)
        {
            try
            {
                using (BATransferEntities db=new BATransferEntities() )
                {
                    var member = db.AspNetUsers.FirstOrDefault(x => x.Id.Equals(id));
                    if (member != null)
                    {
                        member.IsDeleted = true;
                        member.sysModifiedDate = DateTime.UtcNow;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {


                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }

        public void Activemember(string id)
        {
            try
            {
                using (BATransferEntities db = new BATransferEntities())
                {
                    var member = db.AspNetUsers.FirstOrDefault(x => x.Id.Equals(id));
                    if (member != null)
                    {
                        member.IsDeleted = false;
                        member.isAuthorized = true;
                        member.IsEmailVerified = true;
                        member.isUserValidated = true;
                        member.sysModifiedDate = DateTime.UtcNow;
                         db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {


                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }
        #endregion
    }

    public class SaveWorlpayMain
    {
        public bool status { get; set; }
        public string message { get; set; }
        public int statusCode { get; set; }
        public bool data { get; set; }
    }

    public class SpecialPreferenceDisplayView
    {
        public string Note { get; set; }
        public bool IsEnabled { get; set; } 
    }
}