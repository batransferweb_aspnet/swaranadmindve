﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System;
using System.Linq;

namespace SwaranAdmin_New.DataAccess
{
    public class LayoutDal
    {
        #region Singalton
        private static LayoutDal _instance;
        private static readonly object LockThis = new object();
        protected LayoutDal()
        {
        }
        public static LayoutDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new LayoutDal());
                }
            }
        }
        #endregion

        public LayoutViewModel GetLayoutInfo(string userName)
        {
            var layout = new LayoutViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    var today = BritishTime.GetDateTime();
                    var count = db.sp_swaran_layout_waiting_booking(today).ToList().Count;
                    layout.Count = count;
                    layout.UserName = userName;
                    return layout;

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");


            }
            return null;
        }

    }
}