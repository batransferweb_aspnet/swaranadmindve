﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models.Pricing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SwaranAdmin_New.DataAccess
{
    public class PricingDal
    {

        #region Singleton

        private static PricingDal _instance;

        private static readonly object LockThis = new object();

        protected PricingDal()
        {
        }

        public static PricingDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new PricingDal());
                }
            }
        }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<CustomerRateViewModel> GetCustomerRates()
        {

            try
            {
                using (var db = new BATransferEntities())
                {

                    IEnumerable<CustomerRateViewModel> customerRate =
                        db.Rate_Customer.Where(rc => !rc.IsDelete).Select(x => new CustomerRateViewModel()
                        {
                            RateId = x.RateId,
                            RateName = x.RateName,
                            RateType = x.RateType,
                            //IsDistanceShow = x.IsDistanceShow,
                            //IsSpeedShow = x.IsSpeedShow,
                            //IsDefault = x.IsDefault,
                            IsActive = x.IsActive,
                            //RateVehicleList = new RateVehicleList()
                            //{
                            //    RateVehicleTypes = x.Rate_Customer_VehicleType.Select(cvt => new RateVehicleType()
                            //    {
                            //        RateId = cvt.RateId,
                            //        VehicleTypeId = cvt.VehicletypeId,
                            //        VehicleTypeName = cvt.VehicleType.VehicleTypeName

                            //    }).ToList()
                            //}
                        });
                    return customerRate.ToList();

                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rateId"></param>
        /// <returns></returns>
        public CustomerRateDetailModel GetCustomerRateDetail(long rateId)
        {

            try
            {
                using (var db = new BATransferEntities())
                {

                    var relevantCustomerRate = db.Rate_Customer.FirstOrDefault(rc => rc.RateId == rateId);

                    if (relevantCustomerRate != null)
                    {

                        string rateFormula = string.Empty;

                        if (relevantCustomerRate.Rate_SpecialOccasion != null && relevantCustomerRate.Rate_SpecialOccasion.Any())
                        {
                            rateFormula = relevantCustomerRate.Rate_SpecialOccasion.FirstOrDefault().RateFormula;
                        }

                        CustomerRateDetailModel response = new CustomerRateDetailModel()
                        {
                            RateId = relevantCustomerRate.RateId,
                            RateName = relevantCustomerRate.RateName,
                            RateType = relevantCustomerRate.RateType,
                            VatRateId = relevantCustomerRate.VatRateId,
                            IsDistanceShow = relevantCustomerRate.IsDistanceShow,
                            IsSpeedShow = relevantCustomerRate.IsSpeedShow,
                            IsActive = relevantCustomerRate.IsActive,
                            IsDefault = relevantCustomerRate.IsDefault
                        };

                        return response;
                    }
                    else
                    {
                        return null;
                    }


                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public bool UpdateCustomerRate(CustomerRateDetailModel request)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var relevantCustomerRate = db.Rate_Customer.FirstOrDefault(rc => rc.RateId == request.RateId);

                    if (relevantCustomerRate != null)
                    {
                        relevantCustomerRate.RateName = request.RateName;
                        relevantCustomerRate.RateType = request.RateType;
                        relevantCustomerRate.VatRateId = request.VatRateId;
                        relevantCustomerRate.IsDistanceShow = request.IsDistanceShow;
                        relevantCustomerRate.IsSpeedShow = request.IsSpeedShow;
                        relevantCustomerRate.ModifiedDate = DateTime.UtcNow;
                        relevantCustomerRate.IsDefault = request.IsDefault;
                        relevantCustomerRate.IsActive = request.IsActive;

                        db.SaveChanges();

                        return true;

                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetVatRates()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<SelectListItem> vatRates = db.Admin_VAT.ToList()
                   .Where(x => x.IsDelete == false)
                   .Select(x => new SelectListItem
                   {
                       Value = x.VatId.ToString(),
                       Text = x.VatName,
                       Disabled = x.IsDelete
                   });

                    return vatRates;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetCustomerRate()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<SelectListItem> customerRate =
                        db.Rate_Customer.Where(rc => !rc.IsDelete)
                   .Select(x => new SelectListItem
                   {
                       Value = x.RateId.ToString(),
                       Text = x.RateName,
                       Disabled = x.IsDelete
                   });

                    return customerRate;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return null;
        }

        public List<RateVehicleType> GetVehicleTypeForCustomerRate(long rateId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    List<RateVehicleType> relevantVehicleTypes = db.Rate_Customer_VehicleType.Where(rcv => rcv.RateId == rateId).Select(rcvv => new RateVehicleType()
                    {
                        RateCustomerVehicleTypeId = rcvv.RateCustomerVehicleTypeId,
                        RateId = rcvv.RateId,
                        VehicleTypeId = rcvv.VehicletypeId,
                        VehicleTypeName = rcvv.VehicleType.VehicleTypeName
                    }).ToList();

                    return relevantVehicleTypes;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return new List<RateVehicleType>();
        }

        public List<RateCustomerDistanceDayDom> GetRateVehicleTypeDayDetail(long rateCustomerVehicleTypeId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    List<RateCustomerDistanceDayDom> relevantCustomerDistanceDays = db.RateCustomer_Distance_Day.Where(rcv => rcv.RateCustomerVehicleTypeId == rateCustomerVehicleTypeId).Select(rcvv => new RateCustomerDistanceDayDom()
                    {
                        RateCustomerDistanceDayId = rcvv.DayId,
                        DayName = rcvv.Day == 1 ? "Monday" :
                                  rcvv.Day == 2 ? "Tuesday" :
                                  rcvv.Day == 2 ? "Wednesday" :
                                  rcvv.Day == 2 ? "Thursday" :
                                  rcvv.Day == 2 ? "Friday" :
                                  rcvv.Day == 2 ? "Saturday" :
                                  rcvv.Day == 2 ? "Sunday" : "Default"
                        //RateCustomerDistanceRecords = rcvv.RateCustomer_Distance.Where(rcc => !rcc.IsDeleted).Select(rcd => new RateCustomerDistanceDom() {
                        //    RateCustomerDistanceDayId = rcvv.DayId,
                        //    DistanceId = rcd.DistanceId,
                        //    DistanceFrom = rcd.DistanceFrom,
                        //    DistanceTo = rcd.DistanceTo,
                        //    Amount = rcd.Amount,
                        //    Unit = rcd.Unit,
                        //    IsDefault = rcd.IsDefault

                        //}).ToList()
                        //RateCustomerVehicleTypeId = rcvv.RateCustomerVehicleTypeId,
                        //RateId = rcvv.RateId,
                        //VehicleTypeId = rcvv.VehicletypeId,
                        //VehicleTypeName = rcvv.VehicleType.VehicleTypeName
                    }).ToList();

                    return relevantCustomerDistanceDays;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return new List<RateCustomerDistanceDayDom>();
        }

        public List<RateCustomerDistanceDom> GetRateVehicleTypeDistanceDetail(long rateCustomerDistanceDayId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    List<RateCustomerDistanceDom> relevantCustomerDistances = db.RateCustomer_Distance.Where(rcv => rcv.DayId == rateCustomerDistanceDayId && !rcv.IsDeleted).Select(rcvv => new RateCustomerDistanceDom()
                    {
                        RateCustomerDistanceDayId = rcvv.DayId,
                        DistanceId = rcvv.DistanceId,
                        DistanceFrom = rcvv.DistanceFrom,
                        DistanceTo = rcvv.DistanceTo,
                        Amount = rcvv.Amount,
                        Unit = rcvv.Unit,
                        IsDefault = rcvv.IsDefault
                    }).OrderBy(rr => rr.DistanceFrom).ToList();

                    relevantCustomerDistances.Add(
                        new RateCustomerDistanceDom()
                        {
                            RateCustomerDistanceDayId = rateCustomerDistanceDayId,
                            DistanceId = 0,
                            DistanceFrom = 0,
                            DistanceTo = 0,
                            Amount = 0,
                            Unit = "mile",
                            IsDefault = false
                        }
                        );

                    return relevantCustomerDistances;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return new List<RateCustomerDistanceDom>();
        }

        public List<SelectListItem> GetAvailableVehicleTypes(long? rateId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    List<SelectListItem> availableVehicleTypes = (from c in db.VehicleTypes.Where(vt => vt.IsActive && !vt.IsDelete)
                                                                  where !(from o in db.Rate_Customer_VehicleType.Where(rcv => rcv.RateId == rateId).Select(rcvv => rcvv.VehicletypeId).ToList() select o).Contains(c.VehicleTypeID)
                                                                  select c).Select(x => new SelectListItem
                                                                  {
                                                                      Value = x.VehicleTypeID.ToString(),
                                                                      Text = x.VehicleTypeName,
                                                                      Disabled = x.IsDelete
                                                                  }).ToList();
                    return availableVehicleTypes;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return null;
        }

        public List<SelectListItem> GetAvailableDays(long? rateCustomerVehicleTypeId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    List<SelectListItem> days = new List<SelectListItem>();
                    days.Add(new SelectListItem { Value = 0.ToString(), Text = "Default" });
                    days.Add(new SelectListItem { Value = 1.ToString(), Text = "Monday" });
                    days.Add(new SelectListItem { Value = 2.ToString(), Text = "Tuesday" });
                    days.Add(new SelectListItem { Value = 3.ToString(), Text = "Wednesday" });
                    days.Add(new SelectListItem { Value = 4.ToString(), Text = "Thursday" });
                    days.Add(new SelectListItem { Value = 5.ToString(), Text = "Friday" });
                    days.Add(new SelectListItem { Value = 6.ToString(), Text = "Saturday" });
                    days.Add(new SelectListItem { Value = 7.ToString(), Text = "Sunday" });

                    var addedDayRecords = db.RateCustomer_Distance_Day.Where(vt => vt.RateCustomerVehicleTypeId == rateCustomerVehicleTypeId && !vt.IsDeleted);

                    if (addedDayRecords != null && addedDayRecords.Any())
                    {

                        foreach (var record in addedDayRecords)
                        {
                            days.Remove(days.FirstOrDefault(d => d.Value == record.Day.ToString()));
                        }

                        return days;

                    }
                    else
                    {
                        return days;
                    }

                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return null;
        }

        public bool SaveDistanceDetail(RateCustomerDistanceForUpdate distanceModel)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var relevantDistanceDetail = db.RateCustomer_Distance.FirstOrDefault(rcd => rcd.DistanceId == distanceModel.CurrentDistanceId);

                    var relevantNextDistanceDetail = db.RateCustomer_Distance.FirstOrDefault(rcd => rcd.DistanceId == distanceModel.NextDistanceId);

                    var relevantPreviousDistanceDetail = db.RateCustomer_Distance.FirstOrDefault(rcd => rcd.DistanceId == distanceModel.PreviousDistanceId);

                    if (relevantDistanceDetail != null)
                    {
                        relevantDistanceDetail.DistanceFrom = distanceModel.CurrentDistanceFrom;
                        relevantDistanceDetail.DistanceTo = distanceModel.CurrentDistanceTo;
                        relevantDistanceDetail.Amount = distanceModel.CurrentAmount;
                        //relevantDistanceDetail.IsDefault = distanceModel.CurrentIsDefault;

                        db.SaveChanges();
                    }

                    if (relevantNextDistanceDetail != null)
                    {
                        relevantNextDistanceDetail.DistanceFrom = distanceModel.CurrentDistanceTo;

                        db.SaveChanges();
                    }

                    if (relevantPreviousDistanceDetail != null)
                    {
                        relevantPreviousDistanceDetail.DistanceTo = distanceModel.CurrentDistanceFrom;

                        db.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return false;
        }

        public bool DeleteDistanceDetail(RateCustomerDistanceForDelete distanceModel)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var relevantDistanceDetail = db.RateCustomer_Distance.FirstOrDefault(rcd => rcd.DistanceId == distanceModel.CurrentDistanceId);

                    if (relevantDistanceDetail != null && ((distanceModel.CurrentDistanceFrom != 0 && !distanceModel.CurrentIsDefault) || (distanceModel.CurrentDistanceFrom == 0 && distanceModel.CurrentDistanceTo == 0)))
                    {
                        relevantDistanceDetail.IsDeleted = true;
                        relevantDistanceDetail.ModifiedDate = DateTime.UtcNow;

                        db.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return false;
        }

        public bool CreateDistanceDetail(RateCustomerDistanceForCreate distanceModel)
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    RateCustomer_Distance newRateCustomer_Distance = new RateCustomer_Distance()
                    {
                        DayId = distanceModel.CurrentRateCustomerDistanceDayId,
                        DistanceFrom = distanceModel.CurrentDistanceFrom,
                        DistanceTo = distanceModel.CurrentDistanceTo,
                        Amount = distanceModel.CurrentAmount,
                        IsDefault = distanceModel.CurrentIsDefault,
                        Unit = distanceModel.CurrentUnit,
                        CreatedDate = DateTime.UtcNow,
                        ModifiedDate = DateTime.UtcNow,
                        IsDeleted = false
                    };

                    db.RateCustomer_Distance.Add(newRateCustomer_Distance);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return false;
        }

        public long CreateRateVehicle(RateCustomerVehicleForCreate rateVehicle)
        {

            try
            {
                using (var db = new BATransferEntities())
                {
                    Rate_Customer_VehicleType newRate_Customer_VehicleType = new Rate_Customer_VehicleType()
                    {
                        RateId = rateVehicle.RateId,
                        VehicletypeId = rateVehicle.VehicleTypeId,
                        CustomerRateFormulaId = null,
                        DriverRateFormulaId = null,
                        FixedPriceFromula = string.Empty,
                        IsDefault = false,
                        IsDelete = false,
                        CreatedDate = DateTime.UtcNow,
                        ModifiedDate = DateTime.UtcNow
                    };

                    db.Rate_Customer_VehicleType.Add(newRate_Customer_VehicleType);
                    db.SaveChanges();

                    return newRate_Customer_VehicleType.RateCustomerVehicleTypeId;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return 0;
        }
        public long createRateVehicleDay(RateCustomerVehicleDayForCreate rateVehicleDay)
        {

            try
            {
                using (var db = new BATransferEntities())
                {
                    RateCustomer_Distance_Day newRateCustomer_Distance_Day = new RateCustomer_Distance_Day()
                    {
                        RateCustomerVehicleTypeId = rateVehicleDay.RateCustomerVehicleTypeId,
                        Day = rateVehicleDay.Day,
                        IsDefault = rateVehicleDay.Day == 0 ? true : false,
                        IsDefaultTime = true,
                        StartTime = TimeSpan.Parse("00:00:00"),
                        EndTime = TimeSpan.Parse("23:59:00"),
                        Flag = 0,
                        Plot = "ALLPLOT",
                        IsDeleted = false,
                        CreatedDate = DateTime.UtcNow,
                        ModifiedDate = DateTime.UtcNow
                    };

                    db.RateCustomer_Distance_Day.Add(newRateCustomer_Distance_Day);
                    //db.SaveChanges();

                    if (rateVehicleDay.DistanceDetail != null && rateVehicleDay.DistanceDetail.Any())
                    {
                        foreach (var distanceDetail in rateVehicleDay.DistanceDetail)
                        {
                            RateCustomer_Distance newRateCustomer_Distance = new RateCustomer_Distance()
                            {
                                DayId = newRateCustomer_Distance_Day.DayId,
                                DistanceFrom = distanceDetail.DistanceFrom,
                                DistanceTo = distanceDetail.DistanceTo,
                                Amount = distanceDetail.Amount,
                                IsDefault = ((distanceDetail.DistanceFrom == 0) && (distanceDetail.DistanceTo != 0)) ? true : false,
                                Unit = distanceDetail.Unit,
                                CreatedDate = DateTime.UtcNow,
                                ModifiedDate = DateTime.UtcNow,
                                IsDeleted = false

                            };


                            newRateCustomer_Distance_Day.RateCustomer_Distance.Add(newRateCustomer_Distance);
                            //db.RateCustomer_Distance.Add(newRateCustomer_Distance);

                        }

                    }

                    db.SaveChanges();

                    return rateVehicleDay.RateCustomerVehicleTypeId; //newRateCustomer_Distance_Day.DayId;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return 0;
        }
        public string GetRateNameByRateId(long rateId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var relevantCustomerRate = db.Rate_Customer.FirstOrDefault(rc => rc.RateId == rateId);

                    if (relevantCustomerRate != null)
                    {
                        return relevantCustomerRate.RateName;
                    }
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return string.Empty;
        }

        public string GetVehicleTypeNameByRateCustomerVehicleTypeId(long rateCustomerVehicleTypeId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var relevantCustomerRate = db.Rate_Customer_VehicleType.FirstOrDefault(rc => rc.RateCustomerVehicleTypeId == rateCustomerVehicleTypeId);

                    if (relevantCustomerRate != null)
                    {
                        if (relevantCustomerRate.VehicleType != null)
                        {
                            return relevantCustomerRate.VehicleType.VehicleTypeName;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return string.Empty;
        }

        public List<RateDailySurge> GetDailySurgeForCustomerRate(long rateId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    var distinctDays = db.DailySurges.Where(rcv => rcv.CustomerRateId == rateId).Select(rcvv => rcvv.Day).Distinct();
                    //List<RateDailySurge> relevantVehicleTypes = db.DailySurges.Where(rcv => rcv.CustomerRateId == rateId).ToList().Select(rcvv => new RateDailySurge()
                    List<RateDailySurge> relevantVehicleTypes = distinctDays.ToList().Select(rcvv => new RateDailySurge()
                    {
                        //DailySurgeId = rcvv.DailySurgeId,
                        RateId = rateId,
                        DayId = rcvv,
                        DayName = Enum.GetName(typeof(EnumCustomerRateDay), rcvv)
                    }).ToList();

                    //var subResults = relevantVehicleTypes.GroupBy(rrr => rrr.DayId).Distinct();

                    return relevantVehicleTypes;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return new List<RateDailySurge>();
        }

        public List<RateDailySurgeDayDom> GetRateDailySurgeDayDetail(long rateId, int dayId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    List<RateDailySurgeDayDom> relevantCustomerDistanceDays = db.DailySurges.Where(rcv => rcv.CustomerRateId == rateId && rcv.Day == dayId).Select(rcvv => new RateDailySurgeDayDom()
                    {

                        DailySurgeId = rcvv.DailySurgeId,
                        DayId = rcvv.Day,
                        DayName = rcvv.Day == 1 ? "Monday" :
                                  rcvv.Day == 2 ? "Tuesday" :
                                  rcvv.Day == 2 ? "Wednesday" :
                                  rcvv.Day == 2 ? "Thursday" :
                                  rcvv.Day == 2 ? "Friday" :
                                  rcvv.Day == 2 ? "Saturday" :
                                  rcvv.Day == 2 ? "Sunday" : "Default",
                        StartTime = rcvv.StartTime,
                        EndTime = rcvv.EndTime,
                        IsPercentageEnabled = rcvv.IsPercentageEnabled,
                        SurgeAmount = rcvv.SurgeAmount,
                        SurgePercentage = rcvv.SurgePercentage
                        //RateCustomerDistanceRecords = rcvv.RateCustomer_Distance.Where(rcc => !rcc.IsDeleted).Select(rcd => new RateCustomerDistanceDom() {
                        //    RateCustomerDistanceDayId = rcvv.DayId,
                        //    DistanceId = rcd.DistanceId,
                        //    DistanceFrom = rcd.DistanceFrom,
                        //    DistanceTo = rcd.DistanceTo,
                        //    Amount = rcd.Amount,
                        //    Unit = rcd.Unit,
                        //    IsDefault = rcd.IsDefault

                        //}).ToList()
                        //RateCustomerVehicleTypeId = rcvv.RateCustomerVehicleTypeId,
                        //RateId = rcvv.RateId,
                        //VehicleTypeId = rcvv.VehicletypeId,
                        //VehicleTypeName = rcvv.VehicleType.VehicleTypeName
                    }).ToList();

                    return relevantCustomerDistanceDays;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return new List<RateDailySurgeDayDom>();
        }

        public RateDailySurgeDayDom GetRateDailySurgeDetail(long dailySurgeId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    //var dailySurge = db.DailySurges.FirstOrDefault(rcv => rcv.DailySurgeId == dailySurgeId && !rcv.IsDeleted && rcv.IsEnabled);
                    var dailySurge = db.DailySurges.FirstOrDefault(rcv => rcv.DailySurgeId == dailySurgeId && !rcv.IsDeleted);

                    if (dailySurge != null)
                    {
                        RateDailySurgeDayDom relevantDailySurge = new RateDailySurgeDayDom()
                        {
                            DailySurgeId = dailySurge.DailySurgeId,
                            CustomerRateId = dailySurge.CustomerRateId,
                            CustomerRateName = RateHelper.GetRateName(dailySurge.CustomerRateId),
                            DayId = dailySurge.Day,
                            DayName = Enum.GetName(typeof(EnumCustomerRateDay), dailySurge.Day),
                            StartTime = dailySurge.StartTime,
                            EndTime = dailySurge.EndTime,
                            IsPercentageEnabled = dailySurge.IsPercentageEnabled,
                            SurgeAmount = dailySurge.SurgeAmount,
                            SurgePercentage = dailySurge.SurgePercentage,
                            IsActive = dailySurge.IsEnabled,
                            IsAllPlot = dailySurge.IsAllPlot
                        };

                        return relevantDailySurge;
                    }
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return new RateDailySurgeDayDom();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public bool UpdateDailySurge(RateDailySurgeDayDom request)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var relevantDailySurge = db.DailySurges.FirstOrDefault(rc => rc.DailySurgeId == request.DailySurgeId);

                    if (relevantDailySurge != null)
                    {
                        relevantDailySurge.Day = request.DayId;
                        relevantDailySurge.IsPercentageEnabled = request.IsPercentageEnabled;
                        relevantDailySurge.IsEnabled = request.IsActive;
                        relevantDailySurge.StartTime = request.StartTime;
                        relevantDailySurge.EndTime = request.EndTime;
                        relevantDailySurge.SurgeAmount = request.SurgeAmount;
                        relevantDailySurge.SurgePercentage = request.SurgePercentage;
                        relevantDailySurge.ModifiedDate = DateTime.UtcNow;
                        relevantDailySurge.IsAllPlot = request.IsAllPlot;
                        db.SaveChanges();

                        if (!request.IsAllPlot && request.AreaPointIds != null)
                        {

                            var alreadyAddedAreaPoints = db.DailySurgeAreaPoints.Where(dsap => dsap.DailySurgeId == request.DailySurgeId);

                            if (alreadyAddedAreaPoints != null && alreadyAddedAreaPoints.Any())
                            {
                                var alreadyAddedAreaPointIds = alreadyAddedAreaPoints.Select(aaa => aaa.AreaPointId).ToArray();

                                var newlyAddedAreaPointIds = request.AreaPointIds.Except(alreadyAddedAreaPointIds).ToArray();

                                //var existingAreaPointIds = request.AreaPointIds.Except(newlyAddedAreaPointIds).ToArray();

                                //var removedAreaPointIds = alreadyAddedAreaPointIds.Except(existingAreaPointIds).ToArray();

                                var removedAreaPointIds = alreadyAddedAreaPointIds.Except(request.AreaPointIds.Except(newlyAddedAreaPointIds)).ToArray();

                                if (newlyAddedAreaPointIds != null && newlyAddedAreaPointIds.Any())
                                {
                                    foreach (var id in newlyAddedAreaPointIds)
                                    {
                                        DailySurgeAreaPoint newDailySurgeAreaPoint = new DailySurgeAreaPoint()
                                        {
                                            AreaPointId = id,
                                            DailySurgeId = relevantDailySurge.DailySurgeId,
                                            IsEnabled = true,
                                            IsDeleted = false,
                                            sysCreatedDate = DateTime.UtcNow,
                                            sysModifiedDate = DateTime.UtcNow
                                        };

                                        db.DailySurgeAreaPoints.Add(newDailySurgeAreaPoint);
                                        db.SaveChanges();
                                    }
                                }

                                if (removedAreaPointIds != null && removedAreaPointIds.Any())
                                {
                                    foreach (var id in removedAreaPointIds)
                                    {
                                        var relavantDailySurgeAreaPoint = db.DailySurgeAreaPoints.FirstOrDefault(dsap => dsap.DailySurgeId == request.DailySurgeId && dsap.AreaPointId == id);

                                        if(relavantDailySurgeAreaPoint != null)
                                        {
                                            relavantDailySurgeAreaPoint.IsEnabled = false;
                                            relavantDailySurgeAreaPoint.sysModifiedDate = DateTime.UtcNow;
                                            db.SaveChanges();
                                        }
                                    }
                               }
                            }
                            else
                            {

                                foreach (var id in request.AreaPointIds)
                                {
                                    DailySurgeAreaPoint newDailySurgeAreaPoint = new DailySurgeAreaPoint()
                                    {
                                        AreaPointId = id,
                                        DailySurgeId = relevantDailySurge.DailySurgeId,
                                        IsEnabled = true,
                                        IsDeleted = false,
                                        sysCreatedDate = DateTime.UtcNow,
                                        sysModifiedDate = DateTime.UtcNow
                                    };

                                    db.DailySurgeAreaPoints.Add(newDailySurgeAreaPoint);
                                    db.SaveChanges();
                                }
                            }
                        }

                        return true;

                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return false;
        }

        public List<SelectListItem> GetAvailableDaysForDailySurge(long? rateId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    List<SelectListItem> days = new List<SelectListItem>();
                    days.Add(new SelectListItem { Value = 0.ToString(), Text = "Default" });
                    days.Add(new SelectListItem { Value = 1.ToString(), Text = "Monday" });
                    days.Add(new SelectListItem { Value = 2.ToString(), Text = "Tuesday" });
                    days.Add(new SelectListItem { Value = 3.ToString(), Text = "Wednesday" });
                    days.Add(new SelectListItem { Value = 4.ToString(), Text = "Thursday" });
                    days.Add(new SelectListItem { Value = 5.ToString(), Text = "Friday" });
                    days.Add(new SelectListItem { Value = 6.ToString(), Text = "Saturday" });
                    days.Add(new SelectListItem { Value = 7.ToString(), Text = "Sunday" });

                    //var addedDayRecords = db.DailySurges.Where(vt => vt.CustomerRateId == rateId && !vt.IsDeleted && vt.IsEnabled);

                    //if (addedDayRecords != null && addedDayRecords.Any())
                    //{

                    //    foreach (var record in addedDayRecords)
                    //    {
                    //        days.Remove(days.FirstOrDefault(d => d.Value == record.Day.ToString()));
                    //    }

                    //    return days;

                    //}
                    //else
                    //{
                    //    return days;
                    //}

                    return days;

                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return null;
        }

        public SelectList GetTimeList()
        {
            int hour = 0; //BritishTime.GetDateTime().Hour;
            string time = string.Empty;
            var timeList = new List<SelectListItem>();
            for (int i = hour; i < 24; i++)
            {
                for (int j = 0; j < 60; j++)
                {
                    if (j % 60 == 0)
                    {
                        time = i.ToString("00") + ":" + j.ToString("00");
                        var selectedItem = new SelectListItem();
                        selectedItem.Text = time;
                        selectedItem.Value = time;
                        timeList.Add(selectedItem);
                    }
                }

            }
            SelectList list = new SelectList(timeList, "Value", "Text");
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public bool CreateDailySurge(RateDailySurgeDayDom request)
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    DailySurge newDailySurge = new DailySurge()
                    {
                        CustomerRateId = request.CustomerRateId,
                        Day = request.DayId,
                        StartTime = request.StartTime,
                        EndTime = request.EndTime,
                        SurgePercentage = request.SurgePercentage,
                        SurgeAmount = request.SurgeAmount,
                        IsPercentageEnabled = request.IsPercentageEnabled,
                        IsDeleted = false,
                        IsEnabled = request.IsActive,
                        CreatedDate = DateTime.UtcNow,
                        ModifiedDate = DateTime.UtcNow,
                        IsAllPlot = request.IsAllPlot
                    };

                    db.DailySurges.Add(newDailySurge);
                    db.SaveChanges();

                    if (!request.IsAllPlot && request.AreaPointIds != null)
                    {
                        foreach (var id in request.AreaPointIds)
                        {
                            DailySurgeAreaPoint newDailySurgeAreaPoint = new DailySurgeAreaPoint()
                            {
                                AreaPointId = id,
                                DailySurgeId = newDailySurge.DailySurgeId,
                                IsEnabled = true,
                                IsDeleted = false,
                                sysCreatedDate = DateTime.UtcNow,
                                sysModifiedDate = DateTime.UtcNow
                            };

                            db.DailySurgeAreaPoints.Add(newDailySurgeAreaPoint);
                            db.SaveChanges();
                        }
                    }

                    return true;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return false;
        }

        public List<RateCustomerDistanceDom> GetRateVehicleTypeDistanceDetailNew(long rateCustomerDistanceDayId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    List<RateCustomerDistanceDom> relevantCustomerDistances = db.RateCustomer_Distance_FixedPrice.Where(rcv => rcv.DayId == rateCustomerDistanceDayId && !rcv.IsDeleted).Select(rcvv => new RateCustomerDistanceDom()
                    {
                        RateCustomerDistanceDayId = rcvv.DayId,
                        DistanceId = rcvv.RateCustomerDistanceFixedPriceId,
                        DistanceFrom = rcvv.DistanceFrom,
                        DistanceTo = rcvv.DistanceTo,
                        Amount = rcvv.Amount,
                        Unit = rcvv.Unit,
                        IsDefault = rcvv.IsDefault
                    }).OrderBy(rr => rr.DistanceFrom).ToList();

                    relevantCustomerDistances.Add(
                        new RateCustomerDistanceDom()
                        {
                            RateCustomerDistanceDayId = rateCustomerDistanceDayId,
                            DistanceId = 0,
                            DistanceFrom = 0,
                            DistanceTo = 0,
                            Amount = 0,
                            Unit = "mile",
                            IsDefault = false
                        }
                        );

                    return relevantCustomerDistances;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return new List<RateCustomerDistanceDom>();
        }
        public bool SaveDistanceDetailNew(RateCustomerDistanceForUpdate distanceModel)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var relevantDistanceDetail = db.RateCustomer_Distance_FixedPrice.FirstOrDefault(rcd => rcd.RateCustomerDistanceFixedPriceId == distanceModel.CurrentDistanceId);

                    var relevantNextDistanceDetail = db.RateCustomer_Distance_FixedPrice.FirstOrDefault(rcd => rcd.RateCustomerDistanceFixedPriceId == distanceModel.NextDistanceId);

                    var relevantPreviousDistanceDetail = db.RateCustomer_Distance_FixedPrice.FirstOrDefault(rcd => rcd.RateCustomerDistanceFixedPriceId == distanceModel.PreviousDistanceId);

                    if (relevantDistanceDetail != null)
                    {
                        relevantDistanceDetail.DistanceFrom = distanceModel.CurrentDistanceFrom;
                        relevantDistanceDetail.DistanceTo = distanceModel.CurrentDistanceTo;
                        relevantDistanceDetail.Amount = distanceModel.CurrentAmount;
                        //relevantDistanceDetail.IsDefault = distanceModel.CurrentIsDefault;

                        db.SaveChanges();
                    }

                    if (relevantNextDistanceDetail != null)
                    {
                        relevantNextDistanceDetail.DistanceFrom = distanceModel.CurrentDistanceTo;

                        db.SaveChanges();
                    }

                    if (relevantPreviousDistanceDetail != null)
                    {
                        relevantPreviousDistanceDetail.DistanceTo = distanceModel.CurrentDistanceFrom;

                        db.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return false;
        }

        public bool DeleteDistanceDetailNew(RateCustomerDistanceForDelete distanceModel)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var relevantDistanceDetail = db.RateCustomer_Distance_FixedPrice.FirstOrDefault(rcd => rcd.RateCustomerDistanceFixedPriceId == distanceModel.CurrentDistanceId);

                    if (relevantDistanceDetail != null && ((distanceModel.CurrentDistanceFrom != 0 && !distanceModel.CurrentIsDefault) || (distanceModel.CurrentDistanceFrom == 0 && distanceModel.CurrentDistanceTo == 0)))
                    {
                        relevantDistanceDetail.IsDeleted = true;
                        relevantDistanceDetail.ModifiedDate = DateTime.UtcNow;

                        db.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return false;
        }

        public bool CreateDistanceDetailNew(RateCustomerDistanceForCreate distanceModel)
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    RateCustomer_Distance_FixedPrice newRateCustomer_Distance = new RateCustomer_Distance_FixedPrice()
                    {
                        DayId = distanceModel.CurrentRateCustomerDistanceDayId,
                        DistanceFrom = distanceModel.CurrentDistanceFrom,
                        DistanceTo = distanceModel.CurrentDistanceTo,
                        Amount = distanceModel.CurrentAmount,
                        IsDefault = distanceModel.CurrentIsDefault,
                        Unit = distanceModel.CurrentUnit,
                        CreatedDate = DateTime.UtcNow,
                        ModifiedDate = DateTime.UtcNow,
                        IsDeleted = false
                    };

                    db.RateCustomer_Distance_FixedPrice.Add(newRateCustomer_Distance);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return false;
        }

        public long createRateVehicleDayNew(RateCustomerVehicleDayForCreate rateVehicleDay)
        {

            try
            {
                using (var db = new BATransferEntities())
                {
                    RateCustomer_Distance_Day newRateCustomer_Distance_Day = new RateCustomer_Distance_Day()
                    {
                        RateCustomerVehicleTypeId = rateVehicleDay.RateCustomerVehicleTypeId,
                        Day = rateVehicleDay.Day,
                        IsDefault = rateVehicleDay.Day == 0 ? true : false,
                        IsDefaultTime = true,
                        StartTime = TimeSpan.Parse("00:00:00"),
                        EndTime = TimeSpan.Parse("23:59:00"),
                        Flag = 0,
                        Plot = "ALLPLOT",
                        IsDeleted = false,
                        CreatedDate = DateTime.UtcNow,
                        ModifiedDate = DateTime.UtcNow
                    };

                    db.RateCustomer_Distance_Day.Add(newRateCustomer_Distance_Day);
                    //db.SaveChanges();

                    if (rateVehicleDay.DistanceDetail != null && rateVehicleDay.DistanceDetail.Any())
                    {
                        foreach (var distanceDetail in rateVehicleDay.DistanceDetail)
                        {
                            RateCustomer_Distance_FixedPrice newRateCustomer_Distance = new RateCustomer_Distance_FixedPrice()
                            {
                                DayId = newRateCustomer_Distance_Day.DayId,
                                DistanceFrom = distanceDetail.DistanceFrom,
                                DistanceTo = distanceDetail.DistanceTo,
                                Amount = distanceDetail.Amount,
                                IsDefault = ((distanceDetail.DistanceFrom == 0) && (distanceDetail.DistanceTo != 0)) ? true : false,
                                Unit = distanceDetail.Unit,
                                CreatedDate = DateTime.UtcNow,
                                ModifiedDate = DateTime.UtcNow,
                                IsDeleted = false

                            };


                            newRateCustomer_Distance_Day.RateCustomer_Distance_FixedPrice.Add(newRateCustomer_Distance);
                            //db.RateCustomer_Distance.Add(newRateCustomer_Distance);

                        }

                    }

                    db.SaveChanges();

                    return rateVehicleDay.RateCustomerVehicleTypeId; //newRateCustomer_Distance_Day.DayId;
                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return 0;
        }
    }

}