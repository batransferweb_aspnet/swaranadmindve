﻿using Newtonsoft.Json;
using SwaranAdmin_New.Autocab;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.WebPages;

namespace SwaranAdmin_New.DataAccess
{
    public class ReservationDal
    {
        #region Singalton
        private static ReservationDal _instance;
        private static readonly object LockThis = new object();
        protected ReservationDal()
        {
        }
        public static ReservationDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new ReservationDal());
                }
            }
        }
        #endregion
        public List<ReservationViewModel> GetResrvation(string userid)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<ReservationViewModel> objReservation =
                        db.sp_swaran_reservation_booking_history(userid).ToList().Select(x => new ReservationViewModel()
                        {
                            BookingFrom = x.BookingFrom,
                            BookingNumber = x.BookingNumber,
                            BookingTo = x.BookingTo,
                            BookingToken = x.BookingToken,
                            Status = x.Status,
                            Journeydate = x.Journeydate.ToString("dd-MMM-yy HH:mm"),
                            StatusId = x.StatusId
                        });
                    return objReservation.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public List<ReservationHistoryViewModel> GetResrvationByStatus(int bookingStatus)
        {
            try
            {

                using (var db = new BATransferEntities())
                {
                    //         @PAYMENTSTATUS INT,
                    //         @DATE DATETIME,
                    //@BOOKINGSTATUS INT,
                    //         @COMPANYID BIGINT,
                    //@LOYALTYTYPE INT,
                    //         @ORGANIZATIONID INT
                    var today = BritishTime.GetDateTime();
                    IEnumerable<ReservationHistoryViewModel> objReservation =
                        db.sp_swaran_search_by_bookingstatus(bookingStatus, today).ToList().Select(x => new ReservationHistoryViewModel()
                        {
                            BookingId = x.BookingId,
                            BookingToken = HttpContext.Current.Server.UrlEncode(x.BookingToken),
                            Status = x.Status,
                            BookingSource = x.BookingSource,
                            BookingNumber = x.BookingNumber,
                            CreatedDate = x.CreatedDate != null ? x.CreatedDate.Value.ToString("dd-MMM-yy HH:mm") : "",
                            BookingDateTime = x.BookingDateTime.ToString("dd-MMM-yy HH:mm"),
                            JourneyType = x.JourneyType,
                            BookingFrom = x.BookingFrom,
                            BookingTo = x.BookingTo,
                            PaymentType = x.PaymentType,
                            Timeleft = x.Timeleft,
                            BookedPersonName = x.IsCABOperator ?? false ? "(CAB OP) " + x.BookedPersonName : x.BookedPersonName,
                            LoyaltyType = x.LoyaltyType,
                            Country = x.Country,
                            VehicleTypes = x.VehicleTypes,
                            FromPostcode = x.BookingFromFullPostCode,
                            ToPostcode = x.BookingToFullPostCode,
                            IsReturnBooking = x.IsReturnBooking,
                            AutocabNumber = x.AutocabNumber,
                            IsSentToGhost = x.IsSentToGhost ?? false,
                            RemainingHours = x.RemainingHours ?? 0,
                            PaymentStatus = x.PaymentStatus,
                            PaymentTypeId = x.PaymentTypeId,
                            IsVehicleUpgradeEnabeld = x.IsVehicleUpgradeEnabeld ?? false,
                            IsCABOperator = x.IsCABOperator ?? false,

                        });
                    return objReservation.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public List<ReservationHistoryViewModel> GetUserResrvationByStatus(string userid, int bookingStatus)
        {
            try
            {

                using (var db = new BATransferEntities())
                {
                    //         @PAYMENTSTATUS INT,
                    //         @DATE DATETIME,
                    //@BOOKINGSTATUS INT,
                    //         @COMPANYID BIGINT,
                    //@LOYALTYTYPE INT,
                    //         @ORGANIZATIONID INT
                    var today = BritishTime.GetDateTime();
                    IEnumerable<ReservationHistoryViewModel> objReservation =
                        db.sp_swaran_search_by_bookingstatus_user(bookingStatus, userid, today).ToList().Select(x => new ReservationHistoryViewModel()
                        {
                            BookingId = x.BookingId,
                            BookingToken = HttpContext.Current.Server.UrlEncode(x.BookingToken),
                            Status = x.Status,
                            BookingSource = x.BookingSource,
                            BookingNumber = x.BookingNumber,
                            CreatedDate = x.CreatedDate != null ? x.CreatedDate.Value.ToString("dd-MMM-yy HH:mm") : "",
                            BookingDateTime = x.BookingDateTime.ToString("dd-MMM-yy HH:mm"),
                            JourneyType = x.JourneyType,
                            BookingFrom = x.BookingFrom,
                            BookingTo = x.BookingTo,
                            PaymentType = x.PaymentType,
                            Timeleft = x.Timeleft,
                            BookedPersonName = x.BookedPersonName,
                            LoyaltyType = x.LoyaltyType,
                            Country = x.Country,
                            VehicleTypes = x.VehicleTypes,
                            FromPostcode = x.BookingFromFullPostCode,
                            ToPostcode = x.BookingToFullPostCode,
                            IsReturnBooking = x.IsReturnBooking,
                            AutocabNumber = x.AutocabNumber,
                            IsSentToGhost = x.IsSentToGhost ?? false,
                            RemainingHours = x.RemainingHours ?? 0,
                            PaymentStatus = x.PaymentStatus,
                            PaymentTypeId = x.PaymentTypeId


                        });
                    return objReservation.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }
        public bool GetFreejourneycode(string userId, string code)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var isvalid = db.CustomerFreeJourneys.Any(x => x.UserId.Equals(userId) && x.Code.Equals(code) && !x.IsUsed && x.IsEnabled);
                    return isvalid;
                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }

        public List<ReservationMonthlyChartViewModel> GetMonthlyBooking(ReservationDateRange range)
        {
            try
            {
                var stDate = DateTime.Parse(range.startdate);
                var enDate = DateTime.Parse(range.enddate);
                using (var db = new BATransferEntities())
                {
                    //IEnumerable<ReservationMonthlyChartViewModel> objChart =
                    //    db.sp_admin_swaran_monthly_booking(stDate, enDate)
                    //        .ToList()
                    //        .Select(x => new ReservationMonthlyChartViewModel()
                    //        {
                    //            //BookingDate = x.BookingDate != null ? x.BookingDate.Value.ToString("dd/MM/yy") : "",
                    //            BookingDate = x.BookingDate,
                    //            BookingCount = x.BookingCount ?? 0
                    //        });

                    //return objChart.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }
        #region Edit Booking 
        public CurrentBookingDisplayViewModel GetEditBooking(string bookingToken, bool isreturnBooking)
        {
            var currentBooking = new CurrentBookingDisplayViewModel();
            currentBooking.WorldPayReference = string.Empty;
            Booking objBookingReturn = null;
            try
            {
                // bookingToken = "ACV7kZ+VIvLq8C9k7R9b6qbrOEr8Gd+ORwe6mlImtNpnaQOikgtapFhlwROWqJafnA==";
                using (var db = new BATransferEntities())
                {
                    var objBooking = new Booking();
                    var onewayBooking = new Booking();

                    decimal returnWayPrice = 0;

                    objBooking = db.Bookings.FirstOrDefault(x => x.BookingToken.Equals(bookingToken) && x.IsReturnBooking == isreturnBooking);


                    if (objBooking != null)
                    {
                        currentBooking.IsCancelBooking = objBooking.Status ==
                                                            (int)BookingStatusEnum.Cancelled;
                        if (objBooking.IsSourceBooking)
                        {
                            objBookingReturn =
                                db.Bookings.FirstOrDefault(
                                    bt => bt.BookingId == objBooking.ReferenceBookingId && bt.IsReturnBooking);
                            if (objBookingReturn != null && (objBookingReturn.Status == (int)BookingStatusEnum.Requested || objBookingReturn.Status == (int)BookingStatusEnum.Confirmed))
                            {
                                returnWayPrice = objBookingReturn.TotalDistancePrice;
                            }

                        }

                        if (objBooking.IsReturnBooking)
                        {
                            onewayBooking =
                                db.Bookings.FirstOrDefault(
                                    bt => bt.BookingId == objBooking.ReferenceBookingId && !bt.IsReturnBooking);

                        }

                        if (!isreturnBooking)
                            currentBooking.MainBooking = EditBooking(objBooking, objBookingReturn);
                        else
                            currentBooking.MainBooking = EditBooking(onewayBooking, objBooking);

                        currentBooking.BookingToken = HttpContext.Current.Server.UrlEncode(objBooking.BookingToken);
                        currentBooking.BookedPersonEmail = objBooking.BookedPersonEmail;
                        currentBooking.BookingStatus = objBooking.Status;
                        currentBooking.CardPaymentType = objBooking.CardPaymentTypeId ?? 0;
                        currentBooking.PaymentMethodId = objBooking.PaymentMethod;
                        currentBooking.PaymentStatus = objBooking.PaymentStatus;
                        currentBooking.AutocabNumber = objBooking.AutocabNumber;
                        currentBooking.UserId = objBooking.UserId;
                        currentBooking.OneWayPrice = isreturnBooking ? onewayBooking.TotalDistancePrice : objBooking.TotalDistancePrice;
                        currentBooking.ReturnWayPrice = isreturnBooking ? objBooking.TotalDistancePrice : returnWayPrice;
                        currentBooking.JourneyTypeId = objBooking.JourneyType;
                        currentBooking.AutocabAuthorizationReference = objBooking.Autocab_AuthorizationReference;
                        currentBooking.IsSourceBooking = objBooking.IsSourceBooking;
                        currentBooking.IsReturnBooking = objBooking.IsReturnBooking;
                        currentBooking.SiteId = OrganizationHelper.GetRelevantOrganizationFromSite(objBooking.BookingSource);
                        currentBooking.OtherDiscounts = objBooking.OtherDiscount;
                    }



                }

            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return currentBooking;
        }
        private string EditBooking(Booking booking, Booking returnJourney)
        {
            try
            {

                string oneWayBookingStatus = booking != null && booking.Status > 0
                                            ? Enum.GetName(typeof(BookingStatusEnum), booking.Status)
                                            : "";
                string returnBookingStatus = returnJourney != null && returnJourney.Status > 0
                                          ? Enum.GetName(typeof(BookingStatusEnum), returnJourney.Status)
                                          : "";
                StringBuilder builder = new StringBuilder();
                string logFilePathHeader = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/Header.txt"));

                //LOGO-URL
                string textHeader = System.IO.File.ReadAllText(logFilePathHeader);
                string logoUrl = ConfigHelper.GetWebLogoSource(booking.BookingSource);
                textHeader = textHeader.Replace("LOGO-URL", logoUrl);
                textHeader = textHeader.Replace("BOOKINGID", booking.BookingNumber);
                builder.Append(textHeader);

                #region VEHICLE UPGRADE
                if (booking.IsVehicleUpgradeEnabeld ?? false)
                {
                    string currentVehicle = string.Empty;
                    string upgradeVehicle = string.Empty;

                    if (booking.BookingVehicleUpgradeHistories != null && booking.BookingVehicleUpgradeHistories.Count > 0)
                    {
                        currentVehicle = booking.BookingVehicleUpgradeHistories.First().VehicleType.VehicleTypeName;
                        upgradeVehicle = booking.BookingVehicleUpgradeHistories.First().VehicleType1.VehicleTypeName;
                    }


                    if (booking.IsUpgradeByPoints ?? false)
                    {
                        string filePathVehicleUpgrade =
                            System.Web.HttpContext.Current.Server.MapPath(
                                string.Format("~/EmailTemplets/BookingWebView/VehicleUpgradeByPoints.txt"));

                        //LOGO-URL
                        string textVehicleUpgradeByPoints = System.IO.File.ReadAllText(filePathVehicleUpgrade);

                        textVehicleUpgradeByPoints = textVehicleUpgradeByPoints.Replace("CURRENTVEHICLE", currentVehicle);
                        textVehicleUpgradeByPoints = textVehicleUpgradeByPoints.Replace("UPGRADEVEHICLE", upgradeVehicle);
                        textVehicleUpgradeByPoints = textVehicleUpgradeByPoints.Replace("POINTS", booking.UsedPoints.ToString());
                        string updateText = booking.JourneyType == (int)JourneyTypeEnum.TwoWay && !booking.IsReturnBooking
                                           ? "(Vehicle upgrade for oneway)"
                                           : "";
                        textVehicleUpgradeByPoints = textVehicleUpgradeByPoints.Replace("UPGRADEINFO", updateText);
                        textVehicleUpgradeByPoints = textVehicleUpgradeByPoints.Replace("GHOSTID", booking.AutocabNumber);
                        builder.Append(textVehicleUpgradeByPoints);
                    }




                    if (!booking.IsUpgradeByPoints ?? true)
                    {
                        string filePathVehicleUpgrade =
                            System.Web.HttpContext.Current.Server.MapPath(
                                string.Format("~/EmailTemplets/BookingWebView/VehicleUpgradeByAmount.txt"));

                        //LOGO-URL
                        string textVehicleUpgradeAmt = System.IO.File.ReadAllText(filePathVehicleUpgrade);

                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("CURRENTVEHICLE", currentVehicle);
                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("UPGRADEVEHICLE", upgradeVehicle);
                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("AMOUNT", String.Format("{0:0.00}", booking.VehicleUpgradeAmt));
                        string paymentStatus = (booking.IsVehicleUpgradePaid ?? false) ? "Upgrade Amount Paid" : "Upgrade Amount Online Payment Pending";
                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("STATUS", paymentStatus);
                        string updateText = booking.JourneyType == (int)JourneyTypeEnum.TwoWay && !booking.IsReturnBooking
                                        ? "(Vehicle upgrade for oneway)"
                                        : "";
                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("UPGRADEINFO", updateText);
                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("GHOSTID", booking.AutocabNumber);
                        builder.Append(textVehicleUpgradeAmt);
                    }


                }

                if (returnJourney != null && (returnJourney.IsVehicleUpgradeEnabeld ?? false))
                {
                    string currentVehicleReturn = string.Empty;
                    string upgradeVehicleReturn = string.Empty;

                    if (returnJourney.BookingVehicleUpgradeHistories != null && returnJourney.BookingVehicleUpgradeHistories.Count > 0)
                    {
                        if(returnJourney.BookingVehicleUpgradeHistories.First().VehicleType != null)
                        currentVehicleReturn = returnJourney.BookingVehicleUpgradeHistories.First().VehicleType.VehicleTypeName;
                        if (returnJourney.BookingVehicleUpgradeHistories.First().VehicleType1 != null)
                            upgradeVehicleReturn = returnJourney.BookingVehicleUpgradeHistories.First().VehicleType1.VehicleTypeName;
                    }

                    if (returnJourney.IsUpgradeByPoints ?? false)
                    {
                        string filePathVehicleUpgrade =
                            System.Web.HttpContext.Current.Server.MapPath(
                                string.Format("~/EmailTemplets/BookingWebView/VehicleUpgradeByPoints.txt"));

                        //LOGO-URL
                        string textVehicleUpgradeByPointsReturn = System.IO.File.ReadAllText(filePathVehicleUpgrade);

                        textVehicleUpgradeByPointsReturn = textVehicleUpgradeByPointsReturn.Replace("CURRENTVEHICLE", currentVehicleReturn);
                        textVehicleUpgradeByPointsReturn = textVehicleUpgradeByPointsReturn.Replace("UPGRADEVEHICLE", upgradeVehicleReturn);
                        textVehicleUpgradeByPointsReturn = textVehicleUpgradeByPointsReturn.Replace("POINTS", returnJourney.UsedPoints.ToString());
                        string updateText = returnJourney.JourneyType == (int)JourneyTypeEnum.TwoWay && returnJourney.IsReturnBooking
                                      ? "(Vehicle upgrade for returnway)"
                                      : "";
                        textVehicleUpgradeByPointsReturn = textVehicleUpgradeByPointsReturn.Replace("UPGRADEINFO", updateText);
                        textVehicleUpgradeByPointsReturn = textVehicleUpgradeByPointsReturn.Replace("GHOSTID", returnJourney.AutocabNumber);
                        builder.Append(textVehicleUpgradeByPointsReturn);
                    }

                    if (!returnJourney.IsUpgradeByPoints ?? true)
                    {
                        string filePathVehicleUpgrade =
                            System.Web.HttpContext.Current.Server.MapPath(
                                string.Format("~/EmailTemplets/BookingWebView/VehicleUpgradeByAmount.txt"));

                        //LOGO-URL
                        string textVehicleUpgradeAmtReturn = System.IO.File.ReadAllText(filePathVehicleUpgrade);

                        textVehicleUpgradeAmtReturn = textVehicleUpgradeAmtReturn.Replace("CURRENTVEHICLE", currentVehicleReturn);
                        textVehicleUpgradeAmtReturn = textVehicleUpgradeAmtReturn.Replace("UPGRADEVEHICLE", upgradeVehicleReturn);
                        textVehicleUpgradeAmtReturn = textVehicleUpgradeAmtReturn.Replace("AMOUNT", String.Format("{0:0.00}", returnJourney.VehicleUpgradeAmt));
                        string paymentStatus = (returnJourney.IsVehicleUpgradePaid ?? false) ? "Upgrade Amount Paid" : "Upgrade Amount Online Payment Pending";
                        textVehicleUpgradeAmtReturn = textVehicleUpgradeAmtReturn.Replace("STATUS", paymentStatus);
                        string updateText = returnJourney.JourneyType == (int)JourneyTypeEnum.TwoWay && returnJourney.IsReturnBooking
                                     ? "(Vehicle upgrade for returnway)"
                                     : "";
                        textVehicleUpgradeAmtReturn = textVehicleUpgradeAmtReturn.Replace("UPGRADEINFO", updateText);
                        textVehicleUpgradeAmtReturn = textVehicleUpgradeAmtReturn.Replace("GHOSTID", returnJourney.AutocabNumber);
                        builder.Append(textVehicleUpgradeAmtReturn);
                    }

                }
                #endregion


                if (!string.IsNullOrEmpty(booking.PassengerName))
                {
                    string logFilePathLeadPassanger =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/BookingWebView/LeadPassangerDetails.txt"));

                    //LOGO-URL
                    string textPassanger = System.IO.File.ReadAllText(logFilePathLeadPassanger);

                    textPassanger = textPassanger.Replace("PASSENGERNAME", booking.PassengerName);
                    textPassanger = textPassanger.Replace("PASSENGEREMAIL", booking.PassengerEmail);
                    textPassanger = textPassanger.Replace("PASSENGERMOBILE", booking.PassengerTel);
                    builder.Append(textPassanger);
                }

                string logFilePathMember = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/MemberDetails.txt"));

                //LOGO-URL
                string textMember = System.IO.File.ReadAllText(logFilePathMember);

                textMember = textMember.Replace("BOOKINGPERSONNAME", booking.BookedPersonName);
                textMember = textMember.Replace("BOOKINGPERSONMOBILE", booking.BookedPersonTel);
                if (!string.IsNullOrEmpty(booking.AlternativePhoneNumber))
                    textMember = textMember.Replace("BOOKINGPERSONALTERNATIVE", booking.AlternativePhoneNumber);
                else
                    textMember = textMember.Replace("BOOKINGPERSONALTERNATIVE", "--");
                textMember = textMember.Replace("BOOKINGPERSONEMAIL", booking.BookedPersonEmail);
                if (!string.IsNullOrEmpty(booking.UserId))
                    textMember = textMember.Replace("MEMBER-ID", booking.AspNetUser.MemberId.ToString());
                else
                    textMember = textMember.Replace("MEMBER-ID", "NOT FOUND");
                builder.Append(textMember);


                string logFilePath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/EmailMainBody.txt"));
                string text = System.IO.File.ReadAllText(logFilePath);
                string paymentType = booking.PaymentMethod == (int)PaymentTypeEnum.Cash
                    ? "Cash"
                    : booking.PaymentMethod == (int)PaymentTypeEnum.Card
                        ? "Card"
                        : booking.PaymentMethod == (int)PaymentTypeEnum.Bank_Transfer
                            ? "Bank Transfer"
                            : booking.PaymentMethod == (int)PaymentTypeEnum.Free_Journey
                                ? "Free Journey"
                                : booking.PaymentMethod == (int)PaymentTypeEnum.Promo_Code
                                    ? "Promo Code"
                                    : booking.PaymentMethod == (int)PaymentTypeEnum.Voucher
                                        ? "Voucher"
                                        : booking.PaymentMethod == (int)PaymentTypeEnum.Account
                                            ? "Account"
                                            : booking.PaymentMethod == (int)PaymentTypeEnum.Use_My_Credits
                                                ? "Used My Credits"
                                                : "";
                text = text.Replace("BOOKINGID", booking.BookingNumber);
                text = text.Replace("RESERVATIONDATETIME", booking.ReservationDateTime.ToString());
                if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay && returnJourney != null)
                {
                    text = text.Replace("BOOKINGAMOUNT", (booking.FinalPrice + returnJourney.FinalPrice).ToString());
                }
                else
                {
                    text = text.Replace("BOOKINGAMOUNT", booking.FinalPrice.ToString());
                }
                text = text.Replace("BOOKINGPAYMENTMETHOD", paymentType);
                if (booking.CardPaymentTypeId == (int)CardPaymentTypeEnum.PayOverThePhone)
                {
                    text = text.Replace("PAYMENTOVERTHEPHONE",
                        "<span>Pay by Credit/Debit card over the phone to the Office (Call +44 208 900 2299)</span>");
                }
                else
                {
                    text = text.Replace("PAYMENTOVERTHEPHONE", "");

                }

                builder.Append(text);
                
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    string logFilePathOneWayBooking = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/OneWayBooking.txt"));
                    string textOneWay = System.IO.File.ReadAllText(logFilePathOneWayBooking);
                    textOneWay = textOneWay.Replace("BOOKINGDATETIME", booking.BookingDateTime.ToString());
                    textOneWay = textOneWay.Replace("BOOKINGAMOUNTONEWAY", booking.TotalDistancePrice.ToString());
                    textOneWay = textOneWay.Replace("BOOKING-STATUS", oneWayBookingStatus);
                    if (booking.PaymentMethod == (int)PaymentTypeEnum.Free_Journey && (booking.TotalDistancePrice - booking.FreeJourneyAccessAmt) > 0)
                    {
                        textOneWay = textOneWay.Replace("FREEJOURNEYACCESSDIV",
                           "<p>Minimum Freejourney Amount  <span style='float:right;'>:</span><br/></p><p>Balance Pay Amount   <span style='float:right;'>:</span></p> ");
                        textOneWay = textOneWay.Replace("FREEJOURNEYACCESSAMTDIV", "<p> &#163; " + booking.FreeJourneyAccessAmt?.ToString("0.00") + "</p><p>&#163; " + (booking.TotalDistancePrice - booking.FreeJourneyAccessAmt)?.ToString("0.00") + "</p>");
                    }
                    else
                    {
                        textOneWay = textOneWay.Replace("FREEJOURNEYACCESSDIV", "");
                        textOneWay = textOneWay.Replace("FREEJOURNEYACCESSAMTDIV", "");
                    }
                    builder.Append(textOneWay);
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    string logFilePathAirportDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/AirportDetails.txt"));
                    string textAirport = System.IO.File.ReadAllText(logFilePathAirportDetails);
                    textAirport = textAirport.Replace("FLIGHTNUMBER", booking.FlightNo);
                    textAirport = textAirport.Replace("FLIGHTLANDINGDATETIME", booking.FlightLandingDateTime.ToString());
                    textAirport = textAirport.Replace("FLIGHTDISPLAYNAME", booking.DriverDisplayName);
                    textAirport = textAirport.Replace("FLIGHTLANDINGDRIVERELLAPSETIME", booking.DriverEllapseTime.ToString());
                    textAirport = textAirport.Replace("BOOKINGAMOUNTONEWAY", booking.TotalDistancePrice.ToString());
                    textAirport = textAirport.Replace("PICKUPCHARGE", booking.PickupCharge.ToString());
                    textAirport = textAirport.Replace("BOOKING-STATUS", oneWayBookingStatus);
                    if (booking.PaymentMethod == (int)PaymentTypeEnum.Free_Journey && (booking.TotalDistancePrice - booking.FreeJourneyAccessAmt) > 0)
                    {
                        textAirport = textAirport.Replace("FREEJOURNEYACCESSDIV",
                           "<p>Minimum Freejourney Amount  <span style='float:right;'>:</span><br/></p><p>Balance Pay Amount   <span style='float:right;'>:</span></p> ");
                        textAirport = textAirport.Replace("FREEJOURNEYACCESSAMTDIV", "<p> &#163; " + booking.FreeJourneyAccessAmt?.ToString("0.00") + "</p><p>&#163; " + (booking.TotalDistancePrice - booking.FreeJourneyAccessAmt)?.ToString("0.00") + "</p>");
                    }
                    else
                    {
                        textAirport = textAirport.Replace("FREEJOURNEYACCESSDIV", "");
                        textAirport = textAirport.Replace("FREEJOURNEYACCESSAMTDIV", "");
                    }
                    builder.Append(textAirport);
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    string logFilePathCruisePortDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/CruisePortDetails.txt"));
                    string textCruiseport = System.IO.File.ReadAllText(logFilePathCruisePortDetails);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPNUMBER", booking.FlightNo);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPLANDINGDATETIME", booking.FlightLandingDateTime.ToString());
                    textCruiseport = textCruiseport.Replace("CRUISESHIPDISPLAYNAME", booking.DriverDisplayName);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPLANDINGDRIVERELLAPSETIME", booking.DriverEllapseTime.ToString());
                    textCruiseport = textCruiseport.Replace("BOOKINGAMOUNTONEWAY", booking.TotalDistancePrice.ToString());
                    textCruiseport = textCruiseport.Replace("BOOKING-STATUS", oneWayBookingStatus);
                    builder.Append(textCruiseport);
                }
                if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay && returnJourney != null)
                {
                    if (returnJourney.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                    {
                        string logFilePathAirportReturnDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/AirportReturn.txt"));
                        string textAirportReturn = System.IO.File.ReadAllText(logFilePathAirportReturnDetails);
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_NUMBER_RETURN", returnJourney.FlightNo);
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_LANDINGDATE_TIME_RETURN", returnJourney.FlightLandingDateTime.ToString());
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_DISPLAY_NAME_RETURN", returnJourney.DriverDisplayName);
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_LANDING_DRIVER_ELLAPSE_TIME_RETURN", returnJourney.DriverEllapseTime.ToString());
                        textAirportReturn = textAirportReturn.Replace("RETURNDATETIME", returnJourney.FlightLandingDateTime.ToString());
                        textAirportReturn = textAirportReturn.Replace("BOOKINGAMOUNTTWOWAY", returnJourney.TotalDistancePrice.ToString());
                        textAirportReturn = textAirportReturn.Replace("PICKUPCHARGE", returnJourney.PickupCharge.ToString());
                        textAirportReturn = textAirportReturn.Replace("RETURN-BOOKING-STATUS", returnBookingStatus);

                        builder.Append(textAirportReturn);
                    }
                    else if (returnJourney.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                    {
                        string logFilePathCruiseportReturn = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/CruiseportReturn.txt"));
                        string textCruiseportReturn = System.IO.File.ReadAllText(logFilePathCruiseportReturn);
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_NUMBER_RETURN", returnJourney.FlightNo);
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_LANDING_DATE_TIME_RETURN", returnJourney.FlightLandingDateTime.ToString());
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_DISPLAY_NAME_RETURN", returnJourney.DriverDisplayName);
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_LANDING_DRIVER_ELLAPSE_TIME_RETURN", returnJourney.DriverEllapseTime.ToString());
                        textCruiseportReturn = textCruiseportReturn.Replace("RETURNDATETIME", returnJourney.FlightLandingDateTime.ToString());
                        textCruiseportReturn = textCruiseportReturn.Replace("BOOKINGAMOUNTTWOWAY", returnJourney.TotalDistancePrice.ToString());
                        textCruiseportReturn = textCruiseportReturn.Replace("RETURN-BOOKING-STATUS", returnBookingStatus);
                        builder.Append(textCruiseportReturn);
                    }
                    if (returnJourney.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && returnJourney.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                    {
                        string logFilePathTwoWayBooking = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/ReturnBooking.txt"));
                        string textTwoWay = System.IO.File.ReadAllText(logFilePathTwoWayBooking);
                        textTwoWay = textTwoWay.Replace("BOOKINGDATETIMERETURN", returnJourney.BookingDateTime.ToString());
                        textTwoWay = textTwoWay.Replace("BOOKINGAMOUNTTWOWAY", returnJourney.TotalDistancePrice.ToString());
                        textTwoWay = textTwoWay.Replace("RETURN-BOOKING-STATUS", returnBookingStatus);
                        builder.Append(textTwoWay);
                    }
                }

                
                if (booking.OtherCharges > 0 && booking.PaymentMethod != (int)PaymentTypeEnum.Account)
                {
                    string logFilePathAdditionalCharges =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/BookingWebView/BookingFee.txt"));
                    string additionalChargesTxt = System.IO.File.ReadAllText(logFilePathAdditionalCharges);
                    additionalChargesTxt = additionalChargesTxt.Replace("BOOKINGFEE", booking.OtherCharges.ToString());
                    additionalChargesTxt = additionalChargesTxt.Replace("PAYMENT-STATUS",
                           Enum.GetName(typeof(PaymentStatusEnum), booking.PaymentStatus));
                    additionalChargesTxt = additionalChargesTxt.Replace("COMMENT", "");
                    if (booking.PaymentMethod != (int)PaymentTypeEnum.Card)
                    {

                        string comments = "  <div style='float:left;width:100%;font-weight:600;'>" +
                                          " <p> The booking fee " + booking.OtherCharges +
                                          " is paid, Balance still needs to be paid </p>" +
                                          "</div> ";
                        if (booking.PaymentStatus == (int)PaymentStatusEnum.Pending)
                        {
                            additionalChargesTxt = additionalChargesTxt.Replace("COMMENT", comments);
                        }

                    }

                    builder.Append(additionalChargesTxt);
                }
                if (booking.ConsumerDebit > 0 || (returnJourney != null && returnJourney.ConsumerDebit > 0))
                {
                    string discountMoneyBackTxt =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/BookingWebView/DiscountMoneyBack.txt"));
                    string additionalChargesTxt = System.IO.File.ReadAllText(discountMoneyBackTxt);
                    decimal returnConsumerDebit = returnJourney != null && returnJourney.ConsumerDebit > 0
                        ? returnJourney.ConsumerDebit
                        : 0;
                    additionalChargesTxt = additionalChargesTxt.Replace("DISCOUNT", (booking.ConsumerDebit + returnConsumerDebit).ToString());
                    builder.Append(additionalChargesTxt);
                }
                if (booking.OtherDiscount > 0 || (returnJourney != null && returnJourney.OtherDiscount > 0))
                {
                    string discountMoneyBackTxt =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/BookingWebView/AppDiscount.txt"));
                    string additionalChargesTxt = System.IO.File.ReadAllText(discountMoneyBackTxt);
                    decimal returnAppDiscount = returnJourney != null && returnJourney.OtherDiscount > 0
                        ? returnJourney.OtherDiscount
                        : 0;
                    additionalChargesTxt = additionalChargesTxt.Replace("DISCOUNT", (booking.OtherDiscount + returnAppDiscount).ToString());
                    builder.Append(additionalChargesTxt);
                }

                string fileMap = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/Map.txt"));
                string textMap = System.IO.File.ReadAllText(fileMap);
                string pickupnote = "<div class='titile-area' style ='float:left; width: 100%;'>" +
                               "<h2 style='color:#0e3a7d; line-height: 0px; float: left; font-weight: 800; font-size: 15px; margin-left: 10px; text-transform:capitalize; margin-top:10px;'>Pickup Note</h2>" +
                               "</div>" +
                               "<div style='background-color:#ffd1d1;float:left;font-size:15px;line-height:5px'>" +
                               "<p style='color:#000000;line-height:1.2'>" + booking.DespatchPref + "</p>" +
                                "</div>" +
                                "</div>";

                string comment = "<div class='titile-area' style ='background-color:##ffd1d1;float:left; width: 100%;'>" +
                                 "<h2 style='color:#0e3a7d; line-height: 0px; float: left; font-weight: 800; font-size: 15px; margin-left: 10px; text-transform:capitalize; margin-top:10px;'>Customer comments</h2>" +
                                 "</div>" +
                                 "<div style='float:left;color:#000000;font-size:15px;line-height:5px'>" +
                                 "<p style='line-height:1.2'>" + booking.CustomerComment + "</p>" +
                                  "</div>" +
                                  "</div>";
                if (!string.IsNullOrEmpty(booking.DespatchPref))
                {
                    textMap = textMap.Replace("PICKUPNOTE", pickupnote);
                }
                else
                {
                    textMap = textMap.Replace("PICKUPNOTE", "");
                }

                if (!string.IsNullOrEmpty(booking.CustomerComment))
                {
                    textMap = textMap.Replace("CUSTOMERCOMMENTS", comment);
                }
                else
                {
                    textMap = textMap.Replace("CUSTOMERCOMMENTS", "");
                }
                builder.Append(textMap);

                string logFilePathAddressDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/BookingAddress.txt"));
                string addresstext = System.IO.File.ReadAllText(logFilePathAddressDetails);
                //addresstext = addresstext.Replace("CUSTOMERCOMMENTS", booking.CustomerComment);
                //addresstext = addresstext.Replace("MAPURL", booking.MapUrl);
                addresstext = addresstext.Replace("BOOKING-DATE-TIME", booking.BookingDateTime.ToString());
                addresstext = addresstext.Replace("FROM-ADDRESS", booking.BookingFrom);
                addresstext = addresstext.Replace("FROMTOWN", booking.FromTown);
                addresstext = addresstext.Replace("FROMPOSTCODE", booking.BookingFromFullPostCode);
                addresstext = addresstext.Replace("TO-ADDRESS", booking.BookingTo);
                addresstext = addresstext.Replace("TOTOWN", booking.ToTown);
                addresstext = addresstext.Replace("TOPOSTCODE", booking.BookingToFullPostCode);

                #region Via Points

                StringBuilder builderViapoints = new StringBuilder();
                int viaid = 0;
                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var journeyViapoints = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty());
                    foreach (var viapoint in journeyViapoints.ToList())
                    {
                        viaid++;
                        string via = "<div style='width:97%; float: left; font-size:15px'>" +
                                     "<div   style='float:left; width:20%; color:#666666; font-weight:600; line-height: 10px;'>" +
                                     "<p style='margin-top:10px;'>Viapoint " + viaid +
                                     "<span style='float:right;'>:</span></p>" +
                                     "</div>" +
                                     "<div class='form-right' style='color:#666666; margin-left:10px; width: 75%; float: left;'>" +
                                     "<p style='margin-top:10px;line-height: 17px;'>" + viapoint.ViaFullAddress + "</p> " +
                                     "</div>" +
                                     "</div>";
                        builderViapoints.Append(via);
                    }
                    addresstext = addresstext.Replace("VIA-POINTS", builderViapoints.ToString());
                }
                else
                {
                    addresstext = addresstext.Replace("VIA-POINTS", "");
                }

                #endregion

                builder.Append(addresstext);
                if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay && returnJourney != null)
                {
                    string logFilePathReturnBookingAddress = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/BookingAddressReturn.txt"));
                    string returnBookingTxt = System.IO.File.ReadAllText(logFilePathReturnBookingAddress);
                    if (returnJourney.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport &&
                        returnJourney.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                    {
                        returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME", returnJourney.BookingDateTime.ToString());
                    }
                    else
                    {
                        returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME", returnJourney.FlightLandingDateTime.ToString());
                    }
                    returnBookingTxt = returnBookingTxt.Replace("FROM-ADDRESS", returnJourney.BookingFrom);
                    returnBookingTxt = returnBookingTxt.Replace("FROMTOWN", returnJourney.FromTown);
                    returnBookingTxt = returnBookingTxt.Replace("FROMPOSTCODE", returnJourney.BookingFromFullPostCode);
                    returnBookingTxt = returnBookingTxt.Replace("TO-ADDRESS", returnJourney.BookingTo);
                    returnBookingTxt = returnBookingTxt.Replace("TOTOWN", returnJourney.ToTown);
                    returnBookingTxt = returnBookingTxt.Replace("TOPOSTCODE", returnJourney.BookingToFullPostCode);

                    #region Return Via Points 

                    StringBuilder builderViapointsReturn = new StringBuilder();
                    int viaidReturn = 0;
                    if (returnJourney.JourneyViapoints != null && returnJourney.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                    {
                        var journeyViapointsReturn = returnJourney.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty());
                        foreach (var viapoint in journeyViapointsReturn.ToList())
                        {
                            viaidReturn++;
                            string via = "<div style='width:97%; float: left; font-size:15px'>" +
                                         "<div   style='float:left; width:20%; color:#666666; font-weight:600; line-height: 10px;'>" +
                                         "<p style='margin-top:10px;'>Viapoint " + viaidReturn +
                                         "<span style='float:right;'>:</span></p>" +
                                         "</div>" +
                                         "<div class='form-right' style='color:#666666; margin-left:10px; width: 75%; float: left;'>" +
                                         "<p style='margin-top:10px;line-height: 17px;'>" + viapoint.ViaFullAddress + "</p> " +
                                         "</div>" +
                                         "</div>";
                            builderViapointsReturn.Append(via);
                        }
                        returnBookingTxt = returnBookingTxt.Replace("VIA-POINTS", builderViapointsReturn.ToString());
                    }
                    else
                    {
                        returnBookingTxt = returnBookingTxt.Replace("VIA-POINTS", "");
                    }

                    #endregion

                    builder.Append(returnBookingTxt);

                }
                else
                {
                    string fileClosingDiv = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/ClosingDiv.txt"));
                    string fileClosingDivTxt = System.IO.File.ReadAllText(fileClosingDiv);
                    builder.Append(fileClosingDivTxt);
                }


                string fileLuggageInfo = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/LuggageInfo.txt"));
                string textLuggage = System.IO.File.ReadAllText(fileLuggageInfo);
                textLuggage = textLuggage.Replace("BOOKINGVEHICLETYPES", string.Join(",", booking.BookingVehicleTypes.Select(x => x.VehicleType.VehicleTypeName).ToArray()));
                textLuggage = textLuggage.Replace("NUMBEROFPESSANGERS", booking.NumberOfPassengers.ToString());
                textLuggage = textLuggage.Replace("NUMBEROFBABYSEATS", (booking.BoosterSeat + booking.InfantSeats + booking.ChildSeats).ToString());
                textLuggage = textLuggage.Replace("NUMBEROFLUGGAHES", booking.Luggage.ToString());
                textLuggage = textLuggage.Replace("NUMBEROFHANDLUGGAHES", booking.HandLuggage.ToString());

                if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay && returnJourney != null)
                {
                    textLuggage = textLuggage.Replace("BOOKINGAMOUNT", (booking.FinalPrice + returnJourney.FinalPrice).ToString());
                }
                else
                {
                    textLuggage = textLuggage.Replace("BOOKINGAMOUNT", booking.FinalPrice.ToString());
                }
                builder.Append(textLuggage);
                string logFilePathFooderDetals = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/BookingWebView/FooderDetals.txt"));
                string foodertext = System.IO.File.ReadAllText(logFilePathFooderDetals);
                builder.Append(foodertext);

                return builder.ToString();

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }
        #endregion

        #region Cancel Booking
        public MainCancelBooking CancelBooking(CancelBookingViewModel cancelBooking, string userid)
        {
            var main = new MainCancelBooking();
            var token = System.Web.HttpContext.Current.Server.UrlDecode(cancelBooking.Token);
            BookingCancellationResponse autocabCancelObj = null;
            decimal cancelationFee = 0;
            try
            {
                using (var db = new BATransferEntities())
                {

                    var objBookingFixed =
                           db.Bookings.FirstOrDefault(
                               x => x.BookingToken == token && x.IsReturnBooking == cancelBooking.IsRetunBooking);
                    if (cancelBooking.ResonId == (int)BookingCancelConfigurationEnum.Cancelled_by_customer)
                    {
                        cancelationFee = GetBookingCancellationFee(objBookingFixed);
                    }
                    var objBookingTemp =
                            db.Booking_TEMP.FirstOrDefault(
                                x => x.BookingToken == token && x.IsReturnBooking == cancelBooking.IsRetunBooking);

                    if (objBookingTemp != null && objBookingTemp.Status != (int)BookingStatusEnum.Cancelled)
                    {
                        objBookingTemp.Status = (int)BookingStatusEnum.Cancelled;
                        objBookingTemp.CancelledTime = BritishTime.GetDateTime();
                        objBookingTemp.CancelBookingAmt = cancelationFee;
                        objBookingTemp.Autocab_IsBookingCancelled = false;
                        objBookingTemp.BookingCancellationRemarks = cancelBooking.Reson;
                        objBookingTemp.CancelBookingId = cancelBooking.ResonId;
                        db.SaveChanges();


                    }




                    if (objBookingFixed != null)
                    {
                        var cancelByAdmin = new BookingCancelByAdmin();
                        cancelByAdmin.BookingNumber = objBookingFixed.BookingNumber;
                        cancelByAdmin.UserId = userid;
                        cancelByAdmin.Reason = cancelBooking.Reson;
                        cancelByAdmin.CreatedDate = DateTime.Now;
                        db.BookingCancelByAdmins.Add(cancelByAdmin);
                        db.SaveChanges();


                        BookingOperation newBookingOperation = new BookingOperation()
                        {
                            BookingId = objBookingFixed.BookingId,
                            OperationId = (int)BookingStatusEnum.Cancelled,
                            OperatorUserId = userid,
                            Remarks = string.Empty,
                            sysCreatedDate = BritishTime.GetDateTime(),
                            sysModifiedDate = BritishTime.GetDateTime()
                        };
                        db.BookingOperations.Add(newBookingOperation);
                        db.SaveChanges();

                        if (objBookingFixed.Status == (int)BookingStatusEnum.Cancelled)
                        {
                            
                            main.Message = "Booking cancelled successfully";
                            main.Status = true;
                            main.StatusCode = (int)HttpStatusCode.OK;
                            return main;
                        }
                        if (objBookingFixed.Status != (int)BookingStatusEnum.Cancelled)
                        {
                            objBookingFixed.Status = (int)BookingStatusEnum.Cancelled;
                            objBookingFixed.CancelledTime = BritishTime.GetDateTime();
                            objBookingFixed.CancelBookingAmt = cancelationFee;
                            objBookingFixed.Autocab_IsBookingCancelled = false;
                            objBookingFixed.BookingCancellationRemarks = cancelBooking.Reson;
                            objBookingFixed.CancelBookingId = cancelBooking.ResonId;
                            db.SaveChanges();

                            SendPushNotification(objBookingFixed, false, 0);
                            if (objBookingFixed.CardPaymentTypeId == (int)CardPaymentTypeEnum.PayOverThePhone)
                            {

                                SendCancelBookingEmailAsync(objBookingFixed, objBookingFixed.CancelBookingId); 
                                main.Message = "Booking cancelled successfully";
                                main.Status = true;
                                main.StatusCode = (int)HttpStatusCode.OK;
                                return main;
                            }
                            if (!string.IsNullOrEmpty(objBookingFixed.Autocab_AuthorizationReference))
                            {
                                autocabCancelObj =
                                    AutoCapVehicletypeRequest.AgentBookingCancellationRequestAgent(
                                        objBookingFixed.Autocab_AuthorizationReference);

                                if (autocabCancelObj != null && autocabCancelObj.Status)
                                {
                                    if (objBookingTemp != null)
                                    {
                                        objBookingTemp.Autocab_AuthorizationReference = "";
                                        objBookingTemp.AutocabNumber = "";
                                        objBookingTemp.Autocab_BookingReference = "";
                                        db.SaveChanges();

                                    }                                    

                                    main.Message = "Booking cancelled successfully";
                                    main.Status = true;
                                    main.StatusCode = (int)HttpStatusCode.OK;
                                    return main;
                                }
                                else
                                {
                                    main.Message = autocabCancelObj.FailureReason;
                                    main.Status = true;
                                    main.StatusCode = (int)HttpStatusCode.OK;
                                    return main;
                                }
                            }
                            else
                            {
                                main.Message = "Invalid Authorization Reference ";
                                main.Status = true;
                                main.StatusCode = (int)HttpStatusCode.OK;
                                return main;
                            }
                        }
                    }
                    else
                    {
                        main.Message = "Reservation not found";
                        main.Status = false;
                        main.StatusCode = (int)HttpStatusCode.OK;
                    }





                }
            }
            catch (Exception ex)
            {
                main.Message = ex.Message;
                main.Status = false;
                main.StatusCode = (int)HttpStatusCode.BadRequest;
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return main;
        }

        private decimal GetBookingCancellationFee(Booking booking)
        {
            decimal bookingFee = 0;
            decimal journeyCost = 0;
            decimal penalty = 0;
            decimal bookingCancellationFee = 0;

            try
            {
                using (var db = new BATransferEntities())
                {
                    if (booking.PaymentMethod == (int)PaymentTypeEnum.Card)
                    {
                        int diffMinutes = 70;// (int)booking.BookingDateTime.Subtract(DateTime.Now).TotalMinutes;

                        var objCancelBooking = db.BookingCancellationFees.FirstOrDefault(c => c.StatrtTime < diffMinutes && diffMinutes < c.EndTime);

                        if (objCancelBooking != null)
                        {
                            if (objCancelBooking.IsBookingFeeAdd)
                            {
                                bookingFee = booking.OtherCharges;
                            }
                            if (objCancelBooking.IsJourneyCostAdd)
                            {
                                journeyCost = booking.FinalPrice - booking.OtherCharges;
                            }
                            penalty = objCancelBooking.Penalty;
                        }

                        bookingCancellationFee = bookingFee + journeyCost + penalty;
                        bookingCancellationFee = bookingCancellationFee > 0 ? bookingCancellationFee : 0;


                    }

                    return bookingCancellationFee;

                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return 0;
        }

        private void SendCancelBookingEmailAsync(Booking booking, int? cancelBookingId)
        {
            try
            {
                EmailCredentialsViewModel emailCredentials = new EmailCredentialsViewModel();

                int bookingSiteId = OrganizationHelper.GetRelevantOrganizationFromSite(booking.BookingSource);
                string logoUrl = string.Empty;
                switch (bookingSiteId)
                {
                    case (int)BookingSiteEnum.BA:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBA);
                        }
                        break;
                    case (int)BookingSiteEnum.AO:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.AO);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailAO);
                        }
                        break;
                    case (int)BookingSiteEnum.ET:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.ET);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailET);
                        }
                        break;
                    case (int)BookingSiteEnum.WT:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.WT);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailWT);
                        }
                        break;

                    case (int)BookingSiteEnum.BAT:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BAT);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAT);
                        }
                        break;

                    case (int)BookingSiteEnum.BAL:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BAL);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAL);
                        }
                        break;
                    case (int)BookingSiteEnum.BAC:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BAC);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAC);
                        }
                        break;
                    default:
                        break;
                }


                using (var db = new BATransferEntities())
                {

                    string systemComment = string.Empty;

                    var objCancelReson = db.BookingCancelConfigurations.FirstOrDefault(
                           x =>
                               x.BookingCancelId == cancelBookingId && x.IsActive &&
                               !x.IsDelete);
                    if (objCancelReson != null)
                    {
                        systemComment = objCancelReson.TypeOfCancelation;
                    }

                    string bookingInfoDomainUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.BookingInfoApi);
                    string url = bookingInfoDomainUrl + "Booking/ViewDetails?ask=" +
                                 HttpContext.Current.Server.UrlEncode(booking.BookingToken);
                    StringBuilder builder = new StringBuilder();
                    string logFilePath =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/CancelBookingEmail.txt"));
                    string text = System.IO.File.ReadAllText(logFilePath);
                    text = text.Replace("BOOKINGLOGO", logoUrl);
                    text = text.Replace("BOOKINGPERSONNAME", booking.BookedPersonName);
                    text = text.Replace("#SYSTEMCOMMENTS#", systemComment);
                    text = text.Replace("#BOOKINGCANCELCOMMENTS#", systemComment);

                    text = text.Replace("#BOOKINGID#", booking.BookingNumber);
                    text = text.Replace("#RESERVATIONDATETIME#", booking.BookingDateTime.ToString());
                    text = text.Replace("#FROM#", booking.BookingFrom);
                    text = text.Replace("#TO#", booking.BookingTo);

                    text = text.Replace("#URL#",
                        string.Format("<a href={0}>Click here for more information.</a>", url));
                    // text = text.Replace("BOOKINGPERSONALTERNATIVE", booking.AlternativeMobileNo);
                    builder.Append(text);



                    var listEmails = new List<string>();
                    if (booking.IsSendPassengerEmail)
                    {
                        listEmails.Add(booking.PassengerEmail);
                        listEmails.Add(booking.BookedPersonEmail);
                    }
                    else
                    {

                        listEmails.Add(booking.BookedPersonEmail);
                    }
                    // listEmails.Add(booking.PassengerEmail);

                    string _emailSubject = ConfigHelper.GetEmailSubject((int)BookingStatusEnum.Cancelled);

                    if (!string.IsNullOrEmpty(_emailSubject))
                    {
                        //Srikanth, Your Reservation Status at batransfer.com - Ref: BA479068
                        _emailSubject = _emailSubject.Replace("{0}", booking.BookedPersonName);
                        _emailSubject = _emailSubject.Replace("{1}", emailCredentials.DisplayName);
                        _emailSubject = _emailSubject.Replace("{2}", booking.BookingNumber);
                    }


                    var email = new RootEmail();
                    email.Attachments = null;
                    email.Body = builder.ToString();
                    email.FromEmailAddress = emailCredentials.FromEmail;
                    email.Username = emailCredentials.Username;
                    email.Password = emailCredentials.Password;
                    email.Port = emailCredentials.Port;
                    email.Smtp = emailCredentials.Smtp;

                    email.Subject = _emailSubject;
                    email.ToEmailAddresses = listEmails;
                    email.DisplayName = emailCredentials.DisplayName;//emailCredentials.DisplayName;
                    email.IsEnableSsl = emailCredentials.IsEnabledSSL;
                    string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                    EmailHelper.SendEmailWithoutAttachtment(email, apiUrl);

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");
            }

        }
        #endregion

        #region Confirm Booking


        public MainCancelBooking ConfirmBooking(ConfirmBookingViewModel cancelBooking, string userId)
        {
            var main = new MainCancelBooking();
            var token = System.Web.HttpContext.Current.Server.UrlDecode(cancelBooking.Token);

            try
            {
                var booking = new Booking();
                var bookingReturn = new Booking();
                using (var db = new BATransferEntities())
                {
                    if (db.Bookings.Any(bt => bt.BookingToken.Equals(token)))
                    {
                        booking = db.Bookings.First(bt => bt.BookingToken.Equals(token));
                        booking.Status = (int)BookingStatusEnum.Confirmed;
                        booking.ConfirmedTime = BritishTime.GetDateTime();
                        db.SaveChanges();

                        var relevantBookingTemp = db.Booking_TEMP.First(bt => bt.BookingToken.Equals(token));
                        relevantBookingTemp.Status = (int)BookingStatusEnum.Confirmed;
                        relevantBookingTemp.ConfirmedTime = BritishTime.GetDateTime();
                        db.SaveChanges();


                        BookingOperation newBookingOperation = new BookingOperation()
                        {
                            BookingId = booking.BookingId,
                            OperationId = (int)BookingStatusEnum.Confirmed,
                            OperatorUserId = userId,
                            Remarks = string.Empty,
                            sysCreatedDate = BritishTime.GetDateTime(),
                            sysModifiedDate = BritishTime.GetDateTime()
                        };

                        db.BookingOperations.Add(newBookingOperation);
                        db.SaveChanges();
                    }
                    else
                    {
                        main.Message = "Reservation not found";
                        main.Status = false;
                        main.StatusCode = (int)HttpStatusCode.OK;
                        return main;
                    }
                    if (booking.IsSourceBooking)
                    {
                        bookingReturn = db.Bookings.First(bt => bt.BookingId == booking.ReferenceBookingId && bt.IsReturnBooking);

                        bookingReturn.Status = (int)BookingStatusEnum.Confirmed;
                        bookingReturn.ConfirmedTime = BritishTime.GetDateTime();
                        db.SaveChanges();

                        var relevantBookingReturnTemp = db.Booking_TEMP.First(bt => bt.BookingId == booking.ReferenceBookingId && bt.IsReturnBooking);
                        relevantBookingReturnTemp.Status = (int)BookingStatusEnum.Confirmed;
                        relevantBookingReturnTemp.ConfirmedTime = BritishTime.GetDateTime();
                        db.SaveChanges();
                    }
                    if (
                        db.Booking_TEMP.Any(
                            x => x.BookingToken == token && x.IsReturnBooking == cancelBooking.IsRetunBooking))
                    {
                        var objBookingTemp =
                            db.Booking_TEMP.First(
                                x =>
                                    x.BookingToken == token && x.IsReturnBooking == cancelBooking.IsRetunBooking);
                        objBookingTemp.Status = (int)BookingStatusEnum.Confirmed;
                        objBookingTemp.ConfirmedTime = BritishTime.GetDateTime();
                        db.SaveChanges();

                    }

                    SendConfirmationEmail(booking, bookingReturn);

                    SendPushNotification(booking, false, 0);

                    main.Message = "Booking Confirmed successfully";
                    main.Status = true;
                    main.StatusCode = (int)HttpStatusCode.OK;
                    return main;

                }
            }
            catch (Exception ex)
            {
                main.Message = ex.Message;
                main.Status = false;
                main.StatusCode = (int)HttpStatusCode.BadRequest;
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return main;
        }

        private void SendPushNotification(Booking booking, bool isPaymentMethodChanged, int changedPaymentMethodId)
        {
            bool isSendPushNotification = false;

            using (var db = new BATransferEntities())
            {
                string dataMessage = string.Empty;
                string pushNotificationTitle = string.Empty;

                BookingNotificationConfiguration relevantNotificationConfiguration = null;

                if (isPaymentMethodChanged)
                {
                    dataMessage = WebConfigurationManager.AppSettings[" Msg_Push_PaymentMethodChange_Success"];

                    relevantNotificationConfiguration = db.BookingNotificationConfigurations.FirstOrDefault(bncc => bncc.BookingNotificationConfigurationId == (int)BookingNotificationConfigurationEnum.PaymentModeChanged);

                    if (relevantNotificationConfiguration != null)
                    {
                        isSendPushNotification = relevantNotificationConfiguration.IsPushNotificationEnabled;
                    }

                }
                else
                {
                    relevantNotificationConfiguration = db.BookingNotificationConfigurations.FirstOrDefault(bncc => bncc.BookingNotificationConfigurationId == booking.Status);

                    if (relevantNotificationConfiguration != null)
                    {
                        isSendPushNotification = relevantNotificationConfiguration.IsPushNotificationEnabled;
                    }

                    //switch (booking.Status)
                    //{
                    //    case (int)BookingStatusEnum.Confirmed:
                    //        dataMessage = booking.BookingNumber + " : " + WebConfigurationManager.AppSettings["Msg_Push_ConfirmBooking_Success"];
                    //        break;
                    //    case (int)BookingStatusEnum.Cancelled:
                    //        dataMessage = WebConfigurationManager.AppSettings["Msg_Push_CancelBooking_Success"];
                    //        break;
                    //}
                }

                string[] notificationsIds = db.MemberLogins.Where(m => m.userId.Equals(booking.UserId) && !string.IsNullOrEmpty(m.notificationId)).Select(ml => ml.notificationId).Distinct().ToArray();

                if (isSendPushNotification && notificationsIds != null && notificationsIds.Any())
                {
                    int osType = 0;

                    string bookingSourceName = Enum.GetName(typeof(BookingSource_Enum), booking.BookingSource);
                    if (!string.IsNullOrEmpty(bookingSourceName))
                    {
                        if (bookingSourceName.Contains("Android"))
                        {
                            osType = 2;
                        }
                        else if (bookingSourceName.Contains("iOS"))
                        {
                            osType = 1;
                        }
                    }

                    string apiUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.BookingPushNotificationApi);

                    string pushNotificationMessage = string.Empty;                   

                    if (isPaymentMethodChanged)
                    {
                        //string paymentUpdatedMessage = WebConfigurationManager.AppSettings["Msg_Push_PaymentUpdated_Success"];

                        string paymentUpdatedMessage = string.Empty;

                        var pushNotificationObj = PushNotificationHelper.GetPushNotificationMessageByPushNotificationTypeId((int)Base.PushNotificationTypeEnum.PaymentModeChanged);

                        if (pushNotificationObj != null)
                        {
                            paymentUpdatedMessage = pushNotificationObj.Title;
                        }
                        else
                        {
                            paymentUpdatedMessage = WebConfigurationManager.AppSettings["Msg_Push_PaymentUpdated_Success"];
                        }

                        if (changedPaymentMethodId != 100)
                        {
                            string changedPaymentTypeName = Enum.GetName(typeof(Base.PaymentTypeEnum), changedPaymentMethodId);

                            if (pushNotificationObj != null)
                            {
                                paymentUpdatedMessage = pushNotificationObj.Title;

                                if(pushNotificationObj.IsAppendBookingNumberForMessage)
                                {
                                    pushNotificationMessage = booking.BookingNumber + " : " + pushNotificationObj.Message + changedPaymentTypeName;
                                }
                                else
                                {
                                    pushNotificationMessage = pushNotificationObj.Message + changedPaymentTypeName;
                                }                                
                            }
                            else
                            {
                                //paymentUpdatedMessage = WebConfigurationManager.AppSettings["Msg_Push_PaymentUpdated_Success"];
                                //pushNotificationMessage = booking.BookingNumber + " : " + "Payment Has been  Updated";
                                pushNotificationMessage = booking.BookingNumber + " : " + "Payment Has been  Updated to " + changedPaymentTypeName;
                                pushNotificationTitle = booking.BookingNumber + " : " + WebConfigurationManager.AppSettings["Msg_Push_PaymentUpdated_Success"]; ;
                            }

                            //pushNotificationMessage = booking.BookingNumber + " : " + PushNotificationHelper.GetPushNotificationMessageByPushNotificationTypeId((int)Base.PushNotificationTypeEnum.PaymentModeChanged) + changedPaymentTypeName;
                            //statusString = Enum.GetName(typeof(Base.PushNotificationTypeEnum), (int)Base.PushNotificationTypeEnum.PaymentModeChanged);
                            //pushNotificationTitle = booking.BookingNumber + " : " + paymentUpdatedMessage;
                            //dataMessage = pushNotificationMessage;
                        }
                        else
                        {

                            if (pushNotificationObj != null)
                            {
                                paymentUpdatedMessage = pushNotificationObj.Title;

                                if (pushNotificationObj.IsAppendBookingNumberForMessage)
                                {
                                    pushNotificationMessage = booking.BookingNumber + " : " + paymentUpdatedMessage;
                                }
                                else
                                {
                                    pushNotificationMessage = paymentUpdatedMessage;
                                }

                                if (pushNotificationObj.IsAppendBookingNumberForTitle)
                                {
                                    pushNotificationTitle = booking.BookingNumber + " : " + paymentUpdatedMessage;
                                }
                                else
                                {
                                    pushNotificationTitle = paymentUpdatedMessage;
                                }
                            }

                            //pushNotificationMessage = booking.BookingNumber + " : " + paymentUpdatedMessage;
                            //statusString = Enum.GetName(typeof(Base.PushNotificationTypeEnum), (int)Base.PushNotificationTypeEnum.PaymentModeChanged);
                            //pushNotificationTitle = pushNotificationMessage;
                            //dataMessage = pushNotificationMessage;
                        }                      
                       
                    }
                    else
                    {
                        string statusString = string.Empty;

                        var pushNotificationObj = PushNotificationHelper.GetPushNotificationMessageByPushNotificationTypeId(booking.Status);

                        if(pushNotificationObj != null)
                        {

                            if(pushNotificationObj.IsAppendBookingNumberForMessage)
                            {
                                pushNotificationMessage = booking.BookingNumber + " : " + pushNotificationObj.Message;
                            }
                            else
                            {
                                pushNotificationMessage = pushNotificationObj.Message;
                            }
                                                        
                            statusString = pushNotificationObj.Title;

                            if (pushNotificationObj.IsAppendBookingNumberForTitle)
                            {
                                pushNotificationTitle = booking.BookingNumber + " : " + statusString;
                            }
                            else
                            {
                                pushNotificationTitle = pushNotificationObj.Message;
                            }
                        }
                        else
                        {
                            switch (booking.Status)
                            {
                                case (int)BookingStatusEnum.Confirmed:
                                    pushNotificationMessage = booking.BookingNumber + " : " + WebConfigurationManager.AppSettings["Msg_Push_ConfirmBooking_Success"];
                                    break;
                                case (int)BookingStatusEnum.Cancelled:
                                    pushNotificationMessage = booking.BookingNumber + " : " + WebConfigurationManager.AppSettings["Msg_Push_CancelBooking_Success"];
                                    break;
                            }

                            //pushNotificationMessage = booking.BookingNumber;
                            statusString = Enum.GetName(typeof(Base.BookingStatusEnum), booking.Status);
                            pushNotificationTitle = booking.BookingNumber + " : " + statusString;
                        }

                        //dataMessage = pushNotificationMessage;

                        //pushNotificationMessage = booking.BookingNumber + " : " + PushNotificationHelper.GetPushNotificationMessageByPushNotificationTypeId(booking.Status);
                        //statusString = Enum.GetName(typeof(Base.PushNotificationTypeEnum), booking.Status);
                        //statusString = Enum.GetName(typeof(Base.BookingStatusEnum), booking.Status);
                        //pushNotificationTitle = booking.BookingNumber + " : " + statusString;
                    }

                    var data = new { type = 1, datavalue = booking.BookingToken };

                    //var postData = new { isSb = false, title = statusString + " : " + pushNotificationMessage, body = dataMessage, data = data, osType = osType, batchNo = 1, tokens = notificationsIds };
                    //var postData = new { isSb = false, title = pushNotificationMessage, body = dataMessage, data = data, osType = osType, batchNo = 1, tokens = notificationsIds };
                    //var postData = new { isSb = false, title = pushNotificationTitle, body = dataMessage, data = data, osType = osType, batchNo = 1, tokens = notificationsIds };
                    var postData = new { isSb = false, title = pushNotificationTitle, body = pushNotificationMessage, data = data, osType = osType, batchNo = 1, tokens = notificationsIds };
                    string serialisedData = JsonConvert.SerializeObject(postData);
                    HttpWebResponse webResponse = null;
                    PushNotificationHelper.PushMessage(serialisedData, apiUrl, out webResponse);
                }
            }
        }

        private void SendConfirmationEmail(Booking booking, Booking bookingReturn)
        {
            try
            {

                EmailCredentialsViewModel emailCredentials = new EmailCredentialsViewModel();

                int bookingSiteId = OrganizationHelper.GetRelevantOrganizationFromSite(booking.BookingSource);
                string bookingToken = HttpContext.Current.Server.UrlEncode(booking.BookingToken);
                string bookingDetailApi = string.Empty;
                string logoUrl = string.Empty;
                string mainUrl = string.Empty;
                switch (bookingSiteId)
                {
                    case (int)BookingSiteEnum.BA:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBA);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.BA);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.AO:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.AO);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.AO);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailAO);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.AO);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.ET:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.ET);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.ET);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailET);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.ET);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.WT:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.WT);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.WT);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailWT);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.WT);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.BAC:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAC);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.BA);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.BAL:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAL);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.BA);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.BAT:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAT);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.BA);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    default:
                        break;
                }


                string loyaltyProgramUrl = mainUrl + "/MainPage/LoyaltyProgram";
                string contactUsUrl = mainUrl + "/MainPage/Contact";

                StringBuilder builder = new StringBuilder();
                string logFilePathHeader = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/Header.txt"));

                string textHeader = System.IO.File.ReadAllText(logFilePathHeader);
                textHeader = textHeader.Replace("LOGO-URL", logoUrl);
                textHeader = textHeader.Replace("BOOKING-DETAILS-URL", bookingDetailApi);
                textHeader = textHeader.Replace("BOOKING-URL", mainUrl);
                textHeader = textHeader.Replace("LOYALTY-PROGRAM-URL", loyaltyProgramUrl);
                textHeader = textHeader.Replace("CONTACT-US-URL", contactUsUrl);
                textHeader = textHeader.Replace("BOOKINGID", booking.BookingNumber);
                builder.Append(textHeader);

                #region VEHICLE UPGRATE
                if (booking.IsVehicleUpgradeEnabeld ?? false)
                {
                    string currentVehicle = string.Empty;
                    string upgradeVehicle = string.Empty;

                    if (booking.BookingVehicleUpgradeHistories != null)
                    {
                        currentVehicle = booking.BookingVehicleUpgradeHistories.First().VehicleType.VehicleTypeName;
                        upgradeVehicle = booking.BookingVehicleUpgradeHistories.First().VehicleType1.VehicleTypeName;
                    }


                    if (booking.IsUpgradeByPoints ?? false)
                    {
                        string filePathVehicleUpgrade =
                            System.Web.HttpContext.Current.Server.MapPath(
                                string.Format("~/EmailTemplets/Booking/VehicleUpgradeByPoints.txt"));

                        //LOGO-URL
                        string textVehicleUpgradeByPoints = System.IO.File.ReadAllText(filePathVehicleUpgrade);

                        textVehicleUpgradeByPoints = textVehicleUpgradeByPoints.Replace("CURRENTVEHICLE", currentVehicle);
                        textVehicleUpgradeByPoints = textVehicleUpgradeByPoints.Replace("UPGRADEVEHICLE", upgradeVehicle);
                        textVehicleUpgradeByPoints = textVehicleUpgradeByPoints.Replace("POINTS", booking.UsedPoints.ToString());
                        string updateText = booking.JourneyType == (int)JourneyTypeEnum.TwoWay && !booking.IsReturnBooking
                                           ? "(Vehicle upgrade for oneway)"
                                           : "";
                        textVehicleUpgradeByPoints = textVehicleUpgradeByPoints.Replace("UPGRADEINFO", updateText);
                        builder.Append(textVehicleUpgradeByPoints);
                    }




                    if (!booking.IsUpgradeByPoints ?? true)
                    {
                        string filePathVehicleUpgrade =
                            System.Web.HttpContext.Current.Server.MapPath(
                                string.Format("~/EmailTemplets/Booking/VehicleUpgradeByAmount.txt"));

                        //LOGO-URL
                        string textVehicleUpgradeAmt = System.IO.File.ReadAllText(filePathVehicleUpgrade);

                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("CURRENTVEHICLE", currentVehicle);
                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("UPGRADEVEHICLE", upgradeVehicle);
                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("AMOUNT", String.Format("{0:0.00}", booking.VehicleUpgradeAmt));
                        string paymentStatus = (booking.IsVehicleUpgradePaid ?? false) ? "Upgrade Amount Paid" : "Upgrade Amount Online Payment Pending";
                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("STATUS", paymentStatus);
                        string updateText = booking.JourneyType == (int)JourneyTypeEnum.TwoWay && !booking.IsReturnBooking
                                        ? "(Vehicle upgrade for oneway)"
                                        : "";
                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("UPGRADEINFO", updateText);
                        builder.Append(textVehicleUpgradeAmt);
                    }


                }

                if (bookingReturn != null && (bookingReturn.IsVehicleUpgradeEnabeld ?? false))
                {
                    string currentVehicleReturn = string.Empty;
                    string upgradeVehicleReturn = string.Empty;

                    if (bookingReturn.BookingVehicleUpgradeHistories != null)
                    {
                        currentVehicleReturn = bookingReturn.BookingVehicleUpgradeHistories.First().VehicleType.VehicleTypeName;
                        upgradeVehicleReturn = bookingReturn.BookingVehicleUpgradeHistories.First().VehicleType1.VehicleTypeName;
                    }

                    if (bookingReturn.IsUpgradeByPoints ?? false)
                    {
                        string filePathVehicleUpgrade =
                            System.Web.HttpContext.Current.Server.MapPath(
                                string.Format("~/EmailTemplets/Booking/VehicleUpgradeByPoints.txt"));

                        //LOGO-URL
                        string textVehicleUpgradeByPointsReturn = System.IO.File.ReadAllText(filePathVehicleUpgrade);

                        textVehicleUpgradeByPointsReturn = textVehicleUpgradeByPointsReturn.Replace("CURRENTVEHICLE", currentVehicleReturn);
                        textVehicleUpgradeByPointsReturn = textVehicleUpgradeByPointsReturn.Replace("UPGRADEVEHICLE", upgradeVehicleReturn);
                        textVehicleUpgradeByPointsReturn = textVehicleUpgradeByPointsReturn.Replace("POINTS", bookingReturn.UsedPoints.ToString());
                        string updateText = bookingReturn.JourneyType == (int)JourneyTypeEnum.TwoWay && bookingReturn.IsReturnBooking
                                      ? "(Vehicle upgrade for returnway)"
                                      : "";
                        textVehicleUpgradeByPointsReturn = textVehicleUpgradeByPointsReturn.Replace("UPGRADEINFO", updateText);
                        builder.Append(textVehicleUpgradeByPointsReturn);
                    }

                    if (!bookingReturn.IsUpgradeByPoints ?? true)
                    {
                        string filePathVehicleUpgrade =
                            System.Web.HttpContext.Current.Server.MapPath(
                                string.Format("~/EmailTemplets/Booking/VehicleUpgradeByAmount.txt"));

                        //LOGO-URL
                        string textVehicleUpgradeAmtReturn = System.IO.File.ReadAllText(filePathVehicleUpgrade);

                        textVehicleUpgradeAmtReturn = textVehicleUpgradeAmtReturn.Replace("CURRENTVEHICLE", currentVehicleReturn);
                        textVehicleUpgradeAmtReturn = textVehicleUpgradeAmtReturn.Replace("UPGRADEVEHICLE", upgradeVehicleReturn);
                        textVehicleUpgradeAmtReturn = textVehicleUpgradeAmtReturn.Replace("AMOUNT", String.Format("{0:0.00}", bookingReturn.VehicleUpgradeAmt));
                        string paymentStatus = (bookingReturn.IsVehicleUpgradePaid ?? false) ? "Upgrade Amount Paid" : "Upgrade Amount Online Payment Pending";
                        textVehicleUpgradeAmtReturn = textVehicleUpgradeAmtReturn.Replace("STATUS", paymentStatus);
                        string updateText = bookingReturn.JourneyType == (int)JourneyTypeEnum.TwoWay && bookingReturn.IsReturnBooking
                                     ? "(Vehicle upgrade for returnway)"
                                     : "";
                        textVehicleUpgradeAmtReturn = textVehicleUpgradeAmtReturn.Replace("UPGRADEINFO", updateText);
                        builder.Append(textVehicleUpgradeAmtReturn);
                    }

                }
                #endregion

                if (!string.IsNullOrEmpty(booking.PassengerName))
                {
                    string logFilePathLeadPassanger =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/Booking/LeadPassangerDetails.txt"));

                    //LOGO-URL
                    string textPassanger = System.IO.File.ReadAllText(logFilePathLeadPassanger);

                    textPassanger = textPassanger.Replace("PASSENGERNAME", booking.PassengerName);
                    textPassanger = textPassanger.Replace("PASSENGEREMAIL", booking.PassengerEmail);
                    textPassanger = textPassanger.Replace("PASSENGERMOBILE", booking.PassengerTel);
                    builder.Append(textPassanger);
                }

                string logFilePathMember = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/MemberDetails.txt"));

                //LOGO-URL
                string textMember = System.IO.File.ReadAllText(logFilePathMember);
                textMember = textMember.Replace("BOOKINGPERSONNAME", booking.BookedPersonName);
                textMember = textMember.Replace("BOOKINGPERSONMOBILE", booking.BookedPersonTel);
                textMember = textMember.Replace("BOOKINGPERSONALTERNATIVE", booking.AlternativePhoneNumber);
                textMember = textMember.Replace("BOOKINGPERSONEMAIL", booking.BookedPersonEmail);

                builder.Append(textMember);

                string logFilePath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/EmailMainBody.txt"));
                string text = System.IO.File.ReadAllText(logFilePath);
                string paymentType = booking.PaymentMethod == (int)PaymentTypeEnum.Cash
                    ? "Cash"
                    : booking.PaymentMethod == (int)PaymentTypeEnum.Card
                        ? "Card"
                        : booking.PaymentMethod == (int)PaymentTypeEnum.Bank_Transfer
                            ? "Bank Transfer"
                            : booking.PaymentMethod == (int)PaymentTypeEnum.Free_Journey
                                ? "Free Journey"
                                : booking.PaymentMethod == (int)PaymentTypeEnum.Promo_Code
                                    ? "Promo Code"
                                    : booking.PaymentMethod == (int)PaymentTypeEnum.Voucher
                                        ? "Voucher"
                                        : booking.PaymentMethod == (int)PaymentTypeEnum.Bank_Transfer
                                            ? "Bank Transfer"
                                            : booking.PaymentMethod == (int)PaymentTypeEnum.Use_My_Credits
                                                ? "Used My Credits"
                                                : "";

                text = text.Replace("BOOKINGID", booking.BookingNumber);
                text = text.Replace("RESERVATIONDATETIME", ValueParseHelper.ToStringDateTime(booking.ReservationDateTime));
                if (booking.JourneyType == (int)JourneyTypeEnum.OneWay)
                {
                    text = text.Replace("BOOKINGAMOUNT", booking.FinalPrice.ToString());
                }
                else
                {
                    decimal returnFinal = bookingReturn != null ? bookingReturn.FinalPrice : 0;
                    text = text.Replace("BOOKINGAMOUNT", (booking.FinalPrice + returnFinal).ToString());
                }
                text = text.Replace("BOOKINGPAYMENTMETHOD", paymentType);
                text = text.Replace("PAYMENTOVERTHEPHONE", "");
                if (booking.CardPaymentTypeId == (int)CardPaymentTypeEnum.PayOverThePhone)
                {
                    text = text.Replace("PAYMENTOVERTHEPHONE",
                        "<span>Pay by Credit/Debit card over the phone to the Office (Call +44 208 900 2299)</span>");
                }
                else
                {
                    text = text.Replace("PAYMENTOVERTHEPHONE", "");

                }
                builder.Append(text);
                if (booking.ConsumerDebit > 0 || (bookingReturn != null && bookingReturn.ConsumerDebit > 0))
                {
                    decimal returnDebit = bookingReturn != null ? bookingReturn.ConsumerDebit : 0;
                    string discountMoneyBackTxt =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/Booking/DiscountMoneyBack.txt"));
                    string additionalChargesTxt = System.IO.File.ReadAllText(discountMoneyBackTxt);
                    additionalChargesTxt = additionalChargesTxt.Replace("DISCOUNT", (booking.ConsumerDebit + returnDebit).ToString());
                    builder.Append(additionalChargesTxt);
                }
                if (booking.OtherCharges > 0)
                {
                    string logFilePathAdditionalCharges =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/Booking/BookingFee.txt"));
                    string additionalChargesTxt = System.IO.File.ReadAllText(logFilePathAdditionalCharges);
                    additionalChargesTxt = additionalChargesTxt.Replace("BOOKINGFEE", booking.OtherCharges.ToString());
                    additionalChargesTxt = additionalChargesTxt.Replace("PAYMENTSTATUS", Enum.GetName(typeof(PaymentStatusEnum), booking.PaymentStatus));
                    string comments = "  <div style='float:left;width:100%;font-weight:600;'>" +
                                     " <p> The booking fee " + booking.OtherCharges + " is paid, Balance still needs to be paid </p>" +
                                     "</div> ";
                    if (booking.PaymentStatus == (int)PaymentStatusEnum.Pending)
                    {
                        additionalChargesTxt = additionalChargesTxt.Replace("COMMENT", comments);
                    }
                    else
                    {
                        additionalChargesTxt = additionalChargesTxt.Replace("COMMENT", "");
                    }
                    builder.Append(additionalChargesTxt);
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    string logFilePathOneWayBooking = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/OneWayBooking.txt"));
                    string textOneWay = System.IO.File.ReadAllText(logFilePathOneWayBooking);
                    textOneWay = textOneWay.Replace("BOOKINGDATETIME", booking.BookingDateTime.ToString());
                    textOneWay = textOneWay.Replace("BOOKINGAMOUNTONEWAY", booking.TotalDistancePrice.ToString());
                    if (booking.StudentDiscount > 1 && booking.BookingSource == (int)BookingSource_Enum.AO_Web)
                    {
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTDIV",
                            "<p>Student Discount  <span style='float:right;'>:</span></p>");
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + booking.StudentDiscount + "</p>");
                    }
                    else
                    {
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTDIV", "");
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                    }
                    builder.Append(textOneWay);
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    string logFilePathAirportDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/AirportDetails.txt"));
                    string textAirport = System.IO.File.ReadAllText(logFilePathAirportDetails);
                    textAirport = textAirport.Replace("FLIGHTNUMBER", booking.FlightNo);
                    textAirport = textAirport.Replace("FLIGHTLANDINGDATETIME", booking.FlightLandingDateTime.ToString());
                    textAirport = textAirport.Replace("FLIGHTDISPLAYNAME", booking.DriverDisplayName);
                    textAirport = textAirport.Replace("FLIGHTLANDINGDRIVERELLAPSETIME", booking.DriverEllapseTime.ToString());
                    textAirport = textAirport.Replace("BOOKINGAMOUNTONEWAY", booking.TotalDistancePrice.ToString());
                    textAirport = textAirport.Replace("PICKUPCHARGE", booking.PickupCharge.ToString());


                    if (booking.StudentDiscount > 1 && booking.BookingSource == (int)BookingSource_Enum.AO_Web)
                    {
                        textAirport = textAirport.Replace("STUDENTDISCOUNTDIV",
                            "<p>Student Discount  <span style='float:right;'>:</span></p>");
                        textAirport = textAirport.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + booking.StudentDiscount + "</p>");
                    }
                    else
                    {
                        textAirport = textAirport.Replace("STUDENTDISCOUNTDIV", "");
                        textAirport = textAirport.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                    }
                    builder.Append(textAirport);
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    string logFilePathCruisePortDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/CruisePortDetails.txt"));
                    string textCruiseport = System.IO.File.ReadAllText(logFilePathCruisePortDetails);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPNUMBER", booking.FlightNo);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPLANDINGDATETIME", booking.FlightLandingDateTime.ToString());
                    textCruiseport = textCruiseport.Replace("CRUISESHIPDISPLAYNAME", booking.DriverDisplayName);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPLANDINGDRIVERELLAPSETIME", booking.DriverEllapseTime.ToString());
                    textCruiseport = textCruiseport.Replace("BOOKINGAMOUNTONEWAY", booking.TotalDistancePrice.ToString());
                    if (booking.StudentDiscount > 1 && booking.BookingSource == (int)BookingSource_Enum.AO_Web)
                    {
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTDIV",
                            "<p>Student Discount  <span style='float:right;'>:</span></p>");
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + booking.StudentDiscount + "</p>");
                    }
                    else
                    {
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTDIV", "");
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                    }
                    builder.Append(textCruiseport);
                }
                if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay && bookingReturn != null)
                {
                    if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                    {
                        string logFilePathAirportReturnDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/AirportReturn.txt"));
                        string textAirportReturn = System.IO.File.ReadAllText(logFilePathAirportReturnDetails);
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_NUMBER_RETURN", bookingReturn.FlightNo);
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_LANDINGDATE_TIME_RETURN", bookingReturn.FlightLandingDateTime.ToString());
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_DISPLAY_NAME_RETURN", bookingReturn.DriverDisplayName);
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_LANDING_DRIVER_ELLAPSE_TIME_RETURN", bookingReturn.DriverEllapseTime.ToString());
                        textAirportReturn = textAirportReturn.Replace("RETURNDATETIME", bookingReturn.FlightLandingDateTime.ToString());
                        textAirportReturn = textAirportReturn.Replace("BOOKINGAMOUNTTWOWAY", bookingReturn.TotalDistancePrice.ToString());
                        textAirportReturn = textAirportReturn.Replace("PICKUPCHARGE", bookingReturn.PickupCharge.ToString());
                        if (bookingReturn.StudentDiscount > 1 && bookingReturn.BookingSource == (int)BookingSource_Enum.AO_Web)
                        {
                            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTDIV",
                                "<p>Student Discount  <span style='float:right;'>:</span></p>");
                            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + bookingReturn.StudentDiscount + "</p>");
                        }
                        else
                        {
                            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTDIV", "");
                            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                        }
                        builder.Append(textAirportReturn);
                    }
                    else if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                    {
                        string logFilePathCruiseportReturn = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/CruiseportReturn.txt"));
                        string textCruiseportReturn = System.IO.File.ReadAllText(logFilePathCruiseportReturn);
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_NUMBER_RETURN", bookingReturn.FlightNo);
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_LANDING_DATE_TIME_RETURN", bookingReturn.FlightLandingDateTime.ToString());
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_DISPLAY_NAME_RETURN", bookingReturn.DriverDisplayName);
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_LANDING_DRIVER_ELLAPSE_TIME_RETURN", bookingReturn.DriverEllapseTime.ToString());
                        textCruiseportReturn = textCruiseportReturn.Replace("RETURNDATETIME", bookingReturn.FlightLandingDateTime.ToString());
                        textCruiseportReturn = textCruiseportReturn.Replace("BOOKINGAMOUNTTWOWAY", bookingReturn.TotalDistancePrice.ToString());
                        if (bookingReturn.StudentDiscount > 1 && bookingReturn.BookingSource == (int)BookingSource_Enum.AO_Web)
                        {
                            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTDIV",
                                "<p>Student Discount  <span style='float:right;'>:</span></p>");
                            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + bookingReturn.StudentDiscount + "</p>");
                        }
                        else
                        {
                            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTDIV", "");
                            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                        }
                        builder.Append(textCruiseportReturn);
                    }
                    if (bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                    {
                        string logFilePathTwoWayBooking = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/ReturnBooking.txt"));
                        string textTwoWay = System.IO.File.ReadAllText(logFilePathTwoWayBooking);
                        textTwoWay = textTwoWay.Replace("BOOKINGDATETIMERETURN", bookingReturn.BookingDateTime.ToString());
                        textTwoWay = textTwoWay.Replace("BOOKINGAMOUNTTWOWAY", bookingReturn.TotalDistancePrice.ToString());
                        if (bookingReturn.StudentDiscount > 1 && bookingReturn.BookingSource == (int)BookingSource_Enum.AO_Web)
                        {
                            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTDIV",
                                "<p>Student Discount  <span style='float:right;'>:</span></p>");
                            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + bookingReturn.StudentDiscount + "</p>");
                        }
                        else
                        {
                            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTDIV", "");
                            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                        }
                        builder.Append(textTwoWay);
                    }
                }
                string logFilePathAddressDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/BookingAddress.txt"));
                string addresstext = System.IO.File.ReadAllText(logFilePathAddressDetails);
                //addresstext = addresstext.Replace("CUSTOMERCOMMENTS", booking.CustomerComment);
                //addresstext = addresstext.Replace("MAPURL", booking.MapUrl);
                addresstext = addresstext.Replace("BOOKING-DATE-TIME", booking.BookingDateTime.ToString());
                addresstext = addresstext.Replace("FROM-ADDRESS", booking.BookingFrom);
                addresstext = addresstext.Replace("FROMTOWN", booking.FromTown);
                addresstext = addresstext.Replace("FROMPOSTCODE", booking.BookingFromFullPostCode);
                addresstext = addresstext.Replace("TO-ADDRESS", booking.BookingTo);
                addresstext = addresstext.Replace("TOTOWN", booking.ToTown);
                addresstext = addresstext.Replace("TOPOSTCODE", booking.BookingToFullPostCode);

                #region Via Points

                StringBuilder builderViapoints = new StringBuilder();
                int viaid = 0;
                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var journeyViapoints = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty());
                    foreach (var viapoint in journeyViapoints.ToList())
                    {
                        viaid++;
                        string via = "<div style='width:97%; float: left; font-size:15px'>" +
                                     "<div   style='float:left; width:20%; color:#666666; font-weight:600; line-height: 10px;'>" +
                                     "<p style='margin-top:10px;'>Viapoint " + viaid +
                                     "<span style='float:right;'>:</span></p>" +
                                     "</div>" +
                                     "<div class='form-right' style='color:#666666; margin-left:10px; width: 75%; float: left;'>" +
                                     "<p style='margin-top:4px;line-height: 17px;'>" + viapoint.ViaFullAddress + "</p> " +
                                     "</div>" +
                                     "</div>";
                        builderViapoints.Append(via);
                    }
                    addresstext = addresstext.Replace("VIA-POINTS", builderViapoints.ToString());
                }
                else
                {
                    addresstext = addresstext.Replace("VIA-POINTS", "");
                }

                #endregion

                builder.Append(addresstext);
                if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay && bookingReturn != null)
                {
                    string logFilePathReturnBookingAddress = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/BookingAddressReturn.txt"));
                    string returnBookingTxt = System.IO.File.ReadAllText(logFilePathReturnBookingAddress);
                    if (bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport &&
                        bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                    {
                        returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME", bookingReturn.BookingDateTime.ToString());
                    }

                    if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                    {
                        returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME", bookingReturn.FlightLandingDateTime.ToString());
                    }
                    if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                    {
                        returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME", bookingReturn.FlightLandingDateTime.ToString());
                    }
                    returnBookingTxt = returnBookingTxt.Replace("FROM-ADDRESS", bookingReturn.BookingFrom);
                    returnBookingTxt = returnBookingTxt.Replace("FROMTOWN", bookingReturn.FromTown);
                    returnBookingTxt = returnBookingTxt.Replace("FROMPOSTCODE", bookingReturn.BookingFromFullPostCode);
                    returnBookingTxt = returnBookingTxt.Replace("TO-ADDRESS", bookingReturn.BookingTo);
                    returnBookingTxt = returnBookingTxt.Replace("TOTOWN", bookingReturn.ToTown);
                    returnBookingTxt = returnBookingTxt.Replace("TOPOSTCODE", bookingReturn.BookingToFullPostCode);

                    #region Return Via Points

                    StringBuilder builderViapointsReturn = new StringBuilder();
                    int viaidReturn = 0;

                    if (bookingReturn.JourneyViapoints != null && bookingReturn.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                    {
                        var journeyViapointsReturn = bookingReturn.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty());
                        foreach (var viapoint in journeyViapointsReturn.ToList())
                        {
                            viaidReturn++;
                            string via = "<div style='width:97%; float: left; font-size:15px'>" +
                                         "<div   style='float:left; width:20%; color:#666666; font-weight:600; line-height: 10px;'>" +
                                         "<p style='margin-top:10px;'>Viapoint " + viaidReturn +
                                         "<span style='float:right;'>:</span></p>" +
                                         "</div>" +
                                         "<div class='form-right' style='color:#666666; margin-left:10px; width: 75%; float: left;'>" +
                                         "<p style='margin-top:4px;line-height: 17px;'>" + viapoint.ViaFullAddress + "</p> " +
                                         "</div>" +
                                         "</div>";
                            builderViapointsReturn.Append(via);
                        }
                        returnBookingTxt = returnBookingTxt.Replace("VIA-POINTS", builderViapointsReturn.ToString());
                    }
                    else
                    {
                        returnBookingTxt = returnBookingTxt.Replace("VIA-POINTS", "");
                    }

                    #endregion

                    builder.Append(returnBookingTxt);
                }
                else
                {
                    string fileClosingDiv = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/ClosingDiv.txt"));
                    string fileClosingDivTxt = System.IO.File.ReadAllText(fileClosingDiv);
                    builder.Append(fileClosingDivTxt);
                }
                string fileMap = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/Map.txt"));
                string textMap = System.IO.File.ReadAllText(fileMap);
                textMap = textMap.Replace("MAP-URL", booking.MapUrl);
                builder.Append(textMap);
                string fileLuggageInfo = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/LuggageInfo.txt"));
                string textLuggage = System.IO.File.ReadAllText(fileLuggageInfo);
                string vehicleType = string.Join("&", booking.BookingVehicleTypes.Select(x => x.VehicleType.VehicleTypeName).ToArray());

                textLuggage = textLuggage.Replace("BOOKINGVEHICLETYPES", vehicleType);
                textLuggage = textLuggage.Replace("NUMBEROFPESSANGERS", booking.NumberOfPassengers.ToString());
                textLuggage = textLuggage.Replace("NUMBEROFBABYSEATS", (booking.BoosterSeat + booking.InfantSeats + booking.ChildSeats).ToString());
                textLuggage = textLuggage.Replace("NUMBEROFLUGGAHES", booking.Luggage.ToString());
                textLuggage = textLuggage.Replace("NUMBEROFHANDLUGGAHES", booking.HandLuggage.ToString());

                if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay && bookingReturn != null)
                {
                    textLuggage = textLuggage.Replace("BOOKINGAMOUNT", (booking.FinalPrice + bookingReturn.FinalPrice).ToString());
                }
                else
                {
                    textLuggage = textLuggage.Replace("BOOKINGAMOUNT", booking.FinalPrice.ToString());
                }
                //textLuggage = textLuggage.Replace("BOOKINGAMOUNT", booking.FinalPrice.ToString());

                builder.Append(textLuggage);
                string logFilePathFooderDetals = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/FooderDetals.txt"));
                string foodertext = System.IO.File.ReadAllText(logFilePathFooderDetals);
                foodertext = foodertext.Replace("BOOKING-URL", mainUrl);
                foodertext = foodertext.Replace("LOYALTY-PROGRAM-URL", loyaltyProgramUrl);
                foodertext = foodertext.Replace("CONTACT-US-URL", contactUsUrl);
                builder.Append(foodertext);
                var listEmails = new List<string>();
                if (booking.IsSendPassengerEmail)
                {
                    listEmails.Add(booking.PassengerEmail);
                    listEmails.Add(booking.BookedPersonEmail);
                }
                else
                {
                    listEmails.Add(booking.BookedPersonEmail);
                }

                //Sujanika, Your Reservation Confirmed at batransfer.com - Ref: BA544226
                string _emailSubject = booking.BookedPersonName + "," + "Your Reservation Confirmed at " + emailCredentials.DisplayName +
                                       " - Ref: " + booking.BookingNumber;


                var email = new RootEmail();
                email.Attachments = null;
                email.Body = builder.ToString();
                email.FromEmailAddress = emailCredentials.FromEmail;
                email.Username = emailCredentials.Username;
                email.Password = emailCredentials.Password;
                email.Port = emailCredentials.Port;
                email.Smtp = emailCredentials.Smtp;
                email.Subject = _emailSubject;
                email.ToEmailAddresses = listEmails;
                email.DisplayName = emailCredentials.DisplayName;
                email.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                EmailHelper.SendEmailWithoutAttachtment(email, apiUrl);
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }

        public MainCancelBooking ConfirmCancelBooking(ConfirmBookingViewModel confirmBooking, string userId)
        {
            var main = new MainCancelBooking();
            var token = System.Web.HttpContext.Current.Server.UrlDecode(confirmBooking.Token);

            try
            {

                using (var db = new BATransferEntities())
                {
                    var objBooking =
                        db.Bookings.FirstOrDefault(
                            x => x.BookingToken.Equals(token) && x.IsReturnBooking == confirmBooking.IsRetunBooking);

                    if (objBooking != null)
                    {
                        objBooking.Status = (int)BookingStatusEnum.Confirmed;
                        objBooking.ConfirmedTime = BritishTime.GetDateTime();
                        db.SaveChanges();

                        var relevantBookingTemp = db.Booking_TEMP.FirstOrDefault(
                            x => x.BookingToken.Equals(token) && x.IsReturnBooking == confirmBooking.IsRetunBooking);
                        if (relevantBookingTemp != null)
                        {
                            relevantBookingTemp.Status = (int)BookingStatusEnum.Confirmed;
                            relevantBookingTemp.ConfirmedTime = BritishTime.GetDateTime();
                            db.SaveChanges();
                        }

                        BookingOperation newBookingOperation = new BookingOperation()
                        {
                            BookingId = objBooking.BookingId,
                            OperationId = (int)BookingStatusEnum.Confirmed,
                            OperatorUserId = userId,
                            Remarks = string.Empty,
                            sysCreatedDate = BritishTime.GetDateTime(),
                            sysModifiedDate = BritishTime.GetDateTime()
                        };

                        db.BookingOperations.Add(newBookingOperation);
                        db.SaveChanges();
                    }

                    else
                    {
                        main.Message = "Reservation not found";
                        main.Status = false;
                        main.StatusCode = (int)HttpStatusCode.OK;
                        return main;
                    }

                    SendConfirmationEmailCancelBooking(objBooking);
                    SendPushNotification(objBooking, false, 0);

                    main.Message = "Booking Confirmed successfully";
                    main.Status = true;
                    main.StatusCode = (int)HttpStatusCode.OK;
                    return main;

                }
            }
            catch (Exception ex)
            {
                main.Message = ex.Message;
                main.Status = false;
                main.StatusCode = (int)HttpStatusCode.BadRequest;
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return main;
        }
        private void SendConfirmationEmailCancelBooking(Booking booking)
        {
            try
            {

                EmailCredentialsViewModel emailCredentials = new EmailCredentialsViewModel();

                int bookingSiteId = OrganizationHelper.GetRelevantOrganizationFromSite(booking.BookingSource);
                string bookingToken = HttpContext.Current.Server.UrlEncode(booking.BookingToken);
                string bookingDetailApi = string.Empty;
                string logoUrl = string.Empty;
                string mainUrl = string.Empty;
                switch (bookingSiteId)
                {
                    case (int)BookingSiteEnum.BA:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBA);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.BA);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.AO:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.AO);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.AO);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailAO);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.AO);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.ET:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.ET);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.ET);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailET);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.ET);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.WT:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.WT);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.WT);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailWT);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.WT);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.BAC:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAC);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.BA);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.BAL:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAL);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.BA);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.BAT:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAT);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.BA);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    default:
                        break;
                }


                string loyaltyProgramUrl = mainUrl + "/MainPage/LoyaltyProgram";
                string contactUsUrl = mainUrl + "/MainPage/Contact";

                StringBuilder builder = new StringBuilder();
                string logFilePathHeader = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/Header.txt"));

                string textHeader = System.IO.File.ReadAllText(logFilePathHeader);
                textHeader = textHeader.Replace("LOGO-URL", logoUrl);
                textHeader = textHeader.Replace("BOOKING-DETAILS-URL", bookingDetailApi);
                textHeader = textHeader.Replace("BOOKING-URL", mainUrl);
                textHeader = textHeader.Replace("LOYALTY-PROGRAM-URL", loyaltyProgramUrl);
                textHeader = textHeader.Replace("CONTACT-US-URL", contactUsUrl);
                textHeader = textHeader.Replace("BOOKINGID", booking.BookingNumber);
                builder.Append(textHeader);

                #region VEHICLE UPGRATE
                if (booking.IsVehicleUpgradeEnabeld ?? false)
                {
                    string currentVehicle = string.Empty;
                    string upgradeVehicle = string.Empty;

                    if (booking.BookingVehicleUpgradeHistories != null)
                    {
                        currentVehicle = booking.BookingVehicleUpgradeHistories.First().VehicleType.VehicleTypeName;
                        upgradeVehicle = booking.BookingVehicleUpgradeHistories.First().VehicleType1.VehicleTypeName;
                    }


                    if (booking.IsUpgradeByPoints ?? false)
                    {
                        string filePathVehicleUpgrade =
                            System.Web.HttpContext.Current.Server.MapPath(
                                string.Format("~/EmailTemplets/Booking/VehicleUpgradeByPoints.txt"));

                        //LOGO-URL
                        string textVehicleUpgradeByPoints = System.IO.File.ReadAllText(filePathVehicleUpgrade);

                        textVehicleUpgradeByPoints = textVehicleUpgradeByPoints.Replace("CURRENTVEHICLE", currentVehicle);
                        textVehicleUpgradeByPoints = textVehicleUpgradeByPoints.Replace("UPGRADEVEHICLE", upgradeVehicle);
                        textVehicleUpgradeByPoints = textVehicleUpgradeByPoints.Replace("POINTS", booking.UsedPoints.ToString());
                        string updateText = booking.JourneyType == (int)JourneyTypeEnum.TwoWay && !booking.IsReturnBooking
                                           ? "(Vehicle upgrade for oneway)"
                                           : "";
                        textVehicleUpgradeByPoints = textVehicleUpgradeByPoints.Replace("UPGRADEINFO", updateText);
                        builder.Append(textVehicleUpgradeByPoints);
                    }




                    if (!booking.IsUpgradeByPoints ?? true)
                    {
                        string filePathVehicleUpgrade =
                            System.Web.HttpContext.Current.Server.MapPath(
                                string.Format("~/EmailTemplets/Booking/VehicleUpgradeByAmount.txt"));

                        //LOGO-URL
                        string textVehicleUpgradeAmt = System.IO.File.ReadAllText(filePathVehicleUpgrade);

                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("CURRENTVEHICLE", currentVehicle);
                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("UPGRADEVEHICLE", upgradeVehicle);
                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("AMOUNT", String.Format("{0:0.00}", booking.VehicleUpgradeAmt));
                        string paymentStatus = (booking.IsVehicleUpgradePaid ?? false) ? "Upgrade Amount Paid" : "Upgrade Amount Online Payment Pending";
                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("STATUS", paymentStatus);
                        string updateText = booking.JourneyType == (int)JourneyTypeEnum.TwoWay && !booking.IsReturnBooking
                                        ? "(Vehicle upgrade for oneway)"
                                        : "";
                        textVehicleUpgradeAmt = textVehicleUpgradeAmt.Replace("UPGRADEINFO", updateText);
                        builder.Append(textVehicleUpgradeAmt);
                    }


                }

                #endregion

                if (!string.IsNullOrEmpty(booking.PassengerName))
                {
                    string logFilePathLeadPassanger =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/Booking/LeadPassangerDetails.txt"));

                    //LOGO-URL
                    string textPassanger = System.IO.File.ReadAllText(logFilePathLeadPassanger);

                    textPassanger = textPassanger.Replace("PASSENGERNAME", booking.PassengerName);
                    textPassanger = textPassanger.Replace("PASSENGEREMAIL", booking.PassengerEmail);
                    textPassanger = textPassanger.Replace("PASSENGERMOBILE", booking.PassengerTel);
                    builder.Append(textPassanger);
                }

                string logFilePathMember = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/MemberDetails.txt"));

                //LOGO-URL
                string textMember = System.IO.File.ReadAllText(logFilePathMember);
                textMember = textMember.Replace("BOOKINGPERSONNAME", booking.BookedPersonName);
                textMember = textMember.Replace("BOOKINGPERSONMOBILE", booking.BookedPersonTel);
                textMember = textMember.Replace("BOOKINGPERSONALTERNATIVE", booking.AlternativePhoneNumber);
                textMember = textMember.Replace("BOOKINGPERSONEMAIL", booking.BookedPersonEmail);

                builder.Append(textMember);

                string logFilePath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/EmailMainBody.txt"));
                string text = System.IO.File.ReadAllText(logFilePath);
                string paymentType = booking.PaymentMethod == (int)PaymentTypeEnum.Cash
                    ? "Cash"
                    : booking.PaymentMethod == (int)PaymentTypeEnum.Card
                        ? "Card"
                        : booking.PaymentMethod == (int)PaymentTypeEnum.Bank_Transfer
                            ? "Bank Transfer"
                            : booking.PaymentMethod == (int)PaymentTypeEnum.Free_Journey
                                ? "Free Journey"
                                : booking.PaymentMethod == (int)PaymentTypeEnum.Promo_Code
                                    ? "Promo Code"
                                    : booking.PaymentMethod == (int)PaymentTypeEnum.Voucher
                                        ? "Voucher"
                                        : booking.PaymentMethod == (int)PaymentTypeEnum.Bank_Transfer
                                            ? "Bank Transfer"
                                            : booking.PaymentMethod == (int)PaymentTypeEnum.Use_My_Credits
                                                ? "Used My Credits"
                                                : "";

                text = text.Replace("BOOKINGID", booking.BookingNumber);
                text = text.Replace("RESERVATIONDATETIME", ValueParseHelper.ToStringDateTime(booking.ReservationDateTime));
                if (booking.JourneyType == (int)JourneyTypeEnum.OneWay)
                {
                    text = text.Replace("BOOKINGAMOUNT", booking.FinalPrice.ToString());
                }
                else
                {
                    text = text.Replace("BOOKINGAMOUNT", (booking.FinalPrice).ToString());
                }
                text = text.Replace("BOOKINGPAYMENTMETHOD", paymentType);
                text = text.Replace("PAYMENTOVERTHEPHONE", "");
                if (booking.CardPaymentTypeId == (int)CardPaymentTypeEnum.PayOverThePhone)
                {
                    text = text.Replace("PAYMENTOVERTHEPHONE",
                        "<span>Pay by Credit/Debit card over the phone to the Office (Call +44 208 900 2299)</span>");
                }
                else
                {
                    text = text.Replace("PAYMENTOVERTHEPHONE", "");

                }
                builder.Append(text);
                if (booking.ConsumerDebit > 0)
                {
                    string discountMoneyBackTxt =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/Booking/DiscountMoneyBack.txt"));
                    string additionalChargesTxt = System.IO.File.ReadAllText(discountMoneyBackTxt);
                    additionalChargesTxt = additionalChargesTxt.Replace("DISCOUNT", (booking.ConsumerDebit).ToString());
                    builder.Append(additionalChargesTxt);
                }
                if (booking.OtherCharges > 0)
                {
                    string logFilePathAdditionalCharges =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/Booking/BookingFee.txt"));
                    string additionalChargesTxt = System.IO.File.ReadAllText(logFilePathAdditionalCharges);
                    additionalChargesTxt = additionalChargesTxt.Replace("BOOKINGFEE", booking.OtherCharges.ToString());
                    additionalChargesTxt = additionalChargesTxt.Replace("PAYMENTSTATUS", Enum.GetName(typeof(PaymentStatusEnum), booking.PaymentStatus));
                    string comments = "  <div style='float:left;width:100%;font-weight:600;'>" +
                                     " <p> The booking fee " + booking.OtherCharges + " is paid, Balance still needs to be paid </p>" +
                                     "</div> ";
                    if (booking.PaymentStatus == (int)PaymentStatusEnum.Pending)
                    {
                        additionalChargesTxt = additionalChargesTxt.Replace("COMMENT", comments);
                    }
                    else
                    {
                        additionalChargesTxt = additionalChargesTxt.Replace("COMMENT", "");
                    }
                    builder.Append(additionalChargesTxt);
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    string logFilePathOneWayBooking = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/OneWayBooking.txt"));
                    string textOneWay = System.IO.File.ReadAllText(logFilePathOneWayBooking);
                    textOneWay = textOneWay.Replace("BOOKINGDATETIME", booking.BookingDateTime.ToString());
                    textOneWay = textOneWay.Replace("BOOKINGAMOUNTONEWAY", booking.TotalDistancePrice.ToString());
                    if (booking.StudentDiscount > 1 && booking.BookingSource == (int)BookingSource_Enum.AO_Web)
                    {
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTDIV",
                            "<p>Student Discount  <span style='float:right;'>:</span></p>");
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + booking.StudentDiscount + "</p>");
                    }
                    else
                    {
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTDIV", "");
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                    }
                    builder.Append(textOneWay);
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    string logFilePathAirportDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/AirportDetails.txt"));
                    string textAirport = System.IO.File.ReadAllText(logFilePathAirportDetails);
                    textAirport = textAirport.Replace("FLIGHTNUMBER", booking.FlightNo);
                    textAirport = textAirport.Replace("FLIGHTLANDINGDATETIME", booking.FlightLandingDateTime.ToString());
                    textAirport = textAirport.Replace("FLIGHTDISPLAYNAME", booking.DriverDisplayName);
                    textAirport = textAirport.Replace("FLIGHTLANDINGDRIVERELLAPSETIME", booking.DriverEllapseTime.ToString());
                    textAirport = textAirport.Replace("BOOKINGAMOUNTONEWAY", booking.TotalDistancePrice.ToString());
                    textAirport = textAirport.Replace("PICKUPCHARGE", booking.PickupCharge.ToString());


                    if (booking.StudentDiscount > 1 && booking.BookingSource == (int)BookingSource_Enum.AO_Web)
                    {
                        textAirport = textAirport.Replace("STUDENTDISCOUNTDIV",
                            "<p>Student Discount  <span style='float:right;'>:</span></p>");
                        textAirport = textAirport.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + booking.StudentDiscount + "</p>");
                    }
                    else
                    {
                        textAirport = textAirport.Replace("STUDENTDISCOUNTDIV", "");
                        textAirport = textAirport.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                    }
                    builder.Append(textAirport);
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    string logFilePathCruisePortDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/CruisePortDetails.txt"));
                    string textCruiseport = System.IO.File.ReadAllText(logFilePathCruisePortDetails);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPNUMBER", booking.FlightNo);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPLANDINGDATETIME", booking.FlightLandingDateTime.ToString());
                    textCruiseport = textCruiseport.Replace("CRUISESHIPDISPLAYNAME", booking.DriverDisplayName);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPLANDINGDRIVERELLAPSETIME", booking.DriverEllapseTime.ToString());
                    textCruiseport = textCruiseport.Replace("BOOKINGAMOUNTONEWAY", booking.TotalDistancePrice.ToString());
                    if (booking.StudentDiscount > 1 && booking.BookingSource == (int)BookingSource_Enum.AO_Web)
                    {
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTDIV",
                            "<p>Student Discount  <span style='float:right;'>:</span></p>");
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + booking.StudentDiscount + "</p>");
                    }
                    else
                    {
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTDIV", "");
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                    }
                    builder.Append(textCruiseport);
                }
                //if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay)
                //{
                //    if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                //    {
                //        string logFilePathAirportReturnDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/AirportReturn.txt"));
                //        string textAirportReturn = System.IO.File.ReadAllText(logFilePathAirportReturnDetails);
                //        textAirportReturn = textAirportReturn.Replace("FLIGHT_NUMBER_RETURN", bookingReturn.FlightNo);
                //        textAirportReturn = textAirportReturn.Replace("FLIGHT_LANDINGDATE_TIME_RETURN", bookingReturn.FlightLandingDateTime.ToString());
                //        textAirportReturn = textAirportReturn.Replace("FLIGHT_DISPLAY_NAME_RETURN", bookingReturn.DriverDisplayName);
                //        textAirportReturn = textAirportReturn.Replace("FLIGHT_LANDING_DRIVER_ELLAPSE_TIME_RETURN", bookingReturn.DriverEllapseTime.ToString());
                //        textAirportReturn = textAirportReturn.Replace("RETURNDATETIME", bookingReturn.FlightLandingDateTime.ToString());
                //        textAirportReturn = textAirportReturn.Replace("BOOKINGAMOUNTTWOWAY", bookingReturn.TotalDistancePrice.ToString());
                //        textAirportReturn = textAirportReturn.Replace("PICKUPCHARGE", bookingReturn.PickupCharge.ToString());
                //        if (bookingReturn.StudentDiscount > 1 && bookingReturn.BookingSource == (int)BookingSource_Enum.AO_Web)
                //        {
                //            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTDIV",
                //                "<p>Student Discount  <span style='float:right;'>:</span></p>");
                //            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + bookingReturn.StudentDiscount + "</p>");
                //        }
                //        else
                //        {
                //            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTDIV", "");
                //            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                //        }
                //        builder.Append(textAirportReturn);
                //    }
                //    else if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                //    {
                //        string logFilePathCruiseportReturn = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/CruiseportReturn.txt"));
                //        string textCruiseportReturn = System.IO.File.ReadAllText(logFilePathCruiseportReturn);
                //        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_NUMBER_RETURN", bookingReturn.FlightNo);
                //        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_LANDING_DATE_TIME_RETURN", bookingReturn.FlightLandingDateTime.ToString());
                //        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_DISPLAY_NAME_RETURN", bookingReturn.DriverDisplayName);
                //        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_LANDING_DRIVER_ELLAPSE_TIME_RETURN", bookingReturn.DriverEllapseTime.ToString());
                //        textCruiseportReturn = textCruiseportReturn.Replace("RETURNDATETIME", bookingReturn.FlightLandingDateTime.ToString());
                //        textCruiseportReturn = textCruiseportReturn.Replace("BOOKINGAMOUNTTWOWAY", bookingReturn.TotalDistancePrice.ToString());
                //        if (bookingReturn.StudentDiscount > 1 && bookingReturn.BookingSource == (int)BookingSource_Enum.AO_Web)
                //        {
                //            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTDIV",
                //                "<p>Student Discount  <span style='float:right;'>:</span></p>");
                //            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + bookingReturn.StudentDiscount + "</p>");
                //        }
                //        else
                //        {
                //            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTDIV", "");
                //            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                //        }
                //        builder.Append(textCruiseportReturn);
                //    }
                //    if (bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                //    {
                //        string logFilePathTwoWayBooking = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/ReturnBooking.txt"));
                //        string textTwoWay = System.IO.File.ReadAllText(logFilePathTwoWayBooking);
                //        textTwoWay = textTwoWay.Replace("BOOKINGDATETIMERETURN", bookingReturn.BookingDateTime.ToString());
                //        textTwoWay = textTwoWay.Replace("BOOKINGAMOUNTTWOWAY", bookingReturn.TotalDistancePrice.ToString());
                //        if (bookingReturn.StudentDiscount > 1 && bookingReturn.BookingSource == (int)BookingSource_Enum.AO_Web)
                //        {
                //            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTDIV",
                //                "<p>Student Discount  <span style='float:right;'>:</span></p>");
                //            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + bookingReturn.StudentDiscount + "</p>");
                //        }
                //        else
                //        {
                //            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTDIV", "");
                //            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                //        }
                //        builder.Append(textTwoWay);
                //    }
                //}
                string logFilePathAddressDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/BookingAddress.txt"));
                string addresstext = System.IO.File.ReadAllText(logFilePathAddressDetails);
                //addresstext = addresstext.Replace("CUSTOMERCOMMENTS", booking.CustomerComment);
                //addresstext = addresstext.Replace("MAPURL", booking.MapUrl);
                addresstext = addresstext.Replace("BOOKING-DATE-TIME", booking.BookingDateTime.ToString());
                addresstext = addresstext.Replace("FROM-ADDRESS", booking.BookingFrom);
                addresstext = addresstext.Replace("FROMTOWN", booking.FromTown);
                addresstext = addresstext.Replace("FROMPOSTCODE", booking.BookingFromFullPostCode);
                addresstext = addresstext.Replace("TO-ADDRESS", booking.BookingTo);
                addresstext = addresstext.Replace("TOTOWN", booking.ToTown);
                addresstext = addresstext.Replace("TOPOSTCODE", booking.BookingToFullPostCode);

                #region Via Points

                StringBuilder builderViapoints = new StringBuilder();
                int viaid = 0;
                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var journeyViapoints = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty());
                    foreach (var viapoint in journeyViapoints.ToList())
                    {
                        viaid++;
                        string via = "<div style='width:97%; float: left; font-size:15px'>" +
                                     "<div   style='float:left; width:20%; color:#666666; font-weight:600; line-height: 10px;'>" +
                                     "<p style='margin-top:10px;'>Viapoint " + viaid +
                                     "<span style='float:right;'>:</span></p>" +
                                     "</div>" +
                                     "<div class='form-right' style='color:#666666; margin-left:10px; width: 75%; float: left;'>" +
                                     "<p style='margin-top:4px;line-height: 17px;'>" + viapoint.ViaFullAddress + "</p> " +
                                     "</div>" +
                                     "</div>";
                        builderViapoints.Append(via);
                    }
                    addresstext = addresstext.Replace("VIA-POINTS", builderViapoints.ToString());
                }
                else
                {
                    addresstext = addresstext.Replace("VIA-POINTS", "");
                }

                #endregion

                builder.Append(addresstext);
                if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay)
                {
                    //string logFilePathReturnBookingAddress = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/BookingAddressReturn.txt"));
                    //string returnBookingTxt = System.IO.File.ReadAllText(logFilePathReturnBookingAddress);
                    //if (bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport &&
                    //    bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                    //{
                    //    returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME", bookingReturn.BookingDateTime.ToString());
                    //}

                    //if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                    //{
                    //    returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME", bookingReturn.FlightLandingDateTime.ToString());
                    //}
                    //if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                    //{
                    //    returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME", bookingReturn.FlightLandingDateTime.ToString());
                    //}
                    //returnBookingTxt = returnBookingTxt.Replace("FROM-ADDRESS", bookingReturn.BookingFrom);
                    //returnBookingTxt = returnBookingTxt.Replace("FROMTOWN", bookingReturn.FromTown);
                    //returnBookingTxt = returnBookingTxt.Replace("FROMPOSTCODE", bookingReturn.BookingFromFullPostCode);
                    //returnBookingTxt = returnBookingTxt.Replace("TO-ADDRESS", bookingReturn.BookingTo);
                    //returnBookingTxt = returnBookingTxt.Replace("TOTOWN", bookingReturn.ToTown);
                    //returnBookingTxt = returnBookingTxt.Replace("TOPOSTCODE", bookingReturn.BookingToFullPostCode);

                    //#region Return Via Points

                    //StringBuilder builderViapointsReturn = new StringBuilder();
                    //int viaidReturn = 0;

                    //if (bookingReturn.JourneyViapoints != null && bookingReturn.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                    //{
                    //    var journeyViapointsReturn = bookingReturn.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty());
                    //    foreach (var viapoint in journeyViapointsReturn.ToList())
                    //    {
                    //        viaidReturn++;
                    //        string via = "<div style='width:97%; float: left; font-size:15px'>" +
                    //                     "<div   style='float:left; width:20%; color:#666666; font-weight:600; line-height: 10px;'>" +
                    //                     "<p style='margin-top:10px;'>Viapoint " + viaidReturn +
                    //                     "<span style='float:right;'>:</span></p>" +
                    //                     "</div>" +
                    //                     "<div class='form-right' style='color:#666666; margin-left:10px; width: 75%; float: left;'>" +
                    //                     "<p style='margin-top:4px;line-height: 17px;'>" + viapoint.ViaFullAddress + "</p> " +
                    //                     "</div>" +
                    //                     "</div>";
                    //        builderViapointsReturn.Append(via);
                    //    }
                    //    returnBookingTxt = returnBookingTxt.Replace("VIA-POINTS", builderViapointsReturn.ToString());
                    //}
                    //else
                    //{
                    //    returnBookingTxt = returnBookingTxt.Replace("VIA-POINTS", "");
                    //}

                    //#endregion

                    //builder.Append(returnBookingTxt);
                }
                else
                {
                    string fileClosingDiv = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/ClosingDiv.txt"));
                    string fileClosingDivTxt = System.IO.File.ReadAllText(fileClosingDiv);
                    builder.Append(fileClosingDivTxt);
                }
                string fileMap = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/Map.txt"));
                string textMap = System.IO.File.ReadAllText(fileMap);
                textMap = textMap.Replace("MAP-URL", booking.MapUrl);
                builder.Append(textMap);
                string fileLuggageInfo = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/LuggageInfo.txt"));
                string textLuggage = System.IO.File.ReadAllText(fileLuggageInfo);
                string vehicleType = string.Join("&", booking.BookingVehicleTypes.Select(x => x.VehicleType.VehicleTypeName).ToArray());

                textLuggage = textLuggage.Replace("BOOKINGVEHICLETYPES", vehicleType);
                textLuggage = textLuggage.Replace("NUMBEROFPESSANGERS", booking.NumberOfPassengers.ToString());
                textLuggage = textLuggage.Replace("NUMBEROFBABYSEATS", (booking.BoosterSeat + booking.InfantSeats + booking.ChildSeats).ToString());
                textLuggage = textLuggage.Replace("NUMBEROFLUGGAHES", booking.Luggage.ToString());
                textLuggage = textLuggage.Replace("NUMBEROFHANDLUGGAHES", booking.HandLuggage.ToString());

                if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay)
                {
                    textLuggage = textLuggage.Replace("BOOKINGAMOUNT", (booking.FinalPrice).ToString());
                }
                else
                {
                    textLuggage = textLuggage.Replace("BOOKINGAMOUNT", booking.FinalPrice.ToString());
                }
                //textLuggage = textLuggage.Replace("BOOKINGAMOUNT", booking.FinalPrice.ToString());

                builder.Append(textLuggage);
                string logFilePathFooderDetals = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/FooderDetals.txt"));
                string foodertext = System.IO.File.ReadAllText(logFilePathFooderDetals);
                foodertext = foodertext.Replace("BOOKING-URL", mainUrl);
                foodertext = foodertext.Replace("LOYALTY-PROGRAM-URL", loyaltyProgramUrl);
                foodertext = foodertext.Replace("CONTACT-US-URL", contactUsUrl);
                builder.Append(foodertext);
                var listEmails = new List<string>();
                if (booking.IsSendPassengerEmail)
                {
                    listEmails.Add(booking.PassengerEmail);
                    listEmails.Add(booking.BookedPersonEmail);
                }
                else
                {
                    listEmails.Add(booking.BookedPersonEmail);
                }

                //Sujanika, Your Reservation Confirmed at batransfer.com - Ref: BA544226
                string _emailSubject = booking.BookedPersonName + "," + "Your Reservation Confirmed at " + emailCredentials.DisplayName +
                                       " - Ref: " + booking.BookingNumber;


                var email = new RootEmail();
                email.Attachments = null;
                email.Body = builder.ToString();
                email.FromEmailAddress = emailCredentials.FromEmail;
                email.Username = emailCredentials.Username;
                email.Password = emailCredentials.Password;
                email.Port = emailCredentials.Port;
                email.Smtp = emailCredentials.Smtp;
                email.Subject = _emailSubject;
                email.ToEmailAddresses = listEmails;
                email.DisplayName = emailCredentials.DisplayName;
                email.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                EmailHelper.SendEmailWithoutAttachtment(email, apiUrl);
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }
        #endregion

        public async Task<EmailVerificationViewModel> SendQuickEmail(MailCustomeViewModel model, EmailAttachmentsViewMOdel emailAttachement)
        {
            var userVerify = new EmailVerificationViewModel();
            try
            {
                EmailCredentialsViewModel emailCredentials = EmailHelper.GetCustomerEmaillCredentials();

                var listEmails = new List<string>() { model.ToAddress };
                var emailRoot = new RootEmail();
                emailRoot.Attachments = null;
                emailRoot.Body = model.Message;
                emailRoot.FromEmailAddress = emailCredentials.FromEmail;
                emailRoot.Username = emailCredentials.Username;
                emailRoot.Password = emailCredentials.Password;
                emailRoot.Port = emailCredentials.Port;
                emailRoot.Smtp = emailCredentials.Smtp;
                emailRoot.Subject = model.Subject;
                emailRoot.ToEmailAddresses = listEmails;
                emailRoot.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailApi);
                userVerify.IsSuccess = await EmailHelper.SendEmail(emailRoot, apiUrl, emailAttachement);
                userVerify.Message = "Email successfully sent";

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return userVerify;
        }
        public async Task<EmailVerificationViewModel> SendQuickAdminEmail(MailAdminViewModel model, EmailAttachmentsViewMOdel emailAttachement)
        {
            var userVerify = new EmailVerificationViewModel();
            try
            {
                EmailCredentialsViewModel emailCredentials = EmailHelper.GetCustomerEmaillCredentials();

                var listEmails = new List<string>() { model.ToAddress };
                var emailRoot = new RootEmail();
                emailRoot.Attachments = null;
                emailRoot.Body = model.Message;
                emailRoot.FromEmailAddress = emailCredentials.FromEmail;
                emailRoot.Username = emailCredentials.Username;
                emailRoot.Password = emailCredentials.Password;
                emailRoot.Port = emailCredentials.Port;
                emailRoot.Smtp = emailCredentials.Smtp;
                emailRoot.Subject = model.Subject;
                emailRoot.ToEmailAddresses = listEmails;
                emailRoot.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailApi);
                userVerify.IsSuccess = await EmailHelper.SendEmail(emailRoot, apiUrl, emailAttachement);
                userVerify.Message = "Email successfully sent";

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return userVerify;
        }

        public List<ReservationHistoryViewModel> GetCurentbooking(string bookingnumber)
        {
            try
            {

                using (var db = new BATransferEntities())
                {
                    //         @PAYMENTSTATUS INT,
                    //         @DATE DATETIME,
                    //@BOOKINGSTATUS INT,
                    //         @COMPANYID BIGINT,
                    //@LOYALTYTYPE INT,
                    //         @ORGANIZATIONID INT
                    var today = BritishTime.GetDateTime();
                    IEnumerable<ReservationHistoryViewModel> objReservation =
                        db.sp_swaran_search_by_bookingnumber(bookingnumber, today).ToList().Select(x => new ReservationHistoryViewModel()
                        {
                            BookingId = x.BookingId,
                            BookingToken = HttpContext.Current.Server.UrlEncode(x.BookingToken),
                            Status = x.Status,
                            BookingSource = x.BookingSource,
                            BookingNumber = x.BookingNumber,
                            CreatedDate = x.CreatedDate != null ? x.CreatedDate.Value.ToString("dd-MMM-yy HH:mm") : "",
                            BookingDateTime = x.BookingDateTime.ToString("dd-MMM-yy HH:mm"),
                            JourneyType = x.JourneyType,
                            BookingFrom = x.BookingFrom,
                            BookingTo = x.BookingTo,
                            PaymentType = x.PaymentType,
                            Timeleft = x.Timeleft,
                            BookedPersonName = x.BookedPersonName,
                            LoyaltyType = x.LoyaltyType,
                            Country = x.Country,
                            VehicleTypes = x.VehicleTypes,
                            FromPostcode = x.BookingFromFullPostCode,
                            ToPostcode = x.BookingToFullPostCode,
                            IsReturnBooking = x.IsReturnBooking,
                            AutocabNumber = x.AutocabNumber,
                            IsSentToGhost = x.IsSentToGhost ?? false,
                            RemainingHours = x.RemainingHours ?? 0,
                            PaymentStatus = x.PaymentStatus,
                            PaymentTypeId = x.PaymentTypeId


                        });
                    return objReservation.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        #region Change Payment methods
        public BookingSummaryDom ChangePayment(CurrentBookingDisplayViewModel model)
        {
            model.PaymentMethodId = model.PaymentMethodId == (int)PaymentTypeEnum.Use_My_Credits && model.IsUseMyCreditCardPayment
                                                                 ? (int)PaymentTypeEnum.Card
                                                                 : model.PaymentMethodId;
            BookingSummaryDom summary = new BookingSummaryDom();

            try
            {
                if (!string.IsNullOrEmpty(model.AutocabAuthorizationReference))
                {
                    var autocabCancelObj = AutoCapVehicletypeRequest.AgentBookingCancellationRequestAgent(
                                            model.AutocabAuthorizationReference);
                    if (!autocabCancelObj.Status)
                    {
                        //  summary.ToFullAddress = "Can not delete booking in ghost";
                    }
                }
                summary = OnewayChangePaymentmethod(model);
                summary = ReturnwayChangePaymentmethod(model, summary);

                if (model.PaymentMethodId == (int)PaymentTypeEnum.Card || model.PaymentMethodId == 100)
                {
                    if (summary.IsBookingFeePaid)
                    {
                        summary.FinalPrice = summary.OneWayPrice + summary.ReturnWayPrice - (summary.OneWayCredit + summary.ReturnWayCredit) - summary.OnewayOtherDiscounts - summary.ReturnWayOtherDiscounts;
                    }
                    else
                    {
                        summary.FinalPrice = summary.OneWayPrice + summary.ReturnWayPrice + summary.BookingFee - (summary.OneWayCredit + summary.ReturnWayCredit) - summary.OnewayOtherDiscounts - summary.ReturnWayOtherDiscounts;
                    }

                    if (model.PaymentMethodId == (int)PaymentTypeEnum.Card)
                    {
                        string description = "Booking/" + Enum.GetName(typeof(BookingSource_Enum), summary.BookingSource).ToString() + "/" + summary.BookingNumber;
                        string encodeDescription = HttpContext.Current.Server.UrlEncode(description);
                        string encodeToken = HttpContext.Current.Server.UrlEncode(model.BookingToken);
                        string path = string.Empty;
                        Uri uriResult;
                        path = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                        string callBackurl = HttpContext.Current.Server.UrlEncode(path + "/BookingSummary/Details?tkn=" + encodeToken);//"&amnt=" + booking.TotalPrice + "
                        string paymentUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.PaymentApi) + "Payment/CapturePayment" + "?tkn=" + encodeToken + "&amnt=" + summary.FinalPrice + "&desc=" + encodeDescription + "&bnu=" + HttpContext.Current.Server.UrlEncode(summary.BookingNumber) + "&cbu=" + callBackurl;
                        summary.RedirectPaymentUrl = paymentUrl;
                    }
                    else
                    {
                        SendChangePayment(model);
                    }

                }
                if (model.PaymentMethodId == (int)PaymentTypeEnum.Pay_Booking_Fee)
                {
                    string description = "BookingFee/" + Enum.GetName(typeof(BookingSource_Enum), summary.BookingSource) + "/" + summary.BookingNumber;
                    string encodeDescription = System.Web.HttpContext.Current.Server.UrlEncode(description);
                    string encodeToken = HttpContext.Current.Server.UrlEncode(model.BookingToken);
                    string path = string.Empty;
                    Uri uriResult;
                    path = System.Web.HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                    string callBackurl = System.Web.HttpContext.Current.Server.UrlEncode(path + "/BookingSummary/CashBookingFeeView?tkn=" + encodeToken + "&err = 1&msg=ok");//"&amnt=" + booking.TotalPrice + "
                    string bookingFeeUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.PaymentApi) + "Payment/CaptureBookingFee" + "?amnt=" + summary.BookingFee + "&desc=" + encodeDescription + "&bnu=" + System.Web.HttpContext.Current.Server.UrlEncode(summary.BookingNumber) + "&cbu=" + callBackurl;
                    summary.RedirectPaymentUrl = bookingFeeUrl;

                }
                if (model.PaymentMethodId != (int)PaymentTypeEnum.Pay_Booking_Fee)
                {
                    SendChangePayment(model);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return summary;
        }

        public List<PaymentTypeDom> GetCurrentPaymentMethods(string bookingtoken, int paymentTypeId)
        {

            var payment = new List<PaymentTypeDom>();
            var booking = new Booking();
            try
            {
                using (var db = new BATransferEntities())
                {

                    booking = db.Bookings.FirstOrDefault(x => x.BookingToken.Equals(bookingtoken));
                    if (booking == null)
                    {
                        return null;
                    }
                }
                payment = GetPaymentTypes(booking.UserId, paymentTypeId);

                var isFreeJourney = FindIsFreeJournery(booking);

                if (!isFreeJourney)
                {
                    var itemToRemove = payment.SingleOrDefault(r => r.PaymentTypeId == (int)PaymentTypeEnum.Free_Journey);
                    if (itemToRemove != null)
                        payment.Remove(itemToRemove);

                    //  reservation.Surcharge = combination.OnewaySurcharge;
                }
                else
                {
                    //   reservation.Surcharge = _repositiry.FindIsFreeJournerySurcharge(address);
                }

                var isUseCredit = FindUseMyCredit(booking.UserId);
                if (!isUseCredit)
                {
                    var usemyCredits = payment.SingleOrDefault(r => r.PaymentTypeId == (int)PaymentTypeEnum.Use_My_Credits);
                    payment.Remove(usemyCredits);
                }
                if (booking.PaymentStatus != (int)PaymentStatusEnum.BookingFeePending)
                {
                    var payBookingFee = payment.SingleOrDefault(r => r.PaymentTypeId == (int)PaymentTypeEnum.Pay_Booking_Fee);
                    payment.Remove(payBookingFee);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return payment;
        }

        public List<PaymentTypeDom> GetPaymentTypes(string userId, int paymentTypeId)
        {
            var payment = new List<PaymentTypeDom>();// new PaymentViewModel();
            //     bool isFreeJourneyAvailable = false;
            try
            {
                using (var db = new BATransferEntities())
                {

                    if (db.PaymentTypes.Any(pt => pt.IsEnabled))
                    {
                        var allActivePaymentTypes = new List<PaymentTypeDom>();

                        allActivePaymentTypes =
                            db.PaymentTypes.Where(pt => pt.IsEnabled).Select(ptt => new PaymentTypeDom()
                            {
                                PaymentTypeId = ptt.PaymentTypeId,
                                PaymentTypeName = ptt.PaymentTypeName
                            }).ToList();


                        if (paymentTypeId == (int)PaymentTypeEnum.Card || paymentTypeId == (int)PaymentTypeEnum.Cash)
                        {
                            PaymentTypeDom manualCreditCard = new PaymentTypeDom()
                            {
                                PaymentTypeId = 100,
                                PaymentTypeName = "Manual CreditCard"
                            };

                            allActivePaymentTypes.Add(manualCreditCard);
                        }

                        if (allActivePaymentTypes.Any())
                        {
                            List<PaymentTypeDom> allOtherPaymentTypes =
                                   allActivePaymentTypes.Select(ptt => new PaymentTypeDom()
                                   {
                                       PaymentTypeId = ptt.PaymentTypeId,
                                       PaymentTypeName = ptt.PaymentTypeName
                                   }).ToList();

                            return allOtherPaymentTypes;
                        }
                        ;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return payment;
        }

        public bool FindIsFreeJournery(Booking booking)
        {

            try
            {
                string fromAirportCode = string.Empty;
                string toAirportCode = string.Empty;
                int bookingSiteId = OrganizationHelper.GetRelevantOrganizationFromSite(booking.BookingSource);
                using (var db = new BATransferEntities())
                {
                    var fromAdd = db.PointOfInterest_Place.FirstOrDefault(x => x.AriaPointId == booking.FromFixedPriceAriaPointId);
                    if (fromAdd != null)
                    {
                        fromAirportCode = fromAdd.SearchName;
                    }

                    var toAdd = db.PointOfInterest_Place.FirstOrDefault(x => x.AriaPointId == booking.ToFixedPriceAriaPointId);
                    if (toAdd != null)
                    {
                        toAirportCode = toAdd.SearchName;
                    }

                    string fromAreaType = Enum.GetName(typeof(AreapointTypeEnum), booking.FromAriaPointTypeId);
                    string toAreaType = Enum.GetName(typeof(AreapointTypeEnum), booking.ToAriaPointTypeId);
                    int vehicleTypeid = 0;
                    var objBooking = db.Bookings.FirstOrDefault(x => x.BookingId == booking.BookingId);
                    if (objBooking != null)
                    {
                        var objvehicle = objBooking.BookingVehicleTypes.FirstOrDefault();
                        if (objvehicle != null)
                        {
                            vehicleTypeid = (int)objvehicle.VehicleTypeId;
                        }
                    }


                    bool isReturnWay = booking.JourneyType == (int)JourneyTypeEnum.TwoWay;
                    var objFreeJourneyAvailability = db.CheckFreeJourneyAvailability(booking.TotalDistance, booking.TotalDistancePrice, isReturnWay, vehicleTypeid,
                         fromAreaType, toAreaType, booking.UserId, fromAirportCode, toAirportCode, bookingSiteId).FirstOrDefault();

                    return objFreeJourneyAvailability.Value;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return false;
        }

        public bool FindUseMyCredit(string userId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.Customers.Any(x => x.userId.Equals(userId)))
                    {
                        return db.Customers.First(x => x.userId.Equals(userId)).TotalCredits > 0;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return false;
        }


        public UserCreditViewModel GetCustomerCredits(string userId)
        {
            var userCredit = new UserCreditViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.Customers.Any(x => x.userId.Equals(userId)))
                    {
                        userCredit.AvailableCashBalance = db.Customers.First(x => x.userId.Equals(userId)).TotalCredits;

                    }
                    if (db.Organizations.Any(bse => bse.OrganizationId == (int)BookingSiteEnum.BA))
                    {
                        var objOrg = db.Organizations.First(bse => bse.OrganizationId == (int)BookingSiteEnum.BA);
                        userCredit.CardProcesingFee = objOrg.CardProcessingFee;

                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return userCredit;
        }
        //public decimal GetBookingFee(int paymenttypeId, int siteid)
        //{
        //    try
        //    {
        //        using (var db = new BATransferEntities())
        //        {
        //            var objBookingFee = db.OrganizationPaymentBookingCharges.FirstOrDefault(x => x.OrganizationId == siteid && x.PaymentTypeId == paymenttypeId && x.IsEnabled && !x.IsDeleted);
        //            if (objBookingFee != null)
        //            {
        //                return objBookingFee.BookingCharge;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

        //    }
        //    return 0;
        //}

        public BookingFeeViewModel GetBookingFee(int paymenttypeId, int siteid, decimal journeyAmount, string totken)
        {
            var bookingFee = new BookingFeeViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objbooking = db.Bookings.Where(x => x.BookingToken.Equals(totken));
                    foreach (var booking in objbooking.ToList())
                    {
                        if (booking.IsBookingFeePaid ?? false)
                        {
                            bookingFee.BookingFee = 0;
                            bookingFee.AdditionalCharge = 0;
                            return bookingFee;
                        }
                    }
                    var objBookingFee = db.BookingFeeCalculation(siteid, paymenttypeId, journeyAmount).FirstOrDefault();
                    if (objBookingFee != null)
                    {
                        bookingFee.BookingFee = objBookingFee.Value;
                        bookingFee.AdditionalCharge = 0;
                        return bookingFee;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return null;
        }


        public void SubtractCustomerCredits(Booking booking)
        {
            using (var db = new BATransferEntities())
            {
                if (db.Customers.Any(x => x.userId.Equals(booking.UserId) && booking.ConsumerDebit > 0))
                {
                    if (booking.ConsumerDebit > 0)
                    {
                        var customerObj = db.Customers.First(x => x.userId.Equals(booking.UserId));
                        customerObj.TotalCredits = customerObj.TotalCredits - booking.ConsumerDebit;
                        db.SaveChanges();
                        var discount = new CustomerTransaction();
                        discount.BookingId = booking.BookingId;
                        discount.TransactionTypeId = (int)TransactionTypeValEnum.DebitByCustomer;
                        discount.TransactionReference = "";
                        discount.InitialCredit = customerObj.TotalCredits; // ValueParseHelper.TryParseNullDate(model.ExpiryDate);
                        discount.TransactionCredit = 0;
                        discount.TransactionDebit = booking.ConsumerDebit;
                        discount.FinalCredit = customerObj.TotalCredits;
                        discount.CustomerUserId = booking.UserId;
                        discount.IsEnabled = true;
                        discount.IsDeleted = false;
                        discount.IsHold = true;
                        discount.Description = booking.BookingNumber + " (" +
                                               (booking.ConsumerDebit) + " Hold)";
                        discount.StaffUserId = null;
                        discount.sysCreatedDate = DateTime.UtcNow;
                        discount.sysModifiedDate = DateTime.UtcNow;
                        db.CustomerTransactions.Add(discount);
                        db.SaveChanges();
                    }
                    //if (booking.ReturnWayCredit > 0)
                    //{
                    //    var customerObj = db.Customers.First(x => x.userId.Equals(booking.UserId));
                    //    customerObj.TotalCredits = customerObj.TotalCredits - booking.ReturnWayCredit;
                    //    db.SaveChanges();
                    //    var discount = new CustomerTransaction();
                    //    discount.BookingId = returnBookingId;
                    //    discount.TransactionTypeId = (int)TransactionTypeValEnum.DebitByCustomer;
                    //    discount.TransactionReference = "";
                    //    discount.InitialCredit = customerObj.TotalCredits; // ValueParseHelper.TryParseNullDate(model.ExpiryDate);
                    //    discount.TransactionCredit = 0;
                    //    discount.TransactionDebit = booking.ReturnWayCredit;
                    //    discount.FinalCredit = customerObj.TotalCredits;
                    //    discount.CustomerUserId = booking.UserId;
                    //    discount.IsEnabled = true;
                    //    discount.IsDeleted = false;
                    //    discount.IsHold = true;
                    //    discount.Description = booking.BookingNumber + " Return Journey (" +
                    //                           (booking.ReturnWayCredit) + " Hold)";
                    //    discount.StaffUserId = null;
                    //    discount.sysCreatedDate = DateTime.UtcNow;
                    //    discount.sysModifiedDate = DateTime.UtcNow;
                    //    db.CustomerTransactions.AddObject(discount);
                    //    db.SaveChanges();
                    //}
                }
            }
        }
        #endregion

        #region Pay to Driver

        private BookingSummaryDom OnewayChangePaymentmethod(CurrentBookingDisplayViewModel currentBooking)
        {
            string loyalty = string.Empty;
            var summary = new BookingSummaryDom();

            try
            {
                using (var db = new BATransferEntities())
                {

                    Booking booking = null;

                    if (currentBooking.PaymentMethodId == 100)
                    {
                        //booking =
                        //    db.Bookings.FirstOrDefault(
                        //        x =>
                        //            x.BookingToken.Equals(currentBooking.BookingToken) && (x.PaymentMethod == (int)PaymentTypeEnum.Card));
                        booking =
                            db.Bookings.FirstOrDefault(
                                x =>
                                    x.BookingToken.Equals(currentBooking.BookingToken) && (x.PaymentMethod == (int)PaymentTypeEnum.Card || x.PaymentMethod == (int)PaymentTypeEnum.Cash));

                        if (booking == null)
                        {
                            return summary;
                        }

                        if (booking.Status != (int)BookingStatusEnum.Requested && booking.Status != (int)BookingStatusEnum.Confirmed)
                        {
                            return summary;
                        }

                        int currentPaymentMethod = booking.PaymentMethod;

                        booking.PaymentStatus = (int)PaymentStatusEnum.Paid;
                        booking.ModifiedDate = DateTime.UtcNow;
                        booking.PaymentMethod = (int)PaymentTypeEnum.Card;

                        //var cardBookingPayment = booking.BookingPayments.FirstOrDefault(bp => bp.PaymentTypeId == (int)PaymentTypeEnum.Card);
                        var cardBookingPayment = booking.BookingPayments.FirstOrDefault(bp => bp.PaymentTypeId == currentPaymentMethod);

                        if (cardBookingPayment != null)
                        {
                            cardBookingPayment.PaymentTypeId = (int)PaymentTypeEnum.Card;
                            cardBookingPayment.Status = (int)PaymentStatusEnum.Paid;
                            cardBookingPayment.PaymentDate = DateTime.UtcNow;
                            cardBookingPayment.sysModifiedDate = DateTime.UtcNow;
                        }

                        var tempBooking = db.Booking_TEMP.FirstOrDefault(x => x.BookingId == booking.BookingId);

                        if (tempBooking != null)
                        {
                            tempBooking.PaymentStatus = (int)PaymentStatusEnum.Paid;
                            tempBooking.ModifiedDate = DateTime.UtcNow;
                            tempBooking.PaymentMethod = (int)PaymentTypeEnum.Card;
                            //db.SaveChanges();

                            //var cardBookingPaymentTemp = tempBooking.BookingPayment_TEMP.FirstOrDefault(bp => bp.PaymentTypeId == (int)PaymentTypeEnum.Card);
                            var cardBookingPaymentTemp = tempBooking.BookingPayment_TEMP.FirstOrDefault(bp => bp.PaymentTypeId == currentPaymentMethod);

                            if (cardBookingPaymentTemp != null)
                            {
                                cardBookingPaymentTemp.PaymentTypeId = (int)PaymentTypeEnum.Card;
                                cardBookingPaymentTemp.Status = (int)PaymentStatusEnum.Paid;
                                cardBookingPaymentTemp.PaymentDate = DateTime.UtcNow;
                                cardBookingPaymentTemp.sysModifiedDate = DateTime.UtcNow;
                            }
                            else
                            {
                                var objBookingPayment_TEMP = new BookingPayment_TEMP();
                                objBookingPayment_TEMP.BookingId = booking.BookingId;
                                objBookingPayment_TEMP.PaymentTypeId = (int)PaymentTypeEnum.Card;
                                objBookingPayment_TEMP.PaymentDate = DateTime.Now;
                                objBookingPayment_TEMP.PaymentAmount = (currentBooking.OneWayPrice + currentBooking.BookingFee) - currentBooking.OneWayCredit;
                                objBookingPayment_TEMP.Status = (int)PaymentStatusEnum.Paid;
                                objBookingPayment_TEMP.IsDeleted = false;
                                objBookingPayment_TEMP.IsEnabled = true;
                                objBookingPayment_TEMP.sysModifiedDate = DateTime.UtcNow;
                                objBookingPayment_TEMP.sysCreatedDate = DateTime.UtcNow;
                                db.BookingPayment_TEMP.Add(objBookingPayment_TEMP);
                                //db.SaveChanges();

                                var objBookingPayment = new BookingPayment();
                                objBookingPayment.BookingPaymentId = objBookingPayment_TEMP.BookingPaymentId;
                                objBookingPayment.BookingId = objBookingPayment_TEMP.BookingId;
                                objBookingPayment.PaymentTypeId = objBookingPayment_TEMP.PaymentTypeId;
                                objBookingPayment.PaymentDate = objBookingPayment_TEMP.PaymentDate;
                                objBookingPayment.PaymentAmount = objBookingPayment_TEMP.PaymentAmount;
                                objBookingPayment.Status = objBookingPayment_TEMP.Status;
                                objBookingPayment.IsDeleted = objBookingPayment_TEMP.IsDeleted;
                                objBookingPayment.IsEnabled = objBookingPayment_TEMP.IsEnabled;
                                objBookingPayment.RefundAmount = objBookingPayment_TEMP.RefundAmount;
                                objBookingPayment.sysModifiedDate = DateTime.UtcNow;
                                objBookingPayment.sysCreatedDate = DateTime.UtcNow;
                                db.BookingPayments.Add(objBookingPayment);
                            }
                        }

                        db.SaveChanges();
                    }
                    else
                    {
                        booking =
                            db.Bookings.FirstOrDefault(
                                x =>
                                    x.BookingToken.Equals(currentBooking.BookingToken));
                        if (booking == null)
                        {
                            return summary;
                        }

                        if (booking.Status != (int)BookingStatusEnum.Requested && booking.Status != (int)BookingStatusEnum.Confirmed)
                        {
                            return summary;
                        }

                        booking.PaymentStatus = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Free_Journey
                          ? (int)PaymentStatusEnum.Paid
                          : (int)PaymentStatusEnum.Pending;
                        booking.CardTypeId = null;
                        booking.CardPaymentTypeId = null;
                        booking.OtherCharges = currentBooking.BookingFee;
                        booking.TotalPrice = currentBooking.OneWayPrice;
                        booking.FinalPrice = (currentBooking.OneWayPrice + currentBooking.BookingFee) - currentBooking.OneWayCredit;

                        booking.ConsumerDebit = currentBooking.OneWayCredit;
                        booking.PaymentMethod = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Pay_Booking_Fee
                                               ? (int)PaymentTypeEnum.Cash
                                               : currentBooking.PaymentMethodId;
                        booking.Autocab_AuthorizationReference = "";
                        booking.Autocab_BookingReference = "";
                        booking.AutocabNumber = "";
                        booking.Autocab_IsBookingCancelled = true;
                        db.SaveChanges();

                        var tempBooking = db.Booking_TEMP.FirstOrDefault(x => x.BookingId == booking.BookingId);
                        if (tempBooking != null)
                        {
                            tempBooking.PaymentStatus = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Free_Journey
                        ? (int)PaymentStatusEnum.Paid
                        : (int)PaymentStatusEnum.Pending;
                            tempBooking.CardTypeId = null;
                            tempBooking.CardPaymentTypeId = null;
                            tempBooking.OtherCharges = currentBooking.BookingFee;
                            tempBooking.TotalPrice = currentBooking.OneWayPrice;
                            tempBooking.ConsumerDebit = currentBooking.OneWayCredit;
                            tempBooking.FinalPrice = (currentBooking.OneWayPrice + currentBooking.BookingFee) - currentBooking.OneWayCredit;
                            // tempBooking.PaymentMethod = currentBooking.PaymentMethodId;
                            tempBooking.PaymentMethod = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Pay_Booking_Fee
                                              ? (int)PaymentTypeEnum.Cash
                                              : currentBooking.PaymentMethodId;
                            tempBooking.Autocab_AuthorizationReference = "";
                            tempBooking.Autocab_BookingReference = "";
                            tempBooking.AutocabNumber = "";
                            tempBooking.Autocab_IsBookingCancelled = true;
                            db.SaveChanges();
                        }

                        var objBookingPayment_TEMP = new BookingPayment_TEMP();
                        objBookingPayment_TEMP.BookingId = booking.BookingId;
                        // objBookingPayment_TEMP.PaymentTypeId = currentBooking.PaymentMethodId;
                        objBookingPayment_TEMP.PaymentTypeId = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Pay_Booking_Fee
                                              ? (int)PaymentTypeEnum.Cash
                                              : currentBooking.PaymentMethodId;
                        objBookingPayment_TEMP.PaymentDate = DateTime.Now;
                        objBookingPayment_TEMP.PaymentAmount = (currentBooking.OneWayPrice + currentBooking.BookingFee) - currentBooking.OneWayCredit;
                        objBookingPayment_TEMP.Status = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Free_Journey
                          ? (int)PaymentStatusEnum.Paid
                          : (int)PaymentStatusEnum.Pending;
                        objBookingPayment_TEMP.IsDeleted = false;
                        objBookingPayment_TEMP.IsEnabled = true;
                        objBookingPayment_TEMP.sysModifiedDate = DateTime.UtcNow;
                        objBookingPayment_TEMP.sysCreatedDate = DateTime.UtcNow;
                        db.BookingPayment_TEMP.Add(objBookingPayment_TEMP);
                        db.SaveChanges();

                        var objBookingPayment = new BookingPayment();
                        objBookingPayment.BookingPaymentId = objBookingPayment_TEMP.BookingPaymentId;
                        objBookingPayment.BookingId = booking.BookingId;
                        // objBookingPayment.PaymentTypeId = currentBooking.PaymentMethodId;
                        objBookingPayment.PaymentTypeId = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Pay_Booking_Fee
                                              ? (int)PaymentTypeEnum.Cash
                                              : currentBooking.PaymentMethodId;
                        objBookingPayment.PaymentDate = DateTime.Now;
                        objBookingPayment.PaymentAmount = (currentBooking.OneWayPrice + currentBooking.BookingFee) - currentBooking.OneWayCredit;
                        objBookingPayment.Status = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Free_Journey
                       ? (int)PaymentStatusEnum.Paid
                       : (int)PaymentStatusEnum.Pending;
                        objBookingPayment.IsDeleted = false;
                        objBookingPayment.IsEnabled = true;
                        objBookingPayment.RefundAmount = 0;
                        objBookingPayment.sysModifiedDate = DateTime.UtcNow;
                        objBookingPayment.sysCreatedDate = DateTime.UtcNow;
                        db.BookingPayments.Add(objBookingPayment);
                        db.SaveChanges();

                        if (currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Use_My_Credits)
                        {
                            SubtractCustomerCredits(booking);
                        }

                        if (currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Free_Journey)
                        {
                            if (db.CustomerFreeJourneys != null &&
                                db.CustomerFreeJourneys.Any(
                                    x => x.UserId.Equals(booking.UserId) && x.Code.Equals(currentBooking.FreeJourneyCode) && !x.IsUsed && x.IsEnabled))
                            {
                                var freeJourney = db.CustomerFreeJourneys.First(
                                    x =>
                                        x.UserId.Equals(booking.UserId) && x.Code.Equals(currentBooking.FreeJourneyCode) &&
                                        !x.IsUsed && x.IsEnabled);
                                freeJourney.BookingId = booking.BookingId;
                                freeJourney.IsEnabled = false;
                                db.SaveChanges();
                                var freejourneyPoints =
                                    db.BookingPoints.FirstOrDefault(
                                        x => x.BookingPointsId == (int)FreeJourneyPointsTypeEnum.FreeJourney);
                                if (freejourneyPoints != null)
                                {
                                    var customer = db.Customers.FirstOrDefault(x => x.userId.Equals(booking.UserId));
                                    if (customer != null)
                                    {
                                        customer.availablePoints = customer.availablePoints - freejourneyPoints.Points;
                                        customer.HoldPoints = customer.HoldPoints + freejourneyPoints.Points;
                                        db.SaveChanges();
                                    }
                                    var bookingPoint = new BookingPoint();
                                    bookingPoint.BookingId = booking.BookingId;
                                    bookingPoint.PointsTypeId = 1;
                                    bookingPoint.Points = -freejourneyPoints.Points;
                                    bookingPoint.TotalPoints = bookingPoint.TotalPoints - freejourneyPoints.Points;
                                    bookingPoint.IsDeleted = false;
                                    bookingPoint.IsEnabled = false;
                                    bookingPoint.sysCreatedDate = DateTime.UtcNow;
                                    bookingPoint.sysModifiedDate = DateTime.UtcNow;
                                    db.BookingPoints.Add(bookingPoint);
                                    db.SaveChanges();
                                }
                            }
                        }
                        summary.IsBookingFeePaid = booking.IsBookingFeePaid ?? false;
                        summary.BookingFee = currentBooking.BookingFee;
                        summary.BookingStatus = booking.Status;
                        summary.BookingNumber = booking.BookingNumber;
                        summary.BookingSource = booking.BookingSource;
                        summary.BookingDateTime = ValueParseHelper.ToStringDateTime(booking.BookingDateTime);
                        summary.FromFullAddress = booking.BookingFrom;
                        summary.Viapoints = booking.JourneyViapoints != null
                                            ? string.Join(", ", booking.JourneyViapoints.Select(x => x.ViaFullAddress)) : "";
                        summary.ToFullAddress = booking.BookingTo;

                        summary.VehicleTypesName = booking.BookingVehicleTypes != null
                                                ? string.Join(", ", booking.BookingVehicleTypes.Select(x => x.VehicleType.VehicleTypeName))
                                                : "";
                        summary.VehicleTypesNameUrl = booking.MapUrl;
                        summary.PaymentType = Enum.GetName(typeof(PaymentTypeEnum), booking.PaymentMethod);


                        summary.NumberOfPassanger = booking.NumberOfPassengers;
                        summary.NumberOfLuggages = booking.Luggage;
                        summary.NumberHandOfLuggages = booking.HandLuggage;
                        summary.PaymentStatus = booking.PaymentStatus;

                        summary.OneWayCredit = booking.ConsumerDebit;
                        summary.OneWayPrice = booking.TotalDistancePrice;
                        summary.OtherCharges = booking.OtherCharges;
                        summary.TotalDistancePrice = booking.TotalDistancePrice;
                        summary.OnewayOtherDiscounts = booking.OtherDiscount;

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return summary;
        }

        private BookingSummaryDom ReturnwayChangePaymentmethod(CurrentBookingDisplayViewModel currentBooking, BookingSummaryDom summary)
        {
            string loyalty = string.Empty;

            try
            {
                using (var db = new BATransferEntities())
                {

                    Booking booking = null;

                    //  string encodeToken = HttpContext.Current.Server.UrlDecode(currentBooking.BookingToken);

                    if (currentBooking.PaymentMethodId == 100)
                    {
                        //booking =
                        //db.Bookings.FirstOrDefault(
                        //    x =>
                        //        x.BookingToken.Equals(currentBooking.BookingToken) && x.IsReturnBooking && x.PaymentMethod == (int)PaymentTypeEnum.Card);

                        booking =
                        db.Bookings.FirstOrDefault(
                            x =>
                                x.BookingToken.Equals(currentBooking.BookingToken) && x.IsReturnBooking && (x.PaymentMethod == (int)PaymentTypeEnum.Card || x.PaymentMethod == (int)PaymentTypeEnum.Cash));

                        if (booking == null)
                        {
                            return summary;
                        }
                        if (booking.Status != (int)BookingStatusEnum.Requested && booking.Status != (int)BookingStatusEnum.Confirmed)
                        {
                            return summary;
                        }

                        int currentPaymentMethod = booking.PaymentMethod;

                        booking.PaymentStatus = (int)PaymentStatusEnum.Paid;
                        booking.ModifiedDate = DateTime.UtcNow;
                        booking.PaymentMethod = (int)PaymentTypeEnum.Card;

                        //var cardBookingPayment = booking.BookingPayments.FirstOrDefault(bp => bp.PaymentTypeId == (int)PaymentTypeEnum.Card);
                        var cardBookingPayment = booking.BookingPayments.FirstOrDefault(bp => bp.PaymentTypeId == currentPaymentMethod);

                        if (cardBookingPayment != null)
                        {
                            cardBookingPayment.PaymentTypeId = (int)PaymentTypeEnum.Card;
                            cardBookingPayment.Status = (int)PaymentStatusEnum.Paid;
                            cardBookingPayment.PaymentDate = DateTime.UtcNow;
                            cardBookingPayment.sysModifiedDate = DateTime.UtcNow;
                        }

                        var tempBooking = db.Booking_TEMP.FirstOrDefault(x => x.BookingId == booking.BookingId);

                        if (tempBooking != null)
                        {
                            tempBooking.PaymentStatus = (int)PaymentStatusEnum.Paid;
                            tempBooking.ModifiedDate = DateTime.UtcNow;
                            tempBooking.PaymentMethod = (int)PaymentTypeEnum.Card;
                            //db.SaveChanges();

                            //var cardBookingPaymentTemp = tempBooking.BookingPayment_TEMP.FirstOrDefault(bp => bp.PaymentTypeId == (int)PaymentTypeEnum.Card);
                            var cardBookingPaymentTemp = tempBooking.BookingPayment_TEMP.FirstOrDefault(bp => bp.PaymentTypeId == currentPaymentMethod);

                            if (cardBookingPaymentTemp != null)
                            {
                                cardBookingPaymentTemp.PaymentTypeId = (int)PaymentTypeEnum.Card;
                                cardBookingPaymentTemp.Status = (int)PaymentStatusEnum.Paid;
                                cardBookingPaymentTemp.PaymentDate = DateTime.UtcNow;
                                cardBookingPaymentTemp.sysModifiedDate = DateTime.UtcNow;
                            }
                            else
                            {
                                var objBookingPayment_TEMP = new BookingPayment_TEMP();
                                objBookingPayment_TEMP.BookingId = booking.BookingId;
                                objBookingPayment_TEMP.PaymentTypeId = (int)PaymentTypeEnum.Card;
                                objBookingPayment_TEMP.PaymentDate = DateTime.Now;
                                objBookingPayment_TEMP.PaymentAmount = (currentBooking.OneWayPrice + currentBooking.BookingFee) - currentBooking.OneWayCredit;
                                objBookingPayment_TEMP.Status = (int)PaymentStatusEnum.Paid;
                                objBookingPayment_TEMP.IsDeleted = false;
                                objBookingPayment_TEMP.IsEnabled = true;
                                objBookingPayment_TEMP.sysModifiedDate = DateTime.UtcNow;
                                objBookingPayment_TEMP.sysCreatedDate = DateTime.UtcNow;
                                db.BookingPayment_TEMP.Add(objBookingPayment_TEMP);
                                //db.SaveChanges();

                                var objBookingPayment = new BookingPayment();
                                objBookingPayment.BookingPaymentId = objBookingPayment_TEMP.BookingPaymentId;
                                objBookingPayment.BookingId = objBookingPayment_TEMP.BookingId;
                                objBookingPayment.PaymentTypeId = objBookingPayment_TEMP.PaymentTypeId;
                                objBookingPayment.PaymentDate = objBookingPayment_TEMP.PaymentDate;
                                objBookingPayment.PaymentAmount = objBookingPayment_TEMP.PaymentAmount;
                                objBookingPayment.Status = objBookingPayment_TEMP.Status;
                                objBookingPayment.IsDeleted = objBookingPayment_TEMP.IsDeleted;
                                objBookingPayment.IsEnabled = objBookingPayment_TEMP.IsEnabled;
                                objBookingPayment.RefundAmount = objBookingPayment_TEMP.RefundAmount;
                                objBookingPayment.sysModifiedDate = DateTime.UtcNow;
                                objBookingPayment.sysCreatedDate = DateTime.UtcNow;
                                db.BookingPayments.Add(objBookingPayment);
                            }
                        }

                        db.SaveChanges();
                    }
                    else
                    {

                        booking =
                        db.Bookings.FirstOrDefault(
                            x =>
                                x.BookingToken.Equals(currentBooking.BookingToken) && x.IsReturnBooking);
                        if (booking == null)
                        {
                            return summary;
                        }
                        if (booking.Status != (int)BookingStatusEnum.Requested && booking.Status != (int)BookingStatusEnum.Confirmed)
                        {
                            return summary;
                        }
                        var tempBooking = db.Booking_TEMP.FirstOrDefault(x => x.BookingId == booking.BookingId);

                        if (tempBooking != null && !string.IsNullOrEmpty(tempBooking.Autocab_AuthorizationReference))
                        {
                            var autocabCancelObj = AutoCapVehicletypeRequest.AgentBookingCancellationRequestAgent(
                                                    tempBooking.Autocab_AuthorizationReference);
                            if (!autocabCancelObj.Status)
                            {
                                //  summary.ToFullAddress = "Can not delete booking in ghost";
                            }
                        }


                        booking.PaymentStatus = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Free_Journey
                          ? (int)PaymentStatusEnum.Paid
                          : (int)PaymentStatusEnum.Pending;
                        booking.CardTypeId = null;
                        booking.CardPaymentTypeId = null;
                        booking.OtherCharges = 0;
                        booking.TotalPrice = currentBooking.ReturnWayPrice;
                        booking.FinalPrice = currentBooking.ReturnWayPrice - currentBooking.ReturnWayCredit;
                        booking.ConsumerDebit = currentBooking.ReturnWayCredit;
                        //  booking.PaymentMethod = currentBooking.PaymentMethodId;
                        booking.PaymentMethod = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Pay_Booking_Fee
                                               ? (int)PaymentTypeEnum.Cash
                                               : currentBooking.PaymentMethodId;
                        booking.Autocab_AuthorizationReference = "";
                        booking.Autocab_BookingReference = "";
                        booking.AutocabNumber = "";
                        booking.Autocab_IsBookingCancelled = true;
                        db.SaveChanges();


                        if (tempBooking != null)
                        {
                            tempBooking.PaymentStatus = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Free_Journey
                        ? (int)PaymentStatusEnum.Paid
                        : (int)PaymentStatusEnum.Pending;
                            tempBooking.CardTypeId = null;
                            tempBooking.CardPaymentTypeId = null;
                            tempBooking.OtherCharges = 0;
                            tempBooking.TotalPrice = currentBooking.ReturnWayPrice;
                            tempBooking.FinalPrice = currentBooking.ReturnWayPrice - currentBooking.ReturnWayCredit;
                            tempBooking.ConsumerDebit = currentBooking.ReturnWayCredit;
                            //  tempBooking.PaymentMethod = currentBooking.PaymentMethodId;
                            tempBooking.PaymentMethod = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Pay_Booking_Fee
                                               ? (int)PaymentTypeEnum.Cash
                                               : currentBooking.PaymentMethodId;
                            tempBooking.Autocab_AuthorizationReference = "";
                            tempBooking.Autocab_BookingReference = "";
                            tempBooking.AutocabNumber = "";
                            tempBooking.Autocab_IsBookingCancelled = true;
                            db.SaveChanges();
                        }

                        var objBookingPayment_TEMP = new BookingPayment_TEMP();
                        objBookingPayment_TEMP.BookingId = booking.BookingId;
                        // objBookingPayment_TEMP.PaymentTypeId = currentBooking.PaymentMethodId;
                        objBookingPayment_TEMP.PaymentTypeId = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Pay_Booking_Fee
                                              ? (int)PaymentTypeEnum.Cash
                                              : currentBooking.PaymentMethodId;
                        objBookingPayment_TEMP.PaymentDate = DateTime.Now;
                        objBookingPayment_TEMP.PaymentAmount = currentBooking.ReturnWayPrice - currentBooking.ReturnWayCredit;
                        objBookingPayment_TEMP.Status = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Free_Journey
                          ? (int)PaymentStatusEnum.Paid
                          : (int)PaymentStatusEnum.Pending;
                        objBookingPayment_TEMP.IsDeleted = false;
                        objBookingPayment_TEMP.IsEnabled = true;
                        objBookingPayment_TEMP.sysModifiedDate = DateTime.UtcNow;
                        objBookingPayment_TEMP.sysCreatedDate = DateTime.UtcNow;
                        db.BookingPayment_TEMP.Add(objBookingPayment_TEMP);
                        db.SaveChanges();

                        var objBookingPayment = new BookingPayment();
                        objBookingPayment.BookingPaymentId = objBookingPayment_TEMP.BookingPaymentId;
                        objBookingPayment.BookingId = booking.BookingId;
                        // objBookingPayment.PaymentTypeId = currentBooking.PaymentMethodId;
                        objBookingPayment.PaymentTypeId = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Pay_Booking_Fee
                                              ? (int)PaymentTypeEnum.Cash
                                              : currentBooking.PaymentMethodId;
                        objBookingPayment.PaymentDate = DateTime.Now;
                        objBookingPayment.PaymentAmount = currentBooking.ReturnWayPrice - currentBooking.ReturnWayCredit;
                        objBookingPayment.Status = currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Free_Journey
                       ? (int)PaymentStatusEnum.Paid
                       : (int)PaymentStatusEnum.Pending;
                        objBookingPayment.IsDeleted = false;
                        objBookingPayment.IsEnabled = true;
                        objBookingPayment.RefundAmount = 0;
                        objBookingPayment.sysModifiedDate = DateTime.UtcNow;
                        objBookingPayment.sysCreatedDate = DateTime.UtcNow;
                        db.BookingPayments.Add(objBookingPayment);
                        db.SaveChanges();

                        if (currentBooking.PaymentMethodId == (int)PaymentTypeEnum.Use_My_Credits)
                        {
                            SubtractCustomerCredits(booking);
                        }




                        summary.ReturnFromFullAddress = booking.BookingFrom;
                        summary.ReturnViapoints = booking.JourneyViapoints != null
                                            ? string.Join(", ", booking.JourneyViapoints.Select(x => x.ViaFullAddress)) : "";
                        summary.ReturnToFullAddress = booking.BookingTo;
                        summary.PaymentType = Enum.GetName(typeof(PaymentTypeEnum), booking.PaymentMethod);
                        summary.BookingStatusReturnJourney = booking.Status;
                        summary.IsReturnJourney = true;

                        //  summary.TotalDistancePrice = relevantBookingReturn.TotalDistancePrice + relevantBooking.TotalDistancePrice + (relevantBooking.ConsumerDebit + relevantBookingReturn.ConsumerDebit);
                        summary.ReturnWayCredit = booking.ConsumerDebit;
                        summary.ReturnWayPrice = booking.TotalDistancePrice;
                        //summary.OtherCharges = booking.OtherCharges;
                        summary.ReturnWayOtherDiscounts = booking.OtherDiscount;
                        //   summary.ReturnBookingDateTime = booking.TotalDistancePrice;
                        summary.ReturnBookingDateTime = ValueParseHelper.ToStringDateTime(booking.BookingDateTime);

                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return summary;
        }
        #endregion

        #region  Send Change Payment Email

        public void SendChangePayment(CurrentBookingDisplayViewModel model)
        {
            // var main = new MainCancelBooking();
            //  var token = System.Web.HttpContext.Current.Server.UrlDecode(model.BookingToken);

            try
            {
                var booking = new Booking();
                var bookingReturn = new Booking();
                using (var db = new BATransferEntities())
                {
                    booking = db.Bookings.FirstOrDefault(bt => bt.BookingToken.Equals(model.BookingToken));

                    if (booking.IsSourceBooking)
                    {
                        bookingReturn = db.Bookings.First(bt => bt.BookingId == booking.ReferenceBookingId && bt.IsReturnBooking);

                    }

                    if (booking != null)
                    {
                        SendChangePaymentEmail(booking, bookingReturn);

                        SendPushNotification(booking, true, model.PaymentMethodId);
                    }

                }
            }
            catch (Exception ex)
            {
                //main.Message = ex.Message;
                //main.Status = false;
                //main.StatusCode = (int)HttpStatusCode.BadRequest;
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

        }
        private void SendChangePaymentEmail(Booking booking, Booking bookingReturn)
        {
            try
            {

                EmailCredentialsViewModel emailCredentials = new EmailCredentialsViewModel();

                int bookingSiteId = OrganizationHelper.GetRelevantOrganizationFromSite(booking.BookingSource);
                string bookingToken = HttpContext.Current.Server.UrlEncode(booking.BookingToken);
                string bookingDetailApi = string.Empty;
                string logoUrl = string.Empty;
                string mainUrl = string.Empty;
                switch (bookingSiteId)
                {
                    case (int)BookingSiteEnum.BA:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBA);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.BA);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.AO:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.AO);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.AO);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailAO);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.AO);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.ET:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.ET);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.ET);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailET);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.ET);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.WT:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.WT);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.WT);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailWT);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.WT);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.BAC:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAC);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.BA);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.BAL:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAL);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.BA);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    case (int)BookingSiteEnum.BAT:
                        {
                            logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                            mainUrl = ConfigHelper.GetWebsiteUrl((int)BookingSiteEnum.BA);
                            emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBAT);
                            bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.BA);
                            bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                        }
                        break;
                    default:
                        break;
                }


                string loyaltyProgramUrl = mainUrl + "/MainPage/LoyaltyProgram";
                string contactUsUrl = mainUrl + "/MainPage/Contact";

                StringBuilder builder = new StringBuilder();
                string logFilePathHeader = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/Header.txt"));

                string textHeader = System.IO.File.ReadAllText(logFilePathHeader);
                textHeader = textHeader.Replace("LOGO-URL", logoUrl);
                textHeader = textHeader.Replace("BOOKING-DETAILS-URL", bookingDetailApi);
                textHeader = textHeader.Replace("BOOKING-URL", mainUrl);
                textHeader = textHeader.Replace("LOYALTY-PROGRAM-URL", loyaltyProgramUrl);
                textHeader = textHeader.Replace("CONTACT-US-URL", contactUsUrl);
                textHeader = textHeader.Replace("BOOKINGID", booking.BookingNumber);
                builder.Append(textHeader);
                if (!string.IsNullOrEmpty(booking.PassengerName))
                {
                    string logFilePathLeadPassanger =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/ChangePayment/LeadPassangerDetails.txt"));

                    //LOGO-URL
                    string textPassanger = System.IO.File.ReadAllText(logFilePathLeadPassanger);

                    textPassanger = textPassanger.Replace("PASSENGERNAME", booking.PassengerName);
                    textPassanger = textPassanger.Replace("PASSENGEREMAIL", booking.PassengerEmail);
                    textPassanger = textPassanger.Replace("PASSENGERMOBILE", booking.PassengerTel);
                    builder.Append(textPassanger);
                }

                string logFilePathMember = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/MemberDetails.txt"));

                //LOGO-URL
                string textMember = System.IO.File.ReadAllText(logFilePathMember);
                textMember = textMember.Replace("BOOKINGPERSONNAME", booking.BookedPersonName);
                textMember = textMember.Replace("BOOKINGPERSONMOBILE", booking.BookedPersonTel);
                textMember = textMember.Replace("BOOKINGPERSONALTERNATIVE", booking.AlternativePhoneNumber);
                textMember = textMember.Replace("BOOKINGPERSONEMAIL", booking.BookedPersonEmail);

                builder.Append(textMember);

                string logFilePath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/EmailMainBody.txt"));
                string text = System.IO.File.ReadAllText(logFilePath);
                string paymentType = booking.PaymentMethod == (int)PaymentTypeEnum.Cash
                    ? "Cash"
                    : booking.PaymentMethod == (int)PaymentTypeEnum.Card
                        ? "Card"
                        : booking.PaymentMethod == (int)PaymentTypeEnum.Bank_Transfer
                            ? "Bank Transfer"
                            : booking.PaymentMethod == (int)PaymentTypeEnum.Free_Journey
                                ? "Free Journey"
                                : booking.PaymentMethod == (int)PaymentTypeEnum.Promo_Code
                                    ? "Promo Code"
                                    : booking.PaymentMethod == (int)PaymentTypeEnum.Voucher
                                        ? "Voucher"
                                        : booking.PaymentMethod == (int)PaymentTypeEnum.Bank_Transfer
                                            ? "Bank Transfer"
                                            : booking.PaymentMethod == (int)PaymentTypeEnum.Use_My_Credits
                                                ? "Used My Credits"
                                                : "";

                text = text.Replace("BOOKINGID", booking.BookingNumber);
                text = text.Replace("RESERVATIONDATETIME", ValueParseHelper.ToStringDateTime(booking.ReservationDateTime));
                if (booking.JourneyType == (int)JourneyTypeEnum.OneWay)
                {
                    text = text.Replace("BOOKINGAMOUNT", booking.FinalPrice.ToString());
                }
                else
                {
                    text = text.Replace("BOOKINGAMOUNT", (booking.FinalPrice + bookingReturn.FinalPrice).ToString());
                }
                text = text.Replace("BOOKINGPAYMENTMETHOD", paymentType);
                text = text.Replace("PAYMENTOVERTHEPHONE", "");
                if (booking.CardPaymentTypeId == (int)CardPaymentTypeEnum.PayOverThePhone)
                {
                    text = text.Replace("PAYMENTOVERTHEPHONE",
                        "<span>Pay by Credit/Debit card over the phone to the Office (Call +44 208 900 2299)</span>");
                }
                else
                {
                    text = text.Replace("PAYMENTOVERTHEPHONE", "");

                }
                builder.Append(text);
                if (booking.ConsumerDebit > 0 || bookingReturn.ConsumerDebit > 0)
                {
                    string discountMoneyBackTxt =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/ChangePayment/DiscountMoneyBack.txt"));
                    string additionalChargesTxt = System.IO.File.ReadAllText(discountMoneyBackTxt);
                    additionalChargesTxt = additionalChargesTxt.Replace("DISCOUNT", (booking.ConsumerDebit + bookingReturn.ConsumerDebit).ToString());
                    builder.Append(additionalChargesTxt);
                }
                if (booking.PaymentMethod == (int)PaymentTypeEnum.Card)
                {
                    string logFilePathAdditionalCharges =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/ChangePayment/CardPayment.txt"));
                    string additionalChargesTxt = System.IO.File.ReadAllText(logFilePathAdditionalCharges);
                    additionalChargesTxt = additionalChargesTxt.Replace("ADDITIONALCHARGES", booking.OtherCharges.ToString());
                    additionalChargesTxt = additionalChargesTxt.Replace("PAYMENTSTATUS", Enum.GetName(typeof(PaymentStatusEnum), booking.PaymentStatus));
                    builder.Append(additionalChargesTxt);
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    string logFilePathOneWayBooking = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/OneWayBooking.txt"));
                    string textOneWay = System.IO.File.ReadAllText(logFilePathOneWayBooking);
                    textOneWay = textOneWay.Replace("BOOKINGDATETIME", booking.BookingDateTime.ToString());
                    textOneWay = textOneWay.Replace("BOOKINGAMOUNTONEWAY", booking.TotalDistancePrice.ToString());
                    if (booking.StudentDiscount > 1 && booking.BookingSource == (int)BookingSource_Enum.AO_Web)
                    {
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTDIV",
                            "<p>Student Discount  <span style='float:right;'>:</span></p>");
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + booking.StudentDiscount + "</p>");
                    }
                    else
                    {
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTDIV", "");
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                    }
                    builder.Append(textOneWay);
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    string logFilePathAirportDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/AirportDetails.txt"));
                    string textAirport = System.IO.File.ReadAllText(logFilePathAirportDetails);
                    textAirport = textAirport.Replace("FLIGHTNUMBER", booking.FlightNo);
                    textAirport = textAirport.Replace("FLIGHTLANDINGDATETIME", booking.FlightLandingDateTime.ToString());
                    textAirport = textAirport.Replace("FLIGHTDISPLAYNAME", booking.DriverDisplayName);
                    textAirport = textAirport.Replace("FLIGHTLANDINGDRIVERELLAPSETIME", booking.DriverEllapseTime.ToString());
                    textAirport = textAirport.Replace("BOOKINGAMOUNTONEWAY", booking.TotalDistancePrice.ToString());
                    textAirport = textAirport.Replace("PICKUPCHARGE", booking.PickupCharge.ToString());


                    if (booking.StudentDiscount > 1 && booking.BookingSource == (int)BookingSource_Enum.AO_Web)
                    {
                        textAirport = textAirport.Replace("STUDENTDISCOUNTDIV",
                            "<p>Student Discount  <span style='float:right;'>:</span></p>");
                        textAirport = textAirport.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + booking.StudentDiscount + "</p>");
                    }
                    else
                    {
                        textAirport = textAirport.Replace("STUDENTDISCOUNTDIV", "");
                        textAirport = textAirport.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                    }
                    builder.Append(textAirport);
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    string logFilePathCruisePortDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/CruisePortDetails.txt"));
                    string textCruiseport = System.IO.File.ReadAllText(logFilePathCruisePortDetails);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPNUMBER", booking.FlightNo);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPLANDINGDATETIME", booking.FlightLandingDateTime.ToString());
                    textCruiseport = textCruiseport.Replace("CRUISESHIPDISPLAYNAME", booking.DriverDisplayName);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPLANDINGDRIVERELLAPSETIME", booking.DriverEllapseTime.ToString());
                    textCruiseport = textCruiseport.Replace("BOOKINGAMOUNTONEWAY", booking.TotalDistancePrice.ToString());
                    if (booking.StudentDiscount > 1 && booking.BookingSource == (int)BookingSource_Enum.AO_Web)
                    {
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTDIV",
                            "<p>Student Discount  <span style='float:right;'>:</span></p>");
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + booking.StudentDiscount + "</p>");
                    }
                    else
                    {
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTDIV", "");
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                    }
                    builder.Append(textCruiseport);
                }
                if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay)
                {
                    if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                    {
                        string logFilePathAirportReturnDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/AirportReturn.txt"));
                        string textAirportReturn = System.IO.File.ReadAllText(logFilePathAirportReturnDetails);
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_NUMBER_RETURN", bookingReturn.FlightNo);
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_LANDINGDATE_TIME_RETURN", bookingReturn.FlightLandingDateTime.ToString());
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_DISPLAY_NAME_RETURN", bookingReturn.DriverDisplayName);
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_LANDING_DRIVER_ELLAPSE_TIME_RETURN", bookingReturn.DriverEllapseTime.ToString());
                        textAirportReturn = textAirportReturn.Replace("RETURNDATETIME", bookingReturn.FlightLandingDateTime.ToString());
                        textAirportReturn = textAirportReturn.Replace("BOOKINGAMOUNTTWOWAY", bookingReturn.TotalDistancePrice.ToString());
                        textAirportReturn = textAirportReturn.Replace("PICKUPCHARGE", bookingReturn.PickupCharge.ToString());
                        if (bookingReturn.StudentDiscount > 1 && bookingReturn.BookingSource == (int)BookingSource_Enum.AO_Web)
                        {
                            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTDIV",
                                "<p>Student Discount  <span style='float:right;'>:</span></p>");
                            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + bookingReturn.StudentDiscount + "</p>");
                        }
                        else
                        {
                            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTDIV", "");
                            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                        }
                        builder.Append(textAirportReturn);
                    }
                    else if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                    {
                        string logFilePathCruiseportReturn = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/CruiseportReturn.txt"));
                        string textCruiseportReturn = System.IO.File.ReadAllText(logFilePathCruiseportReturn);
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_NUMBER_RETURN", bookingReturn.FlightNo);
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_LANDING_DATE_TIME_RETURN", bookingReturn.FlightLandingDateTime.ToString());
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_DISPLAY_NAME_RETURN", bookingReturn.DriverDisplayName);
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_LANDING_DRIVER_ELLAPSE_TIME_RETURN", bookingReturn.DriverEllapseTime.ToString());
                        textCruiseportReturn = textCruiseportReturn.Replace("RETURNDATETIME", bookingReturn.FlightLandingDateTime.ToString());
                        textCruiseportReturn = textCruiseportReturn.Replace("BOOKINGAMOUNTTWOWAY", bookingReturn.TotalDistancePrice.ToString());
                        if (bookingReturn.StudentDiscount > 1 && bookingReturn.BookingSource == (int)BookingSource_Enum.AO_Web)
                        {
                            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTDIV",
                                "<p>Student Discount  <span style='float:right;'>:</span></p>");
                            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + bookingReturn.StudentDiscount + "</p>");
                        }
                        else
                        {
                            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTDIV", "");
                            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                        }
                        builder.Append(textCruiseportReturn);
                    }
                    if (bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                    {
                        string logFilePathTwoWayBooking = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/ReturnBooking.txt"));
                        string textTwoWay = System.IO.File.ReadAllText(logFilePathTwoWayBooking);
                        textTwoWay = textTwoWay.Replace("BOOKINGDATETIMERETURN", bookingReturn.BookingDateTime.ToString());
                        textTwoWay = textTwoWay.Replace("BOOKINGAMOUNTTWOWAY", bookingReturn.TotalDistancePrice.ToString());
                        if (bookingReturn.StudentDiscount > 1 && bookingReturn.BookingSource == (int)BookingSource_Enum.AO_Web)
                        {
                            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTDIV",
                                "<p>Student Discount  <span style='float:right;'>:</span></p>");
                            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + bookingReturn.StudentDiscount + "</p>");
                        }
                        else
                        {
                            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTDIV", "");
                            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                        }
                        builder.Append(textTwoWay);
                    }
                }
                string logFilePathAddressDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/BookingAddress.txt"));
                string addresstext = System.IO.File.ReadAllText(logFilePathAddressDetails);
                //addresstext = addresstext.Replace("CUSTOMERCOMMENTS", booking.CustomerComment);
                //addresstext = addresstext.Replace("MAPURL", booking.MapUrl);
                addresstext = addresstext.Replace("BOOKING-DATE-TIME", booking.BookingDateTime.ToString());
                addresstext = addresstext.Replace("FROM-ADDRESS", booking.BookingFrom);
                addresstext = addresstext.Replace("FROMTOWN", booking.FromTown);
                addresstext = addresstext.Replace("FROMPOSTCODE", booking.BookingFromFullPostCode);
                addresstext = addresstext.Replace("TO-ADDRESS", booking.BookingTo);
                addresstext = addresstext.Replace("TOTOWN", booking.ToTown);
                addresstext = addresstext.Replace("TOPOSTCODE", booking.BookingToFullPostCode);

                #region Via Points

                StringBuilder builderViapoints = new StringBuilder();
                int viaid = 0;
                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var journeyViapoints = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty());
                    foreach (var viapoint in journeyViapoints.ToList())
                    {
                        viaid++;
                        string via = "<div style='width:97%; float: left; font-size:15px'>" +
                                     "<div   style='float:left; width:20%; color:#666666; font-weight:600; line-height: 10px;'>" +
                                     "<p style='margin-top:10px;'>Viapoint " + viaid +
                                     "<span style='float:right;'>:</span></p>" +
                                     "</div>" +
                                     "<div class='form-right' style='color:#666666; margin-left:10px; width: 75%; float: left;'>" +
                                     "<p style='margin-top:4px;line-height: 17px;'>" + viapoint.ViaFullAddress + "</p> " +
                                     "</div>" +
                                     "</div>";
                        builderViapoints.Append(via);
                    }
                    addresstext = addresstext.Replace("VIA-POINTS", builderViapoints.ToString());
                }
                else
                {
                    addresstext = addresstext.Replace("VIA-POINTS", "");
                }

                #endregion

                builder.Append(addresstext);

                if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay)
                {
                    string logFilePathReturnBookingAddress = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/BookingAddressReturn.txt"));
                    string returnBookingTxt = System.IO.File.ReadAllText(logFilePathReturnBookingAddress);
                    if (bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport &&
                        bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                    {
                        returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME", bookingReturn.BookingDateTime.ToString());
                    }

                    if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                    {
                        returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME", bookingReturn.FlightLandingDateTime.ToString());
                    }
                    if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                    {
                        returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME", bookingReturn.FlightLandingDateTime.ToString());
                    }
                    returnBookingTxt = returnBookingTxt.Replace("FROM-ADDRESS", bookingReturn.BookingFrom);
                    returnBookingTxt = returnBookingTxt.Replace("FROMTOWN", bookingReturn.FromTown);
                    returnBookingTxt = returnBookingTxt.Replace("FROMPOSTCODE", bookingReturn.BookingFromFullPostCode);
                    returnBookingTxt = returnBookingTxt.Replace("TO-ADDRESS", bookingReturn.BookingTo);
                    returnBookingTxt = returnBookingTxt.Replace("TOTOWN", bookingReturn.ToTown);
                    returnBookingTxt = returnBookingTxt.Replace("TOPOSTCODE", bookingReturn.BookingToFullPostCode);

                    #region Return Via Points

                    StringBuilder builderViapointsReturn = new StringBuilder();
                    int viaidReturn = 0;

                    if (bookingReturn.JourneyViapoints != null && bookingReturn.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                    {
                        var journeyViapointsReturn = bookingReturn.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty());
                        foreach (var viapoint in journeyViapointsReturn.ToList())
                        {
                            viaidReturn++;
                            string via = "<div style='width:97%; float: left; font-size:15px'>" +
                                         "<div   style='float:left; width:20%; color:#666666; font-weight:600; line-height: 10px;'>" +
                                         "<p style='margin-top:10px;'>Viapoint " + viaidReturn +
                                         "<span style='float:right;'>:</span></p>" +
                                         "</div>" +
                                         "<div class='form-right' style='color:#666666; margin-left:10px; width: 75%; float: left;'>" +
                                         "<p style='margin-top:4px;line-height: 17px;'>" + viapoint.ViaFullAddress + "</p> " +
                                         "</div>" +
                                         "</div>";
                            builderViapointsReturn.Append(via);
                        }
                        returnBookingTxt = returnBookingTxt.Replace("VIA-POINTS", builderViapointsReturn.ToString());
                    }
                    else
                    {
                        returnBookingTxt = returnBookingTxt.Replace("VIA-POINTS", "");
                    }

                    #endregion

                    builder.Append(returnBookingTxt);
                }
                else
                {
                    string fileClosingDiv = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/ClosingDiv.txt"));
                    string fileClosingDivTxt = System.IO.File.ReadAllText(fileClosingDiv);
                    builder.Append(fileClosingDivTxt);
                }
                string fileMap = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/Map.txt"));
                string textMap = System.IO.File.ReadAllText(fileMap);
                textMap = textMap.Replace("MAP-URL", booking.MapUrl);
                builder.Append(textMap);
                string fileLuggageInfo = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/LuggageInfo.txt"));
                string textLuggage = System.IO.File.ReadAllText(fileLuggageInfo);
                string vehicleType = string.Join("&", booking.BookingVehicleTypes.Select(x => x.VehicleType.VehicleTypeName).ToArray());

                textLuggage = textLuggage.Replace("BOOKINGVEHICLETYPES", vehicleType);
                textLuggage = textLuggage.Replace("NUMBEROFPESSANGERS", booking.NumberOfPassengers.ToString());
                textLuggage = textLuggage.Replace("NUMBEROFBABYSEATS", (booking.BoosterSeat + booking.InfantSeats + booking.ChildSeats).ToString());
                textLuggage = textLuggage.Replace("NUMBEROFLUGGAHES", booking.Luggage.ToString());
                textLuggage = textLuggage.Replace("NUMBEROFHANDLUGGAHES", booking.HandLuggage.ToString());

                if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay)
                {
                    textLuggage = textLuggage.Replace("BOOKINGAMOUNT", (booking.FinalPrice + bookingReturn.FinalPrice).ToString());
                }
                else
                {
                    textLuggage = textLuggage.Replace("BOOKINGAMOUNT", booking.FinalPrice.ToString());
                }
                //textLuggage = textLuggage.Replace("BOOKINGAMOUNT", booking.FinalPrice.ToString());

                builder.Append(textLuggage);
                string logFilePathFooderDetals = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ChangePayment/FooderDetals.txt"));
                string foodertext = System.IO.File.ReadAllText(logFilePathFooderDetals);
                foodertext = foodertext.Replace("BOOKING-URL", mainUrl);
                foodertext = foodertext.Replace("LOYALTY-PROGRAM-URL", loyaltyProgramUrl);
                foodertext = foodertext.Replace("CONTACT-US-URL", contactUsUrl);
                builder.Append(foodertext);
                var listEmails = new List<string>();
                if (booking.IsSendPassengerEmail)
                {
                    listEmails.Add(booking.PassengerEmail);
                    listEmails.Add(booking.BookedPersonEmail);
                }
                else
                {
                    listEmails.Add(booking.BookedPersonEmail);
                }

                //Sujanika, Your Reservation Confirmed at batransfer.com - Ref: BA544226
                string _emailSubject = booking.BookedPersonName + "," + "Your Payment type Change as " + paymentType +
                                       " - Ref: " + booking.BookingNumber;


                var email = new RootEmail();
                email.Attachments = null;
                email.Body = builder.ToString();
                email.FromEmailAddress = emailCredentials.FromEmail;
                email.Username = emailCredentials.Username;
                email.Password = emailCredentials.Password;
                email.Port = emailCredentials.Port;
                email.Smtp = emailCredentials.Smtp;
                email.Subject = _emailSubject;
                email.ToEmailAddresses = listEmails;
                email.DisplayName = emailCredentials.DisplayName;
                email.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                EmailHelper.SendEmailWithoutAttachtment(email, apiUrl);
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }
        #endregion


        #region Vehicle Upgrate

        public List<CurrentBookingsViewModel> GetAllBookings(string bookingNumber)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<CurrentBookingsViewModel> objList =
                        db.sp_vehicleupgrate_currentvehicle_swaran(bookingNumber).ToList()
                            .Select(x => new CurrentBookingsViewModel()
                            {
                                BookingDateTime = x.BookingDateTime.ToString(),
                                BookingFrom = x.BookingFrom,
                                BookingTo = x.BookingTo,
                                BookingId = x.BookingId,
                                VehicleTypeId = x.VehicleTypeId,
                                VehicleTypeName = x.VehicleTypeName
                            });
                    return objList.ToList();
                }

            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                      System.Reflection.MethodBase.GetCurrentMethod().Name,
                      ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public List<UpgrateVehicleListViewModel> LoadVehicleUpdate(long currentVehicleId)
        {
            try
            {
                var list = new List<UpgrateVehicleListViewModel>();
                using (var db = new BATransferEntities())
                {
                    var vehicleList = db.BookingVehicleUpgrades.Where(x => x.RequestedVehicleTypeId == currentVehicleId && x.IsEnabled);
                    foreach (var vehicle in vehicleList.ToList())
                    {
                        var selectedItem = new UpgrateVehicleListViewModel();
                        selectedItem.Text = vehicle.VehicleType1.VehicleTypeName + " ( " + vehicle.PointsRequired + " Points Required )";
                        selectedItem.VehicleTypeId = vehicle.UpgradeVehicleTypeId;
                        selectedItem.Points = vehicle.PointsRequired;
                        list.Add(selectedItem);
                    }

                    return list;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }


        public int GetCurrentPoints(string userId)
        {
            try
            {

                using (var db = new BATransferEntities())
                {
                    var customerObj = db.Customers.FirstOrDefault(x => x.userId.Equals(userId));
                    if (customerObj != null)
                    {
                        return customerObj.availablePoints;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return 0;
        }


        public string SaveVehicleUpdate(SaveUpdateVehicle updateVehicle)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    Customer customer = null;
                    int vehicleUpgrateAmt = 0;
                    var bookingTemp = db.Booking_TEMP.FirstOrDefault(x => x.BookingId == updateVehicle.BookingId);

                    if (bookingTemp != null)
                    {
                        customer = db.Customers.FirstOrDefault(x => x.userId.Equals(bookingTemp.UserId));

                        if (customer != null && customer.availablePoints < updateVehicle.Points)
                        {
                            return "You do not have enough points to do an update";
                        }

                        vehicleUpgrateAmt = GetVehicleUpgrateAmt(updateVehicle.CurrentVehicletypeId,
                            updateVehicle.VehicleUpgradeId);
                        bookingTemp.Status = (int)BookingStatusEnum.Requested;
                        bookingTemp.UsedPoints = updateVehicle.Points;
                        bookingTemp.VehicleUpgradeAmt = vehicleUpgrateAmt;
                        bookingTemp.IsUpgradeByPoints = true;
                        bookingTemp.IsVehicleUpgradeEnabeld = true;
                        db.SaveChanges();

                        var bookingVehicleTypeTemp =
                            db.BookingVehicleType_TEMP.FirstOrDefault(
                                x =>
                                    x.BookingId == updateVehicle.BookingId &&
                                    x.VehicleTypeId == updateVehicle.CurrentVehicletypeId);
                        if (bookingVehicleTypeTemp != null)
                        {
                            bookingVehicleTypeTemp.VehicleTypeId = updateVehicle.VehicleUpgradeId;
                            db.SaveChanges();
                        }

                    }


                    var booking = db.Bookings.FirstOrDefault(x => x.BookingId == updateVehicle.BookingId);

                    if (booking != null)
                    {
                        booking.Status = (int)BookingStatusEnum.Requested;
                        booking.UsedPoints = updateVehicle.Points;
                        booking.VehicleUpgradeAmt = vehicleUpgrateAmt;
                        booking.IsUpgradeByPoints = true;
                        booking.IsVehicleUpgradeEnabeld = true;
                        db.SaveChanges();

                        var bookingVehicleType =
                            db.BookingVehicleTypes.FirstOrDefault(
                                x =>
                                    x.BookingId == updateVehicle.BookingId &&
                                    x.VehicleTypeId == updateVehicle.CurrentVehicletypeId);
                        if (bookingVehicleType != null)
                        {
                            bookingVehicleType.VehicleTypeId = updateVehicle.VehicleUpgradeId;
                            db.SaveChanges();
                        }

                        if (customer != null)
                        {
                            customer.availablePoints = customer.availablePoints - updateVehicle.Points;
                            db.SaveChanges();
                            var bookingPoint = new BookingPoint();
                            bookingPoint.BookingId = booking.BookingId;
                            bookingPoint.PointsTypeId = (int)FreeJourneyPointsTypeEnum.VehicleUpgrade;
                            bookingPoint.Points = -updateVehicle.Points;
                            bookingPoint.TotalPoints = customer.availablePoints;
                            bookingPoint.IsDeleted = false;
                            bookingPoint.IsEnabled = false;
                            bookingPoint.sysCreatedDate = DateTime.UtcNow;
                            bookingPoint.sysModifiedDate = DateTime.UtcNow;
                            db.BookingPoints.Add(bookingPoint);
                            db.SaveChanges();
                        }


                        var upgradeHistory = new BookingVehicleUpgradeHistory();
                        upgradeHistory.BookingId = updateVehicle.BookingId;
                        upgradeHistory.PointsUsed = updateVehicle.Points;
                        upgradeHistory.RequestedVehicleTypeId = updateVehicle.CurrentVehicletypeId;
                        upgradeHistory.UpgradedVehicleTypeId = updateVehicle.VehicleUpgradeId;
                        upgradeHistory.IsDeleted = false;
                        upgradeHistory.IsEnabled = true;
                        upgradeHistory.sysCreatedDate = DateTime.UtcNow;
                        upgradeHistory.sysModifiedDate = DateTime.UtcNow;
                        db.BookingVehicleUpgradeHistories.Add(upgradeHistory);
                        db.SaveChanges();
                    }

                    return "Successfully updated";
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return "Error ";
        }

        public string VehicleUpgratePayOnline(SaveUpdateVehicle updateVehicle)
        {
            try
            {

                using (var db = new BATransferEntities())
                {

                    var bookingTemp = db.Booking_TEMP.FirstOrDefault(x => x.BookingId == updateVehicle.BookingId);

                    if (bookingTemp != null)
                    {
                        bookingTemp.Status = (int)BookingStatusEnum.Requested;
                        bookingTemp.VehicleUpgradeAmt = updateVehicle.Points;
                        bookingTemp.IsUpgradeByPoints = false;
                        bookingTemp.IsVehicleUpgradeEnabeld = true;
                        db.SaveChanges();

                        var bookingVehicleTypeTemp = db.BookingVehicleType_TEMP.FirstOrDefault(x => x.BookingId == updateVehicle.BookingId && x.VehicleTypeId == updateVehicle.CurrentVehicletypeId);
                        if (bookingVehicleTypeTemp != null)
                        {
                            bookingVehicleTypeTemp.VehicleTypeId = updateVehicle.VehicleUpgradeId;
                            db.SaveChanges();
                        }

                    }
                    var booking = db.Bookings.FirstOrDefault(x => x.BookingId == updateVehicle.BookingId);

                    if (booking != null)
                    {
                        booking.Status = (int)BookingStatusEnum.Requested;
                        booking.VehicleUpgradeAmt = updateVehicle.Points;
                        booking.IsUpgradeByPoints = false;
                        booking.IsVehicleUpgradeEnabeld = true;
                        db.SaveChanges();

                        var bookingVehicleType = db.BookingVehicleTypes.FirstOrDefault(x => x.BookingId == updateVehicle.BookingId && x.VehicleTypeId == updateVehicle.CurrentVehicletypeId);
                        if (bookingVehicleType != null)
                        {
                            bookingVehicleType.VehicleTypeId = updateVehicle.VehicleUpgradeId;
                            db.SaveChanges();
                        }

                    }

                    var upgradeHistory = new BookingVehicleUpgradeHistory();
                    upgradeHistory.BookingId = updateVehicle.BookingId;
                    upgradeHistory.PointsUsed = updateVehicle.Points;
                    upgradeHistory.RequestedVehicleTypeId = updateVehicle.CurrentVehicletypeId;
                    upgradeHistory.UpgradedVehicleTypeId = updateVehicle.VehicleUpgradeId;
                    upgradeHistory.IsDeleted = false;
                    upgradeHistory.IsEnabled = true;
                    upgradeHistory.sysCreatedDate = DateTime.UtcNow;
                    upgradeHistory.sysModifiedDate = DateTime.UtcNow;
                    db.BookingVehicleUpgradeHistories.Add(upgradeHistory);
                    db.SaveChanges();
                    string description = "VehicleUpgrade/" + Enum.GetName(typeof(BookingSource_Enum), booking.BookingSource).ToString() + "/" + booking.BookingNumber;
                    string encodeDescription = System.Web.HttpContext.Current.Server.UrlEncode(description);
                    string encodeToken = System.Web.HttpContext.Current.Server.UrlEncode(booking.BookingToken);
                    string path = string.Empty;

                    path = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                    string callBackUrl = System.Web.HttpContext.Current.Server.UrlEncode(path + "/BookingSummary/VehicleUpgradeView?tkn=" + encodeToken + "&isRetrunBooking=" + (booking.IsReturnBooking ? 1 : 0)) + "&";

                    string bookingFeeUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.PaymentApi) +
                                        "Payment/CaptureBookingFee" + "?amnt=" + booking.VehicleUpgradeAmt + "&desc=" +
                                        encodeDescription + "&bnu=" +
                                        System.Web.HttpContext.Current.Server.UrlEncode(booking.BookingNumber) +
                                        "&cbu=" + callBackUrl;

                    return bookingFeeUrl;


                    //if (db.Booking_TEMP.Any(x => x.BookingToken.Equals(bookingToken)))
                    //{
                    //    var booking = db.Bookings.First(x => x.BookingToken.Equals(bookingToken));
                    //    #region Update Table 
                    //    decimal cardProcessingFee = OrganizationHelper.CardProcessingFeefromSite((int)BookingSiteEnum.BA);
                    //    finalAmount = finalAmount + cardProcessingFee;
                    //    var bookingTemp = db.Booking_TEMP.First(x => x.BookingToken.Equals(bookingToken));
                    //    if (booking.Status != (int)BookingStatusEnum.Cancelled)
                    //        finalAmount = booking.FinalPrice;

                    //    if (bookingTemp.IsSourceBooking)
                    //    {
                    //        var returnBookingTemp =
                    //            db.Bookings.First(
                    //                bt => bt.BookingId == bookingTemp.ReferenceBookingId && bt.IsReturnBooking);

                    //        var returnBooking =
                    //            db.Bookings.First(x => x.BookingId == returnBookingTemp.BookingId);
                    //        if (returnBooking.Status != (int)BookingStatusEnum.Cancelled)
                    //            finalAmount = finalAmount + returnBooking.FinalPrice;

                    //    }
                    //    #endregion
                    //    string description = "Booking/" + Enum.GetName(typeof(BookingSource_Enum), booking.BookingSource).ToString() + "/" + booking.BookingNumber;
                    //    string encodeDescription = System.Web.HttpContext.Current.Server.UrlEncode(description);
                    //    string encodeToken = System.Web.HttpContext.Current.Server.UrlEncode(booking.BookingToken);
                    //    string path = string.Empty;

                    //    path = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                    //    //    string callBackurl = HttpContext.Current.Server.UrlEncode(path + ":44359" + "/BookingSummary/Details?tkn=" + encodeToken);
                    //    string callBackurl = System.Web.HttpContext.Current.Server.UrlEncode(path + "/BookingSummary/Details?tkn=" + encodeToken);//"&amnt=" + booking.TotalPrice + "
                    //    string paymentUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.PaymentApi) + "Payment/CapturePayment" + "?tkn=" + encodeToken + "&amnt=" + finalAmount + "&desc=" + encodeDescription + "&bnu=" + System.Web.HttpContext.Current.Server.UrlEncode(booking.BookingNumber) + "&cbu=" + callBackurl;

                    //    return paymentUrl;
                    // }


                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return string.Empty;
        }
        public int GetVehicleUpgradePoints(long currentVehicleId, long upgradeVehicleTypeId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var vehicleUpgrades = db.BookingVehicleUpgrades.FirstOrDefault(x => x.RequestedVehicleTypeId == currentVehicleId && x.UpgradeVehicleTypeId == upgradeVehicleTypeId && x.IsEnabled);
                    if (vehicleUpgrades != null)
                    {
                        return vehicleUpgrades.PointsRequired;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return 0;
        }

        public int GetVehicleUpgrateAmt(long currentVehicletypeId, long upgratedVehicletypeId)
        {

            try
            {
                using (var db = new BATransferEntities())
                {
                    var objCurrent = db.VehicleTypes.FirstOrDefault(x => x.VehicleTypeID == currentVehicletypeId);

                    var objUpgrated = db.VehicleTypes.FirstOrDefault(x => x.VehicleTypeID == upgratedVehicletypeId);

                    if (objCurrent != null && objUpgrated != null)
                    {
                        return (int)objUpgrated.PriceAdd - (int)objCurrent.PriceAdd;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return 0;
        }
        #endregion


        #region Pending Bookings

        public List<PendingBookingViewModel> GetPendingBookings()
        {
            try
            {
                var britishTime = BritishTime.GetDateTime().AddHours(-24);
                using (var db = new BATransferEntities())
                {
                    IEnumerable<PendingBookingViewModel> objPendingBookings =
                        db.sp_pending_bookings(britishTime).ToList().Select(x => new PendingBookingViewModel()
                        {
                            BookedPersonName = x.BookedPersonName,
                            BookingDateTime = x.BookingDateTime,
                            BookingFrom = x.BookingFrom,
                            BookingId = x.BookingId,
                            BookingNumber = x.BookingNumber,
                            BookingTo = x.BookingTo,
                            BookingToken = x.BookingToken,
                            IsBookingFeePaymentFailed = x.IsBookingFeePaymentFailed ?? false,
                            IsCardPaymentFailed = x.IsCardPaymentFailed ?? false,
                            IsReturnBooking = x.IsReturnBooking,
                            Status = x.Status,
                            StatusId = x.StatusId,

                        });
                    return objPendingBookings.ToList();
                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                      System.Reflection.MethodBase.GetCurrentMethod().Name,
                      ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public int GetPendingBookingsCount()
        {
            try
            {
                // var britishTime = BritishTime.GetDateTime();
                var britishTime = BritishTime.GetDateTime().AddHours(-24);
                using (var db = new BATransferEntities())
                {
                    var objPendingBookings =
                        db.sp_pending_bookings_count(britishTime).FirstOrDefault();
                    if (objPendingBookings != null)
                    {
                        return objPendingBookings.Value;
                    }
                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                      System.Reflection.MethodBase.GetCurrentMethod().Name,
                      ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return 0;
        }
        public void UpdateCompletedBookings(long id)
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    var objBooking = db.Bookings.FirstOrDefault(x => x.BookingId == id);
                    if (objBooking != null)
                    {
                        objBooking.Status = (int)BookingStatusEnum.Finished;
                        objBooking.ModifiedDate = DateTime.UtcNow;
                        db.SaveChanges();

                        var freeJourney =
                              db.CustomerFreeJourneys.FirstOrDefault(x => x.BookingId == id);
                        if (freeJourney != null)
                        {
                            freeJourney.UsedDate = objBooking.BookingDateTime;
                            freeJourney.IsUsed = true;
                            db.SaveChanges();
                        }
                    }

                    var objBookingTemp = db.Booking_TEMP.FirstOrDefault(x => x.BookingId == id);
                    if (objBookingTemp != null)
                    {
                        db.Booking_TEMP.Remove(objBookingTemp);
                        db.SaveChanges();

                    }


                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                      System.Reflection.MethodBase.GetCurrentMethod().Name,
                      ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }

        public void UpdateCancelBookings(long id)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objBookingTemp = db.Booking_TEMP.FirstOrDefault(x => x.BookingId == id);
                    if (objBookingTemp != null)
                    {
                        objBookingTemp.Status = (int)BookingStatusEnum.Cancelled;
                        objBookingTemp.ModifiedDate = DateTime.UtcNow;
                        db.SaveChanges();

                    }
                    var objBooking = db.Bookings.FirstOrDefault(x => x.BookingId == id);
                    if (objBooking != null)
                    {
                        objBooking.Status = (int)BookingStatusEnum.Cancelled;
                        objBooking.ModifiedDate = DateTime.UtcNow;
                        db.SaveChanges();

                    }
                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                      System.Reflection.MethodBase.GetCurrentMethod().Name,
                      ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }

        #endregion
    }
}