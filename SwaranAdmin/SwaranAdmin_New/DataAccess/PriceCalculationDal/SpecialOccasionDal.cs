﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using SwaranAdmin_New.Models.PriceCalculation;
using SwaranAdmin_New.Models.Pricing;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SwaranAdmin_New.DataAccess.PriceCalculationDal
{
    public class SpecialOccasionDal
    {
        #region Singalton
        private static SpecialOccasionDal _instance;
        private static readonly object LockThis = new object();
        protected SpecialOccasionDal()
        {
        }
        public static SpecialOccasionDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new SpecialOccasionDal());
                }
            }
        }
        #endregion

        public List<SpecialOccasionViewModel> GetAllSpecialOccasion()
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    IEnumerable<SpecialOccasionViewModel> objaddress =
                        db.Rate_SpecialOccasion.ToList().Select(x => new SpecialOccasionViewModel()
                        {
                            OccasionsId = x.OccasionsId,
                            CustomerRateId = x.CustomerRateId,
                            RateFormula = x.RateFormula,
                            Title = x.Title,
                            FromDate = ValueParseHelper.ToStringDateTime(x.FromDate),
                            ToDate = ValueParseHelper.ToStringDateTime(x.ToDate),
                            Comments = x.Comments,
                            RateName = x.Rate_Customer.RateName,
                            Surcharge = x.Surcharge,
                             DropOffCharge = x.DropOffCharge,
                            DropOffComments = x.DropOffComments,
                            IsAllPlot= x.IsAllPlot ? 1 : x.IsBothAreapointApplied ? 3 :1
                        });

                    return objaddress.ToList();
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");


            }
            return null;
        }

        public SpecialOccasionViewModel GetCurrentSpecialOccasion(long id)
        {
            var occ = new SpecialOccasionViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.Rate_SpecialOccasion.Any(x => x.OccasionsId == id))
                    {
                        var model = db.Rate_SpecialOccasion.First(x => x.OccasionsId == id);
                        occ.OccasionsId = model.OccasionsId;
                        occ.CustomerRateId = model.CustomerRateId;
                        occ.RateName = model.Rate_Customer.RateName;
                        occ.Surcharge = model.Surcharge;
                        occ.RateFormula =!string.IsNullOrEmpty(model.RateFormula)
                            ?model.RateFormula.Replace(occ.RateName, " ")
                            :"";
                        occ.Title = model.Title;
                        occ.FromDate = model.FromDate.ToString("dd/MM/yyyy HH:mm");
                        occ.ToDate = model.ToDate.ToString("dd/MM/yyyy HH:mm");
                        occ.Comments = model.Comments;
                        occ.IsEnabled = model.IsEnabled;
                        occ.IsDeleted = model.IsDeleted;
                       occ.IsAllPlot = model.IsAllPlot ? 1 : model.IsBothAreapointApplied ? 3 : 2;//model.IsAllPlot;
                        occ.DropOffCharge = model.DropOffCharge;
                        occ.DropOffComments = model.DropOffComments;
                        return occ;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");


            }
            return null;
        }
        public void SaveSpecialOccasion(SpecialOccasionViewModel model)
        {

            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.Rate_SpecialOccasion.Any(x => x.OccasionsId == model.OccasionsId))
                    {
                        var occ = db.Rate_SpecialOccasion.First(x => x.OccasionsId == model.OccasionsId);
                        occ.CustomerRateId = model.CustomerRateId;
                        occ.Surcharge = model.Surcharge;
                        occ.RateFormula = model.RateName + model.RateFormula.Trim();
                        occ.Title = model.Title;
                        occ.FromDate = ValueParseHelper.TryParseDateTime(model.FromDate);
                        occ.ToDate = ValueParseHelper.TryParseDateTime(model.ToDate);
                        occ.Comments = model.Comments;
                        occ.IsEnabled = model.IsEnabled;
                        occ.IsDeleted = model.IsDeleted;
                        occ.IsAllPlot = (model.AreaPointId == null && model.FixedPriceId == null) ;//model.AriaPointId == null ?true: model.IsAllPlot;
                        occ.DropOffCharge = model.DropOffCharge;
                        occ.DropOffComments = model.DropOffComments??"";
                        occ.CreatedDate = DateTime.UtcNow;
                        occ.ModifiedDate = DateTime.UtcNow;
                        occ.IsBothAreapointApplied = model.IsBothAreapointApplied;
                        db.SaveChanges();
                        db.sp_swaran_delete_special_surcharge(model.OccasionsId);
                       
                    }
                    else
                    {
                        var occ = new Rate_SpecialOccasion();
                        occ.CustomerRateId = model.CustomerRateId;
                        occ.Surcharge = model.Surcharge;
                        occ.RateFormula = model.RateName + model.RateFormula.Trim(); 
                        occ.Title = model.Title;
                        occ.FromDate = ValueParseHelper.TryParseDateTime(model.FromDate);
                        occ.ToDate = ValueParseHelper.TryParseDateTime(model.ToDate);
                        occ.Comments = model.Comments;
                        occ.IsEnabled = model.IsEnabled;
                        occ.IsDeleted = model.IsDeleted;
                        occ.IsAllPlot = (model.AreaPointId == null && model.FixedPriceId == null);//model.AriaPointId == null ? true : model.IsAllPlot; 
                        occ.DropOffCharge = model.DropOffCharge;
                        occ.DropOffComments = model.DropOffComments ?? "";
                        occ.CreatedDate = DateTime.UtcNow;
                        occ.ModifiedDate = DateTime.UtcNow;
                        occ.IsBothAreapointApplied = model.IsBothAreapointApplied;
                        db.Rate_SpecialOccasion.Add(occ);
                        db.SaveChanges();
                        model.OccasionsId = occ.OccasionsId;
                    }

                    //if (!model.IsAllPlot && model.AriaPointId != null)
                    //if (model.IsAllPlot != 1 && model.AreaPointId != null)
                    if (model.IsAllPlot == 2 && model.AreaPointId != null)
                    {
                        foreach (var id in model.AreaPointId)
                        {
                            var surchagrge = new SpecialOccasionAreaPoint();
                            surchagrge.AreaPointId = id;//id.FromAreaPointId;//id;
                            surchagrge.SpecialOccasionId = model.OccasionsId;
                            surchagrge.isEnabled = true;
                            surchagrge.isDeleted = false;
                            surchagrge.sysModifiedDate = DateTime.UtcNow;
                            surchagrge.sysCreatedDate = DateTime.UtcNow;
                          surchagrge.ToAreaPointId = null;//id.ToAreaPointId;
                             surchagrge.FixedPriceId = null;//id.FixedPriceId;
                             //surchagrge.IsBothAreapointApplied = ((id.FixedPriceId != null && id.FixedPriceId > 0) || (id.ToAreaPointId != null && id.ToAreaPointId > 0));
                            db.SpecialOccasionAreaPoints.Add(surchagrge);
                            db.SaveChanges();
                        }
                    }

                    if (model.IsAllPlot == 3 && model.FixedPriceId != null)
                    {
                        foreach (var id in model.FixedPriceId)
                        {
                            var relevantFixedPrice = AddressHelper.GetFixedPriceDetail(id);

                            var surchagrge = new SpecialOccasionAreaPoint();
                            surchagrge.AreaPointId = relevantFixedPrice != null ? relevantFixedPrice.FromAreaPointId : null;//id.FromAreaPointId;//id;
                            surchagrge.SpecialOccasionId = model.OccasionsId;
                            surchagrge.isEnabled = true;
                            surchagrge.isDeleted = false;
                            surchagrge.sysModifiedDate = DateTime.UtcNow;
                            surchagrge.sysCreatedDate = DateTime.UtcNow;
                             surchagrge.ToAreaPointId = relevantFixedPrice != null ? relevantFixedPrice.ToAreaPointId : null;// id.ToAreaPointId;
                            surchagrge.FixedPriceId = id;//id.FixedPriceId;
                             //surchagrge.IsBothAreapointApplied = ((id.FixedPriceId != null && id.FixedPriceId > 0) || (id.ToAreaPointId != null && id.ToAreaPointId > 0));
                            db.SpecialOccasionAreaPoints.Add(surchagrge);
                            db.SaveChanges();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");


            }
        }

        public List<SpOccCustomerRateViewModel> GetAllCurrentRate()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<SpOccCustomerRateViewModel> objSpOccCusRates =
                        db.Rate_Customer.Where(x => x.IsActive && !x.IsDelete)
                            .Select(x => new SpOccCustomerRateViewModel()
                            {
                                RateId = x.RateId,
                                RateName = x.RateName
                            });
                    return objSpOccCusRates.ToList();
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return null;
        }
        public List<POIAddressViewModel> GetAllPoIAddress(string address)
        {
            string outerPlot = string.Empty;
            string name = string.Empty; 
            try
            {
                
                int numericValue;
                if (int.TryParse(address.Substring(address.Length - 1), out numericValue))
                {
                    outerPlot= address;
                    name = "";
                }
                else
                {
                    outerPlot = "";
                    name = address;
                }


                using (var db = new BATransferEntities())
                {
                    IEnumerable<POIAddressViewModel> objSpOccCusRates =
                        db.sp_load_pintofinterest_address(outerPlot, name).ToList()
                            .Select(x => new POIAddressViewModel()
                            {
                                AriaPointId = x.AriaPointId,
                                OuterPostcode = x.OuterPostcode,
                                FullPostcode = x.FullPostcode,
                                Name = x.Name,
                                PickupCharge = x.PickupCharge,
                                DropoffCharge = x.DropoffCharge,
                                Lat = x.Lat,
                                Long = x.Long,
                                IsDefault = x.IsDefault
                            });
                    return objSpOccCusRates.ToList();
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return null;
        }


        public AllSpecialOccationViewModel GetAllPoIAddress(long specialOccasionId)
        {
            var allSpecialOccationViewModel = new AllSpecialOccationViewModel(); 
            try
            {
                 
                using (var db = new BATransferEntities())
                {
                    IEnumerable<POIAddressViewModel> objAllAddress = 
                        db.sp_load_pintofinterest_all_address(specialOccasionId).ToList()
                            .Select(x => new POIAddressViewModel()
                            {
                                AriaPointId = x.AriaPointId,
                                OuterPostcode = x.OuterPostcode,
                                FullPostcode = x.FullPostcode,
                                Name = x.AreaTypeId==(int)AreapointTypeEnum.Place? x.OuterPostcode+" - "+x.Name: x.Name,
                                PickupCharge = x.PickupCharge,
                                DropoffCharge = x.DropoffCharge,
                                Lat = x.Lat,
                                Long = x.Long,
                                IsDefault = x.IsDefault
                            });
                    allSpecialOccationViewModel.AllPoiAddress = objAllAddress.ToList();

                    IEnumerable<POIAddressViewModel> objSpOccCusRates =
                       db.sp_load_pintofinterest_specialocations(specialOccasionId).ToList() 
                           .Select(x => new POIAddressViewModel()
                           {
                               AriaPointId = x.AriaPointId,
                               OuterPostcode = x.OuterPostcode,
                               FullPostcode = x.FullPostcode,
                               Name = x.AreaTypeId == (int)AreapointTypeEnum.Place ? x.OuterPostcode + " - " + x.Name : x.Name,
                               PickupCharge = x.PickupCharge,
                               DropoffCharge = x.DropoffCharge,
                               Lat = x.Lat,
                               Long = x.Long,
                               IsDefault = x.IsDefault
                           });

                    allSpecialOccationViewModel.SpecialOccAddress = objSpOccCusRates.ToList();
                    return allSpecialOccationViewModel;
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return null;
        }

        public MultiplePriceUpdateViewModel GetRelevantFixedPrices(string fromSearch, string toSearch, bool isSpecialOccasion)
        {

            var relevantFixedPrices = new MultiplePriceUpdateViewModel();
            try
            {
                
                if(isSpecialOccasion)
                {
                    fromSearch = "";
                    toSearch = "";
                }

                using (var db = new BATransferEntities())
                {
                    IEnumerable<BasicPriceViewModel> fixedPrices =
                        db.sp_swaran_loadrelevantfixedprice(fromSearch, toSearch, isSpecialOccasion).ToList()
                            .Select(x => new BasicPriceViewModel()
                            {
                                FixedPriceId = x.FixedPriceId,
                                FromLocation = x.FromLocation + "=>" + x.ToLocation + ": " + x.BasicPrice.ToString(),
                                //ToLocation = x.ToLocation,
                                BasicPrice = x.BasicPrice
                            });

                    relevantFixedPrices.FixedPrices = fixedPrices.ToList();
                    
                    return relevantFixedPrices;
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }

            if(relevantFixedPrices.FixedPrices == null)
            {
                relevantFixedPrices.FixedPrices = new List<BasicPriceViewModel>();
            }

            relevantFixedPrices.FixedPrices.Add(new BasicPriceViewModel());
            return relevantFixedPrices;
        }

        public MultiplePriceUpdateViewModel GetRelevantFixedPricesSpecialOccasion(long specialOccasionId)
        {

            var relevantFixedPrices = new MultiplePriceUpdateViewModel();
            try
            {
                
                using (var db = new BATransferEntities())
                {
                    IEnumerable<BasicPriceViewModel> fixedPrices =
                        db.sp_swaran_loadrelevantfixedprice_specialOccassion(specialOccasionId, true).ToList()
                            .Select(x => new BasicPriceViewModel()
                            {
                                FixedPriceId = x.FixedPriceId,
                                FromLocation = x.FromLocation + "=>" + x.ToLocation + ": " + x.BasicPrice.ToString(),
                                 ToLocation = x.ToLocation,
                                BasicPrice = x.BasicPrice
                            });

                    relevantFixedPrices.FixedPrices = fixedPrices.ToList();

                    IEnumerable<BasicPriceViewModel> nonaddedfixedPrices =
                        db.sp_swaran_loadrelevantfixedprice_specialOccassion(specialOccasionId, false).ToList()
                            .Select(x => new BasicPriceViewModel()
                            {
                                FixedPriceId = x.FixedPriceId,
                                FromLocation = x.FromLocation + "=>" + x.ToLocation + ": " + x.BasicPrice.ToString(),
                                ToLocation = x.ToLocation,
                                BasicPrice = x.BasicPrice
                            });

                    relevantFixedPrices.NonAddedFixedPrices = nonaddedfixedPrices.ToList();
                   
                    return relevantFixedPrices;
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }

            if (relevantFixedPrices.FixedPrices == null)
            {
                relevantFixedPrices.FixedPrices = new List<BasicPriceViewModel>();
            }

            if (relevantFixedPrices.NonAddedFixedPrices == null)
            {
                relevantFixedPrices.NonAddedFixedPrices = new List<BasicPriceViewModel>();
            }

            relevantFixedPrices.FixedPrices.Add(new BasicPriceViewModel());
            relevantFixedPrices.NonAddedFixedPrices.Add(new BasicPriceViewModel());
            return relevantFixedPrices;
        }


        public List<POIAddressViewModel> GetRelevantPointOfInterests(string fromSearchString, string toSearchString)
        {
            //string outerPlot = string.Empty;
            //string name = string.Empty;
            try
            {

                //int numericValue;
                //if (int.TryParse(searchString.Substring(searchString.Length - 1), out numericValue))
                //{
                //    outerPlot = searchString;
                //    name = "";
                //}
                //else
                //{
                //    outerPlot = "";
                //    name = searchString;
                //}


                using (var db = new BATransferEntities())
                {
                    IEnumerable<POIAddressViewModel> objSpOccCusRates =
                        db.sp_swaran_loadrelevantpointofinterest(fromSearchString, toSearchString).ToList()
                            .Select(x => new POIAddressViewModel()
                            {
                                AriaPointId = x.AriaPointId,
                                OuterPostcode = x.OuterPostcode,
                                FullPostcode = x.FullPostcode,
                                Name = x.Name,
                                PickupCharge = x.PickupCharge,
                                DropoffCharge = x.DropoffCharge,
                                Lat = x.Lat,
                                Long = x.Long,
                                IsDefault = x.IsDefault
                            });
                    return objSpOccCusRates.ToList();
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return null;
        }

        public bool SaveFixedPrice(SaveFixedPriceDom saveFixedPriceRequest, string userId)
        {

            try
            {
                using (var db = new BATransferEntities())
                {
                    FixedPriceIdGenerator newFixedPriceIdGenerator = new FixedPriceIdGenerator()
                    {
                        UserId = userId,
                        CreatedDate = DateTime.UtcNow,
                        ModifiedDate = DateTime.UtcNow
                    };

                    db.FixedPriceIdGenerators.Add(newFixedPriceIdGenerator);
                    db.SaveChanges();

                    FixedPrice_AriaPoints_Distance newFixedPrice_AriaPoints_Distance = new FixedPrice_AriaPoints_Distance()
                    {
                        PriceId = newFixedPriceIdGenerator.FixedPriceId,
                        BasicPrice = saveFixedPriceRequest.BasicPrice,
                        FromAriapointId = (int)saveFixedPriceRequest.FromAreaPointId,
                        ToAriapointId = (int)saveFixedPriceRequest.ToAreaPointId,
                        driving_distance = saveFixedPriceRequest.Distance.ToString(),
                        driving_time = saveFixedPriceRequest.Duration.ToString(),
                        last_updated = DateTime.UtcNow.ToString(),
                        comment = string.Empty,
                        basic_price_old_new = 0

                    };

                    db.FixedPrice_AriaPoints_Distance.Add(newFixedPrice_AriaPoints_Distance);
                    db.SaveChanges();
                }

                return true;
            }
            catch(Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
                return false;
            }
            
        }

        public AllDailySurgeViewModel GetDailySurgePoIAddress(long dailySurgeId)
        {
            var allDailySurgeViewModel = new AllDailySurgeViewModel();
            try
            {

                using (var db = new BATransferEntities())
                {
                    IEnumerable<POIAddressViewModel> objAllAddress =
                        db.sp_load_pointofinterest_dailySurge(dailySurgeId, false).ToList()
                            .Select(x => new POIAddressViewModel()
                            {
                                AriaPointId = x.AriaPointId,
                                OuterPostcode = x.OuterPostcode,
                                FullPostcode = x.FullPostcode,
                                Name = x.AreaTypeId == (int)AreapointTypeEnum.Place ? x.OuterPostcode + " - " + x.Name : x.Name,
                                PickupCharge = x.PickupCharge,
                                DropoffCharge = x.DropoffCharge,
                                Lat = x.Lat,
                                Long = x.Long,
                                IsDefault = x.IsDefault
                            });
                    allDailySurgeViewModel.AllPoiAddress = objAllAddress.ToList();

                    IEnumerable<POIAddressViewModel> objSpOccCusRates =
                        db.sp_load_pointofinterest_dailySurge(dailySurgeId, true).ToList()
                           .Select(x => new POIAddressViewModel()
                           {
                               AriaPointId = x.AriaPointId,
                               OuterPostcode = x.OuterPostcode,
                               FullPostcode = x.FullPostcode,
                               Name = x.AreaTypeId == (int)AreapointTypeEnum.Place ? x.OuterPostcode + " - " + x.Name : x.Name,
                               PickupCharge = x.PickupCharge,
                               DropoffCharge = x.DropoffCharge,
                               Lat = x.Lat,
                               Long = x.Long,
                               IsDefault = x.IsDefault
                           });

                    allDailySurgeViewModel.SpecialOccAddress = objSpOccCusRates.ToList();
                    return allDailySurgeViewModel;
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return null;
        }
    }
}