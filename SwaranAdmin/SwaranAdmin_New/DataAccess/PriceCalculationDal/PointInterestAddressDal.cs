﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace SwaranAdmin_New.DataAccess.PriceCalculationDal
{
    public class PointInterestAddressDal
    {
        #region Singalton
        private static PointInterestAddressDal _instance;
        private static readonly object LockThis = new object();
        protected PointInterestAddressDal()
        {
        }
        public static PointInterestAddressDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new PointInterestAddressDal());
                }
            }
        }


        #endregion


        public List<FixedPriceViewModel> GetAllPOIAddress()
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    IEnumerable<FixedPriceViewModel> objaddress =
                        db.sp_swaran_loadallfixedprice().ToList().Select(x => new FixedPriceViewModel()
                        {
                            IsAwayFrom = x.FromAway,
                            IsAwayTo = x.ToAway,
                            FixedPriceId = x.FixedPriceId,
                            FromAriapointId = x.FromAriapointId ?? 0,
                            FromLocation = x.FromLocation,
                            ToAriapointId = x.ToAriapointId ?? 0,
                            ToLocation = x.ToLocation,
                            BasicPrice = x.BasicPrice,
                            FromPickupCharge = x.FromPickupCharge,
                            FromDropoffCharge = x.FromDropoffCharge,
                            ToPickupCharge = x.ToPickupCharge,
                            ToDropOffCharge = x.ToDropOffCharge
                        });

                    return objaddress.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");


            }
            return null;
        }

        public void SaveBulkUpdatePrice(MultiplePriceUpdateViewModel model)
        {

            try
            {
                using (var db = new BATransferEntities())
                {

                    if (model.AriaPointId != null && model.BasicPrice > 0)
                    {

                        //var relevantFixedPrices = db.FixedPrice_AriaPoints_Distance.Where(fad => fad.PriceId)

                        var result = from f in db.FixedPrice_AriaPoints_Distance
                                     where model.AriaPointId.Select(m => m).Contains(f.PriceId)
                                     select f;

                        if (result != null && result.Any())
                        {
                            foreach(var fixedPrice in result)
                            {
                                fixedPrice.BasicPrice = model.BasicPrice;
                                fixedPrice.last_updated = DateTime.UtcNow.ToString();
                            }

                            db.SaveChanges();
                        }
                    }

                    //if (db.Rate_SpecialOccasion.Any(x => x.OccasionsId == model.OccasionsId))
                    //{
                    //    var occ = db.Rate_SpecialOccasion.First(x => x.OccasionsId == model.OccasionsId);
                    //    occ.CustomerRateId = model.CustomerRateId;
                    //    occ.Surcharge = model.Surcharge;
                    //    occ.RateFormula = model.RateName + model.RateFormula.Trim();
                    //    occ.Title = model.Title;
                    //    occ.FromDate = ValueParseHelper.TryParseDateTime(model.FromDate);
                    //    occ.ToDate = ValueParseHelper.TryParseDateTime(model.ToDate);
                    //    occ.Comments = model.Comments;
                    //    occ.IsEnabled = model.IsEnabled;
                    //    occ.IsDeleted = model.IsDeleted;
                    //    occ.IsAllPlot = model.AriaPointId == null ? true : model.IsAllPlot;
                    //    occ.DropOffCharge = model.DropOffCharge;
                    //    occ.DropOffComments = model.DropOffComments ?? "";
                    //    occ.CreatedDate = DateTime.UtcNow;
                    //    occ.ModifiedDate = DateTime.UtcNow;
                    //    db.SaveChanges();
                    //    db.sp_swaran_delete_special_surcharge(model.OccasionsId);

                    //}
                    //else
                    //{
                    //    var occ = new Rate_SpecialOccasion();
                    //    occ.CustomerRateId = model.CustomerRateId;
                    //    occ.Surcharge = model.Surcharge;
                    //    occ.RateFormula = model.RateName + model.RateFormula.Trim();
                    //    occ.Title = model.Title;
                    //    occ.FromDate = ValueParseHelper.TryParseDateTime(model.FromDate);
                    //    occ.ToDate = ValueParseHelper.TryParseDateTime(model.ToDate);
                    //    occ.Comments = model.Comments;
                    //    occ.IsEnabled = model.IsEnabled;
                    //    occ.IsDeleted = model.IsDeleted;
                    //    occ.IsAllPlot = model.AriaPointId == null ? true : model.IsAllPlot;
                    //    occ.DropOffCharge = model.DropOffCharge;
                    //    occ.DropOffComments = model.DropOffComments ?? "";
                    //    occ.CreatedDate = DateTime.UtcNow;
                    //    occ.ModifiedDate = DateTime.UtcNow;
                    //    db.Rate_SpecialOccasion.Add(occ);
                    //    db.SaveChanges();
                    //    model.OccasionsId = occ.OccasionsId;
                    //}

                    //if (!model.IsAllPlot && model.AriaPointId != null)
                    //{
                    //    foreach (var id in model.AriaPointId)
                    //    {
                    //        var surchagrge = new SpecialOccasionAreaPoint();
                    //        surchagrge.AreaPointId = id;
                    //        surchagrge.SpecialOccasionId = model.OccasionsId;
                    //        surchagrge.isEnabled = true;
                    //        surchagrge.isDeleted = false;
                    //        surchagrge.sysModifiedDate = DateTime.UtcNow;
                    //        surchagrge.sysCreatedDate = DateTime.UtcNow;
                    //        db.SpecialOccasionAreaPoints.Add(surchagrge);
                    //        db.SaveChanges();

                    //    }
                    //}
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");


            }
        }
    }
}