﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SwaranAdmin_New.DataAccess
{
    public class CustomerFeedbackDal
    {
        #region Singleton
        private static CustomerFeedbackDal _instance;
        private static readonly object LockThis = new object();
        protected CustomerFeedbackDal()
        {
        }


        public static CustomerFeedbackDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new CustomerFeedbackDal());
                }
            }
        }



        #endregion
        public List<FeedbacksViewModel> LoadFeedbacks(int numberOfRecord, int currentpage)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<FeedbacksViewModel> objFeedbacks =
                        db.sp_swaran_getFeedbacks(currentpage, numberOfRecord).ToList()
                            .Select(x => new FeedbacksViewModel()
                            {
                                FeedBackId = x.CustomerFeedbackId,
                                BookingId = x.BookingId,
                                BookingNumber = x.BookingNumber,
                                CustomerEmail = x.Email,
                                C_Did_the_driver_arrive_on_time_ = x.Did_the_driver_arrive_on_time_,
                                Comments = x.Comments,
                                CustomerName = x.CustomerName,
                                Did_the_driver_help_you_with_the_luggage_ = x.Did_the_driver_help_you_with_the_luggage_,
                                Did_the_driver_maintain_appropriate_speed_ = x.Did_the_driver_maintain_appropriate_speed_,
                                DriverRating = x.DriverRating,
                                FeedbackDate = x.FeedbackDate.ToString("yyyy MMMM dd"),
                                Improvements = x.Improvements,
                                IsDriverOfferedBusinessCard = x.IsDriverOfferedBusinessCard,
                                IsVehicleClean = x.IsVehicleClean,
                                MercuryNumber = x.MercuryNumber,
                                OrganizationName = x.OrganizationName,
                                ReservationRating = x.ReservationRating,
                                ReservationRatingPercentage = (x.ReservationRating / 5.00m) * 100,
                                Was_the_driver_polite_and_friendly_ = x.Was_the_driver_polite_and_friendly_,
                                Was_the_driver_wearing_suit_ = x.Was_the_driver_wearing_suit_,
                                WillYouRecommendUs = x.WillYouRecommendUs,
                                PhoneNumber = x.PhoneNumber,
                                IsAddedToDriverAppreciation = x.IsAddedToDriverAppreciation,
                                IsAddedToDriverComplaint = x.IsAddedToDriverComplaint,
                                IsAddedToSpecialPreference = x.IsAddedToSpecialPreference,
                                IsAddedToSuggestion = x.IsAddedToSuggestion,
                                IsAddedToSystemComplaint = x.IsAddedToSystemComplaint,
                                IsAddedToTestimonial = x.IsAddedToTestimonial,
                                IsMailedToCustomer = x.IsMailedToCustomer,
                                IsMailedToStaff = x.IsMailedToStaff,
                                Loyalty = !string.IsNullOrEmpty(x.Loyalty) ? x.Loyalty.ToUpper() + " CUSTOMER" : "",
                              //  IsSentToCustomerService = x.IsSentToCustomerService ?? (bool)x.IsSentToCustomerService

                            });
                    return objFeedbacks.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }



        public int GetFeedBackPageCount(int numberOfRecord, int currentpage)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<FeedbacksViewModel> objFeedbacks =
                        db.sp_swaran_getFeedbacks(currentpage, numberOfRecord).ToList()
                            .Select(x => new FeedbacksViewModel()
                            {
                                NumberOfPages = x.NumberOfPages ?? 0,


                            });
                    return objFeedbacks.First().NumberOfPages;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return 0;
        }

        public StaffEmailViewModel GetStaffMail(long feedbackid)
        {
            var staffMail = new StaffEmailViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.StaffFeedbackResponseEmails.Any(x => x.StaffFeedbackId == feedbackid))
                    {
                        var feedback = db.StaffFeedbackResponseEmails.First(x => x.StaffFeedbackId == feedbackid);
                        staffMail.StaffName = feedback.CustomerFeedback.AspNetUser.FirstName + " " +
                                                     feedback.CustomerFeedback.AspNetUser.LastName;
                        staffMail.Message = feedback.EmailContent;
                        staffMail.OperatorName = feedback.AspNetUser.FirstName;
                        staffMail.ToAddress = feedback.Email;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return staffMail;
        }

        public SystemComplaintViewModel GetSystemComplaintView(long feedbackid)
        {
            var systemComplint = new SystemComplaintViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.SystemComplaints.Any(x => x.FeedBackId == feedbackid))
                    {
                        var feedback = db.SystemComplaints.First(x => x.FeedBackId == feedbackid);
                        systemComplint.OperatorName = feedback.AspNetUser.FirstName;
                        systemComplint.CustomerEmail = feedback.CustomerEmail;
                        systemComplint.Complaint1 = feedback.MessageContant;
                        systemComplint.CustomerName = feedback.CustomerName;
                        systemComplint.CustomerPhone = feedback.CustomerPhone;
                        systemComplint.IncidentDate = feedback.IncidentDate.ToString();

                        systemComplint.Seriousness = feedback.Seriousness.ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return systemComplint;
        }

        public List<SystemComplaintViewModel> GetSystemComplaintView(int numberOfRecord, int currentpage)
        {

            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<SystemComplaintViewModel> objSystemComplients =
                        db.sp_swaran_RetrieveSystemComplaint(currentpage, numberOfRecord).ToList().Select(x => new SystemComplaintViewModel()
                        {
                            //  ComplaintId = x.,
                            OperatorName = x.OperatorName,
                            CustomerEmail = x.CustomerEmail,
                            Complaint1 = x.MessageContant,
                            CustomerName = x.CustomerName,
                            CustomerPhone = x.CustomerPhone,
                            IncidentDate = x.IncidentDate.ToString(),
                            Seriousness = x.Seriousness.ToString(),
                            CustomerInfo = (!string.IsNullOrEmpty(x.CustomerName) ? x.CustomerName : "") +
                            (!string.IsNullOrEmpty(x.CustomerEmail) ? "/" + x.CustomerEmail : "") +
                            (!string.IsNullOrEmpty(x.CustomerPhone) ? "/" + x.CustomerPhone : "")

                        });
                    return objSystemComplients.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }


        public int GetSystemComplaintPageCount(int numberOfRecord, int currentpage)
        {

            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<SystemComplaintViewModel> objSystemComplients =
                        db.sp_swaran_RetrieveSystemComplaint(currentpage, numberOfRecord).ToList().Select(x => new SystemComplaintViewModel()
                        {
                            PageCount = x.NumberOfPages ?? 0

                        });
                    return objSystemComplients.First().PageCount;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return 0;
        }
        public CustomerEmailViewModel GetCustomerMail(long feedbackid)
        {
            var customerEmail = new CustomerEmailViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.CustomerFeedbackResponseEmails.Any(x => x.CustomerFeedbackId == feedbackid))
                    {
                        var feedback = db.CustomerFeedbackResponseEmails.First(x => x.CustomerFeedbackId == feedbackid);
                        customerEmail.CustomerName = feedback.CustomerFeedback.AspNetUser.FirstName + " " +
                                                     feedback.CustomerFeedback.AspNetUser.LastName;
                        customerEmail.Message = feedback.EmailContent;
                        customerEmail.StaffName = feedback.AspNetUser.FirstName;
                        customerEmail.ToAddress = feedback.Email;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return customerEmail;
        }

        public SpecialPreferencesViewModel GetPickupNote(long? feedbackid)
        {
            var specialPreferences = new SpecialPreferencesViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {

                    if (db.CustomerFeedbacks.Any(x => x.CustomerFeedbackId == feedbackid))
                    {
                        specialPreferences.Note =
                            db.CustomerFeedbacks.First(x => x.CustomerFeedbackId == feedbackid).Comments;
                    }

                    //if (bookingId != null && db.Bookings.Any(x => x.BookingId == bookingId))
                    //{
                    //    var booking = db.Bookings.First(x => x.BookingId == bookingId);
                    //    pickupNote.AddressId = booking.FromFixedPriceAriaPointId;
                    //    pickupNote.UserId = booking.UserId;
                    //    if (db.FavouriteAddresses.Any(
                    //        x => x.UserId.Equals(booking.UserId) && x.AddressId == booking.FromFixedPriceAriaPointId))
                    //    {


                    //        var favouritAddress = db.FavouriteAddresses.First(
                    //            x => x.UserId.Equals(booking.UserId) && x.AddressId == booking.FromFixedPriceAriaPointId);
                    //        pickupNote.FavouriteAddressId = favouritAddress.FavouriteAddressId;
                    //        pickupNote.Note = favouritAddress.Note;

                    //    }

                    //}
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return specialPreferences;
        }



        public bool UpdateSpecialPreferences(SpecialPreferencesViewModel model)
        {
            try
            {
                CustomerFeedback customerFeedback = null;
                using (var db = new BATransferEntities())
                {

                    if (db.CustomerFeedbacks.Any(x => x.CustomerFeedbackId == model.Feedbackid))
                    {
                        customerFeedback =
                            db.CustomerFeedbacks.First(x => x.CustomerFeedbackId == model.Feedbackid);

                        if (db.CustomerSpecialPreferences.Any(x => x.CustomerUserId.Equals(customerFeedback.UserId)))
                        {
                            var specialPreferenceObj =
                                db.CustomerSpecialPreferences.First(x => x.CustomerUserId.Equals(customerFeedback.UserId));

                            specialPreferenceObj.CustomerFeedbackId = model.Feedbackid;
                            specialPreferenceObj.AddressId = model.AddressId;
                            //  specialPreferenceObj.Note = model.Note;
                            specialPreferenceObj.CustomerUserId = customerFeedback.UserId;
                            specialPreferenceObj.StaffUserId = model.StaffId;
                            specialPreferenceObj.sysCreatedDate = DateTime.UtcNow;
                            specialPreferenceObj.sysModifiedDate = DateTime.UtcNow;
                            db.SaveChanges();
                        }
                        else
                        {


                            var specialPreference = new CustomerSpecialPreference();
                            specialPreference.CustomerFeedbackId = model.Feedbackid;
                            specialPreference.AddressId = model.AddressId;
                            //    specialPreference.Note = model.Note;
                            specialPreference.CustomerUserId = customerFeedback.UserId;
                            specialPreference.StaffUserId = model.StaffId;
                            specialPreference.IsDeleted = false;
                            specialPreference.IsEnabled = true;
                            specialPreference.sysCreatedDate = DateTime.UtcNow;
                            specialPreference.sysModifiedDate = DateTime.UtcNow;
                            db.CustomerSpecialPreferences.Add(specialPreference);
                            db.SaveChanges();
                        }

                        customerFeedback.IsAddedToSpecialPreference = true;
                        db.SaveChanges();
                        return true;
                    }



                    //if (db.FavouriteAddresses.Any(
                    //    x => x.FavouriteAddressId == model.FavouriteAddressId))
                    //{
                    //    var favouritAddress = db.FavouriteAddresses.First(x => x.FavouriteAddressId == model.FavouriteAddressId);
                    //    favouritAddress.Note = model.PickupNote;
                    //    db.SaveChanges();

                    //}
                    //else
                    //{
                    //    var favouriteAddress = new FavouriteAddress();
                    //    favouriteAddress.AddressId = model.AddressId;
                    //    favouriteAddress.UserId = model.UserId;
                    //    favouriteAddress.Note = model.PickupNote;
                    //    favouriteAddress.IsOld = false;
                    //    favouriteAddress.NumberOfJourneys = 1;
                    //    favouriteAddress.Label = "";
                    //    favouriteAddress.isEnabled = true;
                    //    favouriteAddress.isDeleted = false;
                    //    favouriteAddress.AirportCode = "";
                    //    favouriteAddress.sysModifiedDate = DateTime.UtcNow;
                    //    favouriteAddress.sysCreatedDate = DateTime.UtcNow;
                    //    db.FavouriteAddresses.Add(favouriteAddress);
                    //    db.SaveChanges();
                    //}


                    //if (db.CustomerFeedbacks.Any(x => x.CustomerFeedbackId == model.Feedbackid))
                    //{
                    //    var customerFeedback =
                    //        db.CustomerFeedbacks.First(x => x.CustomerFeedbackId == model.Feedbackid);
                    //    customerFeedback.IsAddedToSpecialPreference = true;
                    //    db.SaveChanges();
                    //}

                    //return true;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }
        public List<SelectListItem> GetCustomerEmailTeplets()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<SelectListItem> objEmailTemplate =
                        db.CustomerEmailTemplates.Where(x => x.IsEnabled && !x.IsDeleted)
                            .Select(x => new SelectListItem()
                            {
                                Text = x.TemplateName,
                                Value = x.Template
                            });
                    return objEmailTemplate.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public async Task<EmailVerificationViewModel> SendCustomerMail(CustomerEmailViewModel model)
        {
            var userVerify = new EmailVerificationViewModel();
            try
            {
                var listEmails = new List<string>() { model.ToAddress };
                EmailCredentialsViewModel emailCredentials = EmailHelper.GetCustomerEmaillCredentials();
                var rootEmail = new RootEmail();
                rootEmail.Attachments = null;
                rootEmail.Body = model.Message;
                rootEmail.FromEmailAddress = emailCredentials.FromEmail;
                rootEmail.Username = emailCredentials.Username;
                rootEmail.Password = emailCredentials.Password;
                rootEmail.Port = emailCredentials.Port;
                rootEmail.Smtp = emailCredentials.Smtp;
                rootEmail.Subject = model.Subject;
                rootEmail.ToEmailAddresses = listEmails;
                rootEmail.DisplayName = emailCredentials.DisplayName;
                rootEmail.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                userVerify.IsSuccess = await EmailHelper.SendEmailWithoutAttachtment(rootEmail, apiUrl);
                userVerify.Message = "Email successfully sent";
                if (userVerify.IsSuccess)
                {
                    using (var db = new BATransferEntities())
                    {


                        if (db.CustomerFeedbacks.Any(x => x.CustomerFeedbackId == model.Feedbackid))
                        {
                            var customerFeedback =
                                db.CustomerFeedbacks.First(x => x.CustomerFeedbackId == model.Feedbackid);
                            customerFeedback.IsMailedToCustomer = true;
                            db.SaveChanges();
                        }

                        var customerMail = new CustomerFeedbackResponseEmail();
                        customerMail.CustomerFeedbackId = model.Feedbackid;
                        customerMail.Email = model.ToAddress;
                        customerMail.EmailContent = model.Message;
                        customerMail.IsDeleted = false;
                        customerMail.IsEnabled = true;
                        customerMail.IsSent = true;
                        customerMail.StaffUserId = model.StaffId;
                        customerMail.sysCreatedDate = DateTime.UtcNow;
                        customerMail.sysModifiedDate = DateTime.UtcNow;
                        db.CustomerFeedbackResponseEmails.Add(customerMail);
                        db.SaveChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return userVerify;
        }


        public async Task<EmailVerificationViewModel> SendStaffMail(StaffEmailViewModel model)
        {
            var userVerify = new EmailVerificationViewModel();
            try
            {
                var listEmails = new List<string>() { model.ToAddress };
                EmailCredentialsViewModel emailCredentials = EmailHelper.GetCustomerEmaillCredentials();
                var rootEmail = new RootEmail();
                rootEmail.Attachments = null;
                rootEmail.Body = model.Message;
                rootEmail.FromEmailAddress = emailCredentials.FromEmail;
                rootEmail.Username = emailCredentials.Username;
                rootEmail.Password = emailCredentials.Password;
                rootEmail.Port = emailCredentials.Port;
                rootEmail.Smtp = emailCredentials.Smtp;
                rootEmail.Subject = model.Subject;
                rootEmail.ToEmailAddresses = listEmails;
                rootEmail.DisplayName = emailCredentials.DisplayName;
                rootEmail.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                userVerify.IsSuccess = await EmailHelper.SendEmailWithoutAttachtment(rootEmail, apiUrl);
                userVerify.Message = "Email successfully sent";
                if (userVerify.IsSuccess)
                {
                    using (var db = new BATransferEntities())
                    {


                        if (db.CustomerFeedbacks.Any(x => x.CustomerFeedbackId == model.Feedbackid))
                        {
                            var customerFeedback =
                                db.CustomerFeedbacks.First(x => x.CustomerFeedbackId == model.Feedbackid);
                            customerFeedback.IsMailedToStaff = true;
                            db.SaveChanges();
                        }

                        var staffMail = new StaffFeedbackResponseEmail();
                        staffMail.StaffFeedbackId = model.Feedbackid;
                        staffMail.Email = model.ToAddress;
                        staffMail.EmailContent = model.Message;
                        staffMail.IsDeleted = false;
                        staffMail.IsEnabled = true;
                        staffMail.IsSent = true;
                        staffMail.StaffUserId = model.StaffId;
                        staffMail.sysCreatedDate = DateTime.UtcNow;
                        staffMail.sysModifiedDate = DateTime.UtcNow;
                        db.StaffFeedbackResponseEmails.Add(staffMail);
                        db.SaveChanges();

                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return userVerify;
        }

        public bool SaveCustomerReview(CustomerReviewViewModel review)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var customerReview = new CustomerReview();
                    customerReview.Email = review.Email;
                    customerReview.Location = review.Location ?? "";
                    customerReview.Title = review.Title ?? "";
                    customerReview.Rating = review.Rating;
                    customerReview.Message = review.Message;
                    customerReview.Name = review.Name;
                    customerReview.CreatedDate = DateTime.UtcNow;
                    customerReview.SiteId = review.SiteId;
                    customerReview.IsActive = true;
                    customerReview.IsDeleted = false;
                    customerReview.IsEnable = true;
                    db.CustomerReviews.Add(customerReview);
                    db.SaveChanges();

                    if (db.CustomerFeedbacks.Any(x => x.CustomerFeedbackId == review.FeedbackId))
                    {
                        var customerFeedback =
                            db.CustomerFeedbacks.First(x => x.CustomerFeedbackId == review.FeedbackId);
                        customerFeedback.IsAddedToTestimonial = true;
                        db.SaveChanges();
                    }


                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }

        public CustomerReviewViewModel GetCurrentFeedbcak(long feddbackid)
        {
            try
            {
                var customerReview = new CustomerReviewViewModel();
                using (var db = new BATransferEntities())
                {

                    var objFeedback = db.sp_swaran_currentfeedback(feddbackid).ToList().First();

                    customerReview.Name = objFeedback.CustomerName;
                    customerReview.Message = objFeedback.Comments;
                    customerReview.Email = objFeedback.Email;
                    customerReview.PhoneNumber = objFeedback.PhoneNumber;
                    customerReview.SiteId = (int)Enum.Parse(typeof(BookingSiteEnum), objFeedback.OrganizationName);
                    customerReview.Title = "";
                    return customerReview;
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                     System.Reflection.MethodBase.GetCurrentMethod().Name,
                     ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public List<SelectListItem> GetAllStaffEmail()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<SelectListItem> objEmailTemplate =
                        db.sp_swaran_getstaff_emails(1).Select(x => new SelectListItem()
                        {
                            Text = x.FirstName + " " + x.LastName,
                            Value = x.Email
                        });
                    return objEmailTemplate.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }
        public bool SaveSystemComplaint(SystemComplaintViewModel model)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var systemComplint = new SystemComplaint();
                    systemComplint.LiveComplaintId = 0;
                    systemComplint.MercuryNumber = null;
                    systemComplint.VehicleNo = null;
                    systemComplint.EventTitle = null;
                    systemComplint.CreatedBy = model.CreatedBy;
                    systemComplint.CustomerEmail = model.CustomerEmail;
                    systemComplint.MessageContant = model.Complaint1;
                    systemComplint.CustomerName = model.CustomerName;
                    systemComplint.CustomerPhone = model.CustomerPhone;
                    systemComplint.IncidentDate = DateTime.UtcNow;
                    systemComplint.FeedBackId = model.FeedBackId;

                    systemComplint.Seriousness = model.Seriousness.Equals("Major")
                        ? (int)SeriousnessEnum.Major
                        : (int)SeriousnessEnum.Minor;
                    //  systemComplint.UserId = model.UserId;
                    systemComplint.sysCreatedDate = DateTime.UtcNow;
                    systemComplint.sysModifiedDate = DateTime.UtcNow;
                    db.SystemComplaints.Add(systemComplint);
                    db.SaveChanges();

                    if (db.CustomerFeedbacks.Any(x => x.CustomerFeedbackId == model.FeedBackId))
                    {
                        var customerFeedback =
                            db.CustomerFeedbacks.First(x => x.CustomerFeedbackId == model.FeedBackId);
                        customerFeedback.IsAddedToSystemComplaint = true;
                        db.SaveChanges();
                    }


                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }


        public DriverComplaintViewModel GetDriverComplaint(long feedBackId)
        {
            var driverComplaint = new DriverComplaintViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.DriverComplaints != null && db.DriverComplaints.Any(x => x.FeedBackId == feedBackId))
                    {
                        var feedBack = db.DriverComplaints.First(x => x.FeedBackId == feedBackId);
                        driverComplaint.MercuryNumber = null;
                        driverComplaint.VehicleNo = feedBack.VehicleNo;
                        driverComplaint.DriverName = feedBack.DriverName;
                        driverComplaint.DriverEmail = feedBack.DriverEmail;
                        driverComplaint.EventTitle = feedBack.DriverEventTitle.DriverEventTitleName;
                        driverComplaint.CreatedBy = feedBack.CreatedBy;
                        driverComplaint.CustomerEmail = feedBack.CustomerEmail;
                        driverComplaint.MessageContant = feedBack.MessageContant;
                        driverComplaint.CustomerName = feedBack.CustomerName;
                        driverComplaint.CustomerPhone = feedBack.CustomerPhone;
                        driverComplaint.IncidentDate = ValueParseHelper.ToStringDate(feedBack.IncidentDate);

                        driverComplaint.Seriousness = feedBack.Seriousness == 1 ? "ddd" : "trtrt";
                        driverComplaint.UserId = feedBack.UserId;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return driverComplaint;
        }

        public DriverAppreciationViewModel GetDriverAppreciation(long feedBackId)
        {
            var driverComplaint = new DriverAppreciationViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.DriverAppreciations != null && db.DriverAppreciations.Any(x => x.CustomerFeedbackId == feedBackId))
                    {
                        var feedBack = db.DriverComplaints.First(x => x.FeedBackId == feedBackId);
                        //driverComplaint.MercuryNumber = null;
                        //driverComplaint.VehicleNo = feedBack.VehicleNo;
                        //driverComplaint.DriverName = feedBack.DriverName;
                        //driverComplaint.DriverEmail = feedBack.DriverEmail;
                        //driverComplaint.EventTitle = feedBack.DriverEventTitle.DriverEventTitleName;
                        //driverComplaint.CreatedBy = feedBack.CreatedBy;
                        //driverComplaint.CustomerEmail = feedBack.CustomerEmail;
                        //driverComplaint.MessageContant = feedBack.MessageContant;
                        //driverComplaint.CustomerName = feedBack.CustomerName;
                        //driverComplaint.CustomerPhone = feedBack.CustomerPhone;
                        //driverComplaint.IncidentDate = ValueParseHelper.ToStringDate(feedBack.IncidentDate);

                        //driverComplaint.Seriousness = feedBack.Seriousness == 1 ? "ddd" : "trtrt";
                        //driverComplaint.UserId = feedBack.UserId;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return driverComplaint;
        }
        public async Task<EmailVerificationViewModel> SaveDriverComplaint(DriverComplaintViewModel model)
        {
            var userVerify = new EmailVerificationViewModel();
            try
            {
                var listEmails = new List<string>() { model.DriverEmail };
                EmailCredentialsViewModel emailCredentials = EmailHelper.GetCustomerEmaillCredentials();
                var rootEmail = new RootEmail();
                rootEmail.Attachments = null;
                rootEmail.Body = model.MessageContant;
                rootEmail.FromEmailAddress = emailCredentials.FromEmail;
                rootEmail.Username = emailCredentials.Username;
                rootEmail.Password = emailCredentials.Password;
                rootEmail.Port = emailCredentials.Port;
                rootEmail.Smtp = emailCredentials.Smtp;
                rootEmail.Subject = model.EventTitle;
                rootEmail.ToEmailAddresses = listEmails;
                rootEmail.DisplayName = emailCredentials.DisplayName;
                rootEmail.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                userVerify.IsSuccess = await EmailHelper.SendEmailWithoutAttachtment(rootEmail, apiUrl);

                if (userVerify.IsSuccess)
                {
                    using (var db = new BATransferEntities())
                    {
                        var systemComplint = new DriverComplaint();
                        systemComplint.MercuryNumber = null;
                        systemComplint.DriverName = model.DriverName;
                        systemComplint.DriverEmail = model.DriverEmail;
                        systemComplint.VehicleNo = model.VehicleNo;
                        systemComplint.EventTitle = model.EventTitleId;
                        systemComplint.CreatedBy = model.CreatedBy;
                        systemComplint.CustomerEmail = model.CustomerEmail;
                        systemComplint.MessageContant = model.MessageContant;
                        systemComplint.CustomerName = model.CustomerName;
                        systemComplint.CustomerPhone = model.CustomerPhone;
                        systemComplint.IncidentDate = DateTime.UtcNow;
                        systemComplint.FeedBackId = model.FeedBackId;

                        systemComplint.Seriousness = model.Seriousness.Equals("Major")
                            ? (int)SeriousnessEnum.Major
                            : (int)SeriousnessEnum.Minor;
                        systemComplint.UserId = model.UserId;
                        systemComplint.sysCreatedDate = DateTime.UtcNow;
                        systemComplint.sysModifiedDate = DateTime.UtcNow;
                        db.DriverComplaints.Add(systemComplint);
                        db.SaveChanges();

                        if (db.CustomerFeedbacks.Any(x => x.CustomerFeedbackId == model.FeedBackId))
                        {
                            var customerFeedback =
                                db.CustomerFeedbacks.First(x => x.CustomerFeedbackId == model.FeedBackId);
                            customerFeedback.IsAddedToDriverComplaint = true;
                            db.SaveChanges();
                        }

                    }
                    return userVerify;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return userVerify;
        }
        public async Task<EmailVerificationViewModel> SaveDriverAppreciation(DriverAppreciationViewModel model)
        {
            var userVerify = new EmailVerificationViewModel();
            try
            {
                var listEmails = new List<string>() { model.DriverEmail };
                EmailCredentialsViewModel emailCredentials = EmailHelper.GetCustomerEmaillCredentials();
                var rootEmail = new RootEmail();
                rootEmail.Attachments = null;
                rootEmail.Body = model.MessageContant;
                rootEmail.FromEmailAddress = emailCredentials.FromEmail;
                rootEmail.Username = emailCredentials.Username;
                rootEmail.Password = emailCredentials.Password;
                rootEmail.Port = emailCredentials.Port;
                rootEmail.Smtp = emailCredentials.Smtp;
                rootEmail.Subject = model.AppreciationTitle;
                rootEmail.ToEmailAddresses = listEmails;
                rootEmail.DisplayName = emailCredentials.DisplayName;
                rootEmail.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                userVerify.IsSuccess = await EmailHelper.SendEmailWithoutAttachtment(rootEmail, apiUrl);

                if (userVerify.IsSuccess)
                {
                    using (var db = new BATransferEntities())
                    {
                        var systemComplint = new DriverAppreciation();
                        systemComplint.DriverName = model.DriverName;
                        systemComplint.DriverEmail = model.DriverEmail;
                        systemComplint.AppreciationTitleId = model.AppreciationTitleId;
                        systemComplint.CreatedBy = model.CreatedBy;
                        systemComplint.CustomerEmail = model.CustomerEmail;
                        systemComplint.MessageContant = model.MessageContant;
                        systemComplint.CustomerName = model.CustomerName;
                        systemComplint.CustomerPhone = model.CustomerPhone;
                        systemComplint.CustomerFeedbackId = model.FeedBackId;
                        systemComplint.sysCreatedDate = DateTime.UtcNow;
                        systemComplint.sysModifiedDate = DateTime.UtcNow;
                        db.DriverAppreciations.Add(systemComplint);
                        db.SaveChanges();

                        if (db.CustomerFeedbacks.Any(x => x.CustomerFeedbackId == model.FeedBackId))
                        {
                            var customerFeedback =
                                db.CustomerFeedbacks.First(x => x.CustomerFeedbackId == model.FeedBackId);
                            customerFeedback.IsAddedToDriverAppreciation = true;
                            db.SaveChanges();
                        }

                    }
                    return userVerify;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return userVerify;
        }
        public List<SelectListItem> GetDriverTitleTeplets()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<SelectListItem> objEmailTemplate =
                        db.DriverEventTitles.Where(x => x.IsEnabled && !x.IsDeleted)
                            .Select(x => new SelectListItem()
                            {
                                Text = x.DriverEventTitleName,
                                Value = x.DriverEventTitleId.ToString()
                            });
                    return objEmailTemplate.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public List<SelectListItem> GetDriverAppreciationTitleTeplets()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<SelectListItem> objEmailTemplate =
                        db.DriverAppreciationTitles.Where(x => x.IsEnabled && !x.IsDeleted)
                            .Select(x => new SelectListItem()
                            {
                                Text = x.TitleName,
                                Value = x.DriverAppreciationTitleId.ToString()
                            });
                    return objEmailTemplate.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public List<SelectListItem> GetDriverNames()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<SelectListItem> objEmailTemplate =
                        db.AspNetUsers.Where(x => x.IsDriver && x.isAuthorized && x.IsEnabled && !x.IsDeleted)
                            .Select(x => new SelectListItem()
                            {
                                Text = x.FirstName + " " + x.FirstName,
                                Value = x.Id.ToString()
                            });
                    return objEmailTemplate.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public string GetDriverEmail(string userid)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.AspNetUsers.Any(x => x.Id.Equals(userid)))
                    {
                        return db.AspNetUsers.First(x => x.Id.Equals(userid)).Email;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return string.Empty;
        }

        public bool GetHideFeedback(int feedbackid)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.CustomerFeedbacks.Any(x => x.CustomerFeedbackId == feedbackid))
                    {
                        var objFeedback = db.CustomerFeedbacks.First(x => x.CustomerFeedbackId == feedbackid);
                        objFeedback.IsEnabled = false;
                        objFeedback.IsDeleted = true;
                        objFeedback.sysModifiedDate = DateTime.UtcNow;
                        db.SaveChanges();
                        return true;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }

        public bool GetSystemComplaint(int complaintId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.SystemComplaints.Any(x => x.ComplaintId == complaintId))
                    {
                        var objSystemComplaint = db.SystemComplaints.First(x => x.ComplaintId == complaintId);
                        objSystemComplaint.IsEnabled = false;
                        objSystemComplaint.IsDeleted = true;
                        objSystemComplaint.sysModifiedDate = DateTime.UtcNow;
                        db.SaveChanges();
                        return true;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<OrganizationReviewSiteHeader> GetAllOrganizationReviewSites()
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    if (db.OrganizationReviewSites.Where(ors => ors.IsEnabled && !ors.IsDeleted).Any())
                    {
                        return db.OrganizationReviewSites.Where(ors => !ors.IsDeleted).Select(orsa => new OrganizationReviewSiteHeader()
                        {
                            OrganizationReviewSiteId = orsa.OrganizationReviewSiteId,
                            ReviewSiteId = orsa.ReviewSiteId,
                            ReviewSiteName = orsa.ReviewSite.ReviewSiteName,
                            OrganizationId = orsa.OrganizationId,
                            OrganizationName = orsa.Organization.OrganizationName,
                            IsPromotionalMessageEnabled = orsa.IsPromotionalMessageEnabled
                        }).ToList();
                    }

                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return new List<OrganizationReviewSiteHeader>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public OrganizationReviewSiteDetail GetRelevantOrganizationReviewSite(long organizationReviewSiteId)
        {

            try
            {
                using (var db = new BATransferEntities())
                {

                    var relavantOrganizationReviewSite = db.OrganizationReviewSites.FirstOrDefault(orsa => orsa.OrganizationReviewSiteId == organizationReviewSiteId);

                    if (relavantOrganizationReviewSite != null)
                    {
                        return new OrganizationReviewSiteDetail
                        {
                            OrganizationReviewSiteId = relavantOrganizationReviewSite.OrganizationReviewSiteId,
                            ReviewSiteId = relavantOrganizationReviewSite.ReviewSiteId,
                            ReviewSiteName = relavantOrganizationReviewSite.ReviewSite.ReviewSiteName,
                            OrganizationId = relavantOrganizationReviewSite.OrganizationId,
                            OrganizationName = relavantOrganizationReviewSite.Organization.OrganizationName,
                            IsPromotionalMessageEnabled = relavantOrganizationReviewSite.IsPromotionalMessageEnabled,
                            ApiKey = relavantOrganizationReviewSite.ApiKey,
                            BccEmail = relavantOrganizationReviewSite.BCCEmail,
                            IsBccEnabled = relavantOrganizationReviewSite.IsBccEnabled,
                            BeginDay = relavantOrganizationReviewSite.BeginDay,
                            EndDay = relavantOrganizationReviewSite.EndDay,
                            DefaultMessage = relavantOrganizationReviewSite.DefaultMessage,
                            PromotionalMessage = relavantOrganizationReviewSite.PromotionalMessage,
                            SendingOrder = relavantOrganizationReviewSite.SendingOrder,
                            MaximumReviewCount = relavantOrganizationReviewSite.MaximumReviewCount,
                            CurrentReviewCount = relavantOrganizationReviewSite.CurrentReviewCount,
                            IsEnabled = relavantOrganizationReviewSite.IsEnabled
                        };

                    }

                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool UpdateRelevantOrganizationReviewSite(OrganizationReviewSiteDetail model)
        {

            try
            {

                using (var db = new BATransferEntities())
                {
                    var relavantOrganizationReviewSite = db.OrganizationReviewSites.FirstOrDefault(orsa => orsa.OrganizationReviewSiteId == model.OrganizationReviewSiteId);

                    if (relavantOrganizationReviewSite != null)
                    {

                        relavantOrganizationReviewSite.IsPromotionalMessageEnabled = model.IsPromotionalMessageEnabled;
                        relavantOrganizationReviewSite.ApiKey = !string.IsNullOrEmpty(model.ApiKey) ? model.ApiKey : string.Empty;
                        relavantOrganizationReviewSite.BCCEmail = model.BccEmail;
                        relavantOrganizationReviewSite.IsBccEnabled = model.IsBccEnabled;
                        relavantOrganizationReviewSite.BeginDay = model.BeginDay;
                        relavantOrganizationReviewSite.EndDay = model.EndDay;
                        relavantOrganizationReviewSite.DefaultMessage = !string.IsNullOrEmpty(model.DefaultMessage) ? model.DefaultMessage : string.Empty;
                        relavantOrganizationReviewSite.PromotionalMessage = !string.IsNullOrEmpty(model.PromotionalMessage) ? model.PromotionalMessage : string.Empty;
                        relavantOrganizationReviewSite.SendingOrder = model.SendingOrder;
                        relavantOrganizationReviewSite.MaximumReviewCount = model.MaximumReviewCount;
                        relavantOrganizationReviewSite.CurrentReviewCount = model.CurrentReviewCount;
                        relavantOrganizationReviewSite.IsEnabled = model.IsEnabled;

                        db.SaveChanges();

                        return true;
                    }

                }
            }
            catch (Exception exception)
            {
                ErrorHandling.LogFileWrite(exception.Message, exception.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, exception.InnerException != null ? exception.InnerException.Message : "");
            }

            return false;
        }

    }
}