﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using System;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace SwaranAdmin_New.DataAccess
{
    public class WorldpayDal
    {
        #region Singleton
        private static WorldpayDal _instance;
        private static readonly object LockThis = new object();
        protected WorldpayDal()
        {
        }
        public static WorldpayDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new WorldpayDal());
                }
            }
        }
        #endregion


        public string PayOnlineWorldpay(string bookingToken,string userid)
        {
            try
            {
                decimal finalAmount = 0;
                using (var db = new BATransferEntities())
                {
                    if (db.Booking_TEMP.Any(x => x.BookingToken.Equals(bookingToken)))
                    {
                        decimal cardProcessingFee = OrganizationHelper.CardProcessingFeefromSite((int)BookingSiteEnum.BA);
                       
                        var bookingTemp = db.Booking_TEMP.First(x => x.BookingToken.Equals(bookingToken));

                        if (bookingTemp.Status != (int)BookingStatusEnum.Cancelled)
                        {
                            bookingTemp.PaymentMethod = (int)PaymentTypeEnum.Card;
                            bookingTemp.PaymentStatus = (int)PaymentStatusEnum.Pending;
                            if (bookingTemp.OtherCharges == 0)
                            {
                                bookingTemp.OtherCharges = cardProcessingFee;
                                bookingTemp.FinalPrice = bookingTemp.FinalPrice + cardProcessingFee;
                            }
                            db.SaveChanges();
                        }


                        var booking = db.Bookings.First(x => x.BookingToken.Equals(bookingToken));
                        if (booking.Status != (int) BookingStatusEnum.Cancelled)
                        {
                            booking.PaymentMethod = (int) PaymentTypeEnum.Card;
                            booking.PaymentStatus = (int) PaymentStatusEnum.Pending;
                            if (booking.OtherCharges == 0)
                            {
                                booking.OtherCharges = cardProcessingFee;
                                booking.FinalPrice = booking.FinalPrice + cardProcessingFee;
                            }
                            db.SaveChanges();

                            finalAmount = finalAmount+ booking.FinalPrice;
                            var bookingPaymentTemp =
                                db.BookingPayment_TEMP.First(x => x.BookingId == bookingTemp.BookingId);

                            bookingPaymentTemp.PaymentTypeId = (int) PaymentTypeEnum.Card;
                            bookingPaymentTemp.PaymentDate = DateTime.Now;
                            bookingPaymentTemp.PaymentAmount = booking.FinalPrice;
                            bookingPaymentTemp.Status = (int) PaymentStatusEnum.Pending;
                            bookingPaymentTemp.IsDeleted = false;
                            bookingPaymentTemp.IsEnabled = true;
                            bookingPaymentTemp.sysModifiedDate = DateTime.UtcNow;
                            db.SaveChanges();

                            var bookingPayment = db.BookingPayments.First(x => x.BookingId == booking.BookingId);

                            bookingPayment.PaymentTypeId = (int) PaymentTypeEnum.Card;
                            bookingPayment.PaymentDate = DateTime.Now;
                            bookingPayment.PaymentAmount = booking.FinalPrice;
                            bookingPayment.Status = (int) PaymentStatusEnum.Pending;
                            bookingPayment.IsDeleted = false;
                            bookingPayment.IsEnabled = true;
                            bookingPayment.sysModifiedDate = DateTime.UtcNow;
                            db.SaveChanges();
                        }else
                        {
                            finalAmount = finalAmount + cardProcessingFee;
                        }
                        if (bookingTemp.IsSourceBooking)
                        {
                            var returnBookingTemp =
                                db.Bookings.First(
                                    bt => bt.BookingId == bookingTemp.ReferenceBookingId && bt.IsReturnBooking);
                            returnBookingTemp.PaymentMethod = (int)PaymentTypeEnum.Card;
                            returnBookingTemp.PaymentStatus = (int)PaymentStatusEnum.Pending;
                            returnBookingTemp.OtherCharges = 0;
                            db.SaveChanges();
                            if (returnBookingTemp.Status != (int) BookingStatusEnum.Cancelled)
                            {
                                var returnBooking =
                                    db.Bookings.First(x => x.BookingId == returnBookingTemp.BookingId);
                                returnBooking.PaymentMethod = (int) PaymentTypeEnum.Card;
                                returnBooking.PaymentStatus = (int) PaymentStatusEnum.Pending;
                                returnBooking.OtherCharges = 0;
                                db.SaveChanges();

                                finalAmount = finalAmount + returnBooking.FinalPrice;
                                var returnBookingPaymentTemp =
                                    db.BookingPayment_TEMP.First(x => x.BookingId == returnBookingTemp.BookingId);

                                returnBookingPaymentTemp.PaymentTypeId = (int) PaymentTypeEnum.Card;
                                returnBookingPaymentTemp.PaymentDate = DateTime.Now;
                                returnBookingPaymentTemp.PaymentAmount = returnBookingTemp.FinalPrice;
                                returnBookingPaymentTemp.Status = (int) PaymentStatusEnum.Pending;
                                returnBookingPaymentTemp.IsDeleted = false;
                                returnBookingPaymentTemp.IsEnabled = true;
                                returnBookingPaymentTemp.sysModifiedDate = DateTime.UtcNow;
                                db.SaveChanges();

                                var returnBookingbookingPayment =
                                    db.BookingPayments.First(x => x.BookingId == returnBooking.BookingId);

                                returnBookingbookingPayment.PaymentTypeId = (int) PaymentTypeEnum.Card;
                                returnBookingbookingPayment.PaymentDate = DateTime.Now;
                                returnBookingbookingPayment.PaymentAmount = returnBookingTemp.FinalPrice;
                                returnBookingbookingPayment.Status = (int) PaymentStatusEnum.Pending;
                                returnBookingbookingPayment.IsDeleted = false;
                                returnBookingbookingPayment.IsEnabled = true;
                                returnBookingbookingPayment.sysModifiedDate = DateTime.UtcNow;
                                db.SaveChanges();
                            }
                        }
                        string description = "Booking/" + Enum.GetName(typeof(BookingSource_Enum), booking.BookingSource).ToString() + "/" + booking.BookingNumber;
                        string encodeDescription = System.Web.HttpContext.Current.Server.UrlEncode(description);
                        string encodeToken = System.Web.HttpContext.Current.Server.UrlEncode(booking.BookingToken);
                        string operatorid = System.Web.HttpContext.Current.Server.UrlEncode(userid); 
                        string path = string.Empty;

                        path = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                        //    string callBackurl = HttpContext.Current.Server.UrlEncode(path + ":44359" + "/BookingSummary/Details?tkn=" + encodeToken);
                        string callBackurl = System.Web.HttpContext.Current.Server.UrlEncode(path + "/BookingSummary/Details?tkn=" + encodeToken);//"&amnt=" + booking.TotalPrice + "
                        string paymentUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.PaymentApi) + "Payment/CapturePayment" + "?tkn=" + encodeToken + "&amnt=" + finalAmount + 
                            "&desc=" + encodeDescription + "&bnu=" + System.Web.HttpContext.Current.Server.UrlEncode(booking.BookingNumber) + "&cbu=" + callBackurl;
                        //+ "&operatorId=" + operatorid

                        return paymentUrl;
                    }


                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return string.Empty;
        }

        public string PayOnlineWorldpayManual(string bookingNumber, decimal amount, string userId)
        {
            try
            {
               
                string description = "Booking/" + bookingNumber;
                string encodeDescription = System.Web.HttpContext.Current.Server.UrlEncode(description);
                string path = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                string callBackurl = System.Web.HttpContext.Current.Server.UrlEncode(path + "/WorldPayPayment/ManualCardPayment");
                string paymentUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.PaymentApi) + "Payment/CapturePaymentManual" + "?amnt=" + amount +
                    "&desc=" + encodeDescription + "&bnu=" + System.Web.HttpContext.Current.Server.UrlEncode(bookingNumber) + "&cbu=" + callBackurl
                + "&operatorId=" + userId;

                return paymentUrl;
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return string.Empty;
        }
    }
}