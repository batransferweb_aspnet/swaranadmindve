﻿
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models.ReportMo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace SwaranAdmin_New.DataAccess.ReportDal
{
    public class BookingReportDal
    {
        #region Singalton
        private static BookingReportDal _instance;
        private static readonly object LockThis = new object();
        protected BookingReportDal()
        {
        }



        public static BookingReportDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new BookingReportDal());
                }
            }
        }


        #endregion

        public List<ReportBookingStatusViewModel> GetBookingByStatus(string startdate, string enddate)
        {
            try
            {
                DateTime sDate = DateTime.Parse(startdate);
                DateTime eDate = DateTime.Parse(enddate); 
                using (var db = new BATransferEntities())
                {
                    IEnumerable<ReportBookingStatusViewModel> objBookingStatus = db.sp_report_bookingbystatus(sDate, eDate).ToList().Select(x=> new ReportBookingStatusViewModel() {
                        Date=x.Date?? x.Date.Value,
                        Cancelled=x.Cancelled??0,
                        Confirmed=x.Confirmed??0,
                        Finished=x.Finished??0
                    });

                    return objBookingStatus.ToList();
                }

                }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }


        public List<ReportTotalCreatedBookingsViewModel> GetTotBookings(string startdate, string enddate) 
        {
            try
            {
                DateTime sDate = DateTime.Parse(startdate);
                DateTime eDate = DateTime.Parse(enddate);
                using (var db = new BATransferEntities())
                {
                    IEnumerable<ReportTotalCreatedBookingsViewModel> objBookingStatus = db.sp_swaran_report_reservation(sDate, eDate).ToList().Select(x => x.CreatedDate != null ? new ReportTotalCreatedBookingsViewModel()
                    {
                        CreatedDate = (DateTime)x.CreatedDate ,
                        FinalCount = x.FinalCount  ,
                        TotLogings = x.TotLogings ,
                        CreatedBookings = x.CreatedBookings
                    } : null);

                    return objBookingStatus.ToList();
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public ReportSiteViewModel GetBookingBySite(string startdate, string enddate)
        {
            var chart = new ReportSiteViewModel();
            try
            {
                DateTime sDate = DateTime.Parse(startdate);
                DateTime eDate = DateTime.Parse(enddate);
                var pie = new List<BookingSitePieViewModel>(); 
                using (var db = new BATransferEntities())
                {
                    IEnumerable<ReportBookingSiteViewModel> objBookingSite = db.sp_report_bookingbysite(sDate, eDate).ToList().Select(x => new ReportBookingSiteViewModel()
                    {
                        Date = x.Date ?? x.Date.Value,
                        AO = x.AO_Web ?? 0,
                        BA = x.BA_Web ?? 0,
                        BAC = x.BAC_Web ?? 0,
                        BAL = x.BAL_Web ?? 0,
                        BAT = x.BAT_Web ?? 0,
                        ET = x.ET_Web ?? 0,
                        WT = x.WT_Web ?? 0,
                        APP = x.BA_iOS??0,
                        AND = x.BA_Android??0
                    });

                    chart.ListSite = objBookingSite.ToList();
                    var objBookingSitePie = db.sp_report_bookingbysite_pie(sDate, eDate).FirstOrDefault(); 
                    if(objBookingSitePie != null)
                    {
                        var e1 = new BookingSitePieViewModel()
                        {
                            label = "AO",
                            value = objBookingSitePie.AO_Web ?? 0,
                            labelcolor = "#ff3939"
                        };
                        pie.Add(e1);
                        var e2 = new BookingSitePieViewModel()
                        {
                            label = "BA",
                            value = objBookingSitePie.BA_Web ?? 0,
                          labelcolor = "#ff3939"
                        };
                        pie.Add(e2);
                        var e3 = new BookingSitePieViewModel()
                        {
                            label = "BAC",
                            value = objBookingSitePie.BAC_Web ?? 0,
                            labelcolor = "#0003a5"
                        };
                        pie.Add(e3); 
                        var e4 = new BookingSitePieViewModel()
                        {
                            label = "BAL",
                            value = objBookingSitePie.BAL_Web ?? 0,
                            labelcolor = "#a34200"
                        };
                        pie.Add(e4);
                        var e5 = new BookingSitePieViewModel()
                        {
                            label = "BAT",
                            value = objBookingSitePie.BAT_Web ?? 0,
                            labelcolor = "#0efb7d"
                        };
                        pie.Add(e5);
                        var e6 = new BookingSitePieViewModel()
                        {
                            label = "ET",
                            value = objBookingSitePie.ET_Web ?? 0,
                            labelcolor = "#7d097e"
                        };
                        pie.Add(e6);
                        var e7 = new BookingSitePieViewModel()
                        {
                            label = "WT",
                            value = objBookingSitePie.WT_Web ?? 0,
                            labelcolor = "#006c83"
                        };
                        pie.Add(e7);
                        var e8 = new BookingSitePieViewModel()
                        {
                            label = "APP",
                            value = objBookingSitePie.BA_iOS ?? 0,
                            labelcolor = "#b901bc"
                        };
                        pie.Add(e8);

                        var e9 = new BookingSitePieViewModel()
                        {
                            label = "AND",
                            value = objBookingSitePie.BA_Android ?? 0,
                            labelcolor = "#872d00"
                        };
                        pie.Add(e9);
                    }

                    chart.AllSite= pie;
                }



            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return chart;
        }

        public ReportSiteViewModel GetBookingBySiteCreatedDate(string startdate, string enddate) 
        {
            var chart = new ReportSiteViewModel();
            try
            {
                DateTime sDate = DateTime.Parse(startdate);
                DateTime eDate = DateTime.Parse(enddate);
                var pie = new List<BookingSitePieViewModel>();
                using (var db = new BATransferEntities())
                {
                    IEnumerable<ReportBookingSiteViewModel> objBookingSite = db.sp_report_bookingbysite_datecreated(sDate, eDate).ToList().Select(x => new ReportBookingSiteViewModel()
                    {
                        Date = x.Date ?? x.Date.Value,
                        AO = x.AO_Web ?? 0,
                        BA = x.BA_Web ?? 0,
                        BAC = x.BAC_Web ?? 0,
                        BAL = x.BAL_Web ?? 0,
                        BAT = x.BAT_Web ?? 0,
                        ET = x.ET_Web ?? 0,
                        WT = x.WT_Web ?? 0,
                        APP = x.BA_iOS ?? 0,
                        AND = x.BA_Android ?? 0
                    });

                    chart.ListSite = objBookingSite.ToList();
                    var objBookingSitePie = db.sp_report_bookingbysite_pie_datecreated(sDate, eDate).FirstOrDefault();
                    if (objBookingSitePie != null)
                    {
                        var e1 = new BookingSitePieViewModel()
                        {
                            label = "AO",
                            value = objBookingSitePie.AO_Web ?? 0,
                            labelcolor = "#ff3939"
                        };
                        pie.Add(e1);
                        var e2 = new BookingSitePieViewModel()
                        {
                            label = "BA",
                            value = objBookingSitePie.BA_Web ?? 0,
                            labelcolor = "#ff3939"
                        };
                        pie.Add(e2);
                        var e3 = new BookingSitePieViewModel()
                        {
                            label = "BAC",
                            value = objBookingSitePie.BAC_Web ?? 0,
                            labelcolor = "#0003a5"
                        };
                        pie.Add(e3);
                        var e4 = new BookingSitePieViewModel()
                        {
                            label = "BAL",
                            value = objBookingSitePie.BAL_Web ?? 0,
                            labelcolor = "#a34200"
                        };
                        pie.Add(e4);
                        var e5 = new BookingSitePieViewModel()
                        {
                            label = "BAT",
                            value = objBookingSitePie.BAT_Web ?? 0,
                            labelcolor = "#0efb7d"
                        };
                        pie.Add(e5);
                        var e6 = new BookingSitePieViewModel()
                        {
                            label = "ET",
                            value = objBookingSitePie.ET_Web ?? 0,
                            labelcolor = "#7d097e"
                        };
                        pie.Add(e6);
                        var e7 = new BookingSitePieViewModel()
                        {
                            label = "WT",
                            value = objBookingSitePie.WT_Web ?? 0,
                            labelcolor = "#006c83"
                        };
                        pie.Add(e7);
                        var e8 = new BookingSitePieViewModel()
                        {
                            label = "APP",
                            value = objBookingSitePie.BA_iOS ?? 0,
                            labelcolor = "#b901bc"
                        };
                        pie.Add(e8);

                        var e9 = new BookingSitePieViewModel()
                        {
                            label = "AND",
                            value = objBookingSitePie.BA_Android ?? 0,
                            labelcolor = "#872d00"
                        };
                        pie.Add(e9);
                    }

                    chart.AllSite = pie;
                }



            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return chart;
        }
     
        public List<ReportBookingPaymenttypeViewModel> GetBookingByPaymenttype(string startdate, string enddate)
        {
            try
            {
                DateTime sDate = DateTime.Parse(startdate);
                DateTime eDate = DateTime.Parse(enddate);
                using (var db = new BATransferEntities()) 
                {
                    IEnumerable<ReportBookingPaymenttypeViewModel> objBookingPaymenttype = db.sp_report_bookingbypaymenttype(sDate, eDate).ToList().Select(x => new ReportBookingPaymenttypeViewModel ()
                    {
                        Date = x.Date ?? x.Date.Value,
                        Card = x.Card?? 0,
                        Cash = x.Pay_Cash_to_the_Driver?? 0,
                        FreeJourney = x.Free_Journey ?? 0,
                        UseCredits = x.Use_My_Credits ?? 0,
                    });

                    return objBookingPaymenttype.ToList();
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public ReportPlotPieViewModel GetBookingByPlot(string startdate, string enddate)  
        {
            var report = new ReportPlotPieViewModel();
            try
            {
               
                DateTime sDate = DateTime.Parse(startdate);
                DateTime eDate = DateTime.Parse(enddate);

                using (var db = new BATransferEntities())
                {
                    IEnumerable<ReportPlotPieMore50ViewModel> objPlotMore50 = db.sp_report_bookingbyPlot_pie_50_more(sDate, eDate).ToList().Select(x => new ReportPlotPieMore50ViewModel()
                    {
                        label = x.postcode,
                        value = x.count ?? 0 
                    });
                    report.PlotMore50 = objPlotMore50.ToList();

                    IEnumerable<ReportPlotPieLess50ViewModel> objPlotLess50 = db.sp_report_bookingbyPlot_pie_50_20less(sDate, eDate).ToList().Select(x => new ReportPlotPieLess50ViewModel() 
                    {
                        label = x.Plot,
                        value = x.Count ?? 0
                    });
                    report.PlotLess50 = objPlotLess50.ToList();

                    IEnumerable<ReportPlotPieLess20ViewModel> objPlotLess20 = db.sp_report_bookingbyPlot_pie_less20(sDate, eDate).ToList().Select(x => new ReportPlotPieLess20ViewModel()
                    {
                        label = x.postcode,
                        value = x.count ?? 0
                    });
                    report.PlotLess20 = objPlotLess20.ToList();




                    IEnumerable<ReportPlotPieDropoffViewModel> objPlotMore50Dropoff = db.sp_report_bookingbyPlot_pie_50_more_Dropoff(sDate, eDate).ToList().Select(x => new ReportPlotPieDropoffViewModel()
                    {
                        label = x.postcode,
                        value = x.count ?? 0
                    });
                    report.PlotMore50Dropoff = objPlotMore50Dropoff.ToList();

                    IEnumerable<ReportPlotPieDropoffViewModel> objPlotLess50Dropoff = db.sp_report_bookingbyPlot_pie_50_20less_Dropoff(sDate, eDate).ToList().Select(x => new ReportPlotPieDropoffViewModel()
                    {
                        label = x.Plot,
                        value = x.Count ?? 0
                    });
                    report.PlotLess50Dropoff = objPlotLess50Dropoff.ToList();

                    IEnumerable<ReportPlotPieDropoffViewModel> objPlotLess20Dropoff = db.sp_report_bookingbyPlot_pie_less20_Dropoff(sDate, eDate).ToList().Select(x => new ReportPlotPieDropoffViewModel()
                    {
                        label = x.postcode,
                        value = x.count ?? 0
                    });
                    report.PlotLess20Dropoff = objPlotLess20Dropoff.ToList();




                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return report;
        }

        public List<ReportCustomerBookingSearchViewModel> GetCustomerBookingSearchese(string startdate, string enddate)
        {
            try
            {
                DateTime sDate = DateTime.Parse(startdate);
                DateTime eDate = DateTime.Parse(enddate);

                using (var db = new BATransferEntities())
                {
                    IEnumerable<ReportCustomerBookingSearchViewModel> objBookingSearches = db.sp_report_booking_search(sDate, eDate).ToList().Select(x => new ReportCustomerBookingSearchViewModel() 
                    {
                        FromPlot = x.FromPlot+"  ----  " + x.ToPlot,
                        ToPlot = x.ToPlot,
                        BookingCount = x.SearchCount ?? 0
                    });

                    return objBookingSearches.ToList();
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }
    }
}