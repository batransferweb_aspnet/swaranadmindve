﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SwaranAdmin_New.DataAccess
{
    public class DispatchDal
    {
        #region Singalton
        private static DispatchDal _instance;
        private static readonly object LockThis = new object();
        protected DispatchDal()
        {
        }
        public static DispatchDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new DispatchDal());
                }
            }
        }
        #endregion

        public async Task<List<DispathchViewModel>> GetRecentBookings(DispathchSearchViewModel dispathch)
        {

            string bookingDate = dispathch.date;// + "/" + dispathch.month + "/" + dispathch.year;
            try
            {
                int timeLeftId = 0;
                DateTime? serchDate = null;
                if (!string.IsNullOrEmpty(bookingDate) && bookingDate.Split('/')[2] != "0000")
                {
                    serchDate = ValueParseHelper.TryParseDate(bookingDate);
                }
                using (var db = new BATransferEntities())
                {// List<GetRequestedBookings_Result1>
                    var result =
                        db.GetRequestedBookings(dispathch.paymentStatus, serchDate, dispathch.bookingStatus, 0, 0, dispathch.siteId).ToList();
                    IEnumerable<DispathchViewModel> objRecentBooking = result.ToList().Select(x => new DispathchViewModel()
                    {
                        BookingToken = System.Web.HttpContext.Current.Server.UrlEncode(x.BookingToken),
                        BookingId = x.BookingId ?? 0,
                        Loyalty = x.Loyalty,
                        Name = x.Name,
                        BookingStatus = x.Status,
                        BookingSource = x.Source,
                        BookingNumber = x.BookingNumber,
                        BookingDateTime = x.BookingDateTime.ToString(),
                        TimeLeft = CovertLocalTime(x.BookingDateTime ?? DateTime.Now, dispathch.timeZoneInfo, out timeLeftId),
                        TimeLeftId = timeLeftId,
                        BookingTimeSpan = CoverttimeSpan(x.BookingDateTime ?? DateTime.Now, dispathch.timeZoneInfo).ToString(),
                        Journeys = x.Journeys,
                        PaymentType = x.PaymentType,
                        Country = x.Country
                    });

                    return objRecentBooking.ToList();

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");


            }
            return null;
        }
        private TimeSpan CoverttimeSpan(DateTime bookingDate, string timeZoneInfo)
        {
            TimeSpan timeSpan = new TimeSpan();
            try
            {
                //TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo);
                // var currentDateTime = TimeZoneInfo.ConvertTime(DateTime.Now, tz);

                double totalMinutes = bookingDate.Subtract(DateTime.Now).TotalMinutes;

                timeSpan = TimeSpan.FromMinutes(totalMinutes);
                return timeSpan;


            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return timeSpan;
        }
        private string CovertLocalTime(DateTime bookingDate, string timeZoneInfo, out int timeLeftId)
        {
            timeLeftId = 0;
            try
            {
                // TimeZoneInfo tz = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo);
                //   var currentDateTime = TimeZoneInfo.ConvertTime(DateTime.Now, tz);

                double totalMinutes = bookingDate.Subtract(DateTime.Now).TotalMinutes;

                TimeSpan timeSpan = TimeSpan.FromMinutes(totalMinutes);

                var days = timeSpan.Days;
                var hours = timeSpan.Hours;
                var minutes = timeSpan.Minutes;
                if (days > 0)
                {
                    timeLeftId = 4;
                    return String.Format("{0} Days {1} Hours {2} Minutes", days, hours, minutes);
                }
                if (hours > 0)
                {
                    timeLeftId = hours <= 3
                        ? 1
                        : hours > 3 && hours < 6
                        ? 2
                        : hours >= 6 && hours < 12
                        ? 3
                        : hours >= 12
                        ? 4
                        : 0;
                    return String.Format("{0} Hours {1} Minutes", hours, minutes);
                }
                if (minutes > 0)
                {
                    timeLeftId = 1;
                    return String.Format("{0} Minutes", minutes);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return "";
        }


        public PaymentViewModel GetPaymentTypes(string userId)
        {
            var payment = new PaymentViewModel();
            bool isFreeJourneyAvailable = false;
            try
            {
                using (var db = new BATransferEntities())
                {

                    if (db.AspNetUsers.Any(anu => anu.Id.Equals(userId)))
                    {
                        var relevantAspNetUser = db.AspNetUsers.First(anu => anu.Id.Equals(userId));
                        if (relevantAspNetUser.Customers != null && relevantAspNetUser.Customers.Any())
                        {
                            isFreeJourneyAvailable = relevantAspNetUser.Customers.Any() &&
                                                     (relevantAspNetUser.Customers.First().isFreeJourneyAvailable &&
                                                      relevantAspNetUser.Customers.First().numberOfFreeJourneys > 0);
                        }
                        if (db.PaymentTypes.Any(pt => pt.IsEnabled))
                        {
                            List<PaymentTypeDom> allActivePaymentTypes = new List<PaymentTypeDom>();
                            if (relevantAspNetUser.IsCompanyUser)
                            {
                                allActivePaymentTypes =
                                    db.PaymentTypes.Where(pt => pt.IsEnabled)// && pt.IsEnabledForCompany)
                                        .Select(ptt => new PaymentTypeDom()
                                        {
                                            PaymentTypeId = ptt.PaymentTypeId,
                                            PaymentTypeName = ptt.PaymentTypeName
                                        }).ToList();
                            }
                            else
                            {
                                allActivePaymentTypes =
                                    db.PaymentTypes.Where(pt => pt.IsEnabled).Select(ptt => new PaymentTypeDom()
                                    {
                                        PaymentTypeId = ptt.PaymentTypeId,
                                        PaymentTypeName = ptt.PaymentTypeName
                                    }).ToList();
                            }
                            if (allActivePaymentTypes.Any())
                            {
                                List<PaymentTypeDom> allOtherPaymentTypes =
                                    allActivePaymentTypes.Where(
                                        pt => pt.PaymentTypeName.Trim() != PaymentTypeEnum.Free_Journey.ToString())
                                        .Select(ptt => new PaymentTypeDom()
                                        {
                                            PaymentTypeId = ptt.PaymentTypeId,
                                            PaymentTypeName = ptt.PaymentTypeName
                                        }).ToList();
                                if (!isFreeJourneyAvailable)
                                {
                                    if (
                                        allOtherPaymentTypes.Any(
                                            x => x.PaymentTypeId == (int)PaymentTypeEnum.Free_Journey))
                                    {
                                        var objIndex =
                                            allOtherPaymentTypes.Single(
                                                x => x.PaymentTypeId == (int)PaymentTypeEnum.Free_Journey);
                                        allOtherPaymentTypes.Remove(objIndex);
                                    }
                                }
                                payment.AllOtherPaymentTypes = allOtherPaymentTypes;
                            //    payment.PaymentTypeCard =
                            //        allActivePaymentTypes.Any(
                            //            pt => pt.PaymentTypeName == PaymentTypeEnum.Card.ToString())
                            //            ? new PaymentTypeCardDom()
                            //            {
                            //                PaymentTypeId =
                            //                    allActivePaymentTypes.First(
                            //                        pt => pt.PaymentTypeName == PaymentTypeEnum.Card.ToString())
                            //                        .PaymentTypeId,
                            //                PaymentTypeName = PaymentTypeEnum.Card.ToString(),
                            //                CardTypes =
                            //                    db.CardTypes.Where(cpm => !cpm.IsDelete)
                            //                        .Select(cpmm => new CardTypeDom()
                            //                        {
                            //                            CardId = cpmm.CardId,
                            //                            Name = cpmm.Name,
                            //                            ChargeType = cpmm.ChargeType,
                            //                            Value = cpmm.Value,
                            //                            ImageUrl = cpmm.ImageUrl
                            //                        }).ToList(),
                            //                CardPaymentTypes =
                            //                    db.CardPaymentTypes.Where(cpt => cpt.IsEnabled && !cpt.IsDeleted)
                            //                        .Select(cptt => new CardPaymentTypeDom()
                            //                        {
                            //                            CardPaymentTypeId = cptt.CardPaymentTypeId,
                            //                            CardPaymentTypeName = cptt.CardPaymentTypeName
                            //                        }).ToList()
                            //            }
                            //            : new PaymentTypeCardDom();
                            };
                       }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return payment;
        }




        public CurrentBookingViewModel GetCurrentBooking(string bookingToken)
        {
            var currentBooking = new CurrentBookingViewModel();

            try
            {
                using (var db = new BATransferEntities())
                {
                    var objBooking = db.Bookings.First(x => x.BookingToken.Equals(bookingToken));
                    List<GetBookingAdminView_Result> result = db.GetBookingAdminView(bookingToken, false).ToList();
                    if (result != null && result.Any())
                    {
                        var objResult = result.First();
                        string tokent = System.Web.HttpContext.Current.Server.UrlEncode(bookingToken);
                        currentBooking.BookingToken = tokent;
                        currentBooking.BookingStatus = Enum.GetName(typeof(BookingStatusEnum), objResult.Status);
                        currentBooking.BookingNumber = objResult.BookingNumber;
                      //  currentBooking.BookingDate = ValueParseHelper.ToStringDate(objResult.BookingDateTime);
                        currentBooking.TotalAmount = objResult.TotalDistancePrice;
                        currentBooking.OtherCharges = objResult.OtherCharges;
                        currentBooking.TotalPrice = objResult.TotalPrice;
                        currentBooking.BookedPersonName = objResult.BookedPersonName;
                        currentBooking.PrimaryPhoneNumber = objResult.PrimaryPhoneNumber;
                        currentBooking.AlternativeMobile = objResult.AlternativePhoneNumber;
                        //currentBooking.MemberId = objResult.MemberId;
                        currentBooking.Loyalty = objResult.LoyaltyMemberTypeName;
                        currentBooking.PassengerName = objResult.PassengerName;
                        currentBooking.PassengerPhoneNumber = objResult.PassengerTel;
                        currentBooking.VehicleType = objResult.VehicleTypes;
                        currentBooking.BookedPersonEmail = objResult.Email;
                        currentBooking.CustomerComment = objResult.CustomerComment;
                        currentBooking.ReceiptNumber = objResult.ReceiptNumber;
                        currentBooking.BookingFrom = objResult.BookingFrom;
                        currentBooking.BookingTo = objResult.BookingTo;
                        currentBooking.DriverEllapseTime = objResult.DriverEllapseTime;
                        currentBooking.FlightNo = objResult.FlightNo;
                        currentBooking.DriverDisplayName = objResult.DriverDisplayName;
                        currentBooking.NumOfPassengers = objResult.NumberOfPassengers;
                        // currentBooking.ChildSeat = objResult.ChildSeat;
                        currentBooking.NumberOfLuggages = objResult.Luggage;
                        currentBooking.NumberOfHandLuggages = objResult.HandLuggage;
                        if (objResult.IsSourceBooking)
                        {
                            currentBooking.IsReturnBooking = true;
                            currentBooking.ReferenceBookingId = objResult.ReferenceBookingId;
                        }
                        else
                        {
                            currentBooking.IsReturnBooking = false;
                        }
                        currentBooking.ModifiedDate = objResult.ModifiedDate.ToString();
                        currentBooking.Country = objResult.Country;
                        currentBooking.IpAddress = objResult.IpAddress;
                        currentBooking.CreatedDate = objResult.CreatedDate.ToString();
                        currentBooking.FromAriaPointTypeId = objResult.FromAriaPointTypeId;
                        currentBooking.ToAriaPointTypeId = objResult.ToAriaPointTypeId;
                        //  currentBooking.JourneyType = Enum.GetName(typeof(JourneyTypeEnum), objResult.ty;

                        currentBooking.PaymentStatus = Enum.GetName(typeof(PaymentStatusEnum),
                            objBooking.BookingPayments.First(x => x.BookingId == objBooking.BookingId).Status);


                        currentBooking.CardPaymentType = objBooking.CardPaymentTypeId != null
                          ? Enum.GetName(typeof(CardPaymentTypeEnum), objBooking.CardPaymentTypeId)
                          : "";
                        currentBooking.PaymentMethod = objBooking.PaymentType != null
                           ? Enum.GetName(typeof(PaymentTypeEnum), objResult.PaymentMethod)
                           : "";
                    }
                    if (currentBooking.IsReturnBooking)
                    {
                        List<GetBookingAdminView_Result> resultReturnJourney =
                            db.GetBookingAdminView(bookingToken, true).ToList();
                        if (resultReturnJourney != null && resultReturnJourney.Any())
                        {
                            var objresultReturnJourney = resultReturnJourney.First();
                           // currentBooking.BookingDateTimeReturn = ValueParseHelper.ToStringDateTime(objresultReturnJourney.BookingDateTime);
                            currentBooking.BookingFromReturn = objresultReturnJourney.BookingFrom;
                            currentBooking.BookingToReturn = objresultReturnJourney.BookingTo;
                            currentBooking.DriverEllapseTimeReturn = objresultReturnJourney.DriverEllapseTime;
                            currentBooking.FlightNoReturn = objresultReturnJourney.FlightNo;
                            currentBooking.DriverDisplayNameReturn = objresultReturnJourney.DriverDisplayName;
                            currentBooking.TotalDistancePriceReturn = objresultReturnJourney.TotalDistancePrice;
                            currentBooking.FromAriaPointTypeIdReturn = objresultReturnJourney.FromAriaPointTypeId;
                            currentBooking.ToAriaPointTypeIdReturn = objresultReturnJourney.ToAriaPointTypeId;
                        }
                    }

                    return currentBooking;
                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return currentBooking;
        }
    }
}