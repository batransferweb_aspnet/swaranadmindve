﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
namespace SwaranAdmin_New.DataAccess
{
    public class RegisterDal
    {
        #region Singleton
        private static RegisterDal _instance;
        private static readonly object LockThis = new object();
        protected RegisterDal()
        {
        }
        public static RegisterDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new RegisterDal());
                }
            }
        }
        #endregion
        public bool CheckIsDeleted(string email)
        {
            return false;
        }
        public List<TitleViewModel> LoadTitles()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<TitleViewModel> objTitle =
                        db.Titles.Where(x => x.IsDeleted == false && x.IsEnabled == true)
                            .Select(x => new TitleViewModel()
                            {
                                TitleId = x.TitleId,
                                TitleName = x.TitleName
                            });
                    return objTitle.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }
        public List<CountryInfoViewModel> LoadCountrys()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<CountryInfoViewModel> objCounties =
                        db.Countries.ToList().OrderBy(x => x.Name).Select(x => new CountryInfoViewModel()
                        {
                            CountryId = x.ID,
                            CountryName = x.Name,
                            CountryCode = x.Iso,
                            PhoneDisplayCode = x.Name + " (" + x.Phonecode.ToString() + ")",
                            PhoneCode = x.Phonecode.ToString()
                        });
                    return objCounties.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }
        public bool IsVerified(EmailTokenViewModel token, out string message)
        {
            message = string.Empty;
            try
            {
                using (var db = new BATransferEntities())
                {
                    int emailVerificationTokenValidityMinutes = 0;
                    if (
                        db.Customer_Configuration.Any(
                            cc =>
                                cc.customerConfigurationId ==
                                (int)CustomerConfigurationEnum.emailVerificationTokenValidityMinutes))
                    {
                        var relevantNewConfiguration =
                            db.Customer_Configuration.First(
                                cc =>
                                    cc.customerConfigurationId ==
                                    (int)CustomerConfigurationEnum.emailVerificationTokenValidityMinutes);
                        int.TryParse(relevantNewConfiguration.factor, out emailVerificationTokenValidityMinutes);
                    }
                    if (db.AspNetUsers.Any(x => x.Id.Equals(token.UserId)))
                    {
                        var objToken = db.AspNetUsers.First(x => x.Id.Equals(token.UserId));
                        if (objToken.EmailVerificationToken.Equals(token.CurrentEmailToken.Trim()))
                        {
                            if ((DateTime.UtcNow - objToken.EmailVerificationTokenCreatedDate).TotalMinutes <
                                emailVerificationTokenValidityMinutes)
                            {
                                objToken.IsEmailVerified = true;
                                //objToken.isAuthorized = true;
                                objToken.IsStaffAccountVerified = true;
                                db.SaveChanges();
                                message = "email verified successfully";
                                return true;
                            }
                            else
                            {
                                message = "Token expired ";
                                return false;
                            }
                        }
                        else
                        {
                            message = "Invalid token";
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }
        public async Task<bool> ResendEmailToket(string userId)
        {
            string emailToket = string.Empty;
            string email = string.Empty;
            try
            {
                using (var context = new BATransferEntities())
                {
                    if (context.AspNetUsers.Any(x => x.Id == userId))
                    {
                        var objUser = context.AspNetUsers.First(x => x.Id == userId);
                        email = objUser.Email;
                        emailToket = Security.EncryptToken(email + objUser.PrimaryPhoneNumber,
                            DateTime.UtcNow.ToString());
                        objUser.EmailVerificationToken = emailToket;
                        objUser.EmailVerificationTokenCreatedDate = DateTime.UtcNow;
                        context.SaveChanges();
                    }
                }
                if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(emailToket))
                {
                    string urlCustomerFileApi = ApiUrlHelper.GetUrl((int)ApiUrlEnum.UserEmailVerificationBaseUrl);
                    string url = urlCustomerFileApi + System.Web.HttpContext.Current.Server.UrlEncode(emailToket);
                    string logFilePath =
                        System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/RegistorToken.txt"));
                    string text = System.IO.File.ReadAllText(logFilePath);
                    text = text.Replace("TOKENSRI", emailToket);
                    text = text.Replace("MAINURL", url);
                    EmailCredentialsViewModel emailCredentials = EmailHelper.GetCustomerEmaillCredentials();
                    var listEmails = new List<string>() { email };
                    var emailRoot = new RootEmail();
                    emailRoot.Attachments = null;
                    emailRoot.Body = text;
                    emailRoot.FromEmailAddress = emailCredentials.FromEmail;
                    emailRoot.Username = emailCredentials.Username;
                    emailRoot.Password = emailCredentials.Password;
                    emailRoot.Port = emailCredentials.Port;
                    emailRoot.Smtp = emailCredentials.Smtp;
                    emailRoot.Subject = "BaTransfer Admin Email Verification";
                    emailRoot.ToEmailAddresses = listEmails;
                    emailRoot.DisplayName = emailCredentials.DisplayName;
                    string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.PasswordResetBaseUrl);
                    await EmailHelper.SendEmailWithoutAttachtment(emailRoot, apiUrl);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }
        public async Task<EmailVerificationViewModel> CreateEmailToket(string email)
        {
            var userVerify = new EmailVerificationViewModel();
            string emailToket = string.Empty;
            try
            {
                using (var context = new BATransferEntities())
                {
                    if (context.AspNetUsers.Any(x => x.Email == email))
                    {
                        var objUser = context.AspNetUsers.First(x => x.Email == email);
                        emailToket = Security.EncryptToken(objUser.Email + objUser.PrimaryPhoneNumber,
                          DateTime.UtcNow.ToString());
                        objUser.EmailVerificationToken = emailToket;
                        objUser.EmailVerificationTokenCreatedDate = DateTime.UtcNow;
                        context.SaveChanges();
                        userVerify.UserId = objUser.Id;
                    }
                    else
                    {
                        userVerify.UserId = "";
                        userVerify.IsSuccess = false;
                        userVerify.Message = "Invalid email address";
                        return userVerify;
                    }
                }
                if (!string.IsNullOrEmpty(emailToket))
                {
                    string urlCustomerFileApi = ApiUrlHelper.GetUrl((int)ApiUrlEnum.UserEmailVerificationBaseUrl);
                    string url = urlCustomerFileApi + System.Web.HttpContext.Current.Server.UrlEncode(emailToket);
                    string logFilePath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/RegistorToken.txt"));
                    string text = System.IO.File.ReadAllText(logFilePath);
                    text = text.Replace("TOKENSRI", emailToket);
                    text = text.Replace("MAINURL", url);
                    EmailCredentialsViewModel emailCredentials = EmailHelper.GetCustomerEmaillCredentials();
                    var listEmails = new List<string>() { email };
                    var emailRoot = new RootEmail();
                    emailRoot.Attachments = null;
                    emailRoot.Body = text;
                    emailRoot.FromEmailAddress = emailCredentials.FromEmail;
                    emailRoot.Username = emailCredentials.Username;
                    emailRoot.Password = emailCredentials.Password;
                    emailRoot.Port = emailCredentials.Port;
                    emailRoot.Smtp = emailCredentials.Smtp;
                    emailRoot.Subject = "BaTransfer Email Verification";
                    emailRoot.ToEmailAddresses = listEmails;
                    emailRoot.IsEnableSsl = emailCredentials.IsEnabledSSL;
                    string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                    userVerify.IsSuccess = await EmailHelper.SendEmailWithoutAttachtment(emailRoot, apiUrl);
                    userVerify.Message = "Email successfully sent";
                }
            }
            catch (Exception ex)
            {
                userVerify.IsSuccess = false;
                userVerify.Message = "Error";
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return userVerify;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        public async Task<bool> CreateUserProfile(string id, RegisterViewModel model)
        {

            bool response = false;

            try
            {
                using (var db = new BATransferEntities())
                {
                    var objDefault = db.DefaultCustomerProfiles.FirstOrDefault();

                    if (objDefault != null)
                    {
                        var staff = new StaffProfile();
                        staff.UserId = id;
                        staff.StaffTypeId = null;
                        staff.IsPasswordResetRequired = false;
                        staff.RetryCount = 0;
                        staff.IsBlocked = false;
                        staff.ImageUrl = string.Empty;
                        staff.LastLoginDate = null;
                        staff.DeletedDate = null;
                        //staff.IsEnabled = true;
                        staff.IsEnabled = false;
                        staff.IsDeleted = false;
                        staff.sysCreatedDate = DateTime.UtcNow;
                        staff.sysModifiedDate = DateTime.UtcNow;
                        staff.postalCode = string.Empty;
                        staff.street = string.Empty;
                        staff.fax = string.Empty;
                        staff.town = string.Empty;
                        db.StaffProfiles.Add(staff);
                        db.SaveChanges();

                        response = true;

                        #region Comments

                        //var customer = new Customer();
                        //customer.userId = id;
                        //customer.postalCode = objDefault.postalCode;
                        //customer.street = objDefault.street;
                        //customer.town = objDefault.town;
                        //customer.countryID = model.CountryId;
                        //customer.fax = objDefault.fax;
                        //customer.isEnabled = objDefault.isEnabled;
                        //customer.isDeleted = objDefault.isEnabled;
                        //customer.totalJourneyTime = objDefault.totalJourneyTime;
                        //customer.totalJourneyDistance = objDefault.totalJourneyDistance;
                        //customer.totalPoints = objDefault.totalPoints;
                        //customer.totalNumberOfJourneys = objDefault.totalNumberOfJourneys;
                        //customer.availablePoints = objDefault.availablePoints;
                        //customer.availableCashBalance = objDefault.availableCashBalance;
                        //customer.availableDistance = objDefault.availableDistance;
                        //customer.availableTime = objDefault.availableTime;
                        //customer.promoCode = objDefault.promoCode;
                        //customer.ImageUrl = string.Empty;
                        //customer.isFreeJourneyAvailable = objDefault.isFreeJourneyAvailable;
                        //customer.numberOfFreeJourneys = (int)objDefault.totalNumberOfJourneys;
                        //customer.loyaltyMemberType = objDefault.loyaltyMemberType;
                        //customer.isSubscriptionEnabled = objDefault.isSubscriptionEnabled;
                        //customer.isMarketingEmailEnabled = objDefault.isMarketingEmailEnabled;
                        //customer.sysCreatedDate = DateTime.UtcNow;
                        //customer.sysModifiedDate = DateTime.UtcNow;
                        //customer.AnniversaryDate = DateTime.UtcNow.AddYears(1);
                        //db.Customers.Add(customer);
                        //db.SaveChanges();

                        #endregion

                    }
                    else
                    {
                        var staff = new StaffProfile();
                        staff.UserId = id;
                        staff.StaffTypeId = null;
                        staff.IsPasswordResetRequired = false;
                        staff.RetryCount = 0;
                        staff.IsBlocked = false;
                        staff.ImageUrl = string.Empty;
                        staff.LastLoginDate = null;
                        staff.DeletedDate = null;
                        //staff.IsEnabled = true;
                        staff.IsEnabled = false;
                        staff.IsDeleted = false;
                        staff.sysCreatedDate = DateTime.UtcNow;
                        staff.sysModifiedDate = DateTime.UtcNow;
                        staff.postalCode = string.Empty;
                        staff.street = string.Empty;
                        staff.fax = string.Empty;
                        staff.town = string.Empty;
                        db.StaffProfiles.Add(staff);
                        db.SaveChanges();

                        response = true;

                        #region Comments

                        //var customer = new Customer();
                        //customer.userId = id;
                        //customer.postalCode = "";
                        //customer.street = "";
                        //customer.town = "";
                        //customer.countryID = model.CountryId;
                        //customer.fax = "";
                        //customer.isEnabled = true;
                        //customer.isDeleted = false;
                        //customer.totalJourneyTime = 0;
                        //customer.totalJourneyDistance = 0;
                        //customer.totalPoints = 0;
                        //customer.totalNumberOfJourneys = 0;
                        //customer.availablePoints = 0;
                        //customer.availableCashBalance = 0;
                        //customer.availableDistance = 0;
                        //customer.availableTime = 0;
                        //customer.promoCode = "";
                        //customer.ImageUrl = string.Empty;
                        //customer.isFreeJourneyAvailable = false;
                        //customer.numberOfFreeJourneys = 0;
                        //customer.loyaltyMemberType = (int)LoyaltyType_Enum.New;
                        //customer.isSubscriptionEnabled = false;
                        //customer.isMarketingEmailEnabled = true;
                        //customer.sysCreatedDate = DateTime.UtcNow;
                        //customer.sysModifiedDate = DateTime.UtcNow;
                        //customer.AnniversaryDate = DateTime.UtcNow.AddYears(1);
                        //db.Customers.Add(customer);
                        //db.SaveChanges();

                        #endregion
                    }

                    #region Comments

                    //if (db.Organizations.Any())
                    //{
                    //    var objOrg = db.Organizations.Select(x => x);
                    //    foreach (var orgItem in objOrg.ToList())
                    //    {
                    //        if (orgItem.OrganizationId != (int)BookingSource_Enum.Unknown)
                    //        {
                    //            CustomerSite customerSite = new CustomerSite();
                    //            customerSite.UserId = id;
                    //            customerSite.OrganizationId = orgItem.OrganizationId;
                    //            if (customerSite.OrganizationId == (int)BookingSiteEnum.ET)
                    //            {
                    //                customerSite.RegisteredSource = (int)BookingSource_Enum.ET_Web;
                    //            }
                    //            else
                    //            {
                    //                customerSite.RegisteredSource = (int)BookingSource_Enum.Unknown;
                    //            }
                    //            customerSite.IsEnable = true;
                    //            customerSite.IsDeleted = false;
                    //            customerSite.CreatedDate = DateTime.UtcNow;
                    //            customerSite.ModifiedDate = DateTime.UtcNow;
                    //            db.CustomerSites.Add(customerSite);
                    //            db.SaveChanges();
                    //        }
                    //    }
                    //}

                    #endregion
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return response;
        }

        #region Get Account Info
        public long GenerateMemberId(string firstName)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var member = new MemberIdGenerator();
                    member.UserId = firstName;
                    member.ModifiedDate = DateTime.UtcNow;
                    member.CreatedDate = DateTime.UtcNow;
                    db.MemberIdGenerators.Add(member);
                    db.SaveChanges();
                    return member.MemberId;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return 0;
        }
        public bool IsMobileNumberExsist(string mobileNum)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    return db.AspNetUsers.Any(x => x.PrimaryPhoneNumber.Equals(mobileNum));
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }
        #endregion

        #region Save Member Login
        public void SaveMemmerLogin(ApplicationUser user)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var customers = db.Customers.First(x => x.userId.Equals(user.Id));
                    customers.LastLoggedInDateTime = DateTime.UtcNow;
                    db.SaveChanges();
                    var memberLogin = new MemberLogin();
                    memberLogin.userId = user.Id;
                    memberLogin.email = user.Email;
                    memberLogin.loginToken = Security.EncryptToken(user.Id + DateTime.UtcNow.ToString(), DateTime.UtcNow.ToString()); ;
                    memberLogin.deviceId = BookingSiteEnum.ET.ToString();
                    memberLogin.status = 1;
                    memberLogin.deviceType = System.Web.HttpContext.Current.Request.Browser.MobileDeviceModel ?? "";
                    memberLogin.deviceOsType = System.Web.HttpContext.Current.Request.Browser.Browser ?? "";
                    memberLogin.deviceOsVersion = System.Web.HttpContext.Current.Request.Browser.Version ?? "";
                    memberLogin.notificationId = "";
                    memberLogin.deviceIpAddress = System.Web.HttpContext.Current.Request.UserHostAddress ?? "";
                    memberLogin.devicePublicIpAddress = System.Web.HttpContext.Current.Request.UserHostAddress ?? "";
                    memberLogin.sysModifiedDate = DateTime.UtcNow;
                    memberLogin.sysCreatedDate = DateTime.UtcNow;
                    db.MemberLogins.Add(memberLogin);
                    db.SaveChanges();
                    var memberLoginHistory = new MemberLoginHistory();
                    memberLoginHistory.loginId = memberLogin.loginId;
                    memberLoginHistory.userId = user.Id;
                    memberLoginHistory.email = user.Email;
                    memberLoginHistory.loginToken = memberLogin.loginToken;
                    memberLoginHistory.deviceId = BookingSiteEnum.ET.ToString();
                    memberLoginHistory.status = 1;
                    memberLoginHistory.deviceType = System.Web.HttpContext.Current.Request.Browser.MobileDeviceModel ?? "";
                    memberLoginHistory.deviceOsType = System.Web.HttpContext.Current.Request.Browser.Browser ?? "";
                    memberLoginHistory.deviceOsVersion = System.Web.HttpContext.Current.Request.Browser.Version ?? "";
                    memberLoginHistory.notificationId = "";
                    memberLoginHistory.deviceIpAddress = System.Web.HttpContext.Current.Request.UserHostAddress ?? "";
                    memberLoginHistory.devicePublicIpAddress = System.Web.HttpContext.Current.Request.UserHostAddress ?? "";
                    memberLoginHistory.sysModifiedDate = DateTime.UtcNow;
                    memberLoginHistory.sysCreatedDate = DateTime.UtcNow;
                    db.MemberLoginHistories.Add(memberLoginHistory);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }
        #endregion
        #region Save Member Logout
        public void SaveMemmerLogout(ApplicationUser user)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.MemberLogins.Any(x => x.userId.Equals(user.Id)))
                    {
                        var objMember = db.MemberLogins.First(x => x.userId.Equals(user.Id));
                        db.MemberLogins.Remove(objMember);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }
        #endregion

        #region Genarate Email Token
        public string GenerateEmailToken(string id)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.AspNetUsers.Any(x => x.Id == id))
                    {
                        var objUser = db.AspNetUsers.First(x => x.Id == id);
                        string emailToket = Security.EncryptToken(objUser.Email + objUser.PrimaryPhoneNumber,
                              DateTime.UtcNow.ToString());
                        objUser.EmailVerificationToken = emailToket;
                        objUser.EmailVerificationTokenCreatedDate = DateTime.UtcNow;
                        db.SaveChanges();
                        return emailToket;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return "";
        }
        #endregion
        #region validate Email Token
        public EmailTokentStatus ChckIsValidEmailToken(string tokent)
        {
            var tokentStatus = new EmailTokentStatus();
            try
            {
                using (var context = new BATransferEntities())
                {
                    if (context.AspNetUsers.Any(x => x.EmailVerificationToken == tokent))
                    {
                        var objUser = context.AspNetUsers.First(x => x.EmailVerificationToken == tokent);
                        if (DateTime.UtcNow < objUser.EmailVerificationTokenCreatedDate.AddHours(ConfigHelper.GetTokentValidityPeriod((int)TokentValidityPeriod_Enum.PasswordResetTokenValidityHours)))
                        {
                            tokentStatus.IsSuccess = true;
                            tokentStatus.UserId = objUser.Id;
                            tokentStatus.Message = "";
                            return tokentStatus;
                        }
                        else
                        {
                            tokentStatus.Message = "Token expired ";
                            tokentStatus.IsSuccess = false;
                            tokentStatus.UserId = "";
                            return tokentStatus;
                        }
                    }
                    else
                    {
                        tokentStatus.Message = "Invalid Token";
                        tokentStatus.IsSuccess = false;
                        tokentStatus.UserId = "";
                        return tokentStatus;
                    }
                }
            }
            catch (Exception ex)
            {
                tokentStatus.Message = "Error";
                tokentStatus.IsSuccess = false;
                tokentStatus.UserId = "";
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return tokentStatus;
        }
        #endregion
        #region Forget Password
        public async Task<bool> SendForgotPassword(string email, string emailtoken, string userName, string tempPassword)
        {
            bool IsSend = false;
            try
            {
                string path = string.Empty;
                Uri uriResult;
                if (Uri.TryCreate(HttpContext.Current.Request.Url.Host, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps))
                {
                    path = uriResult.AbsoluteUri;
                }
                else
                {
                    //  path = "https://" + HttpContext.Current.Request.Url.Host + ":44359";
                    path = "https://" + HttpContext.Current.Request.Url.Host;
                }
                string encodemailtoken = HttpContext.Current.Server.UrlEncode(emailtoken);
                string callBackurl = path + "/NewPassword/ForgotPasswordReset?token=" + encodemailtoken;// HttpContext.Current.Server.UrlEncode();
                string logFilePathTForgotPassword = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ForgotPassword.txt"));
                string forgotPasswordtext = System.IO.File.ReadAllText(logFilePathTForgotPassword);
                forgotPasswordtext = forgotPasswordtext.Replace("SENDERNAME", userName);
                forgotPasswordtext = forgotPasswordtext.Replace("MAINPASSWORDURL", callBackurl);
                forgotPasswordtext = forgotPasswordtext.Replace("NEWPASSWORD", tempPassword);
                var listemail = new List<string>();
                listemail.Add(email);
                EmailCredentialsViewModel emailCredentials = EmailHelper.GetPasswordResetEmailCredentials();
                var rootEmail = new RootEmail();
                rootEmail.Attachments = null;
                rootEmail.Body = forgotPasswordtext;
                rootEmail.FromEmailAddress = emailCredentials.FromEmail;
                rootEmail.Username = emailCredentials.Username;
                rootEmail.Password = emailCredentials.Password;
                rootEmail.Port = emailCredentials.Port;
                rootEmail.Smtp = emailCredentials.Smtp;
                rootEmail.Subject = "Batransfer Forgot Password";
                rootEmail.ToEmailAddresses = listemail;
                rootEmail.DisplayName = emailCredentials.DisplayName;
                rootEmail.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                IsSend = await EmailHelper.SendEmailWithoutAttachtment(rootEmail, apiUrl);
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return IsSend;
        }
        #endregion

        #region Forget Password
        public async Task<bool> SendCustomerForgotPassword(string email, string emailtoken, string userName, string tempPassword) 
        {
            bool IsSend = false;
            try
            {
                string path = string.Empty;
                Uri uriResult;
                //if (Uri.TryCreate(HttpContext.Current.Request.Url.Host, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps))
                //{
                //    path = uriResult.AbsoluteUri;
                //    //  path = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                //}
                //else
                //{
                //    path = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                //  //  path = "https://" + HttpContext.Current.Request.Url.Host;
                //}
                path = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                string encodemailtoken = HttpContext.Current.Server.UrlEncode(emailtoken);


                string callBackurl = path + "/NewPassword/ForgotPasswordReset?token=" + encodemailtoken;// HttpContext.Current.Server.UrlEncode();



                string logFilePathTForgotPassword = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/ForgotPassword.txt"));
                string forgotPasswordtext = System.IO.File.ReadAllText(logFilePathTForgotPassword);

                forgotPasswordtext = forgotPasswordtext.Replace("SENDERNAME", userName);
                forgotPasswordtext = forgotPasswordtext.Replace("MAINPASSWORDURL", callBackurl);
                forgotPasswordtext = forgotPasswordtext.Replace("NEWPASSWORD", tempPassword);


                var listemail = new List<string>();
                listemail.Add(email);

                EmailCredentialsViewModel emailCredentials = EmailHelper.GetCustomerEmaillCredentials();
                var rootEmail = new RootEmail();
                rootEmail.Attachments = null;
                rootEmail.Body = forgotPasswordtext;
                rootEmail.FromEmailAddress = emailCredentials.FromEmail;
                rootEmail.Username = emailCredentials.Username;
                rootEmail.Password = emailCredentials.Password;
                rootEmail.Port = emailCredentials.Port;
                rootEmail.Smtp = emailCredentials.Smtp;

                rootEmail.Subject = "Batransfer Forgot Password";
                rootEmail.ToEmailAddresses = listemail;
                rootEmail.DisplayName = emailCredentials.DisplayName;
                rootEmail.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                IsSend = await EmailHelper.SendEmailWithoutAttachtment(rootEmail, apiUrl);


            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return IsSend;
        }
        #endregion


    }
    public class EmailTokentStatus
    {
        public string Message { get; set; }
        public string UserId { get; set; }
        public bool IsSuccess { get; set; }
    }
}