﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.WebPages;
using SwaranAdmin_New.Autocab.Base;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using SwaranAdmin_New.Models.BookingMo;
using SwaranAdmin_New.Models.TeleBooking;
using System.Web.Mvc;

namespace SwaranAdmin_New.DataAccess.TeleBooking
{
    public class TelebookingDal
    {
        #region Singleton
        private static TelebookingDal _instance;
        private static readonly object LockThis = new object();
        protected TelebookingDal()
        {
        }
        public static TelebookingDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new TelebookingDal()); 
                }
            }
        }
        #endregion

        public List<TelVehicleTypeViewModel> GetVehicletypes()
        {
            try
            {
                using (var db=new BATransferEntities())
                {
                    IEnumerable<TelVehicleTypeViewModel> objVehi =
                        db.VehicleTypes.Where(x => !x.IsDelete  && x.IsActive).Select(x => new TelVehicleTypeViewModel()
                        {
                            Name = x.VehicleTypeName,
                            VehicleId = x.VehicleTypeID,
                        });
                    return objVehi.ToList();
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public int GetMaxNumOfPessangerCount()
        {
            int maxCount = 0;
            try
            {
                using (var db = new BATransferEntities())
                {

                    int configId = (int)VehicleCombinationConfigurationEnum.MaxPassangersCount;
                    if (db.VehicleCombinationConfigs != null && db.VehicleCombinationConfigs.Any(x => x.VehicleCongId == configId))
                    {
                        maxCount = db.VehicleCombinationConfigs.First(x => x.VehicleCongId == configId).Count;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return maxCount;
        }

        public List<TitleViewModel> LoadTitles()
        {
            try
            {
                using (var db = new BATransferEntities())
                {


                    IEnumerable<TitleViewModel> objTitle =
                        db.Titles.Where(x => x.IsDeleted == false && x.IsEnabled == true)
                            .Select(x => new TitleViewModel()
                            {
                                TitleId = x.TitleId,
                                TitleName = x.TitleName
                            });
                    return objTitle.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

    
        //public List<SelectListItem> LoadCountrys()
        //{
        //    try
        //    {

        //        using (var db = new BATransferEntities())
        //        {

        //            IEnumerable<SelectListItem> objCounties =
        //                db.Countries.OrderBy(x => x.Name).ToList().Select(x => new SelectListItem()
        //                {

        //                    Text = x.Name + "  (" + x.Phonecode.ToString() + ")",
        //                    Value = x.Iso
        //                });
        //            return objCounties.ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
        //    }
        //    return null;
        //}

        public List<DriverEllapseViewMode> GetEllapseTimeList(bool hasLuggages, bool isSinglePassenger, int journeyClassId, int passportTypeId)
        {

            var listEllapseTime = new List<DriverEllapseViewMode>();

            using (var db = new BATransferEntities())
            {
                var relevantDriverDuration = db.DriverPickupTimeDurations.FirstOrDefault(dpd => dpd.IsWithLuggage == hasLuggages && dpd.IsSinglePassenger == isSinglePassenger && dpd.JourneyClassEnumId == journeyClassId && dpd.PassportTypeEnumId == passportTypeId);

                var array = new int[1];

                if (relevantDriverDuration != null)
                {

                    int minTimeInte = int.Parse(ConfigurationManager.AppSettings["MinTimeIntervalDriver"]);

                    int arrayLength = (180 - relevantDriverDuration.TimeDuration) / (minTimeInte == 0 ? 5 : minTimeInte);

                    array = new int[arrayLength + 1];

                    int a = 0;

                    for (int i = relevantDriverDuration.TimeDuration; i <= 180; i++)
                    {
                        if (i % minTimeInte == 0)
                        {
                            array[a] = i;
                            a++;
                        }

                    }
                }
                else
                {
                    if (hasLuggages)
                    {
                        array = new int[] { 30, 45, 60, 90, 120, 150, 180 };
                    }
                    else
                    {
                        array = new int[] { 15, 30, 45, 60, 90, 120, 150, 180 };
                    }
                }



                foreach (var item in array)
                {
                    var ellapseTime = new DriverEllapseViewMode();
                    ellapseTime.Id = item;
                    ellapseTime.Value = item.ToString();
                    listEllapseTime.Add(ellapseTime);

                }
            }

            return listEllapseTime;
        }


        public List<CountryInfoViewModel> LoadCountryCode()
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    IEnumerable<CountryInfoViewModel> objCounties =
                        db.Countries.ToList().OrderBy(x => x.Name).Select(x => new CountryInfoViewModel()
                        {
                            CountryId = x.ID,
                            CountryName = x.Name,
                            CountryCode = x.Iso,
                            PhoneDisplayCode = x.Name + " (" + x.Phonecode.ToString() + ")",
                            // PhoneDisplayCode = x.Phonecode.ToString(),
                            PhoneCode = x.Phonecode.ToString()

                        });
                    return objCounties.ToList();
                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return null;
        }

        public List<CountryInfoViewModel> LoadCountryCodes()
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    IEnumerable<CountryInfoViewModel> objCounties =
                        db.Countries.ToList().OrderBy(x => x.Name).Select(x => new CountryInfoViewModel()
                        {
                            CountryId = x.ID,
                            CountryName = x.Name,
                            CountryCode = x.Iso,
                            PhoneDisplayCode = x.Phonecode.ToString(),
                            PhoneCode = x.Phonecode.ToString()

                        });
                    return objCounties.ToList();
                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return null;
        }

        public string GetCustomerSpecialPreference(string userId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.CustomerSpecialPreferences.Any(x => x.CustomerUserId.Equals(userId)))
                    {
                        var specialPreferences = db.CustomerSpecialPreferences.First(x => x.CustomerUserId.Equals(userId));

                        return specialPreferences.Note;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return string.Empty;
        }

        public bool FindIsFreeJournery(string userId, TelBookingViewModel booking, 
          ReservationModel reservation, out string freeJourneyCode)
        {
            freeJourneyCode = string.Empty;
            try
            {
                using (var db = new BATransferEntities())
                {
                    string fromAreaType = Enum.GetName(typeof(AreapointTypeEnum), booking.FromAriapointTypeId);
                    string toAreaType = Enum.GetName(typeof(AreapointTypeEnum), booking.ToAriapointTypeId);
                    long vehicleTypeid = booking.VehicleTypeId;
                    string fromAirportCode = booking.FromAirportCode;
                    string toAirportCode = booking.ToAirportCode;
                    bool isReturnWay = reservation.JourneyTypeId == (int)JourneyTypeEnum.TwoWay;
                    var objFreeJourneyAvailability = db.CheckFreeJourneyAvailability(booking.Distance, reservation.OneWayPrice, isReturnWay, booking.VehicletypeId,
                         fromAreaType, toAreaType, userId, fromAirportCode, toAirportCode, 122).FirstOrDefault();

                    var freeJourney = db.CustomerFreeJourneys.FirstOrDefault(
                         x => x.UserId.Equals(userId) && x.BookingId == null && !x.IsUsed && !x.IsDeleted && x.IsEnabled);
                    if (freeJourney != null)
                    {
                        freeJourneyCode = !string.IsNullOrEmpty(freeJourney.Code) ? freeJourney.Code.Trim() : freeJourney.Code;
                    }
                    return objFreeJourneyAvailability.Value;

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return false;
        }

        public decimal FindIsFreeJournerySurcharge(TelBookingViewModel address)
        {
            try
            {

                using (var db = new BATransferEntities())
                {
                    var dateTime = address.BookingDate + " " + address.BookingTime;
                    var bookingDateTime = ValueParseHelper.TryParseDateTime(dateTime);
                    if (db.Rate_SpecialOccasion.Any(x => x.FromDate <= bookingDateTime && x.ToDate >= bookingDateTime))
                    {
                        var freeJourney =
                            db.Rate_SpecialOccasion.First(
                                x => x.FromDate <= bookingDateTime && x.ToDate >= bookingDateTime);
                        return freeJourney.Surcharge;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return 0;
        }

        public bool FindUseMyCredit(string userId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.Customers.Any(x => x.userId.Equals(userId)))
                    {
                        return db.Customers.First(x => x.userId.Equals(userId)).TotalCredits > 0;
                        //&& db.Customers.First(x => x.userId.Equals(userId)).IsDiscountSchemeActivated;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return false;
        }

        public bool IsBlockIpAddress(string userId, string ipAddress)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.IpRestrictions.Any(x => x.UserId == userId && x.IpAddress.Equals(ipAddress) && x.IsEnabled == true && x.IsDeleted == false))
                    {
                        var objIpRestrictions =
                            db.IpRestrictions.First(
                                x =>
                                    x.UserId == userId && x.IpAddress.Equals(ipAddress) &&
                                    x.IsEnabled == true && x.IsDeleted == false);
                        return !objIpRestrictions.IsAllowed;


                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
                return true;
            }
            return false;
        }

        public FixedPriceAndDrivingDistanceViewModel IsAvailableFixedPrice(long fromFixedPriceAriaPointId, long toFixedPriceAriaPointId)
        {
            var fixedPrice = new FixedPriceAndDrivingDistanceViewModel();
            try
            {
                using (var context = new BATransferEntities())
                {
                    var firstOrDefault = context.FixedPriceDrivingDistance(fromFixedPriceAriaPointId, toFixedPriceAriaPointId).FirstOrDefault();
                    if (firstOrDefault != null)
                    {
                        fixedPrice.IsFixedPriceAvailable = true;
                        fixedPrice.DrivingDistance = !string.IsNullOrEmpty(firstOrDefault.Distance) ? decimal.Parse(firstOrDefault.Distance.Trim()) : 0;
                        fixedPrice.Drivingtime = !string.IsNullOrEmpty(firstOrDefault.DrivingTime) ? int.Parse(firstOrDefault.DrivingTime.Trim()) / 60 : 0;

                        return fixedPrice;
                    }

                    fixedPrice.IsFixedPriceAvailable = false;
                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                      System.Reflection.MethodBase.GetCurrentMethod().Name,
                      ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return fixedPrice;
        }

       
        public List<SelectListItem> GetEllapseTimeListtest()
        {
            var listEllapseTime = new List<SelectListItem>();
            var array = new int[] { 15, 30, 45, 60, 90, 120, 150, 180 };

            foreach (var item in array)
            {
                var handLuggages = new SelectListItem();
                handLuggages.Text = item.ToString();
                handLuggages.Value = item.ToString();
                listEllapseTime.Add(handLuggages);
            }


            return listEllapseTime;
        }

        public List<SelectListItem> GetNumberOfHandLuggageList(int maxHandLaggaes)
        {
            var listHandLuggages = new List<SelectListItem>();
            var list = Enumerable.Range(0, maxHandLaggaes + 1).ToList();
            foreach (var item in list)
            {
                var handLuggages = new SelectListItem();
                handLuggages.Text = item.ToString();
                handLuggages.Value = item.ToString();
                listHandLuggages.Add(handLuggages);
            }
            return listHandLuggages;
        }

        public List<SelectListItem> GetNumberOfLuggageList(int maxHandLaggaes)
        {
            var listLuggages = new List<SelectListItem>();
            var list = Enumerable.Range(0, maxHandLaggaes + 1).ToList();
            foreach (var item in list)
            {
                var luggages = new SelectListItem();
                luggages.Value = item.ToString();
                luggages.Text = item.ToString();
                listLuggages.Add(luggages);
            }
            return listLuggages;
        }


        #region Price Calulation
        public PriceCalOutViewModel GetOneWayPrice(TelPriceViewModel booking,long rateId, ViaPriceCalViewModel viaModel)
        { 
            var objPrice = new PriceCalOutViewModel();
            decimal finalAmount = 0;
           
            var listBookingVehicle=new List<BookingVehicleDom>();


            try
            {
                using (var context = new BATransferEntities())
                {
                    DateTime bookingDateTime = ValueParseHelper.TryParseDateTime(booking.BookingDate + " " + booking.BookingTime);
                   

                            if (booking.ViaPointAddress != null && booking.ViaPointAddress.Any(x => !x.Postcode.IsEmpty()))
                            {
                           objPrice.listBookingVehicle = GetViapointsPriceNew(booking, rateId, viaModel, out objPrice);
                            }
                            else if ((booking.FromFixedPriceAriaPointId > 0 && booking.ToFixedPriceAriaPointId > 0) || booking.TotalOneWayDistance > 0)
                            {
                                var vehicle = new BookingVehicleDom();
                                List<PriceCalculationVia_Result> result =
                                    context.PriceCalculationVia(bookingDateTime, rateId,
                                        booking.FromFixedPriceAriaPointId, 0,
                                        booking.ToFixedPriceAriaPointId, 0, booking.TotalOneWayDistance, booking.VehicleTypeId, (int)BookingSiteEnum.BA, false, booking.EmissionZoneId, booking.UserId).ToList();

                                string autocabVehicleTypeName = string.Empty;
                                string autocabVehicleCategory = string.Empty;
                                var objVehicle = context.VehicleTypes.FirstOrDefault(
                                  x =>
                                      x.IsDelete == false && x.IsNormal == true &&
                                      x.VehicleTypeID == booking.VehicleTypeId);
                                if (objVehicle != null)
                                {
                                    autocabVehicleTypeName = objVehicle.AutocabVehicleType;
                                    autocabVehicleCategory = objVehicle.AutocabVehicleCategory;
                                }

                                foreach (var price in result)
                                {

                                    decimal amt = Math.Round(Decimal.Parse(price.TotalPrice.ToString()));
                                    finalAmount = finalAmount + amt;
                                    objPrice.PickupCharge = price.PickupCharge ?? 0;
                                    objPrice.Dropoffcharge = price.DropoffCharge ?? 0;
                                    objPrice.AccountFee = price.ACCOUNTFEE ?? 0;
                                    objPrice.OnewaySurcharge = (price.SURCHARGE ?? 0) + (price.DROPOFFSURCHARGE ?? 0);
                                    objPrice.SurChDec = price.SPECIALOCCASIONCOMMENTS;
                                    objPrice.DropoffsurChDec = price.DROPOFFSPECIALOCCASIONCOMMENTS;
                                    //noEmissionDes = price.EMISSIONDESCRIPTION;
                                   // vehicle.VehicletypeName = vehicleType.VehicleType.VehicleTypeName;
                                    vehicle.VehicleTypeId = booking.VehicleTypeId;
                                    vehicle.DistancePrice = price.TotalPrice;
                                    vehicle.AutocabVehicleTyle = autocabVehicleTypeName;
                                    vehicle.AutocabVehicleCategory = autocabVehicleCategory;
                                    listBookingVehicle.Add(vehicle);
                                }
                                objPrice.listBookingVehicle = listBookingVehicle;
                                 objPrice.Amount = finalAmount;


                            }


                    


                }
            }
            catch (Exception ex)
            {
                string addressxml = XmlHelper.GetXMLFromObject(booking);
                ErrorHandling.LogFileWriteWithOblect(addressxml, ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return objPrice;
        }

        public List<BookingVehicleDom> GetViapointsPriceNew(TelPriceViewModel model,long rateId, ViaPriceCalViewModel viaModel, out PriceCalOutViewModel objPrice)
        {
            var listBookingVehicle = new List<BookingVehicleDom>();
            objPrice = new PriceCalOutViewModel();
            decimal mainPrice = 0; 
            decimal distance = 0;
            decimal distancePrice = 0;
            try
            {
                DateTime bookingDateTime = ValueParseHelper.TryParseDateTime(model.BookingDate + " " + model.BookingTime);
                using (var context = new BATransferEntities())
                {
                    if (model.ViaPointAddress != null)
                    {
                        decimal maindistance = viaModel.MainDistanceVia;

                        //   var listViapoints = model.ViaPointAddress.Where(x => !x.Postcode.IsEmpty()).ToArray();

                        if (model.FromFixedPriceAriaPointId == 0)
                            model.FromFixedPriceAriaPointId =
                                FixedPriceHelper.GetFromViapointFixedPriceId(model.FromAriapointTypeId,
                                    model.FromPostCode);
                        if (model.ToFixedPriceAriaPointId == 0)
                            model.ToFixedPriceAriaPointId =
                                FixedPriceHelper.GetFromViapointFixedPriceId(model.ToAriapointTypeId,
                                    model.ToPostCode);
                        List<PriceCalculationVia_Result> result = null;
                        var availableFixedPrice = IsAvailableFixedPrice(model.FromFixedPriceAriaPointId, model.ToFixedPriceAriaPointId);
                        if (availableFixedPrice.IsFixedPriceAvailable)
                        {
                            result =
                               context.PriceCalculationVia(bookingDateTime, rateId,
                                   model.FromFixedPriceAriaPointId, 0,
                                   model.ToFixedPriceAriaPointId, 0, maindistance,
                                   model.VehicleTypeId, (int)BookingSiteEnum.BA, false, model.EmissionZoneId, model.UserId).ToList();
                        }
                        else
                        {


                            result =
                                context.PriceCalculationVia(bookingDateTime, rateId,
                                    model.FromFixedPriceAriaPointId, 0,
                                    model.ToFixedPriceAriaPointId, 0, maindistance,
                                   model.VehicleTypeId, (int)BookingSiteEnum.BA, true, model.EmissionZoneId, model.UserId).ToList();
                        }


                        foreach (var price in result)
                        {
                            mainPrice = mainPrice + price.TotalPrice;
                            objPrice.PickupCharge = price.PickupCharge ?? 0;
                            objPrice.AccountFee = price.ACCOUNTFEE ?? 0;
                            objPrice.Dropoffcharge = price.DropoffCharge ?? 0;
                            objPrice.OnewaySurcharge = (price.SURCHARGE ?? 0) + (price.DROPOFFSURCHARGE ?? 0);
                            objPrice.SurChDec = price.SPECIALOCCASIONCOMMENTS;
                            objPrice.DropoffsurChDec = price.DROPOFFSPECIALOCCASIONCOMMENTS;
                          //  noEmissionDes = price.EMISSIONDESCRIPTION;
                        }

                        decimal viaAmt = 0;
                        if (viaModel.TotalViaDistance > maindistance)
                        {
                            decimal val = viaModel.TotalViaDistance - maindistance;
                            viaAmt = val * viaModel.ViaMileRate;
                        }

                        if (model.ViaPointAddress != null)
                        {
                            var listViapoints = model.ViaPointAddress.Where(x => x.Lat != 0).ToList();
                            foreach (var via in listViapoints)
                            {
                                viaAmt = viaAmt + viaModel.ViaMinimumRate;
                            }
                        }

                        if (viaModel.TotalViaTime > viaModel.MainTimeVia)
                        {
                            decimal val = viaModel.TotalViaTime - viaModel.MainTimeVia;
                            viaAmt = viaAmt + (val * viaModel.ViaTimeRate);
                        }


                        string autocabVehicleTypeName = string.Empty;
                        string autocabVehicleCategory = string.Empty;
                        var objVehicle = context.VehicleTypes.FirstOrDefault(
                          x =>
                              x.IsDelete == false && x.IsNormal == true &&
                              x.VehicleTypeID == model.VehicleTypeId);
                        if (objVehicle != null)
                        {
                            autocabVehicleTypeName = objVehicle.AutocabVehicleType;
                            autocabVehicleCategory = objVehicle.AutocabVehicleCategory;
                        }


                        decimal valAmt = mainPrice + viaAmt;
                        distancePrice = Math.Ceiling(valAmt);
                        var vehicle = new BookingVehicleDom();
                      //  vehicle.VehicletypeName = vehicleType.VehicleType.VehicleTypeName;
                        vehicle.VehicleTypeId = model.VehicleTypeId;
                        vehicle.DistancePrice = distancePrice;
                        vehicle.AutocabVehicleTyle = autocabVehicleTypeName;
                        vehicle.AutocabVehicleCategory = autocabVehicleCategory;

                        listBookingVehicle.Add(vehicle);

                        return listBookingVehicle;
                    }
                }

            }
            catch (Exception ex)
            {
                string addressxml = XmlHelper.GetXMLFromObject(model);
                ErrorHandling.LogFileWriteWithOblect(addressxml, ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return listBookingVehicle;
        }

        public int IsLondonBooking(long fromFixedPriceAriaPointId, long toFixedPriceAriaPointId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objBooking = db.IsLondonBooking(fromFixedPriceAriaPointId, toFixedPriceAriaPointId).FirstOrDefault();
                    if (objBooking != null)
                    {
                        return objBooking.Value;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return 0;
        }

        public ViaConfigurationViewModel GetViaConfiguration()
        {
            var config = new ViaConfigurationViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objViapoint = db.ViapointConfigs.FirstOrDefault(x => x.ConfigId == (int)ViapointConfigEnum.PriceCalculationId);
                    if (objViapoint != null)
                    {
                        config.MileRate = objViapoint.MileRate;
                        config.ViaTimeRate = objViapoint.TimeRate ?? 0;
                        config.MinimumRate = objViapoint.MinimumRate;
                        config.IsFastRoute = objViapoint.IsFastRoute;

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return config;
        }

        public int GetExtraDrivingTime(int currentTime)
        {
            try
            {
                using (var context = new BATransferEntities())
                {
                    var extraDrivingTime = context.FixedPriceExtraDrivingTime(currentTime).FirstOrDefault();
                    if (extraDrivingTime != null)
                    {

                        return extraDrivingTime.Value;
                    }


                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                      System.Reflection.MethodBase.GetCurrentMethod().Name,
                      ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return 0;
        }
        public long GetRateId(string userId)
        {
            long rateId = (int)DefaultRateEnum.CASH_RATE;
            try
            {
                 
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return rateId;
        }
        #endregion
    }
}