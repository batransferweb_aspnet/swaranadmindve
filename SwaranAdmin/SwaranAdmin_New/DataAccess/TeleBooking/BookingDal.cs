﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.WebPages;
using SwaranAdmin_New.Autocab;
using SwaranAdmin_New.Autocab.Base;
using SwaranAdmin_New.Autocab.Model;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using SwaranAdmin_New.Models.BookingMo;

namespace SwaranAdmin_New.DataAccess.TeleBooking 
{
    public class BookingDal
    {
        #region Singleton
        private static BookingDal _instance;
        private static readonly object LockThis = new object();
        protected BookingDal()
        {
        }
        public static BookingDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new BookingDal());
                }
            }
        }
        #endregion
        string alternativeNo = string.Empty;
        #region Save Booking 
        //  public SaveBookingViewModel SaveBooking(BookingDom booking)
        public SaveBookingViewModel SaveBooking(TelBookingViewModel  reservation,    ReturnJourney returnJourney)
        {
            //string reservationModel = XmlHelper.GetXMLFromObject(reservation);
            //string addressxml = XmlHelper.GetXMLFromObject(address);
            //string vehicleCombinationxml = XmlHelper.GetXMLFromObject(vehicleCombination);
            //string returnJourneyxml = XmlHelper.GetXMLFromObject(returnJourney);
            //string xml = "reservationModel \n" + reservationModel + "\n\n addressxml \n " + addressxml + "\n\n vehicleCombinationxml \n" + vehicleCombinationxml + "\n\nreturnJourneyxml\n" + returnJourneyxml;
            //ErrorHandling.LogFileWriteWithOblect(xml, "", "", "", "");


            bool isNullUser = bool.Parse(ConfigurationManager.AppSettings["IsNullUser"]);
            if (isNullUser && string.IsNullOrEmpty(reservation.UserId))
            {
                //string reservationModel = XmlHelper.GetXMLFromObject(reservation);
                //string addressxml =  XmlHelper.GetXMLFromObject(address);
                //string vehicleCombinationxml = XmlHelper.GetXMLFromObject(vehicleCombination);
                //string returnJourneyxml = XmlHelper.GetXMLFromObject(returnJourney);
                //string xml = "reservationModel \n" + reservationModel + "\n\n addressxml \n " + addressxml + "\n\n vehicleCombinationxml \n" + vehicleCombinationxml + "\n\nreturnJourneyxml\n" + returnJourneyxml;
                //ErrorHandling.LogFileWriteWithOblect(xml, "", "", "", "");
            }

            var savedBooking = new SaveBookingViewModel();
            //  string bookingNum = string.Empty;
            string bookingToken = string.Empty;
            Booking_TEMP newBooking = null;
            bool isSendToGhost = false;
            try
            {
                reservation.LandingDateTime = !string.IsNullOrEmpty(reservation.LandingDate)
                    ? ValueParseHelper.TryParseDateTime(reservation.LandingDate + " " + reservation.LandingTime)
                    : (DateTime?)null;
                reservation.BookingDeuTime = DateTime.Now;
                reservation.BookingDeuTimeReturn = DateTime.Now;
                string passaAltCode = !string.IsNullOrEmpty(reservation.PassengerAlternativeMobileCountryCode) ? reservation.PassengerAlternativeMobileCountryCode : "";
                string bookPassonAltCode = !string.IsNullOrEmpty(reservation.BookedPersonAlternativeMobileCountryCode) ? reservation.BookedPersonAlternativeMobileCountryCode : "";
                alternativeNo = !string.IsNullOrEmpty(reservation.BookedPersonAlternativeMobileNo)
                              ? bookPassonAltCode + reservation.BookedPersonAlternativeMobileNo
                              : passaAltCode + reservation.PassengerAlternativeMobileNo;
                // string fromLocation = ConfigHelper.GetOrganizationLocation((int)BookingSiteEnum.ET);
                //var distanceList = BaGoogleApi.GetDistance(fromLocation, address.FromLat + "," + address.FromLong);
                //if (distanceList != null && distanceList.Count > 0)
                //{
                //    var duration = distanceList.Min(x => x.Time);
                //    booking.BookingDeuTime = ValueParseHelper.TryParseDateTime(booking.BookingDateTime).AddMinutes(-(double)duration);
                //}
                //if (booking.JourneyTypeId == (int)JourneyTypeEnum.TwoWay)
                //{
                //    var valuesReturn = BaGoogleApi.GetDistance(fromLocation, returnJourney.ToLat + "," + returnJourney.ToLong);
                //    if (valuesReturn != null && valuesReturn.Count > 0)
                //    {
                //        var durationReturn = valuesReturn.Min(x => x.Time);
                //        booking.BookingDeuTimeReturn = ValueParseHelper.TryParseDateTime(booking.BookingDateTime).AddMinutes(-(double)durationReturn);
                //    }
                //}
                //  reservation.IpAddress = System.Web.HttpContext.Current.Request.UserHostAddress ?? "";
                using (var db = new BATransferEntities())
                {
                    reservation.BookedPersonMobileNumberCountryCode = CountryCodeHelper.GetCountryCode(reservation.BookedPersonMobileNumberCountryCode);
                    reservation.PassengerMobileNumberCountryCode = CountryCodeHelper.GetCountryCode(reservation.PassengerMobileNumberCountryCode);
                    if (reservation.PaymentMethodId != (int)PaymentTypeEnum.Account)
                    {
                        reservation.OneWayPrice = reservation.OneWayPrice - reservation.AccountFee;
                        reservation.ReturnWayPrice = reservation.ReturnWayPrice - returnJourney.AccountFee;
                    }
                    reservation.JourneyAmount = reservation.JourneyTypeId == (int)JourneyTypeEnum.OneWay
                     ? reservation.OneWayPrice
                     : reservation.OneWayPrice + reservation.ReturnWayPrice;
                    isSendToGhost = OrganizationHelper.IsSendToGhostFromSite((int)BookingSiteEnum.BA);
                    reservation.BookingSiteId = (int)BookingSiteEnum.BA;

                    
                    // reservation.BookingNumber = bookingNum;
                    bookingToken = Security.EncryptToken(reservation.UserId + DateTime.UtcNow.ToString(),
                      DateTime.UtcNow.ToString());
                    reservation.BookingToken = bookingToken;
                    newBooking = new Booking_TEMP()
                    {
                        #region Booking temp model
                        UserId = reservation.UserId,
                        CompanyId = reservation.CompanyId > 0 ? reservation.CompanyId : null,
                        //relevantAspNetUser.IsCompanyUser ? relevantAspNetUser.CompanyId : 0,
                        BookingToken = bookingToken,
                        //Status = booking.CardPaymentTypeId != null && booking.CardPaymentTypeId == (int)CardPaymentTypeEnum.PayOverThePhone
                        //? (int)BookingStatusEnum.Requested
                        //: (int)BookingStatusEnum.Requested,
                        Status = (int)BookingStatusEnum.Requested,
                        // isBookingConfirmationUponRequestBooking ? (int)BookingStatusEnum.Confirmed :
                        JourneyType = reservation.JourneyTypeId,
                        //BookingDispatchTime = ValueParseHelper.TryParseDate(request.BookingDispatchTime),
                        IsMultipleBooking = false,
                        MultipleBookingId = string.Empty,
                        RateId = reservation.RateId,
                        CostCenterId = null,
                        CostCodeId = null,
                        NumberOfPassengers = reservation.NumOfPassengers,
                        DespatchPref = reservation.PickupNote ?? "",
                        FromTown = reservation.FromPostTown ?? "",
                        BookingFrom = reservation.BookingFrom,
                        ToTown = reservation.ToPostTown ?? "",
                        BookingTo = reservation.BookingTo,
                        PassengerTitleId = reservation.TitleId,
                        PassengerName = !string.IsNullOrEmpty(reservation.PassengerFirstName)
                                        ? reservation.PassengerFirstName + " " + reservation.PassengerLastName
                                        : "",
                        PassengerTel = reservation.PassengerMobileNumberCountryCode + reservation.PassengerMobileNo,
                        PassengerEmail = reservation.PassengerEmail,
                        BookedPersonTitleId = reservation.BookedPersonTitleId,
                        BookedPersonName = reservation.BookedPersonFirstName + " " + reservation.BookedPersonLastName,
                        BookedPersonTel = reservation.BookedPersonMobileNumberCountryCode + reservation.BookedPersonMobileNo,
                        BookedPersonEmail = reservation.BookedPersonEmail,
                        CustomerComment = reservation.CustomerComment ?? "",
                        AlternativePhoneNumber = alternativeNo ?? "",
                        DriverComment = string.Empty,
                        OfficeComment = string.Empty,
                        Alert = string.Empty,
                        BookingFromFullPostCode = reservation.FromPostCode ?? "",
                        BookingToFullPostCode = reservation.ToPostCode ?? "",
                        FromLat = reservation.FromLat,
                        FromLong = reservation.FromLong,
                        ToLat = reservation.ToLat,
                        ToLong = reservation.ToLong,
                        CustomerType = ((int)CustomerTypeEnum.Member),// IsCompanyUser ? ((int)CustomerTypeEnum.Corporate) : ((int)CustomerTypeEnum.Member),
                        ToFixedPriceAriaPointId = reservation.ToFixedPriceAriaPointId,
                        FromFixedPriceAriaPointId = reservation.FromFixedPriceAriaPointId,
                        //CardTypeId = booking.CardTypeId != 0 ? booking.CardTypeId : null,
                        CardTypeId = null,
                        CardPaymentTypeId = reservation.CardPaymentTypeId != 0 ? reservation.CardPaymentTypeId : null,
                        IsSpeedCalculation = reservation.IsSpeedCalculation,
                        TotalTime = reservation.TotalTime,
                        TotalDistance = reservation.TotalDistance,
                        PaymentMethod = reservation.PaymentMethodId == (int)PaymentTypeEnum.Use_My_Credits && reservation.BookingFee > 0
                                                                    ? (int)PaymentTypeEnum.Card
                                                                    : reservation.PaymentMethodId,
                        BookingSource = reservation.BookingSource,
                        IpAddress = reservation.IpAddress,
                        OperatorId = 0,
                        PCMacAddress = "BA Web",
                        CreatedDate = DateTime.UtcNow,
                        ModifiedDate = DateTime.UtcNow,
                        PaymentStatus = reservation.PaymentMethodId == (int)PaymentTypeEnum.Cash
                                        ? (int)PaymentStatusEnum.BookingFeePending
                                        : reservation.PaymentStatusId,
                        ReceiptNumber = string.Empty,
                        IsSendSmsUponArrival = true, // request.IsSendSmsUponArrival,
                        IsSendSmsUponDispatch = true, //request.IsSendSmsUponDispatch,
                        IsSendSmsUponFinish = true, // request.IsSendSmsUponFinish,
                        IsSendSmsUponConfirmation = true, // request.IsSendSmsUponConfirmation,
                        BookingNumber = reservation.BookingNumber,
                        FlightNo = reservation.FromAriapointTypeId == (int)AreapointTypeEnum.Airport
                            ? reservation.FlightNo
                            : reservation.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport
                            ? reservation.CruiseShipName : "",
                        DriverDisplayName = reservation.FromAriapointTypeId == (int)AreapointTypeEnum.Airport
                            ? reservation.DisplayNameFlight ?? ""
                            : reservation.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport
                            ? reservation.DisplayNameCruiseShip ?? "" : "",
                        BookingDateTime = (!string.IsNullOrEmpty(reservation.LandingDate) && (reservation.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport || reservation.FromAriapointTypeId == (int)AreapointTypeEnum.Airport)) ? ValueParseHelper.TryParseDateTime(reservation.LandingDate + " " + reservation.LandingTime) : ValueParseHelper.TryParseDateTime(reservation.BookingDateTime),
                        BookingDeuTime = reservation.BookingDeuTime,
                        DriverEllapseTime = reservation.DriverEllapseTime,
                        FlightLandingDateTime = reservation.LandingDateTime,
                        MapUrl = "",
                        IsSourceBooking = reservation.JourneyTypeId == (int)JourneyTypeEnum.TwoWay,
                        Country = reservation.CountryName ?? "",
                        CancelBookingId = null,
                        FromAriaPointTypeId = reservation.FromAriapointTypeId,
                        ToAriaPointTypeId = reservation.ToAriapointTypeId,
                        ReservationDateTime = ValueParseHelper.TryParseDateTime(reservation.ClientDatetTime),
                        Luggage = reservation.NumberOfLuggages,
                        HandLuggage = reservation.NumberOfHandLuggages,
                        BoosterSeat = reservation.BoosterSeat,
                        InfantSeats = reservation.InfantSeats,
                        ChildSeats = reservation.ChildSeats,
                        IsSendPassengerEmail = reservation.IsSendPassengerEmail,
                        IsSendPassengerSms = reservation.IsSendPassengerSms,
                        Autocab_AuthorizationReference = "",
                        Autocab_BookingReference = "",
                        Autocab_IsBookingCancelled = false,
                        VendorID = "",
                        TotalDistancePrice = reservation.OneWayPrice,
                        OtherCharges = reservation.BookingFee,
                        StudentDiscount = reservation.StudentDiscountAmount,
                        CustomerCredit = 0,
                        ConsumerDebit = reservation.OneWayCredit,
                        Surcharge = reservation.Surcharge,
                        TotalPrice = (reservation.OneWayPrice + reservation.BookingFee),
                        FinalPrice = (reservation.OneWayPrice + reservation.BookingFee) - reservation.OneWayCredit,
                        RefundAmount = 0,
                        JourneyClass = 1,
                        PickupCharge = reservation.PickupCharge,
                        DropoffCharge = reservation.DropoffCharge,
                        IsFreeJourneyAccessAmt = !(reservation.FreeJourneyAccessAmt > 0),
                        FreeJourneyAccessAmt = reservation.FreeJourneyAccessAmt,
                        UsedPoints = reservation.UpgradePoints,
                        VehicleUpgradeAmt = reservation.UpgradeAmount,
                        IsUpgradeByPoints = reservation.IsVehicleUpgradeEnabeld,//reservation.IsUpgradeByPoints,
                        IsVehicleUpgradeEnabeld = reservation.IsVehicleUpgradeEnabeld,
                        IsBookingFeePaid = false,

                        #endregion
                    };
                    db.Booking_TEMP.Add(newBooking);
                    db.SaveChanges();
                    reservation.BookingId = newBooking.BookingId;
                    savedBooking.BookingdateTime = ValueParseHelper.TryParseDateTime(reservation.BookingDateTime);
                    savedBooking.BookingFrom = reservation.BookingFrom;
                    savedBooking.BookingTo = reservation.BookingTo;
                    savedBooking.BookingNumber = reservation.BookingNumber;
                    long returnFixedBookingId = 0;
                    long returnFixedBookingPaymentId = 0;
                    var listBookingVehicletypesReturn = new List<BookingVehicleDom>();
                    var listBookingVehicletypes = new List<BookingVehicleDom>();
                   
                            var bookingVehicletype = new BookingVehicleType_TEMP();
                            bookingVehicletype.BookingId = reservation.BookingId;
                            bookingVehicletype.VehicleTypeId = reservation.VehicleTypeId;
                            bookingVehicletype.DistancePrice = reservation.DistancePrice;
                            bookingVehicletype.ModifiedDate = DateTime.UtcNow;
                            bookingVehicletype.CreatedDate = DateTime.UtcNow;
                            db.BookingVehicleType_TEMP.Add(bookingVehicletype);
                            db.SaveChanges();
                            var newBookingVehicle = new BookingVehicleDom();
                            newBookingVehicle.BookingVehicleTypeId = bookingVehicletype.BookingVehicleTypeId;
                            newBookingVehicle.VehicleTypeId = reservation.VehicleTypeId;
                            newBookingVehicle.DistancePrice = reservation.DistancePrice;
                            listBookingVehicletypes.Add(newBookingVehicle);
                       
                    if (reservation.JourneyTypeId == (int)JourneyTypeEnum.TwoWay)
                    {
                        if (reservation.PaymentMethodId != (int)PaymentTypeEnum.Free_Journey)
                        {
                            //  SaveJourneyPoints(booking);
                        }
                        returnFixedBookingId = SaveReturnBooking(db,  reservation,  returnJourney, out returnFixedBookingPaymentId, out listBookingVehicletypesReturn);
                    }
                    bool isSendSms = bool.Parse(ConfigurationManager.AppSettings["IsSendSms"]);
                    reservation.FinalPrice = reservation.OneWayPrice + reservation.ReturnWayPrice + reservation.BookingFee - (reservation.OneWayCredit + reservation.ReturnWayCredit);
                    var bookingPayment = new BookingPayment_TEMP();
                    bookingPayment.BookingId = newBooking.BookingId;
                    bookingPayment.PaymentTypeId = reservation.PaymentMethodId == (int)PaymentTypeEnum.Use_My_Credits && reservation.BookingFee > 0
                                                                  ? (int)PaymentTypeEnum.Card
                                                                  : reservation.PaymentMethodId;
                    bookingPayment.PaymentDate = DateTime.Now;
                    bookingPayment.PaymentAmount = reservation.OneWayPrice + reservation.BookingFee - (reservation.OneWayCredit);
                    bookingPayment.Status = (int)PaymentStatusEnum.Pending;
                    bookingPayment.IsDeleted = false;
                    bookingPayment.IsEnabled = true;
                    bookingPayment.sysModifiedDate = DateTime.UtcNow;
                    bookingPayment.sysCreatedDate = DateTime.UtcNow;
                    db.BookingPayment_TEMP.Add(bookingPayment);
                    db.SaveChanges();
                    if (isSendSms && ConfigHelper.IsSendSms(newBooking.Status))
                    {
                        SendSms(reservation);
                    }
                    SaveFixed(reservation,  bookingPayment.BookingPaymentId, db, returnFixedBookingId,  returnJourney, returnFixedBookingPaymentId, listBookingVehicletypes, listBookingVehicletypesReturn);
                    //Save new aria point 
                    #region Save Viapoints
                    if (reservation.ViaPointAddress != null && reservation.ViaPointAddress.Any(x => !x.Postcode.IsEmpty()))
                    {
                        var viaList = reservation.ViaPointAddress.Where(x => !x.Postcode.IsEmpty()).ToList();
                        int orderId = 0;
                        foreach (var item in viaList)
                        {
                            orderId++;
                            var viapoints = new JourneyViapoints_TEMP();
                            viapoints.BookingId = reservation.BookingId;
                            viapoints.ViaFullAddress = item.FullAddress;
                            viapoints.ViapointPostCode = item.Postcode;
                            viapoints.AreaPointId = item.AriaPoitId;
                            viapoints.OrderId = orderId;
                            viapoints.Lat = item.Lat;
                            viapoints.Long = item.Long;
                            viapoints.TitleId = null;
                            viapoints.PersonName = "";
                            viapoints.ContactNumber = "";
                            viapoints.PickupCharge = 0;
                            viapoints.DropoffCharge = 0;
                            viapoints.IsDeleted = false;
                            viapoints.CreatedDate = DateTime.UtcNow;
                            viapoints.ModifiedDate = DateTime.UtcNow;
                            viapoints.DeletedDate = null;
                            db.JourneyViapoints_TEMP.Add(viapoints);
                            db.SaveChanges();
                            item.ViapointId = viapoints.ViapointId;
                            var viapointsFixed = new JourneyViapoint();
                            viapointsFixed.ViapointId = viapoints.ViapointId;
                            viapointsFixed.BookingId = reservation.BookingId;
                            viapointsFixed.ViaFullAddress = item.FullAddress;
                            viapointsFixed.ViapointPostCode = item.Postcode;
                            viapointsFixed.AreaPointId = item.AriaPoitId;
                            viapointsFixed.OrderId = orderId;
                            viapointsFixed.Lat = item.Lat;
                            viapointsFixed.Long = item.Long;
                            viapointsFixed.TitleId = null;
                            viapointsFixed.PersonName = "";
                            viapointsFixed.ContactNumber = "";
                            viapointsFixed.RefViaPointId = null;
                            viapointsFixed.PickupCharge = 0;
                            viapointsFixed.DropoffCharge = 0;
                            viapointsFixed.IsDeleted = false;
                            viapointsFixed.CreatedDate = DateTime.UtcNow;
                            viapointsFixed.ModifiedDate = DateTime.UtcNow;
                            viapointsFixed.DeletedDate = null;
                            db.JourneyViapoints.Add(viapointsFixed);
                            db.SaveChanges();
                        }
                    }
                    #endregion
                    SaveOtherCharge(reservation, db);
                    SubtractCustomerCredits(reservation, db, returnFixedBookingId);
                    reservation.MapUrl = CreateGoogleImage( db, reservation);
                    if (reservation.IsVehicleUpgradeEnabeld)
                    {
                        UpdateVehicleUpdate(reservation);
                    }

                    if (!string.IsNullOrEmpty(reservation.UserId))
                    {
                        var customer = db.Customers.FirstOrDefault(x => x.userId.Equals(reservation.UserId));
                        if (customer != null)
                        {
                            reservation.Loyalty = Enum.GetName((typeof(LoyaltyType_Enum)), customer.loyaltyMemberType);
                            reservation.IsCABOperator = customer.IsCABOperator ?? false;
                            if (reservation.IsCABOperator)
                            {
                                var objOrganization = db.Organizations.FirstOrDefault(x => x.OrganizationId == reservation.BookingSiteId);
                                if (objOrganization != null)
                                {
                                    reservation.OfficePhoneNo = objOrganization.OfZPhoneNo;
                                }
                            }
                        }
                    }
                    if (reservation.PaymentMethodId == (int)PaymentTypeEnum.Free_Journey)
                    {
                        if (db.CustomerFreeJourneys != null &&
                            db.CustomerFreeJourneys.Any(
                                x => x.UserId.Equals(reservation.UserId) && x.Code.Equals(reservation.FreeJourneyCode) && !x.IsUsed && x.IsEnabled))
                        {
                            var freeJourney = db.CustomerFreeJourneys.First(
                                x =>
                                    x.UserId.Equals(reservation.UserId) && x.Code.Equals(reservation.FreeJourneyCode) &&
                                    !x.IsUsed && x.IsEnabled);
                            freeJourney.BookingId = reservation.BookingId;
                            freeJourney.IsEnabled = false;
                            db.SaveChanges();

                            var freejourneyPoints =
                                db.FreeJourneyPointsTypes.FirstOrDefault(
                                    x => x.PointsTypeId == (int)FreeJourneyPointsTypeEnum.FreeJourney);
                            if (freejourneyPoints != null)
                            {
                                var customer = db.Customers.FirstOrDefault(x => x.userId.Equals(reservation.UserId));
                                if (customer != null)
                                {
                                    var bookingPoint = new BookingPoint();
                                    bookingPoint.BookingId = reservation.BookingId;
                                    bookingPoint.PointsTypeId = (int)FreeJourneyPointsTypeEnum.FreeJourney;
                                    bookingPoint.Points = -freejourneyPoints.Points;
                                    bookingPoint.TotalPoints = customer.availablePoints;
                                    bookingPoint.IsDeleted = false;
                                    bookingPoint.IsEnabled = false;
                                    bookingPoint.sysCreatedDate = DateTime.UtcNow;
                                    bookingPoint.sysModifiedDate = DateTime.UtcNow;
                                    db.BookingPoints.Add(bookingPoint);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }

                    if (isSendToGhost)
                    {
                        var config = AutoCabHelper.GetAutoCabAgentConfig();
                        if (config != null)
                        {
                            #region Autocab  
                            //saved booking Temp table PaymentMethod
                            bool isSetPrice = bool.Parse(ConfigurationManager.AppSettings["IsSetPrice"]);
                            bool isWebPriceEnabled = bool.Parse(ConfigurationManager.AppSettings["IsWebPriceEnabled"]);
                            if (newBooking.PaymentMethod != (int)PaymentTypeEnum.Card)
                            {
                                AgentBookingAvailabilityResponse bookingAvailability = null;
                                AuthorizationResponse authorizationResponse = null;
                                if (reservation.PaymentMethodId == (int)PaymentTypeEnum.Account)
                                {
                                    decimal price = (reservation.OneWayPrice + reservation.BookingFee) - reservation.OneWayCredit;
                                    int distancePrice = price > 0 ? (int)price * 100 : 0;
                                    //bookingAvailability = isSetPrice
                                    //  ? AutoCapVehicletypeRequest.GetBookingAvailabilityMainSetPrice(address, reservation, distancePrice, config, reservation.GhostPaymentType, reservation.ContractReference)
                                    //  : AutoCapVehicletypeRequest.GetBookingAvailabilityMain(address, reservation, config);
                                    //authorizationResponse = AutoCapVehicletypeRequest.BookingAuthorizationRequest(reservation,
                                    //    bookingAvailability.AvailabilityReference, reservation.OneWayPrice, config);
                                }
                                else if (reservation.PaymentMethodId == (int)PaymentTypeEnum.Free_Journey)
                                {
                                    int distancePrice = reservation.OneWayPrice > 0 ? (int)(reservation.OneWayPrice + reservation.BookingFee) * 100 : 0;
                                    //bookingAvailability = isSetPrice
                                    //    ? AutoCapVehicletypeRequest.GetBookingAvailabilityFreeJourneyMainSetPrice(address, reservation, distancePrice, config)
                                    //    : AutoCapVehicletypeRequest.GetBookingAvailabilityFreeJourneyMain(address,
                                    //        address.BidReference, reservation, config);
                                    //authorizationResponse = AutoCapVehicletypeRequest.BookingAuthorizationRequest(reservation,
                                    //    bookingAvailability.AvailabilityReference, reservation.OneWayPrice, config);
                                }
                                else if (reservation.PaymentMethodId == (int)PaymentTypeEnum.Use_My_Credits)
                                {
                                    decimal price = (reservation.OneWayPrice + reservation.BookingFee) - reservation.OneWayCredit;
                                    int distancePrice = price > 0 ? (int)price * 100 : 0;
                                    //bookingAvailability = isSetPrice
                                    //    ? AutoCapVehicletypeRequest.GetBookingAvailabilityUseMyCreditsMainSetPrice(address, reservation, distancePrice, config)
                                    //    : AutoCapVehicletypeRequest.GetBookingAvailabilityUsMyCreditsMain(address,
                                    //        address.BidReference, reservation, config);
                                    //authorizationResponse = AutoCapVehicletypeRequest.BookingAuthorizationRequest(reservation,
                                    //    bookingAvailability.AvailabilityReference, reservation.OneWayPrice, config);
                                }
                                else
                                {
                                    int distancePrice = 0;
                                    var hasBookingFeeByCard = bool.Parse(ConfigurationManager.AppSettings["HasBookingFeeByCard"]);
                                    if (hasBookingFeeByCard)
                                    {
                                        distancePrice = reservation.OneWayPrice > 0
                                           ? (int)(reservation.OneWayPrice) * 100
                                           : 0;
                                    }
                                    else
                                    {
                                        distancePrice = reservation.OneWayPrice > 0
                                          ? (int)(reservation.OneWayPrice + reservation.BookingFee) * 100
                                          : 0;
                                    }
                                    //bookingAvailability = isSetPrice
                                    //    ? AutoCapVehicletypeRequest.GetBookingAvailabilityMainSetPrice(address, reservation, distancePrice, config, config.CashAccountPaymentType, config.CashAccountContractReference)
                                    //    : AutoCapVehicletypeRequest.GetBookingAvailabilityMain(address, reservation, config);
                                    //authorizationResponse = AutoCapVehicletypeRequest.BookingAuthorizationRequest(reservation,
                                    //    bookingAvailability.AvailabilityReference, reservation.OneWayPrice, config);
                                }
                                if (db.Booking_TEMP != null && authorizationResponse.AuthorizationReference != null &&
                                    db.Booking_TEMP.Any(x => x.BookingId == reservation.BookingId))
                                {
                                    var objBooking = db.Booking_TEMP.First(x => x.BookingId == reservation.BookingId);
                                    objBooking.Autocab_AuthorizationReference = authorizationResponse.AuthorizationReference;
                                    objBooking.Autocab_BookingReference = authorizationResponse.BookingReference;
                                    objBooking.VendorID = bookingAvailability.Vendor.Id;
                                    objBooking.AutocabNumber = authorizationResponse.AuthorizationReference != null &&
                                                               authorizationResponse.AuthorizationReference.Split('-')
                                                                   .Length > 0
                                        ? authorizationResponse.AuthorizationReference.Split('-')[2]
                                        : "";
                                    db.SaveChanges();
                                }
                                if (authorizationResponse.AuthorizationReference != null &&
                                    db.Bookings.Any(x => x.BookingId == reservation.BookingId))
                                {
                                    var objBooking = db.Bookings.First(x => x.BookingId == reservation.BookingId);
                                    objBooking.Autocab_AuthorizationReference = authorizationResponse.AuthorizationReference;
                                    objBooking.Autocab_BookingReference = authorizationResponse.BookingReference;
                                    objBooking.VendorID = bookingAvailability.Vendor.Id;
                                    objBooking.AutocabNumber = authorizationResponse.AuthorizationReference != null &&
                                                               authorizationResponse.AuthorizationReference.Split('-')
                                                                   .Length > 0
                                        ? authorizationResponse.AuthorizationReference.Split('-')[2]
                                        : "";
                                    db.SaveChanges();
                                }
                                if (reservation.JourneyTypeId == (int)JourneyTypeEnum.TwoWay)
                                {
                                    AuthorizationResponse authorizationResponseReturn = null;
                                    AgentBookingAvailabilityResponse bookingAvailabilityReturn = null;
                                    if (reservation.PaymentMethodId == (int)PaymentTypeEnum.Account)
                                    {
                                        int distancePrice = reservation.ReturnWayPrice > 0
                                           ? (int)reservation.ReturnWayPrice * 100
                                           : 0;
                                        //bookingAvailabilityReturn = isSetPrice
                                        //    ? AutoCapVehicletypeRequest.GetBookingAvailabilityReturnMainSetPrice(
                                        //        returnJourney,
                                        //        reservation, distancePrice, config, reservation.GhostPaymentType, reservation.ContractReference)
                                        //    : AutoCapVehicletypeRequest.GetBookingAvailabilityReturnMain(returnJourney,
                                        //        reservation,
                                        //        returnJourney.BidReference, config);
                                        //authorizationResponseReturn =
                                        //    AutoCapVehicletypeRequest.BookingAuthorizationRequestReturn(returnJourney,
                                        //        reservation,
                                        //        bookingAvailabilityReturn.AvailabilityReference, reservation.ReturnWayPrice, config);


                                    }
                                    else if (reservation.PaymentMethodId == (int)PaymentTypeEnum.Use_My_Credits)
                                    {
                                        decimal price = reservation.OneWayPrice - reservation.OneWayCredit;
                                        int distancePrice = price > 0 ? (int)price * 100 : 0;
                                        //bookingAvailabilityReturn = isSetPrice
                                        //    ? AutoCapVehicletypeRequest.GetBookingAvailabilityUseMyCreditsMainSetPriceReturn(
                                        //        returnJourney, reservation, distancePrice, config)
                                        //    : AutoCapVehicletypeRequest.GetBookingAvailabilityUsMyCreditsMain(address,
                                        //        address.BidReference, reservation, config);
                                        //authorizationResponseReturn =
                                        //    AutoCapVehicletypeRequest.BookingAuthorizationRequestReturn(returnJourney,
                                        //        reservation,
                                        //        bookingAvailabilityReturn.AvailabilityReference, reservation.ReturnWayPrice, config);
                                    }
                                    else
                                    {
                                        int distancePrice = reservation.ReturnWayPrice > 0
                                            ? (int)reservation.ReturnWayPrice * 100
                                            : 0;
                                        //bookingAvailabilityReturn = isSetPrice
                                        //    ? AutoCapVehicletypeRequest.GetBookingAvailabilityReturnMainSetPrice(
                                        //        returnJourney,
                                        //        reservation, distancePrice, config, config.CashAccountPaymentType, config.CashAccountContractReference)
                                        //    : AutoCapVehicletypeRequest.GetBookingAvailabilityReturnMain(returnJourney,
                                        //        reservation,
                                        //        returnJourney.BidReference, config);
                                        //authorizationResponseReturn =
                                        //    AutoCapVehicletypeRequest.BookingAuthorizationRequestReturn(returnJourney,
                                        //        reservation,
                                        //        bookingAvailabilityReturn.AvailabilityReference, reservation.ReturnWayPrice, config);
                                    }
                                    if (db.Booking_TEMP != null &&
                                        authorizationResponseReturn.AuthorizationReference != null &&
                                        db.Booking_TEMP.Any(x => x.BookingId == returnFixedBookingId))
                                    {
                                        var objBooking = db.Booking_TEMP.First(x => x.BookingId == returnFixedBookingId);
                                        objBooking.Autocab_AuthorizationReference =
                                            authorizationResponseReturn.AuthorizationReference;
                                        objBooking.Autocab_BookingReference = authorizationResponseReturn.BookingReference;
                                        objBooking.VendorID = bookingAvailabilityReturn.Vendor.Id;
                                        objBooking.AutocabNumber = authorizationResponseReturn.AuthorizationReference !=
                                                                   null &&
                                                                   authorizationResponseReturn.AuthorizationReference.Split(
                                                                       '-').Length > 0
                                            ? authorizationResponseReturn.AuthorizationReference.Split('-')[2]
                                            : "";
                                        db.SaveChanges();
                                    }
                                    if (db.Bookings != null && authorizationResponseReturn.AuthorizationReference != null &&
                                        db.Bookings.Any(x => x.BookingId == returnFixedBookingId))
                                    {
                                        var objBooking = db.Bookings.First(x => x.BookingId == returnFixedBookingId);
                                        objBooking.Autocab_AuthorizationReference =
                                            authorizationResponseReturn.AuthorizationReference;
                                        objBooking.Autocab_BookingReference = authorizationResponseReturn.BookingReference;
                                        objBooking.VendorID = bookingAvailabilityReturn.Vendor.Id;
                                        objBooking.AutocabNumber = authorizationResponseReturn.AuthorizationReference !=
                                                                   null &&
                                                                   authorizationResponseReturn.AuthorizationReference.Split(
                                                                       '-').Length > 0
                                            ? authorizationResponseReturn.AuthorizationReference.Split('-')[2]
                                            : "";
                                        db.SaveChanges();
                                    }
                                }
                            }

                            #endregion
                        }
                    }
                }
                SaveFavouriteAddresses(reservation);
                if (ConfigHelper.IsSendEmail(newBooking.Status))
                {
                    SendEmailAsync(reservation,  returnJourney);
                }
                if (newBooking.PaymentMethod == (int)PaymentTypeEnum.Card)
                {
                    if (reservation.IsSavedCardPayment)
                    {
                        savedBooking.FinalPrice = reservation.FinalPrice;
                        savedBooking.WorldpayToken = reservation.WorldpayToken;
                        savedBooking.Description = "Booking/" + Enum.GetName(typeof(BookingSource_Enum), reservation.BookingSource).ToString() + "/" + reservation.BookingNumber;
                        savedBooking.BookingToken = HttpContext.Current.Server.UrlEncode(bookingToken);
                        savedBooking.RedirectPaymentUrl = "/CardReuseable/CardReuse";
                    }
                    else
                    {
                        string description = "Booking/" + Enum.GetName(typeof(BookingSource_Enum), reservation.BookingSource).ToString() + "/" + reservation.BookingNumber;
                        string encodeDescription = HttpContext.Current.Server.UrlEncode(description);
                        string encodeToken = HttpContext.Current.Server.UrlEncode(bookingToken);
                        string path = string.Empty;
                        path = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                        //string url = HttpContext.Current.Request.Url.Host;
                        //  path = ConfigurationManager.AppSettings["url."+url];
                        // ErrorHandling.LogFileWriteWithOblect(url, path,"","","");
                        string callBackurl = HttpContext.Current.Server.UrlEncode(path + "/BookingSummary/Details?tkn=" + encodeToken);//"&amnt=" + booking.TotalPrice + "
                        string paymentUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.PaymentApi) + "Payment/CapturePayment" + "?tkn=" + encodeToken + "&amnt=" + reservation.FinalPrice + "&desc=" + encodeDescription + "&bnu=" + HttpContext.Current.Server.UrlEncode(reservation.BookingNumber) + "&cbu=" + callBackurl;
                        savedBooking.RedirectPaymentUrl = paymentUrl;
                    }
                    savedBooking.BookingNumber = reservation.BookingNumber;
                    savedBooking.BookingSource = Enum.GetName(typeof(BookingSource_Enum), reservation.BookingSource).ToString();
                    return savedBooking;
                }
                if (newBooking.PaymentMethod == (int)PaymentTypeEnum.Free_Journey && (reservation.OneWayPrice - reservation.FreeJourneyAccessAmt) > 0)
                {
                    decimal amt = reservation.OneWayPrice - reservation.FreeJourneyAccessAmt;
                    savedBooking.OneWayPrice = reservation.OneWayPrice;
                    savedBooking.FreeJourneyAccessAmt = reservation.FreeJourneyAccessAmt;
                    if (reservation.IsSavedCardPayment)
                    {
                        savedBooking.WorldpayToken = reservation.WorldpayToken;
                        savedBooking.Description = "FreeJourney/" + Enum.GetName(typeof(BookingSource_Enum), reservation.BookingSource).ToString() + "/" + reservation.BookingNumber;
                        savedBooking.BookingToken = HttpContext.Current.Server.UrlEncode(bookingToken);
                        savedBooking.RedirectPaymentUrl = "/CardReuseable/CardReuseFreeJourneyView";
                    }
                    else
                    {
                        string description = "FreeJourney/" + Enum.GetName(typeof(BookingSource_Enum), reservation.BookingSource).ToString() + "/" + reservation.BookingNumber;
                        string encodeDescription = HttpContext.Current.Server.UrlEncode(description);
                        string encodeToken = HttpContext.Current.Server.UrlEncode(bookingToken);
                        string path = string.Empty;
                        path = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                        //  string url = HttpContext.Current.Request.Url.Host;
                        //  path = ConfigurationManager.AppSettings["url." + url];
                        string callBackurl = HttpContext.Current.Server.UrlEncode(path + "/BookingSummary/FreejurneyPaymentView?tkn=" + encodeToken);//"&amnt=" + booking.TotalPrice + "
                        string paymentUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.PaymentApi) + "Payment/CaptureFreejourneyAccessFee" + "?tkn=" + encodeToken + "&amnt=" + amt + "&desc=" + encodeDescription + "&bnu=" + HttpContext.Current.Server.UrlEncode(reservation.BookingNumber) + "&cbu=" + callBackurl;
                        savedBooking.RedirectPaymentUrl = paymentUrl;
                    }
                }

                savedBooking.BookingNumber = reservation.BookingNumber;
                savedBooking.BookingSource = Enum.GetName(typeof(BookingSource_Enum), reservation.BookingSource).ToString();
                return savedBooking;
            }
            catch (Exception ex)
            {
                string reservationModel = XmlHelper.GetXMLFromObject(reservation);
                string addressxml = "";
                string vehicleCombinationxml ="";
                string returnJourneyxml = XmlHelper.GetXMLFromObject(returnJourney);
                string xml = "reservationModel \n" + reservationModel + "\n\n addressxml \n " + addressxml + "\n\n vehicleCombinationxml \n" + vehicleCombinationxml + "\n\nreturnJourneyxml\n" + returnJourneyxml;

                ErrorHandling.LogFileWriteWithOblect(xml, ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }
        public void SendSms(TelBookingViewModel booking)
        {
            try
            {
                string bookingToken = HttpContext.Current.Server.UrlEncode(booking.BookingToken);
                string bookingDetailApi = string.Empty;
                bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.BA);
                bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                 
                var listMobilenumber = new List<string>();
                string number = booking.BookedPersonMobileNumberCountryCode.Equals("44")
                    ? booking.BookedPersonMobileNo
                    : booking.BookedPersonMobileNumberCountryCode + booking.BookedPersonMobileNo;
                listMobilenumber.Add(number);
                var postData =
                    new
                    {
                        mobileNumbers = listMobilenumber.ToArray(),
                        url = bookingDetailApi,
                        bookingNumber = booking.BookingNumber
                    };
                string serialisedData = JsonConvert.SerializeObject(postData);
                string smsUrl = ApiUrlHelper.GetUrl((int)ApiUrlEnum.SmsBookingApi);
                if (!string.IsNullOrEmpty(smsUrl))
                {
                    // SmsHelper.SendSms(serialisedData, smsUrl);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }
        public long SaveReturnBooking(BATransferEntities db,  TelBookingViewModel reservation,  ReturnJourney returnJourney, out long bookingPaymentId, out List<BookingVehicleDom> bookingVehicleList)
        {
            bookingPaymentId = 0;
            bookingVehicleList = new List<BookingVehicleDom>();
            var cruiseShipLandingDateTime = !string.IsNullOrEmpty(returnJourney.LandingDateCruiseShip) && !string.IsNullOrEmpty(returnJourney.LandingTimeCruiseShip)
                                  ? ValueParseHelper.TryParseDateTime(returnJourney.LandingDateCruiseShip + " " + returnJourney.LandingTimeCruiseShip) : (DateTime?)null;
            var flightLandingDateTime = !string.IsNullOrEmpty(returnJourney.LandingDateFlight) && !string.IsNullOrEmpty(returnJourney.LandingTimeFlight)
                                 ? ValueParseHelper.TryParseDateTime(returnJourney.LandingDateFlight + " " + returnJourney.LandingTimeFlight) : (DateTime?)null;
            #region Booking_TEMP
            Booking_TEMP newBooking = new Booking_TEMP()
            {
                UserId = reservation.UserId,
                CompanyId = reservation.CompanyId > 0 ? reservation.CompanyId : null,
                //relevantAspNetUser.IsCompanyUser ? relevantAspNetUser.CompanyId : 0,
                BookingToken = reservation.BookingToken,
                //Status = booking.CardPaymentTypeId != null && booking.CardPaymentTypeId == (int)CardPaymentTypeEnum.PayOverThePhone
                //        ? (int)BookingStatusEnum.Hold
                //        : (int)BookingStatusEnum.Requested,
                Status = (int)BookingStatusEnum.Requested,
                // isBookingConfirmationUponRequestBooking ? (int)BookingStatusEnum.Confirmed :
                JourneyType = reservation.JourneyTypeId,
                BookingDeuTime = reservation.BookingDeuTimeReturn,
                IsMultipleBooking = false,
                MultipleBookingId = string.Empty,
                RateId = reservation.RateId,
                CostCenterId = null,
                CostCodeId = null,
                NumberOfPassengers = reservation.NumOfPassengers,
                DespatchPref = "",
                FromTown = returnJourney.FromPostTown ?? "",
                BookingFrom = returnJourney.PickupPoint,
                FromLat = returnJourney.FromLat,
                FromLong = returnJourney.FromLong,
                ToTown = returnJourney.ToPostTown ?? "",
                BookingTo = returnJourney.DropoffPoint,
                ToLat = returnJourney.ToLat,
                ToLong = returnJourney.ToLong,
                FromFixedPriceAriaPointId = returnJourney.FromFixedPriceAriaPointId,
                ToFixedPriceAriaPointId = returnJourney.ToFixedPriceAriaPointId,
                PassengerTitleId = reservation.TitleId,
                PassengerName = !string.IsNullOrEmpty(reservation.PassengerFirstName)
                                        ? reservation.PassengerFirstName + " " + reservation.PassengerLastName
                                        : "",
                PassengerTel = reservation.PassengerMobileNumberCountryCode + reservation.PassengerMobileNo,
                PassengerEmail = reservation.PassengerEmail,
                BookedPersonTitleId = reservation.BookedPersonTitleId,
                BookedPersonName = reservation.BookedPersonFirstName + " " + reservation.BookedPersonLastName,
                BookedPersonTel = reservation.BookedPersonMobileNumberCountryCode + reservation.BookedPersonMobileNo,
                BookedPersonEmail = reservation.BookedPersonEmail,
                CustomerComment = reservation.CustomerComment ?? "",
                AlternativePhoneNumber = alternativeNo ?? "",
                DriverComment = string.Empty,
                OfficeComment = string.Empty,
                Alert = string.Empty,
                BookingFromFullPostCode = returnJourney.FromPostCode ?? "",
                BookingToFullPostCode = returnJourney.ToPostCode ?? "",
                CustomerType = ((int)CustomerTypeEnum.Member),
                // PaymentMethod = booking.PaymentMethodId,
                PaymentMethod = reservation.PaymentMethodId == (int)PaymentTypeEnum.Use_My_Credits && reservation.BookingFee > 0
                                                                    ? (int)PaymentTypeEnum.Card
                                                                    : reservation.PaymentMethodId,
                CardTypeId = null,
                //  CardTypeId = booking.CardTypeId != 0 ? booking.CardTypeId : null,
                CardPaymentTypeId = reservation.CardPaymentTypeId != 0 ? reservation.CardPaymentTypeId : null,
                IsSpeedCalculation = reservation.IsSpeedCalculation,
                TotalTime = reservation.TotalTime,
                TotalDistance = reservation.TotalDistance,
                BookingSource = reservation.BookingSource,
                IpAddress = reservation.IpAddress,
                OperatorId = 0,
                PCMacAddress = "BA Web",
                CreatedDate = DateTime.UtcNow,
                ModifiedDate = DateTime.UtcNow,
                //  PaymentStatus = (int)PaymentStatusEnum.Pending,
                //   PaymentStatus = reservation.PaymentStatusId,
                PaymentStatus = reservation.PaymentMethodId == (int)PaymentTypeEnum.Cash
                                        ? (int)PaymentStatusEnum.BookingFeePending
                                        : reservation.PaymentStatusId,
                ReceiptNumber = string.Empty,
                IsSendSmsUponArrival = true, // request.IsSendSmsUponArrival,
                IsSendSmsUponDispatch = true, //request.IsSendSmsUponDispatch,
                IsSendSmsUponFinish = true, // request.IsSendSmsUponFinish,
                IsSendSmsUponConfirmation = true, // request.IsSendSmsUponConfirmation,
                BookingNumber = reservation.BookingNumber,
                BookingDateTime = returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Airport
                                ? ValueParseHelper.TryParseDateTime(returnJourney.LandingDateFlight + " " + returnJourney.LandingTimeFlight)
                                : returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport
                                ? ValueParseHelper.TryParseDateTime(returnJourney.LandingDateCruiseShip + " " + returnJourney.LandingTimeCruiseShip)
                                : ValueParseHelper.TryParseDateTime(returnJourney.BookingDate + " " + returnJourney.BookingTime),
                FlightNo = returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Airport
                           ? returnJourney.FlightNo
                           : returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport
                           ? returnJourney.CruiseShipName : "",
                DriverDisplayName = returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Airport
                                   ? returnJourney.DisplayNameFlight ?? ""
                                   : returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport
                                   ? returnJourney.DisplayNameCruiseShip ?? "" : "",
                DriverEllapseTime = returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Airport
                                   ? returnJourney.FlightDriverEllapseTime
                                   : returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport
                                   ? returnJourney.CruiseShipDriverEllapseTime : 0,
                FlightLandingDateTime = returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Airport
                               ? flightLandingDateTime
                               : returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport
                               ? cruiseShipLandingDateTime : null,
                MapUrl = "",
                IsSourceBooking = false,
                IsReturnBooking = true,
                ReferenceBookingId = reservation.BookingId,
                Country = reservation.CountryName ?? "",
                CancelBookingId = null,
                FromAriaPointTypeId = returnJourney.FromAriapointTypeId,
                ToAriaPointTypeId = returnJourney.ToAriapointTypeId,
                ReservationDateTime = ValueParseHelper.TryParseDateTime(reservation.ClientDatetTime),
                Luggage = reservation.NumberOfLuggages,
                HandLuggage = reservation.NumberOfHandLuggages,
                BoosterSeat = reservation.BoosterSeat,
                InfantSeats = reservation.InfantSeats,
                ChildSeats = reservation.ChildSeats,
                IsSendPassengerEmail = reservation.IsSendPassengerEmail,
                IsSendPassengerSms = reservation.IsSendPassengerSms,
                Autocab_AuthorizationReference = "",
                Autocab_BookingReference = "",
                Autocab_IsBookingCancelled = false,
                VendorID = "",
                RefundAmount = 0,
                JourneyClass = 1,
                TotalDistancePrice = reservation.ReturnWayPrice,
                OtherCharges = 0,
                TotalPrice = reservation.ReturnWayPrice,
                StudentDiscount = returnJourney.StudentDiscountAmount,
                CustomerCredit = 0,
                ConsumerDebit = reservation.ReturnWayCredit,
                FinalPrice = reservation.ReturnWayPrice - reservation.ReturnWayCredit,
                PickupCharge = returnJourney.PickupCharge,
                DropoffCharge = returnJourney.DropoffCharge,
                Surcharge = returnJourney.ReturnSurcharge,
                IsFreeJourneyAccessAmt = false,
                FreeJourneyAccessAmt = 0,
                UsedPoints = 0,
                VehicleUpgradeAmt = 0,
                IsUpgradeByPoints = false,
                IsVehicleUpgradeEnabeld = false,
                IsBookingFeePaid = false
            };
            #endregion
            db.Booking_TEMP.Add(newBooking);
            db.SaveChanges();
            long newBookingId = newBooking.BookingId;
            
                    var bookingVehicletype = new BookingVehicleType_TEMP();
                    bookingVehicletype.BookingId = newBooking.BookingId;
                    bookingVehicletype.VehicleTypeId = reservation.VehicleTypeId;
                    bookingVehicletype.DistancePrice = reservation.DistancePrice;
                    bookingVehicletype.ModifiedDate = DateTime.UtcNow;
                    bookingVehicletype.CreatedDate = DateTime.UtcNow;
                    db.BookingVehicleType_TEMP.Add(bookingVehicletype);
                    db.SaveChanges();
        //    reservation.BookingVehicleTypeId = bookingVehicletype.BookingVehicleTypeId;
                    var newBookingVehicle = new BookingVehicleDom();
                    newBookingVehicle.BookingVehicleTypeId = bookingVehicletype.BookingVehicleTypeId;
                    newBookingVehicle.VehicleTypeId = reservation.VehicleTypeId;
                    newBookingVehicle.DistancePrice = reservation.DistancePrice;
                    bookingVehicleList.Add(newBookingVehicle);
             
            var bookingPayment = new BookingPayment_TEMP();
            bookingPayment.BookingId = newBookingId;
            bookingPayment.PaymentTypeId = reservation.PaymentMethodId;
            bookingPayment.PaymentDate = DateTime.Now;
            bookingPayment.PaymentAmount = reservation.ReturnWayPrice - reservation.ReturnWayCredit;
            bookingPayment.Status = (int)PaymentStatusEnum.Pending;
            bookingPayment.IsDeleted = false;
            bookingPayment.IsEnabled = true;
            bookingPayment.sysModifiedDate = DateTime.UtcNow;
            bookingPayment.sysCreatedDate = DateTime.UtcNow;
            db.BookingPayment_TEMP.Add(bookingPayment);
            db.SaveChanges();
            bookingPaymentId = bookingPayment.BookingPaymentId;
            if (db.Booking_TEMP.Any(x => x.BookingId == reservation.BookingId))
            {
                var returnBooking = db.Booking_TEMP.First(x => x.BookingId == reservation.BookingId);
                returnBooking.ReferenceBookingId = newBooking.BookingId;
                db.SaveChanges();
            }
            return newBooking.BookingId;
        }
        public void SaveFixedReturnBooking(BATransferEntities db,  TelBookingViewModel reservation, ReturnJourney returnJourney, List<BookingVehicleDom> bookingVehicleListReturn, long returnFixedBookingId, long bookingPaymentId)
        {
            try
            {
                var cruiseShipLandingDateTime = !string.IsNullOrEmpty(returnJourney.LandingDateCruiseShip) && !string.IsNullOrEmpty(returnJourney.LandingTimeCruiseShip)
                                      ? ValueParseHelper.TryParseDateTime(returnJourney.LandingDateCruiseShip + " " + returnJourney.LandingTimeCruiseShip) : (DateTime?)null;
                var flightLandingDateTime = !string.IsNullOrEmpty(returnJourney.LandingDateFlight) && !string.IsNullOrEmpty(returnJourney.LandingTimeFlight)
                                     ? ValueParseHelper.TryParseDateTime(returnJourney.LandingDateFlight + " " + returnJourney.LandingTimeFlight) : (DateTime?)null;
                Booking newFixedReturnBooking = new Booking()
                {
                    #region Return Booking
                    BookingId = returnFixedBookingId,
                    UserId = reservation.UserId,
                    CompanyId = reservation.CompanyId > 0 ? reservation.CompanyId : null,
                    //relevantAspNetUser.IsCompanyUser ? relevantAspNetUser.CompanyId : 0,
                    BookingToken = reservation.BookingToken,
                    //Status = booking.CardPaymentTypeId != null && booking.CardPaymentTypeId == (int)CardPaymentTypeEnum.PayOverThePhone
                    //    ? (int)BookingStatusEnum.Hold
                    //    : (int)BookingStatusEnum.Requested,
                    Status = (int)BookingStatusEnum.Requested,
                    // isBookingConfirmationUponRequestBooking ? (int)BookingStatusEnum.Confirmed :
                    JourneyType = reservation.JourneyTypeId,
                    BookingDeuTime = reservation.BookingDeuTimeReturn,
                    IsMultipleBooking = false,
                    MultipleBookingId = string.Empty,
                    RateId = reservation.RateId,
                    CostCenterId = null,
                    CostCodeId = null,
                    NumberOfPassengers = reservation.NumOfPassengers,
                    DespatchPref = "",
                    FromTown = returnJourney.FromPostTown ?? "",
                    BookingFrom = returnJourney.PickupPoint,
                    FromLat = returnJourney.FromLat,
                    FromLong = returnJourney.FromLong,
                    ToTown = returnJourney.ToPostTown ?? "",
                    BookingTo = returnJourney.DropoffPoint,
                    ToLat = returnJourney.ToLat,
                    ToLong = returnJourney.ToLong,
                    FromFixedPriceAriaPointId = returnJourney.FromFixedPriceAriaPointId,//returnJourney.FromAddressId,//
                    ToFixedPriceAriaPointId = returnJourney.ToFixedPriceAriaPointId,//returnJourney.ToAddressId,//

                    PassengerTitleId = reservation.TitleId,
                    PassengerName = !string.IsNullOrEmpty(reservation.PassengerFirstName)
                                        ? reservation.PassengerFirstName + " " + reservation.PassengerLastName
                                        : "",
                    PassengerTel = reservation.PassengerMobileNumberCountryCode + reservation.PassengerMobileNo,
                    PassengerEmail = reservation.PassengerEmail,
                    BookedPersonTitleId = reservation.BookedPersonTitleId,
                    BookedPersonName = reservation.BookedPersonFirstName + " " + reservation.BookedPersonLastName,
                    BookedPersonTel = reservation.BookedPersonMobileNumberCountryCode + reservation.BookedPersonMobileNo,
                    BookedPersonEmail = reservation.BookedPersonEmail,
                    CustomerComment = reservation.CustomerComment ?? "",
                    AlternativePhoneNumber = alternativeNo ?? "",
                    DriverComment = string.Empty,
                    OfficeComment = string.Empty,
                    Alert = string.Empty,
                    BookingFromFullPostCode = returnJourney.FromPostCode ?? "",
                    BookingToFullPostCode = returnJourney.ToPostCode ?? "",
                    CustomerType = ((int)CustomerTypeEnum.Member),
                    // PaymentMethod = booking.PaymentMethodId,
                    PaymentMethod = reservation.PaymentMethodId == (int)PaymentTypeEnum.Use_My_Credits && reservation.BookingFee > 0
                                                                    ? (int)PaymentTypeEnum.Card
                                                                    : reservation.PaymentMethodId,
                    CardTypeId = null,
                    //  CardTypeId = booking.CardTypeId != 0 ? booking.CardTypeId : null,
                    CardPaymentTypeId = reservation.CardPaymentTypeId != 0 ? reservation.CardPaymentTypeId : null,
                    IsSpeedCalculation = reservation.IsSpeedCalculation,
                    TotalTime = reservation.TotalTime,
                    TotalDistance = reservation.TotalDistance,
                    BookingSource = reservation.BookingSource,
                    IpAddress = reservation.IpAddress,
                    OperatorId = 0,
                    PCMacAddress = "BA Web",
                    CreatedDate = DateTime.UtcNow,
                    ModifiedDate = DateTime.UtcNow,
                    // PaymentStatus = reservation.PaymentStatusId,
                    PaymentStatus = reservation.PaymentMethodId == (int)PaymentTypeEnum.Cash
                                        ? (int)PaymentStatusEnum.BookingFeePending
                                        : reservation.PaymentStatusId,
                    ReceiptNumber = string.Empty,
                    IsSendSmsUponArrival = true, // request.IsSendSmsUponArrival,
                    IsSendSmsUponDispatch = true, //request.IsSendSmsUponDispatch,
                    IsSendSmsUponFinish = true, // request.IsSendSmsUponFinish,
                    IsSendSmsUponConfirmation = true, // request.IsSendSmsUponConfirmation,
                    BookingNumber = reservation.BookingNumber,
                    BookingDateTime = returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Airport
                                     ? ValueParseHelper.TryParseDateTime(returnJourney.LandingDateFlight + " " + returnJourney.LandingTimeFlight)
                                     : returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport
                                    ? ValueParseHelper.TryParseDateTime(returnJourney.LandingDateCruiseShip + " " + returnJourney.LandingTimeCruiseShip)
                                    : ValueParseHelper.TryParseDateTime(returnJourney.BookingDate + " " + returnJourney.BookingTime),
                    FlightNo = returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Airport
                               ? returnJourney.FlightNo
                               : returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport
                               ? returnJourney.CruiseShipName : "",
                    DriverDisplayName = returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Airport
                                       ? returnJourney.DisplayNameFlight ?? ""
                                       : returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport
                                       ? returnJourney.DisplayNameCruiseShip ?? "" : "",
                    DriverEllapseTime = returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Airport
                                   ? returnJourney.FlightDriverEllapseTime
                                   : returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport
                                   ? returnJourney.CruiseShipDriverEllapseTime : 0,
                    FlightLandingDateTime = returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Airport
                               ? flightLandingDateTime
                               : returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport
                               ? cruiseShipLandingDateTime : null,
                    MapUrl = "",
                    IsSourceBooking = false,
                    IsReturnBooking = true,
                    ReferenceBookingId = reservation.BookingId,
                    Country = reservation.CountryName ?? "",
                    CancelBookingId = null,
                    FromAriaPointTypeId = returnJourney.FromAriapointTypeId,
                    ToAriaPointTypeId = returnJourney.ToAriapointTypeId,
                    ReservationDateTime = ValueParseHelper.TryParseDateTime(reservation.ClientDatetTime),
                    Luggage = reservation.NumberOfLuggages,
                    HandLuggage = reservation.NumberOfHandLuggages,
                    BoosterSeat = reservation.BoosterSeat,
                    InfantSeats = reservation.InfantSeats,
                    ChildSeats = reservation.ChildSeats,
                    IsSendPassengerEmail = reservation.IsSendPassengerEmail,
                    IsSendPassengerSms = reservation.IsSendPassengerSms,
                    Autocab_AuthorizationReference = "",
                    Autocab_BookingReference = "",
                    Autocab_IsBookingCancelled = false,
                    VendorID = "",
                    RefundAmount = 0,
                    JourneyClass = 1,
                    TotalDistancePrice = reservation.ReturnWayPrice,
                    OtherCharges = 0,
                    TotalPrice = reservation.ReturnWayPrice,
                    StudentDiscount = returnJourney.StudentDiscountAmount,
                    CustomerCredit = 0,
                    ConsumerDebit = reservation.ReturnWayCredit,
                    FinalPrice = reservation.ReturnWayPrice - reservation.ReturnWayCredit,
                    PickupCharge = returnJourney.PickupCharge,
                    DropoffCharge = returnJourney.DropoffCharge,
                    Surcharge = returnJourney.ReturnSurcharge,
                    IsFreeJourneyAccessAmt = false,
                    FreeJourneyAccessAmt = 0,
                    UsedPoints = 0,
                    VehicleUpgradeAmt = 0,
                    IsUpgradeByPoints = false,
                    IsVehicleUpgradeEnabeld = false,
                    IsBookingFeePaid = false,
                    LoginId = reservation.LoginId,
                    AccountFee = reservation.PaymentMethodId == (int)PaymentTypeEnum.Account ? returnJourney.AccountFee : 0
                    #endregion
                };
                db.Bookings.Add(newFixedReturnBooking);
                db.SaveChanges();
                if (db.Bookings.Any(x => x.BookingId == reservation.BookingId))
                {
                    var returnBooking = db.Bookings.First(x => x.BookingId == reservation.BookingId);
                    returnBooking.ReferenceBookingId = newFixedReturnBooking.BookingId;
                    db.SaveChanges();
                }
                if (bookingVehicleListReturn != null && bookingVehicleListReturn.Count > 0)
                {
                    foreach (var vehicle in bookingVehicleListReturn)
                    {
                        var bookingVehicletypeFixed = new BookingVehicleType();
                        bookingVehicletypeFixed.BookingVehicleTypeId = vehicle.BookingVehicleTypeId;
                        bookingVehicletypeFixed.BookingId = newFixedReturnBooking.BookingId;
                        bookingVehicletypeFixed.VehicleTypeId = vehicle.VehicleTypeId;
                        bookingVehicletypeFixed.DistancePrice = vehicle.DistancePrice;
                        bookingVehicletypeFixed.ModifiedDate = DateTime.UtcNow;
                        bookingVehicletypeFixed.CreatedDate = DateTime.UtcNow;
                        bookingVehicletypeFixed.CustomerRating = 0;
                        bookingVehicletypeFixed.CustomerComment = "";
                        bookingVehicletypeFixed.DriverRating = 0;
                        bookingVehicletypeFixed.DriverComment = "";
                        db.BookingVehicleTypes.Add(bookingVehicletypeFixed);
                        db.SaveChanges();
                    }
                }
                var bookingPaymetFixed = new BookingPayment();
                bookingPaymetFixed.BookingPaymentId = bookingPaymentId;
                bookingPaymetFixed.BookingId = newFixedReturnBooking.BookingId;
                bookingPaymetFixed.PaymentTypeId = reservation.PaymentMethodId;
                bookingPaymetFixed.PaymentAmount = reservation.ReturnWayPrice - reservation.ReturnWayCredit;
                bookingPaymetFixed.PaymentDate = DateTime.Now;
                bookingPaymetFixed.Status = (int)PaymentStatusEnum.Pending;
                bookingPaymetFixed.IsDeleted = false;
                bookingPaymetFixed.IsEnabled = true;
                bookingPaymetFixed.sysCreatedDate = DateTime.UtcNow;
                bookingPaymetFixed.sysModifiedDate = DateTime.UtcNow;
                db.BookingPayments.Add(bookingPaymetFixed);
                db.SaveChanges();
                if (returnJourney.ViaPointAddress != null && returnJourney.ViaPointAddress.Any(x => !x.Postcode.IsEmpty()))
                {
                    var viaList = returnJourney.ViaPointAddress.Where(x => !x.Postcode.IsEmpty()).ToList();
                    int orderId = 0;
                    foreach (var item in viaList)
                    {
                        orderId++;
                        var viapoints = new JourneyViapoints_TEMP();
                        viapoints.BookingId = newFixedReturnBooking.BookingId;
                        viapoints.ViaFullAddress = item.FullAddress;
                        viapoints.ViapointPostCode = item.Postcode;
                        viapoints.AreaPointId = item.AriaPoitId;
                        viapoints.OrderId = orderId;
                        viapoints.Lat = item.Lat;
                        viapoints.Long = item.Long;
                        viapoints.TitleId = null;
                        viapoints.PersonName = "";
                        viapoints.ContactNumber = "";
                        viapoints.PickupCharge = 0;
                        viapoints.DropoffCharge = 0;
                        viapoints.IsDeleted = false;
                        viapoints.CreatedDate = DateTime.UtcNow;
                        viapoints.ModifiedDate = DateTime.UtcNow;
                        viapoints.DeletedDate = null;
                        db.JourneyViapoints_TEMP.Add(viapoints);
                        db.SaveChanges();
                        item.ViapointId = viapoints.ViapointId;
                        var viapointsFixed = new JourneyViapoint();
                        viapointsFixed.ViapointId = viapoints.ViapointId;
                        viapointsFixed.BookingId = newFixedReturnBooking.BookingId;
                        viapointsFixed.ViaFullAddress = item.FullAddress;
                        viapointsFixed.ViapointPostCode = item.Postcode;
                        viapointsFixed.AreaPointId = item.AriaPoitId;
                        viapointsFixed.OrderId = orderId;
                        viapointsFixed.Lat = item.Lat;
                        viapointsFixed.Long = item.Long;
                        viapointsFixed.TitleId = null;
                        viapointsFixed.PersonName = "";
                        viapointsFixed.ContactNumber = "";
                        viapointsFixed.RefViaPointId = null;
                        viapointsFixed.PickupCharge = 0;
                        viapointsFixed.DropoffCharge = 0;
                        viapointsFixed.IsDeleted = false;
                        viapointsFixed.CreatedDate = DateTime.UtcNow;
                        viapointsFixed.ModifiedDate = DateTime.UtcNow;
                        viapointsFixed.DeletedDate = null;
                        db.JourneyViapoints.Add(viapointsFixed);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }
        public void SaveFixed(TelBookingViewModel reservation,  long bookingPaymentId, BATransferEntities db,
            long returnFixedBookingId,  ReturnJourney returnJourney, long returnFixedBookingPaymentId, List<BookingVehicleDom> listBookingVehicletypes, List<BookingVehicleDom> bookingVehicleListReturn)
        {
            #region Booking
            Booking newFixedBooking = new Booking()
            {
                BookingId = reservation.BookingId,
                UserId = reservation.UserId,
                CompanyId = reservation.CompanyId > 0 ? reservation.CompanyId : null,
                BookingToken = reservation.BookingToken,
                //Status = booking.CardPaymentTypeId != null && booking.CardPaymentTypeId == (int)CardPaymentTypeEnum.PayOverThePhone
                //        ? (int)BookingStatusEnum.Hold
                //        : (int)BookingStatusEnum.Requested,
                Status = (int)BookingStatusEnum.Requested,
                JourneyType = reservation.JourneyTypeId,
                //BookingDispatchTime = ValueParseHelper.TryParseDate(request.BookingDispatchTime),
                BookingDateTime = (!string.IsNullOrEmpty(reservation.LandingDate) && (reservation.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport || reservation.FromAriapointTypeId == (int)AreapointTypeEnum.Airport)) ? ValueParseHelper.TryParseDateTime(reservation.LandingDate + " " + reservation.LandingTime) : ValueParseHelper.TryParseDateTime(reservation.BookingDateTime),
                BookingDeuTime = reservation.BookingDeuTime,
                IsMultipleBooking = false,
                MultipleBookingId = string.Empty,
                RateId = reservation.RateId,
                CostCenterId = null,
                CostCodeId = null,
                NumberOfPassengers = reservation.NumOfPassengers,
                DespatchPref = reservation.PickupNote ?? "",
                FromTown = reservation.FromPostTown ?? "",
                BookingFrom = reservation.BookingFrom ?? "",
                ToTown = reservation.ToPostTown ?? "",
                BookingTo = reservation.BookingTo,
                PassengerTitleId = reservation.TitleId,
                PassengerName = !string.IsNullOrEmpty(reservation.PassengerFirstName)
                                        ? reservation.PassengerFirstName + " " + reservation.PassengerLastName
                                        : "",
                PassengerTel = reservation.PassengerMobileNumberCountryCode + reservation.PassengerMobileNo,
                PassengerEmail = reservation.PassengerEmail,
                BookedPersonTitleId = reservation.BookedPersonTitleId,
                BookedPersonName = reservation.BookedPersonFirstName + " " + reservation.BookedPersonLastName,
                BookedPersonTel = reservation.BookedPersonMobileNumberCountryCode + reservation.BookedPersonMobileNo,
                BookedPersonEmail = reservation.BookedPersonEmail,
                CustomerComment = reservation.CustomerComment ?? "",
                AlternativePhoneNumber = alternativeNo ?? "",
                DriverComment = string.Empty,
                OfficeComment = string.Empty,
                Alert = string.Empty,
                BookingFromFullPostCode = reservation.FromPostCode ?? "",
                BookingToFullPostCode = reservation.ToPostCode ?? "",
                FromLat = reservation.FromLat,
                FromLong = reservation.FromLong,
                ToLat = reservation.ToLat,
                ToLong = reservation.ToLong,
                CustomerType = ((int)CustomerTypeEnum.Member),
                ToFixedPriceAriaPointId = reservation.ToFixedPriceAriaPointId,// reservation.ToAddressId,//
                FromFixedPriceAriaPointId = reservation.FromFixedPriceAriaPointId,//reservation.FromAddressId,//
                CardTypeId = null,
                // CardTypeId = booking.CardTypeId != 0 ? booking.CardTypeId : null,
                CardPaymentTypeId = reservation.CardPaymentTypeId != 0 ? reservation.CardPaymentTypeId : null,
                IsSpeedCalculation = reservation.IsSpeedCalculation,
                TotalTime = reservation.TotalTime,
                TotalDistance = reservation.TotalDistance,
                PaymentMethod = reservation.PaymentMethodId == (int)PaymentTypeEnum.Use_My_Credits && reservation.BookingFee > 0
                                                                    ? (int)PaymentTypeEnum.Card
                                                                    : reservation.PaymentMethodId,

                BookingSource = reservation.BookingSource,
                IpAddress = reservation.IpAddress,
                OperatorId = 0,
                PCMacAddress = "BA Web",
                CreatedDate = DateTime.UtcNow,
                ModifiedDate = DateTime.UtcNow,
                PaymentStatus = reservation.PaymentMethodId == (int)PaymentTypeEnum.Cash
                                        ? (int)PaymentStatusEnum.BookingFeePending
                                        : reservation.PaymentStatusId,
                ReceiptNumber = string.Empty,
                IsSendSmsUponArrival = true,// request.IsSendSmsUponArrival,
                IsSendSmsUponDispatch = true,//request.IsSendSmsUponDispatch,
                IsSendSmsUponFinish = true,// request.IsSendSmsUponFinish,
                IsSendSmsUponConfirmation = true,// request.IsSendSmsUponConfirmation,
                BookingNumber = reservation.BookingNumber,
                FlightNo = reservation.FromAriapointTypeId == (int)AreapointTypeEnum.Airport
                          ? reservation.FlightNo
                          : reservation.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport
                          ? reservation.CruiseShipName : "",
                DriverDisplayName = reservation.FromAriapointTypeId == (int)AreapointTypeEnum.Airport
                              ? reservation.DisplayNameFlight ?? ""
                              : reservation.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport
                              ? reservation.DisplayNameCruiseShip ?? "" : "",
                DriverEllapseTime = reservation.DriverEllapseTime,
                FlightLandingDateTime = reservation.LandingDateTime,
                MapUrl = "",
                IsSourceBooking = reservation.JourneyTypeId == (int)JourneyTypeEnum.TwoWay,
                Country = reservation.CountryName ?? "",
                CancelBookingId = null,
                FromAriaPointTypeId = reservation.FromAriapointTypeId,
                ToAriaPointTypeId = reservation.ToAriapointTypeId,
                ReservationDateTime = ValueParseHelper.TryParseDateTime(reservation.ClientDatetTime),
                Luggage = reservation.NumberOfLuggages,
                HandLuggage = reservation.NumberOfHandLuggages,
                BoosterSeat = reservation.BoosterSeat,
                InfantSeats = reservation.InfantSeats,
                ChildSeats = reservation.ChildSeats,
                IsSendPassengerEmail = reservation.IsSendPassengerEmail,
                IsSendPassengerSms = reservation.IsSendPassengerSms,
                Autocab_AuthorizationReference = "",
                Autocab_BookingReference = "",
                Autocab_IsBookingCancelled = false,
                VendorID = "",
                TotalDistancePrice = reservation.OneWayPrice,
                OtherCharges = reservation.BookingFee,
                TotalPrice = (reservation.OneWayPrice + reservation.BookingFee),
                RefundAmount = 0,
                JourneyClass = 1,
                StudentDiscount = reservation.StudentDiscountAmount,
                CustomerCredit = 0,
                ConsumerDebit = reservation.OneWayCredit,
                FinalPrice = (reservation.OneWayPrice + reservation.BookingFee) - reservation.OneWayCredit,
                Surcharge = reservation.Surcharge,
                PickupCharge = reservation.PickupCharge,
                DropoffCharge = reservation.DropoffCharge,
                AddressSearchId = reservation.AddressSearchId == 0 ? null : reservation.AddressSearchId,
                IsFreeJourneyAccessAmt = !(reservation.FreeJourneyAccessAmt > 0),
                FreeJourneyAccessAmt = reservation.FreeJourneyAccessAmt,
                UsedPoints = reservation.UpgradePoints,
                VehicleUpgradeAmt = reservation.UpgradeAmount,
                IsUpgradeByPoints = reservation.IsVehicleUpgradeEnabeld,//reservation.IsUpgradeByPoints,
                IsVehicleUpgradeEnabeld = reservation.IsVehicleUpgradeEnabeld,
                IsBookingFeePaid = false,
                LoginId = reservation.LoginId,
                AccountFee = reservation.PaymentMethodId == (int)PaymentTypeEnum.Account ? reservation.AccountFee : 0
            };
            #endregion
            db.Bookings.Add(newFixedBooking);
            db.SaveChanges();
            if (listBookingVehicletypes != null && listBookingVehicletypes.Count > 0)
            {
                foreach (var vehicle in listBookingVehicletypes)
                {
                    var bookingVehicletypeFixed = new BookingVehicleType();
                    bookingVehicletypeFixed.BookingVehicleTypeId = vehicle.BookingVehicleTypeId;
                    bookingVehicletypeFixed.BookingId = reservation.BookingId;
                    bookingVehicletypeFixed.VehicleTypeId = vehicle.VehicleTypeId;
                    bookingVehicletypeFixed.DistancePrice = vehicle.DistancePrice;
                    bookingVehicletypeFixed.ModifiedDate = DateTime.UtcNow;
                    bookingVehicletypeFixed.CreatedDate = DateTime.UtcNow;
                    bookingVehicletypeFixed.CustomerRating = 0;
                    bookingVehicletypeFixed.CustomerComment = "";
                    bookingVehicletypeFixed.DriverRating = 0;
                    bookingVehicletypeFixed.DriverComment = "";
                    db.BookingVehicleTypes.Add(bookingVehicletypeFixed);
                    db.SaveChanges();
                }
            }
            // booking.TotalPrice = booking.OneWayPrice + booking.ReturnWayPrice + booking.OtherCharges - (booking.OneWayCredit + booking.ReturnWayCredit);
            var bookingPaymetFixed = new BookingPayment();
            bookingPaymetFixed.BookingPaymentId = bookingPaymentId;
            bookingPaymetFixed.BookingId = newFixedBooking.BookingId;
            bookingPaymetFixed.PaymentTypeId = reservation.PaymentMethodId == (int)PaymentTypeEnum.Use_My_Credits && reservation.BookingFee > 0
                                                                  ? (int)PaymentTypeEnum.Card
                                                                  : reservation.PaymentMethodId;
            bookingPaymetFixed.PaymentAmount = reservation.OneWayPrice + reservation.BookingFee - (reservation.OneWayCredit);
            bookingPaymetFixed.PaymentDate = DateTime.Now;
            bookingPaymetFixed.Status = (int)PaymentStatusEnum.Pending;
            bookingPaymetFixed.IsDeleted = false;
            bookingPaymetFixed.IsEnabled = true;
            bookingPaymetFixed.sysCreatedDate = DateTime.UtcNow;
            bookingPaymetFixed.sysModifiedDate = DateTime.UtcNow;
            db.BookingPayments.Add(bookingPaymetFixed);
            db.SaveChanges();



            if (reservation.JourneyTypeId == (int)JourneyTypeEnum.TwoWay)
            {
                SaveFixedReturnBooking(db,  reservation, returnJourney, bookingVehicleListReturn, returnFixedBookingId, returnFixedBookingPaymentId);
            }
        }
        public void SubtractCustomerCredits(TelBookingViewModel booking, BATransferEntities db, long returnBookingId)
        {
            if (db.Customers.Any(x => x.userId.Equals(booking.UserId) && (booking.OneWayCredit > 0 || booking.ReturnWayCredit > 0)))
            {
                if (booking.OneWayCredit > 0)
                {
                    var customerObj = db.Customers.First(x => x.userId.Equals(booking.UserId));
                    customerObj.TotalCredits = customerObj.TotalCredits - booking.OneWayCredit;
                    db.SaveChanges();
                    var discount = new CustomerTransaction();
                    discount.BookingId = booking.BookingId;
                    discount.TransactionTypeId = (int)TransactionTypeValEnum.DebitByCustomer;
                    discount.TransactionReference = "";
                    discount.InitialCredit = customerObj.TotalCredits; // ValueParseHelper.TryParseNullDate(model.ExpiryDate);
                    discount.TransactionCredit = 0;
                    discount.TransactionDebit = booking.OneWayCredit;
                    discount.FinalCredit = customerObj.TotalCredits;
                    discount.CustomerUserId = booking.UserId;
                    discount.IsEnabled = true;
                    discount.IsDeleted = false;
                    discount.IsHold = true;
                    discount.Description = booking.BookingNumber + " (" +
                                           (booking.OneWayCredit) + " Hold)";
                    discount.StaffUserId = null;
                    discount.sysCreatedDate = DateTime.UtcNow;
                    discount.sysModifiedDate = DateTime.UtcNow;
                    db.CustomerTransactions.Add(discount);
                    db.SaveChanges();
                }
                if (booking.ReturnWayCredit > 0)
                {
                    var customerObj = db.Customers.First(x => x.userId.Equals(booking.UserId));
                    customerObj.TotalCredits = customerObj.TotalCredits - booking.ReturnWayCredit;
                    db.SaveChanges();
                    var discount = new CustomerTransaction();
                    discount.BookingId = returnBookingId;
                    discount.TransactionTypeId = (int)TransactionTypeValEnum.DebitByCustomer;
                    discount.TransactionReference = "";
                    discount.InitialCredit = customerObj.TotalCredits; // ValueParseHelper.TryParseNullDate(model.ExpiryDate);
                    discount.TransactionCredit = 0;
                    discount.TransactionDebit = booking.ReturnWayCredit;
                    discount.FinalCredit = customerObj.TotalCredits;
                    discount.CustomerUserId = booking.UserId;
                    discount.IsEnabled = true;
                    discount.IsDeleted = false;
                    discount.IsHold = true;
                    discount.Description = booking.BookingNumber + " Return Journey (" +
                                           (booking.ReturnWayCredit) + " Hold)";
                    discount.StaffUserId = null;
                    discount.sysCreatedDate = DateTime.UtcNow;
                    discount.sysModifiedDate = DateTime.UtcNow;
                    db.CustomerTransactions.Add(discount);
                    db.SaveChanges();
                }
            }
        }
        #endregion 

     

        #region Send Email
        private void SendEmailAsync(TelBookingViewModel resevation,  ReturnJourney returnJourney)
        {
            try
            {
                string logoUrl = string.Empty;
                string bookingDetailApi = string.Empty;
                EmailCredentialsViewModel emailCredentials = null;
                string bookingToken = HttpContext.Current.Server.UrlEncode(resevation.BookingToken);
                logoUrl = ConfigHelper.GetWebLogo((int)BookingSiteEnum.BA);
                emailCredentials = EmailHelper.GetBookingNotificationEmailCredentials((int)EmailDomainEnum.BookingNotificationEmailBA);
                bookingDetailApi = ConfigHelper.GetBookingSummeryUrl((int)BookingSiteEnum.BA);
                bookingDetailApi = bookingDetailApi + "Booking/ViewDetails?ask=" + bookingToken;
                string mainUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                string loyaltyProgramUrl = mainUrl + "/MainPage/LoyaltyProgram";
                string contactUsUrl = mainUrl + "/MainPage/Contact";
                StringBuilder builder = new StringBuilder();
                string logFilePathHeader = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/Header.txt"));
                string textHeader = System.IO.File.ReadAllText(logFilePathHeader);
                textHeader = textHeader.Replace("LOGO-URL", logoUrl);
                textHeader = textHeader.Replace("BOOKING-DETAILS-URL", bookingDetailApi);
                textHeader = textHeader.Replace("BOOKING-URL", mainUrl);
                textHeader = textHeader.Replace("LOYALTY-PROGRAM-URL", loyaltyProgramUrl);
                textHeader = textHeader.Replace("CONTACT-US-URL", contactUsUrl);
                textHeader = textHeader.Replace("BOOKINGID", resevation.BookingNumber);
                builder.Append(textHeader);
                if (!string.IsNullOrEmpty(resevation.PassengerFirstName))
                {
                    string logFilePathLeadPassanger =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/Booking/LeadPassangerDetails.txt"));
                    //LOGO-URL
                    string passMobiCode = !string.IsNullOrEmpty(resevation.PassengerMobileNo) ? resevation.PassengerMobileNumberCountryCode : "";
                    string textPassanger = System.IO.File.ReadAllText(logFilePathLeadPassanger);
                    textPassanger = textPassanger.Replace("PASSENGERNAME", resevation.PassengerFirstName);
                    textPassanger = textPassanger.Replace("PASSENGEREMAIL", resevation.PassengerEmail);
                    textPassanger = textPassanger.Replace("PASSENGERMOBILE", passMobiCode + resevation.PassengerMobileNo);
                    builder.Append(textPassanger);
                }
                string logFilePathMember = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/MemberDetails.txt"));
                //LOGO-URL
                string textMember = System.IO.File.ReadAllText(logFilePathMember);
                textMember = textMember.Replace("BOOKINGPERSONNAME", resevation.BookedPersonFirstName);
                textMember = textMember.Replace("BOOKINGPERSONMOBILE", resevation.BookedPersonMobileNumberCountryCode + " " + resevation.BookedPersonMobileNo);

                textMember = textMember.Replace("BOOKINGPERSONALTERNATIVE", alternativeNo);
                textMember = textMember.Replace("BOOKINGPERSONEMAIL", resevation.BookedPersonEmail);
                builder.Append(textMember);
                string logFilePath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/EmailMainBody.txt"));
                string text = System.IO.File.ReadAllText(logFilePath);
                string paymentType = resevation.PaymentMethodId == (int)PaymentTypeEnum.Cash
                    ? "Cash"
                    : resevation.PaymentMethodId == (int)PaymentTypeEnum.Card
                        ? "Card"
                        : resevation.PaymentMethodId == (int)PaymentTypeEnum.Bank_Transfer
                            ? "Bank Transfer"
                            : resevation.PaymentMethodId == (int)PaymentTypeEnum.Free_Journey
                                ? "Free Journey"
                                : resevation.PaymentMethodId == (int)PaymentTypeEnum.Promo_Code
                                    ? "Promo Code"
                                    : resevation.PaymentMethodId == (int)PaymentTypeEnum.Voucher
                                        ? "Voucher"
                                        : resevation.PaymentMethodId == (int)PaymentTypeEnum.Account
                                            ? "Account"
                                            : resevation.PaymentMethodId == (int)PaymentTypeEnum.Use_My_Credits && resevation.BookingFee > 0
                                                ? "Card"
                                                : "Used My Credits";
                text = text.Replace("BOOKINGID", resevation.BookingNumber);
                text = text.Replace("RESERVATIONDATETIME", resevation.ClientDatetTime);
                text = text.Replace("BOOKINGAMOUNT", ((resevation.BookingFee + resevation.JourneyAmount) - (resevation.OneWayCredit + resevation.ReturnWayCredit)).ToString("0.00"));
                text = text.Replace("BOOKINGPAYMENTMETHOD", paymentType);
                if (resevation.CardPaymentTypeId == (int)CardPaymentTypeEnum.PayOverThePhone)
                {
                    text = text.Replace("PAYMENTOVERTHEPHONE",
                        "<span>Pay by Credit/Debit card over the phone to the Office (Call +44 208 900 2299)</span>");
                }
                else
                {
                    text = text.Replace("PAYMENTOVERTHEPHONE", "");
                }
                builder.Append(text);
                if (resevation.FromAriapointTypeId != (int)AreapointTypeEnum.Airport && resevation.FromAriapointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    string logFilePathOneWayBooking = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/OneWayBooking.txt"));
                    string textOneWay = System.IO.File.ReadAllText(logFilePathOneWayBooking);
                    textOneWay = textOneWay.Replace("BOOKINGDATETIME", resevation.BookingDateTime);
                    textOneWay = textOneWay.Replace("BOOKINGAMOUNTONEWAY", resevation.OneWayPrice.ToString("0.00"));
                    if (resevation.StudentDiscount == 1 && resevation.BookingSource == (int)BookingSource_Enum.AO_Web)
                    {
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTDIV",
                            "<p>Student Discount  <span style='float:right;'>:</span></p>");
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + resevation.StudentDiscountAmount.ToString("0.00") + "</p>");
                    }
                    else
                    {
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTDIV", "");
                        textOneWay = textOneWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                    }

                    if (resevation.PaymentMethodId == (int)PaymentTypeEnum.Free_Journey && (resevation.OneWayPrice - resevation.FreeJourneyAccessAmt) > 0)
                    {
                        textOneWay = textOneWay.Replace("FREEJOURNEYACCESSDIV",
                           "<p>Minimum Freejourney Amount  <span style='float:right;'>:</span><br/></p><p>Balance Pay Amount   <span style='float:right;'>:</span></p> ");
                        textOneWay = textOneWay.Replace("FREEJOURNEYACCESSAMTDIV", "<p> &#163; " + resevation.FreeJourneyAccessAmt.ToString("0.00") + "</p><p>&#163; " + (resevation.OneWayPrice - resevation.FreeJourneyAccessAmt).ToString("0.00") + "</p>");
                    }
                    else
                    {
                        textOneWay = textOneWay.Replace("FREEJOURNEYACCESSDIV", "");
                        textOneWay = textOneWay.Replace("FREEJOURNEYACCESSAMTDIV", "");
                    }
                    builder.Append(textOneWay);
                }
                if (resevation.FromAriapointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    string logFilePathAirportDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/AirportDetails.txt"));
                    string textAirport = System.IO.File.ReadAllText(logFilePathAirportDetails);
                    textAirport = textAirport.Replace("FLIGHTNUMBER", resevation.FlightNo);
                    textAirport = textAirport.Replace("FLIGHTLANDINGDATETIME", resevation.LandingDate + " " + resevation.LandingTime);
                    textAirport = textAirport.Replace("FLIGHTDISPLAYNAME", (!string.IsNullOrEmpty(resevation.DisplayNameFlight) ? resevation.DisplayNameFlight : " --"));
                    textAirport = textAirport.Replace("FLIGHTLANDINGDRIVERELLAPSETIME", resevation.DriverEllapseTime.ToString());
                    textAirport = textAirport.Replace("BOOKINGAMOUNTONEWAY", resevation.OneWayPrice.ToString("0.00"));
                    textAirport = textAirport.Replace("PICKUPCHARGE", resevation.PickupCharge.ToString("0.00"));
                    if (resevation.StudentDiscount == 1 && resevation.BookingSource == (int)BookingSource_Enum.AO_Web)
                    {
                        textAirport = textAirport.Replace("STUDENTDISCOUNTDIV",
                            "<p>Student Discount  <span style='float:right;'>:</span></p>");
                        textAirport = textAirport.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + resevation.StudentDiscountAmount.ToString("0.00") + "</p>");
                    }
                    else
                    {
                        textAirport = textAirport.Replace("STUDENTDISCOUNTDIV", "");
                        textAirport = textAirport.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                    }
                    if (resevation.PaymentMethodId == (int)PaymentTypeEnum.Free_Journey && (resevation.OneWayPrice - resevation.FreeJourneyAccessAmt) > 0)
                    {
                        textAirport = textAirport.Replace("FREEJOURNEYACCESSDIV",
                           "<p>Minimum Freejourney Amount  <span style='float:right;'>:</span><br/></p><p>Balance Pay Amount   <span style='float:right;'>:</span></p> ");
                        textAirport = textAirport.Replace("FREEJOURNEYACCESSAMTDIV", "<p> &#163; " + resevation.FreeJourneyAccessAmt.ToString("0.00") + "</p><p>&#163; " + (resevation.OneWayPrice - resevation.FreeJourneyAccessAmt).ToString("0.00") + "</p>");
                    }
                    else
                    {
                        textAirport = textAirport.Replace("FREEJOURNEYACCESSDIV", "");
                        textAirport = textAirport.Replace("FREEJOURNEYACCESSAMTDIV", "");
                    }
                    builder.Append(textAirport);
                }
                if (resevation.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    string logFilePathCruisePortDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/CruisePortDetails.txt"));
                    string textCruiseport = System.IO.File.ReadAllText(logFilePathCruisePortDetails);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPNUMBER", resevation.CruiseShipName);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPLANDINGDATETIME", resevation.LandingDate + " " + resevation.LandingTime);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPDISPLAYNAME", resevation.DisplayNameCruiseShip);
                    textCruiseport = textCruiseport.Replace("CRUISESHIPLANDINGDRIVERELLAPSETIME", resevation.DriverEllapseTime.ToString());
                    textCruiseport = textCruiseport.Replace("BOOKINGAMOUNTONEWAY", resevation.OneWayPrice.ToString("0.00"));
                    if (resevation.StudentDiscount == 1 && resevation.BookingSource == (int)BookingSource_Enum.AO_Web)
                    {
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTDIV",
                            "<p>Student Discount  <span style='float:right;'>:</span></p>");
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + resevation.StudentDiscountAmount.ToString("0.00") + "</p>");
                    }
                    else
                    {
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTDIV", "");
                        textCruiseport = textCruiseport.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                    }
                    builder.Append(textCruiseport);
                }
                if (resevation.JourneyTypeId == (int)JourneyTypeEnum.TwoWay)
                {
                    if (returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Airport)
                    {
                        string logFilePathAirportReturnDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/AirportReturn.txt"));
                        string textAirportReturn = System.IO.File.ReadAllText(logFilePathAirportReturnDetails);
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_NUMBER_RETURN", returnJourney.FlightNo);
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_LANDINGDATE_TIME_RETURN", returnJourney.LandingDateFlight + " " + returnJourney.LandingTimeFlight);
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_DISPLAY_NAME_RETURN", (!string.IsNullOrEmpty(returnJourney.DisplayNameFlight) ? returnJourney.DisplayNameFlight : "--"));
                        textAirportReturn = textAirportReturn.Replace("FLIGHT_LANDING_DRIVER_ELLAPSE_TIME_RETURN", returnJourney.FlightDriverEllapseTime.ToString());
                        textAirportReturn = textAirportReturn.Replace("RETURNDATETIME", returnJourney.LandingDateFlight + " " + returnJourney.LandingTimeFlight);
                        textAirportReturn = textAirportReturn.Replace("BOOKINGAMOUNTTWOWAY", resevation.ReturnWayPrice.ToString("0.00"));
                        textAirportReturn = textAirportReturn.Replace("PICKUPCHARGE", returnJourney.PickupCharge.ToString("0.00"));
                        if (resevation.StudentDiscount == 1 && resevation.BookingSource == (int)BookingSource_Enum.AO_Web)
                        {
                            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTDIV",
                                "<p>Student Discount  <span style='float:right;'>:</span></p>");
                            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + returnJourney.StudentDiscountAmount.ToString("0.00") + "</p>");
                        }
                        else
                        {
                            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTDIV", "");
                            textAirportReturn = textAirportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                        }
                        builder.Append(textAirportReturn);
                    }
                    else if (returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport)
                    {
                        string logFilePathCruiseportReturn = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/CruiseportReturn.txt"));
                        string textCruiseportReturn = System.IO.File.ReadAllText(logFilePathCruiseportReturn);
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_NUMBER_RETURN", returnJourney.CruiseShipName);
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_LANDING_DATE_TIME_RETURN", returnJourney.LandingDateCruiseShip + " " + returnJourney.LandingTimeCruiseShip);
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_DISPLAY_NAME_RETURN", returnJourney.DisplayNameCruiseShip);
                        textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_LANDING_DRIVER_ELLAPSE_TIME_RETURN", returnJourney.CruiseShipDriverEllapseTime.ToString());
                        textCruiseportReturn = textCruiseportReturn.Replace("RETURNDATETIME", returnJourney.LandingDateCruiseShip + " " + returnJourney.LandingTimeCruiseShip);
                        textCruiseportReturn = textCruiseportReturn.Replace("BOOKINGAMOUNTTWOWAY", resevation.ReturnWayPrice.ToString("0.00"));
                        if (resevation.StudentDiscount == 1 && resevation.BookingSource == (int)BookingSource_Enum.AO_Web)
                        {
                            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTDIV",
                                "<p>Student Discount  <span style='float:right;'>:</span></p>");
                            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + returnJourney.StudentDiscountAmount.ToString("0.00") + "</p>");
                        }
                        else
                        {
                            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTDIV", "");
                            textCruiseportReturn = textCruiseportReturn.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                        }
                        builder.Append(textCruiseportReturn);
                    }
                    if (returnJourney.FromAriapointTypeId != (int)AreapointTypeEnum.Airport && returnJourney.FromAriapointTypeId != (int)AreapointTypeEnum.Cruiseport)
                    {
                        string logFilePathTwoWayBooking = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/ReturnBooking.txt"));
                        string textTwoWay = System.IO.File.ReadAllText(logFilePathTwoWayBooking);
                        textTwoWay = textTwoWay.Replace("BOOKINGDATETIMERETURN", returnJourney.BookingDate + " " + returnJourney.BookingTime);
                        textTwoWay = textTwoWay.Replace("BOOKINGAMOUNTTWOWAY", resevation.ReturnWayPrice.ToString("0.00"));
                        if (resevation.StudentDiscount == 1 && resevation.BookingSource == (int)BookingSource_Enum.AO_Web)
                        {
                            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTDIV",
                                "<p>Student Discount  <span style='float:right;'>:</span></p>");
                            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "<p>&#163; " + returnJourney.StudentDiscountAmount.ToString("0.00") + "</p>");
                        }
                        else
                        {
                            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTDIV", "");
                            textTwoWay = textTwoWay.Replace("STUDENTDISCOUNTAMOUNTDIV", "");
                        }
                        builder.Append(textTwoWay);
                    }
                }
                if (resevation.OneWayCredit > 0 || resevation.ReturnWayCredit > 0)
                {
                    string discountMoneyBackTxt =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/Booking/DiscountMoneyBack.txt"));
                    string additionalChargesTxt = System.IO.File.ReadAllText(discountMoneyBackTxt);
                    additionalChargesTxt = additionalChargesTxt.Replace("DISCOUNT", (resevation.OneWayCredit + resevation.ReturnWayCredit).ToString("0.00"));
                    builder.Append(additionalChargesTxt);
                }
                //if (resevation.PaymentMethodId == (int)PaymentTypeEnum.Card || (resevation.PaymentMethodId == (int)PaymentTypeEnum.Use_My_Credits && resevation.BookingFee > 0))
                if (resevation.BookingFee > 0 && resevation.PaymentMethodId != (int)PaymentTypeEnum.Free_Journey)
                {
                    string logFilePathAdditionalCharges =
                        System.Web.HttpContext.Current.Server.MapPath(
                            string.Format("~/EmailTemplets/Booking/BookingFee.txt"));
                    string additionalChargesTxt = System.IO.File.ReadAllText(logFilePathAdditionalCharges);
                    additionalChargesTxt = additionalChargesTxt.Replace("ADDITIONALCHARGES", resevation.BookingFee.ToString("0.00"));
                    builder.Append(additionalChargesTxt);
                }
                string logFilePathAddressDetails = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/BookingAddress.txt"));
                string addresstext = System.IO.File.ReadAllText(logFilePathAddressDetails);

                //addresstext = addresstext.Replace("MAPURL", booking.MapUrl);
                addresstext = addresstext.Replace("BOOKING-DATE-TIME", (!string.IsNullOrEmpty(resevation.LandingDate) && (resevation.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport || resevation.FromAriapointTypeId == (int)AreapointTypeEnum.Airport)) ? ValueParseHelper.TryParseDateTime(resevation.LandingDate + " " + resevation.LandingTime).ToString() : resevation.BookingDateTime);
                addresstext = addresstext.Replace("FROM-ADDRESS", resevation.BookingFrom);
                addresstext = addresstext.Replace("TO-ADDRESS", resevation.BookingTo);
                StringBuilder builderViapoints = new StringBuilder();
                int viaid = 0;
                if (resevation.ViaPointAddress != null && resevation.ViaPointAddress.Any(x => !x.Postcode.IsEmpty()))
                {
                    var journeyViapoints = resevation.ViaPointAddress.Where(x => !x.Postcode.IsEmpty());
                    foreach (var viapoint in journeyViapoints.ToList())
                    {
                        viaid++;
                        string via = "<div style='width:97%; float: left; font-size:15px'>" +
                                     "<div   style='float:left; width:20%; color:#666666; font-weight:600; line-height: 10px;'>" +
                                     "<p style='margin-top:10px;'>Viapoint " + viaid +
                                     "<span style='float:right;'>:</span></p>" +
                                     "</div>" +
                                     "<div class='form-right' style='color:#666666; margin-left:10px; width: 75%; float: left;'>" +
                                     "<p style='margin-top:10px;line-height: 17px;'>" + viapoint.FullAddress + "</p> " +
                                     "</div>" +
                                     "</div>";
                        builderViapoints.Append(via);
                    }
                    addresstext = addresstext.Replace("VIA-POINTS", builderViapoints.ToString());
                }
                else
                {
                    addresstext = addresstext.Replace("VIA-POINTS", "");
                }
                builder.Append(addresstext);
                if (resevation.JourneyTypeId == (int)JourneyTypeEnum.TwoWay)
                {
                    string logFilePathReturnBookingAddress = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/BookingAddressReturn.txt"));
                    string returnBookingTxt = System.IO.File.ReadAllText(logFilePathReturnBookingAddress);
                    if (returnJourney.FromAriapointTypeId != (int)AreapointTypeEnum.Airport &&
                        returnJourney.FromAriapointTypeId != (int)AreapointTypeEnum.Cruiseport)
                    {
                        returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME", returnJourney.BookingDate + " " + returnJourney.BookingTime);
                    }
                    if (returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Airport)
                    {
                        returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME", returnJourney.LandingDateFlight + " " + returnJourney.LandingTimeFlight);
                    }
                    if (returnJourney.FromAriapointTypeId == (int)AreapointTypeEnum.Cruiseport)
                    {
                        returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME", returnJourney.LandingDateCruiseShip + " " + returnJourney.LandingTimeCruiseShip);
                    }
                    returnBookingTxt = returnBookingTxt.Replace("FROM-ADDRESS", returnJourney.PickupPoint);
                    returnBookingTxt = returnBookingTxt.Replace("TO-ADDRESS", returnJourney.DropoffPoint);
                    StringBuilder builderViapointsReturn = new StringBuilder();
                    int viaidReturn = 0;
                    if (returnJourney.ViaPointAddress != null && returnJourney.ViaPointAddress.Any(x => !x.Postcode.IsEmpty()))
                    {
                        var journeyViapointsReturn = returnJourney.ViaPointAddress.Where(x => !x.Postcode.IsEmpty());
                        foreach (var viapoint in journeyViapointsReturn.ToList())
                        {
                            viaidReturn++;
                            string via = "<div style='width:97%; float: left; font-size:15px'>" +
                                         "<div   style='float:left; width:20%; color:#666666; font-weight:600; line-height: 10px;'>" +
                                         "<p style='margin-top:10px;'>Viapoint " + viaidReturn +
                                         "<span style='float:right;'>:</span></p>" +
                                         "</div>" +
                                         "<div class='form-right' style='color:#666666; margin-left:10px; width: 75%; float: left;'>" +
                                         "<p style='margin-top:10px;line-height: 17px;'>" + viapoint.FullAddress + "</p> " +
                                         "</div>" +
                                         "</div>";
                            builderViapointsReturn.Append(via);
                        }
                        returnBookingTxt = returnBookingTxt.Replace("VIA-POINTS", builderViapointsReturn.ToString());
                    }
                    else
                    {
                        returnBookingTxt = returnBookingTxt.Replace("VIA-POINTS", "");
                    }
                    builder.Append(returnBookingTxt);
                }
                else
                {
                    string fileClosingDiv = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/ClosingDiv.txt"));
                    string fileClosingDivTxt = System.IO.File.ReadAllText(fileClosingDiv);
                    builder.Append(fileClosingDivTxt);
                }
                string fileMap = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/Map.txt"));
                string textMap = System.IO.File.ReadAllText(fileMap);
                string comment = "<div class='titile-area' style ='float:left; width: 100%;'>" +
                                 "<h2 style='color:#0e3a7d; line-height: 0px; float: left; font-weight: 800; font-size: 15px; margin-left: 20px; text-transform:capitalize; margin-top: 35px;'>Customer comments</h2>" +
                                 "</div>" +
                                 "<div style='float:left;color:#666666;font-size:15px;line-height:5px'>" +
                                 "<p style='line-height:1.2'>" + resevation.CustomerComment + "</p>" +
                                  "</div>" +
                                  "</div>";


                if (!string.IsNullOrEmpty(resevation.CustomerComment))
                {
                    textMap = textMap.Replace("CUSTOMERCOMMENTS", comment);
                }
                else
                {
                    textMap = textMap.Replace("CUSTOMERCOMMENTS", "");
                }
                textMap = textMap.Replace("MAP-URL", resevation.MapUrl);

                builder.Append(textMap);
                string fileLuggageInfo = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/LuggageInfo.txt"));
                string textLuggage = System.IO.File.ReadAllText(fileLuggageInfo);
                string vehicleTypeName = string.Join(" & ", resevation.VehicleTypeId);
                textLuggage = textLuggage.Replace("BOOKINGVEHICLETYPES", vehicleTypeName);
                textLuggage = textLuggage.Replace("NUMBEROFPESSANGERS", resevation.NumOfPassengers.ToString());
                textLuggage = textLuggage.Replace("NUMBEROFBABYSEATS", (resevation.BoosterSeat + resevation.InfantSeats + resevation.ChildSeats).ToString());
                textLuggage = textLuggage.Replace("NUMBEROFLUGGAHES", resevation.NumberOfLuggages.ToString());
                textLuggage = textLuggage.Replace("NUMBEROFHANDLUGGAHES", resevation.NumberOfHandLuggages.ToString());
                textLuggage = textLuggage.Replace("BOOKINGAMOUNT", ((resevation.BookingFee + resevation.JourneyAmount) - (resevation.OneWayCredit + resevation.ReturnWayCredit)).ToString("0.00"));
                builder.Append(textLuggage);
                string logFilePathFooderDetals = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/EmailTemplets/Booking/FooderDetals.txt"));
                string foodertext = System.IO.File.ReadAllText(logFilePathFooderDetals);
                foodertext = foodertext.Replace("BOOKING-URL", mainUrl);
                foodertext = foodertext.Replace("LOYALTY-PROGRAM-URL", loyaltyProgramUrl);
                foodertext = foodertext.Replace("CONTACT-US-URL", contactUsUrl);
                builder.Append(foodertext);
                var listEmails = new List<string>();
                if (resevation.IsSendPassengerEmail)
                {
                    listEmails.Add(resevation.PassengerEmail);
                    listEmails.Add(resevation.BookedPersonEmail);
                }
                else
                {
                    listEmails.Add(resevation.BookedPersonEmail);
                }
                if (resevation.PaymentMethodId == (int)PaymentTypeEnum.Account)
                {
                    string companyemail = GetCompanyEmail(resevation);
                    if (!string.IsNullOrEmpty(companyemail))
                        listEmails.Add(companyemail);
                }
                // listEmails.Add(booking.PassengerEmail);
                string _emailSubject = ConfigHelper.GetEmailSubject((int)BookingStatusEnum.Requested);
                if (!string.IsNullOrEmpty(_emailSubject))
                {
                    //Srikanth, Your Reservation Status at batransfer.com - Ref: BA479068
                    _emailSubject = _emailSubject.Replace("{0}", resevation.BookedPersonFirstName);
                    _emailSubject = _emailSubject.Replace("{1}", emailCredentials.DisplayName);
                    _emailSubject = _emailSubject.Replace("{2}", resevation.BookingNumber);
                }
                var email = new RootEmail();
                email.Attachments = null;
                email.Body = builder.ToString();
                email.FromEmailAddress = emailCredentials.FromEmail;
                email.Username = emailCredentials.Username;
                email.Password = emailCredentials.Password;
                email.Port = emailCredentials.Port;
                email.Smtp = emailCredentials.Smtp;
                email.Subject = _emailSubject;
                email.ToEmailAddresses = listEmails;
                email.DisplayName = emailCredentials.DisplayName;
                email.IsEnableSsl = emailCredentials.IsEnabledSSL;
                string apiUrl = EmailHelper.GetEmailUrl((int)ApiUrlEnum.EmailWithoutAttachmentApi);
                EmailHelper.SendEmailWithoutAttachtment(email, apiUrl);
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }


        public string GetCompanyEmail(TelBookingViewModel resevation)
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    var objContact = db.ContactPersons.FirstOrDefault(x => x.CompanyId == resevation.CompanyId && x.IsEnabled);

                    if (objContact != null)
                    {
                        return objContact.Email;
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return string.Empty;
        }
        #endregion

        #region Create Image Url 
        private string CreateGoogleImage(  BATransferEntities db, TelBookingViewModel booking)
        {
            string mapUrl = string.Empty;
            try
            {
                var root = new RootMap();
                root.BookingNumber = booking.BookingNumber;
                var pickuppoint = new Pickuppoint();
                pickuppoint.Lat = booking.FromLat.ToString();
                pickuppoint.Long = booking.FromLong.ToString();
                var dropoffpoint = new Dropoffpoint();
                dropoffpoint.Lat = booking.ToLat.ToString();
                dropoffpoint.Long = booking.ToLong.ToString();
                var listVia = new List<Viapoint>();
                if (booking.ViaPointAddress != null)
                {
                    foreach (var item in booking.ViaPointAddress.Where(x => !x.Postcode.IsEmpty()))
                    {
                        var via = new Viapoint();
                        via.Lat = item.Lat.ToString();
                        via.Long = item.Long.ToString();
                        listVia.Add(via);
                    }
                }
                if (listVia != null && listVia.Count > 0)
                    root.ViaPoints = listVia;
                root.PickupPoint = pickuppoint;
                root.DropoffPoint = dropoffpoint;
                root.Route = " ";
                root.Size = "600x400";
                string jsonmap = JSonHelper.ConvertObjectToJSon(root);
                string urlCustomerFileApi = ApiUrlHelper.GetUrl((int)ApiUrlEnum.MapApi) +
                                            "/api/file/generateMapUrl";
                mapUrl = MapHelper.SendMap(jsonmap, urlCustomerFileApi);
                // address.MapUrl = mapUrl;
                if (db.Booking_TEMP.Any(x => x.BookingId == booking.BookingId))
                {
                    var objbooking = db.Booking_TEMP.First(x => x.BookingId == booking.BookingId);
                    objbooking.MapUrl = mapUrl;
                    db.SaveChanges();
                }
                if (db.Bookings.Any(x => x.BookingId == booking.BookingId))
                {
                    var objbooking = db.Bookings.First(x => x.BookingId == booking.BookingId);
                    objbooking.MapUrl = mapUrl;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return mapUrl;
        }
        #endregion

        #region Save Other Charge
        private void SaveOtherCharge(TelBookingViewModel booking, BATransferEntities db)
        {
            try
            {
                if (booking.PaymentMethodId == (int)PaymentTypeEnum.Card || booking.PaymentMethodId == (int)PaymentTypeEnum.Use_My_Credits)
                {
                    var tempCarge = new BookingCustomerCharge_TEMP();
                    tempCarge.BookingId = booking.BookingId;
                    tempCarge.BookingChargeEnumId = (int)BookingCharge_Enum.Card_Charge;
                    tempCarge.Amount = booking.BookingFee;
                    db.BookingCustomerCharge_TEMP.Add(tempCarge);
                    db.SaveChanges();
                    var Carge = new BookingCustomerCharge();
                    Carge.BookingCustomerChargeId = tempCarge.BookingCustomerChargeId;
                    Carge.BookingId = booking.BookingId;
                    Carge.BookingChargeEnumId = (int)BookingCharge_Enum.Card_Charge;
                    Carge.Amount = booking.BookingFee;
                    db.BookingCustomerCharges.Add(Carge);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }
        #endregion

        #region Save Faorit Address
        private void SaveFavouriteAddresses(TelBookingViewModel reservation)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (reservation.FromLat != 0 && reservation.FromLong != 0)
                    {
                        int fromAriapointTypeId = reservation.FromAriapointTypeId == (int)AreapointTypeEnum.POI ? (int)AreapointTypeEnum.Place : reservation.FromAriapointTypeId;
                        string fromlabel =  reservation.BookingFrom;
                        db.sp_favouriteAddress_insert_update(
                             reservation.FromFavouriteAddressId
                            , reservation.UserId
                            , reservation.FromAddressId
                            , fromAriapointTypeId
                            , reservation.BookingFrom
                            , reservation.FromOrganisationName
                            , reservation.FromDepartmentName
                            , reservation.FromBuildingName
                            , reservation.FromSubBuildingName
                            , reservation.FromBuildingNumber
                            , reservation.FromStreet
                            , reservation.FromPostTown
                            , reservation.FromPostCode
                            , "UK"
                            , reservation.FromLat
                            , reservation.FromLong
                            , reservation.PickupNote ?? ""
                            , fromlabel
                            , reservation.FromAirportCode
                            , reservation.FromFixedPriceAriaPointId
                            );
                    }

                    if (reservation.ToLat != 0 && reservation.ToLong != 0)
                    {
                        int toAriapointTypeId = reservation.ToAriapointTypeId == (int)AreapointTypeEnum.POI ? (int)AreapointTypeEnum.Place : reservation.ToAriapointTypeId;
                        string tolabel =   reservation.BookingTo;
                        db.sp_favouriteAddress_insert_update(
                              reservation.ToFavouriteAddressId
                            , reservation.UserId
                            , reservation.ToAddressId
                            , toAriapointTypeId
                            , reservation.BookingTo
                            , reservation.ToOrganisationName
                            , reservation.ToDepartmentName
                            , reservation.ToBuildingName
                            , reservation.ToSubBuildingName
                            , reservation.ToBuildingNumber
                            , reservation.ToStreet
                            , reservation.ToPostTown
                            , reservation.ToPostCode
                            , "UK"
                            , reservation.ToLat
                            , reservation.ToLong
                            , ""
                            , tolabel
                            , reservation.ToAirportCode
                            , reservation.ToFixedPriceAriaPointId
                            );
                    }
                    saveBookingAddress(reservation);
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }

        public void saveBookingAddress(TelBookingViewModel reservation)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (reservation.FromAriapointTypeId != (int)AreapointTypeEnum.Airport &&
               reservation.FromAriapointTypeId != (int)AreapointTypeEnum.POI &&
               reservation.FromAriapointTypeId != (int)AreapointTypeEnum.Cruiseport)
                    {
                        var frombookingAddress = new BookingAddress();
                        frombookingAddress.BookingId = reservation.BookingId;
                        frombookingAddress.AreaPointId = reservation.FromAddressId;
                        frombookingAddress.OrganisationName = reservation.FromOrganisationName ?? "";
                        frombookingAddress.DepartmentName = reservation.FromDepartmentName ?? "";
                        frombookingAddress.BuildingName = reservation.FromBuildingName ?? "";
                        frombookingAddress.SubBuildingName = reservation.FromSubBuildingName ?? "";
                        frombookingAddress.BuildingNumber = reservation.FromBuildingNumber;
                        frombookingAddress.Street = reservation.FromStreet ?? "";
                        frombookingAddress.PostTown = reservation.FromPostTown ?? "";
                        frombookingAddress.Postcode = reservation.FromPostCode;
                        frombookingAddress.CountryCode = "UK";
                        frombookingAddress.Lat = reservation.FromLat;
                        frombookingAddress.Long = reservation.FromLong;
                        frombookingAddress.IsFromAddress = true;
                        frombookingAddress.IsActive = true;
                        frombookingAddress.ModifiedDate = DateTime.UtcNow;
                        frombookingAddress.CreatedDate = DateTime.UtcNow;
                        db.BookingAddresses.Add(frombookingAddress);
                        db.SaveChanges();
                    }
                    if (reservation.ToAriapointTypeId != (int)AreapointTypeEnum.Airport &&
                        reservation.ToAriapointTypeId != (int)AreapointTypeEnum.POI &&
                        reservation.ToAriapointTypeId != (int)AreapointTypeEnum.Cruiseport)
                    {
                        var tobookingAddress = new BookingAddress();
                        tobookingAddress.BookingId = reservation.BookingId;
                        tobookingAddress.AreaPointId = reservation.ToAddressId;
                        tobookingAddress.OrganisationName = reservation.ToOrganisationName ?? "";
                        tobookingAddress.DepartmentName = reservation.ToDepartmentName ?? "";
                        tobookingAddress.BuildingName = reservation.ToBuildingName ?? "";
                        tobookingAddress.SubBuildingName = reservation.ToSubBuildingName ?? "";
                        tobookingAddress.BuildingNumber = reservation.ToBuildingNumber;
                        tobookingAddress.Street = reservation.ToStreet ?? "";
                        tobookingAddress.PostTown = reservation.ToPostTown ?? "";
                        tobookingAddress.Postcode = reservation.ToPostCode ?? "";
                        tobookingAddress.CountryCode = "UK";
                        tobookingAddress.Lat = reservation.ToLat;
                        tobookingAddress.Long = reservation.ToLong;
                        tobookingAddress.IsFromAddress = false;
                        tobookingAddress.IsActive = true;
                        tobookingAddress.ModifiedDate = DateTime.UtcNow;
                        tobookingAddress.CreatedDate = DateTime.UtcNow;
                        db.BookingAddresses.Add(tobookingAddress);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }
        #endregion

        #region vehicle Upgrate 
        public void UpdateVehicleUpdate(TelBookingViewModel reservation)
        {
            try
            {
                Customer customer = null;
                using (var db = new BATransferEntities())
                {


                    var upgradeHistory = new BookingVehicleUpgradeHistory();
                    upgradeHistory.BookingId = reservation.BookingId;
                    upgradeHistory.PointsUsed = reservation.UpgradePoints;
                    upgradeHistory.VehicleUpgradeAmt = reservation.UpgradeAmount;
                    upgradeHistory.RequestedVehicleTypeId = reservation.CurrentVehicletypeId;
                    upgradeHistory.UpgradedVehicleTypeId = reservation.UpgradeVehicleTypeId;
                    upgradeHistory.IsDeleted = false;
                    upgradeHistory.IsEnabled = true;
                    upgradeHistory.sysCreatedDate = DateTime.UtcNow;
                    upgradeHistory.sysModifiedDate = DateTime.UtcNow;
                    db.BookingVehicleUpgradeHistories.Add(upgradeHistory);
                    db.SaveChanges();

                    customer = db.Customers.FirstOrDefault(x => x.userId.Equals(reservation.UserId));
                    if (customer != null)
                    {
                        customer.availablePoints = customer.availablePoints - reservation.UpgradePoints;
                        db.SaveChanges();
                        var bookingPoint = new BookingPoint();
                        bookingPoint.BookingId = reservation.BookingId;
                        bookingPoint.PointsTypeId = (int)FreeJourneyPointsTypeEnum.VehicleUpgrade;
                        bookingPoint.Points = -reservation.UpgradePoints;
                        bookingPoint.TotalPoints = customer.availablePoints;
                        bookingPoint.IsDeleted = false;
                        bookingPoint.IsEnabled = false;
                        bookingPoint.sysCreatedDate = DateTime.UtcNow;
                        bookingPoint.sysModifiedDate = DateTime.UtcNow;
                        db.BookingPoints.Add(bookingPoint);
                        db.SaveChanges();
                    }

                }

            }
            catch (Exception ex)
            {
                string xml = XmlHelper.GetXMLFromObject(reservation);
                ErrorHandling.LogFileWriteWithOblect(xml + "|CurrentVehicletypeId ---> " + reservation.CurrentVehicletypeId + "| UpgradeVehicleTypeId ---->" + reservation.UpgradeVehicleTypeId, ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
        }

        #endregion
    }
}
