﻿using SwaranAdmin_New.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwaranAdmin_New.DataAccess
{
    public class MissingAddressDal
    {
        #region Singalton
        private static MissingAddressDal _instance;
        private static readonly object LockThis = new object();
        protected MissingAddressDal()
        {
        }
        public static MissingAddressDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new MissingAddressDal());
                }
            }
        }
        #endregion

        public void GetAddress()
        {
            try
            {
                //using (var db=)
                //{

                //}
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                   System.Reflection.MethodBase.GetCurrentMethod().Name,
                   ex.InnerException != null ? ex.InnerException.Message : ""); 
            }
        }
    }
}