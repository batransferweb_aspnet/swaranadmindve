﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;

namespace SwaranAdmin_New.DataAccess
{
    public class LowEmissionZoneDal
    {

        #region Singalton
        private static LowEmissionZoneDal _instance;
        private static readonly object LockThis = new object();
        protected LowEmissionZoneDal()
        {
        }
        public static LowEmissionZoneDal Instance 
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new LowEmissionZoneDal());
                }
            }
        }


        #endregion
 

        public void SaveEmissionZones(LowEmissionZoneViewModel list) 
        {
            try
            {
                using (var db=new BATransferEntities()) 
                {
                    foreach (var item in list.StreetList) 
                    {
                        var zone = new EmissionZone();
                        zone.AddressId = item.AddressId;
                        zone.StreetName = item.Name;
                        zone.Description = list.Comments;
                        zone.EmissionType = list.EmissionType;
                        zone.IsEnabeld = list.IsEnabled;
                        zone.Postcode = list.Postcode; 
                        zone.Title = list.Title;
                        zone.CreatedDate = DateTime.UtcNow;
                        zone.ModifiedDate = DateTime.UtcNow;
                        db.EmissionZones.Add(zone);
                        db.SaveChanges();

                        foreach (var day in list.Days)
                        {
                            var cuDay = new EmissionZoneDay();
                            cuDay.DayId = day.DayId;
                            cuDay.ZoneId = zone.ZoneId;
                            cuDay.IsEnabeld = list.IsEnabled;
                            cuDay.CreatedDate = DateTime.UtcNow;
                            cuDay.ModifiedDate = DateTime.UtcNow;
                            db.EmissionZoneDays.Add(cuDay);
                            db.SaveChanges();

                            foreach (var time in list.TimeRangeList)
                            {
                                var cutime = new EmissionZoneTime();
                                cutime.ZoneDayId = cuDay.ZoneDayId; 
                                cutime.StartTime =  TimeSpan.Parse(time.TimeRange.Split('-')[0].Trim());
                                cutime.EndTime = TimeSpan.Parse(time.TimeRange.Split('-')[1].Trim());
                                cutime.IsEnabeld = list.IsEnabled;
                                cutime.EmissionRateFormula = "PRICE" + list.EmissionRateFormula;
                                cutime.CreatedDate = DateTime.UtcNow;
                                cutime.ModifiedDate = DateTime.UtcNow;
                                db.EmissionZoneTimes.Add(cutime);
                                db.SaveChanges();
                            }
                           

                        }

                    }

                   
                }
                 
             
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var entityValidationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in entityValidationErrors.ValidationErrors)
                    {
                        ErrorHandling.LogFileWrite(ex.Message, "Property: " + validationError.PropertyName, System.Reflection.MethodBase.GetCurrentMethod().Name, " Error: " + validationError.ErrorMessage);
                    }
                }
                  
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
        }


        public List<EmissionZoneViewModel> GetAllEmissionZones()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<EmissionZoneViewModel> objEmissionZone =
                        db.sp_swaran_noemission().Select(x => new EmissionZoneViewModel()
                        {
                            AddressId = x.AddressId,
                            Description = x.Description,
                            EmissionType = x.EmissionType == 1 
                                         ?"Ultra Low Emission Zone"
                                         : "No Emission Zone",
                            Postcode = x.Postcode,
                            StreetName = x.StreetName,
                            Title = x.Title,
                            ZoneId = x.ZoneId
                        });
                    return objEmissionZone.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return null;
        }
    }
}