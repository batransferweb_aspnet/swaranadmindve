﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SwaranAdmin_New.DataAccess
{
    public class CustomerEmailDal
    {
        #region Singalton
        private static CustomerEmailDal _instance;
        private static readonly object LockThis = new object();
        protected CustomerEmailDal()
        {
        }
        public static CustomerEmailDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new CustomerEmailDal());
                }
            }
        }
        #endregion

        #region Send Email
        //public void SendEmailAsync(CurrentBookingViewModel model)
        //{
        //    var returnJourney = new GetBookingAdminView_Result();
        //    try
        //    {
        //        using (var db = new BATransferEntities())
        //        {


        //            var objBooking = db.Bookings.First(x => x.BookingToken.Equals(model.BookingToken));
        //            List<GetBookingAdminView_Result> result = db.GetBookingAdminView(model.BookingToken, false).ToList();
        //            if (result != null && result.Any())
        //            {
        //                var booking = result.First();
        //                string logoUrl = ConfigHelper.GetWebLogo((int) BookingSiteEnum.BA);

        //                StringBuilder builder = new StringBuilder();

        //                string logFilePathHeader =
        //                    System.Web.HttpContext.Current.Server.MapPath(
        //                        string.Format("~/EmailTemplets/Booking/Header.txt"));
        //                string textHeader = System.IO.File.ReadAllText(logFilePathHeader);
        //                builder.Append(textHeader);


        //                if (model.IsConfirmBooking)
        //                {
        //                    string logFilePath =
        //                        System.Web.HttpContext.Current.Server.MapPath(
        //                            string.Format("~/EmailTemplets/Booking/EmailMainBodyConfirmed.txt"));
        //                    string text = System.IO.File.ReadAllText(logFilePath);
        //                    text = text.Replace("BOOKINGPERSONNAME",
        //                        booking.BookedPersonName);
        //                    text = text.Replace("BOOKINGPERSONMOBILE",
        //                        booking.PrimaryPhoneNumber);
        //                    text = text.Replace("BOOKINGPERSONALTERNATIVE",
        //                        booking.AlternativePhoneNumber);
        //                    text = text.Replace("BOOKINGPERSONEMAIL", booking.Email);
        //                    text = text.Replace("BOOKINGID", booking.BookingNumber);
        //                    text = text.Replace("RESERVATIONDATETIME", booking.CreatedDate.ToString());
        //                    text = text.Replace("BOOKINGAMOUNT",
        //                        (booking.OtherCharges + booking.TotalDistancePrice).ToString());
        //                    text = text.Replace("BOOKINGPAYMENTMETHOD",
        //                        Enum.GetName((typeof (PaymentTypeEnum)), booking.PaymentMethod));

        //                    builder.Append(text);
        //                }

        //                if (model.IsCancelBooking)
        //                {
        //                    string logFilePath =
        //                        System.Web.HttpContext.Current.Server.MapPath(
        //                            string.Format("~/EmailTemplets/Booking/EmailMainBodyCanceled.txt"));
        //                    string text = System.IO.File.ReadAllText(logFilePath);
        //                    text = text.Replace("BOOKINGPERSONNAME",
        //                        booking.BookedPersonName);
        //                    text = text.Replace("BOOKINGPERSONMOBILE",
        //                        booking.PrimaryPhoneNumber);
        //                    text = text.Replace("BOOKINGPERSONALTERNATIVE",
        //                        booking.AlternativePhoneNumber);
        //                    text = text.Replace("BOOKINGPERSONEMAIL", booking.Email);
        //                    text = text.Replace("BOOKINGID", booking.BookingNumber);
        //                    text = text.Replace("RESERVATIONDATETIME", booking.CreatedDate.ToString());
        //                    text = text.Replace("BOOKINGAMOUNT",
        //                        (booking.OtherCharges + booking.TotalDistancePrice).ToString());
        //                    text = text.Replace("BOOKINGPAYMENTMETHOD",
        //                        Enum.GetName((typeof (PaymentTypeEnum)), booking.PaymentMethod));

        //                    builder.Append(text);
        //                }

        //                if (booking.PaymentMethod == (int) PaymentTypeEnum.Card)
        //                {
        //                    string logFilePathAdditionalCharges =
        //                        System.Web.HttpContext.Current.Server.MapPath(
        //                            string.Format("~/EmailTemplets/Booking/CardPayment.txt"));
        //                    string additionalChargesTxt = System.IO.File.ReadAllText(logFilePathAdditionalCharges);
        //                    additionalChargesTxt = additionalChargesTxt.Replace("ADDITIONALCHARGES",
        //                        booking.OtherCharges.ToString());
        //                    builder.Append(additionalChargesTxt);
        //                }

        //                if (booking.FromAriaPointTypeId != (int) AreapointTypeEnum.Airport &&
        //                    booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
        //                {
        //                    string logFilePathOneWayBooking =
        //                        System.Web.HttpContext.Current.Server.MapPath(
        //                            string.Format("~/EmailTemplets/Booking/OneWayBooking.txt"));
        //                    string textOneWay = System.IO.File.ReadAllText(logFilePathOneWayBooking);

        //                    //textOneWay = textOneWay.Replace("BOOKINGDATETIME",
        //                    //    booking.BookingDateTime.ToLongDateString());
        //                    textOneWay = textOneWay.Replace("BOOKINGAMOUNTONEWAY", booking.TotalDistancePrice.ToString());
        //                    builder.Append(textOneWay);
        //                }



        //                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
        //                {
        //                    string logFilePathAirportDetails =
        //                        System.Web.HttpContext.Current.Server.MapPath(
        //                            string.Format("~/EmailTemplets/Booking/AirportDetails.txt"));
        //                    string textAirport = System.IO.File.ReadAllText(logFilePathAirportDetails);
        //                    textAirport = textAirport.Replace("FLIGHTNUMBER", booking.FlightNo);
        //                    textAirport = textAirport.Replace("FLIGHTLANDINGDATETIME",
        //                        booking.BookingDateTime.ToString());
        //                    textAirport = textAirport.Replace("FLIGHTDISPLAYNAME", booking.DriverDisplayName);
        //                    textAirport = textAirport.Replace("FLIGHTLANDINGDRIVERELLAPSETIME",
        //                        booking.DriverEllapseTime.ToString());
        //                    textAirport = textAirport.Replace("BOOKINGAMOUNTONEWAY",
        //                        booking.TotalDistancePrice.ToString());
        //                    builder.Append(textAirport);
        //                }
        //                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
        //                {
        //                    string logFilePathCruisePortDetails =
        //                        System.Web.HttpContext.Current.Server.MapPath(
        //                            string.Format("~/EmailTemplets/Booking/CruisePortDetails.txt"));
        //                    string textCruiseport = System.IO.File.ReadAllText(logFilePathCruisePortDetails);

        //                    textCruiseport = textCruiseport.Replace("CRUISESHIPNUMBER", booking.FlightNo);
        //                    textCruiseport = textCruiseport.Replace("CRUISESHIPLANDINGDATETIME",
        //                        booking.BookingDateTime.ToString());
        //                    textCruiseport = textCruiseport.Replace("CRUISESHIPDISPLAYNAME", booking.DriverDisplayName);
        //                    textCruiseport = textCruiseport.Replace("CRUISESHIPLANDINGDRIVERELLAPSETIME",
        //                        booking.DriverEllapseTime.ToString());
        //                    textCruiseport = textCruiseport.Replace("BOOKINGAMOUNTONEWAY",
        //                        booking.TotalDistancePrice.ToString());


        //                    builder.Append(textCruiseport);

        //                }


        //                if (booking.IsReturnBooking)
        //                {
        //                    List<GetBookingAdminView_Result> resultReturnJourney =
        //                        db.GetBookingAdminView(model.BookingToken, true).ToList();

        //                    if (resultReturnJourney != null && resultReturnJourney.Any())
        //                    {
        //                        returnJourney = resultReturnJourney.First();
        //                        if (returnJourney.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
        //                        {
        //                            string logFilePathAirportReturnDetails =
        //                                System.Web.HttpContext.Current.Server.MapPath(
        //                                    string.Format("~/EmailTemplets/Booking/AirportReturn.txt"));
        //                            string textAirportReturn =
        //                                System.IO.File.ReadAllText(logFilePathAirportReturnDetails);

        //                            textAirportReturn = textAirportReturn.Replace("FLIGHT_NUMBER_RETURN",
        //                                returnJourney.FlightNo);
        //                            textAirportReturn = textAirportReturn.Replace("FLIGHT_LANDINGDATE_TIME_RETURN",
        //                                returnJourney.BookingDateTime.ToString());
        //                            textAirportReturn = textAirportReturn.Replace("FLIGHT_DISPLAY_NAME_RETURN",
        //                                returnJourney.DriverDisplayName);
        //                            textAirportReturn =
        //                                textAirportReturn.Replace("FLIGHT_LANDING_DRIVER_ELLAPSE_TIME_RETURN",
        //                                    returnJourney.DriverEllapseTime.ToString());
        //                            textAirportReturn = textAirportReturn.Replace("RETURNDATETIME",
        //                                returnJourney.BookingDateTime.ToString());
        //                            textAirportReturn = textAirportReturn.Replace("BOOKINGAMOUNTTWOWAY",
        //                                booking.TotalDistancePrice.ToString());
        //                            builder.Append(textAirportReturn);
        //                        }
        //                        else if (returnJourney.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
        //                        {
        //                            string logFilePathCruiseportReturn =
        //                                System.Web.HttpContext.Current.Server.MapPath(
        //                                    string.Format("~/EmailTemplets/Booking/CruiseportReturn.txt"));
        //                            string textCruiseportReturn = System.IO.File.ReadAllText(logFilePathCruiseportReturn);

        //                            textCruiseportReturn = textCruiseportReturn.Replace("CRUISESHIP_NUMBER_RETURN",
        //                                returnJourney.FlightNo);
        //                            textCruiseportReturn =
        //                                textCruiseportReturn.Replace("CRUISESHIP_LANDING_DATE_TIME_RETURN",
        //                                    returnJourney.BookingDateTime.ToString());
        //                            textCruiseportReturn = textCruiseportReturn.Replace(
        //                                "CRUISESHIP_DISPLAY_NAME_RETURN",
        //                                returnJourney.DriverDisplayName);
        //                            textCruiseportReturn =
        //                                textCruiseportReturn.Replace("CRUISESHIP_LANDING_DRIVER_ELLAPSE_TIME_RETURN",
        //                                    returnJourney.DriverEllapseTime.ToString());
        //                            textCruiseportReturn = textCruiseportReturn.Replace("RETURNDATETIME",
        //                                returnJourney.BookingDateTime.ToString());
        //                            textCruiseportReturn = textCruiseportReturn.Replace("BOOKINGAMOUNTTWOWAY",
        //                                booking.TotalDistancePrice.ToString());
        //                            builder.Append(textCruiseportReturn);
        //                        }

        //                        if (returnJourney.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport &&
        //                            returnJourney.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
        //                        {
        //                            string logFilePathTwoWayBooking =
        //                                System.Web.HttpContext.Current.Server.MapPath(
        //                                    string.Format("~/EmailTemplets/Booking/ReturnBooking.txt"));
        //                            string textTwoWay = System.IO.File.ReadAllText(logFilePathTwoWayBooking);

        //                            textTwoWay = textTwoWay.Replace("BOOKINGDATETIMERETURN",
        //                                returnJourney.BookingDateTime.ToString());
        //                            textTwoWay = textTwoWay.Replace("BOOKINGAMOUNTTWOWAY",
        //                                booking.TotalDistancePrice.ToString());
        //                            builder.Append(textTwoWay);

        //                        }
        //                    }

        //                }

        //                string logFilePathAddressDetails =
        //                    System.Web.HttpContext.Current.Server.MapPath(
        //                        string.Format("~/EmailTemplets/Booking/BookingAddress.txt"));
        //                string addresstext = System.IO.File.ReadAllText(logFilePathAddressDetails);
        //                //addresstext = addresstext.Replace("CUSTOMERCOMMENTS", booking.CustomerComment);
        //                //addresstext = addresstext.Replace("MAPURL", booking.MapUrl);
        //                addresstext = addresstext.Replace("BOOKING-DATE-TIME", booking.BookingDateTime.ToString());
        //                addresstext = addresstext.Replace("FROM-ADDRESS", booking.BookingFrom);
        //                //addresstext = addresstext.Replace("FROMTOWN", booking.FromPostTown);
        //                //addresstext = addresstext.Replace("FROMPOSTCODE", booking.FromPostCode);
        //                addresstext = addresstext.Replace("TO-ADDRESS", booking.BookingTo);
        //                //addresstext = addresstext.Replace("TOTOWN", booking.ToPostTown);
        //                //addresstext = addresstext.Replace("TOPOSTCODE", booking.ToPostCode);
        //                builder.Append(addresstext);

        //                if (booking.IsReturnBooking)
        //                {
        //                    string logFilePathReturnBookingAddress =
        //                        System.Web.HttpContext.Current.Server.MapPath(
        //                            string.Format("~/EmailTemplets/Booking/BookingAddressReturn.txt"));
        //                    string returnBookingTxt = System.IO.File.ReadAllText(logFilePathReturnBookingAddress);
        //                    if (returnJourney.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport &&
        //                        returnJourney.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
        //                    {
        //                        returnBookingTxt = returnBookingTxt.Replace("RETURN-DATE-TIME",
        //                            returnJourney.BookingDateTime.ToString());
        //                    }
        //                    else
        //                    {
        //                        returnBookingTxt = returnBookingTxt.Replace("RETURNDATETIME",
        //                            returnJourney.BookingDateTime.ToString());
        //                    }
        //                    returnBookingTxt = returnBookingTxt.Replace("FROM-ADDRESS", returnJourney.BookingFrom);
        //                    //returnBookingTxt = returnBookingTxt.Replace("FROMTOWN", returnJourney.FromPostTown);
        //                    //returnBookingTxt = returnBookingTxt.Replace("FROMPOSTCODE", returnJourney.FromPostCode);
        //                    returnBookingTxt = returnBookingTxt.Replace("TO-ADDRESS", returnJourney.BookingTo);
        //                    //returnBookingTxt = returnBookingTxt.Replace("TOTOWN", returnJourney.ToPostTown);
        //                    //returnBookingTxt = returnBookingTxt.Replace("TOPOSTCODE", returnJourney.ToPostCode);
        //                    builder.Append(returnBookingTxt);

        //                }
        //                else
        //                {
        //                    string fileClosingDiv =
        //                        System.Web.HttpContext.Current.Server.MapPath(
        //                            string.Format("~/EmailTemplets/Booking/ClosingDiv.txt"));
        //                    string fileClosingDivTxt = System.IO.File.ReadAllText(fileClosingDiv);
        //                    builder.Append(fileClosingDivTxt);
        //                }

        //                string fileMap =
        //                    System.Web.HttpContext.Current.Server.MapPath(
        //                        string.Format("~/EmailTemplets/Booking/Map.txt"));
        //                string textMap = System.IO.File.ReadAllText(fileMap);
        //                textMap = textMap.Replace("MAP-URL", booking.MapUrl);
        //                builder.Append(textMap);

        //                string fileLuggageInfo =
        //                    System.Web.HttpContext.Current.Server.MapPath(
        //                        string.Format("~/EmailTemplets/Booking/LuggageInfo.txt"));
        //                string textLuggage = System.IO.File.ReadAllText(fileLuggageInfo);


        //                textLuggage = textLuggage.Replace("BOOKINGVEHICLETYPES", booking.VehicleTypes);
        //                textLuggage = textLuggage.Replace("NUMBEROFPESSANGERS", booking.NumberOfPassengers.ToString());
        //                textLuggage = textLuggage.Replace("NUMBEROFBABYSEATS", (booking.ChildSeat ?? 0).ToString());
        //                textLuggage = textLuggage.Replace("NUMBEROFLUGGAHES", booking.Luggage.ToString());
        //                textLuggage = textLuggage.Replace("NUMBEROFHANDLUGGAHES",
        //                    booking.HandLuggage.ToString());
        //                textLuggage = textLuggage.Replace("BOOKINGAMOUNT",
        //                    (booking.OtherCharges + booking.TotalDistancePrice).ToString());
        //                builder.Append(textLuggage);

        //                string logFilePathFooderDetals =
        //                    System.Web.HttpContext.Current.Server.MapPath(
        //                        string.Format("~/EmailTemplets/Booking/FooderDetals.txt"));
        //                string foodertext = System.IO.File.ReadAllText(logFilePathFooderDetals);
        //                builder.Append(foodertext);



        //                var listEmails = new List<string>();
        //                //if (booking.Email)
        //                //{
        //                //    listEmails.Add(booking.PassengerEmail);
        //                //    listEmails.Add(booking.BookedPersonEmail);
        //                //}
        //                //else
        //                //{

        //                listEmails.Add(booking.Email);
        //                // }
        //                // listEmails.Add(booking.PassengerEmail);
                         
        //                    string _emailSubject = ConfigHelper.GetEmailSubject(
        //                        model.IsConfirmBooking
        //                            ? (int) BookingStatusEnum.Confirmed
        //                            : model.IsCancelBooking ? (int) BookingStatusEnum.Cancelled : 0);

        //                    if (!string.IsNullOrEmpty(_emailSubject))
        //                    {
        //                        //Srikanth, Your Reservation Status at batransfer.com - Ref: BA479068
        //                        _emailSubject = _emailSubject.Replace("{0}", booking.BookedPersonName);
        //                        _emailSubject = _emailSubject.Replace("{1}", booking.BookingNumber);
        //                    }

        //                    EmailCredentialsViewModel emailCredentials =
        //                        EmailHelper.GetBookingNotificationEmailCredentials();
        //                    var email = new RootEmail();
        //                    email.Attachments = null;
        //                    email.Body = builder.ToString();
        //                    email.FromEmailAddress = emailCredentials.FromEmail;
        //                    email.Username = emailCredentials.Username;
        //                    email.Password = emailCredentials.Password;
        //                    email.Port = emailCredentials.Port;
        //                    email.Smtp = emailCredentials.Smtp;

        //                    email.Subject = _emailSubject;
        //                    email.ToEmailAddresses = listEmails;
        //                    email.DisplayName = emailCredentials.DisplayName;
        //                    email.IsEnableSsl = emailCredentials.IsEnabledSSL;
        //                    string apiUrl = EmailHelper.GetEmailUrl((int) ApiUrlEnum.EmailWithoutAttachmentApi);
        //                    EmailHelper.SendEmailWithoutAttachtment(email, apiUrl);
        //                }

                    
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
        //            System.Reflection.MethodBase.GetCurrentMethod().Name,
        //            ex.InnerException != null ? ex.InnerException.Message : "");
        //    }
        //}
        #endregion
    }
}