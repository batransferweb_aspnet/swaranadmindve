﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models.DriverMo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwaranAdmin_New.DataAccess.DriverDal
{
    public class DriverApplicationDal
    {
        #region Singalton
        private static DriverApplicationDal _instance;
        private static readonly object LockThis = new object();
        protected DriverApplicationDal()
        {
        }



        public static DriverApplicationDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new DriverApplicationDal());
                }
            }
        }

       
        #endregion

        public List<DriverApplicationViewModel> GetDriverJobjs(int status)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                     IEnumerable<DriverApplicationViewModel> objDriverApp=db.sp_swaran_driver_applications(status).ToList().Select(x=> new DriverApplicationViewModel(){
                         AppliedDate=x.sysCreatedDate.ToShortDateString(),
                         DriverApplicationId=x.DriverApplicationId,
                         JobType=x.PreferToWork,
                         Name=x.TitleName+" "+x.FirstName+" "+x.LastName,
                         Status=Enum.GetName(typeof(DriverJobStatusEnum), x.Status??1),
                         StatusId= x.Status??1,
                     });

                    return objDriverApp.ToList();
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public bool  ChaneDriverJobStatus(long appid, int statusid)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objDriverApp = db.DriverApplications.FirstOrDefault(x => x.DriverApplicationId == appid);
                    if (objDriverApp != null)
                    { 
                        objDriverApp.Status = statusid;
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
             }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
                  return false;
            }
        }

        public CurrentDriverApplicationViewModel GetCurrentDriverJob(long driverappid)
        {
            var currentApp = new CurrentDriverApplicationViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    var driverApp = db.sp_swaran_driver_current_applications(driverappid).FirstOrDefault();
                    if(driverApp != null)
                    {
                        currentApp.DriverApplicationId = driverApp.DriverApplicationId;
                        currentApp.FullName = driverApp.TitleName+" "+ driverApp.FirstName+" "+ driverApp.LastName;
                        currentApp.FirstName = driverApp.FirstName;
                        currentApp.LastName = driverApp.LastName;
                        currentApp.FullAddress = driverApp.FullAddress;
                        currentApp.Postcode = driverApp.Postcode;
                        currentApp.PostTown = driverApp.PostTown;
                        currentApp.Street = driverApp.Street;
                        currentApp.HomePhone = driverApp.HomePhone;
                        currentApp.MobilePhone = driverApp.MobilePhone;
                        currentApp.Email = driverApp.Email;
                        currentApp.PreferredMethodofContact = driverApp.PreferredMethodofContact;
                        currentApp.DateOfBirth = driverApp.DateOfBirth != null? driverApp.DateOfBirth.Value.ToShortDateString():"";
                        currentApp.YearsDriversLicenceHeld = driverApp.YearsDriversLicenceHeld;
                        currentApp.PreferToWork = driverApp.PreferToWork;
                        currentApp.WillingToStart = driverApp.WillingToStart;
                        currentApp.ExperienceDetail = driverApp.ExperienceDetail;
                        //currentApp.IsEnabled = driverApp.IsEnabled;
                        //currentApp.IsDeleted = driverApp.IsDeleted;
                        //currentApp.sysModifiedDate = driverApp.sysModifiedDate;
                        //currentApp.sysCreatedDate = driverApp.sysCreatedDate;
                        // currentApp.Status = driverApp.Status.ToString();...
                        currentApp.Status = Enum.GetName(typeof(DriverJobStatusEnum), driverApp.Status ?? 1);
                        currentApp.County = driverApp.County;
                        currentApp.PrivateHireLicenceType = driverApp.PrivateHireLicenceType.ToString(); 
                        currentApp.DrivingLicenseType = driverApp.DrivingLicenseType.ToString(); ;
                        currentApp.PointsOnLicense = driverApp.PointsOnLicense.ToString(); ;
                        //currentApp.RegisteredOrganization = driverApp.RegisteredOrganization;
                        //currentApp.Gender = driverApp.Gender;

                        return currentApp;
                    }
                }

                }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return null;
        }

        public string DeleteDriverJob(int id)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objDriverApp = db.DriverApplications.FirstOrDefault(x=>x.DriverApplicationId == id);
                    if(objDriverApp != null)
                    {
                        objDriverApp.IsDeleted = true;
                        objDriverApp.Status = (int)DriverJobStatusEnum.Cancelled;
                        db.SaveChanges();
                        return "successfully removed";
                    }
                    return "Not fount";
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
                return ex.Message;
            }
        }
    }
}