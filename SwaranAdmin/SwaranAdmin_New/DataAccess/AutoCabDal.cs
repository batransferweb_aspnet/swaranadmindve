﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SwaranAdmin_New.DataAccess
{
    public class AutoCabDal
    {
        #region Singalton
        private static AutoCabDal _instance;
        private static readonly object LockThis = new object();
        protected AutoCabDal()
        {
        }



        public static AutoCabDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new AutoCabDal());
                }
            }
        }
        #endregion

        public List<SelectListItem> GetAllVehicleType()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<SelectListItem> objVehicletyprs =
                        db.VehicleTypes.Where(x => x.IsActive && !x.IsDelete)
                            .ToList()
                            .Select(x => new SelectListItem()
                            {
                                Value = x.VehicleTypeID.ToString(),
                                Text = x.VehicleTypeName,
                            });
                    return objVehicletyprs.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public string UpdateAutocabVehicletypes(AutoCabVehicletypeConfigViewModel model)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.VehicleTypes.Any(x => x.VehicleTypeID == model.VehicleTypeId))
                    {
                        var objVehicletype = db.VehicleTypes.First(x => x.VehicleTypeID == model.VehicleTypeId);

                        objVehicletype.AutocabVehicleType = model.VehicleTypeName??"";
                        objVehicletype.AutocabVehicleCategory = model.VehicleCatogory??"";
                        db.SaveChanges();
                        return "Successfully updated";
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
                return "Something went to wrong try again";
            }
            return "";
        }

        public AutoCabVehicletypeConfigViewModel GetVehicletypes(long vehicletypeid)
        {
            var model = new AutoCabVehicletypeConfigViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (db.VehicleTypes.Any(x => x.VehicleTypeID == vehicletypeid))
                    {
                        var objVehicletype = db.VehicleTypes.First(x => x.VehicleTypeID == vehicletypeid);
                        model.VehicleTypeName = objVehicletype.AutocabVehicleType;
                        model.VehicleCatogory = objVehicletype.AutocabVehicleCategory;

                        return model;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return model;
        }
    }
}