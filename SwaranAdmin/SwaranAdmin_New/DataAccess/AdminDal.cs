﻿using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using SwaranAdmin_New.Models.Staff;
using System;
using System.Collections.Generic;
using System.Linq;
using SwaranAdmin_New.DataAccess.Company;
using SwaranAdmin_New.Models.Member;

namespace SwaranAdmin_New.DataAccess
{
    public class AdminDal
    {
        #region Singalton
        private static AdminDal _instance;
        private static readonly object LockThis = new object();
        protected AdminDal()
        {
        }
        public static AdminDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new AdminDal());
                }
            }
        }


        #endregion

        public List<RefundHistoryViewModel> GetRefoundHistory()
        {
            var payment = new PaymentViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {

                    IEnumerable<RefundHistoryViewModel> refund =
                        db.sp_swran_refund_history().ToList().Select(x => new RefundHistoryViewModel()
                        {
                            BookingNumber = x.BookingNumber,
                            BookedPersonName = x.BookedPersonName,
                            BookingId = x.BookingId,
                            BookingFrom = x.BookingFrom,
                            BookingTo = x.BookingTo,
                            BookingStatus = x.BookingStatus,
                            RefundBy = x.RefundBy,
                            Reason = x.Reason,
                            RefundDate = x.RefundDate.ToString("yy/MMM/dd")

                        });
                    return refund.ToList();

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }



        public FreeJourneyAdminViewModel GetFreejourneyConfig()
        {
            var freeJourney = new FreeJourneyAdminViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {


                    var objFreejourneyAriatype = db.CustomerFreeJourneyAreaTypes.Where(x => x.IsEnabled && !x.IsDeleted);
                    var freeJourneyAreatypes = new List<FreeJourneyAreaTypeViewModel>();
                    foreach (var freeItem in objFreejourneyAriatype.ToList())
                    {
                        var free = new FreeJourneyAreaTypeViewModel();
                        var objFree = db.FreeJourneyAreaTypes.First(x => x.JourneyTypeId == freeItem.JourneyTypeId);
                        free.JourneyTypeId = objFree.JourneyTypeId;
                        free.AreaType = objFree.FromAreaType + " --> " + objFree.ToAreaType;
                        freeJourneyAreatypes.Add(free);
                    }
                    freeJourney.FreeJourneyAreaTypes = freeJourneyAreatypes;





                    IEnumerable<FreeJourneyAreaTypeViewModel> objFreeJourneyAreaType =
                      db.FreeJourneyAreaTypes.Select(x => new FreeJourneyAreaTypeViewModel()
                      {
                          AreaType = x.FromAreaType + " --> " + x.ToAreaType,
                          JourneyTypeId = x.JourneyTypeId,
                      });

                    var listFreeJourneyAreatypes = objFreeJourneyAreaType.ToList();
                    foreach (var areatype in freeJourneyAreatypes)
                    {
                        listFreeJourneyAreatypes.RemoveAll(x => x.JourneyTypeId == areatype.JourneyTypeId);
                    }

                    freeJourney.AllAreaTypes = listFreeJourneyAreatypes;


                    if (db.CustomerFreeJourneyTypes.Any(x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney))
                    {
                        var types = new CustomerFreeJourneyTypeViewModel();
                        var freeJourneyType =
                            db.CustomerFreeJourneyTypes.First(
                                x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney);
                        types.CustomerFreeJourneyTypeId = freeJourneyType.CustomerFreeJourneyTypeId;
                        types.FromDateTime = ValueParseHelper.ToStringDate(freeJourneyType.FromDateTime);
                        types.FromPostCode = freeJourneyType.FromPostCode;
                        types.IsDeleted = freeJourneyType.IsDeleted;
                        types.IsDistanceBased = freeJourneyType.IsDistanceBased;
                        types.IsEnabled = freeJourneyType.IsEnabled;
                        types.IsJourneyTypeBased = freeJourneyType.IsJourneyTypeBased;
                        types.IsPriceBased = freeJourneyType.IsPriceBased;
                        types.IsTwoWay = freeJourneyType.IsTwoWay;
                        types.IsVehicleBased = freeJourneyType.IsVehicleBased;
                        types.MaxDistance = freeJourneyType.MaxDistance;
                        types.MaxPrice = freeJourneyType.MaxPrice;
                        types.Name = freeJourneyType.Name;
                        types.ToDateTime = ValueParseHelper.ToStringDate(freeJourneyType.ToDateTime);
                        types.ToPostCode = freeJourneyType.ToPostCode;

                        freeJourney.CustomerFreeJourneyType = types;
                    }


                    IEnumerable<FreeJourneyVehicleTypesViewModel> objVehicleTypes =
                        db.CustomerFreeJourneyTypeVehicleTypes.Where(x => x.IsEnabled && !x.IsDeleted)
                            .Select(x => new FreeJourneyVehicleTypesViewModel()
                            {
                                Id = x.VehicleTypeID,
                                Name = x.VehicleType.VehicleTypeName
                            });
                    freeJourney.FreeJourneyVehicleTypes = objVehicleTypes.ToList();


                    IEnumerable<FreeJourneyVehicleTypesViewModel> objAllVehicleTypes =
                       db.VehicleTypes.Where(x => x.IsActive && !x.IsDelete)
                           .Select(x => new FreeJourneyVehicleTypesViewModel()
                           {
                               Id = x.VehicleTypeID,
                               Name = x.VehicleTypeName
                           });
                    var allVehicles = objAllVehicleTypes.ToList();


                    foreach (var vehicle in objVehicleTypes)
                    {
                        allVehicles.RemoveAll(x => x.Id == vehicle.Id);
                    }

                    freeJourney.AllVehicleTypes = allVehicles;
                    return freeJourney;
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public bool SaveDistanceBase(SaveDistanceBaseViewModel model)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (
                        db.CustomerFreeJourneyTypes.Any(
                            x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney))
                    {

                        var freeJourneyType =
                            db.CustomerFreeJourneyTypes.First(
                                x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney);
                        freeJourneyType.IsDistanceBased = model.IsDistanceBasd;
                        freeJourneyType.MaxDistance = model.MaxDistance ?? 0;
                        db.SaveChanges();
                        return true;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }

        public bool SavePriceBase(SavePriceBaseViewModel model)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    if (
                        db.CustomerFreeJourneyTypes.Any(
                            x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney))
                    {

                        var freeJourneyType =
                            db.CustomerFreeJourneyTypes.First(
                                x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney);
                        freeJourneyType.IsPriceBased = model.IsPriceBasd;
                        freeJourneyType.MaxPrice = model.MaxPrice ?? 0;
                        db.SaveChanges();
                        return true;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }

        public bool SaveVehicleBased(SaveVehiclebasedViewModel model)
        {
            try
            {
                using (var db = new BATransferEntities())
                {



                    if (
                        db.CustomerFreeJourneyTypes.Any(
                            x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney))
                    {

                        var freeJourneyType =
                            db.CustomerFreeJourneyTypes.First(
                                x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney);
                        freeJourneyType.IsVehicleBased = model.IsVehicleBased;
                        db.SaveChanges();

                    }
                    if (db.CustomerFreeJourneyTypeVehicleTypes.Any(
                                x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney))
                    {
                        var objAllVehicle = db.CustomerFreeJourneyTypeVehicleTypes.Where(
                            x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney);

                        foreach (var all in objAllVehicle.ToList())
                        {
                            var freeJourneyType =
                              db.CustomerFreeJourneyTypeVehicleTypes.First(
                                  x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney &&
                                       x.VehicleTypeID == all.VehicleTypeID);
                            freeJourneyType.IsEnabled = false;
                            freeJourneyType.IsDeleted = false;
                            db.SaveChanges();
                        }
                    }

                    foreach (var vehicle in model.Items.ToList())
                    {
                        long vehicleTypeId = long.Parse(vehicle.Value);

                        if (
                            db.CustomerFreeJourneyTypeVehicleTypes.Any(
                                x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney &&
                                     x.VehicleTypeID == vehicleTypeId))
                        {

                            var freeJourneyType =
                                db.CustomerFreeJourneyTypeVehicleTypes.First(
                                    x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney &&
                                         x.VehicleTypeID == vehicleTypeId);
                            freeJourneyType.IsEnabled = true;
                            freeJourneyType.IsDeleted = false;
                            db.SaveChanges();

                        }
                        else
                        {
                            var freejourneyVehicle = new CustomerFreeJourneyTypeVehicleType();
                            freejourneyVehicle.CustomerFreeJourneyTypeId = (int)CustomerFreeJourneyTypeEnum.FreeJourney;
                            freejourneyVehicle.VehicleTypeID = vehicleTypeId;
                            freejourneyVehicle.IsEnabled = true;
                            freejourneyVehicle.IsDeleted = false;
                            freejourneyVehicle.sysCreatedDate = DateTime.UtcNow;
                            freejourneyVehicle.sysModifiedDate = DateTime.UtcNow;
                            db.CustomerFreeJourneyTypeVehicleTypes.Add(freejourneyVehicle);
                            db.SaveChanges();


                        }

                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }

        public bool SaveJourneyTypeBased(SaveJourneyTypeBasedViewModel model)
        {
            try
            {
                using (var db = new BATransferEntities())
                {



                    if (
                        db.CustomerFreeJourneyTypes.Any(
                            x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney))
                    {

                        var freeJourneyType =
                            db.CustomerFreeJourneyTypes.First(
                                x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney);
                        freeJourneyType.IsVehicleBased = model.IsJourneyTypeBased;
                        db.SaveChanges();

                    }
                    if (db.CustomerFreeJourneyAreaTypes.Any(
                                x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney))
                    {
                        var objAllareatyps = db.CustomerFreeJourneyAreaTypes.Where(
                            x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney);

                        foreach (var all in objAllareatyps.ToList())
                        {
                            all.IsEnabled = false;
                            all.IsDeleted = false;
                            db.SaveChanges();
                        }
                    }

                    foreach (var areatype in model.Items.ToList())
                    {
                        int journeyTypeId = int.Parse(areatype.Value);

                        if (db.CustomerFreeJourneyAreaTypes.Any(
                                x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney &&
                                     x.JourneyTypeId == journeyTypeId))
                        {

                            var freeJourneyType = db.CustomerFreeJourneyAreaTypes.First(
                                x => x.CustomerFreeJourneyTypeId == (int)CustomerFreeJourneyTypeEnum.FreeJourney &&
                                     x.JourneyTypeId == journeyTypeId);
                            freeJourneyType.IsEnabled = true;
                            freeJourneyType.IsDeleted = false;
                            db.SaveChanges();

                        }
                        else
                        {
                            var freejourneyAreatype = new CustomerFreeJourneyAreaType();
                            freejourneyAreatype.CustomerFreeJourneyTypeId = (int)CustomerFreeJourneyTypeEnum.FreeJourney;
                            freejourneyAreatype.JourneyTypeId = journeyTypeId;
                            freejourneyAreatype.IsEnabled = true;
                            freejourneyAreatype.IsDeleted = false;
                            freejourneyAreatype.sysCreatedDate = DateTime.UtcNow;
                            freejourneyAreatype.sysModifiedDate = DateTime.UtcNow;
                            db.CustomerFreeJourneyAreaTypes.Add(freejourneyAreatype);
                            db.SaveChanges();


                        }

                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }

        public List<PendingStaffViewModel> GetPendingStaff()
        {

            try
            {
                using (var db = new BATransferEntities())
                {

                    IEnumerable<PendingStaffViewModel> staffs =
                        db.StaffProfiles.Where(sp => !sp.IsEnabled && !sp.IsOldUser).ToList().Select(x => new PendingStaffViewModel()
                        {
                            MemberId = x.AspNetUser.MemberId,
                            StaffProfileId = x.StaffProfileId,
                            StaffName = x.AspNetUser.FirstName + " " + x.AspNetUser.LastName,
                            Email = x.AspNetUser.Email,
                            ContactNumber = x.AspNetUser.PrimaryPhoneNumber

                        });
                    return staffs.ToList();

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public bool ApproveStaff(long staffId)
        {

            try
            {
                using (var db = new BATransferEntities())
                {

                    var relevantStaffProfile = db.StaffProfiles.FirstOrDefault(sp => sp.StaffProfileId == staffId && !sp.IsEnabled);

                    if (relevantStaffProfile != null)
                    {
                        relevantStaffProfile.IsEnabled = true;
                        db.SaveChanges();
                    }

                    return true;

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }

        public List<StaffViewModel> GetAllStaff()
        {

            try
            {
                using (var db = new BATransferEntities())
                {

                    IEnumerable<StaffViewModel> staffs =
                        db.StaffProfiles.Where(sp => !sp.IsDeleted).ToList().Select(x => new StaffViewModel()
                        {
                            MemberId = x.AspNetUser.MemberId,
                            StaffProfileId = x.StaffProfileId,
                            StaffName = x.AspNetUser.FirstName + " " + x.AspNetUser.LastName,
                            Email = x.AspNetUser.Email,
                            ContactNumber = x.AspNetUser.PrimaryPhoneNumber,
                            IsEnabled = x.IsEnabled

                        });
                    return staffs.ToList();

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public bool ModifyStaff(long staffId, bool isEnabled)
        {

            try
            {
                using (var db = new BATransferEntities())
                {

                    var relevantStaffProfile = db.StaffProfiles.FirstOrDefault(sp => sp.StaffProfileId == staffId);

                    if (relevantStaffProfile != null)
                    {
                        relevantStaffProfile.IsEnabled = isEnabled;
                        db.SaveChanges();
                    }

                    return true;

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }

        public List<AdminAllBookings> Getallbookings(int minimumCount, int numberOfRecord, int currentpage)
        {
            try
            {
                using (var db = new BATransferEntities())
                {

                    IEnumerable<AdminAllBookings> objAllBookings =
                        db.sp_swaran_totalbookings(minimumCount, numberOfRecord, currentpage)
                            .ToList()
                            .Select(x => new AdminAllBookings()
                            {
                                BookedPersonEmail = x.BookedPersonEmail,
                                BookedPersonName = x.BookedPersonName,
                                BookedPersonTel = x.BookedPersonTel,
                                BookingCount = x.BookingCount ?? 0,
                                NumberOfPages = x.NumberOfPages ?? 0,
                                UserId = x.UserId

                            });

                    return objAllBookings.ToList();

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");


            }
            return null;
        }

        public List<CustomerSavedCardViewModel> GetCustomerCards()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<CustomerSavedCardViewModel> objSavedCard =
                        db.sp_swaran_customer_saved_card().ToList().Select(x => new CustomerSavedCardViewModel()
                        {
                             Email = x.Email,
                             CardClass = x.CardClass,
                             CardIssuer = x.CardClass,
                             CardSchemeName = x.CardSchemeName, 
                             CardSchemeType = x.CardSchemeType,
                             CardType = x.CardType,
                             CustomerCardId = x.CustomerCardId,
                             Expiry = x.Expiry,
                             FullName = x.FullName,
                             MemberId = x.MemberId,
                             MaskedCardNumber = x.MaskedCardNumber,
                             Name = x.Name,
                             Type = x.Type
                        });
                    return objSavedCard.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return null;
        }

        public bool DeleteCustomerCards(CustomerDeleteSavedCardViewModel model)
        {
            try
            {
                using (var db=new BATransferEntities())
                {
                    var objCustomerCd = db.CustomerCards.FirstOrDefault(x => x.CustomerCardId == model.Customercard);
                    if (objCustomerCd != null)
                    {
                        db.CustomerCards.Remove(objCustomerCd);
                        db.SaveChanges();
                        return true;
                    }
                }
               

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                   System.Reflection.MethodBase.GetCurrentMethod().Name,
                   ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return false;
        }
        public ViapintConfigViewModel GetViapointConfig()
        {
            ViapintConfigViewModel config = new ViapintConfigViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objViapoint = db.ViapointConfigs.FirstOrDefault(x => x.ConfigId == (int)ViapointConfigEnum.PriceCalculationId);
                    if (objViapoint != null)
                    {
                        config.MileRate = objViapoint.MileRate;
                        config.TimeRate = objViapoint.TimeRate??0;
                        config.MinimumRate = objViapoint.MinimumRate;
                        config.IsFastViaRoute = objViapoint.IsFastRoute;

                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return config;
        }

        public string SaveViapointConfig(ViapintConfigViewModel config)
        {

            try
            {
                using (var db = new BATransferEntities())
                {
                    var objViapoint = db.ViapointConfigs.FirstOrDefault(x => x.ConfigId == (int)ViapointConfigEnum.PriceCalculationId);
                    if (objViapoint != null)
                    {
                       objViapoint.MileRate=config.MileRate ;
                        objViapoint.TimeRate = config.TimeRate;
                        objViapoint.MinimumRate= config.MinimumRate;
                       objViapoint.IsFastRoute= config.IsFastViaRoute ;
                        db.SaveChanges();
                        return "Successfully updated";
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return "try again";
        }

        public List<PushNotificationConfigModel> GetPushNotificationConfiguration()
        {
            List<PushNotificationConfigModel> configs = new List<PushNotificationConfigModel>();
            try
            {
                using (var db = new BATransferEntities())
                {
                    configs = db.PushNotificationTypeEnums.Select(pte => new PushNotificationConfigModel()
                    {
                        PushNotificationTypeEnumId = pte.PushNotificationTypeEnumId,
                        PushNotificationTypeEnumName = pte.PushNotificationTypeEnumName,
                        Message = pte.Message,
                        Title = pte.Title,
                        IsAppendBookingNumberForMessage = pte.IsAppendBookingNumberForMessage,
                        IsAppendBookingNumberForTitle = pte.IsAppendBookingNumberForTitle
                    }).ToList();

                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return configs;
        }

        public PushNotificationConfigModel GetPushNotificationConfigurationById(int pushNotificationTypeId)
        {
            PushNotificationConfigModel config = new PushNotificationConfigModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    var relevantConfig = db.PushNotificationTypeEnums.FirstOrDefault(pte => pte.PushNotificationTypeEnumId == pushNotificationTypeId);

                    if(relevantConfig != null)
                    {
                        config = new PushNotificationConfigModel()
                        {
                            PushNotificationTypeEnumId = relevantConfig.PushNotificationTypeEnumId,
                            PushNotificationTypeEnumName = relevantConfig.PushNotificationTypeEnumName,
                            Message = relevantConfig.Message,
                            Title = relevantConfig.Title,
                            IsAppendBookingNumberForMessage = relevantConfig.IsAppendBookingNumberForMessage,
                            IsAppendBookingNumberForTitle = relevantConfig.IsAppendBookingNumberForTitle
                        };
                    }                    

                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return config;
        }

        public string SavePushNotificationConfiguration(PushNotificationConfigModel config, out bool isSuccess)
        {

            try
            {
                using (var db = new BATransferEntities())
                {
                    var relevantConfig = db.PushNotificationTypeEnums.FirstOrDefault(pte => pte.PushNotificationTypeEnumId == config.PushNotificationTypeEnumId);

                    if (relevantConfig != null)
                    {
                        relevantConfig.Message = config.Message;
                        relevantConfig.Title = config.Title;
                        relevantConfig.IsAppendBookingNumberForMessage = config.IsAppendBookingNumberForMessage;
                        relevantConfig.IsAppendBookingNumberForTitle = config.IsAppendBookingNumberForTitle;
                        db.SaveChanges();
                        isSuccess = true;
                        return "Successfully updated";
                    }
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");
            }
            isSuccess = false;
            return "try again";
        }

        public SaveMessageViewModel UpdateBannerText(BanarTextViewModel model)  
        {
            var message = new SaveMessageViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objViapoint = db.BannerTexts.FirstOrDefault(x => x.SiteId == model.SiteId);
                    if (objViapoint != null)
                    {
                        objViapoint.Message = model.Message;
                         objViapoint.ForColor = model.ForColor;
                        objViapoint.BackColor = model.BackColor;
                        objViapoint.IsEnabeld = model.IsEnabeld;
                        objViapoint.IsDeleted = false;
                        db.SaveChanges();
                        message.IsSaved = true;
                        message.Message = "Successfully updated";
                        return message;
                    }
                    else
                    {
                        var bannerText = new BannerText();
                        bannerText.SiteId = model.SiteId;
                        bannerText.Message = model.Message;
                        bannerText.ForColor = model.ForColor;
                        bannerText.BackColor = model.BackColor;
                        bannerText.IsEnabeld = model.IsEnabeld;
                        bannerText.IsDeleted = false;
                        db.BannerTexts.Add(bannerText);
                        db.SaveChanges();
                        message.IsSaved = true;
                        message.Message = "Successfully Saved";
                        return message;
                    }
                }

            }
            catch (Exception ex)
            {
                message.IsSaved = false;
                message.Message = ex.Message;
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return message;  
        }

        public List<OrganizationViewModel> GetOrganizations()
        {
            try
            {
                using (var db=new BATransferEntities())
                {
                    IEnumerable<OrganizationViewModel> objOrg =
                        db.Organizations.Where(x => x.IsEnable).Select(x => new OrganizationViewModel()
                        {
                            SiteId = x.OrganizationId,
                            SiteName = x.OrganizationName
                        });
                    return objOrg.ToList();
                }

            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }


        public BanarTextViewModel GetBannerMessage(int siteId)
        {
            var bannerText = new BanarTextViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objViapoint = db.BannerTexts.FirstOrDefault(x => x.SiteId == siteId);
                    if (objViapoint != null)
                    {
                        bannerText.Message = objViapoint.Message;
                        bannerText.ForColor = objViapoint.ForColor;
                        bannerText.BackColor = objViapoint.BackColor;
                        bannerText.IsEnabeld = objViapoint.IsEnabeld;
                        bannerText.IsDeleted = false;
                       
                        return bannerText;
                    }
                    
                }

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace,
                    System.Reflection.MethodBase.GetCurrentMethod().Name,
                    ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bannerText;
        }

    }
}
