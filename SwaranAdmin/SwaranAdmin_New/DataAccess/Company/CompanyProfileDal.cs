﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models.Company;

namespace SwaranAdmin_New.DataAccess.Company
{
    public class CompanyProfileDal
    {
        #region Singalton
        private static CompanyProfileDal _instance;
        private static readonly object LockThis = new object();
        protected CompanyProfileDal()
        {
        }
        public static CompanyProfileDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new CompanyProfileDal());
                }
            }
        }


        #endregion

        public List<CompanyProfileViewModel> GetCompanyProfiles()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<CompanyProfileViewModel> objProf =
                        db.CompanyProfiles.Where(x => x.IsApproved && x.IsEnabled)
                            .Select(x => new CompanyProfileViewModel()
                            {
                                CompanyId = x.CompanyId,
                                CompanyName = x.CompanyName,
                                CompanyRegNumber = x.CompanyRegNumber,
                                ContractReference = x.ContractReference,
                                RateFormula = x.RateFormula,
                                RateName = x.RateName,
                                PaymentType = x.PaymentType,
                                CompanyEmail = x.Email
                            }); 
                    return objProf.ToList();
                }

            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return null;
        }

        public string GetEmail(string userid)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                     var objCont=db.ContactPersons.FirstOrDefault(x => x.UserId.Equals(userid));
                    if(objCont != null)
                    {
                        return objCont.Email;
                    }

                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return string.Empty;
        }

        public SaveMessageViewModel SaveCompanyUser(CompanyUserViewModel model)
        {
            var message = new SaveMessageViewModel();
           try
            {
                using (var db=new BATransferEntities())
                {
                    if (string.IsNullOrEmpty(model.UserId)) 
                    {
                        message.IsSaved = false;
                        message.Message = "Please select user";
                        return message;
                    }
                   
                    if (model.ComapanyId == 0)
                    {
                        message.IsSaved = false;
                        message.Message = "Please select company";
                        return message;
                    }
                    if (string.IsNullOrEmpty(model.CompanyEmail))
                    {
                        message.IsSaved = false;
                        message.Message = "Please enter valid company email id";
                        return message;
                    }

                    var objAspUser = db.AspNetUsers.FirstOrDefault(x => x.Id.Equals(model.UserId));
                        if (objAspUser != null)
                        {
                            objAspUser.IsCompanyUser = true;
                            objAspUser.CompanyId = model.ComapanyId;
                            db.SaveChanges();
                        } 

                    var objCompany = db.CompanyProfiles.FirstOrDefault(c => c.CompanyId == model.ComapanyId);
                    if (objCompany != null)
                    { 
                        objCompany.RateName = model.RateName; 
                        objCompany.RateFormula = model.RateFormula;
                        db.SaveChanges();
                    }

                    var companyCustomer = db.ContactPersons.FirstOrDefault(x => x.UserId.Equals(model.UserId));
                    if (companyCustomer != null)
                    {
                        companyCustomer.CompanyId = model.ComapanyId;
                        companyCustomer.Email = model.CompanyEmail;
                        db.SaveChanges();
                    }
                    else
                    {
                        var company = new ContactPerson();
                        company.UserId = model.UserId;
                        company.CompanyId = model.ComapanyId;
                        company.FirstName = "";
                        company.LastName = "";
                        company.Position = "";
                        company.Department = "";
                        company.Telephone01 = "";
                        company.Telephone02 = "";
                        company.Email = model.CompanyEmail;
                        company.IsAdmin = false;
                        company.IsEnabled = true;
                        company.IsDeleted = false;
                        company.sysModifiedDate = DateTime.UtcNow;
                        company.sysCreatedDate = DateTime.UtcNow;
                        db.ContactPersons.Add(company);
                        db.SaveChanges();

                    }

                    message.IsSaved = true;
                    message.Message = "Successfully updated ";
                    return message;
                } 
            }
            catch (Exception ex)
            { 
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
                message.IsSaved = false;
                message.Message = ex.Message;
                return message;
            }
            
        }

        public SaveMessageViewModel SaveContractReference (ContractReferanceViewModel model)
        {
            var message = new SaveMessageViewModel();
            try
            {
                if (string.IsNullOrEmpty(model.CompanyEmail))
                {
                    message.IsSaved = false;
                    message.Message = "Please enter valid company email id";
                    return message;
                }

                if (model.ComapanyId == 0)
                {
                    message.IsSaved = false;
                    message.Message = "Please select company";
                    return message;
                }
                using (var db = new BATransferEntities())
                { 

                    var objCompany = db.CompanyProfiles.FirstOrDefault(c => c.CompanyId == model.ComapanyId);
                    if (objCompany != null)
                    {
                        objCompany.ContractReference = model.ContractReference; 
                        objCompany.Email = model.CompanyEmail;
                        objCompany.PaymentType = model.PaymentType;
                        db.SaveChanges();
                    }
                    message.IsSaved = true;
                    message.Message = "Successfully Updated ";
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
                message.IsSaved = false;
                message.Message = ex.Message;
                return message;
            }
            return message;
        }
        public List<sp_swaran_get_account_customer_Result> GetAccountUsers()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objAccCus = db.sp_swaran_get_account_customer().ToList();
                    return objAccCus.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
               
            }
            return null;
        }


        public string RemoveAccountUsers(long memberId)
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objUser = db.AspNetUsers.FirstOrDefault(x => x.MemberId == memberId);
                    if (objUser != null)
                    {
                        objUser.IsCompanyUser = false;
                        objUser.CompanyId = null;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
                return ex.Message;
            }
            return "Successfully removed";
        }

        public List<CompanyViewModel> GetCompanyList()
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    IEnumerable<CompanyViewModel> objCompany =
                        db.CompanyProfiles.Where(x => !x.IsDeleted && x.IsEnabled).Select(x => new CompanyViewModel()
                        {
                            CompanyId=x.CompanyId,
                            CompanyName = x.CompanyName,
                            CompanyRegNumber=x.CompanyRegNumber,
                            ContractReference=x.ContractReference,
                            Email=x.Email,
                            //IsApproved = x.IsApproved,
                            NatureOfBusiness = x.NatureOfBusiness,
                            PaymentType = x.PaymentType,
                            TelephoneNumber = x.TelephoneNumber,
                            Website=x.Website,
                            AddressLine1 = x.AddressLine1,
                            AddressLine2 = x.AddressLine2,
                             Street = x.Street,
                             Town = x.Town,
                             AuthorisedBy = x.AuthorisedBy,
                             Fax = x.Fax,   
                             IsEnabled = x.IsEnabled,
                             IsDeleted = x.IsDeleted,
                             IsMarketingEmailEnabled = x.IsMarketingEmailEnabled,

                });
                    return objCompany.ToList();
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
                
            }
            return null;
        }

        public  CompanyViewModel GetCompany(long companyId)
        {
            var compa = new CompanyViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objCompany =
                        db.CompanyProfiles.FirstOrDefault(x => !x.IsDeleted && x.IsEnabled &&  x.CompanyId == companyId);
                        if(objCompany != null)
                        {
                            compa.CompanyId = objCompany.CompanyId;
                            compa.CompanyName = objCompany.CompanyName;
                            compa.CompanyRegNumber = objCompany.CompanyRegNumber;
                            compa.ContractReference = objCompany.ContractReference;
                            compa.Email = objCompany.Email;
                           // compa.IsApproved = objCompany.IsApproved;
                            compa.NatureOfBusiness = objCompany.NatureOfBusiness;
                            compa.PaymentType = objCompany.PaymentType;
                            compa.TelephoneNumber = objCompany.TelephoneNumber;
                            compa.Website = objCompany.Website;
                            compa.AddressLine1 = objCompany.AddressLine1;
                            compa.AddressLine2 = objCompany.AddressLine2;
                            compa.PostalCode = objCompany.PostalCode;
                            compa.Street = objCompany.Street;
                            compa.Town = objCompany.Town;
                            compa.AuthorisedBy = objCompany.AuthorisedBy;
                            compa.CompanyEstablishedYear = objCompany.CompanyEstablishedYear;
                            compa.Fax = objCompany.Fax;  
                            compa.IsEnabled = objCompany.IsEnabled;
                            compa.IsDeleted = objCompany.IsDeleted;
                            compa.IsMarketingEmailEnabled = objCompany.IsMarketingEmailEnabled;
                    }
                    return compa;
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return null;
        }


        public SaveMessageViewModel UpdateCompanyProfile(CompanyViewModel model) 
        {
            var message = new SaveMessageViewModel(); 
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objCompany =
                        db.CompanyProfiles.FirstOrDefault(x => !x.IsDeleted && x.IsEnabled && x.CompanyId == model.CompanyId);
                    if (objCompany != null)
                    { 
                        objCompany.CompanyName = model.CompanyName;
                        objCompany.CompanyRegNumber = model.CompanyRegNumber; 
                        objCompany.Email = model.Email;
                        //compa.IsApproved = objCompany.IsApproved;
                        objCompany.NatureOfBusiness = model.NatureOfBusiness;
                        objCompany.PaymentType = model.PaymentType;
                        objCompany.ContractReference = model.ContractReference;
                        objCompany.PostalCode = model.PostalCode;
                        objCompany.Street = model.Street;
                        objCompany.Town = model.Town;
                        objCompany.AuthorisedBy = model.AuthorisedBy;
                        objCompany.CompanyEstablishedYear = model.CompanyEstablishedYear;
                        objCompany.Fax = model.Fax; 
                        objCompany.TelephoneNumber = model.TelephoneNumber;
                        objCompany.Website = model.Website;
                        objCompany.AddressLine1 = model.AddressLine1;
                        objCompany.AddressLine2 = model.AddressLine2;
                        objCompany.IsEnabled = model.IsEnabled;
                        objCompany.IsDeleted = model.IsDeleted;
                        objCompany.IsMarketingEmailEnabled = model.IsMarketingEmailEnabled;
                        db.SaveChanges();
                        message.IsSaved = true;
                        message.Message = "Successfully Updated ";
                    }
                   
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
                message.IsSaved = false;
                message.Message = ex.Message;
                
            }
            return message;
        }
        public string Removeaccount(long companyid)  
        {
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objUser = db.CompanyProfiles.FirstOrDefault(x => x.CompanyId == companyid);
                    if (objUser != null)
                    {
                        objUser.IsDeleted = true;
                        objUser.IsEnabled = false;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
                return ex.Message;
            }
            return "Successfully removed";
        }
        
    }

    public class SaveMessageViewModel
    {
        public bool IsSaved { get; set; }
        public string Message { get; set; } 
    }
}