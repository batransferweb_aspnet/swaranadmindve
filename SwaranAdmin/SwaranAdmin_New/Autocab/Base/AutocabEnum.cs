﻿namespace SwaranAdmin_New.Autocab.Base
{

    public enum AutoCabConfigurationEnum
    {
        AgentId = 1,
        AgentPassword = 2,
        VendorId = 3,
        ApiUrl = 4,
        PaymentType = 5,
        ContractReference = 6,
        FreeJourneyPaymentType = 7,
        FreeJourneyContractReference = 8,
        CashAccountPaymentType = 9,
        CashAccountContractReference = 10,
        UseMyCreditPaymentType = 11,
        UseMyCreditContractReference = 12,
    }

}