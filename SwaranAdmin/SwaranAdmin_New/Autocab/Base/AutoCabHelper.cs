﻿using System.Configuration;
using SwaranAdmin_New.DataManager;
using System.Linq;

namespace SwaranAdmin_New.Autocab.Base
{
    public class AutoCabHelper
    {
        public static AutoCabConfigurationDom GetAutoCabConfigurationById(int autoCabConfigurationId) 
        {

            AutoCabConfigurationDom autoCabConfigurationResponse = null;

            using (var db = new BATransferEntities())
            {
                var relevantAutoCabConfiguration = db.AutoCabConfigurations.FirstOrDefault(acc => acc.AutoCabConfigurationId == autoCabConfigurationId);

                if (relevantAutoCabConfiguration != null)
                {
                    autoCabConfigurationResponse = new AutoCabConfigurationDom()
                    {
                        AutoCabConfigurationId = relevantAutoCabConfiguration.AutoCabConfigurationId,
                        AutoCabConfigurationName = relevantAutoCabConfiguration.AutoCabConfigurationName,
                        AutoCabConfigurationValue = relevantAutoCabConfiguration.AutoCabConfigurationValue
                    };
                }
            }

            return autoCabConfigurationResponse;
        }

        public static AutoCabAgentViewModel GetAutoCabAgentConfig()
        {
            var autoConfig = new AutoCabAgentViewModel();
            using (var db = new BATransferEntities())
            {
                int agentConfigId = int.Parse(ConfigurationManager.AppSettings["AutoCabAgentConfigurationId"]);
                var AutoCabAgentSource = ConfigurationManager.AppSettings["AutoCabAgentSource"];
                var config = db.AutoCabAgentConfigurations.FirstOrDefault(x => x.AutoCabAgentConfigurationId == agentConfigId && x.IsEnabled && !x.IsDeleted);
                if (config != null)
                {
                    autoConfig.AgentId = config.AgentId;
                    autoConfig.AgentPassword = config.AgentPassword;
                    autoConfig.ApiUrl = config.ApiUrl;
                    autoConfig.CashAccountContractReference = config.CashAccountContractReference;
                    autoConfig.CashAccountPaymentType = config.CashAccountPaymentType;
                    autoConfig.ContractReference = config.ContractReference;
                    autoConfig.FreeJourneyContractReference = config.FreeJourneyContractReference;
                    autoConfig.FreeJourneyPaymentType = config.FreeJourneyPaymentType;
                    autoConfig.PaymentType = config.PaymentType;
                    autoConfig.UseMyCreditContractReference = config.UseMyCreditContractReference;
                    autoConfig.UseMyCreditPaymentType = config.UseMyCreditPaymentType;
                    autoConfig.VendorId = config.VendorId;
                    autoConfig.Source = AutoCabAgentSource;
                }
            }
            return autoConfig;
        }
    }


    public class AutoCabAgentViewModel
    {
        public int AgentId { get; set; }
        public string AgentPassword { get; set; }
        public int VendorId { get; set; }
        public string ApiUrl { get; set; }
        public string PaymentType { get; set; }
        public string ContractReference { get; set; }
        public string FreeJourneyPaymentType { get; set; }
        public string FreeJourneyContractReference { get; set; }
        public string CashAccountPaymentType { get; set; }
        public string CashAccountContractReference { get; set; }
        public string UseMyCreditPaymentType { get; set; }
        public string UseMyCreditContractReference { get; set; }

        public string Source { get; set; }

    }
    public class AutoCabConfigurationDom
    {
        public int AutoCabConfigurationId { get; set; }
        public string AutoCabConfigurationName { get; set; }
        public string AutoCabConfigurationValue { get; set; }
    }
}