﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace  SwaranAdmin_New.Autocab.Base
{
    public class XmlHelper
    {
        public static T Deserialize<T>(string fullPath) where T : class, new()
        {

            using (var stream = new StringReader(fullPath))
            {
                var xmlSerializer = new XmlSerializer(typeof(T));
                return (T)xmlSerializer.Deserialize(stream);
            }


        }

        public static string Serialize<T>(object objectToSerialize)
        {
            //Do cool serialisation here
            T objOutput = (T)objectToSerialize;
            XmlSerializer objSerializer = new XmlSerializer(typeof(T));
            StringWriter objWriter = new StringWriter();
            objSerializer.Serialize(objWriter, objOutput);

            return objWriter.ToString();
        }
        public static string GetXMLFromObject(object o)
        {
            StringWriter sw = new StringWriter();
            XmlTextWriter tw = null;
            try
            {
                XmlSerializer serializer = new XmlSerializer(o.GetType());
                tw = new XmlTextWriter(sw);
                serializer.Serialize(tw, o);
            }
            catch (Exception ex)
            {
                //Handle Exception Code
            }
            finally
            {
                sw.Close();
                if (tw != null)
                {
                    tw.Close();
                }
            }
            return sw.ToString();
        }
    }
}