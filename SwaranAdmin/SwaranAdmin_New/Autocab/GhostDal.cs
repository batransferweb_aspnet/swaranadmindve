﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using SwaranAdmin_New.Autocab.Model;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using SwaranAdmin_New.Autocab.Base;

namespace SwaranAdmin_New.Autocab
{
    public class GhostDal 
    {
        #region Singleton
        private static GhostDal _instance;
        private static readonly object LockThis = new object();
        protected GhostDal()  
        {
        }
        public static GhostDal Instance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new GhostDal());
                }
            }
        }
        #endregion

        public AuthorizationResponse SendToGhost(string token,bool isReturnBooking) 
        {
            string loyalty = string.Empty;
            AuthorizationResponse authorizationResponse = null;

            try
            {
                AutoCabAgentViewModel config = AutoCabHelper.GetAutoCabAgentConfig();

                using (var db=new BATransferEntities())
                {
                    var booking =
                        db.Bookings.FirstOrDefault(
                            x =>
                                x.BookingToken.Equals(token)   && x.IsReturnBooking == isReturnBooking);
                    if (booking == null)
                    {
                        authorizationResponse=new AuthorizationResponse();
                        authorizationResponse.Status = false;
                        authorizationResponse.FailureReason = "Booking not found";
                        return authorizationResponse;
                    }

                    if (db.Customers.Any(x => x.userId.Equals(booking.UserId)))
                    {
                        var objLoyalty = db.Customers.First(x => x.userId.Equals(booking.UserId));
                        loyalty = Enum.GetName((typeof(LoyaltyType_Enum)), objLoyalty.loyaltyMemberType);
                    }

                    bool isSetPrice = bool.Parse(ConfigurationManager.AppSettings["IsSetPrice"]);
                bool isWebPriceEnabled = bool.Parse(ConfigurationManager.AppSettings["IsWebPriceEnabled"]);
                    AgentBookingAvailabilityResponse bookingAvailability = null;

                    if (booking.PaymentMethod != (int) PaymentTypeEnum.Card)
                    {
                        if (booking.PaymentMethod== (int)PaymentTypeEnum.Account)
                        {

                            var objAcc =  GetAccountReference(booking.CompanyId ?? 0);
                            string contractReference = objAcc != null ? objAcc.ContractReference : ""; 
                            string ghostPaymentType = objAcc != null ? objAcc.PaymentType : "";

                            int distancePrice = booking.TotalDistancePrice > 0
                                                         ? (int)booking.TotalDistancePrice : 0;

                            if (string.IsNullOrEmpty(contractReference))
                            {
                                authorizationResponse = new AuthorizationResponse();
                                authorizationResponse.Status = false;
                                authorizationResponse.FailureReason = "Contract reference not set to account";
                                return authorizationResponse;
                            }              
                            
                            bookingAvailability = isSetPrice
                              ? AutoCapVehicletypeRequest.GetBookingAvailabilityMainSetAccountPrice(booking, distancePrice, contractReference, ghostPaymentType)
                              : AutoCapVehicletypeRequest.GetBookingAvailabilityMain(booking);
                            authorizationResponse = AutoCapVehicletypeRequest.BookingAuthorizationRequest(booking,
                                bookingAvailability.AvailabilityReference, distancePrice, loyalty, config);
                        }

                        if (booking.PaymentMethod == (int) PaymentTypeEnum.Free_Journey)
                        {
                            int distancePrice = booking.TotalDistancePrice > 0
                                ? (int) booking.TotalDistancePrice*100
                                : 0;
                            bookingAvailability = isSetPrice
                                ? AutoCapVehicletypeRequest.GetBookingAvailabilityFreeJourneyMainSetPrice(booking,
                                    distancePrice, config)
                                : AutoCapVehicletypeRequest.GetBookingAvailabilityFreeJourneyMain(booking);


                            authorizationResponse = AutoCapVehicletypeRequest.BookingAuthorizationRequest(booking,
                                bookingAvailability.AvailabilityReference, booking.TotalDistancePrice, loyalty, config);
                        }
                        else if (booking.PaymentMethod == (int) PaymentTypeEnum.Use_My_Credits)
                        {
                            decimal price = booking.TotalDistancePrice - booking.CustomerCredit;
                            int distancePrice = price > 0 ? (int) price*100 : 0;
                            bookingAvailability = isSetPrice
                                ? AutoCapVehicletypeRequest.GetBookingAvailabilityUseMyCreditsMainSetPrice(booking,
                                    distancePrice, config)
                                : AutoCapVehicletypeRequest.GetBookingAvailabilityUsMyCreditsMain(booking);


                            authorizationResponse = AutoCapVehicletypeRequest.BookingAuthorizationRequest(booking,
                                bookingAvailability.AvailabilityReference, price, loyalty, config);
                        }
                        else if (booking.PaymentMethod == (int) PaymentTypeEnum.Cash)
                        {
                            int distancePrice = booking.TotalDistancePrice > 0
                                ?   (int)booking.TotalDistancePrice 
                                : 0;
                            bookingAvailability = isSetPrice
                                ? AutoCapVehicletypeRequest.GetBookingAvailabilityMainSetPrice(booking, distancePrice, config)
                                : AutoCapVehicletypeRequest.GetBookingAvailabilityMain(booking);


                            authorizationResponse = AutoCapVehicletypeRequest.BookingAuthorizationRequest(booking,
                                bookingAvailability.AvailabilityReference, distancePrice, loyalty, config);
                        }

                    }
                    else if (booking.PaymentMethod == (int) PaymentTypeEnum.Card)
                    {
                        bookingAvailability = isSetPrice
                                           ? AutoCapVehicletypeRequest.GetBookingAvailabilityReturnMainCardSetPrice(
                                               booking, config)
                                           : AutoCapVehicletypeRequest.GetBookingAvailabilityReturnMainCard(
                                               booking);

                        authorizationResponse =
                            AutoCapVehicletypeRequest.BookingAuthorizationRequestMainCard(
                                booking,
                                bookingAvailability.AvailabilityReference, 0, loyalty, config);
                    }

                        if (db.Booking_TEMP != null && authorizationResponse != null && authorizationResponse.AuthorizationReference != null)
                    {
                        var objBooking = db.Booking_TEMP.FirstOrDefault(x => x.BookingId == booking.BookingId);
                        if (objBooking != null)
                        {
                            objBooking.Autocab_AuthorizationReference = authorizationResponse.AuthorizationReference;
                            objBooking.Autocab_BookingReference = authorizationResponse.BookingReference;
                            objBooking.VendorID = bookingAvailability.Vendor.Id;
                            objBooking.AutocabNumber = authorizationResponse.AuthorizationReference != null &&
                                                       authorizationResponse.AuthorizationReference.Split('-').Length >
                                                       0
                                ? authorizationResponse.AuthorizationReference.Split('-')[2]
                                : "";
                            db.SaveChanges();
                        }
                    }
                    if (authorizationResponse != null  && authorizationResponse.AuthorizationReference != null )
                    {

                            booking.Autocab_AuthorizationReference = authorizationResponse.AuthorizationReference;
                            booking.Autocab_BookingReference = authorizationResponse.BookingReference;
                            booking.VendorID = bookingAvailability.Vendor.Id;
                            booking.AutocabNumber = authorizationResponse.AuthorizationReference != null && authorizationResponse.AuthorizationReference.Split('-').Length > 0
                                                      ? authorizationResponse.AuthorizationReference.Split('-')[2]
                                                      : "";
                        db.SaveChanges();
                    }
            
                
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");

            }
            return authorizationResponse;
        }

        public CorporateAccViewModel GetAccountReference(long companyId)
        {
            var acc = new CorporateAccViewModel();
            try
            {
                using (var db = new BATransferEntities())
                {
                    var objCompany = db.CompanyProfiles.FirstOrDefault(x => x.CompanyId == companyId);
                    if (objCompany != null)
                    {
                        acc.ContractReference = objCompany.ContractReference;
                        acc.PaymentType = objCompany.PaymentType;
                    }
                }
            }
            catch (Exception ex)
            {

                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return acc;
        }



    }
}


public class CorporateAccViewModel
{
    public string ContractReference { get; set; }
    public string PaymentType { get; set; }
}

//        if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay)
//                    {
//                        AuthorizationResponse authorizationResponseReturn = null;
//AgentBookingAvailabilityResponse bookingAvailabilityReturn = null;
//                        if (booking.PaymentMethod == (int)PaymentTypeEnum.Use_My_Credits)
//                        {
//                            decimal price = booking.TotalDistancePrice - booking.ConsumerDebit;
//int distancePrice = price > 0 ? (int)price * 100 : 0;
//bookingAvailabilityReturn = isSetPrice
//    ? AutoCapVehicletypeRequest.GetBookingAvailabilityUseMyCreditsMainSetPrice(booking, distancePrice)
//                                : AutoCapVehicletypeRequest.GetBookingAvailabilityUsMyCreditsMain(booking);

//                            authorizationResponseReturn =
//                           AutoCapVehicletypeRequest.BookingAuthorizationRequestReturn( booking,
//                               bookingAvailabilityReturn.AvailabilityReference, price,loyalty);
//                        }
//                        else
//                        {

//                            int distancePrice = booking.TotalDistancePrice > 0 ? (int)booking.TotalDistancePrice * 100 : 0;

//bookingAvailabilityReturn = isSetPrice
//  ? AutoCapVehicletypeRequest.GetBookingAvailabilityReturnMainSetPrice(booking, distancePrice)
//                              : AutoCapVehicletypeRequest.GetBookingAvailabilityReturnMain( booking);
//                            authorizationResponseReturn =
//                              AutoCapVehicletypeRequest.BookingAuthorizationRequestReturn( booking,
//                                  bookingAvailabilityReturn.AvailabilityReference, distancePrice,loyalty);
//                        }


//                        if (db.Booking_TEMP != null && authorizationResponseReturn.AuthorizationReference != null )
//                        {
//                            var objBooking = db.Booking_TEMP.FirstOrDefault(x => x.BookingId == booking.BookingId);
//                            if (objBooking != null)
//                            {
//                                objBooking.Autocab_AuthorizationReference =
//                                    authorizationResponseReturn.AuthorizationReference;
//                                objBooking.Autocab_BookingReference = authorizationResponseReturn.BookingReference;
//                                objBooking.VendorID = bookingAvailabilityReturn.Vendor.Id;
//                                objBooking.AutocabNumber = authorizationResponseReturn.AuthorizationReference != null &&
//                                                           authorizationResponseReturn.AuthorizationReference.Split('-')
//                                                               .Length > 0
//                                    ? authorizationResponseReturn.AuthorizationReference.Split('-')[2]
//                                    : "";
//                                db.SaveChanges();
//                            }
//                        }
//                        if ( authorizationResponseReturn.AuthorizationReference != null )
//                        {

//                                booking.Autocab_AuthorizationReference =
//                                authorizationResponseReturn.AuthorizationReference;
//                                booking.Autocab_BookingReference = authorizationResponseReturn.BookingReference;
//                                booking.VendorID = bookingAvailabilityReturn.Vendor.Id;
//                                booking.AutocabNumber = authorizationResponseReturn.AuthorizationReference != null && authorizationResponseReturn.AuthorizationReference.Split('-').Length > 0
//                                                      ? authorizationResponseReturn.AuthorizationReference.Split('-')[2]
//                                                      : "";
//                            db.SaveChanges();
//                        }
//                    }