﻿namespace SwaranAdmin_New.Autocab.Model
{

}
/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class AgentBidResponse
{

    private AgentBidResponseAgent agentField;

    private AgentBidResponseResult resultField;

    private string bidReferenceField;

    private object offersField;

    private ushort requestTimeField;

    /// <remarks/>
    public AgentBidResponseAgent Agent
    {
        get
        {
            return this.agentField;
        }
        set
        {
            this.agentField = value;
        }
    }

    /// <remarks/>
    public AgentBidResponseResult Result
    {
        get
        {
            return this.resultField;
        }
        set
        {
            this.resultField = value;
        }
    }

    /// <remarks/>
    public string BidReference
    {
        get
        {
            return this.bidReferenceField;
        }
        set
        {
            this.bidReferenceField = value;
        }
    }

    /// <remarks/>
    public object Offers
    {
        get
        {
            return this.offersField;
        }
        set
        {
            this.offersField = value;
        }
    }

    /// <remarks/>
    public ushort RequestTime
    {
        get
        {
            return this.requestTimeField;
        }
        set
        {
            this.requestTimeField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class AgentBidResponseAgent
{

    private string referenceField;

    private System.DateTime timeField;

    private uint idField;

    /// <remarks/>
    public string Reference
    {
        get
        {
            return this.referenceField;
        }
        set
        {
            this.referenceField = value;
        }
    }

    /// <remarks/>
    public System.DateTime Time
    {
        get
        {
            return this.timeField;
        }
        set
        {
            this.timeField = value;
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttributeAttribute()]
    public uint Id
    {
        get
        {
            return this.idField;
        }
        set
        {
            this.idField = value;
        }
    }
}

/// <remarks/>
[System.SerializableAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class AgentBidResponseResult
{

    private bool successField;

    private byte failureCodeField;

    private string failureReasonField;

    /// <remarks/>
    public bool Success
    {
        get
        {
            return this.successField;
        }
        set
        {
            this.successField = value;
        }
    }

    /// <remarks/>
    public byte FailureCode
    {
        get
        {
            return this.failureCodeField;
        }
        set
        {
            this.failureCodeField = value;
        }
    }

    /// <remarks/>
    public string FailureReason
    {
        get
        {
            return this.failureReasonField;
        }
        set
        {
            this.failureReasonField = value;
        }
    }
}

