﻿namespace SwaranAdmin_New.Autocab.Model
{

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class AgentBookingAuthorizationRequest
    {

        private AgentBookingAuthorizationRequestAgent agentField;

        private AgentBookingAuthorizationRequestVendor vendorField;

        private string availabilityReferenceField;

        private string agentBookingReferenceField;

        private AgentBookingAuthorizationRequestPassengerDetails[] passengersField;

        private string driverNoteField;

        private string yourReferenceField;

        private AgentBookingAuthorizationRequestFlightDetails flightDetailsField;

        private AgentBookingAuthorizationRequestNotifications notificationsField;

        /// <remarks/>
        public AgentBookingAuthorizationRequestAgent Agent
        {
            get
            {
                return this.agentField;
            }
            set
            {
                this.agentField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAuthorizationRequestVendor Vendor
        {
            get
            {
                return this.vendorField;
            }
            set
            {
                this.vendorField = value;
            }
        }

        /// <remarks/>
        public string AvailabilityReference
        {
            get
            {
                return this.availabilityReferenceField;
            }
            set
            {
                this.availabilityReferenceField = value;
            }
        }

        /// <remarks/>
        public string AgentBookingReference
        {
            get
            {
                return this.agentBookingReferenceField;
            }
            set
            {
                this.agentBookingReferenceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("PassengerDetails", IsNullable = false)]
        public AgentBookingAuthorizationRequestPassengerDetails[] Passengers
        {
            get
            {
                return this.passengersField;
            }
            set
            {
                this.passengersField = value;
            }
        }

        /// <remarks/>
        public string DriverNote
        {
            get
            {
                return this.driverNoteField;
            }
            set
            {
                this.driverNoteField = value;
            }
        }

        /// <remarks/>
        public string YourReference
        {
            get
            {
                return this.yourReferenceField;
            }
            set
            {
                this.yourReferenceField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAuthorizationRequestFlightDetails FlightDetails
        {
            get
            {
                return this.flightDetailsField;
            }
            set
            {
                this.flightDetailsField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAuthorizationRequestNotifications Notifications
        {
            get
            {
                return this.notificationsField;
            }
            set
            {
                this.notificationsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAuthorizationRequestAgent
    {

        private string passwordField;

        private string referenceField;

        private string timeField;

        private int idField;

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        public string Time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAuthorizationRequestVendor
    {

        private int idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAuthorizationRequestPassengerDetails
    {

        private string nameField;

        private string telephoneNumberField;

        private string emailAddressField;

        private bool isLeadField;

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string TelephoneNumber
        {
            get
            {
                return this.telephoneNumberField;
            }
            set
            {
                this.telephoneNumberField = value;
            }
        }

        /// <remarks/>
        public string EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool IsLead
        {
            get
            {
                return this.isLeadField;
            }
            set
            {
                this.isLeadField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAuthorizationRequestFlightDetails
    {

        private string flightNumberField;

        private string flightDirectionField;

        private System.DateTime departureDateLocalField;

        private string arrivalDateLocalField;

        private string departureAirportCodeField;

        private string departureAirportNameField;

        private string arrivalAirportCodeField;

        private string arrivalAirportNameField;

        private byte departureTerminalField;

        private byte arrivalTerminalField;

        private string airlineCodeField;

        private string airlineNameField;

        /// <remarks/>
        public string FlightNumber
        {
            get
            {
                return this.flightNumberField;
            }
            set
            {
                this.flightNumberField = value;
            }
        }

        /// <remarks/>
        public string FlightDirection
        {
            get
            {
                return this.flightDirectionField;
            }
            set
            {
                this.flightDirectionField = value;
            }
        }

        /// <remarks/>
        public System.DateTime DepartureDateLocal
        {
            get
            {
                return this.departureDateLocalField;
            }
            set
            {
                this.departureDateLocalField = value;
            }
        }

        /// <remarks/>
        public string ArrivalDateLocal
        {
            get
            {
                return this.arrivalDateLocalField;
            }
            set
            {
                this.arrivalDateLocalField = value;
            }
        }

        /// <remarks/>
        public string DepartureAirportCode
        {
            get
            {
                return this.departureAirportCodeField;
            }
            set
            {
                this.departureAirportCodeField = value;
            }
        }

        /// <remarks/>
        public string DepartureAirportName
        {
            get
            {
                return this.departureAirportNameField;
            }
            set
            {
                this.departureAirportNameField = value;
            }
        }

        /// <remarks/>
        public string ArrivalAirportCode
        {
            get
            {
                return this.arrivalAirportCodeField;
            }
            set
            {
                this.arrivalAirportCodeField = value;
            }
        }

        /// <remarks/>
        public string ArrivalAirportName
        {
            get
            {
                return this.arrivalAirportNameField;
            }
            set
            {
                this.arrivalAirportNameField = value;
            }
        }

        /// <remarks/>
        public byte DepartureTerminal
        {
            get
            {
                return this.departureTerminalField;
            }
            set
            {
                this.departureTerminalField = value;
            }
        }

        /// <remarks/>
        public byte ArrivalTerminal
        {
            get
            {
                return this.arrivalTerminalField;
            }
            set
            {
                this.arrivalTerminalField = value;
            }
        }

        /// <remarks/>
        public string AirlineCode
        {
            get
            {
                return this.airlineCodeField;
            }
            set
            {
                this.airlineCodeField = value;
            }
        }

        /// <remarks/>
        public string AirlineName
        {
            get
            {
                return this.airlineNameField;
            }
            set
            {
                this.airlineNameField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAuthorizationRequestNotifications
    {

        private string vendorEventsField;

        private string alertMethodField;

        private string agentEventsField;

        /// <remarks/>
        public string VendorEvents
        {
            get
            {
                return this.vendorEventsField;
            }
            set
            {
                this.vendorEventsField = value;
            }
        }

        /// <remarks/>
        public string AlertMethod
        {
            get
            {
                return this.alertMethodField;
            }
            set
            {
                this.alertMethodField = value;
            }
        }

        /// <remarks/>
        public string AgentEvents
        {
            get
            {
                return this.agentEventsField;
            }
            set
            {
                this.agentEventsField = value;
            }
        }
    }


}
