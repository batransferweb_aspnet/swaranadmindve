﻿namespace SwaranAdmin_New.Autocab.Model
{
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class AgentGetDriverDetailsResponse
    {

        private AgentGetDriverDetailsResponseAgent agentField;

        private AgentGetDriverDetailsResponseVendor vendorField;

        private AgentGetDriverDetailsResponseResult resultField;

        private byte callsignField;

        private string forenameField;

        private string surnameField;

        private long mobileNumberField;

        private AgentGetDriverDetailsResponseImage imageField;

        /// <remarks/>
        public AgentGetDriverDetailsResponseAgent Agent
        {
            get
            {
                return this.agentField;
            }
            set
            {
                this.agentField = value;
            }
        }

        /// <remarks/>
        public AgentGetDriverDetailsResponseVendor Vendor
        {
            get
            {
                return this.vendorField;
            }
            set
            {
                this.vendorField = value;
            }
        }

        /// <remarks/>
        public AgentGetDriverDetailsResponseResult Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        public byte Callsign
        {
            get
            {
                return this.callsignField;
            }
            set
            {
                this.callsignField = value;
            }
        }

        /// <remarks/>
        public string Forename
        {
            get
            {
                return this.forenameField;
            }
            set
            {
                this.forenameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public long MobileNumber
        {
            get
            {
                return this.mobileNumberField;
            }
            set
            {
                this.mobileNumberField = value;
            }
        }

        /// <remarks/>
        public AgentGetDriverDetailsResponseImage Image
        {
            get
            {
                return this.imageField;
            }
            set
            {
                this.imageField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentGetDriverDetailsResponseAgent
    {

        private string referenceField;

        private System.DateTime timeField;

        private byte idField;

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        public System.DateTime Time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentGetDriverDetailsResponseVendor
    {

        private byte idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentGetDriverDetailsResponseResult
    {

        private bool successField;

        private object failureCodeField;

        private object failureReasonField;

        /// <remarks/>
        public bool Success
        {
            get
            {
                return this.successField;
            }
            set
            {
                this.successField = value;
            }
        }

        /// <remarks/>
        public object FailureCode
        {
            get
            {
                return this.failureCodeField;
            }
            set
            {
                this.failureCodeField = value;
            }
        }

        /// <remarks/>
        public object FailureReason
        {
            get
            {
                return this.failureReasonField;
            }
            set
            {
                this.failureReasonField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentGetDriverDetailsResponseImage
    {

        private string formatField;

        private ushort widthField;

        private byte heightField;

        private string encodedImageField;

        /// <remarks/>
        public string Format
        {
            get
            {
                return this.formatField;
            }
            set
            {
                this.formatField = value;
            }
        }

        /// <remarks/>
        public ushort Width
        {
            get
            {
                return this.widthField;
            }
            set
            {
                this.widthField = value;
            }
        }

        /// <remarks/>
        public byte Height
        {
            get
            {
                return this.heightField;
            }
            set
            {
                this.heightField = value;
            }
        }

        /// <remarks/>
        public string EncodedImage
        {
            get
            {
                return this.encodedImageField;
            }
            set
            {
                this.encodedImageField = value;
            }
        }
    }


}
