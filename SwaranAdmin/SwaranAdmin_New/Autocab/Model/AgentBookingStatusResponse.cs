﻿namespace SwaranAdmin_New.Autocab.Model
{

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class AgentBookingStatusResponse
    {

        private AgentBookingStatusResponseAgent agentField;

        private AgentBookingStatusResponseVendor vendorField;

        private AgentBookingStatusResponseResult resultField;

        private string bookingReferenceField;

        private string statusField;

        private AgentBookingStatusResponseVehicle vehicleField;

        private AgentBookingStatusResponseLocation locationField;

        private System.DateTime etaField;

        private System.DateTime localEtaField;

        private ushort etaInSecondsField;

        private ushort distanceField;

        private AgentBookingStatusResponseJourneyDetails journeyDetailsField;

        private AgentBookingStatusResponseTimingDetails timingDetailsField;

        private AgentBookingStatusResponsePricingDetails pricingDetailsField;

        private AgentBookingStatusResponsePayment paymentField;

        /// <remarks/>
        public AgentBookingStatusResponseAgent Agent
        {
            get
            {
                return this.agentField;
            }
            set
            {
                this.agentField = value;
            }
        }

        /// <remarks/>
        public AgentBookingStatusResponseVendor Vendor
        {
            get
            {
                return this.vendorField;
            }
            set
            {
                this.vendorField = value;
            }
        }

        /// <remarks/>
        public AgentBookingStatusResponseResult Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        public string BookingReference
        {
            get
            {
                return this.bookingReferenceField;
            }
            set
            {
                this.bookingReferenceField = value;
            }
        }

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public AgentBookingStatusResponseVehicle Vehicle
        {
            get
            {
                return this.vehicleField;
            }
            set
            {
                this.vehicleField = value;
            }
        }

        /// <remarks/>
        public AgentBookingStatusResponseLocation Location
        {
            get
            {
                return this.locationField;
            }
            set
            {
                this.locationField = value;
            }
        }

        /// <remarks/>
        public System.DateTime Eta
        {
            get
            {
                return this.etaField;
            }
            set
            {
                this.etaField = value;
            }
        }

        /// <remarks/>
        public System.DateTime LocalEta
        {
            get
            {
                return this.localEtaField;
            }
            set
            {
                this.localEtaField = value;
            }
        }

        /// <remarks/>
        public ushort EtaInSeconds
        {
            get
            {
                return this.etaInSecondsField;
            }
            set
            {
                this.etaInSecondsField = value;
            }
        }

        /// <remarks/>
        public ushort Distance
        {
            get
            {
                return this.distanceField;
            }
            set
            {
                this.distanceField = value;
            }
        }

        /// <remarks/>
        public AgentBookingStatusResponseJourneyDetails JourneyDetails
        {
            get
            {
                return this.journeyDetailsField;
            }
            set
            {
                this.journeyDetailsField = value;
            }
        }

        /// <remarks/>
        public AgentBookingStatusResponseTimingDetails TimingDetails
        {
            get
            {
                return this.timingDetailsField;
            }
            set
            {
                this.timingDetailsField = value;
            }
        }

        /// <remarks/>
        public AgentBookingStatusResponsePricingDetails PricingDetails
        {
            get
            {
                return this.pricingDetailsField;
            }
            set
            {
                this.pricingDetailsField = value;
            }
        }

        /// <remarks/>
        public AgentBookingStatusResponsePayment Payment
        {
            get
            {
                return this.paymentField;
            }
            set
            {
                this.paymentField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseAgent
    {

        private string referenceField;

        private System.DateTime timeField;

        private byte idField;

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        public System.DateTime Time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseVendor
    {

        private byte idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseResult
    {

        private bool successField;

        private object failureCodeField;

        private object failureReasonField;

        /// <remarks/>
        public bool Success
        {
            get
            {
                return this.successField;
            }
            set
            {
                this.successField = value;
            }
        }

        /// <remarks/>
        public object FailureCode
        {
            get
            {
                return this.failureCodeField;
            }
            set
            {
                this.failureCodeField = value;
            }
        }

        /// <remarks/>
        public object FailureReason
        {
            get
            {
                return this.failureReasonField;
            }
            set
            {
                this.failureReasonField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseVehicle
    {

        private byte callsignField;

        private string makeField;

        private string modelField;

        private string colourField;

        private string registrationField;

        private uint plateNumberField;

        /// <remarks/>
        public byte Callsign
        {
            get
            {
                return this.callsignField;
            }
            set
            {
                this.callsignField = value;
            }
        }

        /// <remarks/>
        public string Make
        {
            get
            {
                return this.makeField;
            }
            set
            {
                this.makeField = value;
            }
        }

        /// <remarks/>
        public string Model
        {
            get
            {
                return this.modelField;
            }
            set
            {
                this.modelField = value;
            }
        }

        /// <remarks/>
        public string Colour
        {
            get
            {
                return this.colourField;
            }
            set
            {
                this.colourField = value;
            }
        }

        /// <remarks/>
        public string Registration
        {
            get
            {
                return this.registrationField;
            }
            set
            {
                this.registrationField = value;
            }
        }

        /// <remarks/>
        public uint PlateNumber
        {
            get
            {
                return this.plateNumberField;
            }
            set
            {
                this.plateNumberField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseLocation
    {

        private System.DateTime timeField;

        private System.DateTime localTimeField;

        private decimal headingField;

        private decimal speedField;

        private AgentBookingStatusResponseLocationCoordinate coordinateField;

        /// <remarks/>
        public System.DateTime Time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime LocalTime
        {
            get
            {
                return this.localTimeField;
            }
            set
            {
                this.localTimeField = value;
            }
        }

        /// <remarks/>
        public decimal Heading
        {
            get
            {
                return this.headingField;
            }
            set
            {
                this.headingField = value;
            }
        }

        /// <remarks/>
        public decimal Speed
        {
            get
            {
                return this.speedField;
            }
            set
            {
                this.speedField = value;
            }
        }

        /// <remarks/>
        public AgentBookingStatusResponseLocationCoordinate Coordinate
        {
            get
            {
                return this.coordinateField;
            }
            set
            {
                this.coordinateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseLocationCoordinate
    {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal Latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal Longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseJourneyDetails
    {

        private AgentBookingStatusResponseJourneyDetailsFrom fromField;

        private AgentBookingStatusResponseJourneyDetailsVias viasField;

        private AgentBookingStatusResponseJourneyDetailsTO toField;

        /// <remarks/>
        public AgentBookingStatusResponseJourneyDetailsFrom From
        {
            get
            {
                return this.fromField;
            }
            set
            {
                this.fromField = value;
            }
        }

        /// <remarks/>
        public AgentBookingStatusResponseJourneyDetailsVias Vias
        {
            get
            {
                return this.viasField;
            }
            set
            {
                this.viasField = value;
            }
        }

        /// <remarks/>
        public AgentBookingStatusResponseJourneyDetailsTO To
        {
            get
            {
                return this.toField;
            }
            set
            {
                this.toField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseJourneyDetailsFrom
    {

        private string typeField;

        private string dataField;

        private AgentBookingStatusResponseJourneyDetailsFromCoordinate coordinateField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string Data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }

        /// <remarks/>
        public AgentBookingStatusResponseJourneyDetailsFromCoordinate Coordinate
        {
            get
            {
                return this.coordinateField;
            }
            set
            {
                this.coordinateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseJourneyDetailsFromCoordinate
    {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal Latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal Longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseJourneyDetailsVias
    {

        private AgentBookingStatusResponseJourneyDetailsViasVia viaField;

        /// <remarks/>
        public AgentBookingStatusResponseJourneyDetailsViasVia Via
        {
            get
            {
                return this.viaField;
            }
            set
            {
                this.viaField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseJourneyDetailsViasVia
    {

        private string typeField;

        private string dataField;

        private AgentBookingStatusResponseJourneyDetailsViasViaCoordinate coordinateField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string Data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }

        /// <remarks/>
        public AgentBookingStatusResponseJourneyDetailsViasViaCoordinate Coordinate
        {
            get
            {
                return this.coordinateField;
            }
            set
            {
                this.coordinateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseJourneyDetailsViasViaCoordinate
    {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal Latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal Longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseJourneyDetailsTO
    {

        private string typeField;

        private string dataField;

        private AgentBookingStatusResponseJourneyDetailsTOCoordinate coordinateField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string Data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }

        /// <remarks/>
        public AgentBookingStatusResponseJourneyDetailsTOCoordinate Coordinate
        {
            get
            {
                return this.coordinateField;
            }
            set
            {
                this.coordinateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseJourneyDetailsTOCoordinate
    {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal Latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal Longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponseTimingDetails
    {

        private ushort totalTimeField;

        private System.DateTime bookedAtTimeField;

        private System.DateTime localBookedAtTimeField;

        private System.DateTime pickupDueTimeField;

        private System.DateTime localPickupDueTimeField;

        private System.DateTime dispatchTimeField;

        private System.DateTime localDispatchTimeField;

        private System.DateTime vehicleArrivedTimeField;

        private System.DateTime localVehicleArrivedTimeField;

        private System.DateTime pickedUpTimeField;

        private System.DateTime localPickedUpTimeField;

        private System.DateTime completionTimeField;

        private System.DateTime localCompletionTimeField;

        private System.DateTime estimatedVehicleArrivalTimeField;

        private System.DateTime localEstimatedVehicleArrivalTimeField;

        /// <remarks/>
        public ushort TotalTime
        {
            get
            {
                return this.totalTimeField;
            }
            set
            {
                this.totalTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime BookedAtTime
        {
            get
            {
                return this.bookedAtTimeField;
            }
            set
            {
                this.bookedAtTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime LocalBookedAtTime
        {
            get
            {
                return this.localBookedAtTimeField;
            }
            set
            {
                this.localBookedAtTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime PickupDueTime
        {
            get
            {
                return this.pickupDueTimeField;
            }
            set
            {
                this.pickupDueTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime LocalPickupDueTime
        {
            get
            {
                return this.localPickupDueTimeField;
            }
            set
            {
                this.localPickupDueTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime DispatchTime
        {
            get
            {
                return this.dispatchTimeField;
            }
            set
            {
                this.dispatchTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime LocalDispatchTime
        {
            get
            {
                return this.localDispatchTimeField;
            }
            set
            {
                this.localDispatchTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime VehicleArrivedTime
        {
            get
            {
                return this.vehicleArrivedTimeField;
            }
            set
            {
                this.vehicleArrivedTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime LocalVehicleArrivedTime
        {
            get
            {
                return this.localVehicleArrivedTimeField;
            }
            set
            {
                this.localVehicleArrivedTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime PickedUpTime
        {
            get
            {
                return this.pickedUpTimeField;
            }
            set
            {
                this.pickedUpTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime LocalPickedUpTime
        {
            get
            {
                return this.localPickedUpTimeField;
            }
            set
            {
                this.localPickedUpTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime CompletionTime
        {
            get
            {
                return this.completionTimeField;
            }
            set
            {
                this.completionTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime LocalCompletionTime
        {
            get
            {
                return this.localCompletionTimeField;
            }
            set
            {
                this.localCompletionTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime EstimatedVehicleArrivalTime
        {
            get
            {
                return this.estimatedVehicleArrivalTimeField;
            }
            set
            {
                this.estimatedVehicleArrivalTimeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime LocalEstimatedVehicleArrivalTime
        {
            get
            {
                return this.localEstimatedVehicleArrivalTimeField;
            }
            set
            {
                this.localEstimatedVehicleArrivalTimeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponsePricingDetails
    {

        private ushort priceField;

        private string currencyField;

        private ushort fareField;

        private byte waitingTimePriceField;

        private byte waitingTimeMinutesField;

        private byte extrasPriceField;

        private byte gratuityField;

        private byte loyaltyPriceField;

        private ushort distanceField;

        /// <remarks/>
        public ushort Price
        {
            get
            {
                return this.priceField;
            }
            set
            {
                this.priceField = value;
            }
        }

        /// <remarks/>
        public string Currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        public ushort Fare
        {
            get
            {
                return this.fareField;
            }
            set
            {
                this.fareField = value;
            }
        }

        /// <remarks/>
        public byte WaitingTimePrice
        {
            get
            {
                return this.waitingTimePriceField;
            }
            set
            {
                this.waitingTimePriceField = value;
            }
        }

        /// <remarks/>
        public byte WaitingTimeMinutes
        {
            get
            {
                return this.waitingTimeMinutesField;
            }
            set
            {
                this.waitingTimeMinutesField = value;
            }
        }

        /// <remarks/>
        public byte ExtrasPrice
        {
            get
            {
                return this.extrasPriceField;
            }
            set
            {
                this.extrasPriceField = value;
            }
        }

        /// <remarks/>
        public byte Gratuity
        {
            get
            {
                return this.gratuityField;
            }
            set
            {
                this.gratuityField = value;
            }
        }

        /// <remarks/>
        public byte LoyaltyPrice
        {
            get
            {
                return this.loyaltyPriceField;
            }
            set
            {
                this.loyaltyPriceField = value;
            }
        }

        /// <remarks/>
        public ushort Distance
        {
            get
            {
                return this.distanceField;
            }
            set
            {
                this.distanceField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingStatusResponsePayment
    {

        private string paymentTypeField;

        /// <remarks/>
        public string PaymentType
        {
            get
            {
                return this.paymentTypeField;
            }
            set
            {
                this.paymentTypeField = value;
            }
        }
    }


}
