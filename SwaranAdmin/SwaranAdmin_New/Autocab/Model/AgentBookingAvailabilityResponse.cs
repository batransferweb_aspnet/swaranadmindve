﻿namespace SwaranAdmin_New.Autocab.Model
{

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class AgentBookingAvailabilityResponse
    {

        private AgentBookingAvailabilityResponseAgent agentField;

        private AgentBookingAvailabilityResponseResult resultField;

        private AgentBookingAvailabilityResponseVendor vendorField;

        private string availabilityReferenceField;

        private AgentBookingAvailabilityResponsePricing pricingField;

        private AgentBookingAvailabilityResponseEstimatedJourney estimatedJourneyField;

        private string vehicleTypeField;

        private string vehicleCategoryField;

        private string availableEventsField;

        private System.DateTime expiryTimeField;

        private AgentBookingAvailabilityResponseVendorDetails vendorDetailsField;

        /// <remarks/>
        public AgentBookingAvailabilityResponseAgent Agent
        {
            get
            {
                return this.agentField;
            }
            set
            {
                this.agentField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityResponseResult Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityResponseVendor Vendor
        {
            get
            {
                return this.vendorField;
            }
            set
            {
                this.vendorField = value;
            }
        }

        /// <remarks/>
        public string AvailabilityReference
        {
            get
            {
                return this.availabilityReferenceField;
            }
            set
            {
                this.availabilityReferenceField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityResponsePricing Pricing
        {
            get
            {
                return this.pricingField;
            }
            set
            {
                this.pricingField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityResponseEstimatedJourney EstimatedJourney
        {
            get
            {
                return this.estimatedJourneyField;
            }
            set
            {
                this.estimatedJourneyField = value;
            }
        }

        /// <remarks/>
        public string VehicleType
        {
            get
            {
                return this.vehicleTypeField;
            }
            set
            {
                this.vehicleTypeField = value;
            }
        }

        /// <remarks/>
        public string VehicleCategory
        {
            get
            {
                return this.vehicleCategoryField;
            }
            set
            {
                this.vehicleCategoryField = value;
            }
        }

        /// <remarks/>
        public string AvailableEvents
        {
            get
            {
                return this.availableEventsField;
            }
            set
            {
                this.availableEventsField = value;
            }
        }

        /// <remarks/>
        public System.DateTime ExpiryTime
        {
            get
            {
                return this.expiryTimeField;
            }
            set
            {
                this.expiryTimeField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityResponseVendorDetails VendorDetails
        {
            get
            {
                return this.vendorDetailsField;
            }
            set
            {
                this.vendorDetailsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityResponseAgent
    {

        private string referenceField;

        private System.DateTime timeField;

        private uint idField;

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        public System.DateTime Time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public uint Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityResponseResult
    {

        private bool successField;

        /// <remarks/>
        public bool Success
        {
            get
            {
                return this.successField;
            }
            set
            {
                this.successField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityResponseVendor
    {

        private string idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityResponsePricing
    {

        private string pricingMethodField;

        private uint priceField;

        private byte commissionField;

        private byte gratuityField;

        private string currencyField;

        private byte loyaltyCardField;

        /// <remarks/>
        public string PricingMethod
        {
            get
            {
                return this.pricingMethodField;
            }
            set
            {
                this.pricingMethodField = value;
            }
        }

        /// <remarks/>
        public uint Price
        {
            get
            {
                return this.priceField;
            }
            set
            {
                this.priceField = value;
            }
        }

        /// <remarks/>
        public byte Commission
        {
            get
            {
                return this.commissionField;
            }
            set
            {
                this.commissionField = value;
            }
        }

        /// <remarks/>
        public byte Gratuity
        {
            get
            {
                return this.gratuityField;
            }
            set
            {
                this.gratuityField = value;
            }
        }

        /// <remarks/>
        public string Currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        public byte LoyaltyCard
        {
            get
            {
                return this.loyaltyCardField;
            }
            set
            {
                this.loyaltyCardField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityResponseEstimatedJourney
    {

        private uint distanceField;

        private ushort durationField;

        /// <remarks/>
        public uint Distance
        {
            get
            {
                return this.distanceField;
            }
            set
            {
                this.distanceField = value;
            }
        }

        /// <remarks/>
        public ushort Duration
        {
            get
            {
                return this.durationField;
            }
            set
            {
                this.durationField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityResponseVendorDetails
    {

        private string nameField;

        private string addressField;

        private string descriptionField;

        private object telephoneNumberField;

        private object emailAddressField;

        private AgentBookingAvailabilityResponseVendorDetailsFleet fleetField;

        private string ratingField;

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string Address
        {
            get
            {
                return this.addressField;
            }
            set
            {
                this.addressField = value;
            }
        }

        /// <remarks/>
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                this.descriptionField = value;
            }
        }

        /// <remarks/>
        public object TelephoneNumber
        {
            get
            {
                return this.telephoneNumberField;
            }
            set
            {
                this.telephoneNumberField = value;
            }
        }

        /// <remarks/>
        public object EmailAddress
        {
            get
            {
                return this.emailAddressField;
            }
            set
            {
                this.emailAddressField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityResponseVendorDetailsFleet Fleet
        {
            get
            {
                return this.fleetField;
            }
            set
            {
                this.fleetField = value;
            }
        }

        /// <remarks/>
        public string Rating
        {
            get
            {
                return this.ratingField;
            }
            set
            {
                this.ratingField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityResponseVendorDetailsFleet
    {

        private byte countField;

        private string uniformField;

        private AgentBookingAvailabilityResponseVendorDetailsFleetVehicleInformation[] vehiclesField;

        /// <remarks/>
        public byte Count
        {
            get
            {
                return this.countField;
            }
            set
            {
                this.countField = value;
            }
        }

        /// <remarks/>
        public string Uniform
        {
            get
            {
                return this.uniformField;
            }
            set
            {
                this.uniformField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("VehicleInformation", IsNullable = false)]
        public AgentBookingAvailabilityResponseVendorDetailsFleetVehicleInformation[] Vehicles
        {
            get
            {
                return this.vehiclesField;
            }
            set
            {
                this.vehiclesField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityResponseVendorDetailsFleetVehicleInformation
    {

        private string makeField;

        private string modelField;

        private byte countField;

        /// <remarks/>
        public string Make
        {
            get
            {
                return this.makeField;
            }
            set
            {
                this.makeField = value;
            }
        }

        /// <remarks/>
        public string Model
        {
            get
            {
                return this.modelField;
            }
            set
            {
                this.modelField = value;
            }
        }

        /// <remarks/>
        public byte Count
        {
            get
            {
                return this.countField;
            }
            set
            {
                this.countField = value;
            }
        }
    }


}
