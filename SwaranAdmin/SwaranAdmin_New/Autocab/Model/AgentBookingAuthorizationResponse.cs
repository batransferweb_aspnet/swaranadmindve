﻿namespace SwaranAdmin_New.Autocab.Model
{


    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class AgentBookingAuthorizationResponse
    {

        private AgentBookingAuthorizationResponseAgent agentField;

        private AgentBookingAuthorizationResponseVendor vendorField;

        private AgentBookingAuthorizationResponseResult resultField;

        private string authorizationReferenceField;

        private string bookingReferenceField;

        private AgentBookingAuthorizationResponseVehicleDetails vehicleDetailsField;

        private AgentBookingAuthorizationResponseDriverDetails driverDetailsField;

        /// <remarks/>
        public AgentBookingAuthorizationResponseAgent Agent
        {
            get
            {
                return this.agentField;
            }
            set
            {
                this.agentField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAuthorizationResponseVendor Vendor
        {
            get
            {
                return this.vendorField;
            }
            set
            {
                this.vendorField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAuthorizationResponseResult Result
        {
            get
            {
                return this.resultField;
            }
            set
            {
                this.resultField = value;
            }
        }

        /// <remarks/>
        public string AuthorizationReference
        {
            get
            {
                return this.authorizationReferenceField;
            }
            set
            {
                this.authorizationReferenceField = value;
            }
        }

        /// <remarks/>
        public string BookingReference
        {
            get
            {
                return this.bookingReferenceField;
            }
            set
            {
                this.bookingReferenceField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAuthorizationResponseVehicleDetails VehicleDetails
        {
            get
            {
                return this.vehicleDetailsField;
            }
            set
            {
                this.vehicleDetailsField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAuthorizationResponseDriverDetails DriverDetails
        {
            get
            {
                return this.driverDetailsField;
            }
            set
            {
                this.driverDetailsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAuthorizationResponseAgent
    {

        private string referenceField;

        private System.DateTime timeField;

        private byte idField;

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        public System.DateTime Time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAuthorizationResponseVendor
    {

        private byte idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAuthorizationResponseResult
    {

        private bool successField;

        private object failureCodeField;

        private object failureReasonField;

        /// <remarks/>
        public bool Success
        {
            get
            {
                return this.successField;
            }
            set
            {
                this.successField = value;
            }
        }

        /// <remarks/>
        public object FailureCode
        {
            get
            {
                return this.failureCodeField;
            }
            set
            {
                this.failureCodeField = value;
            }
        }

        /// <remarks/>
        public object FailureReason
        {
            get
            {
                return this.failureReasonField;
            }
            set
            {
                this.failureReasonField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAuthorizationResponseVehicleDetails
    {

        private byte callsignField;

        private string makeField;

        private string modelField;

        private string colourField;

        private string registrationField;

        private uint plateNumberField;

        private AgentBookingAuthorizationResponseVehicleDetailsLocation locationField;

        /// <remarks/>
        public byte Callsign
        {
            get
            {
                return this.callsignField;
            }
            set
            {
                this.callsignField = value;
            }
        }

        /// <remarks/>
        public string Make
        {
            get
            {
                return this.makeField;
            }
            set
            {
                this.makeField = value;
            }
        }

        /// <remarks/>
        public string Model
        {
            get
            {
                return this.modelField;
            }
            set
            {
                this.modelField = value;
            }
        }

        /// <remarks/>
        public string Colour
        {
            get
            {
                return this.colourField;
            }
            set
            {
                this.colourField = value;
            }
        }

        /// <remarks/>
        public string Registration
        {
            get
            {
                return this.registrationField;
            }
            set
            {
                this.registrationField = value;
            }
        }

        /// <remarks/>
        public uint PlateNumber
        {
            get
            {
                return this.plateNumberField;
            }
            set
            {
                this.plateNumberField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAuthorizationResponseVehicleDetailsLocation Location
        {
            get
            {
                return this.locationField;
            }
            set
            {
                this.locationField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAuthorizationResponseVehicleDetailsLocation
    {

        private System.DateTime timeField;

        private System.DateTime localTimeField;

        private decimal headingField;

        private decimal speedField;

        private AgentBookingAuthorizationResponseVehicleDetailsLocationCoordinate coordinateField;

        /// <remarks/>
        public System.DateTime Time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime LocalTime
        {
            get
            {
                return this.localTimeField;
            }
            set
            {
                this.localTimeField = value;
            }
        }

        /// <remarks/>
        public decimal Heading
        {
            get
            {
                return this.headingField;
            }
            set
            {
                this.headingField = value;
            }
        }

        /// <remarks/>
        public decimal Speed
        {
            get
            {
                return this.speedField;
            }
            set
            {
                this.speedField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAuthorizationResponseVehicleDetailsLocationCoordinate Coordinate
        {
            get
            {
                return this.coordinateField;
            }
            set
            {
                this.coordinateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAuthorizationResponseVehicleDetailsLocationCoordinate
    {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal Latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal Longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAuthorizationResponseDriverDetails
    {

        private byte callsignField;

        private string forenameField;

        private string surnameField;

        private long mobileNumberField;

        private AgentBookingAuthorizationResponseDriverDetailsImage imageField;

        /// <remarks/>
        public byte Callsign
        {
            get
            {
                return this.callsignField;
            }
            set
            {
                this.callsignField = value;
            }
        }

        /// <remarks/>
        public string Forename
        {
            get
            {
                return this.forenameField;
            }
            set
            {
                this.forenameField = value;
            }
        }

        /// <remarks/>
        public string Surname
        {
            get
            {
                return this.surnameField;
            }
            set
            {
                this.surnameField = value;
            }
        }

        /// <remarks/>
        public long MobileNumber
        {
            get
            {
                return this.mobileNumberField;
            }
            set
            {
                this.mobileNumberField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAuthorizationResponseDriverDetailsImage Image
        {
            get
            {
                return this.imageField;
            }
            set
            {
                this.imageField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAuthorizationResponseDriverDetailsImage
    {

        private string formatField;

        private ushort widthField;

        private byte heightField;

        private string encodedImageField;

        /// <remarks/>
        public string Format
        {
            get
            {
                return this.formatField;
            }
            set
            {
                this.formatField = value;
            }
        }

        /// <remarks/>
        public ushort Width
        {
            get
            {
                return this.widthField;
            }
            set
            {
                this.widthField = value;
            }
        }

        /// <remarks/>
        public byte Height
        {
            get
            {
                return this.heightField;
            }
            set
            {
                this.heightField = value;
            }
        }

        /// <remarks/>
        public string EncodedImage
        {
            get
            {
                return this.encodedImageField;
            }
            set
            {
                this.encodedImageField = value;
            }
        }
    }



}
