﻿namespace SwaranAdmin_New.Autocab.Model
{

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    //public partial class AgentBidResponse
    //{

    //    private AgentBidResponseAgent agentField;

    //    private string bidReferenceField;

    //    private AgentBidResponseOffer[] offersField;

    //    private AgentBidResponseFailedBids failedBidsField;

    //    /// <remarks/>
    //    public AgentBidResponseAgent Agent
    //    {
    //        get
    //        {
    //            return this.agentField;
    //        }
    //        set
    //        {
    //            this.agentField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string BidReference
    //    {
    //        get
    //        {
    //            return this.bidReferenceField;
    //        }
    //        set
    //        {
    //            this.bidReferenceField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlArrayItemAttribute("Offer", IsNullable = false)]
    //    public AgentBidResponseOffer[] Offers
    //    {
    //        get
    //        {
    //            return this.offersField;
    //        }
    //        set
    //        {
    //            this.offersField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public AgentBidResponseFailedBids FailedBids
    //    {
    //        get
    //        {
    //            return this.failedBidsField;
    //        }
    //        set
    //        {
    //            this.failedBidsField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class AgentBidResponseAgent
    //{

    //    private string referenceField;

    //    private System.DateTime timeField;

    //    private byte idField;

    //    /// <remarks/>
    //    public string Reference
    //    {
    //        get
    //        {
    //            return this.referenceField;
    //        }
    //        set
    //        {
    //            this.referenceField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public System.DateTime Time
    //    {
    //        get
    //        {
    //            return this.timeField;
    //        }
    //        set
    //        {
    //            this.timeField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Id
    //    {
    //        get
    //        {
    //            return this.idField;
    //        }
    //        set
    //        {
    //            this.idField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class AgentBidResponseOffer
    //{

    //    private AgentBidResponseOfferVendor vendorField;

    //    private AgentBidResponseOfferPricing pricingField;

    //    private AgentBidResponseOfferEstimatedJourney estimatedJourneyField;

    //    private string vehicleTypeField;

    //    private string vehicleCategoryField;

    //    private string availableEventsField;

    //    private AgentBidResponseOfferVendorDetails vendorDetailsField;

    //    private string referenceField;

    //    /// <remarks/>
    //    public AgentBidResponseOfferVendor Vendor
    //    {
    //        get
    //        {
    //            return this.vendorField;
    //        }
    //        set
    //        {
    //            this.vendorField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public AgentBidResponseOfferPricing Pricing
    //    {
    //        get
    //        {
    //            return this.pricingField;
    //        }
    //        set
    //        {
    //            this.pricingField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public AgentBidResponseOfferEstimatedJourney EstimatedJourney
    //    {
    //        get
    //        {
    //            return this.estimatedJourneyField;
    //        }
    //        set
    //        {
    //            this.estimatedJourneyField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string VehicleType
    //    {
    //        get
    //        {
    //            return this.vehicleTypeField;
    //        }
    //        set
    //        {
    //            this.vehicleTypeField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string VehicleCategory
    //    {
    //        get
    //        {
    //            return this.vehicleCategoryField;
    //        }
    //        set
    //        {
    //            this.vehicleCategoryField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string AvailableEvents
    //    {
    //        get
    //        {
    //            return this.availableEventsField;
    //        }
    //        set
    //        {
    //            this.availableEventsField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public AgentBidResponseOfferVendorDetails VendorDetails
    //    {
    //        get
    //        {
    //            return this.vendorDetailsField;
    //        }
    //        set
    //        {
    //            this.vendorDetailsField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public string Reference
    //    {
    //        get
    //        {
    //            return this.referenceField;
    //        }
    //        set
    //        {
    //            this.referenceField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class AgentBidResponseOfferVendor
    //{

    //    private byte idField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Id
    //    {
    //        get
    //        {
    //            return this.idField;
    //        }
    //        set
    //        {
    //            this.idField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class AgentBidResponseOfferPricing
    //{

    //    private string pricingMethodField;

    //    private ushort priceField;

    //    private byte commissionField;

    //    private byte gratuityField;

    //    private string currencyField;

    //    /// <remarks/>
    //    public string PricingMethod
    //    {
    //        get
    //        {
    //            return this.pricingMethodField;
    //        }
    //        set
    //        {
    //            this.pricingMethodField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public ushort Price
    //    {
    //        get
    //        {
    //            return this.priceField;
    //        }
    //        set
    //        {
    //            this.priceField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public byte Commission
    //    {
    //        get
    //        {
    //            return this.commissionField;
    //        }
    //        set
    //        {
    //            this.commissionField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public byte Gratuity
    //    {
    //        get
    //        {
    //            return this.gratuityField;
    //        }
    //        set
    //        {
    //            this.gratuityField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Currency
    //    {
    //        get
    //        {
    //            return this.currencyField;
    //        }
    //        set
    //        {
    //            this.currencyField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class AgentBidResponseOfferEstimatedJourney
    //{

    //    private System.DateTime etaField;

    //    private System.DateTime localEtaField;

    //    private ushort etaInSecondsField;

    //    private ushort distanceField;

    //    private byte durationField;

    //    /// <remarks/>
    //    public System.DateTime Eta
    //    {
    //        get
    //        {
    //            return this.etaField;
    //        }
    //        set
    //        {
    //            this.etaField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public System.DateTime LocalEta
    //    {
    //        get
    //        {
    //            return this.localEtaField;
    //        }
    //        set
    //        {
    //            this.localEtaField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public ushort EtaInSeconds
    //    {
    //        get
    //        {
    //            return this.etaInSecondsField;
    //        }
    //        set
    //        {
    //            this.etaInSecondsField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public ushort Distance
    //    {
    //        get
    //        {
    //            return this.distanceField;
    //        }
    //        set
    //        {
    //            this.distanceField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public byte Duration
    //    {
    //        get
    //        {
    //            return this.durationField;
    //        }
    //        set
    //        {
    //            this.durationField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class AgentBidResponseOfferVendorDetails
    //{

    //    private string nameField;

    //    private string addressField;

    //    private string descriptionField;

    //    private uint telephoneNumberField;

    //    private string emailAddressField;

    //    private string twitterField;

    //    private string facebookField;

    //    private ushort averageEtaField;

    //    private AgentBidResponseOfferVendorDetailsFleet fleetField;

    //    private string ratingField;

    //    /// <remarks/>
    //    public string Name
    //    {
    //        get
    //        {
    //            return this.nameField;
    //        }
    //        set
    //        {
    //            this.nameField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Address
    //    {
    //        get
    //        {
    //            return this.addressField;
    //        }
    //        set
    //        {
    //            this.addressField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Description
    //    {
    //        get
    //        {
    //            return this.descriptionField;
    //        }
    //        set
    //        {
    //            this.descriptionField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public uint TelephoneNumber
    //    {
    //        get
    //        {
    //            return this.telephoneNumberField;
    //        }
    //        set
    //        {
    //            this.telephoneNumberField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string EmailAddress
    //    {
    //        get
    //        {
    //            return this.emailAddressField;
    //        }
    //        set
    //        {
    //            this.emailAddressField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Twitter
    //    {
    //        get
    //        {
    //            return this.twitterField;
    //        }
    //        set
    //        {
    //            this.twitterField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Facebook
    //    {
    //        get
    //        {
    //            return this.facebookField;
    //        }
    //        set
    //        {
    //            this.facebookField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public ushort AverageEta
    //    {
    //        get
    //        {
    //            return this.averageEtaField;
    //        }
    //        set
    //        {
    //            this.averageEtaField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public AgentBidResponseOfferVendorDetailsFleet Fleet
    //    {
    //        get
    //        {
    //            return this.fleetField;
    //        }
    //        set
    //        {
    //            this.fleetField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Rating
    //    {
    //        get
    //        {
    //            return this.ratingField;
    //        }
    //        set
    //        {
    //            this.ratingField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class AgentBidResponseOfferVendorDetailsFleet
    //{

    //    private ushort countField;

    //    private byte averageAgeField;

    //    private string uniformField;

    //    private AgentBidResponseOfferVendorDetailsFleetVehicleInformation[] vehiclesField;

    //    /// <remarks/>
    //    public ushort Count
    //    {
    //        get
    //        {
    //            return this.countField;
    //        }
    //        set
    //        {
    //            this.countField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public byte AverageAge
    //    {
    //        get
    //        {
    //            return this.averageAgeField;
    //        }
    //        set
    //        {
    //            this.averageAgeField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Uniform
    //    {
    //        get
    //        {
    //            return this.uniformField;
    //        }
    //        set
    //        {
    //            this.uniformField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlArrayItemAttribute("VehicleInformation", IsNullable = false)]
    //    public AgentBidResponseOfferVendorDetailsFleetVehicleInformation[] Vehicles
    //    {
    //        get
    //        {
    //            return this.vehiclesField;
    //        }
    //        set
    //        {
    //            this.vehiclesField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class AgentBidResponseOfferVendorDetailsFleetVehicleInformation
    //{

    //    private string makeField;

    //    private string modelField;

    //    private ushort countField;

    //    /// <remarks/>
    //    public string Make
    //    {
    //        get
    //        {
    //            return this.makeField;
    //        }
    //        set
    //        {
    //            this.makeField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string Model
    //    {
    //        get
    //        {
    //            return this.modelField;
    //        }
    //        set
    //        {
    //            this.modelField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public ushort Count
    //    {
    //        get
    //        {
    //            return this.countField;
    //        }
    //        set
    //        {
    //            this.countField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class AgentBidResponseFailedBids
    //{

    //    private AgentBidResponseFailedBidsFailedBid failedBidField;

    //    /// <remarks/>
    //    public AgentBidResponseFailedBidsFailedBid FailedBid
    //    {
    //        get
    //        {
    //            return this.failedBidField;
    //        }
    //        set
    //        {
    //            this.failedBidField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class AgentBidResponseFailedBidsFailedBid
    //{

    //    private AgentBidResponseFailedBidsFailedBidVendor vendorField;

    //    private AgentBidResponseFailedBidsFailedBidResult resultField;

    //    /// <remarks/>
    //    public AgentBidResponseFailedBidsFailedBidVendor Vendor
    //    {
    //        get
    //        {
    //            return this.vendorField;
    //        }
    //        set
    //        {
    //            this.vendorField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public AgentBidResponseFailedBidsFailedBidResult Result
    //    {
    //        get
    //        {
    //            return this.resultField;
    //        }
    //        set
    //        {
    //            this.resultField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class AgentBidResponseFailedBidsFailedBidVendor
    //{

    //    private byte idField;

    //    /// <remarks/>
    //    [System.Xml.Serialization.XmlAttributeAttribute()]
    //    public byte Id
    //    {
    //        get
    //        {
    //            return this.idField;
    //        }
    //        set
    //        {
    //            this.idField = value;
    //        }
    //    }
    //}

    ///// <remarks/>
    //[System.SerializableAttribute()]
    //[System.ComponentModel.DesignerCategoryAttribute("code")]
    //[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    //public partial class AgentBidResponseFailedBidsFailedBidResult
    //{

    //    private bool successField;

    //    private byte failureCodeField;

    //    private string failureReasonField;

    //    /// <remarks/>
    //    public bool Success
    //    {
    //        get
    //        {
    //            return this.successField;
    //        }
    //        set
    //        {
    //            this.successField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public byte FailureCode
    //    {
    //        get
    //        {
    //            return this.failureCodeField;
    //        }
    //        set
    //        {
    //            this.failureCodeField = value;
    //        }
    //    }

    //    /// <remarks/>
    //    public string FailureReason
    //    {
    //        get
    //        {
    //            return this.failureReasonField;
    //        }
    //        set
    //        {
    //            this.failureReasonField = value;
    //        }
    //    }
    //}


}
