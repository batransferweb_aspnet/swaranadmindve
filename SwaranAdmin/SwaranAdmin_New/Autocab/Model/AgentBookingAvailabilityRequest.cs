﻿using System.Collections.Generic;

namespace SwaranAdmin_New.Autocab.Model
{

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class AgentBookingAvailabilityRequest
    {

        private AgentBookingAvailabilityRequestAgent agentField;

        private AgentBookingAvailabilityRequestVendor vendorField;

        private string bidReferenceField;

        private AgentBookingAvailabilityRequestBookingParameters bookingParametersField;

        /// <remarks/>
        public AgentBookingAvailabilityRequestAgent Agent
        {
            get
            {
                return this.agentField;
            }
            set
            {
                this.agentField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityRequestVendor Vendor
        {
            get
            {
                return this.vendorField;
            }
            set
            {
                this.vendorField = value;
            }
        }

        /// <remarks/>
        public string BidReference
        {
            get
            {
                return this.bidReferenceField;
            }
            set
            {
                this.bidReferenceField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityRequestBookingParameters BookingParameters
        {
            get
            {
                return this.bookingParametersField;
            }
            set
            {
                this.bookingParametersField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestAgent
    {

        private string passwordField;

        private string referenceField;

        private string timeField;

        private int idField;

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        public string Time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestVendor
    {

        private int idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestBookingParameters
    {

        private string sourceField;

        private string bookingTimeField;

        private string availabilityField;

        private AgentBookingAvailabilityRequestBookingParametersPricing pricingField;

        private AgentBookingAvailabilityRequestBookingParametersJourney journeyField;

        private AgentBookingAvailabilityRequestBookingParametersRide rideField;

        /// <remarks/>
        public string Source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }

        /// <remarks/>
        public string BookingTime
        {
            get
            {
                return this.bookingTimeField;
            }
            set
            {
                this.bookingTimeField = value;
            }
        }

        /// <remarks/>
        public string Availability
        {
            get
            {
                return this.availabilityField;
            }
            set
            {
                this.availabilityField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityRequestBookingParametersPricing Pricing
        {
            get
            {
                return this.pricingField;
            }
            set
            {
                this.pricingField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityRequestBookingParametersJourney Journey
        {
            get
            {
                return this.journeyField;
            }
            set
            {
                this.journeyField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityRequestBookingParametersRide Ride
        {
            get
            {
                return this.rideField;
            }
            set
            {
                this.rideField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestBookingParametersPricing
    {

        private string currencyField;

        private string paymentTypeField;

        private string contractReferenceField;

        private string passcodeField;

        private string loyaltyCardNumberField;

        private AgentBookingAvailabilityRequestBookingParametersPricingInvoicedAccountCredentials invoicedAccountCredentialsField;

        private ushort maxPriceField;

        private int setPriceField;

        private string pricingMethodField;

        private AgentBookingAvailabilityRequestBookingParametersPricingCommission commissionField;

        private AgentBookingAvailabilityRequestBookingParametersPricingGratuity gratuityField;

        /// <remarks/>
        public string Currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        public string PaymentType
        {
            get
            {
                return this.paymentTypeField;
            }
            set
            {
                this.paymentTypeField = value;
            }
        }

        /// <remarks/>
        public string ContractReference
        {
            get
            {
                return this.contractReferenceField;
            }
            set
            {
                this.contractReferenceField = value;
            }
        }

        /// <remarks/>
        public string Passcode
        {
            get
            {
                return this.passcodeField;
            }
            set
            {
                this.passcodeField = value;
            }
        }

        /// <remarks/>
        public string LoyaltyCardNumber
        {
            get
            {
                return this.loyaltyCardNumberField;
            }
            set
            {
                this.loyaltyCardNumberField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityRequestBookingParametersPricingInvoicedAccountCredentials InvoicedAccountCredentials
        {
            get
            {
                return this.invoicedAccountCredentialsField;
            }
            set
            {
                this.invoicedAccountCredentialsField = value;
            }
        }

        /// <remarks/>
        public ushort MaxPrice
        {
            get
            {
                return this.maxPriceField;
            }
            set
            {
                this.maxPriceField = value;
            }
        }

        /// <remarks/>
        public int SetPrice
        {
            get
            {
                return this.setPriceField;
            }
            set
            {
                this.setPriceField = value;
            }
        }

        /// <remarks/>
        public string PricingMethod
        {
            get
            {
                return this.pricingMethodField;
            }
            set
            {
                this.pricingMethodField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityRequestBookingParametersPricingCommission Commission
        {
            get
            {
                return this.commissionField;
            }
            set
            {
                this.commissionField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityRequestBookingParametersPricingGratuity Gratuity
        {
            get
            {
                return this.gratuityField;
            }
            set
            {
                this.gratuityField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestBookingParametersPricingInvoicedAccountCredentials
    {

        private string identifierField;

        private string passwordField;

        private string keywordField;

        private AgentBookingAvailabilityRequestBookingParametersPricingInvoicedAccountCredentialsPayingAgent payingAgentField;

        /// <remarks/>
        public string Identifier
        {
            get
            {
                return this.identifierField;
            }
            set
            {
                this.identifierField = value;
            }
        }

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string Keyword
        {
            get
            {
                return this.keywordField;
            }
            set
            {
                this.keywordField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityRequestBookingParametersPricingInvoicedAccountCredentialsPayingAgent PayingAgent
        {
            get
            {
                return this.payingAgentField;
            }
            set
            {
                this.payingAgentField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestBookingParametersPricingInvoicedAccountCredentialsPayingAgent
    {

        private byte idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public byte Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestBookingParametersPricingCommission
    {

        private byte fixedField;

        private ushort rateField;

        /// <remarks/>
        public byte Fixed
        {
            get
            {
                return this.fixedField;
            }
            set
            {
                this.fixedField = value;
            }
        }

        /// <remarks/>
        public ushort Rate
        {
            get
            {
                return this.rateField;
            }
            set
            {
                this.rateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestBookingParametersPricingGratuity
    {

        private string typeField;

        private byte valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public byte Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestBookingParametersJourney
    {

        private AgentBookingAvailabilityRequestBookingParametersJourneyFrom fromField;

        private List<AgentBookingAvailabilityRequestBookingParametersJourneyVia> viasField;

        private AgentBookingAvailabilityRequestBookingParametersJourneyTO toField;

        /// <remarks/>
        public AgentBookingAvailabilityRequestBookingParametersJourneyFrom From
        {
            get
            {
                return this.fromField;
            }
            set
            {
                this.fromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Via", IsNullable = false)]
        public List<AgentBookingAvailabilityRequestBookingParametersJourneyVia> Vias
        {
            get
            {
                return this.viasField;
            }
            set
            {
                this.viasField = value;
            }
        }

        /// <remarks/>
        public AgentBookingAvailabilityRequestBookingParametersJourneyTO To
        {
            get
            {
                return this.toField;
            }
            set
            {
                this.toField = value;
            }
        }



    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestBookingParametersJourneyFrom
    {





        private string typeField;

        private JourneyPointCoordinate coordinateField;

        private string noteField;

        private string dataField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public JourneyPointCoordinate Coordinate
        {
            get
            {
                return this.coordinateField;
            }
            set
            {
                this.coordinateField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return this.noteField;
            }
            set
            {
                this.noteField = value;
            }
        }

        /// <remarks/>
        public string Data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }


        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class JourneyPointCoordinate
        {

            private decimal latitudeField;

            private decimal longitudeField;

            /// <remarks/>
            public decimal Latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public decimal Longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }
        }






    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestBookingParametersJourneyFromCoordinate
    {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal Latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal Longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestBookingParametersJourneyVia
    {


        private string typeField;

        private JourneyPointCoordinate coordinateField;

        private string noteField;

        private string dataField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public JourneyPointCoordinate Coordinate
        {
            get
            {
                return this.coordinateField;
            }
            set
            {
                this.coordinateField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return this.noteField;
            }
            set
            {
                this.noteField = value;
            }
        }

        /// <remarks/>
        public string Data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }


        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class JourneyPointCoordinate
        {

            private decimal latitudeField;

            private decimal longitudeField;

            /// <remarks/>
            public decimal Latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public decimal Longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }
        }


    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestBookingParametersJourneyViaCoordinate
    {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal Latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal Longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestBookingParametersJourneyTO
    {



        private string typeField;

        private JourneyPointCoordinate coordinateField;

        private string noteField;

        private string dataField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public JourneyPointCoordinate Coordinate
        {
            get
            {
                return this.coordinateField;
            }
            set
            {
                this.coordinateField = value;
            }
        }

        /// <remarks/>
        public string Note
        {
            get
            {
                return this.noteField;
            }
            set
            {
                this.noteField = value;
            }
        }

        /// <remarks/>
        public string Data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }


        /// <remarks/>
        [System.SerializableAttribute()]
        [System.ComponentModel.DesignerCategoryAttribute("code")]
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class JourneyPointCoordinate
        {

            private decimal latitudeField;

            private decimal longitudeField;

            /// <remarks/>
            public decimal Latitude
            {
                get
                {
                    return this.latitudeField;
                }
                set
                {
                    this.latitudeField = value;
                }
            }

            /// <remarks/>
            public decimal Longitude
            {
                get
                {
                    return this.longitudeField;
                }
                set
                {
                    this.longitudeField = value;
                }
            }
        }

    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestBookingParametersJourneyTOCoordinate
    {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal Latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal Longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBookingAvailabilityRequestBookingParametersRide
    {

        private int countField;

        private int luggageField;

        private string facilitiesField;

        private string driverTypeField;

        private string vehicleTypeField;

        private string vehicleCategoryField;

        private string typeField;

        /// <remarks/>
        public int Count
        {
            get
            {
                return this.countField;
            }
            set
            {
                this.countField = value;
            }
        }

        /// <remarks/>
        public int Luggage
        {
            get
            {
                return this.luggageField;
            }
            set
            {
                this.luggageField = value;
            }
        }

        /// <remarks/>
        public string Facilities
        {
            get
            {
                return this.facilitiesField;
            }
            set
            {
                this.facilitiesField = value;
            }
        }

        /// <remarks/>
        public string DriverType
        {
            get
            {
                return this.driverTypeField;
            }
            set
            {
                this.driverTypeField = value;
            }
        }

        /// <remarks/>
        public string VehicleType
        {
            get
            {
                return this.vehicleTypeField;
            }
            set
            {
                this.vehicleTypeField = value;
            }
        }

        /// <remarks/>
        public string VehicleCategory
        {
            get
            {
                return this.vehicleCategoryField;
            }
            set
            {
                this.vehicleCategoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }


}
