﻿using System.Collections.Generic;

namespace SwaranAdmin_New.Autocab.Model
{
    //SwaranAdmin_New.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class AgentBidRequest
    {

        private AgentBidRequestAgent agentField;

        private List<AgentBidRequestVendor> vendorsField;

        private AgentBidRequestBidParameters bidParametersField;

        /// <remarks/>
        public AgentBidRequestAgent Agent
        {
            get
            {
                return this.agentField;
            }
            set
            {
                this.agentField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Vendor", IsNullable = false)]
        public List<AgentBidRequestVendor> Vendors
        {
            get
            {
                return this.vendorsField;
            }
            set
            {
                this.vendorsField = value;
            }
        }

        /// <remarks/>
        public AgentBidRequestBidParameters BidParameters
        {
            get
            {
                return this.bidParametersField;
            }
            set
            {
                this.bidParametersField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestAgent
    {

        private string passwordField;

        private string referenceField;

        private string timeField;

        private int idField;

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string Reference
        {
            get
            {
                return this.referenceField;
            }
            set
            {
                this.referenceField = value;
            }
        }

        /// <remarks/>
        public string Time
        {
            get
            {
                return this.timeField;
            }
            set
            {
                this.timeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestVendor
    {

        private int idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestBidParameters
    {

        private string sourceField;

        private string bookingTimeField;

        private string availabilityField;

        private string vendorRatingField;

        private AgentBidRequestBidParametersPricing pricingField;

        private AgentBidRequestBidParametersJourney journeyField;

        private AgentBidRequestBidParametersRide rideField;

        /// <remarks/>
        public string Source
        {
            get
            {
                return this.sourceField;
            }
            set
            {
                this.sourceField = value;
            }
        }

        /// <remarks/>
        public string BookingTime
        {
            get
            {
                return this.bookingTimeField;
            }
            set
            {
                this.bookingTimeField = value;
            }
        }

        /// <remarks/>
        public string Availability
        {
            get
            {
                return this.availabilityField;
            }
            set
            {
                this.availabilityField = value;
            }
        }

        /// <remarks/>
        public string VendorRating
        {
            get
            {
                return this.vendorRatingField;
            }
            set
            {
                this.vendorRatingField = value;
            }
        }

        /// <remarks/>
        public AgentBidRequestBidParametersPricing Pricing
        {
            get
            {
                return this.pricingField;
            }
            set
            {
                this.pricingField = value;
            }
        }

        /// <remarks/>
        public AgentBidRequestBidParametersJourney Journey
        {
            get
            {
                return this.journeyField;
            }
            set
            {
                this.journeyField = value;
            }
        }

        /// <remarks/>
        public AgentBidRequestBidParametersRide Ride
        {
            get
            {
                return this.rideField;
            }
            set
            {
                this.rideField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestBidParametersPricing
    {

        private string currencyField;

        private string paymentTypeField;

        private string contractReferenceField;

        private string passcodeField;

        private string loyaltyCardNumberField;

        private AgentBidRequestBidParametersPricingInvoicedAccountCredentials invoicedAccountCredentialsField;

        private ushort maxPriceField;

        private ushort setPriceField;

        private string pricingMethodField;

        private AgentBidRequestBidParametersPricingCommission commissionField;

        private AgentBidRequestBidParametersPricingGratuity gratuityField;

        /// <remarks/>
        public string Currency
        {
            get
            {
                return this.currencyField;
            }
            set
            {
                this.currencyField = value;
            }
        }

        /// <remarks/>
        public string PaymentType
        {
            get
            {
                return this.paymentTypeField;
            }
            set
            {
                this.paymentTypeField = value;
            }
        }

        /// <remarks/>
        public string ContractReference
        {
            get
            {
                return this.contractReferenceField;
            }
            set
            {
                this.contractReferenceField = value;
            }
        }

        /// <remarks/>
        public string Passcode
        {
            get
            {
                return this.passcodeField;
            }
            set
            {
                this.passcodeField = value;
            }
        }

        /// <remarks/>
        public string LoyaltyCardNumber
        {
            get
            {
                return this.loyaltyCardNumberField;
            }
            set
            {
                this.loyaltyCardNumberField = value;
            }
        }

        /// <remarks/>
        public AgentBidRequestBidParametersPricingInvoicedAccountCredentials InvoicedAccountCredentials
        {
            get
            {
                return this.invoicedAccountCredentialsField;
            }
            set
            {
                this.invoicedAccountCredentialsField = value;
            }
        }

        /// <remarks/>
        public ushort MaxPrice
        {
            get
            {
                return this.maxPriceField;
            }
            set
            {
                this.maxPriceField = value;
            }
        }

        /// <remarks/>
        public ushort SetPrice
        {
            get
            {
                return this.setPriceField;
            }
            set
            {
                this.setPriceField = value;
            }
        }

        /// <remarks/>
        public string PricingMethod
        {
            get
            {
                return this.pricingMethodField;
            }
            set
            {
                this.pricingMethodField = value;
            }
        }

        /// <remarks/>
        public AgentBidRequestBidParametersPricingCommission Commission
        {
            get
            {
                return this.commissionField;
            }
            set
            {
                this.commissionField = value;
            }
        }

        /// <remarks/>
        public AgentBidRequestBidParametersPricingGratuity Gratuity
        {
            get
            {
                return this.gratuityField;
            }
            set
            {
                this.gratuityField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestBidParametersPricingInvoicedAccountCredentials
    {

        private string identifierField;

        private string passwordField;

        private string keywordField;

        private AgentBidRequestBidParametersPricingInvoicedAccountCredentialsPayingAgent payingAgentField;

        /// <remarks/>
        public string Identifier
        {
            get
            {
                return this.identifierField;
            }
            set
            {
                this.identifierField = value;
            }
        }

        /// <remarks/>
        public string Password
        {
            get
            {
                return this.passwordField;
            }
            set
            {
                this.passwordField = value;
            }
        }

        /// <remarks/>
        public string Keyword
        {
            get
            {
                return this.keywordField;
            }
            set
            {
                this.keywordField = value;
            }
        }

        /// <remarks/>
        public AgentBidRequestBidParametersPricingInvoicedAccountCredentialsPayingAgent PayingAgent
        {
            get
            {
                return this.payingAgentField;
            }
            set
            {
                this.payingAgentField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestBidParametersPricingInvoicedAccountCredentialsPayingAgent
    {

        private int idField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestBidParametersPricingCommission
    {

        private int fixedField;

        private ushort rateField;

        /// <remarks/>
        public int Fixed
        {
            get
            {
                return this.fixedField;
            }
            set
            {
                this.fixedField = value;
            }
        }

        /// <remarks/>
        public ushort Rate
        {
            get
            {
                return this.rateField;
            }
            set
            {
                this.rateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestBidParametersPricingGratuity
    {

        private string typeField;

        private int valueField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTextAttribute()]
        public int Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestBidParametersJourney
    {

        private AgentBidRequestBidParametersJourneyFrom fromField;

        private List<AgentBidRequestBidParametersJourneyVia> viasField;

        private AgentBidRequestBidParametersJourneyTO toField;

        /// <remarks/>
        public AgentBidRequestBidParametersJourneyFrom From
        {
            get
            {
                return this.fromField;
            }
            set
            {
                this.fromField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Via", IsNullable = false)]
        public List<AgentBidRequestBidParametersJourneyVia> Vias
        {
            get
            {
                return this.viasField;
            }
            set
            {
                this.viasField = value;
            }
        }

        /// <remarks/>
        public AgentBidRequestBidParametersJourneyTO To
        {
            get
            {
                return this.toField;
            }
            set
            {
                this.toField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestBidParametersJourneyFrom
    {

        private string typeField;

        private string dataField;

        private AgentBidRequestBidParametersJourneyFromCoordinate coordinateField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string Data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }

        /// <remarks/>
        public AgentBidRequestBidParametersJourneyFromCoordinate Coordinate
        {
            get
            {
                return this.coordinateField;
            }
            set
            {
                this.coordinateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestBidParametersJourneyFromCoordinate
    {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal Latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal Longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestBidParametersJourneyVia
    {

        private string typeField;

        private string dataField;

        private AgentBidRequestBidParametersJourneyViaCoordinate coordinateField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string Data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }

        /// <remarks/>
        public AgentBidRequestBidParametersJourneyViaCoordinate Coordinate
        {
            get
            {
                return this.coordinateField;
            }
            set
            {
                this.coordinateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestBidParametersJourneyViaCoordinate
    {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal Latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal Longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestBidParametersJourneyTO
    {

        private string typeField;

        private string dataField;

        private AgentBidRequestBidParametersJourneyTOCoordinate coordinateField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string Data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }

        /// <remarks/>
        public AgentBidRequestBidParametersJourneyTOCoordinate Coordinate
        {
            get
            {
                return this.coordinateField;
            }
            set
            {
                this.coordinateField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestBidParametersJourneyTOCoordinate
    {

        private decimal latitudeField;

        private decimal longitudeField;

        /// <remarks/>
        public decimal Latitude
        {
            get
            {
                return this.latitudeField;
            }
            set
            {
                this.latitudeField = value;
            }
        }

        /// <remarks/>
        public decimal Longitude
        {
            get
            {
                return this.longitudeField;
            }
            set
            {
                this.longitudeField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class AgentBidRequestBidParametersRide
    {

        private int countField;

        private int luggageField;

        private string facilitiesField;

        private string driverTypeField;

        private string vehicleTypeField;

        private string vehicleCategoryField;

        private string typeField;

        /// <remarks/>
        public int Count
        {
            get
            {
                return this.countField;
            }
            set
            {
                this.countField = value;
            }
        }

        /// <remarks/>
        public int Luggage
        {
            get
            {
                return this.luggageField;
            }
            set
            {
                this.luggageField = value;
            }
        }

        /// <remarks/>
        public string Facilities
        {
            get
            {
                return this.facilitiesField;
            }
            set
            {
                this.facilitiesField = value;
            }
        }

        /// <remarks/>
        public string DriverType
        {
            get
            {
                return this.driverTypeField;
            }
            set
            {
                this.driverTypeField = value;
            }
        }

        /// <remarks/>
        public string VehicleType
        {
            get
            {
                return this.vehicleTypeField;
            }
            set
            {
                this.vehicleTypeField = value;
            }
        }

        /// <remarks/>
        public string VehicleCategory
        {
            get
            {
                return this.vehicleCategoryField;
            }
            set
            {
                this.vehicleCategoryField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }
    }


}
