﻿using SwaranAdmin_New.Autocab.Base;
using SwaranAdmin_New.Autocab.Model;
using SwaranAdmin_New.Base;
using SwaranAdmin_New.DataManager;
using SwaranAdmin_New.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.WebPages;
using System.Xml;
using System.Xml.Linq;
using SwaranAdmin_New.Models.BookingMo;

namespace SwaranAdmin_New.Autocab
{
    public class AutoCapVehicletypeRequest
    {

      

        #region Autocap Set Price
        public static AgentBookingAvailabilityResponse GetBookingAvailabilityMainSetPrice(Booking booking, int distancePrice, AutoCabAgentViewModel config)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            string result = string.Empty;
            try
            {
                var request = new AgentBookingAvailabilityRequest();
                var agent = new AgentBookingAvailabilityRequestAgent();
                agent.Id = config.AgentId;//int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = config.AgentPassword;//AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;
                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = config.VendorId;//76479;// 700999;
                request.Vendor = vendor;
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();
                parameters.Source = config.Source;//"Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime = booking.BookingDateTime.ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = config.CashAccountPaymentType;//AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.CashAccountPaymentType).AutoCabConfigurationValue;
                pricing.ContractReference = config.CashAccountContractReference;//AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.CashAccountContractReference).AutoCabConfigurationValue;
                pricing.Passcode = "";
                pricing.LoyaltyCardNumber = "";
                pricing.SetPrice = distancePrice*100;
                parameters.Pricing = pricing;
                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();
                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                from.Data = booking.BookingFrom;
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                            (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                         (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = booking.DespatchPref + " " + (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                         (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                        (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = booking.FromLat;
                coordinate.Longitude = booking.FromLong;
                from.Coordinate = coordinate;
                journey.From = from;
                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                to.Data = booking.BookingTo;
                // to.Note = "to->Note";
                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();
                coordinatesTo.Latitude = booking.ToLat;
                coordinatesTo.Longitude = booking.ToLong;
                to.Coordinate = coordinatesTo;
                journey.To = to;
                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var viaList = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        //  newVia.Note = "newVia->Note";
                        newVia.Data = via.ViaFullAddress;
                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();
                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;
                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }
                parameters.Journey = journey;
                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";
                //int chileSeats = (booking.BoosterSeat > 0 ? booking.BoosterSeat : 0)
                //          + (booking.InfantSeats > 0 ? booking.InfantSeats : 0)
                //          + (booking.ChildSeats > 0 ? booking.ChildSeats : 0);
                ride.Count = booking.NumberOfPassengers;
                ride.Luggage = booking.Luggage;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleType;
                ride.VehicleCategory = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleCategory;
                parameters.Ride = ride;
                request.BookingParameters = parameters;
                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);
                XElement rootElement = XDocument.Parse(xmlString).Root;
                // string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = config.ApiUrl;//"https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;
                result = response.Content.ReadAsStringAsync().Result;
                XmlReader reader = XmlReader.Create(new StringReader(result));
                string failureCode = string.Empty;
                while (reader.Read())
                {
                    //if (reader.IsStartElement())
                    if (reader.IsStartElement())
                    {
                        //return only when you have START tag
                        switch (reader.Name.ToString())
                        {
                            case "Success":
                                if (Boolean.Parse(reader.ReadString()))
                                {
                                    bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);
                                }
                                break;
                            case "FailureCode":
                                failureCode = reader.ReadString();
                                break;
                            case "FailureReason":
                                ErrorHandling.LogFileWrite(  booking.BookingNumber +"FailureCode ->" + failureCode + "  ERROR => " + reader.ReadString(), result, System.Reflection.MethodBase.GetCurrentMethod().Name, reader.ReadString());
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message+"--->" + booking.BookingNumber, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }

        public static AgentBookingAvailabilityResponse GetBookingAvailabilityMainSetAccountPrice(Booking booking, int distancePrice, string contractReference, string ghostPaymentType) 
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            string result = string.Empty;
            try
            {
                var request = new AgentBookingAvailabilityRequest();
                var agent = new AgentBookingAvailabilityRequestAgent();
                agent.Id = int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;
                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = 76479;// 700999;
                request.Vendor = vendor;
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();
                parameters.Source = "Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime = booking.BookingDateTime.ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = ghostPaymentType;
                pricing.ContractReference = contractReference;
                pricing.Passcode = "";
                pricing.LoyaltyCardNumber = "";
                pricing.SetPrice = distancePrice * 100;
                parameters.Pricing = pricing;
                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();
                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                from.Data = booking.BookingFrom;
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                            (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                         (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = booking.DespatchPref + " " + (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                         (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                        (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = booking.FromLat;
                coordinate.Longitude = booking.FromLong;
                from.Coordinate = coordinate;
                journey.From = from;
                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                to.Data = booking.BookingTo;
                // to.Note = "to->Note";
                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();
                coordinatesTo.Latitude = booking.ToLat;
                coordinatesTo.Longitude = booking.ToLong;
                to.Coordinate = coordinatesTo;
                journey.To = to;
                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var viaList = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        //  newVia.Note = "newVia->Note";
                        newVia.Data = via.ViaFullAddress;
                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();
                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;
                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }
                parameters.Journey = journey;
                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";
                //int chileSeats = (booking.BoosterSeat > 0 ? booking.BoosterSeat : 0)
                //          + (booking.InfantSeats > 0 ? booking.InfantSeats : 0)
                //          + (booking.ChildSeats > 0 ? booking.ChildSeats : 0);
                ride.Count = booking.NumberOfPassengers;
                ride.Luggage = booking.Luggage;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleType;
                ride.VehicleCategory = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleCategory;
                parameters.Ride = ride;
                request.BookingParameters = parameters;
                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);
                XElement rootElement = XDocument.Parse(xmlString).Root;
                // string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = "https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;
                result = response.Content.ReadAsStringAsync().Result;
                XmlReader reader = XmlReader.Create(new StringReader(result));
                string failureCode = string.Empty;
                while (reader.Read())
                {
                    //if (reader.IsStartElement())
                    if (reader.IsStartElement())
                    {
                        //return only when you have START tag
                        switch (reader.Name.ToString())
                        {
                            case "Success":
                                if (Boolean.Parse(reader.ReadString()))
                                {
                                    bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);
                                }
                                break;
                            case "FailureCode":
                                failureCode = reader.ReadString();
                                break;
                            case "FailureReason":
                                ErrorHandling.LogFileWrite(booking.BookingNumber + "FailureCode ->" + failureCode + "  ERROR => " + reader.ReadString(), result, System.Reflection.MethodBase.GetCurrentMethod().Name, reader.ReadString());
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message + "--->" + booking.BookingNumber, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }

        public static AgentBookingAvailabilityResponse GetBookingAvailabilityReturnMainSetPrice(Booking booking, int distancePrice)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            try
            {
                var request = new AgentBookingAvailabilityRequest();
                var agent = new AgentBookingAvailabilityRequestAgent();
                agent.Id = int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;
                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = 76479;//700999;
                request.Vendor = vendor;
                // request.BidReference = bidReference;
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();
                parameters.Source = "Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime = booking.BookingDateTime.ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.CashAccountPaymentType).AutoCabConfigurationValue;
                pricing.ContractReference = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.CashAccountContractReference).AutoCabConfigurationValue;
                pricing.Passcode = "";
                pricing.LoyaltyCardNumber = "";
                pricing.SetPrice = distancePrice;
                parameters.Pricing = pricing;
                //distancePrice
                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();
                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                from.Data = booking.BookingFrom;
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                            (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                         (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                         (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                        (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = booking.FromLat;
                coordinate.Longitude = booking.FromLong;
                from.Coordinate = coordinate;
                journey.From = from;
                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                // to.Note = "to Note";
                to.Data = booking.BookingTo;
                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();
                coordinatesTo.Latitude = booking.ToLat;
                coordinatesTo.Longitude = booking.ToLong;
                to.Coordinate = coordinatesTo;
                journey.To = to;
                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var viaList = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        // newVia.Note = "Via Note";
                        newVia.Data = via.ViaFullAddress;
                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();
                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;
                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }
                parameters.Journey = journey;
                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";
                //int chileSeats = (booking.BoosterSeat > 0 ? booking.BoosterSeat : 0)
                //           + (booking.InfantSeats > 0 ? booking.InfantSeats : 0)
                //           + (booking.ChildSeats > 0 ? booking.ChildSeats : 0);
                ride.Count = booking.NumberOfPassengers;//+ chileSeats;
                ride.Luggage = booking.Luggage;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleType;
                ride.VehicleCategory = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleCategory;
                parameters.Ride = ride;
                request.BookingParameters = parameters;
                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);
                XElement rootElement = XDocument.Parse(xmlString).Root;
                //  string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = "https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                XmlReader reader = XmlReader.Create(new StringReader(result));
                string failureCode = string.Empty;
                while (reader.Read())
                {
                    //if (reader.IsStartElement())
                    if (reader.IsStartElement())
                    {
                        //return only when you have START tag
                        switch (reader.Name.ToString())
                        {
                            case "Success":
                                if (Boolean.Parse(reader.ReadString()))
                                {
                                    bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);
                                }
                                break;
                            case "FailureCode":
                                failureCode = reader.ReadString();
                                break;
                            case "FailureReason":
                                ErrorHandling.LogFileWrite(booking.BookingNumber + "FailureCode ->" + failureCode + "  ERROR => " + reader.ReadString(), result, System.Reflection.MethodBase.GetCurrentMethod().Name, reader.ReadString());
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message+"--->"+ booking.BookingNumber , ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }
        public static AgentBookingAvailabilityResponse GetBookingAvailabilityCardMainSetPrice(Booking booking, string worldpayOrderCode)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            try
            {
                var request = new AgentBookingAvailabilityRequest();
                var agent = new AgentBookingAvailabilityRequestAgent();
                //agent.Id = 20026;//300999;
                //agent.Password = "KyrSKp6f";// "jEHjE5Kv";
                agent.Id = int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;
                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = 76479;// 700999;
                request.Vendor = vendor;
                //   request.BidReference = bidReference;
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();
                parameters.Source = "Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime = booking.BookingDateTime.ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.PaymentType).AutoCabConfigurationValue;
                pricing.ContractReference = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.ContractReference).AutoCabConfigurationValue; ;
                pricing.Passcode = "";
                pricing.LoyaltyCardNumber = "";
                pricing.SetPrice = (int)booking.TotalDistancePrice * 100;
                //pricing.MaxPrice = 53;
                parameters.Pricing = pricing;
                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();
                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                from.Data = booking.BookingFrom;
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                            (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                         (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = booking.DespatchPref + " " + (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                         (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                        (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = booking.FromLat;
                coordinate.Longitude = booking.FromLong;
                from.Coordinate = coordinate;
                journey.From = from;
                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                to.Data = booking.BookingTo;
                // to.Note = "to->Note";
                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();
                coordinatesTo.Latitude = booking.ToLat;
                coordinatesTo.Longitude = booking.ToLong;
                to.Coordinate = coordinatesTo;
                journey.To = to;
                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var viaList = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        //  newVia.Note = "newVia->Note";
                        newVia.Data = via.ViaFullAddress;
                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();
                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;
                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }
                parameters.Journey = journey;
                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";
                //int chileSeats = (booking.BoosterSeat > 0 ? booking.BoosterSeat : 0)
                //               + (booking.InfantSeats > 0 ? booking.InfantSeats : 0)
                //               + (booking.ChildSeats > 0 ? booking.ChildSeats : 0);
                ride.Count = booking.NumberOfPassengers;// + chileSeats;
                ride.Luggage = booking.Luggage;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleType;
                ride.VehicleCategory = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleCategory;
                parameters.Ride = ride;
                request.BookingParameters = parameters;
                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);
                XElement rootElement = XDocument.Parse(xmlString).Root;
                // string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = "https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                XmlReader reader = XmlReader.Create(new StringReader(result));
                string failureCode = string.Empty;
                while (reader.Read())
                {
                    //if (reader.IsStartElement())
                    if (reader.IsStartElement())
                    {
                        //return only when you have START tag
                        switch (reader.Name.ToString())
                        {
                            case "Success":
                                if (Boolean.Parse(reader.ReadString()))
                                {
                                    bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);
                                }
                                break;
                            case "FailureCode":
                                failureCode = reader.ReadString();
                                break;
                            case "FailureReason":
                                ErrorHandling.LogFileWrite(booking.BookingNumber + "FailureCode ->" + failureCode + "  ERROR => " + reader.ReadString(), result, System.Reflection.MethodBase.GetCurrentMethod().Name, reader.ReadString());
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message+ "--->"+booking.BookingNumber , ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }
        public static AgentBookingAvailabilityResponse GetBookingAvailabilityReturnMainCardSetPrice(Booking bookingReturn, AutoCabAgentViewModel config)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            try
            {
                var request = new AgentBookingAvailabilityRequest();
                var agent = new AgentBookingAvailabilityRequestAgent();
                //agent.Id = 20026;// 300999;
                //agent.Password = "KyrSKp6f";// "jEHjE5Kv";
                agent.Id = config.AgentId;//int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = config.AgentPassword;//AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;
                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = config.VendorId;//76479;//700999;
                request.Vendor = vendor;
                //  request.BidReference = bidReference;
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();
                parameters.Source = config.Source;//"Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime = bookingReturn.BookingDateTime.ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = config.PaymentType;//AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.PaymentType).AutoCabConfigurationValue;
                pricing.ContractReference = config.ContractReference;//AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.ContractReference).AutoCabConfigurationValue;
                pricing.Passcode = "";
                pricing.LoyaltyCardNumber = "";
                pricing.SetPrice = ((int) bookingReturn.TotalDistancePrice - (int) bookingReturn.ConsumerDebit)*100;
                //pricing.MaxPrice = 53;
                parameters.Pricing = pricing;
                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();
                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    from.Note = "SA " + bookingReturn.DriverEllapseTime + " min" +
                            (bookingReturn.BoosterSeat > 0 ? "/BOO-" + bookingReturn.BoosterSeat : "") +
                           (bookingReturn.InfantSeats > 0 ? "/INF-" + bookingReturn.InfantSeats : "") +
                          (bookingReturn.ChildSeats > 0 ? "/CHS-" + bookingReturn.ChildSeats : "");
                }
                if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = "SA " + bookingReturn.DriverEllapseTime + " min" +
                         (bookingReturn.BoosterSeat > 0 ? "/BOO-" + bookingReturn.BoosterSeat : "") +
                           (bookingReturn.InfantSeats > 0 ? "/INF-" + bookingReturn.InfantSeats : "") +
                          (bookingReturn.ChildSeats > 0 ? "/CHS-" + bookingReturn.ChildSeats : "");
                }
                if (bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = (bookingReturn.BoosterSeat > 0 ? "/BOO-" + bookingReturn.BoosterSeat : "") +
                         (bookingReturn.InfantSeats > 0 ? "/INF-" + bookingReturn.InfantSeats : "") +
                        (bookingReturn.ChildSeats > 0 ? "/CHS-" + bookingReturn.ChildSeats : "");
                }
                from.Data = bookingReturn.BookingFrom;
                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = bookingReturn.FromLat;
                coordinate.Longitude = bookingReturn.FromLong;
                from.Coordinate = coordinate;
                journey.From = from;
                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                // to.Note = "to Note";
                to.Data = bookingReturn.BookingTo;
                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();
                coordinatesTo.Latitude = bookingReturn.ToLat;
                coordinatesTo.Longitude = bookingReturn.ToLong;
                to.Coordinate = coordinatesTo;
                journey.To = to;
                if (bookingReturn.JourneyViapoints != null && bookingReturn.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var viaList = bookingReturn.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        // newVia.Note = "Via Note";
                        newVia.Data = via.ViaFullAddress;
                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();
                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;
                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }
                parameters.Journey = journey;
                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";
                //int chileSeats = (bookingReturn.BoosterSeat > 0 ? bookingReturn.BoosterSeat : 0)
                //           + (bookingReturn.InfantSeats > 0 ? bookingReturn.InfantSeats : 0)
                //           + (bookingReturn.ChildSeats > 0 ? bookingReturn.ChildSeats : 0);
                ride.Count = bookingReturn.NumberOfPassengers;// + chileSeats;
                ride.Luggage = bookingReturn.Luggage;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = bookingReturn.BookingVehicleTypes.First().VehicleType.AutocabVehicleType;
                ride.VehicleCategory = bookingReturn.BookingVehicleTypes.First().VehicleType.AutocabVehicleCategory;
                parameters.Ride = ride;
                request.BookingParameters = parameters;
                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);
                XElement rootElement = XDocument.Parse(xmlString).Root;
                //  string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = config.ApiUrl;//"https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                XmlReader reader = XmlReader.Create(new StringReader(result));
                string failureCode = string.Empty;
                while (reader.Read())
                {
                    //if (reader.IsStartElement())
                    if (reader.IsStartElement())
                    {
                        //return only when you have START tag
                        switch (reader.Name.ToString())
                        {
                            case "Success":
                                if (Boolean.Parse(reader.ReadString()))
                                {
                                    bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);
                                }
                                break;
                            case "FailureCode":
                                failureCode = reader.ReadString();
                                break;
                            case "FailureReason":
                                ErrorHandling.LogFileWrite(bookingReturn.BookingNumber + "FailureCode ->" + failureCode + "  ERROR => " + reader.ReadString(), result, System.Reflection.MethodBase.GetCurrentMethod().Name, reader.ReadString());
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message+"-->"+ bookingReturn.BookingNumber , ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }
        private static AgentBookingAvailabilityResponse GetBookingAvailabilityeturnSetPrice(Booking booking, string bidReference)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            try
            {
                var request = new AgentBookingAvailabilityRequest();
                var agent = new AgentBookingAvailabilityRequestAgent();
                //agent.Id = 20026;// 300999;
                //agent.Password = "KyrSKp6f";// "jEHjE5Kv";
                agent.Id = int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;
                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = 76479;// 700999;
                request.Vendor = vendor;
                request.BidReference = bidReference;
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();
                parameters.Source = "Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime =   booking.BookingDateTime.ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = "Cash";
                pricing.LoyaltyCardNumber = "";
                parameters.Pricing = pricing;
                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();
                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                from.Note = "Note";
                from.Data = booking.BookingFrom;
                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = booking.FromLat;
                coordinate.Longitude = booking.FromLong;
                from.Coordinate = coordinate;
                journey.From = from;
                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                to.Note = "to Note";
                to.Data = booking.BookingTo;
                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();
                coordinatesTo.Latitude = booking.ToLat;
                coordinatesTo.Longitude = booking.ToLong;
                to.Coordinate = coordinatesTo;
                journey.To = to;
                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var viaList = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        newVia.Note = "Via Note";
                        newVia.Data = via.ViaFullAddress;
                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();
                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;
                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }
                parameters.Journey = journey;
                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";
                ride.Count = booking.NumberOfPassengers;
                ride.Luggage = 0;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleType;
                ride.VehicleCategory = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleCategory;
                parameters.Ride = ride;
                request.BookingParameters = parameters;
                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);
                XElement rootElement = XDocument.Parse(xmlString).Root;
                // string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = "https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                XmlReader reader = XmlReader.Create(new StringReader(result));
                string failureCode = string.Empty;
                while (reader.Read())
                {
                    //if (reader.IsStartElement())
                    if (reader.IsStartElement())
                    {
                        //return only when you have START tag
                        switch (reader.Name.ToString())
                        {
                            case "Success":
                                if (Boolean.Parse(reader.ReadString()))
                                {
                                    bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);
                                }
                                break;
                            case "FailureCode":
                                failureCode = reader.ReadString();
                                break;
                            case "FailureReason":
                                ErrorHandling.LogFileWrite(booking.BookingNumber + "FailureCode ->" + failureCode + "  ERROR => " + reader.ReadString(), result, System.Reflection.MethodBase.GetCurrentMethod().Name, reader.ReadString());
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message+"-->"+ booking.BookingNumber , ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }
        public static AgentBookingAvailabilityResponse GetBookingAvailabilityFreeJourneyMainSetPrice(Booking booking, int distancePrice, AutoCabAgentViewModel config)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            try
            {
                var request = new AgentBookingAvailabilityRequest();
                var agent = new AgentBookingAvailabilityRequestAgent();
                //agent.Id = 20026;//300999;
                //agent.Password = "KyrSKp6f";// "jEHjE5Kv";
                agent.Id = config.AgentId;//int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = config.AgentPassword;//AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;
                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = config.VendorId;//76479;// 700999;
                request.Vendor = vendor;
               // request.BidReference = bidReference;
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();
                parameters.Source = config.Source;//"Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime = booking.BookingDateTime.ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = config.FreeJourneyPaymentType;//AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.FreeJourneyPaymentType).AutoCabConfigurationValue;
                pricing.ContractReference = config.FreeJourneyContractReference;//AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.FreeJourneyContractReference).AutoCabConfigurationValue;
                pricing.Passcode = "";
                pricing.LoyaltyCardNumber = "";
                pricing.SetPrice = distancePrice;
                //pricing.MaxPrice = 53;
                parameters.Pricing = pricing;
                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();
                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                from.Data = booking.BookingFrom;
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                            (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                         (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = booking.DespatchPref + " " + (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                         (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                        (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = booking.FromLat;
                coordinate.Longitude = booking.FromLong;
                from.Coordinate = coordinate;
                journey.From = from;
                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                to.Data = booking.BookingTo;
                // to.Note = "to->Note";
                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();
                coordinatesTo.Latitude = booking.ToLat;
                coordinatesTo.Longitude = booking.ToLong;
                to.Coordinate = coordinatesTo;
                journey.To = to;
                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var viaList = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        //  newVia.Note = "newVia->Note";
                        newVia.Data = via.ViaFullAddress;
                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();
                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;
                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }
                parameters.Journey = journey;
                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";
                //int chileSeats = (booking.BoosterSeat > 0 ? booking.BoosterSeat : 0)
                //               + (booking.InfantSeats > 0 ? booking.InfantSeats : 0)
                //               + (booking.ChildSeats > 0 ? booking.ChildSeats : 0);
                ride.Count = booking.NumberOfPassengers;// + chileSeats;
                ride.Luggage = booking.Luggage;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleType;
                ride.VehicleCategory = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleCategory;
                parameters.Ride = ride;
                request.BookingParameters = parameters;
                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);
                XElement rootElement = XDocument.Parse(xmlString).Root;
                // string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = config.ApiUrl;//"https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                XmlReader reader = XmlReader.Create(new StringReader(result));
                string failureCode = string.Empty;
                while (reader.Read())
                {
                    //if (reader.IsStartElement())
                    if (reader.IsStartElement())
                    {
                        //return only when you have START tag
                        switch (reader.Name.ToString())
                        {
                            case "Success":
                                if (Boolean.Parse(reader.ReadString()))
                                {
                                    bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);
                                }
                                break;
                            case "FailureCode":
                                failureCode = reader.ReadString();
                                break;
                            case "FailureReason":
                                ErrorHandling.LogFileWrite(booking.BookingNumber + "FailureCode ->" + failureCode + "  ERROR => " + reader.ReadString(), result, System.Reflection.MethodBase.GetCurrentMethod().Name, reader.ReadString());
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message+"-->"+booking.BookingNumber, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }
        public static AgentBookingAvailabilityResponse GetBookingAvailabilityUseMyCreditsMainSetPrice(Booking booking, int distancePrice, AutoCabAgentViewModel config)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            try
            {
                var request = new AgentBookingAvailabilityRequest();
                var agent = new AgentBookingAvailabilityRequestAgent();
                //agent.Id = 20026;//300999;
                //agent.Password = "KyrSKp6f";// "jEHjE5Kv";
                agent.Id = config.AgentId;//int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = config.AgentPassword;//AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;
                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = config.VendorId;//76479;// 700999;
                request.Vendor = vendor; 
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();
                parameters.Source = config.Source;//"Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime = booking.BookingDateTime .ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = config.UseMyCreditPaymentType;//AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.UseMyCreditPaymentType).AutoCabConfigurationValue;
                pricing.ContractReference = config.UseMyCreditContractReference;//AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.UseMyCreditContractReference).AutoCabConfigurationValue;
                pricing.Passcode = "";
                pricing.LoyaltyCardNumber = "";
                pricing.SetPrice = distancePrice;
                //pricing.MaxPrice = 53;
                parameters.Pricing = pricing;
                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();
                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                from.Data = booking.BookingFrom;
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                            (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                         (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = booking.DespatchPref + " " + (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                         (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                        (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = booking.FromLat;
                coordinate.Longitude = booking.FromLong;
                from.Coordinate = coordinate;
                journey.From = from;
                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                to.Data = booking.BookingTo;
                // to.Note = "to->Note";
                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();
                coordinatesTo.Latitude = booking.ToLat;
                coordinatesTo.Longitude = booking.ToLong;
                to.Coordinate = coordinatesTo;
                journey.To = to;
                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var viaList = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        //  newVia.Note = "newVia->Note";
                        newVia.Data = via.ViaFullAddress;
                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();
                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;
                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }
                parameters.Journey = journey;
                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";
                //int chileSeats = (booking.BoosterSeat > 0 ? booking.BoosterSeat : 0)
                //               + (booking.InfantSeats > 0 ? booking.InfantSeats : 0)
                //               + (booking.ChildSeats > 0 ? booking.ChildSeats : 0);
                ride.Count = booking.NumberOfPassengers;// + chileSeats;
                ride.Luggage = booking.Luggage;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleType;
                ride.VehicleCategory = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleCategory;
                parameters.Ride = ride;
                request.BookingParameters = parameters;
                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);
                XElement rootElement = XDocument.Parse(xmlString).Root;
                // string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = config.ApiUrl;//"https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                XmlReader reader = XmlReader.Create(new StringReader(result));
                string failureCode = string.Empty;
                while (reader.Read())
                {
                    //if (reader.IsStartElement())
                    if (reader.IsStartElement())
                    {
                        //return only when you have START tag
                        switch (reader.Name.ToString())
                        {
                            case "Success":
                                if (Boolean.Parse(reader.ReadString()))
                                {
                                    bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);
                                }
                                break;
                            case "FailureCode":
                                failureCode = reader.ReadString();
                                break;
                            case "FailureReason":
                                ErrorHandling.LogFileWrite(booking.BookingNumber + "FailureCode ->" + failureCode + "  ERROR => " + reader.ReadString(), result, System.Reflection.MethodBase.GetCurrentMethod().Name, reader.ReadString());
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message+"-->"+booking.BookingNumber , ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }
        #endregion

        #region  Agent Booking Availability 

        public static AgentBookingAvailabilityResponse GetBookingAvailability(TelBookingViewModel booking, string bidReference, int agentId, string agentPw)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            try
            {

                var request = new AgentBookingAvailabilityRequest();


                var agent = new AgentBookingAvailabilityRequestAgent();
                agent.Id = agentId;// 20026;//300999;
                agent.Password = agentPw;// "KyrSKp6f";// "jEHjE5Kv";
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;

                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = 76479;// 700999;
                request.Vendor = vendor;

                request.BidReference = bidReference;
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();

                parameters.Source = "Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime = DateTime.Parse(booking.BookingDate + " " + booking.BookingTime).ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = "Cash";
                pricing.LoyaltyCardNumber = "";
                parameters.Pricing = pricing;


                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();


                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                from.Data = booking.BookingFrom;
                // from.Note = "from->Note";
                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = booking.FromLat;
                coordinate.Longitude = booking.FromLong;
                from.Coordinate = coordinate;

                journey.From = from;


                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                to.Data = booking.BookingTo;
                // to.Note = "to->Note";
                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();

                coordinatesTo.Latitude = booking.ToLat;
                coordinatesTo.Longitude = booking.ToLong;
                to.Coordinate = coordinatesTo;




                journey.To = to;

                if (booking.ViaPointAddress != null && booking.ViaPointAddress.Any(x => !x.Postcode.IsEmpty()))
                {
                    var viaList = booking.ViaPointAddress.Where(x => !x.Postcode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        //newVia.Note = "newVia->Note";
                        newVia.Data = via.FullAddress;
                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();

                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;


                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }

                parameters.Journey = journey;

                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";
                ride.Count = booking.NumOfPassengers;
                ride.Luggage = 0;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = booking.AutocabVehicleType;
                ride.VehicleCategory = booking.AutocabVehicleCategory;
                parameters.Ride = ride;
                request.BookingParameters = parameters;






                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);

                XElement rootElement = XDocument.Parse(xmlString).Root;

                // string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = "https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;


                var result = response.Content.ReadAsStringAsync().Result;
                bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);


            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }
        //public static AgentBookingAvailabilityResponse GetBookingAvailabilityCard(AddressInfoViewModel booking, string bidReference, int agentId, string agentPw)
        //{
        //    var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
        //    try
        //    {

        //        var request = new AgentBookingAvailabilityRequest();


        //        var agent = new AgentBookingAvailabilityRequestAgent();
        //        agent.Id = agentId;// 20026;//300999;
        //        agent.Password = agentPw;//"KyrSKp6f";// "jEHjE5Kv";
        //        agent.Reference = "BookingAvailabilityRef";
        //        agent.Time = DateTime.UtcNow.ToString("s") + "Z";
        //        request.Agent = agent;

        //        var vendor = new AgentBookingAvailabilityRequestVendor();
        //        vendor.Id = 76479;// 700999;
        //        request.Vendor = vendor;

        //        request.BidReference = bidReference;
        //        var parameters = new AgentBookingAvailabilityRequestBookingParameters();

        //        parameters.Source = "Website";
        //        //parameters.BookingTime = "2012-11-16T15:00:00";
        //        parameters.BookingTime = DateTime.Parse(booking.BookingDate + " " + booking.BookingTime).ToString("s");
        //        parameters.Availability = "Any";
        //        var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
        //        pricing.Currency = "GBP";
        //        pricing.PaymentType = "Account";
        //        pricing.ContractReference = "BAWP9999";//worldpay reference num
        //        //pricing.PaymentType = "Account"; Worldpay
        //        //pricing.ContractReference = "BACC9999";//worldpayOrderCode

        //        pricing.Passcode = "";
        //        pricing.LoyaltyCardNumber = "";
        //        //pricing.SetPrice = 33;
        //        //pricing.MaxPrice = 53;
        //        parameters.Pricing = pricing;


        //        var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();


        //        var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
        //        from.Type = "Address";
        //        from.Data = booking.BookingFrom;
        //        // from.Note = "from->Note";
        //        var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
        //        coordinate.Latitude = booking.FromLat;
        //        coordinate.Longitude = booking.FromLong;
        //        from.Coordinate = coordinate;

        //        journey.From = from;


        //        var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
        //        to.Type = "Address";
        //        to.Data = booking.BookingTo;
        //        // to.Note = "to->Note";
        //        var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();

        //        coordinatesTo.Latitude = booking.ToLat;
        //        coordinatesTo.Longitude = booking.ToLong;
        //        to.Coordinate = coordinatesTo;




        //        journey.To = to;

        //        if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
        //        {
        //            var viaList = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
        //            var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
        //            foreach (var via in viaList)
        //            {
        //                var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
        //                newVia.Type = "Address";
        //                //newVia.Note = "newVia->Note";
        //                newVia.Data = via.ViaFullAddress;
        //                var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();

        //                viaCoordinates.Latitude = via.Lat;
        //                viaCoordinates.Longitude = via.Long;
        //                newVia.Coordinate = viaCoordinates;


        //                listVia.Add(newVia);
        //            }
        //            journey.Vias = listVia;
        //        }

        //        parameters.Journey = journey;

        //        var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
        //        ride.Type = "Passenger";
        //        ride.Count = booking.NumberOfPassengers;
        //        ride.Luggage = 0;
        //        ride.Facilities = "None";
        //        ride.DriverType = "Any";
        //        ride.VehicleType = booking.AutocabVehicleType;
        //        ride.VehicleCategory = booking.AutocabVehicleCategory;
        //        parameters.Ride = ride;
        //        request.BookingParameters = parameters;






        //        string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);

        //        XElement rootElement = XDocument.Parse(xmlString).Root;

        //        // string apiUrl = "https://cxs-staging.autocab.net/api/agent";
        //        string apiUrl = "https://cxs.autocab.net/api/agent";
        //        HttpClient httpClient = new HttpClient();
        //        var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
        //        var response = httpClient.PostAsync(apiUrl, httpContent).Result;


        //        var result = response.Content.ReadAsStringAsync().Result;
        //        bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);


        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
        //    }
        //    return bookingAvailabilityResponse;
        //}

        public static AgentBookingAvailabilityResponse GetBookingAvailabilityMain(Booking booking)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            try
            {

                var request = new AgentBookingAvailabilityRequest();


                var agent = new AgentBookingAvailabilityRequestAgent();
                agent.Id = int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;

                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = 76479;// 700999;
                request.Vendor = vendor;

               // request.BidReference = bidReference;
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();

                parameters.Source = "Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime = booking.BookingDateTime.ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = "Cash";
                pricing.LoyaltyCardNumber = "";
                parameters.Pricing = pricing;



                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();


                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                from.Data = booking.BookingFrom;


                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                            (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                         (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = booking.DespatchPref + " " + (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                         (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                        (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }

                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = booking.FromLat;
                coordinate.Longitude = booking.FromLong;
                from.Coordinate = coordinate;

                journey.From = from;


                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                to.Data = booking.BookingTo;
                // to.Note = "to->Note";
                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();

                coordinatesTo.Latitude = booking.ToLat;
                coordinatesTo.Longitude = booking.ToLong;
                to.Coordinate = coordinatesTo;




                journey.To = to;

                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var viaList = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        //  newVia.Note = "newVia->Note";
                        newVia.Data = via.ViaFullAddress;
                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();

                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;


                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }

                parameters.Journey = journey;

                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";

                int chileSeats = (booking.BoosterSeat > 0 ? booking.BoosterSeat : 0)
                          + (booking.InfantSeats > 0 ? booking.InfantSeats : 0)
                          + (booking.ChildSeats > 0 ? booking.ChildSeats : 0);
                ride.Count = booking.NumberOfPassengers + chileSeats;
                ride.Luggage = booking.Luggage;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleType;
                ride.VehicleCategory = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleCategory;
                parameters.Ride = ride;
                request.BookingParameters = parameters;






                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);

                XElement rootElement = XDocument.Parse(xmlString).Root;

                // string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = "https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;


                var result = response.Content.ReadAsStringAsync().Result;
                bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);

                // ErrorHandling.LogFileWrite(xmlString, "", "", "");
                //  ErrorHandling.LogFileWrite(result, "", "", "");

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }
        public static AgentBookingAvailabilityResponse GetBookingAvailabilityReturnMain(Booking booking)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            try
            {
                var request = new AgentBookingAvailabilityRequest();


                var agent = new AgentBookingAvailabilityRequestAgent();
                agent.Id = int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;

                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = 76479;//700999;
                request.Vendor = vendor;

                //request.BidReference = bidReference;
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();

                parameters.Source = "Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime = booking.BookingDateTime.ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = "Cash";
                pricing.LoyaltyCardNumber = "";
                parameters.Pricing = pricing;


                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();

                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                from.Data = booking.BookingFrom;
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                            (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                         (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                         (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                        (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }

                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = booking.FromLat;
                coordinate.Longitude = booking.FromLong;
                from.Coordinate = coordinate;
                journey.From = from;


                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                // to.Note = "to Note";
                to.Data = booking.BookingTo;

                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();

                coordinatesTo.Latitude = booking.ToLat;
                coordinatesTo.Longitude = booking.ToLong;
                to.Coordinate = coordinatesTo;
                journey.To = to;

                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var viaList = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        // newVia.Note = "Via Note";
                        newVia.Data = via.ViaFullAddress;

                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();

                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;
                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }

                parameters.Journey = journey;

                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";
                int chileSeats = (booking.BoosterSeat > 0 ? booking.BoosterSeat : 0)
                           + (booking.InfantSeats > 0 ? booking.InfantSeats : 0)
                           + (booking.ChildSeats > 0 ? booking.ChildSeats : 0);
                ride.Count = booking.NumberOfPassengers + chileSeats;
                ride.Luggage = booking.Luggage;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleType;
                ride.VehicleCategory = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleCategory;
                parameters.Ride = ride;
                request.BookingParameters = parameters;






                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);

                XElement rootElement = XDocument.Parse(xmlString).Root;

                //  string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = "https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;


                var result = response.Content.ReadAsStringAsync().Result;
                bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);



            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }

        public static AgentBookingAvailabilityResponse GetBookingAvailabilityCardMain(Booking booking, string worldpayOrderCode)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            try
            {

                var request = new AgentBookingAvailabilityRequest();


                var agent = new AgentBookingAvailabilityRequestAgent();
                //agent.Id = 20026;//300999;
                //agent.Password = "KyrSKp6f";// "jEHjE5Kv";
                agent.Id = int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;

                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = 76479;// 700999;
                request.Vendor = vendor;
                 
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();

                parameters.Source = "Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime = booking.BookingDateTime.ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = "Account";
                pricing.ContractReference = "BAWP9999";
                pricing.Passcode = "";
                pricing.LoyaltyCardNumber = "";
                //pricing.SetPrice = 33;
                //pricing.MaxPrice = 53;

                parameters.Pricing = pricing;


                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();


                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                from.Data = booking.BookingFrom;

                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                            (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                         (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = booking.DespatchPref + " " + (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                         (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                        (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }


                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = booking.FromLat;
                coordinate.Longitude = booking.FromLong;
                from.Coordinate = coordinate;

                journey.From = from;


                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                to.Data = booking.BookingTo;
                // to.Note = "to->Note";
                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();

                coordinatesTo.Latitude = booking.ToLat;
                coordinatesTo.Longitude = booking.ToLong;
                to.Coordinate = coordinatesTo;




                journey.To = to;

                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var viaList = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        //  newVia.Note = "newVia->Note";
                        newVia.Data = via.ViaFullAddress;
                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();

                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;


                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }

                parameters.Journey = journey;

                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";
                int chileSeats = (booking.BoosterSeat > 0 ? booking.BoosterSeat : 0)
                               + (booking.InfantSeats > 0 ? booking.InfantSeats : 0)
                               + (booking.ChildSeats > 0 ? booking.ChildSeats : 0);

                ride.Count = booking.NumberOfPassengers + chileSeats;
                ride.Luggage = booking.Luggage;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleType;
                ride.VehicleCategory = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleCategory;
                parameters.Ride = ride;
                request.BookingParameters = parameters;






                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);

                XElement rootElement = XDocument.Parse(xmlString).Root;

                // string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = "https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;


                var result = response.Content.ReadAsStringAsync().Result;
                bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);


            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }

        public static AgentBookingAvailabilityResponse GetBookingAvailabilityReturnMainCard(Booking bookingReturn)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            try
            {
                var request = new AgentBookingAvailabilityRequest();


                var agent = new AgentBookingAvailabilityRequestAgent();
                //agent.Id = 20026;// 300999;
                //agent.Password = "KyrSKp6f";// "jEHjE5Kv";
                agent.Id = int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;

                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = 76479;//700999;
                request.Vendor = vendor;

               // request.BidReference = bidReference;
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();

                parameters.Source = "Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime = bookingReturn.BookingDateTime.ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = "Account";
                pricing.ContractReference = "BAWP9999";
                pricing.Passcode = "";
                pricing.LoyaltyCardNumber = "";
                //pricing.SetPrice = 33;
                //pricing.MaxPrice = 53;

                parameters.Pricing = pricing;


                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();

                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();

                from.Type = "Address";
                if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    from.Note = "SA " + bookingReturn.DriverEllapseTime + " min" +
                            (bookingReturn.BoosterSeat > 0 ? "/BOO-" + bookingReturn.BoosterSeat : "") +
                           (bookingReturn.InfantSeats > 0 ? "/INF-" + bookingReturn.InfantSeats : "") +
                          (bookingReturn.ChildSeats > 0 ? "/CHS-" + bookingReturn.ChildSeats : "");
                }
                if (bookingReturn.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = "SA " + bookingReturn.DriverEllapseTime + " min" +
                         (bookingReturn.BoosterSeat > 0 ? "/BOO-" + bookingReturn.BoosterSeat : "") +
                           (bookingReturn.InfantSeats > 0 ? "/INF-" + bookingReturn.InfantSeats : "") +
                          (bookingReturn.ChildSeats > 0 ? "/CHS-" + bookingReturn.ChildSeats : "");
                }
                if (bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && bookingReturn.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = (bookingReturn.BoosterSeat > 0 ? "/BOO-" + bookingReturn.BoosterSeat : "") +
                         (bookingReturn.InfantSeats > 0 ? "/INF-" + bookingReturn.InfantSeats : "") +
                        (bookingReturn.ChildSeats > 0 ? "/CHS-" + bookingReturn.ChildSeats : "");
                }

                from.Data = bookingReturn.BookingFrom;

                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = bookingReturn.FromLat;
                coordinate.Longitude = bookingReturn.FromLong;
                from.Coordinate = coordinate;
                journey.From = from;


                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                // to.Note = "to Note";
                to.Data = bookingReturn.BookingTo;

                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();

                coordinatesTo.Latitude = bookingReturn.ToLat;
                coordinatesTo.Longitude = bookingReturn.ToLong;
                to.Coordinate = coordinatesTo;
                journey.To = to;

                if (bookingReturn.JourneyViapoints != null && bookingReturn.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var viaList = bookingReturn.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        // newVia.Note = "Via Note";
                        newVia.Data = via.ViaFullAddress;

                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();

                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;
                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }

                parameters.Journey = journey;

                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";
                int chileSeats = (bookingReturn.BoosterSeat > 0 ? bookingReturn.BoosterSeat : 0)
                           + (bookingReturn.InfantSeats > 0 ? bookingReturn.InfantSeats : 0)
                           + (bookingReturn.ChildSeats > 0 ? bookingReturn.ChildSeats : 0);
                ride.Count = bookingReturn.NumberOfPassengers + chileSeats;
                ride.Luggage = bookingReturn.Luggage;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = bookingReturn.BookingVehicleTypes.First().VehicleType.AutocabVehicleType;
                parameters.Ride = ride;
                request.BookingParameters = parameters;






                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);

                XElement rootElement = XDocument.Parse(xmlString).Root;

                //  string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = "https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;


                var result = response.Content.ReadAsStringAsync().Result;
                bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);



            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }

        private static AgentBookingAvailabilityResponse GetBookingAvailabilityeturn(ReturnJourney bookingReturn, TelBookingViewModel booking, string bidReference)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            try
            {
                var request = new AgentBookingAvailabilityRequest();


                var agent = new AgentBookingAvailabilityRequestAgent();
                //agent.Id = 20026;// 300999;
                //agent.Password = "KyrSKp6f";// "jEHjE5Kv";
                agent.Id = int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;

                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = 76479;// 700999;
                request.Vendor = vendor;

                request.BidReference = bidReference;
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();

                parameters.Source = "Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime =
                    DateTime.Parse(bookingReturn.BookingDate + " " + bookingReturn.BookingTime).ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = "Cash";
                pricing.LoyaltyCardNumber = "";
                parameters.Pricing = pricing;


                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();

                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                from.Note = "Note";
                from.Data = bookingReturn.PickupPoint;

                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = bookingReturn.FromLat;
                coordinate.Longitude = bookingReturn.FromLong;
                from.Coordinate = coordinate;
                journey.From = from;


                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                to.Note = "to Note";
                to.Data = bookingReturn.DropoffPoint;

                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();

                coordinatesTo.Latitude = bookingReturn.ToLat;
                coordinatesTo.Longitude = bookingReturn.ToLong;
                to.Coordinate = coordinatesTo;
                journey.To = to;

                if (bookingReturn.ViaPointAddress != null && bookingReturn.ViaPointAddress.Any(x => !x.Postcode.IsEmpty()))
                {
                    var viaList = bookingReturn.ViaPointAddress.Where(x => !x.Postcode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        newVia.Note = "Via Note";
                        newVia.Data = via.FullAddress;

                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();

                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;
                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }

                parameters.Journey = journey;

                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";

                ride.Count = booking.NumOfPassengers;
                ride.Luggage = 0;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = bookingReturn.AutocabVehicleTypeName;
                parameters.Ride = ride;
                request.BookingParameters = parameters;






                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);

                XElement rootElement = XDocument.Parse(xmlString).Root;

                // string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = "https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;


                var result = response.Content.ReadAsStringAsync().Result;
                bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);



            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }

        public static AgentBookingAvailabilityResponse GetBookingAvailabilityFreeJourneyMain(Booking booking)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            try
            {

                var request = new AgentBookingAvailabilityRequest();


                var agent = new AgentBookingAvailabilityRequestAgent();
                //agent.Id = 20026;//300999;
                //agent.Password = "KyrSKp6f";// "jEHjE5Kv";
                agent.Id = int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;

                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = 76479;// 700999;
                request.Vendor = vendor;

             //   request.BidReference = bidReference;
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();

                parameters.Source = "Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime = booking.BookingDateTime.ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = "Account";
                pricing.ContractReference = "BAWP9999";
                pricing.Passcode = "";
                pricing.LoyaltyCardNumber = "";
                //pricing.SetPrice = 33;
                //pricing.MaxPrice = 53;

                parameters.Pricing = pricing;


                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();


                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                from.Data = booking.BookingFrom;

                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                            (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                         (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = booking.DespatchPref + " " + (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                         (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                        (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }


                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = booking.FromLat;
                coordinate.Longitude = booking.FromLong;
                from.Coordinate = coordinate;

                journey.From = from;


                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                to.Data = booking.BookingTo;
                // to.Note = "to->Note";
                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();

                coordinatesTo.Latitude = booking.ToLat;
                coordinatesTo.Longitude = booking.ToLong;
                to.Coordinate = coordinatesTo;




                journey.To = to;

                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var viaList = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        //  newVia.Note = "newVia->Note";
                        newVia.Data = via.ViaFullAddress;
                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();

                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;


                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }

                parameters.Journey = journey;

                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";
                int chileSeats = (booking.BoosterSeat > 0 ? booking.BoosterSeat : 0)
                               + (booking.InfantSeats > 0 ? booking.InfantSeats : 0)
                               + (booking.ChildSeats > 0 ? booking.ChildSeats : 0);

                ride.Count = booking.NumberOfPassengers + chileSeats;
                ride.Luggage = booking.Luggage;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleType;
                ride.VehicleCategory = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleCategory;
                parameters.Ride = ride;
                request.BookingParameters = parameters;






                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);

                XElement rootElement = XDocument.Parse(xmlString).Root;

                // string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = "https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;


                var result = response.Content.ReadAsStringAsync().Result;
                bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);


            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }

        public static AgentBookingAvailabilityResponse GetBookingAvailabilityUsMyCreditsMain(Booking booking)
        {
            var bookingAvailabilityResponse = new AgentBookingAvailabilityResponse();
            try
            {
                var request = new AgentBookingAvailabilityRequest();
                var agent = new AgentBookingAvailabilityRequestAgent();
                //agent.Id = 20026;//300999;
                //agent.Password = "KyrSKp6f";// "jEHjE5Kv";
                agent.Id = int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                request.Agent = agent;
                var vendor = new AgentBookingAvailabilityRequestVendor();
                vendor.Id = 76479;// 700999;
                request.Vendor = vendor;
                //request.BidReference = bidReference;
                var parameters = new AgentBookingAvailabilityRequestBookingParameters();
                parameters.Source = "Website";
                //parameters.BookingTime = "2012-11-16T15:00:00";
                parameters.BookingTime = booking.BookingDateTime.ToString("s");
                parameters.Availability = "Any";
                var pricing = new AgentBookingAvailabilityRequestBookingParametersPricing();
                pricing.Currency = "GBP";
                pricing.PaymentType = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.UseMyCreditPaymentType).AutoCabConfigurationValue;
                pricing.ContractReference = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.UseMyCreditContractReference).AutoCabConfigurationValue;
                pricing.Passcode = "";
                pricing.LoyaltyCardNumber = "";
                //pricing.SetPrice = 33;
                //pricing.MaxPrice = 53;
                parameters.Pricing = pricing;
                var journey = new AgentBookingAvailabilityRequestBookingParametersJourney();
                var from = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom();
                from.Type = "Address";
                from.Data = booking.BookingFrom;
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                            (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = "SA " + booking.DriverEllapseTime + " min" +
                         (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                           (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                          (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    from.Note = booking.DespatchPref + " " + (booking.BoosterSeat > 0 ? "/BOO-" + booking.BoosterSeat : "") +
                         (booking.InfantSeats > 0 ? "/INF-" + booking.InfantSeats : "") +
                        (booking.ChildSeats > 0 ? "/CHS-" + booking.ChildSeats : "");
                }
                var coordinate = new AgentBookingAvailabilityRequestBookingParametersJourneyFrom.JourneyPointCoordinate();
                coordinate.Latitude = booking.FromLat;
                coordinate.Longitude = booking.FromLong;
                from.Coordinate = coordinate;
                journey.From = from;
                var to = new AgentBookingAvailabilityRequestBookingParametersJourneyTO();
                to.Type = "Address";
                to.Data = booking.BookingTo;
                // to.Note = "to->Note";
                var coordinatesTo = new AgentBookingAvailabilityRequestBookingParametersJourneyTO.JourneyPointCoordinate();
                coordinatesTo.Latitude = booking.ToLat;
                coordinatesTo.Longitude = booking.ToLong;
                to.Coordinate = coordinatesTo;
                journey.To = to;
                if (booking.JourneyViapoints != null && booking.JourneyViapoints.Any(x => !x.ViapointPostCode.IsEmpty()))
                {
                    var viaList = booking.JourneyViapoints.Where(x => !x.ViapointPostCode.IsEmpty()).ToList();
                    var listVia = new List<AgentBookingAvailabilityRequestBookingParametersJourneyVia>();
                    foreach (var via in viaList)
                    {
                        var newVia = new AgentBookingAvailabilityRequestBookingParametersJourneyVia();
                        newVia.Type = "Address";
                        //  newVia.Note = "newVia->Note";
                        newVia.Data = via.ViaFullAddress;
                        var viaCoordinates = new AgentBookingAvailabilityRequestBookingParametersJourneyVia.JourneyPointCoordinate();
                        viaCoordinates.Latitude = via.Lat;
                        viaCoordinates.Longitude = via.Long;
                        newVia.Coordinate = viaCoordinates;
                        listVia.Add(newVia);
                    }
                    journey.Vias = listVia;
                }
                parameters.Journey = journey;
                var ride = new AgentBookingAvailabilityRequestBookingParametersRide();
                ride.Type = "Passenger";
                //int chileSeats = (booking.BoosterSeat > 0 ? booking.BoosterSeat : 0)
                //               + (booking.InfantSeats > 0 ? booking.InfantSeats : 0)
                //               + (booking.ChildSeats > 0 ? booking.ChildSeats : 0);
                ride.Count = booking.NumberOfPassengers;// + chileSeats;
                ride.Luggage = booking.Luggage;
                ride.Facilities = "None";
                ride.DriverType = "Any";
                ride.VehicleType = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleType;
                ride.VehicleCategory = booking.BookingVehicleTypes.First().VehicleType.AutocabVehicleCategory;
                parameters.Ride = ride;
                request.BookingParameters = parameters;
                string xmlString = XmlHelper.Serialize<AgentBookingAvailabilityRequest>(request);
                XElement rootElement = XDocument.Parse(xmlString).Root;
                // string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = "https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                bookingAvailabilityResponse = XmlHelper.Deserialize<AgentBookingAvailabilityResponse>(result);
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return bookingAvailabilityResponse;
        }
        #endregion

        #region Booking Authorization Request
        public static AuthorizationResponse BookingAuthorizationRequestReturn(Booking booking,string availabilityReference, decimal amount,string loyalty)
        {
            var authoResponse = new AuthorizationResponse();
            try
            {
                var bookingAuthorizationRequest = new AgentBookingAuthorizationRequest();
                var agent = new AgentBookingAuthorizationRequestAgent();
                //agent.Id = 20026;//300999;
                //agent.Password = "KyrSKp6f";//"jEHjE5Kv";
                agent.Id = int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                bookingAuthorizationRequest.Agent = agent;
                var vendor = new AgentBookingAuthorizationRequestVendor();
                vendor.Id = 76479;// 700999;
                bookingAuthorizationRequest.Vendor = vendor;
                bookingAuthorizationRequest.AvailabilityReference = availabilityReference;
                bookingAuthorizationRequest.AgentBookingReference = "";
                var passangerlist = new List<AgentBookingAuthorizationRequestPassengerDetails>();
                bool isLive = bool.Parse(ConfigurationManager.AppSettings["IsLive"]);
                if (!string.IsNullOrEmpty(booking.PassengerName))
                {
                    var bookedPassanger = new AgentBookingAuthorizationRequestPassengerDetails();
                    bookedPassanger.IsLead = true;
                    if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                    {
                        bookedPassanger.Name = isLive
                            ? booking.PassengerName + "-" +
                              booking.DriverDisplayName
                            : "Test Booking" + "-" + booking.DriverDisplayName;
                    }
                    if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                    {
                        bookedPassanger.Name = isLive
                            ? booking.PassengerName + "-" +
                              booking.DriverDisplayName
                            : "Test Booking" + "-" +booking.DriverDisplayName;
                    }
                    if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport &&
                        booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                    {
                        bookedPassanger.Name = isLive
                            ? booking.PassengerName
                            : "Test Booking";
                    }
                    bookedPassanger.TelephoneNumber = booking.PassengerTel;
                    string emailAdd = !string.IsNullOrEmpty(booking.PassengerEmail) ? booking.PassengerEmail : booking.BookedPersonEmail;
                    bookedPassanger.EmailAddress =emailAdd;
                    passangerlist.Add(bookedPassanger);
                }
                var bookedPerson = new AgentBookingAuthorizationRequestPassengerDetails();
                bookedPerson.IsLead = string.IsNullOrEmpty(booking.PassengerName);
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    bookedPerson.Name = isLive
                                         ? booking.PassengerName + "-" +booking.DriverDisplayName
                                         : "Test Booking" + "-" +booking.DriverDisplayName;
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    bookedPerson.Name = isLive
                                        ? booking.PassengerName + "-" + booking.DriverDisplayName
                                        : "Test Booking" + "-" +booking.DriverDisplayName;
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    bookedPerson.Name = isLive
                        ? booking.PassengerName
                        : "Test Booking";
                }
                bookedPerson.TelephoneNumber = booking.BookedPersonTel;//+ "/" + booking.BookedPersonAlternativeMobileNo;
                bookedPerson.EmailAddress = booking.BookedPersonEmail;
                passangerlist.Add(bookedPerson);
                bookingAuthorizationRequest.Passengers = passangerlist.ToArray();
                string alternativeNumber = !string.IsNullOrEmpty(booking.AlternativePhoneNumber)
                                        ? booking.AlternativePhoneNumber
                                        :"";
                string handLuggage = booking.HandLuggage > 0 ? "/HAN-LUG " + booking.HandLuggage : "";

                if (booking.PaymentMethod == (int)PaymentTypeEnum.Cash)
                {
                    bookingAuthorizationRequest.DriverNote = isLive ? "Cash £ " + amount + "/" + loyalty + " Customer/" + alternativeNumber + handLuggage + " / " + booking.CustomerComment
                                                              : "This is a test booking don't dispatch";
                }
                else
                {
                    bookingAuthorizationRequest.DriverNote = isLive ? loyalty + " Customer/" + alternativeNumber + handLuggage + " / " + booking.CustomerComment
                                                           : "This is a test booking don't dispatch";
                }
                bookingAuthorizationRequest.YourReference = booking.BookingNumber;
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    var flightDetails = new AgentBookingAuthorizationRequestFlightDetails();
                    flightDetails.FlightNumber = booking.FlightNo;
                    flightDetails.FlightDirection = "Arriving";
                    // flightDetails.DepartureDateLocal = booking.FlightNo;
                    flightDetails.ArrivalDateLocal = booking.FlightLandingDateTime != null ?
                        booking.FlightLandingDateTime.Value.ToString("s") : null;
                    // flightDetails.DepartureAirportCode = booking.FlightNo;
                    //  flightDetails.DepartureAirportName = "Arriving";
                    flightDetails.ArrivalAirportCode = booking.BookingFrom;
                    // flightDetails.ArrivalAirportName = "N" + booking.BookingFrom;
                    //flightDetails.DepartureTerminal = "Arriving";
                    //  flightDetails.ArrivalTerminal = booking.FlightNo;
                    flightDetails.AirlineCode = "";
                    //  flightDetails.AirlineName = "Arriving";
                    bookingAuthorizationRequest.FlightDetails = flightDetails;
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    var flightDetails = new AgentBookingAuthorizationRequestFlightDetails();
                    flightDetails.FlightNumber = booking.FlightNo;
                    flightDetails.FlightDirection = "Arriving";
                    // flightDetails.DepartureDateLocal = booking.FlightNo;
                    flightDetails.ArrivalDateLocal = booking.FlightLandingDateTime != null ?
                        booking.FlightLandingDateTime.Value.ToString("s") : null;
                    // flightDetails.DepartureAirportCode = booking.FlightNo;
                    //  flightDetails.DepartureAirportName = booking.BookingFrom;
                    flightDetails.ArrivalAirportCode = booking.BookingFrom;
                    //flightDetails.ArrivalAirportName = booking.BookingFrom;
                    //flightDetails.DepartureTerminal = "Arriving";
                    //  flightDetails.ArrivalTerminal = booking.FlightNo;
                    flightDetails.AirlineCode = "";
                    //  flightDetails.AirlineName = "Arriving";
                    bookingAuthorizationRequest.FlightDetails = flightDetails;
                }
                var notifications = new AgentBookingAuthorizationRequestNotifications();
                notifications.VendorEvents = "BookingDispatched  VehicleArrived PassengerOnBoard NoFare BookingCompleted BookingCancelled LocationUpdate";
                //   notifications.VendorEvents = " LocationUpdate";
                notifications.AlertMethod = "Textback";
                notifications.AgentEvents = "BookingDispatched  VehicleArrived PassengerOnBoard NoFare BookingCompleted BookingCancelled LocationUpdate";
                //   notifications.AgentEvents = "LocationUpdate";
                bookingAuthorizationRequest.Notifications = notifications;
                string xmlString = XmlHelper.Serialize<AgentBookingAuthorizationRequest>(bookingAuthorizationRequest);
                XElement rootElement = XDocument.Parse(xmlString).Root;
                //  string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = "https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                XElement xml = XDocument.Parse(result.ToString()).Root;
                if (xml != null)
                {
                    XElement statusElement = xml.Descendants("Success").First();
                    authoResponse.Status = bool.Parse(statusElement.Value);
                    if (authoResponse.Status)
                    {
                        XElement authorizationReferenceElement = xml.Descendants("AuthorizationReference").First();
                        authoResponse.AuthorizationReference = authorizationReferenceElement.Value;
                        XElement bookingReferenceElement = xml.Descendants("BookingReference").First();
                        authoResponse.BookingReference = bookingReferenceElement.Value;
                        authoResponse.FailureReason = "Successfully sent...";
                    }
                    else
                    {
                        XElement failureReasonElement = xml.Descendants("FailureReason").First();
                        authoResponse.FailureReason = failureReasonElement.Value;
                    }
                }
                // ErrorHandling.LogFileWrite(xmlString, "", "", "");
                //  ErrorHandling.LogFileWrite(result, "", "", "");
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message+"-->"+ booking.BookingNumber , ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return authoResponse;
        }
        public static AuthorizationResponse BookingAuthorizationRequest(Booking booking, string availabilityReference, decimal amount,string loyalty, AutoCabAgentViewModel config)
        {
            var authoResponse = new AuthorizationResponse();
            try
            {
                var bookingAuthorizationRequest = new AgentBookingAuthorizationRequest();
                var agent = new AgentBookingAuthorizationRequestAgent();
                //agent.Id = 20026;//300999;
                //agent.Password = "KyrSKp6f";//"jEHjE5Kv";
                agent.Id = config.AgentId; //int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = config.AgentPassword;//AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                bookingAuthorizationRequest.Agent = agent;
                var vendor = new AgentBookingAuthorizationRequestVendor();
                vendor.Id = config.VendorId;//76479;// 700999;
                bookingAuthorizationRequest.Vendor = vendor;
                bookingAuthorizationRequest.AvailabilityReference = availabilityReference;
                bookingAuthorizationRequest.AgentBookingReference = "";
                bool isLive = bool.Parse(ConfigurationManager.AppSettings["IsLive"]);
                var passangerlist = new List<AgentBookingAuthorizationRequestPassengerDetails>();
                if (!string.IsNullOrEmpty(booking.PassengerName))
                {
                    var bookedPassanger = new AgentBookingAuthorizationRequestPassengerDetails();
                    bookedPassanger.IsLead = true;
                    if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                    {
                        bookedPassanger.Name = isLive
                            ? booking.PassengerName + "-" +
                             booking.DriverDisplayName
                            : "Test Booking" + "-" +booking.DriverDisplayName;
                    }
                    if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                    {
                        bookedPassanger.Name = isLive
                            ? booking.PassengerName + "-" +
                              booking.DriverDisplayName
                            : "Test Booking" + "-" +booking.DriverDisplayName;
                    }
                    if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport &&
                        booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                    {
                        bookedPassanger.Name = isLive
                            ? booking.PassengerName
                            : "Test Booking";
                    }
                    bookedPassanger.TelephoneNumber = booking.PassengerTel;
                    string emailAdd = !string.IsNullOrEmpty(booking.PassengerEmail) ? booking.PassengerEmail : booking.BookedPersonEmail;
                    bookedPassanger.EmailAddress = emailAdd;
                    passangerlist.Add(bookedPassanger);
                }
                var bookedPerson = new AgentBookingAuthorizationRequestPassengerDetails();
                bookedPerson.IsLead = string.IsNullOrEmpty(booking.PassengerName);
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    bookedPerson.Name = isLive
                                         ? booking.BookedPersonName + "-" +booking.DriverDisplayName
                                         : "Test Booking" + "-" +booking.DriverDisplayName;
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    bookedPerson.Name = isLive
                                        ? booking.BookedPersonName + "-" + booking.DriverDisplayName
                                        : "Test Booking" + "-" +booking.DriverDisplayName;
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    bookedPerson.Name = isLive
                        ? booking.BookedPersonName
                        : "Test Booking";
                }
                bookedPerson.TelephoneNumber = booking.BookedPersonTel;//+ "/" + booking.BookedPersonAlternativeMobileNo;
                bookedPerson.EmailAddress = booking.BookedPersonEmail;
                passangerlist.Add(bookedPerson);
                bookingAuthorizationRequest.Passengers = passangerlist.ToArray();
                

                string alternativeNumber = !string.IsNullOrEmpty(booking.AlternativePhoneNumber)
                                        ? booking.AlternativePhoneNumber
                                        : "";
                string handLuggage = booking.HandLuggage > 0 ? "/HAN-LUG " + booking.HandLuggage : "";

                if (booking.PaymentMethod == (int)PaymentTypeEnum.Cash)
                {
                    bookingAuthorizationRequest.DriverNote = isLive ? "Cash £ " + amount + "/" + loyalty + " Customer/" + alternativeNumber + handLuggage + " / " + booking.CustomerComment
                                                              : "This is a test booking don't dispatch";
                }
                else
                {
                    bookingAuthorizationRequest.DriverNote = isLive ? loyalty + " Customer/" + alternativeNumber + handLuggage + " / " + booking.CustomerComment
                                                           : "This is a test booking don't dispatch";
                }

                
                bookingAuthorizationRequest.YourReference = booking.BookingNumber;
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    var flightDetails = new AgentBookingAuthorizationRequestFlightDetails();
                    flightDetails.FlightNumber = booking.FlightNo;
                    flightDetails.FlightDirection = "Arriving";
                    // flightDetails.DepartureDateLocal = booking.FlightNo;
                    flightDetails.ArrivalDateLocal = booking.FlightLandingDateTime!= null ? booking.FlightLandingDateTime.Value.ToString("s"):"";
                    // flightDetails.DepartureAirportCode = booking.FlightNo;
                    //  flightDetails.DepartureAirportName = "Arriving";
                    flightDetails.ArrivalAirportCode = booking.BookingFrom;
                    // flightDetails.ArrivalAirportName = "N" + booking.BookingFrom;
                    //flightDetails.DepartureTerminal = "Arriving";
                    //  flightDetails.ArrivalTerminal = booking.FlightNo;
                    flightDetails.AirlineCode = "";
                    //  flightDetails.AirlineName = "Arriving";
                    bookingAuthorizationRequest.FlightDetails = flightDetails;
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    var flightDetails = new AgentBookingAuthorizationRequestFlightDetails();
                    flightDetails.FlightNumber = booking.FlightNo;
                    flightDetails.FlightDirection = "Arriving";
                    // flightDetails.DepartureDateLocal = booking.FlightNo;
                    flightDetails.ArrivalDateLocal = booking.FlightLandingDateTime != null ?
                        booking.FlightLandingDateTime.Value.ToString("s") : null;
                    // flightDetails.DepartureAirportCode = booking.FlightNo;
                    //  flightDetails.DepartureAirportName = booking.BookingFrom;
                    flightDetails.ArrivalAirportCode = booking.BookingFrom;
                    //flightDetails.ArrivalAirportName = booking.BookingFrom;
                    //flightDetails.DepartureTerminal = "Arriving";
                    //  flightDetails.ArrivalTerminal = booking.FlightNo;
                    flightDetails.AirlineCode = "";
                    //  flightDetails.AirlineName = "Arriving";
                    bookingAuthorizationRequest.FlightDetails = flightDetails;
                }
                var notifications = new AgentBookingAuthorizationRequestNotifications();
                notifications.VendorEvents = "BookingDispatched  VehicleArrived PassengerOnBoard NoFare BookingCompleted BookingCancelled LocationUpdate";
                //   notifications.VendorEvents = " LocationUpdate";
                notifications.AlertMethod = "Textback";
                notifications.AgentEvents = "BookingDispatched  VehicleArrived PassengerOnBoard NoFare BookingCompleted BookingCancelled LocationUpdate";
                //   notifications.AgentEvents = "LocationUpdate";
                bookingAuthorizationRequest.Notifications = notifications;
                string xmlString = XmlHelper.Serialize<AgentBookingAuthorizationRequest>(bookingAuthorizationRequest);
                XElement rootElement = XDocument.Parse(xmlString).Root;
                string apiUrl = config.ApiUrl;//"https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;
                var result = response.Content.ReadAsStringAsync().Result;
                XElement xml = XDocument.Parse(result.ToString()).Root;
                if (xml != null)
                {
                    XElement statusElement = xml.Descendants("Success").First();
                    authoResponse.Status = bool.Parse(statusElement.Value); 
                    if (authoResponse.Status)
                    {
                        XElement authorizationReferenceElement = xml.Descendants("AuthorizationReference").First();
                        authoResponse.AuthorizationReference = authorizationReferenceElement.Value;
                        XElement bookingReferenceElement = xml.Descendants("BookingReference").First();
                        authoResponse.BookingReference = bookingReferenceElement.Value;
                        authoResponse.FailureReason = "Successfully sent...";
                    }
                    else
                    {
                        XElement failureReasonElement = xml.Descendants("FailureReason").First(); 
                        authoResponse.FailureReason = failureReasonElement.Value;
                    }
                }
                // ErrorHandling.LogFileWrite(xmlString, "", "", "");
                //  ErrorHandling.LogFileWrite(result, "", "", "");
            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message + "-->" + booking.BookingNumber, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return authoResponse;
        }  
        public static AuthorizationResponse BookingAuthorizationRequestMainCard(Booking booking, string availabilityReference, decimal amount, string loyalty, AutoCabAgentViewModel config)
        {
            var authoResponse = new AuthorizationResponse();
            try
            {
                var bookingAuthorizationRequest = new AgentBookingAuthorizationRequest();
                var agent = new AgentBookingAuthorizationRequestAgent();
                //agent.Id = 20026;//300999;
                //agent.Password = "KyrSKp6f";//"jEHjE5Kv";
                agent.Id = config.AgentId;//int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = config.AgentPassword;//AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                bookingAuthorizationRequest.Agent = agent;

                var vendor = new AgentBookingAuthorizationRequestVendor();
                vendor.Id = config.VendorId;//76479;// 700999;
                bookingAuthorizationRequest.Vendor = vendor;

                bookingAuthorizationRequest.AvailabilityReference = availabilityReference;
                bookingAuthorizationRequest.AgentBookingReference = "";

                var passangerlist = new List<AgentBookingAuthorizationRequestPassengerDetails>();




                bool isLive = bool.Parse(ConfigurationManager.AppSettings["IsLive"]);
                if (!string.IsNullOrEmpty(booking.PassengerName))
                {
                    var bookedPassanger = new AgentBookingAuthorizationRequestPassengerDetails();
                    bookedPassanger.IsLead = true;

                    if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                    {
                        bookedPassanger.Name = isLive
                            ? booking.PassengerName + "-" +
                              booking.DriverDisplayName
                            : "Test Booking" + "-" + booking.DriverDisplayName;

                    }
                    if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                    {

                        bookedPassanger.Name = isLive
                            ? booking.PassengerName + "-" +
                              booking.DriverDisplayName
                            : "Test Booking" + "-" + booking.DriverDisplayName;
                    }
                    if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport &&
                        booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                    {
                        bookedPassanger.Name = isLive
                            ? booking.PassengerName
                            : "Test Booking";
                    }
                    bookedPassanger.TelephoneNumber = booking.PassengerTel;
                    string emailAdd = !string.IsNullOrEmpty(booking.PassengerEmail) ? booking.PassengerEmail : booking.BookedPersonEmail;
                    bookedPassanger.EmailAddress = emailAdd;
                    passangerlist.Add(bookedPassanger);
                }


                var bookedPerson = new AgentBookingAuthorizationRequestPassengerDetails();
                bookedPerson.IsLead = string.IsNullOrEmpty(booking.PassengerName);

                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    bookedPerson.Name = isLive
                                         ? booking.BookedPersonName + "-" + booking.DriverDisplayName
                                         : "Test Booking" + "-" + booking.DriverDisplayName;

                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {

                    bookedPerson.Name = isLive
                                        ? booking.BookedPersonName + "-" + booking.DriverDisplayName
                                        : "Test Booking" + "-" + booking.DriverDisplayName;
                }
                if (booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Airport && booking.FromAriaPointTypeId != (int)AreapointTypeEnum.Cruiseport)
                {
                    bookedPerson.Name = isLive
                        ? booking.BookedPersonName
                        : "Test Booking";
                }
                bookedPerson.TelephoneNumber = booking.BookedPersonTel;//+ "/" + booking.BookedPersonAlternativeMobileNo;
                bookedPerson.EmailAddress = booking.BookedPersonEmail;
                passangerlist.Add(bookedPerson);

                bookingAuthorizationRequest.Passengers = passangerlist.ToArray();

               


                string handLuggage = booking.HandLuggage > 0 ? "/HAN-LUG " + booking.HandLuggage : "";
                if (booking.PaymentMethod == (int)PaymentTypeEnum.Cash)
                {
                    bookingAuthorizationRequest.DriverNote = isLive ? "Cash £ " + amount + "/" + loyalty + "/" + booking.AlternativePhoneNumber + handLuggage + "/" + booking.CustomerComment
                                                              : "This is a test booking don't dispatch";
                }
                else if (booking.JourneyType == (int)JourneyTypeEnum.TwoWay && booking.PaymentMethod == (int)PaymentTypeEnum.Card)
                {
                  //  string cardprocessingfee = booking.OtherCharges > 0 ? "CPF £ " + booking.OtherCharges + " /" : "";
                    bookingAuthorizationRequest.DriverNote = isLive ?  loyalty + "/" + booking.AlternativePhoneNumber + handLuggage + " / " + booking.CustomerComment
                                                              : "This is a test booking don't dispatch";
                }
                else
                {
                    bookingAuthorizationRequest.DriverNote = isLive ? loyalty + "/" + booking.AlternativePhoneNumber + handLuggage + " / " + booking.CustomerComment
                                                           : "This is a test booking don't dispatch";
                }
                bookingAuthorizationRequest.YourReference = booking.BookingNumber;
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Airport)
                {
                    var flightDetails = new AgentBookingAuthorizationRequestFlightDetails();
                    flightDetails.FlightNumber = booking.FlightNo;
                    flightDetails.FlightDirection = "Arriving";
                    // flightDetails.DepartureDateLocal = booking.FlightNo;
                    flightDetails.ArrivalDateLocal = booking.FlightLandingDateTime != null ?
                        booking.FlightLandingDateTime.Value.ToString("s") : null;
                    // flightDetails.DepartureAirportCode = booking.FlightNo;
                    //  flightDetails.DepartureAirportName = "Arriving";

                    flightDetails.ArrivalAirportCode = booking.BookingFrom;
                    // flightDetails.ArrivalAirportName = "N" + booking.BookingFrom;
                    //flightDetails.DepartureTerminal = "Arriving";
                    //  flightDetails.ArrivalTerminal = booking.FlightNo;
                    flightDetails.AirlineCode = "";
                    //  flightDetails.AirlineName = "Arriving";

                    bookingAuthorizationRequest.FlightDetails = flightDetails;
                }
                if (booking.FromAriaPointTypeId == (int)AreapointTypeEnum.Cruiseport)
                {
                    var flightDetails = new AgentBookingAuthorizationRequestFlightDetails();
                    flightDetails.FlightNumber = booking.FlightNo;
                    flightDetails.FlightDirection = "Arriving";
                    // flightDetails.DepartureDateLocal = booking.FlightNo;
                    flightDetails.ArrivalDateLocal = booking.FlightLandingDateTime != null ?
                       booking.FlightLandingDateTime.Value.ToString("s") : null;
                    // flightDetails.DepartureAirportCode = booking.FlightNo;
                    //  flightDetails.DepartureAirportName = booking.BookingFrom;

                    flightDetails.ArrivalAirportCode = booking.BookingFrom;
                    //flightDetails.ArrivalAirportName = booking.BookingFrom;
                    //flightDetails.DepartureTerminal = "Arriving";
                    //  flightDetails.ArrivalTerminal = booking.FlightNo;
                    flightDetails.AirlineCode = "";
                    //  flightDetails.AirlineName = "Arriving";

                    bookingAuthorizationRequest.FlightDetails = flightDetails;
                }

                var notifications = new AgentBookingAuthorizationRequestNotifications();

                notifications.VendorEvents = "BookingDispatched  VehicleArrived PassengerOnBoard NoFare BookingCompleted BookingCancelled LocationUpdate";
                //   notifications.VendorEvents = " LocationUpdate";
                notifications.AlertMethod = "Textback";
                notifications.AgentEvents = "BookingDispatched  VehicleArrived PassengerOnBoard NoFare BookingCompleted BookingCancelled LocationUpdate";
                //   notifications.AgentEvents = "LocationUpdate";
                bookingAuthorizationRequest.Notifications = notifications;
                string xmlString = XmlHelper.Serialize<AgentBookingAuthorizationRequest>(bookingAuthorizationRequest);

                XElement rootElement = XDocument.Parse(xmlString).Root;

                //  string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = config.ApiUrl;//"https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;


                var result = response.Content.ReadAsStringAsync().Result;
                XElement xml = XDocument.Parse(result.ToString()).Root;
                if (xml != null)
                {
                    XElement statusElement = xml.Descendants("Success").First();
                    authoResponse.Status = bool.Parse(statusElement.Value);
                    if (authoResponse.Status)
                    {
                        XElement authorizationReferenceElement = xml.Descendants("AuthorizationReference").First();
                        authoResponse.AuthorizationReference = authorizationReferenceElement.Value;

                        XElement bookingReferenceElement = xml.Descendants("BookingReference").First();
                        authoResponse.BookingReference = bookingReferenceElement.Value;
                        authoResponse.FailureReason = "Successfully sent...";
                    }
                    else
                    {
                        XElement failureReasonElement = xml.Descendants("FailureReason").First();
                        authoResponse.FailureReason = failureReasonElement.Value;
                    }
                }
                // ErrorHandling.LogFileWrite(xmlString, "", "", "");
                //  ErrorHandling.LogFileWrite(result, "", "", "");

            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message + "-->" + booking.BookingNumber, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }

            return authoResponse;
        }

        #endregion

        #region AgentBooking Cancellation Request
        public static BookingCancellationResponse AgentBookingCancellationRequestAgent(string authorizationReference)
        {
            var objResponse = new BookingCancellationResponse();
            try
            {
                var bookingCancellation = new AgentBookingCancellationRequest();

                var agent = new AgentBookingCancellationRequestAgent();
                //agent.Id = 20026;// 300999;
                //agent.Password = "KyrSKp6f";// "jEHjE5Kv";
                agent.Id = int.Parse(AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentId).AutoCabConfigurationValue);
                agent.Password = AutoCabHelper.GetAutoCabConfigurationById((int)AutoCabConfigurationEnum.AgentPassword).AutoCabConfigurationValue;
                agent.Reference = "BookingAvailabilityRef";
                agent.Time = DateTime.UtcNow.ToString("s") + "Z";
                bookingCancellation.Agent = agent;

                var vendor = new AgentBookingCancellationRequestVendor();
                vendor.Id = 76479;//700999;
                bookingCancellation.Vendor = vendor;

                bookingCancellation.AuthorizationReference = authorizationReference;

                string xmlString = XmlHelper.Serialize<AgentBookingCancellationRequest>(bookingCancellation);

                XElement rootElement = XDocument.Parse(xmlString).Root;

                // string apiUrl = "https://cxs-staging.autocab.net/api/agent";
                string apiUrl = "https://cxs.autocab.net/api/agent";
                HttpClient httpClient = new HttpClient();
                var httpContent = new StringContent(rootElement.ToString(), Encoding.UTF8, "application/xml");
                var response = httpClient.PostAsync(apiUrl, httpContent).Result;

                var result = response.Content.ReadAsStringAsync().Result;
                XElement xml = XDocument.Parse(result.ToString()).Root;
                if (xml != null)
                {
                    XElement authorizationReferenceElement = xml.Descendants("Success").First();
                    objResponse.Status = bool.Parse(authorizationReferenceElement.Value);
                    if (!objResponse.Status)
                    {
                        XElement bookingReferenceElement = xml.Descendants("FailureReason").First();
                        objResponse.FailureReason = bookingReferenceElement.Value;
                    }
                }



            }
            catch (Exception ex)
            {
                ErrorHandling.LogFileWrite(ex.Message, ex.StackTrace, System.Reflection.MethodBase.GetCurrentMethod().Name, ex.InnerException != null ? ex.InnerException.Message : "");
            }
            return objResponse;
        }
        #endregion

     
    }


    public class AuthorizationResponse
    {
        public string AuthorizationReference { get; set; }

        public string BookingReference { get; set; }

        public bool Status { get; set; }
        public int FailureCode { get; set; }
        public string FailureReason { get; set; }
    }

    public class BookingCancellationResponse
    {
        public bool Status { get; set; }
        public int FailureCode { get; set; }
        public string FailureReason { get; set; }
    }
}